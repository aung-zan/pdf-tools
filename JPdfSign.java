import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.TSAClient;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.CrlClient;
import java.util.Collection;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import java.security.Provider;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import com.itextpdf.text.Rectangle;
import java.io.OutputStream;
import com.itextpdf.text.pdf.PdfStamper;
import java.io.FileOutputStream;
import com.itextpdf.text.pdf.PdfReader;
import java.security.PrivateKey;
import java.io.InputStream;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.util.Arrays;
import java.io.IOException;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Image;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.Phrase;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfGState;
import java.util.Calendar;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.security.CertificateInfo;
import java.security.cert.X509Certificate;
import java.security.cert.Certificate;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

// 
// Decompiled by Procyon v0.5.36
// 

public class JPdfSign
{
    protected static Map<String, String> getOptions(final String[] args) {
        final Map<String, String> options = new HashMap<String, String>();
        for (int i = 0; i < args.length; ++i) {
            if (args[i].startsWith("-")) {
                options.put(args[i].replaceAll("^[-]+", ""), args[++i]);
            }
        }
        return options;
    }
    
    protected static String[] getParam(final String[] args) {
        final List<String> arr = new ArrayList<String>();
        for (int i = 0; i < args.length; ++i) {
            if (args[i].startsWith("-")) {
                ++i;
            }
            else {
                arr.add(args[i]);
            }
        }
        return arr.toArray(new String[arr.size()]);
    }
    
    public static void main(final String[] args) {
        sign(args);
    }
    
    public static String extractName(final Certificate signCertificate) {
        String name = "";
        final CertificateInfo.X500Name x500name = CertificateInfo.getSubjectFields((X509Certificate)signCertificate);
        if (x500name != null) {
            name = x500name.getField("CN");
            if (name == null) {
                name = x500name.getField("E");
            }
        }
        return name;
    }
    
    public static void setupSignerInfo(final PdfSignatureAppearance sap, final Map<String, String> options) {
        sap.setReason(options.containsKey("reason") ? options.get("reason") : "\u5909\u66f4\u304c\u306a\u3044\u3068\u8a3c\u660e\u3059\u308b");
        sap.setLocation(options.containsKey("location") ? options.get("location") : "\u6771\u4eac");
        sap.setContact(options.containsKey("contact") ? options.get("contact") : "Tel: 03-5424-4052");
        sap.setSignDate(Calendar.getInstance());
    }
    
    public static void drawSignDate(final PdfSignatureAppearance sap) throws DocumentException {
        final PdfTemplate layer2 = sap.getLayer(2);
        final PdfGState gstate = new PdfGState();
        gstate.setFillOpacity(0.6f);
        layer2.setGState(gstate);
        final ColumnText ct = new ColumnText(layer2);
        ct.setRunDirection(sap.getRunDirection());
        final Font font = new Font(Font.FontFamily.HELVETICA, 12.0f, 1, BaseColor.RED);
        ct.setSimpleColumn(new Phrase(new SimpleDateFormat("YYYY-MM-dd").format(new Date()), font), 2.0f, 2.0f, layer2.getWidth() - 2.0f, 5.0f + font.getSize(), font.getSize(), 1);
        ct.go();
    }
    
    public static void drawImageSignature(final PdfSignatureAppearance sap, final String logo) throws BadElementException, DocumentException, IOException {
        final PdfTemplate layer0 = sap.getLayer(0);
        final ColumnText ct = new ColumnText(layer0);
        ct.setRunDirection(sap.getRunDirection());
        ct.setSimpleColumn(layer0.getBoundingBox());
        final Image image = Image.getInstance(logo);
        image.scaleToFit(layer0.getWidth(), layer0.getHeight());
        final Paragraph p = new Paragraph(layer0.getHeight());
        p.add(new Chunk(image, 0.0f, 0.0f, false));
        ct.addElement(p);
        ct.go();
    }
    
    public static void drawName(final PdfSignatureAppearance sap, final String name) throws DocumentException {
        final PdfTemplate layer0 = sap.getLayer(0);
        final PdfGState gstate = new PdfGState();
        gstate.setFillOpacity(0.6f);
        layer0.setGState(gstate);
        final ColumnText ct = new ColumnText(layer0);
        ct.setRunDirection(sap.getRunDirection());
        final Font font = new Font(Font.FontFamily.HELVETICA, 20.0f, 1, BaseColor.RED);
        ct.setSimpleColumn(new Phrase(name, font), 2.0f, 2.0f, layer0.getWidth() - 2.0f, layer0.getHeight() - 2.0f, font.getSize(), 1);
        ct.go();
    }
    
    public static int getIntOption(final Map<String, String> options, final String key, final int defaultV) {
        if (options.containsKey(key)) {
            return Integer.parseInt(options.get(key));
        }
        return defaultV;
    }
    
    public static void sign(final String[] args) {
        try {
            final String[] params = getParam(args);
            final Map<String, String> options = getOptions(args);
            if (options.containsKey("debug")) {
                System.out.println(Arrays.toString(args));
                System.out.println(Arrays.toString(params));
                System.out.println(options);
            }
            String pkcs12FileName = "test.pfx";
            String pdfInputFileName = "test.pdf";
            String pdfOutputFileName = "out.pdf";
            String password = "123456";
            if (params.length > 0) {
                pkcs12FileName = params[0].trim();
            }
            if (params.length > 1) {
                password = params[1].trim();
            }
            if (params.length > 2) {
                pdfInputFileName = params[2].trim();
            }
            if (params.length > 3) {
                pdfOutputFileName = params[3].trim();
            }
            final KeyStore ks = KeyStore.getInstance("pkcs12");
            ks.load(new FileInputStream(pkcs12FileName), password.toCharArray());
            final String alias = ks.aliases().nextElement();
            final PrivateKey privateKey = (PrivateKey)ks.getKey(alias, password.toCharArray());
            final Certificate[] certificateChain = ks.getCertificateChain(alias);
            final PdfReader reader = new PdfReader(pdfInputFileName);
            final FileOutputStream fout = new FileOutputStream(pdfOutputFileName);
            final PdfStamper stp = PdfStamper.createSignature(reader, fout, '\0');
            stp.setEncryption(null, "!!!c???p??????s??#!!!".getBytes(), 2052, true);
            final PdfSignatureAppearance sap = stp.getSignatureAppearance();
            setupSignerInfo(sap, options);
            final Rectangle pageSize = reader.getPageSize(1);
            sap.setVisibleSignature(new Rectangle((float)getIntOption(options, "signatureX", 50), pageSize.getHeight() - getIntOption(options, "signatureY", 50) - getIntOption(options, "signatureH", 50), (float)(getIntOption(options, "signatureX", 50) + getIntOption(options, "signatureW", 100)), pageSize.getHeight() - getIntOption(options, "signatureY", 50)), 1, null);
            if (options.containsKey("signature")) {
                drawImageSignature(sap, options.get("signature"));
            }
            else {
                drawName(sap, extractName(certificateChain[0]));
            }
            drawSignDate(sap);
            final BouncyCastleProvider provider = new BouncyCastleProvider();
            Security.addProvider(provider);
            final PrivateKeySignature pks = new PrivateKeySignature(privateKey, "SHA-256", provider.getName());
            final ExternalDigest digest = new BouncyCastleDigest();
            MakeSignature.signDetached(sap, digest, pks, certificateChain, null, null, null, 0, MakeSignature.CryptoStandard.CMS);
        }
        catch (Throwable kse) {
            kse.printStackTrace(System.out);
            System.exit(-1);
        }
    }
}
