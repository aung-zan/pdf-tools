// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.log;

public class SysoCounter implements Counter
{
    protected String name;
    
    public SysoCounter() {
        this.name = "iText";
    }
    
    protected SysoCounter(final Class<?> klass) {
        this.name = klass.getName();
    }
    
    @Override
    public Counter getCounter(final Class<?> klass) {
        return new SysoCounter(klass);
    }
    
    @Override
    public void read(final long l) {
        System.out.println(String.format("[%s] %s bytes read", this.name, l));
    }
    
    @Override
    public void written(final long l) {
        System.out.println(String.format("[%s] %s bytes written", this.name, l));
    }
}
