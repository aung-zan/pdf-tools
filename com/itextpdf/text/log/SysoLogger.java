// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.log;

public class SysoLogger implements Logger
{
    private String name;
    private final int shorten;
    
    public SysoLogger() {
        this(1);
    }
    
    public SysoLogger(final int packageReduce) {
        this.shorten = packageReduce;
    }
    
    protected SysoLogger(final String klass, final int shorten) {
        this.shorten = shorten;
        this.name = klass;
    }
    
    @Override
    public Logger getLogger(final Class<?> klass) {
        return new SysoLogger(klass.getName(), this.shorten);
    }
    
    @Override
    public Logger getLogger(final String name) {
        return new SysoLogger("[itext]", 0);
    }
    
    @Override
    public boolean isLogging(final Level level) {
        return true;
    }
    
    @Override
    public void warn(final String message) {
        System.out.println(String.format("%s WARN  %s", this.shorten(this.name), message));
    }
    
    private String shorten(final String className) {
        if (this.shorten != 0) {
            final StringBuilder target = new StringBuilder();
            String name = className;
            for (int fromIndex = className.indexOf(46); fromIndex != -1; fromIndex = name.indexOf(46)) {
                final int parseTo = (fromIndex < this.shorten) ? fromIndex : this.shorten;
                target.append(name.substring(0, parseTo));
                target.append('.');
                name = name.substring(fromIndex + 1);
            }
            target.append(className.substring(className.lastIndexOf(46) + 1));
            return target.toString();
        }
        return className;
    }
    
    @Override
    public void trace(final String message) {
        System.out.println(String.format("%s TRACE %s", this.shorten(this.name), message));
    }
    
    @Override
    public void debug(final String message) {
        System.out.println(String.format("%s DEBUG %s", this.shorten(this.name), message));
    }
    
    @Override
    public void info(final String message) {
        System.out.println(String.format("%s INFO  %s", this.shorten(this.name), message));
    }
    
    @Override
    public void error(final String message) {
        System.out.println(String.format("%s ERROR %s", this.name, message));
    }
    
    @Override
    public void error(final String message, final Exception e) {
        System.out.println(String.format("%s ERROR %s", this.name, message));
        e.printStackTrace(System.out);
    }
}
