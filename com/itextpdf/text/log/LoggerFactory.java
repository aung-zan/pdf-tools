// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.log;

public class LoggerFactory
{
    private static LoggerFactory myself;
    private Logger logger;
    
    public static Logger getLogger(final Class<?> klass) {
        return LoggerFactory.myself.logger.getLogger(klass);
    }
    
    public static Logger getLogger(final String name) {
        return LoggerFactory.myself.logger.getLogger(name);
    }
    
    public static LoggerFactory getInstance() {
        return LoggerFactory.myself;
    }
    
    private LoggerFactory() {
        this.logger = new NoOpLogger();
    }
    
    public void setLogger(final Logger logger) {
        this.logger = logger;
    }
    
    public Logger logger() {
        return this.logger;
    }
    
    static {
        LoggerFactory.myself = new LoggerFactory();
    }
}
