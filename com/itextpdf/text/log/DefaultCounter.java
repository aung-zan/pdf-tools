// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.log;

import com.itextpdf.text.pdf.codec.Base64;
import com.itextpdf.text.Version;

public class DefaultCounter implements Counter
{
    private int count;
    private int level;
    private final int[] repeat;
    private int repeat_level;
    private static byte[] message;
    
    public DefaultCounter() {
        this.count = 0;
        this.level = 0;
        this.repeat = new int[] { 10000, 5000, 1000 };
        this.repeat_level = 10000;
    }
    
    @Override
    public Counter getCounter(final Class<?> klass) {
        return this;
    }
    
    @Override
    public void read(final long l) {
        this.plusOne();
    }
    
    @Override
    public void written(final long l) {
        this.plusOne();
    }
    
    private void plusOne() {
        if (this.count++ > this.repeat_level) {
            if (Version.isAGPLVersion()) {
                ++this.level;
                if (this.level == 1) {
                    this.repeat_level = this.repeat[1];
                }
                else {
                    this.repeat_level = this.repeat[2];
                }
                System.out.println(new String(DefaultCounter.message));
            }
            this.count = 0;
        }
    }
    
    static {
        DefaultCounter.message = Base64.decode("DQoNCllvdSBhcmUgdXNpbmcgaVRleHQgdW5kZXIgdGhlIEFHUEwuDQoNCklmIHRoaXMgaXMgeW91ciBpbnRlbnRpb24sIHlvdSBoYXZlIHB1Ymxpc2hlZCB5b3VyIG93biBzb3VyY2UgY29kZSBhcyBBR1BMIHNvZnR3YXJlIHRvby4NClBsZWFzZSBsZXQgdXMga25vdyB3aGVyZSB0byBmaW5kIHlvdXIgc291cmNlIGNvZGUgYnkgc2VuZGluZyBhIG1haWwgdG8gYWdwbEBpdGV4dHBkZi5jb20NCldlJ2QgYmUgaG9ub3JlZCB0byBhZGQgaXQgdG8gb3VyIGxpc3Qgb2YgQUdQTCBwcm9qZWN0cyBidWlsdCBvbiB0b3Agb2YgaVRleHQgb3IgaVRleHRTaGFycA0KYW5kIHdlJ2xsIGV4cGxhaW4gaG93IHRvIHJlbW92ZSB0aGlzIG1lc3NhZ2UgZnJvbSB5b3VyIGVycm9yIGxvZ3MuDQoNCklmIHRoaXMgd2Fzbid0IHlvdXIgaW50ZW50aW9uLCB5b3UgYXJlIHByb2JhYmx5IHVzaW5nIGlUZXh0IGluIGEgbm9uLWZyZWUgZW52aXJvbm1lbnQuDQpJbiB0aGlzIGNhc2UsIHBsZWFzZSBjb250YWN0IHVzIGJ5IGZpbGxpbmcgb3V0IHRoaXMgZm9ybTogaHR0cDovL2l0ZXh0cGRmLmNvbS9zYWxlcw0KSWYgeW91IGFyZSBhIGN1c3RvbWVyLCB3ZSdsbCBleHBsYWluIGhvdyB0byBpbnN0YWxsIHlvdXIgbGljZW5zZSBrZXkgdG8gYXZvaWQgdGhpcyBtZXNzYWdlLg0KSWYgeW91J3JlIG5vdCBhIGN1c3RvbWVyLCB3ZSdsbCBleHBsYWluIHRoZSBiZW5lZml0cyBvZiBiZWNvbWluZyBhIGN1c3RvbWVyLg0KDQo=");
    }
}
