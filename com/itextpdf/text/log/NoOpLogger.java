// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.log;

public final class NoOpLogger implements Logger
{
    @Override
    public Logger getLogger(final Class<?> name) {
        return this;
    }
    
    @Override
    public void warn(final String message) {
    }
    
    @Override
    public void trace(final String message) {
    }
    
    @Override
    public void debug(final String message) {
    }
    
    @Override
    public void info(final String message) {
    }
    
    @Override
    public void error(final String message, final Exception e) {
    }
    
    @Override
    public boolean isLogging(final Level level) {
        return false;
    }
    
    @Override
    public void error(final String message) {
    }
    
    @Override
    public Logger getLogger(final String name) {
        return this;
    }
}
