// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.log;

public class NoOpCounter implements Counter
{
    @Override
    public Counter getCounter(final Class<?> klass) {
        return this;
    }
    
    @Override
    public void read(final long l) {
    }
    
    @Override
    public void written(final long l) {
    }
}
