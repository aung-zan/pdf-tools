// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.log;

public interface Counter
{
    Counter getCounter(final Class<?> p0);
    
    void read(final long p0);
    
    void written(final long p0);
}
