// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.log;

public interface Logger
{
    Logger getLogger(final Class<?> p0);
    
    Logger getLogger(final String p0);
    
    boolean isLogging(final Level p0);
    
    void warn(final String p0);
    
    void trace(final String p0);
    
    void debug(final String p0);
    
    void info(final String p0);
    
    void error(final String p0);
    
    void error(final String p0, final Exception p1);
}
