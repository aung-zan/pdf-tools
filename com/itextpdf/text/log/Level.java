// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.log;

public enum Level
{
    ERROR, 
    WARN, 
    INFO, 
    DEBUG, 
    TRACE;
}
