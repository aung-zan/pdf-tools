// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.log;

public class CounterFactory
{
    private static CounterFactory myself;
    private Counter counter;
    
    private CounterFactory() {
        this.counter = new DefaultCounter();
    }
    
    public static CounterFactory getInstance() {
        return CounterFactory.myself;
    }
    
    public static Counter getCounter(final Class<?> klass) {
        return CounterFactory.myself.counter.getCounter(klass);
    }
    
    public Counter getCounter() {
        return this.counter;
    }
    
    public void setCounter(final Counter counter) {
        this.counter = counter;
    }
    
    static {
        CounterFactory.myself = new CounterFactory();
    }
}
