// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

public interface LargeElement extends Element
{
    void setComplete(final boolean p0);
    
    boolean isComplete();
    
    void flushContent();
}
