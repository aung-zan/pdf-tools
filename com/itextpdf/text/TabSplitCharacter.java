// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import com.itextpdf.text.pdf.PdfChunk;

public class TabSplitCharacter implements SplitCharacter
{
    public static final SplitCharacter TAB;
    
    @Override
    public boolean isSplitCharacter(final int start, final int current, final int end, final char[] cc, final PdfChunk[] ck) {
        return true;
    }
    
    static {
        TAB = new TabSplitCharacter();
    }
}
