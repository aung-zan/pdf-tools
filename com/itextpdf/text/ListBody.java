// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import com.itextpdf.text.pdf.PdfObject;
import java.util.HashMap;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;

public class ListBody implements IAccessibleElement
{
    protected PdfName role;
    private AccessibleElementId id;
    protected HashMap<PdfName, PdfObject> accessibleAttributes;
    protected ListItem parentItem;
    
    protected ListBody(final ListItem parentItem) {
        this.role = PdfName.LBODY;
        this.id = null;
        this.accessibleAttributes = null;
        this.parentItem = null;
        this.parentItem = parentItem;
    }
    
    @Override
    public PdfObject getAccessibleAttribute(final PdfName key) {
        if (this.accessibleAttributes != null) {
            return this.accessibleAttributes.get(key);
        }
        return null;
    }
    
    @Override
    public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
        if (this.accessibleAttributes == null) {
            this.accessibleAttributes = new HashMap<PdfName, PdfObject>();
        }
        this.accessibleAttributes.put(key, value);
    }
    
    @Override
    public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
        return this.accessibleAttributes;
    }
    
    @Override
    public PdfName getRole() {
        return this.role;
    }
    
    @Override
    public void setRole(final PdfName role) {
        this.role = role;
    }
    
    @Override
    public AccessibleElementId getId() {
        if (this.id == null) {
            this.id = new AccessibleElementId();
        }
        return this.id;
    }
    
    @Override
    public void setId(final AccessibleElementId id) {
        this.id = id;
    }
    
    @Override
    public boolean isInline() {
        return false;
    }
}
