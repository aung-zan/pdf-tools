// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import java.util.EventListener;

public interface ElementListener extends EventListener
{
    boolean add(final Element p0) throws DocumentException;
}
