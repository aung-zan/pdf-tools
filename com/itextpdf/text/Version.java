// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import java.lang.reflect.Method;

public final class Version
{
    public static String AGPL;
    private static Version version;
    private String iText;
    private String release;
    private String iTextVersion;
    private String key;
    
    public Version() {
        this.iText = "iText®";
        this.release = "5.5.13";
        this.iTextVersion = this.iText + " " + this.release + " ©2000-2018 iText Group NV";
        this.key = null;
    }
    
    public static Version getInstance() {
        if (Version.version == null) {
            synchronized (Version.version = new Version()) {
                try {
                    final Class<?> klass = Class.forName("com.itextpdf.licensekey.LicenseKey");
                    if (klass != null) {
                        final Class[] cArg = { String.class };
                        final Method m = klass.getMethod("getLicenseeInfoForVersion", (Class<?>[])cArg);
                        final Object[] args = { Version.version.release };
                        final String[] info = (String[])m.invoke(klass.newInstance(), args);
                        if (info[3] != null && info[3].trim().length() > 0) {
                            Version.version.key = info[3];
                        }
                        else {
                            Version.version.key = "Trial version ";
                            if (info[5] == null) {
                                final StringBuilder sb = new StringBuilder();
                                final Version version = Version.version;
                                version.key = sb.append(version.key).append("unauthorised").toString();
                            }
                            else {
                                final StringBuilder sb2 = new StringBuilder();
                                final Version version2 = Version.version;
                                version2.key = sb2.append(version2.key).append(info[5]).toString();
                            }
                        }
                        if (info[4] != null && info[4].trim().length() > 0) {
                            Version.version.iTextVersion = info[4];
                        }
                        else if (info[2] != null && info[2].trim().length() > 0) {
                            final StringBuilder sb3 = new StringBuilder();
                            final Version version3 = Version.version;
                            version3.iTextVersion = sb3.append(version3.iTextVersion).append(" (").append(info[2]).toString();
                            if (!Version.version.key.toLowerCase().startsWith("trial")) {
                                final StringBuilder sb4 = new StringBuilder();
                                final Version version4 = Version.version;
                                version4.iTextVersion = sb4.append(version4.iTextVersion).append("; licensed version)").toString();
                            }
                            else {
                                final StringBuilder sb5 = new StringBuilder();
                                final Version version5 = Version.version;
                                version5.iTextVersion = sb5.append(version5.iTextVersion).append("; ").append(Version.version.key).append(")").toString();
                            }
                        }
                        else {
                            if (info[0] == null || info[0].trim().length() <= 0) {
                                throw new Exception();
                            }
                            final StringBuilder sb6 = new StringBuilder();
                            final Version version6 = Version.version;
                            version6.iTextVersion = sb6.append(version6.iTextVersion).append(" (").append(info[0]).toString();
                            if (!Version.version.key.toLowerCase().startsWith("trial")) {
                                final StringBuilder sb7 = new StringBuilder();
                                final Version version7 = Version.version;
                                version7.iTextVersion = sb7.append(version7.iTextVersion).append("; licensed version)").toString();
                            }
                            else {
                                final StringBuilder sb8 = new StringBuilder();
                                final Version version8 = Version.version;
                                version8.iTextVersion = sb8.append(version8.iTextVersion).append("; ").append(Version.version.key).append(")").toString();
                            }
                        }
                    }
                }
                catch (Exception e) {
                    final StringBuilder sb9 = new StringBuilder();
                    final Version version9 = Version.version;
                    version9.iTextVersion = sb9.append(version9.iTextVersion).append(Version.AGPL).toString();
                }
            }
        }
        return Version.version;
    }
    
    public String getProduct() {
        return this.iText;
    }
    
    public String getRelease() {
        return this.release;
    }
    
    public String getVersion() {
        return this.iTextVersion;
    }
    
    public String getKey() {
        return this.key;
    }
    
    public static boolean isAGPLVersion() {
        return getInstance().getVersion().indexOf(Version.AGPL) > 0;
    }
    
    static {
        Version.AGPL = " (AGPL-version)";
        Version.version = null;
    }
}
