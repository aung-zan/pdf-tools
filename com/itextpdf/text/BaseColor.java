// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import com.itextpdf.text.error_messages.MessageLocalization;

public class BaseColor
{
    public static final BaseColor WHITE;
    public static final BaseColor LIGHT_GRAY;
    public static final BaseColor GRAY;
    public static final BaseColor DARK_GRAY;
    public static final BaseColor BLACK;
    public static final BaseColor RED;
    public static final BaseColor PINK;
    public static final BaseColor ORANGE;
    public static final BaseColor YELLOW;
    public static final BaseColor GREEN;
    public static final BaseColor MAGENTA;
    public static final BaseColor CYAN;
    public static final BaseColor BLUE;
    private static final double FACTOR = 0.7;
    private int value;
    
    public BaseColor(final int red, final int green, final int blue, final int alpha) {
        this.setValue(red, green, blue, alpha);
    }
    
    public BaseColor(final int red, final int green, final int blue) {
        this(red, green, blue, 255);
    }
    
    public BaseColor(final float red, final float green, final float blue, final float alpha) {
        this((int)(red * 255.0f + 0.5), (int)(green * 255.0f + 0.5), (int)(blue * 255.0f + 0.5), (int)(alpha * 255.0f + 0.5));
    }
    
    public BaseColor(final float red, final float green, final float blue) {
        this(red, green, blue, 1.0f);
    }
    
    public BaseColor(final int argb) {
        this.value = argb;
    }
    
    public int getRGB() {
        return this.value;
    }
    
    public int getRed() {
        return this.getRGB() >> 16 & 0xFF;
    }
    
    public int getGreen() {
        return this.getRGB() >> 8 & 0xFF;
    }
    
    public int getBlue() {
        return this.getRGB() >> 0 & 0xFF;
    }
    
    public int getAlpha() {
        return this.getRGB() >> 24 & 0xFF;
    }
    
    public BaseColor brighter() {
        int r = this.getRed();
        int g = this.getGreen();
        int b = this.getBlue();
        final int i = 3;
        if (r == 0 && g == 0 && b == 0) {
            return new BaseColor(i, i, i);
        }
        if (r > 0 && r < i) {
            r = i;
        }
        if (g > 0 && g < i) {
            g = i;
        }
        if (b > 0 && b < i) {
            b = i;
        }
        return new BaseColor(Math.min((int)(r / 0.7), 255), Math.min((int)(g / 0.7), 255), Math.min((int)(b / 0.7), 255));
    }
    
    public BaseColor darker() {
        return new BaseColor(Math.max((int)(this.getRed() * 0.7), 0), Math.max((int)(this.getGreen() * 0.7), 0), Math.max((int)(this.getBlue() * 0.7), 0));
    }
    
    @Override
    public boolean equals(final Object obj) {
        return obj instanceof BaseColor && ((BaseColor)obj).value == this.value;
    }
    
    @Override
    public int hashCode() {
        return this.value;
    }
    
    protected void setValue(final int red, final int green, final int blue, final int alpha) {
        validate(red);
        validate(green);
        validate(blue);
        validate(alpha);
        this.value = ((alpha & 0xFF) << 24 | (red & 0xFF) << 16 | (green & 0xFF) << 8 | (blue & 0xFF) << 0);
    }
    
    private static void validate(final int value) {
        if (value < 0 || value > 255) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("color.value.outside.range.0.255", new Object[0]));
        }
    }
    
    @Override
    public String toString() {
        return "Color value[" + Integer.toString(this.value, 16) + "]";
    }
    
    static {
        WHITE = new BaseColor(255, 255, 255);
        LIGHT_GRAY = new BaseColor(192, 192, 192);
        GRAY = new BaseColor(128, 128, 128);
        DARK_GRAY = new BaseColor(64, 64, 64);
        BLACK = new BaseColor(0, 0, 0);
        RED = new BaseColor(255, 0, 0);
        PINK = new BaseColor(255, 175, 175);
        ORANGE = new BaseColor(255, 200, 0);
        YELLOW = new BaseColor(255, 255, 0);
        GREEN = new BaseColor(0, 255, 0);
        MAGENTA = new BaseColor(255, 0, 255);
        CYAN = new BaseColor(0, 255, 255);
        BLUE = new BaseColor(0, 0, 255);
    }
}
