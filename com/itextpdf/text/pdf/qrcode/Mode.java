// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.qrcode;

public final class Mode
{
    public static final Mode TERMINATOR;
    public static final Mode NUMERIC;
    public static final Mode ALPHANUMERIC;
    public static final Mode STRUCTURED_APPEND;
    public static final Mode BYTE;
    public static final Mode ECI;
    public static final Mode KANJI;
    public static final Mode FNC1_FIRST_POSITION;
    public static final Mode FNC1_SECOND_POSITION;
    private final int[] characterCountBitsForVersions;
    private final int bits;
    private final String name;
    
    private Mode(final int[] characterCountBitsForVersions, final int bits, final String name) {
        this.characterCountBitsForVersions = characterCountBitsForVersions;
        this.bits = bits;
        this.name = name;
    }
    
    public static Mode forBits(final int bits) {
        switch (bits) {
            case 0: {
                return Mode.TERMINATOR;
            }
            case 1: {
                return Mode.NUMERIC;
            }
            case 2: {
                return Mode.ALPHANUMERIC;
            }
            case 3: {
                return Mode.STRUCTURED_APPEND;
            }
            case 4: {
                return Mode.BYTE;
            }
            case 5: {
                return Mode.FNC1_FIRST_POSITION;
            }
            case 7: {
                return Mode.ECI;
            }
            case 8: {
                return Mode.KANJI;
            }
            case 9: {
                return Mode.FNC1_SECOND_POSITION;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
    
    public int getCharacterCountBits(final Version version) {
        if (this.characterCountBitsForVersions == null) {
            throw new IllegalArgumentException("Character count doesn't apply to this mode");
        }
        final int number = version.getVersionNumber();
        int offset;
        if (number <= 9) {
            offset = 0;
        }
        else if (number <= 26) {
            offset = 1;
        }
        else {
            offset = 2;
        }
        return this.characterCountBitsForVersions[offset];
    }
    
    public int getBits() {
        return this.bits;
    }
    
    public String getName() {
        return this.name;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    static {
        TERMINATOR = new Mode(new int[] { 0, 0, 0 }, 0, "TERMINATOR");
        NUMERIC = new Mode(new int[] { 10, 12, 14 }, 1, "NUMERIC");
        ALPHANUMERIC = new Mode(new int[] { 9, 11, 13 }, 2, "ALPHANUMERIC");
        STRUCTURED_APPEND = new Mode(new int[] { 0, 0, 0 }, 3, "STRUCTURED_APPEND");
        BYTE = new Mode(new int[] { 8, 16, 16 }, 4, "BYTE");
        ECI = new Mode(null, 7, "ECI");
        KANJI = new Mode(new int[] { 8, 10, 12 }, 8, "KANJI");
        FNC1_FIRST_POSITION = new Mode(null, 5, "FNC1_FIRST_POSITION");
        FNC1_SECOND_POSITION = new Mode(null, 9, "FNC1_SECOND_POSITION");
    }
}
