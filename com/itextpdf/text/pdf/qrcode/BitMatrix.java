// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.qrcode;

public final class BitMatrix
{
    public final int width;
    public final int height;
    public final int rowSize;
    public final int[] bits;
    
    public BitMatrix(final int dimension) {
        this(dimension, dimension);
    }
    
    public BitMatrix(final int width, final int height) {
        if (width < 1 || height < 1) {
            throw new IllegalArgumentException("Both dimensions must be greater than 0");
        }
        this.width = width;
        this.height = height;
        int rowSize = width >> 5;
        if ((width & 0x1F) != 0x0) {
            ++rowSize;
        }
        this.rowSize = rowSize;
        this.bits = new int[rowSize * height];
    }
    
    public boolean get(final int x, final int y) {
        final int offset = y * this.rowSize + (x >> 5);
        return (this.bits[offset] >>> (x & 0x1F) & 0x1) != 0x0;
    }
    
    public void set(final int x, final int y) {
        final int offset = y * this.rowSize + (x >> 5);
        final int[] bits = this.bits;
        final int n = offset;
        bits[n] |= 1 << (x & 0x1F);
    }
    
    public void flip(final int x, final int y) {
        final int offset = y * this.rowSize + (x >> 5);
        final int[] bits = this.bits;
        final int n = offset;
        bits[n] ^= 1 << (x & 0x1F);
    }
    
    public void clear() {
        for (int max = this.bits.length, i = 0; i < max; ++i) {
            this.bits[i] = 0;
        }
    }
    
    public void setRegion(final int left, final int top, final int width, final int height) {
        if (top < 0 || left < 0) {
            throw new IllegalArgumentException("Left and top must be nonnegative");
        }
        if (height < 1 || width < 1) {
            throw new IllegalArgumentException("Height and width must be at least 1");
        }
        final int right = left + width;
        final int bottom = top + height;
        if (bottom > this.height || right > this.width) {
            throw new IllegalArgumentException("The region must fit inside the matrix");
        }
        for (int y = top; y < bottom; ++y) {
            final int offset = y * this.rowSize;
            for (int x = left; x < right; ++x) {
                final int[] bits = this.bits;
                final int n = offset + (x >> 5);
                bits[n] |= 1 << (x & 0x1F);
            }
        }
    }
    
    public BitArray getRow(final int y, BitArray row) {
        if (row == null || row.getSize() < this.width) {
            row = new BitArray(this.width);
        }
        final int offset = y * this.rowSize;
        for (int x = 0; x < this.rowSize; ++x) {
            row.setBulk(x << 5, this.bits[offset + x]);
        }
        return row;
    }
    
    public int getWidth() {
        return this.width;
    }
    
    public int getHeight() {
        return this.height;
    }
    
    public int getDimension() {
        if (this.width != this.height) {
            throw new RuntimeException("Can't call getDimension() on a non-square matrix");
        }
        return this.width;
    }
    
    @Override
    public String toString() {
        final StringBuffer result = new StringBuffer(this.height * (this.width + 1));
        for (int y = 0; y < this.height; ++y) {
            for (int x = 0; x < this.width; ++x) {
                result.append(this.get(x, y) ? "X " : "  ");
            }
            result.append('\n');
        }
        return result.toString();
    }
}
