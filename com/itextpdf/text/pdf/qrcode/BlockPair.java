// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.qrcode;

final class BlockPair
{
    private final ByteArray dataBytes;
    private final ByteArray errorCorrectionBytes;
    
    BlockPair(final ByteArray data, final ByteArray errorCorrection) {
        this.dataBytes = data;
        this.errorCorrectionBytes = errorCorrection;
    }
    
    public ByteArray getDataBytes() {
        return this.dataBytes;
    }
    
    public ByteArray getErrorCorrectionBytes() {
        return this.errorCorrectionBytes;
    }
}
