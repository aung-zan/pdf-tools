// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.qrcode;

public final class EncodeHintType
{
    public static final EncodeHintType ERROR_CORRECTION;
    public static final EncodeHintType CHARACTER_SET;
    
    private EncodeHintType() {
    }
    
    static {
        ERROR_CORRECTION = new EncodeHintType();
        CHARACTER_SET = new EncodeHintType();
    }
}
