// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.qrcode;

public final class ErrorCorrectionLevel
{
    public static final ErrorCorrectionLevel L;
    public static final ErrorCorrectionLevel M;
    public static final ErrorCorrectionLevel Q;
    public static final ErrorCorrectionLevel H;
    private static final ErrorCorrectionLevel[] FOR_BITS;
    private final int ordinal;
    private final int bits;
    private final String name;
    
    private ErrorCorrectionLevel(final int ordinal, final int bits, final String name) {
        this.ordinal = ordinal;
        this.bits = bits;
        this.name = name;
    }
    
    public int ordinal() {
        return this.ordinal;
    }
    
    public int getBits() {
        return this.bits;
    }
    
    public String getName() {
        return this.name;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    public static ErrorCorrectionLevel forBits(final int bits) {
        if (bits < 0 || bits >= ErrorCorrectionLevel.FOR_BITS.length) {
            throw new IllegalArgumentException();
        }
        return ErrorCorrectionLevel.FOR_BITS[bits];
    }
    
    static {
        L = new ErrorCorrectionLevel(0, 1, "L");
        M = new ErrorCorrectionLevel(1, 0, "M");
        Q = new ErrorCorrectionLevel(2, 3, "Q");
        H = new ErrorCorrectionLevel(3, 2, "H");
        FOR_BITS = new ErrorCorrectionLevel[] { ErrorCorrectionLevel.M, ErrorCorrectionLevel.L, ErrorCorrectionLevel.H, ErrorCorrectionLevel.Q };
    }
}
