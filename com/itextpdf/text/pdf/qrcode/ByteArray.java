// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.qrcode;

public final class ByteArray
{
    private static final int INITIAL_SIZE = 32;
    private byte[] bytes;
    private int size;
    
    public ByteArray() {
        this.bytes = null;
        this.size = 0;
    }
    
    public ByteArray(final int size) {
        this.bytes = new byte[size];
        this.size = size;
    }
    
    public ByteArray(final byte[] byteArray) {
        this.bytes = byteArray;
        this.size = this.bytes.length;
    }
    
    public int at(final int index) {
        return this.bytes[index] & 0xFF;
    }
    
    public void set(final int index, final int value) {
        this.bytes[index] = (byte)value;
    }
    
    public int size() {
        return this.size;
    }
    
    public boolean isEmpty() {
        return this.size == 0;
    }
    
    public void appendByte(final int value) {
        if (this.size == 0 || this.size >= this.bytes.length) {
            final int newSize = Math.max(32, this.size << 1);
            this.reserve(newSize);
        }
        this.bytes[this.size] = (byte)value;
        ++this.size;
    }
    
    public void reserve(final int capacity) {
        if (this.bytes == null || this.bytes.length < capacity) {
            final byte[] newArray = new byte[capacity];
            if (this.bytes != null) {
                System.arraycopy(this.bytes, 0, newArray, 0, this.bytes.length);
            }
            this.bytes = newArray;
        }
    }
    
    public void set(final byte[] source, final int offset, final int count) {
        this.bytes = new byte[count];
        this.size = count;
        for (int x = 0; x < count; ++x) {
            this.bytes[x] = source[offset + x];
        }
    }
}
