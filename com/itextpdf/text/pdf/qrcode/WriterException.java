// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.qrcode;

public final class WriterException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public WriterException() {
    }
    
    public WriterException(final String message) {
        super(message);
    }
}
