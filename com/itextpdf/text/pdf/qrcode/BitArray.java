// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.qrcode;

public final class BitArray
{
    public int[] bits;
    public final int size;
    
    public BitArray(final int size) {
        if (size < 1) {
            throw new IllegalArgumentException("size must be at least 1");
        }
        this.size = size;
        this.bits = makeArray(size);
    }
    
    public int getSize() {
        return this.size;
    }
    
    public boolean get(final int i) {
        return (this.bits[i >> 5] & 1 << (i & 0x1F)) != 0x0;
    }
    
    public void set(final int i) {
        final int[] bits = this.bits;
        final int n = i >> 5;
        bits[n] |= 1 << (i & 0x1F);
    }
    
    public void flip(final int i) {
        final int[] bits = this.bits;
        final int n = i >> 5;
        bits[n] ^= 1 << (i & 0x1F);
    }
    
    public void setBulk(final int i, final int newBits) {
        this.bits[i >> 5] = newBits;
    }
    
    public void clear() {
        for (int max = this.bits.length, i = 0; i < max; ++i) {
            this.bits[i] = 0;
        }
    }
    
    public boolean isRange(final int start, int end, final boolean value) {
        if (end < start) {
            throw new IllegalArgumentException();
        }
        if (end == start) {
            return true;
        }
        --end;
        final int firstInt = start >> 5;
        for (int lastInt = end >> 5, i = firstInt; i <= lastInt; ++i) {
            final int firstBit = (i > firstInt) ? 0 : (start & 0x1F);
            final int lastBit = (i < lastInt) ? 31 : (end & 0x1F);
            int mask;
            if (firstBit == 0 && lastBit == 31) {
                mask = -1;
            }
            else {
                mask = 0;
                for (int j = firstBit; j <= lastBit; ++j) {
                    mask |= 1 << j;
                }
            }
            if ((this.bits[i] & mask) != (value ? mask : 0)) {
                return false;
            }
        }
        return true;
    }
    
    public int[] getBitArray() {
        return this.bits;
    }
    
    public void reverse() {
        final int[] newBits = new int[this.bits.length];
        for (int size = this.size, i = 0; i < size; ++i) {
            if (this.get(size - i - 1)) {
                final int[] array = newBits;
                final int n = i >> 5;
                array[n] |= 1 << (i & 0x1F);
            }
        }
        this.bits = newBits;
    }
    
    private static int[] makeArray(final int size) {
        int arraySize = size >> 5;
        if ((size & 0x1F) != 0x0) {
            ++arraySize;
        }
        return new int[arraySize];
    }
    
    @Override
    public String toString() {
        final StringBuffer result = new StringBuffer(this.size);
        for (int i = 0; i < this.size; ++i) {
            if ((i & 0x7) == 0x0) {
                result.append(' ');
            }
            result.append(this.get(i) ? 'X' : '.');
        }
        return result.toString();
    }
}
