// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.qrcode;

public final class BitVector
{
    private int sizeInBits;
    private byte[] array;
    private static final int DEFAULT_SIZE_IN_BYTES = 32;
    
    public BitVector() {
        this.sizeInBits = 0;
        this.array = new byte[32];
    }
    
    public int at(final int index) {
        if (index < 0 || index >= this.sizeInBits) {
            throw new IllegalArgumentException("Bad index: " + index);
        }
        final int value = this.array[index >> 3] & 0xFF;
        return value >> 7 - (index & 0x7) & 0x1;
    }
    
    public int size() {
        return this.sizeInBits;
    }
    
    public int sizeInBytes() {
        return this.sizeInBits + 7 >> 3;
    }
    
    public void appendBit(final int bit) {
        if (bit != 0 && bit != 1) {
            throw new IllegalArgumentException("Bad bit");
        }
        final int numBitsInLastByte = this.sizeInBits & 0x7;
        if (numBitsInLastByte == 0) {
            this.appendByte(0);
            this.sizeInBits -= 8;
        }
        final byte[] array = this.array;
        final int n = this.sizeInBits >> 3;
        array[n] |= (byte)(bit << 7 - numBitsInLastByte);
        ++this.sizeInBits;
    }
    
    public void appendBits(final int value, final int numBits) {
        if (numBits < 0 || numBits > 32) {
            throw new IllegalArgumentException("Num bits must be between 0 and 32");
        }
        int numBitsLeft = numBits;
        while (numBitsLeft > 0) {
            if ((this.sizeInBits & 0x7) == 0x0 && numBitsLeft >= 8) {
                final int newByte = value >> numBitsLeft - 8 & 0xFF;
                this.appendByte(newByte);
                numBitsLeft -= 8;
            }
            else {
                final int bit = value >> numBitsLeft - 1 & 0x1;
                this.appendBit(bit);
                --numBitsLeft;
            }
        }
    }
    
    public void appendBitVector(final BitVector bits) {
        for (int size = bits.size(), i = 0; i < size; ++i) {
            this.appendBit(bits.at(i));
        }
    }
    
    public void xor(final BitVector other) {
        if (this.sizeInBits != other.size()) {
            throw new IllegalArgumentException("BitVector sizes don't match");
        }
        for (int sizeInBytes = this.sizeInBits + 7 >> 3, i = 0; i < sizeInBytes; ++i) {
            final byte[] array = this.array;
            final int n = i;
            array[n] ^= other.array[i];
        }
    }
    
    @Override
    public String toString() {
        final StringBuffer result = new StringBuffer(this.sizeInBits);
        for (int i = 0; i < this.sizeInBits; ++i) {
            if (this.at(i) == 0) {
                result.append('0');
            }
            else {
                if (this.at(i) != 1) {
                    throw new IllegalArgumentException("Byte isn't 0 or 1");
                }
                result.append('1');
            }
        }
        return result.toString();
    }
    
    public byte[] getArray() {
        return this.array;
    }
    
    private void appendByte(final int value) {
        if (this.sizeInBits >> 3 == this.array.length) {
            final byte[] newArray = new byte[this.array.length << 1];
            System.arraycopy(this.array, 0, newArray, 0, this.array.length);
            this.array = newArray;
        }
        this.array[this.sizeInBits >> 3] = (byte)value;
        this.sizeInBits += 8;
    }
}
