// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.qrcode;

import java.util.Map;

public final class QRCodeWriter
{
    private static final int QUIET_ZONE_SIZE = 4;
    
    public ByteMatrix encode(final String contents, final int width, final int height) throws WriterException {
        return this.encode(contents, width, height, null);
    }
    
    public ByteMatrix encode(final String contents, final int width, final int height, final Map<EncodeHintType, Object> hints) throws WriterException {
        if (contents == null || contents.length() == 0) {
            throw new IllegalArgumentException("Found empty contents");
        }
        if (width < 0 || height < 0) {
            throw new IllegalArgumentException("Requested dimensions are too small: " + width + 'x' + height);
        }
        ErrorCorrectionLevel errorCorrectionLevel = ErrorCorrectionLevel.L;
        if (hints != null) {
            final ErrorCorrectionLevel requestedECLevel = hints.get(EncodeHintType.ERROR_CORRECTION);
            if (requestedECLevel != null) {
                errorCorrectionLevel = requestedECLevel;
            }
        }
        final QRCode code = new QRCode();
        Encoder.encode(contents, errorCorrectionLevel, hints, code);
        return renderResult(code, width, height);
    }
    
    private static ByteMatrix renderResult(final QRCode code, final int width, final int height) {
        final ByteMatrix input = code.getMatrix();
        final int inputWidth = input.getWidth();
        final int inputHeight = input.getHeight();
        final int qrWidth = inputWidth + 8;
        final int qrHeight = inputHeight + 8;
        final int outputWidth = Math.max(width, qrWidth);
        final int outputHeight = Math.max(height, qrHeight);
        final int multiple = Math.min(outputWidth / qrWidth, outputHeight / qrHeight);
        final int leftPadding = (outputWidth - inputWidth * multiple) / 2;
        final int topPadding = (outputHeight - inputHeight * multiple) / 2;
        final ByteMatrix output = new ByteMatrix(outputWidth, outputHeight);
        final byte[][] outputArray = output.getArray();
        final byte[] row = new byte[outputWidth];
        for (int y = 0; y < topPadding; ++y) {
            setRowColor(outputArray[y], (byte)(-1));
        }
        final byte[][] inputArray = input.getArray();
        for (int y2 = 0; y2 < inputHeight; ++y2) {
            for (int x = 0; x < leftPadding; ++x) {
                row[x] = -1;
            }
            int offset = leftPadding;
            for (int x2 = 0; x2 < inputWidth; ++x2) {
                final byte value = (byte)((inputArray[y2][x2] == 1) ? 0 : -1);
                for (int z = 0; z < multiple; ++z) {
                    row[offset + z] = value;
                }
                offset += multiple;
            }
            int x2;
            for (offset = (x2 = leftPadding + inputWidth * multiple); x2 < outputWidth; ++x2) {
                row[x2] = -1;
            }
            offset = topPadding + y2 * multiple;
            for (int z2 = 0; z2 < multiple; ++z2) {
                System.arraycopy(row, 0, outputArray[offset + z2], 0, outputWidth);
            }
        }
        int y3;
        for (int offset2 = y3 = topPadding + inputHeight * multiple; y3 < outputHeight; ++y3) {
            setRowColor(outputArray[y3], (byte)(-1));
        }
        return output;
    }
    
    private static void setRowColor(final byte[] row, final byte value) {
        for (int x = 0; x < row.length; ++x) {
            row[x] = value;
        }
    }
}
