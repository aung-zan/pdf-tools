// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.io.IOException;
import com.itextpdf.text.DocumentException;
import java.io.OutputStream;
import com.itextpdf.text.Document;

public class PdfConcatenate
{
    protected Document document;
    protected PdfCopy copy;
    
    public PdfConcatenate(final OutputStream os) throws DocumentException {
        this(os, false);
    }
    
    public PdfConcatenate(final OutputStream os, final boolean smart) throws DocumentException {
        this.document = new Document();
        if (smart) {
            this.copy = new PdfSmartCopy(this.document, os);
        }
        else {
            this.copy = new PdfCopy(this.document, os);
        }
    }
    
    public int addPages(final PdfReader reader) throws DocumentException, IOException {
        this.open();
        final int n = reader.getNumberOfPages();
        for (int i = 1; i <= n; ++i) {
            this.copy.addPage(this.copy.getImportedPage(reader, i));
        }
        this.copy.freeReader(reader);
        reader.close();
        return n;
    }
    
    public PdfCopy getWriter() {
        return this.copy;
    }
    
    public void open() {
        if (!this.document.isOpen()) {
            this.document.open();
        }
    }
    
    public void close() {
        this.document.close();
    }
}
