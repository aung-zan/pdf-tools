// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public class RefKey
{
    int num;
    int gen;
    
    RefKey(final int num, final int gen) {
        this.num = num;
        this.gen = gen;
    }
    
    public RefKey(final PdfIndirectReference ref) {
        this.num = ref.getNumber();
        this.gen = ref.getGeneration();
    }
    
    RefKey(final PRIndirectReference ref) {
        this.num = ref.getNumber();
        this.gen = ref.getGeneration();
    }
    
    @Override
    public int hashCode() {
        return (this.gen << 16) + this.num;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof RefKey)) {
            return false;
        }
        final RefKey other = (RefKey)o;
        return this.gen == other.gen && this.num == other.num;
    }
    
    @Override
    public String toString() {
        return Integer.toString(this.num) + ' ' + this.gen;
    }
}
