// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import java.io.OutputStream;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Node;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.TransformerFactory;
import java.io.ByteArrayOutputStream;
import org.w3c.dom.Document;
import java.io.IOException;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.security.XmlLocator;

public class XfaXmlLocator implements XmlLocator
{
    private PdfStamper stamper;
    private XfaForm xfaForm;
    private String encoding;
    
    public XfaXmlLocator(final PdfStamper stamper) throws DocumentException, IOException {
        this.stamper = stamper;
        try {
            this.createXfaForm();
        }
        catch (ParserConfigurationException e) {
            throw new DocumentException(e);
        }
        catch (SAXException e2) {
            throw new DocumentException(e2);
        }
    }
    
    protected void createXfaForm() throws ParserConfigurationException, SAXException, IOException {
        this.xfaForm = new XfaForm(this.stamper.getReader());
    }
    
    @Override
    public Document getDocument() {
        return this.xfaForm.getDomDocument();
    }
    
    @Override
    public void setDocument(final Document document) throws IOException, DocumentException {
        try {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            final TransformerFactory tf = TransformerFactory.newInstance();
            try {
                tf.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
            }
            catch (Exception ex) {}
            final Transformer trans = tf.newTransformer();
            trans.transform(new DOMSource(document), new StreamResult(outputStream));
            final PdfIndirectReference iref = this.stamper.getWriter().addToBody(new PdfStream(outputStream.toByteArray())).getIndirectReference();
            this.stamper.getReader().getAcroForm().put(PdfName.XFA, iref);
        }
        catch (TransformerConfigurationException e) {
            throw new DocumentException(e);
        }
        catch (TransformerException e2) {
            throw new DocumentException(e2);
        }
    }
    
    @Override
    public String getEncoding() {
        return this.encoding;
    }
    
    public void setEncoding(final String encoding) {
        this.encoding = encoding;
    }
}
