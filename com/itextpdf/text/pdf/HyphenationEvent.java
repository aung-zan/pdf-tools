// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public interface HyphenationEvent
{
    String getHyphenSymbol();
    
    String getHyphenatedWordPre(final String p0, final BaseFont p1, final float p2, final float p3);
    
    String getHyphenatedWordPost();
}
