// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.Map;
import java.io.IOException;
import java.util.Iterator;
import java.util.HashMap;
import com.itextpdf.text.pdf.interfaces.IPdfStructureElement;

public class PdfStructureTreeRoot extends PdfDictionary implements IPdfStructureElement
{
    private HashMap<Integer, PdfObject> parentTree;
    private PdfIndirectReference reference;
    private PdfDictionary classMap;
    protected HashMap<PdfName, PdfObject> classes;
    private HashMap<Integer, PdfIndirectReference> numTree;
    private HashMap<String, PdfObject> idTreeMap;
    private PdfWriter writer;
    
    PdfStructureTreeRoot(final PdfWriter writer) {
        super(PdfName.STRUCTTREEROOT);
        this.parentTree = new HashMap<Integer, PdfObject>();
        this.classMap = null;
        this.classes = null;
        this.numTree = null;
        this.writer = writer;
        this.reference = writer.getPdfIndirectReference();
    }
    
    private void createNumTree() throws IOException {
        if (this.numTree != null) {
            return;
        }
        this.numTree = new HashMap<Integer, PdfIndirectReference>();
        for (final Integer i : this.parentTree.keySet()) {
            final PdfObject obj = this.parentTree.get(i);
            if (obj.isArray()) {
                final PdfArray ar = (PdfArray)obj;
                this.numTree.put(i, this.writer.addToBody(ar).getIndirectReference());
            }
            else {
                if (!(obj instanceof PdfIndirectReference)) {
                    continue;
                }
                this.numTree.put(i, (PdfIndirectReference)obj);
            }
        }
    }
    
    public void mapRole(final PdfName used, final PdfName standard) {
        PdfDictionary rm = (PdfDictionary)this.get(PdfName.ROLEMAP);
        if (rm == null) {
            rm = new PdfDictionary();
            this.put(PdfName.ROLEMAP, rm);
        }
        rm.put(used, standard);
    }
    
    public void mapClass(final PdfName name, final PdfObject object) {
        if (this.classMap == null) {
            this.classMap = new PdfDictionary();
            this.classes = new HashMap<PdfName, PdfObject>();
        }
        this.classes.put(name, object);
    }
    
    void putIDTree(final String record, final PdfObject reference) {
        if (this.idTreeMap == null) {
            this.idTreeMap = new HashMap<String, PdfObject>();
        }
        this.idTreeMap.put(record, reference);
    }
    
    public PdfObject getMappedClass(final PdfName name) {
        if (this.classes == null) {
            return null;
        }
        return this.classes.get(name);
    }
    
    public PdfWriter getWriter() {
        return this.writer;
    }
    
    public HashMap<Integer, PdfIndirectReference> getNumTree() throws IOException {
        if (this.numTree == null) {
            this.createNumTree();
        }
        return this.numTree;
    }
    
    public PdfIndirectReference getReference() {
        return this.reference;
    }
    
    void setPageMark(final int page, final PdfIndirectReference struc) {
        final Integer i = page;
        PdfArray ar = this.parentTree.get(i);
        if (ar == null) {
            ar = new PdfArray();
            this.parentTree.put(i, ar);
        }
        ar.add(struc);
    }
    
    void setAnnotationMark(final int structParentIndex, final PdfIndirectReference struc) {
        this.parentTree.put(structParentIndex, struc);
    }
    
    void buildTree() throws IOException {
        this.createNumTree();
        final PdfDictionary dicTree = PdfNumberTree.writeTree(this.numTree, this.writer);
        if (dicTree != null) {
            this.put(PdfName.PARENTTREE, this.writer.addToBody(dicTree).getIndirectReference());
        }
        if (this.classMap != null && !this.classes.isEmpty()) {
            for (final Map.Entry<PdfName, PdfObject> entry : this.classes.entrySet()) {
                final PdfObject value = entry.getValue();
                if (value.isDictionary()) {
                    this.classMap.put(entry.getKey(), this.writer.addToBody(value).getIndirectReference());
                }
                else {
                    if (!value.isArray()) {
                        continue;
                    }
                    final PdfArray newArray = new PdfArray();
                    final PdfArray array = (PdfArray)value;
                    for (int i = 0; i < array.size(); ++i) {
                        if (array.getPdfObject(i).isDictionary()) {
                            newArray.add(this.writer.addToBody(array.getAsDict(i)).getIndirectReference());
                        }
                    }
                    this.classMap.put(entry.getKey(), newArray);
                }
            }
            this.put(PdfName.CLASSMAP, this.writer.addToBody(this.classMap).getIndirectReference());
        }
        if (this.idTreeMap != null && !this.idTreeMap.isEmpty()) {
            final PdfDictionary dic = PdfNameTree.writeTree(this.idTreeMap, this.writer);
            this.put(PdfName.IDTREE, dic);
        }
        this.writer.addToBody(this, this.reference);
    }
    
    @Override
    public PdfObject getAttribute(final PdfName name) {
        final PdfDictionary attr = this.getAsDict(PdfName.A);
        if (attr != null && attr.contains(name)) {
            return attr.get(name);
        }
        return null;
    }
    
    @Override
    public void setAttribute(final PdfName name, final PdfObject obj) {
        PdfDictionary attr = this.getAsDict(PdfName.A);
        if (attr == null) {
            attr = new PdfDictionary();
            this.put(PdfName.A, attr);
        }
        attr.put(name, obj);
    }
}
