// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.io.IOException;
import java.io.OutputStream;

public class PRIndirectReference extends PdfIndirectReference
{
    protected PdfReader reader;
    
    public PRIndirectReference(final PdfReader reader, final int number, final int generation) {
        this.type = 10;
        this.number = number;
        this.generation = generation;
        this.reader = reader;
    }
    
    public PRIndirectReference(final PdfReader reader, final int number) {
        this(reader, number, 0);
    }
    
    @Override
    public void toPdf(final PdfWriter writer, final OutputStream os) throws IOException {
        if (writer != null) {
            final int n = writer.getNewObjectNumber(this.reader, this.number, this.generation);
            os.write(PdfEncodings.convertToBytes(new StringBuffer().append(n).append(" ").append(this.reader.isAppendable() ? this.generation : 0).append(" R").toString(), null));
        }
        else {
            super.toPdf(null, os);
        }
    }
    
    public PdfReader getReader() {
        return this.reader;
    }
    
    public void setNumber(final int number, final int generation) {
        this.number = number;
        this.generation = generation;
    }
}
