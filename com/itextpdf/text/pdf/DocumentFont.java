// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.Utilities;
import java.io.IOException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.fonts.cmaps.CidLocation;
import com.itextpdf.text.pdf.fonts.cmaps.AbstractCMap;
import com.itextpdf.text.pdf.fonts.cmaps.CMapParserEx;
import com.itextpdf.text.pdf.fonts.cmaps.CidLocationFromByte;
import java.util.Map;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.pdf.fonts.cmaps.CMapToUnicode;
import java.util.Iterator;
import java.util.HashMap;

public class DocumentFont extends BaseFont
{
    private HashMap<Integer, int[]> metrics;
    private String fontName;
    private PRIndirectReference refFont;
    private PdfDictionary font;
    private IntHashtable uni2byte;
    private IntHashtable byte2uni;
    private IntHashtable diffmap;
    private float ascender;
    private float capHeight;
    private float descender;
    private float italicAngle;
    private float fontWeight;
    private float llx;
    private float lly;
    private float urx;
    private float ury;
    protected boolean isType0;
    protected int defaultWidth;
    private IntHashtable hMetrics;
    protected String cjkEncoding;
    protected String uniMap;
    private BaseFont cjkMirror;
    private static final int[] stdEnc;
    
    DocumentFont(final PdfDictionary font) {
        this.metrics = new HashMap<Integer, int[]>();
        this.uni2byte = new IntHashtable();
        this.byte2uni = new IntHashtable();
        this.ascender = 800.0f;
        this.capHeight = 700.0f;
        this.descender = -200.0f;
        this.italicAngle = 0.0f;
        this.fontWeight = 0.0f;
        this.llx = -50.0f;
        this.lly = -200.0f;
        this.urx = 100.0f;
        this.ury = 900.0f;
        this.isType0 = false;
        this.defaultWidth = 1000;
        this.refFont = null;
        this.font = font;
        this.init();
    }
    
    DocumentFont(final PRIndirectReference refFont) {
        this.metrics = new HashMap<Integer, int[]>();
        this.uni2byte = new IntHashtable();
        this.byte2uni = new IntHashtable();
        this.ascender = 800.0f;
        this.capHeight = 700.0f;
        this.descender = -200.0f;
        this.italicAngle = 0.0f;
        this.fontWeight = 0.0f;
        this.llx = -50.0f;
        this.lly = -200.0f;
        this.urx = 100.0f;
        this.ury = 900.0f;
        this.isType0 = false;
        this.defaultWidth = 1000;
        this.refFont = refFont;
        this.font = (PdfDictionary)PdfReader.getPdfObject(refFont);
        this.init();
    }
    
    DocumentFont(final PRIndirectReference refFont, final PdfDictionary drEncoding) {
        this.metrics = new HashMap<Integer, int[]>();
        this.uni2byte = new IntHashtable();
        this.byte2uni = new IntHashtable();
        this.ascender = 800.0f;
        this.capHeight = 700.0f;
        this.descender = -200.0f;
        this.italicAngle = 0.0f;
        this.fontWeight = 0.0f;
        this.llx = -50.0f;
        this.lly = -200.0f;
        this.urx = 100.0f;
        this.ury = 900.0f;
        this.isType0 = false;
        this.defaultWidth = 1000;
        this.refFont = refFont;
        this.font = (PdfDictionary)PdfReader.getPdfObject(refFont);
        if (this.font.get(PdfName.ENCODING) == null && drEncoding != null) {
            for (final PdfName key : drEncoding.getKeys()) {
                this.font.put(PdfName.ENCODING, drEncoding.get(key));
            }
        }
        this.init();
    }
    
    public PdfDictionary getFontDictionary() {
        return this.font;
    }
    
    private void init() {
        this.encoding = "";
        this.fontSpecific = false;
        this.fontType = 4;
        final PdfName baseFont = this.font.getAsName(PdfName.BASEFONT);
        this.fontName = ((baseFont != null) ? PdfName.decodeName(baseFont.toString()) : "Unspecified Font Name");
        final PdfName subType = this.font.getAsName(PdfName.SUBTYPE);
        if (PdfName.TYPE1.equals(subType) || PdfName.TRUETYPE.equals(subType)) {
            this.doType1TT();
        }
        else if (PdfName.TYPE3.equals(subType)) {
            this.fillEncoding(null);
            this.fillDiffMap(this.font.getAsDict(PdfName.ENCODING), null);
            this.fillWidths();
        }
        else {
            final PdfName encodingName = this.font.getAsName(PdfName.ENCODING);
            if (encodingName != null) {
                final String enc = PdfName.decodeName(encodingName.toString());
                final String ffontname = CJKFont.GetCompatibleFont(enc);
                if (ffontname != null) {
                    try {
                        this.cjkMirror = BaseFont.createFont(ffontname, enc, false);
                    }
                    catch (Exception e) {
                        throw new ExceptionConverter(e);
                    }
                    this.cjkEncoding = enc;
                    this.uniMap = ((CJKFont)this.cjkMirror).getUniMap();
                }
                if (PdfName.TYPE0.equals(subType)) {
                    this.isType0 = true;
                    if (!enc.equals("Identity-H") && this.cjkMirror != null) {
                        final PdfArray df = (PdfArray)PdfReader.getPdfObjectRelease(this.font.get(PdfName.DESCENDANTFONTS));
                        final PdfDictionary cidft = (PdfDictionary)PdfReader.getPdfObjectRelease(df.getPdfObject(0));
                        final PdfNumber dwo = (PdfNumber)PdfReader.getPdfObjectRelease(cidft.get(PdfName.DW));
                        if (dwo != null) {
                            this.defaultWidth = dwo.intValue();
                        }
                        this.hMetrics = this.readWidths((PdfArray)PdfReader.getPdfObjectRelease(cidft.get(PdfName.W)));
                        final PdfDictionary fontDesc = (PdfDictionary)PdfReader.getPdfObjectRelease(cidft.get(PdfName.FONTDESCRIPTOR));
                        this.fillFontDesc(fontDesc);
                    }
                    else {
                        this.processType0(this.font);
                    }
                }
            }
        }
    }
    
    private void processType0(final PdfDictionary font) {
        try {
            final PdfObject toUniObject = PdfReader.getPdfObjectRelease(font.get(PdfName.TOUNICODE));
            final PdfArray df = (PdfArray)PdfReader.getPdfObjectRelease(font.get(PdfName.DESCENDANTFONTS));
            final PdfDictionary cidft = (PdfDictionary)PdfReader.getPdfObjectRelease(df.getPdfObject(0));
            final PdfNumber dwo = (PdfNumber)PdfReader.getPdfObjectRelease(cidft.get(PdfName.DW));
            int dw = 1000;
            if (dwo != null) {
                dw = dwo.intValue();
            }
            final IntHashtable widths = this.readWidths((PdfArray)PdfReader.getPdfObjectRelease(cidft.get(PdfName.W)));
            final PdfDictionary fontDesc = (PdfDictionary)PdfReader.getPdfObjectRelease(cidft.get(PdfName.FONTDESCRIPTOR));
            this.fillFontDesc(fontDesc);
            if (toUniObject instanceof PRStream) {
                this.fillMetrics(PdfReader.getStreamBytes((PRStream)toUniObject), widths, dw);
            }
            else if (new PdfName("Identity-H").equals(toUniObject)) {
                this.fillMetricsIdentity(widths, dw);
            }
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    private IntHashtable readWidths(final PdfArray ws) {
        final IntHashtable hh = new IntHashtable();
        if (ws == null) {
            return hh;
        }
        for (int k = 0; k < ws.size(); ++k) {
            int c1 = ((PdfNumber)PdfReader.getPdfObjectRelease(ws.getPdfObject(k))).intValue();
            final PdfObject obj = PdfReader.getPdfObjectRelease(ws.getPdfObject(++k));
            if (obj.isArray()) {
                final PdfArray a2 = (PdfArray)obj;
                for (int j = 0; j < a2.size(); ++j) {
                    final int c2 = ((PdfNumber)PdfReader.getPdfObjectRelease(a2.getPdfObject(j))).intValue();
                    hh.put(c1++, c2);
                }
            }
            else {
                final int c3 = ((PdfNumber)obj).intValue();
                final int w = ((PdfNumber)PdfReader.getPdfObjectRelease(ws.getPdfObject(++k))).intValue();
                while (c1 <= c3) {
                    hh.put(c1, w);
                    ++c1;
                }
            }
        }
        return hh;
    }
    
    private String decodeString(final PdfString ps) {
        if (ps.isHexWriting()) {
            return PdfEncodings.convertToString(ps.getBytes(), "UnicodeBigUnmarked");
        }
        return ps.toUnicodeString();
    }
    
    private void fillMetricsIdentity(final IntHashtable widths, final int dw) {
        for (int i = 0; i < 65536; ++i) {
            int w = dw;
            if (widths.containsKey(i)) {
                w = widths.get(i);
            }
            this.metrics.put(i, new int[] { i, w });
        }
    }
    
    private void fillMetrics(final byte[] touni, final IntHashtable widths, final int dw) {
        try {
            final PdfContentParser ps = new PdfContentParser(new PRTokeniser(new RandomAccessFileOrArray(new RandomAccessSourceFactory().createSource(touni))));
            PdfObject ob = null;
            boolean notFound = true;
            int nestLevel = 0;
            int maxExc = 50;
            while (true) {
                if (!notFound) {
                    if (nestLevel <= 0) {
                        break;
                    }
                }
                try {
                    ob = ps.readPRObject();
                }
                catch (Exception ex) {
                    if (--maxExc < 0) {
                        break;
                    }
                    continue;
                }
                if (ob == null) {
                    break;
                }
                if (ob.type() != 200) {
                    continue;
                }
                if (ob.toString().equals("begin")) {
                    notFound = false;
                    ++nestLevel;
                }
                else if (ob.toString().equals("end")) {
                    --nestLevel;
                }
                else if (ob.toString().equals("beginbfchar")) {
                    while (true) {
                        final PdfObject nx = ps.readPRObject();
                        if (nx.toString().equals("endbfchar")) {
                            break;
                        }
                        final String cid = this.decodeString((PdfString)nx);
                        final String uni = this.decodeString((PdfString)ps.readPRObject());
                        if (uni.length() != 1) {
                            continue;
                        }
                        final int cidc = cid.charAt(0);
                        final int unic = uni.charAt(uni.length() - 1);
                        int w = dw;
                        if (widths.containsKey(cidc)) {
                            w = widths.get(cidc);
                        }
                        this.metrics.put(unic, new int[] { cidc, w });
                    }
                }
                else {
                    if (!ob.toString().equals("beginbfrange")) {
                        continue;
                    }
                    while (true) {
                        final PdfObject nx = ps.readPRObject();
                        if (nx.toString().equals("endbfrange")) {
                            break;
                        }
                        final String cid2 = this.decodeString((PdfString)nx);
                        final String cid3 = this.decodeString((PdfString)ps.readPRObject());
                        int cid1c = cid2.charAt(0);
                        final int cid2c = cid3.charAt(0);
                        final PdfObject ob2 = ps.readPRObject();
                        if (ob2.isString()) {
                            final String uni2 = this.decodeString((PdfString)ob2);
                            if (uni2.length() != 1) {
                                continue;
                            }
                            for (int unic2 = uni2.charAt(uni2.length() - 1); cid1c <= cid2c; ++cid1c, ++unic2) {
                                int w2 = dw;
                                if (widths.containsKey(cid1c)) {
                                    w2 = widths.get(cid1c);
                                }
                                this.metrics.put(unic2, new int[] { cid1c, w2 });
                            }
                        }
                        else {
                            final PdfArray a = (PdfArray)ob2;
                            for (int j = 0; j < a.size(); ++j, ++cid1c) {
                                final String uni3 = this.decodeString(a.getAsString(j));
                                if (uni3.length() == 1) {
                                    final int unic3 = uni3.charAt(uni3.length() - 1);
                                    int w3 = dw;
                                    if (widths.containsKey(cid1c)) {
                                        w3 = widths.get(cid1c);
                                    }
                                    this.metrics.put(unic3, new int[] { cid1c, w3 });
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    private void doType1TT() {
        CMapToUnicode toUnicode = null;
        PdfObject enc = PdfReader.getPdfObject(this.font.get(PdfName.ENCODING));
        if (enc == null) {
            final PdfName baseFont = this.font.getAsName(PdfName.BASEFONT);
            if (DocumentFont.BuiltinFonts14.containsKey(this.fontName) && (PdfName.SYMBOL.equals(baseFont) || PdfName.ZAPFDINGBATS.equals(baseFont))) {
                this.fillEncoding(baseFont);
            }
            else {
                this.fillEncoding(null);
            }
            try {
                toUnicode = this.processToUnicode();
                if (toUnicode != null) {
                    final Map<Integer, Integer> rm = toUnicode.createReverseMapping();
                    for (final Map.Entry<Integer, Integer> kv : rm.entrySet()) {
                        this.uni2byte.put(kv.getKey(), kv.getValue());
                        this.byte2uni.put(kv.getValue(), kv.getKey());
                    }
                }
            }
            catch (Exception ex) {
                throw new ExceptionConverter(ex);
            }
        }
        else if (enc.isName()) {
            this.fillEncoding((PdfName)enc);
        }
        else if (enc.isDictionary()) {
            final PdfDictionary encDic = (PdfDictionary)enc;
            enc = PdfReader.getPdfObject(encDic.get(PdfName.BASEENCODING));
            if (enc == null) {
                this.fillEncoding(null);
            }
            else {
                this.fillEncoding((PdfName)enc);
            }
            this.fillDiffMap(encDic, toUnicode);
        }
        if (DocumentFont.BuiltinFonts14.containsKey(this.fontName)) {
            BaseFont bf;
            try {
                bf = BaseFont.createFont(this.fontName, "Cp1252", false);
            }
            catch (Exception e) {
                throw new ExceptionConverter(e);
            }
            int[] e2 = this.uni2byte.toOrderedKeys();
            for (int k = 0; k < e2.length; ++k) {
                final int n = this.uni2byte.get(e2[k]);
                this.widths[n] = bf.getRawWidth(n, GlyphList.unicodeToName(e2[k]));
            }
            if (this.diffmap != null) {
                e2 = this.diffmap.toOrderedKeys();
                for (int k = 0; k < e2.length; ++k) {
                    final int n = this.diffmap.get(e2[k]);
                    this.widths[n] = bf.getRawWidth(n, GlyphList.unicodeToName(e2[k]));
                }
                this.diffmap = null;
            }
            this.ascender = bf.getFontDescriptor(1, 1000.0f);
            this.capHeight = bf.getFontDescriptor(2, 1000.0f);
            this.descender = bf.getFontDescriptor(3, 1000.0f);
            this.italicAngle = bf.getFontDescriptor(4, 1000.0f);
            this.fontWeight = bf.getFontDescriptor(23, 1000.0f);
            this.llx = bf.getFontDescriptor(5, 1000.0f);
            this.lly = bf.getFontDescriptor(6, 1000.0f);
            this.urx = bf.getFontDescriptor(7, 1000.0f);
            this.ury = bf.getFontDescriptor(8, 1000.0f);
        }
        this.fillWidths();
        this.fillFontDesc(this.font.getAsDict(PdfName.FONTDESCRIPTOR));
    }
    
    private void fillWidths() {
        final PdfArray newWidths = this.font.getAsArray(PdfName.WIDTHS);
        final PdfNumber first = this.font.getAsNumber(PdfName.FIRSTCHAR);
        final PdfNumber last = this.font.getAsNumber(PdfName.LASTCHAR);
        if (first != null && last != null && newWidths != null) {
            final int f = first.intValue();
            final int nSize = f + newWidths.size();
            if (this.widths.length < nSize) {
                final int[] tmp = new int[nSize];
                System.arraycopy(this.widths, 0, tmp, 0, f);
                this.widths = tmp;
            }
            for (int k = 0; k < newWidths.size(); ++k) {
                this.widths[f + k] = newWidths.getAsNumber(k).intValue();
            }
        }
    }
    
    private void fillDiffMap(final PdfDictionary encDic, CMapToUnicode toUnicode) {
        final PdfArray diffs = encDic.getAsArray(PdfName.DIFFERENCES);
        if (diffs != null) {
            this.diffmap = new IntHashtable();
            int currentNumber = 0;
            for (int k = 0; k < diffs.size(); ++k) {
                final PdfObject obj = diffs.getPdfObject(k);
                if (obj.isNumber()) {
                    currentNumber = ((PdfNumber)obj).intValue();
                }
                else {
                    final int[] c = GlyphList.nameToUnicode(PdfName.decodeName(((PdfName)obj).toString()));
                    if (c != null && c.length > 0) {
                        this.uni2byte.put(c[0], currentNumber);
                        this.byte2uni.put(currentNumber, c[0]);
                        this.diffmap.put(c[0], currentNumber);
                    }
                    else {
                        if (toUnicode == null) {
                            toUnicode = this.processToUnicode();
                            if (toUnicode == null) {
                                toUnicode = new CMapToUnicode();
                            }
                        }
                        final String unicode = toUnicode.lookup(new byte[] { (byte)currentNumber }, 0, 1);
                        if (unicode != null && unicode.length() == 1) {
                            this.uni2byte.put(unicode.charAt(0), currentNumber);
                            this.byte2uni.put(currentNumber, unicode.charAt(0));
                            this.diffmap.put(unicode.charAt(0), currentNumber);
                        }
                    }
                    ++currentNumber;
                }
            }
        }
    }
    
    private CMapToUnicode processToUnicode() {
        CMapToUnicode cmapRet = null;
        final PdfObject toUni = PdfReader.getPdfObjectRelease(this.font.get(PdfName.TOUNICODE));
        if (toUni instanceof PRStream) {
            try {
                final byte[] touni = PdfReader.getStreamBytes((PRStream)toUni);
                final CidLocationFromByte lb = new CidLocationFromByte(touni);
                cmapRet = new CMapToUnicode();
                CMapParserEx.parseCid("", cmapRet, lb);
            }
            catch (Exception e) {
                cmapRet = null;
            }
        }
        return cmapRet;
    }
    
    private void fillFontDesc(final PdfDictionary fontDesc) {
        if (fontDesc == null) {
            return;
        }
        PdfNumber v = fontDesc.getAsNumber(PdfName.ASCENT);
        if (v != null) {
            this.ascender = v.floatValue();
        }
        v = fontDesc.getAsNumber(PdfName.CAPHEIGHT);
        if (v != null) {
            this.capHeight = v.floatValue();
        }
        v = fontDesc.getAsNumber(PdfName.DESCENT);
        if (v != null) {
            this.descender = v.floatValue();
        }
        v = fontDesc.getAsNumber(PdfName.ITALICANGLE);
        if (v != null) {
            this.italicAngle = v.floatValue();
        }
        v = fontDesc.getAsNumber(PdfName.FONTWEIGHT);
        if (v != null) {
            this.fontWeight = v.floatValue();
        }
        final PdfArray bbox = fontDesc.getAsArray(PdfName.FONTBBOX);
        if (bbox != null) {
            this.llx = bbox.getAsNumber(0).floatValue();
            this.lly = bbox.getAsNumber(1).floatValue();
            this.urx = bbox.getAsNumber(2).floatValue();
            this.ury = bbox.getAsNumber(3).floatValue();
            if (this.llx > this.urx) {
                final float t = this.llx;
                this.llx = this.urx;
                this.urx = t;
            }
            if (this.lly > this.ury) {
                final float t = this.lly;
                this.lly = this.ury;
                this.ury = t;
            }
        }
        final float maxAscent = Math.max(this.ury, this.ascender);
        final float minDescent = Math.min(this.lly, this.descender);
        this.ascender = maxAscent * 1000.0f / (maxAscent - minDescent);
        this.descender = minDescent * 1000.0f / (maxAscent - minDescent);
    }
    
    private void fillEncoding(final PdfName encoding) {
        if (encoding == null && this.isSymbolic()) {
            for (int k = 0; k < 256; ++k) {
                this.uni2byte.put(k, k);
                this.byte2uni.put(k, k);
            }
        }
        else if (PdfName.MAC_ROMAN_ENCODING.equals(encoding) || PdfName.WIN_ANSI_ENCODING.equals(encoding) || PdfName.SYMBOL.equals(encoding) || PdfName.ZAPFDINGBATS.equals(encoding)) {
            final byte[] b = new byte[256];
            for (int i = 0; i < 256; ++i) {
                b[i] = (byte)i;
            }
            String enc = "Cp1252";
            if (PdfName.MAC_ROMAN_ENCODING.equals(encoding)) {
                enc = "MacRoman";
            }
            else if (PdfName.SYMBOL.equals(encoding)) {
                enc = "Symbol";
            }
            else if (PdfName.ZAPFDINGBATS.equals(encoding)) {
                enc = "ZapfDingbats";
            }
            final String cv = PdfEncodings.convertToString(b, enc);
            final char[] arr = cv.toCharArray();
            for (int j = 0; j < 256; ++j) {
                this.uni2byte.put(arr[j], j);
                this.byte2uni.put(j, arr[j]);
            }
            this.encoding = enc;
        }
        else {
            for (int k = 0; k < 256; ++k) {
                this.uni2byte.put(DocumentFont.stdEnc[k], k);
                this.byte2uni.put(k, DocumentFont.stdEnc[k]);
            }
        }
    }
    
    @Override
    public String[][] getFamilyFontName() {
        return this.getFullFontName();
    }
    
    @Override
    public float getFontDescriptor(final int key, final float fontSize) {
        if (this.cjkMirror != null) {
            return this.cjkMirror.getFontDescriptor(key, fontSize);
        }
        switch (key) {
            case 1:
            case 9: {
                return this.ascender * fontSize / 1000.0f;
            }
            case 2: {
                return this.capHeight * fontSize / 1000.0f;
            }
            case 3:
            case 10: {
                return this.descender * fontSize / 1000.0f;
            }
            case 4: {
                return this.italicAngle;
            }
            case 5: {
                return this.llx * fontSize / 1000.0f;
            }
            case 6: {
                return this.lly * fontSize / 1000.0f;
            }
            case 7: {
                return this.urx * fontSize / 1000.0f;
            }
            case 8: {
                return this.ury * fontSize / 1000.0f;
            }
            case 11: {
                return 0.0f;
            }
            case 12: {
                return (this.urx - this.llx) * fontSize / 1000.0f;
            }
            case 23: {
                return this.fontWeight * fontSize / 1000.0f;
            }
            default: {
                return 0.0f;
            }
        }
    }
    
    @Override
    public String[][] getFullFontName() {
        return new String[][] { { "", "", "", this.fontName } };
    }
    
    @Override
    public String[][] getAllNameEntries() {
        return new String[][] { { "4", "", "", "", this.fontName } };
    }
    
    @Override
    public int getKerning(final int char1, final int char2) {
        return 0;
    }
    
    @Override
    public String getPostscriptFontName() {
        return this.fontName;
    }
    
    @Override
    int getRawWidth(final int c, final String name) {
        return 0;
    }
    
    @Override
    public boolean hasKernPairs() {
        return false;
    }
    
    @Override
    void writeFont(final PdfWriter writer, final PdfIndirectReference ref, final Object[] params) throws DocumentException, IOException {
    }
    
    public PdfStream getFullFontStream() {
        return null;
    }
    
    @Override
    public int getWidth(final int char1) {
        if (this.isType0) {
            if (this.hMetrics != null && this.cjkMirror != null && !this.cjkMirror.isVertical()) {
                final int c = this.cjkMirror.getCidCode(char1);
                final int v = this.hMetrics.get(c);
                if (v > 0) {
                    return v;
                }
                return this.defaultWidth;
            }
            else {
                final int[] ws = this.metrics.get(char1);
                if (ws != null) {
                    return ws[1];
                }
                return 0;
            }
        }
        else {
            if (this.cjkMirror != null) {
                return this.cjkMirror.getWidth(char1);
            }
            return super.getWidth(char1);
        }
    }
    
    @Override
    public int getWidth(final String text) {
        if (this.isType0) {
            int total = 0;
            if (this.hMetrics != null && this.cjkMirror != null && !this.cjkMirror.isVertical()) {
                if (((CJKFont)this.cjkMirror).isIdentity()) {
                    for (int k = 0; k < text.length(); ++k) {
                        total += this.getWidth(text.charAt(k));
                    }
                }
                else {
                    for (int k = 0; k < text.length(); ++k) {
                        int val;
                        if (Utilities.isSurrogatePair(text, k)) {
                            val = Utilities.convertToUtf32(text, k);
                            ++k;
                        }
                        else {
                            val = text.charAt(k);
                        }
                        total += this.getWidth(val);
                    }
                }
            }
            else {
                final char[] chars = text.toCharArray();
                for (int len = chars.length, i = 0; i < len; ++i) {
                    final int[] ws = this.metrics.get((int)chars[i]);
                    if (ws != null) {
                        total += ws[1];
                    }
                }
            }
            return total;
        }
        if (this.cjkMirror != null) {
            return this.cjkMirror.getWidth(text);
        }
        return super.getWidth(text);
    }
    
    @Override
    public byte[] convertToBytes(final String text) {
        if (this.cjkMirror != null) {
            return this.cjkMirror.convertToBytes(text);
        }
        if (this.isType0) {
            final char[] chars = text.toCharArray();
            final int len = chars.length;
            final byte[] b = new byte[len * 2];
            int bptr = 0;
            for (int k = 0; k < len; ++k) {
                final int[] ws = this.metrics.get((int)chars[k]);
                if (ws != null) {
                    final int g = ws[0];
                    b[bptr++] = (byte)(g / 256);
                    b[bptr++] = (byte)g;
                }
            }
            if (bptr == b.length) {
                return b;
            }
            final byte[] nb = new byte[bptr];
            System.arraycopy(b, 0, nb, 0, bptr);
            return nb;
        }
        else {
            final char[] cc = text.toCharArray();
            final byte[] b2 = new byte[cc.length];
            int ptr = 0;
            for (int i = 0; i < cc.length; ++i) {
                if (this.uni2byte.containsKey(cc[i])) {
                    b2[ptr++] = (byte)this.uni2byte.get(cc[i]);
                }
            }
            if (ptr == b2.length) {
                return b2;
            }
            final byte[] b3 = new byte[ptr];
            System.arraycopy(b2, 0, b3, 0, ptr);
            return b3;
        }
    }
    
    @Override
    byte[] convertToBytes(final int char1) {
        if (this.cjkMirror != null) {
            return this.cjkMirror.convertToBytes(char1);
        }
        if (this.isType0) {
            final int[] ws = this.metrics.get(char1);
            if (ws != null) {
                final int g = ws[0];
                return new byte[] { (byte)(g / 256), (byte)g };
            }
            return new byte[0];
        }
        else {
            if (this.uni2byte.containsKey(char1)) {
                return new byte[] { (byte)this.uni2byte.get(char1) };
            }
            return new byte[0];
        }
    }
    
    PdfIndirectReference getIndirectReference() {
        if (this.refFont == null) {
            throw new IllegalArgumentException("Font reuse not allowed with direct font objects.");
        }
        return this.refFont;
    }
    
    @Override
    public boolean charExists(final int c) {
        if (this.cjkMirror != null) {
            return this.cjkMirror.charExists(c);
        }
        if (this.isType0) {
            return this.metrics.containsKey(c);
        }
        return super.charExists(c);
    }
    
    @Override
    public double[] getFontMatrix() {
        if (this.font.getAsArray(PdfName.FONTMATRIX) != null) {
            return this.font.getAsArray(PdfName.FONTMATRIX).asDoubleArray();
        }
        return DocumentFont.DEFAULT_FONT_MATRIX;
    }
    
    @Override
    public void setPostscriptFontName(final String name) {
    }
    
    @Override
    public boolean setKerning(final int char1, final int char2, final int kern) {
        return false;
    }
    
    @Override
    public int[] getCharBBox(final int c) {
        return null;
    }
    
    @Override
    protected int[] getRawCharBBox(final int c, final String name) {
        return null;
    }
    
    @Override
    public boolean isVertical() {
        if (this.cjkMirror != null) {
            return this.cjkMirror.isVertical();
        }
        return super.isVertical();
    }
    
    IntHashtable getUni2Byte() {
        return this.uni2byte;
    }
    
    IntHashtable getByte2Uni() {
        return this.byte2uni;
    }
    
    IntHashtable getDiffmap() {
        return this.diffmap;
    }
    
    boolean isSymbolic() {
        final PdfDictionary fontDescriptor = this.font.getAsDict(PdfName.FONTDESCRIPTOR);
        if (fontDescriptor == null) {
            return false;
        }
        final PdfNumber flags = fontDescriptor.getAsNumber(PdfName.FLAGS);
        return flags != null && (flags.intValue() & 0x4) != 0x0;
    }
    
    static {
        stdEnc = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32, 33, 34, 35, 36, 37, 38, 8217, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 8216, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 161, 162, 163, 8260, 165, 402, 167, 164, 39, 8220, 171, 8249, 8250, 64257, 64258, 0, 8211, 8224, 8225, 183, 0, 182, 8226, 8218, 8222, 8221, 187, 8230, 8240, 0, 191, 0, 96, 180, 710, 732, 175, 728, 729, 168, 0, 730, 184, 0, 733, 731, 711, 8212, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 198, 0, 170, 0, 0, 0, 0, 321, 216, 338, 186, 0, 0, 0, 0, 0, 230, 0, 0, 0, 305, 0, 0, 322, 248, 339, 223, 0, 0, 0, 0 };
    }
}
