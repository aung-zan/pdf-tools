// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.security.AccessController;
import java.lang.reflect.Method;
import java.security.PrivilegedAction;
import java.nio.ByteBuffer;
import java.nio.BufferUnderflowException;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class MappedRandomAccessFile
{
    private static final int BUFSIZE = 1073741824;
    private FileChannel channel;
    private MappedByteBuffer[] mappedBuffers;
    private long size;
    private long pos;
    
    public MappedRandomAccessFile(final String filename, final String mode) throws FileNotFoundException, IOException {
        this.channel = null;
        if (mode.equals("rw")) {
            this.init(new RandomAccessFile(filename, mode).getChannel(), FileChannel.MapMode.READ_WRITE);
        }
        else {
            this.init(new FileInputStream(filename).getChannel(), FileChannel.MapMode.READ_ONLY);
        }
    }
    
    private void init(final FileChannel channel, final FileChannel.MapMode mapMode) throws IOException {
        this.channel = channel;
        this.size = channel.size();
        this.pos = 0L;
        final int requiredBuffers = (int)(this.size / 1073741824L) + ((this.size % 1073741824L != 0L) ? 1 : 0);
        this.mappedBuffers = new MappedByteBuffer[requiredBuffers];
        try {
            int index = 0;
            for (long offset = 0L; offset < this.size; offset += 1073741824L) {
                final long size2 = Math.min(this.size - offset, 1073741824L);
                (this.mappedBuffers[index] = channel.map(mapMode, offset, size2)).load();
                ++index;
            }
            if (index != requiredBuffers) {
                throw new Error("Should never happen - " + index + " != " + requiredBuffers);
            }
        }
        catch (IOException e) {
            this.close();
            throw e;
        }
        catch (RuntimeException e2) {
            this.close();
            throw e2;
        }
    }
    
    public FileChannel getChannel() {
        return this.channel;
    }
    
    public int read() {
        try {
            final int mapN = (int)(this.pos / 1073741824L);
            final int offN = (int)(this.pos % 1073741824L);
            if (mapN >= this.mappedBuffers.length) {
                return -1;
            }
            if (offN >= this.mappedBuffers[mapN].limit()) {
                return -1;
            }
            final byte b = this.mappedBuffers[mapN].get(offN);
            ++this.pos;
            final int n = b & 0xFF;
            return n;
        }
        catch (BufferUnderflowException e) {
            return -1;
        }
    }
    
    public int read(final byte[] bytes, int off, final int len) {
        int mapN = (int)(this.pos / 1073741824L);
        int offN = (int)(this.pos % 1073741824L);
        int totalRead;
        int bytesFromThisBuffer;
        for (totalRead = 0; totalRead < len; totalRead += bytesFromThisBuffer, ++mapN, offN = 0) {
            if (mapN >= this.mappedBuffers.length) {
                break;
            }
            final MappedByteBuffer currentBuffer = this.mappedBuffers[mapN];
            if (offN > currentBuffer.limit()) {
                break;
            }
            currentBuffer.position(offN);
            bytesFromThisBuffer = Math.min(len - totalRead, currentBuffer.remaining());
            currentBuffer.get(bytes, off, bytesFromThisBuffer);
            off += bytesFromThisBuffer;
            this.pos += bytesFromThisBuffer;
        }
        return (totalRead == 0) ? -1 : totalRead;
    }
    
    public long getFilePointer() {
        return this.pos;
    }
    
    public void seek(final long pos) {
        this.pos = pos;
    }
    
    public long length() {
        return this.size;
    }
    
    public void close() throws IOException {
        for (int i = 0; i < this.mappedBuffers.length; ++i) {
            if (this.mappedBuffers[i] != null) {
                clean(this.mappedBuffers[i]);
                this.mappedBuffers[i] = null;
            }
        }
        if (this.channel != null) {
            this.channel.close();
        }
        this.channel = null;
    }
    
    @Override
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();
    }
    
    public static boolean clean(final ByteBuffer buffer) {
        if (buffer == null || !buffer.isDirect()) {
            return false;
        }
        final Boolean b = AccessController.doPrivileged((PrivilegedAction<Boolean>)new PrivilegedAction<Boolean>() {
            @Override
            public Boolean run() {
                Boolean success = Boolean.FALSE;
                try {
                    final Method getCleanerMethod = buffer.getClass().getMethod("cleaner", (Class<?>[])null);
                    getCleanerMethod.setAccessible(true);
                    final Object cleaner = getCleanerMethod.invoke(buffer, (Object[])null);
                    final Method clean = cleaner.getClass().getMethod("clean", (Class<?>[])null);
                    clean.invoke(cleaner, (Object[])null);
                    success = Boolean.TRUE;
                }
                catch (Exception ex) {}
                return success;
            }
        });
        return b;
    }
}
