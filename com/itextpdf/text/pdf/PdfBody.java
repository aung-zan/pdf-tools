// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.Chunk;
import java.util.List;
import com.itextpdf.text.ElementListener;
import com.itextpdf.text.Element;
import com.itextpdf.text.Rectangle;

public class PdfBody extends Rectangle implements Element
{
    public PdfBody(final Rectangle rectangle) {
        super(rectangle);
    }
    
    @Override
    public boolean process(final ElementListener listener) {
        return false;
    }
    
    @Override
    public int type() {
        return 38;
    }
    
    @Override
    public boolean isContent() {
        return false;
    }
    
    @Override
    public boolean isNestable() {
        return false;
    }
    
    @Override
    public List<Chunk> getChunks() {
        return null;
    }
}
