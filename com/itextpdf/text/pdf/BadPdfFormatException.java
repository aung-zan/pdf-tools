// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public class BadPdfFormatException extends PdfException
{
    private static final long serialVersionUID = 1802317735708833538L;
    
    BadPdfFormatException() {
    }
    
    BadPdfFormatException(final String message) {
        super(message);
    }
}
