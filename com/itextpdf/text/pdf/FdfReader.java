// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.log.CounterFactory;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import com.itextpdf.text.log.Counter;
import java.util.HashMap;

public class FdfReader extends PdfReader
{
    HashMap<String, PdfDictionary> fields;
    String fileSpec;
    PdfName encoding;
    protected static Counter COUNTER;
    
    public FdfReader(final String filename) throws IOException {
        super(filename);
    }
    
    public FdfReader(final byte[] pdfIn) throws IOException {
        super(pdfIn);
    }
    
    public FdfReader(final URL url) throws IOException {
        super(url);
    }
    
    public FdfReader(final InputStream is) throws IOException {
        super(is);
    }
    
    @Override
    protected Counter getCounter() {
        return FdfReader.COUNTER;
    }
    
    @Override
    protected void readPdf() throws IOException {
        this.fields = new HashMap<String, PdfDictionary>();
        this.tokens.checkFdfHeader();
        this.rebuildXref();
        this.readDocObj();
        this.readFields();
    }
    
    protected void kidNode(final PdfDictionary merged, String name) {
        final PdfArray kids = merged.getAsArray(PdfName.KIDS);
        if (kids == null || kids.isEmpty()) {
            if (name.length() > 0) {
                name = name.substring(1);
            }
            this.fields.put(name, merged);
        }
        else {
            merged.remove(PdfName.KIDS);
            for (int k = 0; k < kids.size(); ++k) {
                final PdfDictionary dic = new PdfDictionary();
                dic.merge(merged);
                final PdfDictionary newDic = kids.getAsDict(k);
                final PdfString t = newDic.getAsString(PdfName.T);
                String newName = name;
                if (t != null) {
                    newName = newName + "." + t.toUnicodeString();
                }
                dic.merge(newDic);
                dic.remove(PdfName.T);
                this.kidNode(dic, newName);
            }
        }
    }
    
    protected void readFields() {
        this.catalog = this.trailer.getAsDict(PdfName.ROOT);
        final PdfDictionary fdf = this.catalog.getAsDict(PdfName.FDF);
        if (fdf == null) {
            return;
        }
        final PdfString fs = fdf.getAsString(PdfName.F);
        if (fs != null) {
            this.fileSpec = fs.toUnicodeString();
        }
        final PdfArray fld = fdf.getAsArray(PdfName.FIELDS);
        if (fld == null) {
            return;
        }
        this.encoding = fdf.getAsName(PdfName.ENCODING);
        final PdfDictionary merged = new PdfDictionary();
        merged.put(PdfName.KIDS, fld);
        this.kidNode(merged, "");
    }
    
    public HashMap<String, PdfDictionary> getFields() {
        return this.fields;
    }
    
    public PdfDictionary getField(final String name) {
        return this.fields.get(name);
    }
    
    public byte[] getAttachedFile(final String name) throws IOException {
        final PdfDictionary field = this.fields.get(name);
        if (field != null) {
            PdfIndirectReference ir = (PRIndirectReference)field.get(PdfName.V);
            final PdfDictionary filespec = (PdfDictionary)this.getPdfObject(ir.getNumber());
            final PdfDictionary ef = filespec.getAsDict(PdfName.EF);
            ir = (PRIndirectReference)ef.get(PdfName.F);
            final PRStream stream = (PRStream)this.getPdfObject(ir.getNumber());
            return PdfReader.getStreamBytes(stream);
        }
        return new byte[0];
    }
    
    public String getFieldValue(final String name) {
        final PdfDictionary field = this.fields.get(name);
        if (field == null) {
            return null;
        }
        final PdfObject v = PdfReader.getPdfObject(field.get(PdfName.V));
        if (v == null) {
            return null;
        }
        if (v.isName()) {
            return PdfName.decodeName(((PdfName)v).toString());
        }
        if (!v.isString()) {
            return null;
        }
        final PdfString vs = (PdfString)v;
        if (this.encoding == null || vs.getEncoding() != null) {
            return vs.toUnicodeString();
        }
        final byte[] b = vs.getBytes();
        if (b.length >= 2 && b[0] == -2 && b[1] == -1) {
            return vs.toUnicodeString();
        }
        try {
            if (this.encoding.equals(PdfName.SHIFT_JIS)) {
                return new String(b, "SJIS");
            }
            if (this.encoding.equals(PdfName.UHC)) {
                return new String(b, "MS949");
            }
            if (this.encoding.equals(PdfName.GBK)) {
                return new String(b, "GBK");
            }
            if (this.encoding.equals(PdfName.BIGFIVE)) {
                return new String(b, "Big5");
            }
            if (this.encoding.equals(PdfName.UTF_8)) {
                return new String(b, "UTF8");
            }
        }
        catch (Exception ex) {}
        return vs.toUnicodeString();
    }
    
    public String getFileSpec() {
        return this.fileSpec;
    }
    
    static {
        FdfReader.COUNTER = CounterFactory.getCounter(FdfReader.class);
    }
}
