// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.BaseColor;

public class PdfSpotColor implements ICachedColorSpace, IPdfSpecialColorSpace
{
    public PdfName name;
    public BaseColor altcs;
    public ColorDetails altColorDetails;
    
    public PdfSpotColor(final String name, final BaseColor altcs) {
        this.name = new PdfName(name);
        this.altcs = altcs;
    }
    
    @Override
    public ColorDetails[] getColorantDetails(final PdfWriter writer) {
        if (this.altColorDetails == null && this.altcs instanceof ExtendedColor && ((ExtendedColor)this.altcs).getType() == 7) {
            this.altColorDetails = writer.addSimple(((LabColor)this.altcs).getLabColorSpace());
        }
        return new ColorDetails[] { this.altColorDetails };
    }
    
    public BaseColor getAlternativeCS() {
        return this.altcs;
    }
    
    public PdfName getName() {
        return this.name;
    }
    
    @Deprecated
    protected PdfObject getSpotObject(final PdfWriter writer) {
        return this.getPdfObject(writer);
    }
    
    @Override
    public PdfObject getPdfObject(final PdfWriter writer) {
        final PdfArray array = new PdfArray(PdfName.SEPARATION);
        array.add(this.name);
        PdfFunction func = null;
        if (this.altcs instanceof ExtendedColor) {
            final int type = ((ExtendedColor)this.altcs).type;
            switch (type) {
                case 1: {
                    array.add(PdfName.DEVICEGRAY);
                    func = PdfFunction.type2(writer, new float[] { 0.0f, 1.0f }, null, new float[] { 1.0f }, new float[] { ((GrayColor)this.altcs).getGray() }, 1.0f);
                    break;
                }
                case 2: {
                    array.add(PdfName.DEVICECMYK);
                    final CMYKColor cmyk = (CMYKColor)this.altcs;
                    func = PdfFunction.type2(writer, new float[] { 0.0f, 1.0f }, null, new float[] { 0.0f, 0.0f, 0.0f, 0.0f }, new float[] { cmyk.getCyan(), cmyk.getMagenta(), cmyk.getYellow(), cmyk.getBlack() }, 1.0f);
                    break;
                }
                case 7: {
                    final LabColor lab = (LabColor)this.altcs;
                    if (this.altColorDetails != null) {
                        array.add(this.altColorDetails.getIndirectReference());
                    }
                    else {
                        array.add(lab.getLabColorSpace().getPdfObject(writer));
                    }
                    func = PdfFunction.type2(writer, new float[] { 0.0f, 1.0f }, null, new float[] { 100.0f, 0.0f, 0.0f }, new float[] { lab.getL(), lab.getA(), lab.getB() }, 1.0f);
                    break;
                }
                default: {
                    throw new RuntimeException(MessageLocalization.getComposedMessage("only.rgb.gray.and.cmyk.are.supported.as.alternative.color.spaces", new Object[0]));
                }
            }
        }
        else {
            array.add(PdfName.DEVICERGB);
            func = PdfFunction.type2(writer, new float[] { 0.0f, 1.0f }, null, new float[] { 1.0f, 1.0f, 1.0f }, new float[] { this.altcs.getRed() / 255.0f, this.altcs.getGreen() / 255.0f, this.altcs.getBlue() / 255.0f }, 1.0f);
        }
        array.add(func.getReference());
        return array;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PdfSpotColor)) {
            return false;
        }
        final PdfSpotColor spotColor = (PdfSpotColor)o;
        return this.altcs.equals(spotColor.altcs) && this.name.equals(spotColor.name);
    }
    
    @Override
    public int hashCode() {
        int result = this.name.hashCode();
        result = 31 * result + this.altcs.hashCode();
        return result;
    }
}
