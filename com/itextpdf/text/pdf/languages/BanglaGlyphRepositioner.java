// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.languages;

import java.util.Arrays;
import java.util.List;
import com.itextpdf.text.pdf.Glyph;
import java.util.Map;

public class BanglaGlyphRepositioner extends IndicGlyphRepositioner
{
    private static final String[] CHARCTERS_TO_BE_SHIFTED_LEFT_BY_1;
    private final Map<Integer, int[]> cmap31;
    private final Map<String, Glyph> glyphSubstitutionMap;
    
    public BanglaGlyphRepositioner(final Map<Integer, int[]> cmap31, final Map<String, Glyph> glyphSubstitutionMap) {
        this.cmap31 = cmap31;
        this.glyphSubstitutionMap = glyphSubstitutionMap;
    }
    
    @Override
    public void repositionGlyphs(final List<Glyph> glyphList) {
        for (int i = 0; i < glyphList.size(); ++i) {
            final Glyph glyph = glyphList.get(i);
            if (glyph.chars.equals("\u09cb")) {
                this.handleOKaarAndOUKaar(i, glyphList, '\u09c7', '\u09be');
            }
            else if (glyph.chars.equals("\u09cc")) {
                this.handleOKaarAndOUKaar(i, glyphList, '\u09c7', '\u09d7');
            }
        }
        super.repositionGlyphs(glyphList);
    }
    
    public List<String> getCharactersToBeShiftedLeftByOnePosition() {
        return Arrays.asList(BanglaGlyphRepositioner.CHARCTERS_TO_BE_SHIFTED_LEFT_BY_1);
    }
    
    private void handleOKaarAndOUKaar(final int currentIndex, final List<Glyph> glyphList, final char first, final char second) {
        final Glyph g1 = this.getGlyph(first);
        final Glyph g2 = this.getGlyph(second);
        glyphList.set(currentIndex, g1);
        glyphList.add(currentIndex + 1, g2);
    }
    
    private Glyph getGlyph(final char c) {
        final Glyph glyph = this.glyphSubstitutionMap.get(String.valueOf(c));
        if (glyph != null) {
            return glyph;
        }
        final int[] metrics = this.cmap31.get((int)c);
        final int glyphCode = metrics[0];
        final int glyphWidth = metrics[1];
        return new Glyph(glyphCode, glyphWidth, String.valueOf(c));
    }
    
    static {
        CHARCTERS_TO_BE_SHIFTED_LEFT_BY_1 = new String[] { "\u09bf", "\u09c7", "\u09c8" };
    }
}
