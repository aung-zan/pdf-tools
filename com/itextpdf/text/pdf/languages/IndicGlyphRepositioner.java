// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.languages;

import com.itextpdf.text.pdf.Glyph;
import java.util.List;

abstract class IndicGlyphRepositioner implements GlyphRepositioner
{
    @Override
    public void repositionGlyphs(final List<Glyph> glyphList) {
        for (int i = 0; i < glyphList.size(); ++i) {
            final Glyph glyph = glyphList.get(i);
            final Glyph nextGlyph = this.getNextGlyph(glyphList, i);
            if (nextGlyph != null && this.getCharactersToBeShiftedLeftByOnePosition().contains(nextGlyph.chars)) {
                glyphList.set(i, nextGlyph);
                glyphList.set(i + 1, glyph);
                ++i;
            }
        }
    }
    
    abstract List<String> getCharactersToBeShiftedLeftByOnePosition();
    
    private Glyph getNextGlyph(final List<Glyph> glyphs, final int currentIndex) {
        if (currentIndex + 1 < glyphs.size()) {
            return glyphs.get(currentIndex + 1);
        }
        return null;
    }
}
