// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.languages;

import com.itextpdf.text.pdf.Glyph;
import java.util.List;

public interface GlyphRepositioner
{
    void repositionGlyphs(final List<Glyph> p0);
}
