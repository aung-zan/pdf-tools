// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.languages;

public interface LanguageProcessor
{
    String process(final String p0);
    
    boolean isRTL();
}
