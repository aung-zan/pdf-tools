// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.languages;

public class DevanagariLigaturizer extends IndicLigaturizer
{
    public static final char DEVA_MATRA_AA = '\u093e';
    public static final char DEVA_MATRA_I = '\u093f';
    public static final char DEVA_MATRA_E = '\u0947';
    public static final char DEVA_MATRA_AI = '\u0948';
    public static final char DEVA_MATRA_HLR = '\u0962';
    public static final char DEVA_MATRA_HLRR = '\u0963';
    public static final char DEVA_LETTER_A = '\u0905';
    public static final char DEVA_LETTER_AU = '\u0914';
    public static final char DEVA_LETTER_KA = '\u0915';
    public static final char DEVA_LETTER_HA = '\u0939';
    public static final char DEVA_HALANTA = '\u094d';
    
    public DevanagariLigaturizer() {
        (this.langTable = new char[11])[0] = '\u093e';
        this.langTable[1] = '\u093f';
        this.langTable[2] = '\u0947';
        this.langTable[3] = '\u0948';
        this.langTable[4] = '\u0962';
        this.langTable[5] = '\u0963';
        this.langTable[6] = '\u0905';
        this.langTable[7] = '\u0914';
        this.langTable[8] = '\u0915';
        this.langTable[9] = '\u0939';
        this.langTable[10] = '\u094d';
    }
}
