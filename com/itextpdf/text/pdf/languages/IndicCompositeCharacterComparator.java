// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.languages;

import java.util.Comparator;

public class IndicCompositeCharacterComparator implements Comparator<String>
{
    @Override
    public int compare(final String o1, final String o2) {
        if (o1.length() < o2.length()) {
            return 1;
        }
        if (o1.length() > o2.length()) {
            return -1;
        }
        return o1.compareTo(o2);
    }
}
