// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.languages;

import com.itextpdf.text.pdf.BidiLine;

public class HebrewProcessor implements LanguageProcessor
{
    protected int runDirection;
    
    public HebrewProcessor() {
        this.runDirection = 3;
    }
    
    public HebrewProcessor(final int runDirection) {
        this.runDirection = 3;
        this.runDirection = runDirection;
    }
    
    @Override
    public String process(final String s) {
        return BidiLine.processLTR(s, this.runDirection, 0);
    }
    
    @Override
    public boolean isRTL() {
        return true;
    }
}
