// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.languages;

public class GujaratiLigaturizer extends IndicLigaturizer
{
    public static final char GUJR_MATRA_AA = '\u0abe';
    public static final char GUJR_MATRA_I = '\u0abf';
    public static final char GUJR_MATRA_E = '\u0ac7';
    public static final char GUJR_MATRA_AI = '\u0ac8';
    public static final char GUJR_MATRA_HLR = '\u0ae2';
    public static final char GUJR_MATRA_HLRR = '\u0ae3';
    public static final char GUJR_LETTER_A = '\u0a85';
    public static final char GUJR_LETTER_AU = '\u0a94';
    public static final char GUJR_LETTER_KA = '\u0a95';
    public static final char GUJR_LETTER_HA = '\u0ab9';
    public static final char GUJR_HALANTA = '\u0acd';
    
    public GujaratiLigaturizer() {
        (this.langTable = new char[11])[0] = '\u0abe';
        this.langTable[1] = '\u0abf';
        this.langTable[2] = '\u0ac7';
        this.langTable[3] = '\u0ac8';
        this.langTable[4] = '\u0ae2';
        this.langTable[5] = '\u0ae3';
        this.langTable[6] = '\u0a85';
        this.langTable[7] = '\u0a94';
        this.langTable[8] = '\u0a95';
        this.langTable[9] = '\u0ab9';
        this.langTable[10] = '\u0acd';
    }
}
