// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.ExceptionConverter;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.InputStream;
import java.net.URL;
import com.itextpdf.text.io.IndependentRandomAccessSource;
import java.io.IOException;
import com.itextpdf.text.Document;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import com.itextpdf.text.io.RandomAccessSource;
import java.io.DataInput;

public class RandomAccessFileOrArray implements DataInput
{
    private final RandomAccessSource byteSource;
    private long byteSourcePosition;
    private byte back;
    private boolean isBack;
    
    @Deprecated
    public RandomAccessFileOrArray(final String filename) throws IOException {
        this(new RandomAccessSourceFactory().setForceRead(false).setUsePlainRandomAccess(Document.plainRandomAccess).createBestSource(filename));
    }
    
    @Deprecated
    public RandomAccessFileOrArray(final RandomAccessFileOrArray source) {
        this(new IndependentRandomAccessSource(source.byteSource));
    }
    
    public RandomAccessFileOrArray createView() {
        return new RandomAccessFileOrArray(new IndependentRandomAccessSource(this.byteSource));
    }
    
    public RandomAccessSource createSourceView() {
        return new IndependentRandomAccessSource(this.byteSource);
    }
    
    public RandomAccessFileOrArray(final RandomAccessSource byteSource) {
        this.isBack = false;
        this.byteSource = byteSource;
    }
    
    @Deprecated
    public RandomAccessFileOrArray(final String filename, final boolean forceRead, final boolean plainRandomAccess) throws IOException {
        this(new RandomAccessSourceFactory().setForceRead(forceRead).setUsePlainRandomAccess(plainRandomAccess).createBestSource(filename));
    }
    
    @Deprecated
    public RandomAccessFileOrArray(final URL url) throws IOException {
        this(new RandomAccessSourceFactory().createSource(url));
    }
    
    @Deprecated
    public RandomAccessFileOrArray(final InputStream is) throws IOException {
        this(new RandomAccessSourceFactory().createSource(is));
    }
    
    @Deprecated
    public RandomAccessFileOrArray(final byte[] arrayIn) {
        this(new RandomAccessSourceFactory().createSource(arrayIn));
    }
    
    @Deprecated
    protected RandomAccessSource getByteSource() {
        return this.byteSource;
    }
    
    public void pushBack(final byte b) {
        this.back = b;
        this.isBack = true;
    }
    
    public int read() throws IOException {
        if (this.isBack) {
            this.isBack = false;
            return this.back & 0xFF;
        }
        return this.byteSource.get(this.byteSourcePosition++);
    }
    
    public int read(final byte[] b, int off, int len) throws IOException {
        if (len == 0) {
            return 0;
        }
        int count = 0;
        if (this.isBack && len > 0) {
            this.isBack = false;
            b[off++] = this.back;
            --len;
            ++count;
        }
        if (len > 0) {
            final int byteSourceCount = this.byteSource.get(this.byteSourcePosition, b, off, len);
            if (byteSourceCount > 0) {
                count += byteSourceCount;
                this.byteSourcePosition += byteSourceCount;
            }
        }
        if (count == 0) {
            return -1;
        }
        return count;
    }
    
    public int read(final byte[] b) throws IOException {
        return this.read(b, 0, b.length);
    }
    
    @Override
    public void readFully(final byte[] b) throws IOException {
        this.readFully(b, 0, b.length);
    }
    
    @Override
    public void readFully(final byte[] b, final int off, final int len) throws IOException {
        int n = 0;
        do {
            final int count = this.read(b, off + n, len - n);
            if (count < 0) {
                throw new EOFException();
            }
            n += count;
        } while (n < len);
    }
    
    public long skip(long n) throws IOException {
        if (n <= 0L) {
            return 0L;
        }
        int adj = 0;
        if (this.isBack) {
            this.isBack = false;
            if (n == 1L) {
                return 1L;
            }
            --n;
            adj = 1;
        }
        final long pos = this.getFilePointer();
        final long len = this.length();
        long newpos = pos + n;
        if (newpos > len) {
            newpos = len;
        }
        this.seek(newpos);
        return newpos - pos + adj;
    }
    
    @Override
    public int skipBytes(final int n) throws IOException {
        return (int)this.skip(n);
    }
    
    @Deprecated
    public void reOpen() throws IOException {
        this.seek(0L);
    }
    
    public void close() throws IOException {
        this.isBack = false;
        this.byteSource.close();
    }
    
    public long length() throws IOException {
        return this.byteSource.length();
    }
    
    public void seek(final long pos) throws IOException {
        this.byteSourcePosition = pos;
        this.isBack = false;
    }
    
    public long getFilePointer() throws IOException {
        return this.byteSourcePosition - (this.isBack ? 1 : 0);
    }
    
    @Override
    public boolean readBoolean() throws IOException {
        final int ch = this.read();
        if (ch < 0) {
            throw new EOFException();
        }
        return ch != 0;
    }
    
    @Override
    public byte readByte() throws IOException {
        final int ch = this.read();
        if (ch < 0) {
            throw new EOFException();
        }
        return (byte)ch;
    }
    
    @Override
    public int readUnsignedByte() throws IOException {
        final int ch = this.read();
        if (ch < 0) {
            throw new EOFException();
        }
        return ch;
    }
    
    @Override
    public short readShort() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        if ((ch1 | ch2) < 0) {
            throw new EOFException();
        }
        return (short)((ch1 << 8) + ch2);
    }
    
    public final short readShortLE() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        if ((ch1 | ch2) < 0) {
            throw new EOFException();
        }
        return (short)((ch2 << 8) + (ch1 << 0));
    }
    
    @Override
    public int readUnsignedShort() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        if ((ch1 | ch2) < 0) {
            throw new EOFException();
        }
        return (ch1 << 8) + ch2;
    }
    
    public final int readUnsignedShortLE() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        if ((ch1 | ch2) < 0) {
            throw new EOFException();
        }
        return (ch2 << 8) + (ch1 << 0);
    }
    
    @Override
    public char readChar() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        if ((ch1 | ch2) < 0) {
            throw new EOFException();
        }
        return (char)((ch1 << 8) + ch2);
    }
    
    public final char readCharLE() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        if ((ch1 | ch2) < 0) {
            throw new EOFException();
        }
        return (char)((ch2 << 8) + (ch1 << 0));
    }
    
    @Override
    public int readInt() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        final int ch3 = this.read();
        final int ch4 = this.read();
        if ((ch1 | ch2 | ch3 | ch4) < 0) {
            throw new EOFException();
        }
        return (ch1 << 24) + (ch2 << 16) + (ch3 << 8) + ch4;
    }
    
    public final int readIntLE() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        final int ch3 = this.read();
        final int ch4 = this.read();
        if ((ch1 | ch2 | ch3 | ch4) < 0) {
            throw new EOFException();
        }
        return (ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << 0);
    }
    
    public final long readUnsignedInt() throws IOException {
        final long ch1 = this.read();
        final long ch2 = this.read();
        final long ch3 = this.read();
        final long ch4 = this.read();
        if ((ch1 | ch2 | ch3 | ch4) < 0L) {
            throw new EOFException();
        }
        return (ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4 << 0);
    }
    
    public final long readUnsignedIntLE() throws IOException {
        final long ch1 = this.read();
        final long ch2 = this.read();
        final long ch3 = this.read();
        final long ch4 = this.read();
        if ((ch1 | ch2 | ch3 | ch4) < 0L) {
            throw new EOFException();
        }
        return (ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << 0);
    }
    
    @Override
    public long readLong() throws IOException {
        return ((long)this.readInt() << 32) + ((long)this.readInt() & 0xFFFFFFFFL);
    }
    
    public final long readLongLE() throws IOException {
        final int i1 = this.readIntLE();
        final int i2 = this.readIntLE();
        return ((long)i2 << 32) + ((long)i1 & 0xFFFFFFFFL);
    }
    
    @Override
    public float readFloat() throws IOException {
        return Float.intBitsToFloat(this.readInt());
    }
    
    public final float readFloatLE() throws IOException {
        return Float.intBitsToFloat(this.readIntLE());
    }
    
    @Override
    public double readDouble() throws IOException {
        return Double.longBitsToDouble(this.readLong());
    }
    
    public final double readDoubleLE() throws IOException {
        return Double.longBitsToDouble(this.readLongLE());
    }
    
    @Override
    public String readLine() throws IOException {
        final StringBuilder input = new StringBuilder();
        int c = -1;
        boolean eol = false;
        while (!eol) {
            switch (c = this.read()) {
                case -1:
                case 10: {
                    eol = true;
                    continue;
                }
                case 13: {
                    eol = true;
                    final long cur = this.getFilePointer();
                    if (this.read() != 10) {
                        this.seek(cur);
                        continue;
                    }
                    continue;
                }
                default: {
                    input.append((char)c);
                    continue;
                }
            }
        }
        if (c == -1 && input.length() == 0) {
            return null;
        }
        return input.toString();
    }
    
    @Override
    public String readUTF() throws IOException {
        return DataInputStream.readUTF(this);
    }
    
    public String readString(final int length, final String encoding) throws IOException {
        final byte[] buf = new byte[length];
        this.readFully(buf);
        try {
            return new String(buf, encoding);
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
}
