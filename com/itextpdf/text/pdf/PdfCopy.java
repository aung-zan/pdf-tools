// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.log.CounterFactory;
import com.itextpdf.text.log.LoggerFactory;
import com.itextpdf.text.ExceptionConverter;
import java.util.StringTokenizer;
import java.util.Collection;
import com.itextpdf.text.exceptions.BadPasswordException;
import java.util.List;
import com.itextpdf.text.Rectangle;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.DocListener;
import java.io.OutputStream;
import com.itextpdf.text.Document;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.HashSet;
import java.util.HashMap;
import com.itextpdf.text.log.Counter;
import com.itextpdf.text.log.Logger;

public class PdfCopy extends PdfWriter
{
    private static final Logger LOGGER;
    protected static Counter COUNTER;
    protected HashMap<RefKey, IndirectReferences> indirects;
    protected HashMap<PdfReader, HashMap<RefKey, IndirectReferences>> indirectMap;
    protected HashMap<PdfObject, PdfObject> parentObjects;
    protected HashSet<PdfObject> disableIndirects;
    protected PdfReader reader;
    protected int[] namePtr;
    private boolean rotateContents;
    protected PdfArray fieldArray;
    protected HashSet<PdfTemplate> fieldTemplates;
    private PdfStructTreeController structTreeController;
    private int currentStructArrayNumber;
    protected PRIndirectReference structTreeRootReference;
    protected LinkedHashMap<RefKey, PdfIndirectObject> indirectObjects;
    protected ArrayList<PdfIndirectObject> savedObjects;
    protected ArrayList<ImportedPage> importedPages;
    protected boolean updateRootKids;
    private static final PdfName annotId;
    private static int annotIdCnt;
    protected boolean mergeFields;
    private boolean needAppearances;
    private boolean hasSignature;
    private PdfIndirectReference acroForm;
    private HashMap<PdfArray, ArrayList<Integer>> tabOrder;
    private ArrayList<Object> calculationOrderRefs;
    private PdfDictionary resources;
    protected ArrayList<AcroFields> fields;
    private ArrayList<String> calculationOrder;
    private HashMap<String, Object> fieldTree;
    private HashMap<Integer, PdfIndirectObject> unmergedMap;
    private HashMap<RefKey, PdfIndirectObject> unmergedIndirectRefsMap;
    private HashMap<Integer, PdfIndirectObject> mergedMap;
    private HashSet<PdfIndirectObject> mergedSet;
    private boolean mergeFieldsInternalCall;
    private static final PdfName iTextTag;
    private static final Integer zero;
    private HashSet<Object> mergedRadioButtons;
    private HashMap<Object, PdfString> mergedTextFields;
    private HashSet<PdfReader> readersWithImportedStructureTreeRootKids;
    protected static final HashSet<PdfName> widgetKeys;
    protected static final HashSet<PdfName> fieldKeys;
    
    @Override
    protected Counter getCounter() {
        return PdfCopy.COUNTER;
    }
    
    public PdfCopy(final Document document, final OutputStream os) throws DocumentException {
        super(new PdfDocument(), os);
        this.namePtr = new int[] { 0 };
        this.rotateContents = true;
        this.structTreeController = null;
        this.currentStructArrayNumber = 0;
        this.updateRootKids = false;
        this.mergeFields = false;
        this.needAppearances = false;
        this.mergeFieldsInternalCall = false;
        this.mergedRadioButtons = new HashSet<Object>();
        this.mergedTextFields = new HashMap<Object, PdfString>();
        this.readersWithImportedStructureTreeRootKids = new HashSet<PdfReader>();
        document.addDocListener(this.pdf);
        this.pdf.addWriter(this);
        this.indirectMap = new HashMap<PdfReader, HashMap<RefKey, IndirectReferences>>();
        this.parentObjects = new HashMap<PdfObject, PdfObject>();
        this.disableIndirects = new HashSet<PdfObject>();
        this.indirectObjects = new LinkedHashMap<RefKey, PdfIndirectObject>();
        this.savedObjects = new ArrayList<PdfIndirectObject>();
        this.importedPages = new ArrayList<ImportedPage>();
    }
    
    @Override
    public void setPageEvent(final PdfPageEvent event) {
        throw new UnsupportedOperationException();
    }
    
    public boolean isRotateContents() {
        return this.rotateContents;
    }
    
    public void setRotateContents(final boolean rotateContents) {
        this.rotateContents = rotateContents;
    }
    
    public void setMergeFields() {
        this.mergeFields = true;
        this.resources = new PdfDictionary();
        this.fields = new ArrayList<AcroFields>();
        this.calculationOrder = new ArrayList<String>();
        this.fieldTree = new LinkedHashMap<String, Object>();
        this.unmergedMap = new HashMap<Integer, PdfIndirectObject>();
        this.unmergedIndirectRefsMap = new HashMap<RefKey, PdfIndirectObject>();
        this.mergedMap = new HashMap<Integer, PdfIndirectObject>();
        this.mergedSet = new HashSet<PdfIndirectObject>();
    }
    
    @Override
    public PdfImportedPage getImportedPage(final PdfReader reader, final int pageNumber) {
        if (this.mergeFields && !this.mergeFieldsInternalCall) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("1.method.cannot.be.used.in.mergeFields.mode.please.use.addDocument", "getImportedPage"));
        }
        if (this.mergeFields) {
            final ImportedPage newPage = new ImportedPage(reader, pageNumber, this.mergeFields);
            this.importedPages.add(newPage);
        }
        if (this.structTreeController != null) {
            this.structTreeController.reader = null;
        }
        this.disableIndirects.clear();
        this.parentObjects.clear();
        return this.getImportedPageImpl(reader, pageNumber);
    }
    
    public PdfImportedPage getImportedPage(final PdfReader reader, final int pageNumber, final boolean keepTaggedPdfStructure) throws BadPdfFormatException {
        if (this.mergeFields && !this.mergeFieldsInternalCall) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("1.method.cannot.be.used.in.mergeFields.mode.please.use.addDocument", "getImportedPage"));
        }
        this.updateRootKids = false;
        if (!keepTaggedPdfStructure) {
            if (this.mergeFields) {
                final ImportedPage newPage = new ImportedPage(reader, pageNumber, this.mergeFields);
                this.importedPages.add(newPage);
            }
            return this.getImportedPageImpl(reader, pageNumber);
        }
        if (this.structTreeController != null) {
            if (reader != this.structTreeController.reader) {
                this.structTreeController.setReader(reader);
            }
        }
        else {
            this.structTreeController = new PdfStructTreeController(reader, this);
        }
        final ImportedPage newPage = new ImportedPage(reader, pageNumber, this.mergeFields);
        switch (this.checkStructureTreeRootKids(newPage)) {
            case -1: {
                this.clearIndirects(reader);
                this.updateRootKids = true;
                break;
            }
            case 0: {
                this.updateRootKids = false;
                break;
            }
            case 1: {
                this.updateRootKids = true;
                break;
            }
        }
        this.importedPages.add(newPage);
        this.disableIndirects.clear();
        this.parentObjects.clear();
        return this.getImportedPageImpl(reader, pageNumber);
    }
    
    private void clearIndirects(final PdfReader reader) {
        final HashMap<RefKey, IndirectReferences> currIndirects = this.indirectMap.get(reader);
        final ArrayList<RefKey> forDelete = new ArrayList<RefKey>();
        for (final Map.Entry<RefKey, IndirectReferences> entry : currIndirects.entrySet()) {
            final PdfIndirectReference iRef = entry.getValue().theRef;
            final RefKey key = new RefKey(iRef);
            final PdfIndirectObject iobj = this.indirectObjects.get(key);
            if (iobj == null) {
                forDelete.add(entry.getKey());
            }
            else {
                if (!iobj.object.isArray() && !iobj.object.isDictionary() && !iobj.object.isStream()) {
                    continue;
                }
                forDelete.add(entry.getKey());
            }
        }
        for (final RefKey key2 : forDelete) {
            currIndirects.remove(key2);
        }
    }
    
    private int checkStructureTreeRootKids(final ImportedPage newPage) {
        if (this.importedPages.size() == 0) {
            return 1;
        }
        boolean readerExist = false;
        for (final ImportedPage page : this.importedPages) {
            if (page.reader.equals(newPage.reader)) {
                readerExist = true;
                break;
            }
        }
        if (!readerExist) {
            return 1;
        }
        final ImportedPage lastPage = this.importedPages.get(this.importedPages.size() - 1);
        final boolean equalReader = lastPage.reader.equals(newPage.reader);
        if (!equalReader || newPage.pageNumber <= lastPage.pageNumber) {
            return -1;
        }
        if (this.readersWithImportedStructureTreeRootKids.contains(newPage.reader)) {
            return 0;
        }
        return 1;
    }
    
    protected void structureTreeRootKidsForReaderImported(final PdfReader reader) {
        this.readersWithImportedStructureTreeRootKids.add(reader);
    }
    
    protected void fixStructureTreeRoot(final HashSet<RefKey> activeKeys, final HashSet<PdfName> activeClassMaps) {
        final HashMap<PdfName, PdfObject> newClassMap = new HashMap<PdfName, PdfObject>(activeClassMaps.size());
        for (final PdfName key : activeClassMaps) {
            final PdfObject cm = this.structureTreeRoot.classes.get(key);
            if (cm != null) {
                newClassMap.put(key, cm);
            }
        }
        this.structureTreeRoot.classes = newClassMap;
        final PdfArray kids = this.structureTreeRoot.getAsArray(PdfName.K);
        if (kids != null) {
            for (int i = 0; i < kids.size(); ++i) {
                final PdfIndirectReference iref = (PdfIndirectReference)kids.getPdfObject(i);
                final RefKey key2 = new RefKey(iref);
                if (!activeKeys.contains(key2)) {
                    kids.remove(i--);
                }
            }
        }
    }
    
    protected PdfImportedPage getImportedPageImpl(final PdfReader reader, final int pageNumber) {
        if (this.currentPdfReaderInstance != null) {
            if (this.currentPdfReaderInstance.getReader() != reader) {
                this.currentPdfReaderInstance = super.getPdfReaderInstance(reader);
            }
        }
        else {
            this.currentPdfReaderInstance = super.getPdfReaderInstance(reader);
        }
        return this.currentPdfReaderInstance.getImportedPage(pageNumber);
    }
    
    protected PdfIndirectReference copyIndirect(final PRIndirectReference in, final boolean keepStructure, final boolean directRootKids) throws IOException, BadPdfFormatException {
        final RefKey key = new RefKey(in);
        IndirectReferences iRef = this.indirects.get(key);
        final PdfObject obj = PdfReader.getPdfObjectRelease(in);
        if (keepStructure && directRootKids && obj instanceof PdfDictionary) {
            final PdfDictionary dict = (PdfDictionary)obj;
            if (dict.contains(PdfName.PG)) {
                return null;
            }
        }
        PdfIndirectReference theRef;
        if (iRef != null) {
            theRef = iRef.getRef();
            if (iRef.getCopied()) {
                return theRef;
            }
        }
        else {
            theRef = this.body.getPdfIndirectReference();
            iRef = new IndirectReferences(theRef);
            this.indirects.put(key, iRef);
        }
        if (obj != null && obj.isDictionary()) {
            final PdfObject type = PdfReader.getPdfObjectRelease(((PdfDictionary)obj).get(PdfName.TYPE));
            if (type != null) {
                if (PdfName.PAGE.equals(type)) {
                    return theRef;
                }
                if (PdfName.CATALOG.equals(type)) {
                    PdfCopy.LOGGER.warn(MessageLocalization.getComposedMessage("make.copy.of.catalog.dictionary.is.forbidden", new Object[0]));
                    return null;
                }
            }
        }
        iRef.setCopied();
        if (obj != null) {
            this.parentObjects.put(obj, in);
        }
        final PdfObject res = this.copyObject(obj, keepStructure, directRootKids);
        if (this.disableIndirects.contains(obj)) {
            iRef.setNotCopied();
        }
        if (res != null) {
            this.addToBody(res, theRef);
            return theRef;
        }
        this.indirects.remove(key);
        return null;
    }
    
    protected PdfIndirectReference copyIndirect(final PRIndirectReference in) throws IOException, BadPdfFormatException {
        return this.copyIndirect(in, false, false);
    }
    
    protected PdfDictionary copyDictionary(final PdfDictionary in, final boolean keepStruct, final boolean directRootKids) throws IOException, BadPdfFormatException {
        final PdfDictionary out = new PdfDictionary(in.size());
        final PdfObject type = PdfReader.getPdfObjectRelease(in.get(PdfName.TYPE));
        if (keepStruct) {
            if (directRootKids && in.contains(PdfName.PG)) {
                PdfObject curr = in;
                this.disableIndirects.add(curr);
                while (this.parentObjects.containsKey(curr) && !this.disableIndirects.contains(curr)) {
                    curr = this.parentObjects.get(curr);
                    this.disableIndirects.add(curr);
                }
                return null;
            }
            final PdfName structType = in.getAsName(PdfName.S);
            this.structTreeController.addRole(structType);
            this.structTreeController.addClass(in);
        }
        if (this.structTreeController != null && this.structTreeController.reader != null && (in.contains(PdfName.STRUCTPARENTS) || in.contains(PdfName.STRUCTPARENT))) {
            PdfName key = PdfName.STRUCTPARENT;
            if (in.contains(PdfName.STRUCTPARENTS)) {
                key = PdfName.STRUCTPARENTS;
            }
            final PdfObject value = in.get(key);
            out.put(key, new PdfNumber(this.currentStructArrayNumber));
            this.structTreeController.copyStructTreeForPage((PdfNumber)value, this.currentStructArrayNumber++);
        }
        for (final Object element : in.getKeys()) {
            final PdfName key2 = (PdfName)element;
            final PdfObject value2 = in.get(key2);
            if (this.structTreeController != null && this.structTreeController.reader != null) {
                if (key2.equals(PdfName.STRUCTPARENTS)) {
                    continue;
                }
                if (key2.equals(PdfName.STRUCTPARENT)) {
                    continue;
                }
            }
            if (PdfName.PAGE.equals(type)) {
                if (key2.equals(PdfName.B) || key2.equals(PdfName.PARENT)) {
                    continue;
                }
                this.parentObjects.put(value2, in);
                final PdfObject res = this.copyObject(value2, keepStruct, directRootKids);
                if (res == null) {
                    continue;
                }
                out.put(key2, res);
            }
            else {
                PdfObject res;
                if (this.tagged && value2.isIndirect() && this.isStructTreeRootReference((PdfIndirectReference)value2)) {
                    res = this.structureTreeRoot.getReference();
                }
                else {
                    res = this.copyObject(value2, keepStruct, directRootKids);
                }
                if (res == null) {
                    continue;
                }
                out.put(key2, res);
            }
        }
        return out;
    }
    
    protected PdfDictionary copyDictionary(final PdfDictionary in) throws IOException, BadPdfFormatException {
        return this.copyDictionary(in, false, false);
    }
    
    protected PdfStream copyStream(final PRStream in) throws IOException, BadPdfFormatException {
        final PRStream out = new PRStream(in, null);
        for (final Object element : in.getKeys()) {
            final PdfName key = (PdfName)element;
            final PdfObject value = in.get(key);
            this.parentObjects.put(value, in);
            final PdfObject res = this.copyObject(value);
            if (res != null) {
                out.put(key, res);
            }
        }
        return out;
    }
    
    protected PdfArray copyArray(final PdfArray in, final boolean keepStruct, final boolean directRootKids) throws IOException, BadPdfFormatException {
        final PdfArray out = new PdfArray(in.size());
        final Iterator<PdfObject> i = in.listIterator();
        while (i.hasNext()) {
            final PdfObject value = i.next();
            this.parentObjects.put(value, in);
            final PdfObject res = this.copyObject(value, keepStruct, directRootKids);
            if (res != null) {
                out.add(res);
            }
        }
        return out;
    }
    
    protected PdfArray copyArray(final PdfArray in) throws IOException, BadPdfFormatException {
        return this.copyArray(in, false, false);
    }
    
    protected PdfObject copyObject(final PdfObject in, final boolean keepStruct, final boolean directRootKids) throws IOException, BadPdfFormatException {
        if (in == null) {
            return PdfNull.PDFNULL;
        }
        switch (in.type) {
            case 6: {
                return this.copyDictionary((PdfDictionary)in, keepStruct, directRootKids);
            }
            case 10: {
                if (!keepStruct && !directRootKids) {
                    return this.copyIndirect((PRIndirectReference)in);
                }
                return this.copyIndirect((PRIndirectReference)in, keepStruct, directRootKids);
            }
            case 5: {
                return this.copyArray((PdfArray)in, keepStruct, directRootKids);
            }
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 8: {
                return in;
            }
            case 7: {
                return this.copyStream((PRStream)in);
            }
            default: {
                if (in.type >= 0) {
                    System.out.println("CANNOT COPY type " + in.type);
                    return null;
                }
                final String lit = ((PdfLiteral)in).toString();
                if (lit.equals("true") || lit.equals("false")) {
                    return new PdfBoolean(lit);
                }
                return new PdfLiteral(lit);
            }
        }
    }
    
    protected PdfObject copyObject(final PdfObject in) throws IOException, BadPdfFormatException {
        return this.copyObject(in, false, false);
    }
    
    protected int setFromIPage(final PdfImportedPage iPage) {
        final int pageNum = iPage.getPageNumber();
        final PdfReaderInstance pdfReaderInstance = iPage.getPdfReaderInstance();
        this.currentPdfReaderInstance = pdfReaderInstance;
        final PdfReaderInstance inst = pdfReaderInstance;
        this.setFromReader(this.reader = inst.getReader());
        return pageNum;
    }
    
    protected void setFromReader(final PdfReader reader) {
        this.reader = reader;
        this.indirects = this.indirectMap.get(reader);
        if (this.indirects == null) {
            this.indirects = new HashMap<RefKey, IndirectReferences>();
            this.indirectMap.put(reader, this.indirects);
        }
    }
    
    public void addPage(final PdfImportedPage iPage) throws IOException, BadPdfFormatException {
        if (this.mergeFields && !this.mergeFieldsInternalCall) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("1.method.cannot.be.used.in.mergeFields.mode.please.use.addDocument", "addPage"));
        }
        final int pageNum = this.setFromIPage(iPage);
        final PdfDictionary thePage = this.reader.getPageN(pageNum);
        final PRIndirectReference origRef = this.reader.getPageOrigRef(pageNum);
        this.reader.releasePage(pageNum);
        final RefKey key = new RefKey(origRef);
        IndirectReferences iRef = this.indirects.get(key);
        if (iRef != null && !iRef.getCopied()) {
            this.pageReferences.add(iRef.getRef());
            iRef.setCopied();
        }
        final PdfIndirectReference pageRef = this.getCurrentPage();
        if (iRef == null) {
            iRef = new IndirectReferences(pageRef);
            this.indirects.put(key, iRef);
        }
        iRef.setCopied();
        if (this.tagged) {
            this.structTreeRootReference = (PRIndirectReference)this.reader.getCatalog().get(PdfName.STRUCTTREEROOT);
        }
        final PdfDictionary newPage = this.copyDictionary(thePage);
        if (this.mergeFields) {
            final ImportedPage importedPage = this.importedPages.get(this.importedPages.size() - 1);
            importedPage.annotsIndirectReference = this.body.getPdfIndirectReference();
            newPage.put(PdfName.ANNOTS, importedPage.annotsIndirectReference);
        }
        this.root.addPage(newPage);
        iPage.setCopied();
        ++this.currentPageNumber;
        this.pdf.setPageCount(this.currentPageNumber);
        this.structTreeRootReference = null;
    }
    
    public void addPage(final Rectangle rect, final int rotation) throws DocumentException {
        if (this.mergeFields && !this.mergeFieldsInternalCall) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("1.method.cannot.be.used.in.mergeFields.mode.please.use.addDocument", "addPage"));
        }
        final PdfRectangle mediabox = new PdfRectangle(rect, rotation);
        final PageResources resources = new PageResources();
        final PdfPage page = new PdfPage(mediabox, new HashMap<String, PdfRectangle>(), resources.getResources(), 0);
        page.put(PdfName.TABS, this.getTabs());
        this.root.addPage(page);
        ++this.currentPageNumber;
        this.pdf.setPageCount(this.currentPageNumber);
    }
    
    public void addDocument(final PdfReader reader, final List<Integer> pagesToKeep) throws DocumentException, IOException {
        if (this.indirectMap.containsKey(reader)) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("document.1.has.already.been.added", reader.toString()));
        }
        reader.selectPages(pagesToKeep, false);
        this.addDocument(reader);
    }
    
    public void copyDocumentFields(final PdfReader reader) throws DocumentException, IOException {
        if (!this.document.isOpen()) {
            throw new DocumentException(MessageLocalization.getComposedMessage("the.document.is.not.open.yet.you.can.only.add.meta.information", new Object[0]));
        }
        if (this.indirectMap.containsKey(reader)) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("document.1.has.already.been.added", reader.toString()));
        }
        if (!reader.isOpenedWithFullPermissions()) {
            throw new BadPasswordException(MessageLocalization.getComposedMessage("pdfreader.not.opened.with.owner.password", new Object[0]));
        }
        if (!this.mergeFields) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("1.method.can.be.only.used.in.mergeFields.mode.please.use.addDocument", "copyDocumentFields"));
        }
        this.indirects = new HashMap<RefKey, IndirectReferences>();
        this.indirectMap.put(reader, this.indirects);
        reader.consolidateNamedDestinations();
        reader.shuffleSubsetNames();
        if (this.tagged && PdfStructTreeController.checkTagged(reader)) {
            this.structTreeRootReference = (PRIndirectReference)reader.getCatalog().get(PdfName.STRUCTTREEROOT);
            if (this.structTreeController != null) {
                if (reader != this.structTreeController.reader) {
                    this.structTreeController.setReader(reader);
                }
            }
            else {
                this.structTreeController = new PdfStructTreeController(reader, this);
            }
        }
        final List<PdfObject> annotationsToBeCopied = new ArrayList<PdfObject>();
        for (int i = 1; i <= reader.getNumberOfPages(); ++i) {
            final PdfDictionary page = reader.getPageNRelease(i);
            if (page != null && page.contains(PdfName.ANNOTS)) {
                final PdfArray annots = page.getAsArray(PdfName.ANNOTS);
                if (annots != null && annots.size() > 0) {
                    if (this.importedPages.size() < i) {
                        throw new DocumentException(MessageLocalization.getComposedMessage("there.are.not.enough.imported.pages.for.copied.fields", new Object[0]));
                    }
                    this.indirectMap.get(reader).put(new RefKey(reader.pageRefs.getPageOrigRef(i)), new IndirectReferences(this.pageReferences.get(i - 1)));
                    for (int j = 0; j < annots.size(); ++j) {
                        final PdfDictionary annot = annots.getAsDict(j);
                        if (annot != null) {
                            annot.put(PdfCopy.annotId, new PdfNumber(++PdfCopy.annotIdCnt));
                            annotationsToBeCopied.add(annots.getPdfObject(j));
                        }
                    }
                }
            }
        }
        for (final PdfObject annot2 : annotationsToBeCopied) {
            this.copyObject(annot2);
        }
        if (this.tagged && this.structTreeController != null) {
            this.structTreeController.attachStructTreeRootKids(null);
        }
        final AcroFields acro = reader.getAcroFields();
        final boolean needapp = !acro.isGenerateAppearances();
        if (needapp) {
            this.needAppearances = true;
        }
        this.fields.add(acro);
        this.updateCalculationOrder(reader);
        this.structTreeRootReference = null;
    }
    
    public void addDocument(final PdfReader reader) throws DocumentException, IOException {
        if (!this.document.isOpen()) {
            throw new DocumentException(MessageLocalization.getComposedMessage("the.document.is.not.open.yet.you.can.only.add.meta.information", new Object[0]));
        }
        if (this.indirectMap.containsKey(reader)) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("document.1.has.already.been.added", reader.toString()));
        }
        if (!reader.isOpenedWithFullPermissions()) {
            throw new BadPasswordException(MessageLocalization.getComposedMessage("pdfreader.not.opened.with.owner.password", new Object[0]));
        }
        if (this.mergeFields) {
            reader.consolidateNamedDestinations();
            reader.shuffleSubsetNames();
            for (int i = 1; i <= reader.getNumberOfPages(); ++i) {
                final PdfDictionary page = reader.getPageNRelease(i);
                if (page != null && page.contains(PdfName.ANNOTS)) {
                    final PdfArray annots = page.getAsArray(PdfName.ANNOTS);
                    if (annots != null) {
                        for (int j = 0; j < annots.size(); ++j) {
                            final PdfDictionary annot = annots.getAsDict(j);
                            if (annot != null) {
                                annot.put(PdfCopy.annotId, new PdfNumber(++PdfCopy.annotIdCnt));
                            }
                        }
                    }
                }
            }
            final AcroFields acro = reader.getAcroFields();
            final boolean needapp = !acro.isGenerateAppearances();
            if (needapp) {
                this.needAppearances = true;
            }
            this.fields.add(acro);
            this.updateCalculationOrder(reader);
        }
        final boolean tagged = this.tagged && PdfStructTreeController.checkTagged(reader);
        this.mergeFieldsInternalCall = true;
        for (int k = 1; k <= reader.getNumberOfPages(); ++k) {
            this.addPage(this.getImportedPage(reader, k, tagged));
        }
        this.mergeFieldsInternalCall = false;
    }
    
    @Override
    public PdfIndirectObject addToBody(final PdfObject object, final PdfIndirectReference ref) throws IOException {
        return this.addToBody(object, ref, false);
    }
    
    @Override
    public PdfIndirectObject addToBody(final PdfObject object, final PdfIndirectReference ref, final boolean formBranching) throws IOException {
        if (formBranching) {
            this.updateReferences(object);
        }
        PdfIndirectObject iobj;
        if ((this.tagged || this.mergeFields) && this.indirectObjects != null && (object.isArray() || object.isDictionary() || object.isStream() || object.isNull())) {
            final RefKey key = new RefKey(ref);
            PdfIndirectObject obj = this.indirectObjects.get(key);
            if (obj == null) {
                obj = new PdfIndirectObject(ref, object, this);
                this.indirectObjects.put(key, obj);
            }
            iobj = obj;
        }
        else {
            iobj = super.addToBody(object, ref);
        }
        if (this.mergeFields && object.isDictionary()) {
            final PdfNumber annotId = ((PdfDictionary)object).getAsNumber(PdfCopy.annotId);
            if (annotId != null) {
                if (formBranching) {
                    this.mergedMap.put(annotId.intValue(), iobj);
                    this.mergedSet.add(iobj);
                }
                else {
                    this.unmergedMap.put(annotId.intValue(), iobj);
                    this.unmergedIndirectRefsMap.put(new RefKey(iobj.number, iobj.generation), iobj);
                }
            }
        }
        return iobj;
    }
    
    @Override
    protected void cacheObject(final PdfIndirectObject iobj) {
        if ((this.tagged || this.mergeFields) && this.indirectObjects != null) {
            this.savedObjects.add(iobj);
            final RefKey key = new RefKey(iobj.number, iobj.generation);
            if (!this.indirectObjects.containsKey(key)) {
                this.indirectObjects.put(key, iobj);
            }
        }
    }
    
    @Override
    protected void flushTaggedObjects() throws IOException {
        try {
            this.fixTaggedStructure();
        }
        catch (ClassCastException ex) {}
        finally {
            this.flushIndirectObjects();
        }
    }
    
    @Override
    protected void flushAcroFields() throws IOException, BadPdfFormatException {
        if (this.mergeFields) {
            try {
                for (final ImportedPage page : this.importedPages) {
                    final PdfDictionary pageDict = page.reader.getPageN(page.pageNumber);
                    if (pageDict != null) {
                        final PdfArray pageFields = pageDict.getAsArray(PdfName.ANNOTS);
                        if (pageFields == null) {
                            continue;
                        }
                        if (pageFields.size() == 0) {
                            continue;
                        }
                        for (final AcroFields.Item items : page.reader.getAcroFields().getFields().values()) {
                            for (final PdfIndirectReference ref : items.widget_refs) {
                                pageFields.arrayList.remove(ref);
                            }
                        }
                        this.indirects = this.indirectMap.get(page.reader);
                        for (final PdfObject ref2 : pageFields.arrayList) {
                            page.mergedFields.add(this.copyObject(ref2));
                        }
                    }
                }
                for (final PdfReader reader : this.indirectMap.keySet()) {
                    reader.removeFields();
                }
                this.mergeFields();
                this.createAcroForms();
            }
            catch (ClassCastException ex) {}
            finally {
                if (!this.tagged) {
                    this.flushIndirectObjects();
                }
            }
        }
    }
    
    protected void fixTaggedStructure() throws IOException {
        final HashMap<Integer, PdfIndirectReference> numTree = this.structureTreeRoot.getNumTree();
        final HashSet<RefKey> activeKeys = new HashSet<RefKey>();
        final ArrayList<PdfIndirectReference> actives = new ArrayList<PdfIndirectReference>();
        int pageRefIndex = 0;
        if (this.mergeFields && this.acroForm != null) {
            actives.add(this.acroForm);
            activeKeys.add(new RefKey(this.acroForm));
        }
        for (final PdfIndirectReference page : this.pageReferences) {
            actives.add(page);
            activeKeys.add(new RefKey(page));
        }
        for (int i = numTree.size() - 1; i >= 0; --i) {
            final PdfIndirectReference currNum = numTree.get(i);
            if (currNum != null) {
                final RefKey numKey = new RefKey(currNum);
                final PdfObject obj = this.indirectObjects.get(numKey).object;
                if (obj.isDictionary()) {
                    boolean addActiveKeys = false;
                    if (this.pageReferences.contains(((PdfDictionary)obj).get(PdfName.PG))) {
                        addActiveKeys = true;
                    }
                    else {
                        final PdfDictionary k = PdfStructTreeController.getKDict((PdfDictionary)obj);
                        if (k != null && this.pageReferences.contains(k.get(PdfName.PG))) {
                            addActiveKeys = true;
                        }
                    }
                    if (addActiveKeys) {
                        activeKeys.add(numKey);
                        actives.add(currNum);
                    }
                    else {
                        numTree.remove(i);
                    }
                }
                else if (obj.isArray()) {
                    activeKeys.add(numKey);
                    actives.add(currNum);
                    final PdfArray currNums = (PdfArray)obj;
                    final PdfIndirectReference currPage = this.pageReferences.get(pageRefIndex++);
                    actives.add(currPage);
                    activeKeys.add(new RefKey(currPage));
                    PdfIndirectReference prevKid = null;
                    for (int j = 0; j < currNums.size(); ++j) {
                        final PdfIndirectReference currKid = (PdfIndirectReference)currNums.getDirectObject(j);
                        if (!currKid.equals(prevKid)) {
                            final RefKey kidKey = new RefKey(currKid);
                            activeKeys.add(kidKey);
                            actives.add(currKid);
                            final PdfIndirectObject iobj = this.indirectObjects.get(kidKey);
                            if (iobj.object.isDictionary()) {
                                final PdfDictionary dict = (PdfDictionary)iobj.object;
                                final PdfIndirectReference pg = (PdfIndirectReference)dict.get(PdfName.PG);
                                if (pg != null && !this.pageReferences.contains(pg) && !pg.equals(currPage)) {
                                    dict.put(PdfName.PG, currPage);
                                    final PdfArray kids = dict.getAsArray(PdfName.K);
                                    if (kids != null) {
                                        final PdfObject firstKid = kids.getDirectObject(0);
                                        if (firstKid.isNumber()) {
                                            kids.remove(0);
                                        }
                                    }
                                }
                            }
                            prevKid = currKid;
                        }
                    }
                }
            }
        }
        final HashSet<PdfName> activeClassMaps = new HashSet<PdfName>();
        this.findActives(actives, activeKeys, activeClassMaps);
        final ArrayList<PdfIndirectReference> newRefs = this.findActiveParents(activeKeys);
        this.fixPgKey(newRefs, activeKeys);
        this.fixStructureTreeRoot(activeKeys, activeClassMaps);
        for (final Map.Entry<RefKey, PdfIndirectObject> entry : this.indirectObjects.entrySet()) {
            if (!activeKeys.contains(entry.getKey())) {
                entry.setValue(null);
            }
            else if (entry.getValue().object.isArray()) {
                this.removeInactiveReferences((PdfArray)entry.getValue().object, activeKeys);
            }
            else {
                if (!entry.getValue().object.isDictionary()) {
                    continue;
                }
                final PdfObject kids2 = ((PdfDictionary)entry.getValue().object).get(PdfName.K);
                if (kids2 == null || !kids2.isArray()) {
                    continue;
                }
                this.removeInactiveReferences((PdfArray)kids2, activeKeys);
            }
        }
    }
    
    private void removeInactiveReferences(final PdfArray array, final HashSet<RefKey> activeKeys) {
        for (int i = 0; i < array.size(); ++i) {
            final PdfObject obj = array.getPdfObject(i);
            if ((obj.type() == 0 && !activeKeys.contains(new RefKey((PdfIndirectReference)obj))) || (obj.isDictionary() && this.containsInactivePg((PdfDictionary)obj, activeKeys))) {
                array.remove(i--);
            }
        }
    }
    
    private boolean containsInactivePg(final PdfDictionary dict, final HashSet<RefKey> activeKeys) {
        final PdfObject pg = dict.get(PdfName.PG);
        return pg != null && !activeKeys.contains(new RefKey((PdfIndirectReference)pg));
    }
    
    private ArrayList<PdfIndirectReference> findActiveParents(final HashSet<RefKey> activeKeys) {
        final ArrayList<PdfIndirectReference> newRefs = new ArrayList<PdfIndirectReference>();
        final ArrayList<RefKey> tmpActiveKeys = new ArrayList<RefKey>(activeKeys);
        for (int i = 0; i < tmpActiveKeys.size(); ++i) {
            final PdfIndirectObject iobj = this.indirectObjects.get(tmpActiveKeys.get(i));
            if (iobj != null) {
                if (iobj.object.isDictionary()) {
                    final PdfObject parent = ((PdfDictionary)iobj.object).get(PdfName.P);
                    if (parent != null && parent.type() == 0) {
                        final RefKey key = new RefKey((PdfIndirectReference)parent);
                        if (!activeKeys.contains(key)) {
                            activeKeys.add(key);
                            tmpActiveKeys.add(key);
                            newRefs.add((PdfIndirectReference)parent);
                        }
                    }
                }
            }
        }
        return newRefs;
    }
    
    private void fixPgKey(final ArrayList<PdfIndirectReference> newRefs, final HashSet<RefKey> activeKeys) {
        for (final PdfIndirectReference iref : newRefs) {
            final PdfIndirectObject iobj = this.indirectObjects.get(new RefKey(iref));
            if (iobj != null) {
                if (!iobj.object.isDictionary()) {
                    continue;
                }
                final PdfDictionary dict = (PdfDictionary)iobj.object;
                final PdfObject pg = dict.get(PdfName.PG);
                if (pg == null) {
                    continue;
                }
                if (activeKeys.contains(new RefKey((PdfIndirectReference)pg))) {
                    continue;
                }
                final PdfArray kids = dict.getAsArray(PdfName.K);
                if (kids == null) {
                    continue;
                }
                for (int i = 0; i < kids.size(); ++i) {
                    final PdfObject obj = kids.getPdfObject(i);
                    if (obj.type() != 0) {
                        kids.remove(i--);
                    }
                    else {
                        final PdfIndirectObject kid = this.indirectObjects.get(new RefKey((PdfIndirectReference)obj));
                        if (kid != null && kid.object.isDictionary()) {
                            final PdfObject kidPg = ((PdfDictionary)kid.object).get(PdfName.PG);
                            if (kidPg != null && activeKeys.contains(new RefKey((PdfIndirectReference)kidPg))) {
                                dict.put(PdfName.PG, kidPg);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void findActives(final ArrayList<PdfIndirectReference> actives, final HashSet<RefKey> activeKeys, final HashSet<PdfName> activeClassMaps) {
        for (int i = 0; i < actives.size(); ++i) {
            final RefKey key = new RefKey(actives.get(i));
            final PdfIndirectObject iobj = this.indirectObjects.get(key);
            if (iobj != null) {
                if (iobj.object != null) {
                    switch (iobj.object.type()) {
                        case 0: {
                            this.findActivesFromReference((PdfIndirectReference)iobj.object, actives, activeKeys);
                            break;
                        }
                        case 5: {
                            this.findActivesFromArray((PdfArray)iobj.object, actives, activeKeys, activeClassMaps);
                            break;
                        }
                        case 6:
                        case 7: {
                            this.findActivesFromDict((PdfDictionary)iobj.object, actives, activeKeys, activeClassMaps);
                            break;
                        }
                    }
                }
            }
        }
    }
    
    private void findActivesFromReference(final PdfIndirectReference iref, final ArrayList<PdfIndirectReference> actives, final HashSet<RefKey> activeKeys) {
        final RefKey key = new RefKey(iref);
        final PdfIndirectObject iobj = this.indirectObjects.get(key);
        if (iobj != null && iobj.object.isDictionary() && this.containsInactivePg((PdfDictionary)iobj.object, activeKeys)) {
            return;
        }
        if (!activeKeys.contains(key)) {
            activeKeys.add(key);
            actives.add(iref);
        }
    }
    
    private void findActivesFromArray(final PdfArray array, final ArrayList<PdfIndirectReference> actives, final HashSet<RefKey> activeKeys, final HashSet<PdfName> activeClassMaps) {
        for (final PdfObject obj : array) {
            switch (obj.type()) {
                case 0: {
                    this.findActivesFromReference((PdfIndirectReference)obj, actives, activeKeys);
                    continue;
                }
                case 5: {
                    this.findActivesFromArray((PdfArray)obj, actives, activeKeys, activeClassMaps);
                    continue;
                }
                case 6:
                case 7: {
                    this.findActivesFromDict((PdfDictionary)obj, actives, activeKeys, activeClassMaps);
                    continue;
                }
            }
        }
    }
    
    private void findActivesFromDict(final PdfDictionary dict, final ArrayList<PdfIndirectReference> actives, final HashSet<RefKey> activeKeys, final HashSet<PdfName> activeClassMaps) {
        if (this.containsInactivePg(dict, activeKeys)) {
            return;
        }
        for (final PdfName key : dict.getKeys()) {
            final PdfObject obj = dict.get(key);
            if (key.equals(PdfName.P)) {
                continue;
            }
            if (key.equals(PdfName.C)) {
                if (obj.isArray()) {
                    for (final PdfObject cm : (PdfArray)obj) {
                        if (cm.isName()) {
                            activeClassMaps.add((PdfName)cm);
                        }
                    }
                }
                else {
                    if (!obj.isName()) {
                        continue;
                    }
                    activeClassMaps.add((PdfName)obj);
                }
            }
            else {
                switch (obj.type()) {
                    case 0: {
                        this.findActivesFromReference((PdfIndirectReference)obj, actives, activeKeys);
                        continue;
                    }
                    case 5: {
                        this.findActivesFromArray((PdfArray)obj, actives, activeKeys, activeClassMaps);
                        continue;
                    }
                    case 6:
                    case 7: {
                        this.findActivesFromDict((PdfDictionary)obj, actives, activeKeys, activeClassMaps);
                        continue;
                    }
                }
            }
        }
    }
    
    protected void flushIndirectObjects() throws IOException {
        for (final PdfIndirectObject iobj : this.savedObjects) {
            this.indirectObjects.remove(new RefKey(iobj.number, iobj.generation));
        }
        final HashSet<RefKey> inactives = new HashSet<RefKey>();
        for (final Map.Entry<RefKey, PdfIndirectObject> entry : this.indirectObjects.entrySet()) {
            if (entry.getValue() != null) {
                this.writeObjectToBody(entry.getValue());
            }
            else {
                inactives.add(entry.getKey());
            }
        }
        final ArrayList<PdfBody.PdfCrossReference> pdfCrossReferences = new ArrayList<PdfBody.PdfCrossReference>(this.body.xrefs);
        for (final PdfBody.PdfCrossReference cr : pdfCrossReferences) {
            final RefKey key = new RefKey(cr.getRefnum(), 0);
            if (inactives.contains(key)) {
                this.body.xrefs.remove(cr);
            }
        }
        this.indirectObjects = null;
    }
    
    private void writeObjectToBody(final PdfIndirectObject object) throws IOException {
        boolean skipWriting = false;
        if (this.mergeFields) {
            this.updateAnnotationReferences(object.object);
            if (object.object.isDictionary() || object.object.isStream()) {
                final PdfDictionary dictionary = (PdfDictionary)object.object;
                if (this.unmergedIndirectRefsMap.containsKey(new RefKey(object.number, object.generation))) {
                    final PdfNumber annotId = dictionary.getAsNumber(PdfCopy.annotId);
                    if (annotId != null && this.mergedMap.containsKey(annotId.intValue())) {
                        skipWriting = true;
                    }
                }
                if (this.mergedSet.contains(object)) {
                    final PdfNumber annotId = dictionary.getAsNumber(PdfCopy.annotId);
                    if (annotId != null) {
                        final PdfIndirectObject unmerged = this.unmergedMap.get(annotId.intValue());
                        if (unmerged != null && unmerged.object.isDictionary()) {
                            final PdfNumber structParent = ((PdfDictionary)unmerged.object).getAsNumber(PdfName.STRUCTPARENT);
                            if (structParent != null) {
                                dictionary.put(PdfName.STRUCTPARENT, structParent);
                            }
                        }
                    }
                }
            }
        }
        if (!skipWriting) {
            PdfDictionary dictionary = null;
            PdfNumber annotId = null;
            if (this.mergeFields && object.object.isDictionary()) {
                dictionary = (PdfDictionary)object.object;
                annotId = dictionary.getAsNumber(PdfCopy.annotId);
                if (annotId != null) {
                    dictionary.remove(PdfCopy.annotId);
                }
            }
            this.body.add(object.object, object.number, object.generation, true);
            if (annotId != null) {
                dictionary.put(PdfCopy.annotId, annotId);
            }
        }
    }
    
    private void updateAnnotationReferences(final PdfObject obj) {
        if (obj.isArray()) {
            final PdfArray array = (PdfArray)obj;
            for (int i = 0; i < array.size(); ++i) {
                final PdfObject o = array.getPdfObject(i);
                if (o != null && o.type() == 0) {
                    final PdfIndirectObject entry = this.unmergedIndirectRefsMap.get(new RefKey((PdfIndirectReference)o));
                    if (entry != null && entry.object.isDictionary()) {
                        final PdfNumber annotId = ((PdfDictionary)entry.object).getAsNumber(PdfCopy.annotId);
                        if (annotId != null) {
                            final PdfIndirectObject merged = this.mergedMap.get(annotId.intValue());
                            if (merged != null) {
                                array.set(i, merged.getIndirectReference());
                            }
                        }
                    }
                }
                else {
                    this.updateAnnotationReferences(o);
                }
            }
        }
        else if (obj.isDictionary() || obj.isStream()) {
            final PdfDictionary dictionary = (PdfDictionary)obj;
            for (final PdfName key : dictionary.getKeys()) {
                final PdfObject o2 = dictionary.get(key);
                if (o2 != null && o2.type() == 0) {
                    final PdfIndirectObject entry2 = this.unmergedIndirectRefsMap.get(new RefKey((PdfIndirectReference)o2));
                    if (entry2 == null || !entry2.object.isDictionary()) {
                        continue;
                    }
                    final PdfNumber annotId2 = ((PdfDictionary)entry2.object).getAsNumber(PdfCopy.annotId);
                    if (annotId2 == null) {
                        continue;
                    }
                    final PdfIndirectObject merged2 = this.mergedMap.get(annotId2.intValue());
                    if (merged2 == null) {
                        continue;
                    }
                    dictionary.put(key, merged2.getIndirectReference());
                }
                else {
                    this.updateAnnotationReferences(o2);
                }
            }
        }
    }
    
    private void updateCalculationOrder(final PdfReader reader) {
        final PdfDictionary catalog = reader.getCatalog();
        final PdfDictionary acro = catalog.getAsDict(PdfName.ACROFORM);
        if (acro == null) {
            return;
        }
        final PdfArray co = acro.getAsArray(PdfName.CO);
        if (co == null || co.size() == 0) {
            return;
        }
        final AcroFields af = reader.getAcroFields();
        for (int k = 0; k < co.size(); ++k) {
            final PdfObject obj = co.getPdfObject(k);
            if (obj != null) {
                if (obj.isIndirect()) {
                    String name = getCOName(reader, (PRIndirectReference)obj);
                    if (af.getFieldItem(name) != null) {
                        name = "." + name;
                        if (!this.calculationOrder.contains(name)) {
                            this.calculationOrder.add(name);
                        }
                    }
                }
            }
        }
    }
    
    private static String getCOName(final PdfReader reader, PRIndirectReference ref) {
        String name = "";
        while (ref != null) {
            final PdfObject obj = PdfReader.getPdfObject(ref);
            if (obj == null) {
                break;
            }
            if (obj.type() != 6) {
                break;
            }
            final PdfDictionary dic = (PdfDictionary)obj;
            final PdfString t = dic.getAsString(PdfName.T);
            if (t != null) {
                name = t.toUnicodeString() + "." + name;
            }
            ref = (PRIndirectReference)dic.get(PdfName.PARENT);
        }
        if (name.endsWith(".")) {
            name = name.substring(0, name.length() - 2);
        }
        return name;
    }
    
    private void mergeFields() {
        int pageOffset = 0;
        for (int k = 0; k < this.fields.size(); ++k) {
            final AcroFields af = this.fields.get(k);
            final Map<String, AcroFields.Item> fd = af.getFields();
            if (pageOffset < this.importedPages.size() && this.importedPages.get(pageOffset).reader == af.reader) {
                this.addPageOffsetToField(fd, pageOffset);
                pageOffset += af.reader.getNumberOfPages();
            }
            this.mergeWithMaster(fd);
        }
    }
    
    private void addPageOffsetToField(final Map<String, AcroFields.Item> fd, final int pageOffset) {
        if (pageOffset == 0) {
            return;
        }
        for (final AcroFields.Item item : fd.values()) {
            for (int k = 0; k < item.size(); ++k) {
                final int p = item.getPage(k);
                item.forcePage(k, p + pageOffset);
            }
        }
    }
    
    private void mergeWithMaster(final Map<String, AcroFields.Item> fd) {
        for (final Map.Entry<String, AcroFields.Item> entry : fd.entrySet()) {
            final String name = entry.getKey();
            this.mergeField(name, entry.getValue());
        }
    }
    
    private void mergeField(final String name, final AcroFields.Item item) {
        HashMap<String, Object> map = this.fieldTree;
        final StringTokenizer tk = new StringTokenizer(name, ".");
        if (!tk.hasMoreTokens()) {
            return;
        }
        while (true) {
            final String s = tk.nextToken();
            Object obj = map.get(s);
            if (tk.hasMoreTokens()) {
                if (obj == null) {
                    obj = new LinkedHashMap();
                    map.put(s, obj);
                    map = (HashMap<String, Object>)obj;
                }
                else {
                    if (!(obj instanceof HashMap)) {
                        return;
                    }
                    map = (HashMap<String, Object>)obj;
                }
            }
            else {
                if (obj instanceof HashMap) {
                    return;
                }
                final PdfDictionary merged = item.getMerged(0);
                if (obj == null) {
                    final PdfDictionary field = new PdfDictionary();
                    if (PdfName.SIG.equals(merged.get(PdfName.FT))) {
                        this.hasSignature = true;
                    }
                    for (final Object element : merged.getKeys()) {
                        final PdfName key = (PdfName)element;
                        if (PdfCopy.fieldKeys.contains(key)) {
                            field.put(key, merged.get(key));
                        }
                    }
                    final ArrayList<Object> list = new ArrayList<Object>();
                    list.add(field);
                    this.createWidgets(list, item);
                    map.put(s, list);
                }
                else {
                    final ArrayList<Object> list2 = (ArrayList<Object>)obj;
                    final PdfDictionary field2 = list2.get(0);
                    final PdfName type1 = (PdfName)field2.get(PdfName.FT);
                    final PdfName type2 = (PdfName)merged.get(PdfName.FT);
                    if (type1 == null || !type1.equals(type2)) {
                        return;
                    }
                    int flag1 = 0;
                    final PdfObject f1 = field2.get(PdfName.FF);
                    if (f1 != null && f1.isNumber()) {
                        flag1 = ((PdfNumber)f1).intValue();
                    }
                    int flag2 = 0;
                    final PdfObject f2 = merged.get(PdfName.FF);
                    if (f2 != null && f2.isNumber()) {
                        flag2 = ((PdfNumber)f2).intValue();
                    }
                    if (type1.equals(PdfName.BTN)) {
                        if (((flag1 ^ flag2) & 0x10000) != 0x0) {
                            return;
                        }
                        if ((flag1 & 0x10000) == 0x0 && ((flag1 ^ flag2) & 0x8000) != 0x0) {
                            return;
                        }
                    }
                    else if (type1.equals(PdfName.CH) && ((flag1 ^ flag2) & 0x20000) != 0x0) {
                        return;
                    }
                    this.createWidgets(list2, item);
                }
            }
        }
    }
    
    private void createWidgets(final ArrayList<Object> list, final AcroFields.Item item) {
        for (int k = 0; k < item.size(); ++k) {
            list.add(item.getPage(k));
            final PdfDictionary merged = item.getMerged(k);
            final PdfObject dr = merged.get(PdfName.DR);
            if (dr != null) {
                PdfFormField.mergeResources(this.resources, (PdfDictionary)PdfReader.getPdfObject(dr));
            }
            final PdfDictionary widget = new PdfDictionary();
            for (final Object element : merged.getKeys()) {
                final PdfName key = (PdfName)element;
                if (PdfCopy.widgetKeys.contains(key)) {
                    widget.put(key, merged.get(key));
                }
            }
            widget.put(PdfCopy.iTextTag, new PdfNumber(item.getTabOrder(k) + 1));
            list.add(widget);
        }
    }
    
    private PdfObject propagate(PdfObject obj) throws IOException {
        if (obj == null) {
            return new PdfNull();
        }
        if (obj.isArray()) {
            final PdfArray a = (PdfArray)obj;
            for (int i = 0; i < a.size(); ++i) {
                a.set(i, this.propagate(a.getPdfObject(i)));
            }
            return a;
        }
        if (obj.isDictionary() || obj.isStream()) {
            final PdfDictionary d = (PdfDictionary)obj;
            for (final PdfName key : d.getKeys()) {
                d.put(key, this.propagate(d.get(key)));
            }
            return d;
        }
        if (obj.isIndirect()) {
            obj = PdfReader.getPdfObject(obj);
            return this.addToBody(this.propagate(obj)).getIndirectReference();
        }
        return obj;
    }
    
    private void createAcroForms() throws IOException, BadPdfFormatException {
        if (this.fieldTree.isEmpty()) {
            for (final ImportedPage importedPage : this.importedPages) {
                if (importedPage.mergedFields.size() > 0) {
                    this.addToBody(importedPage.mergedFields, importedPage.annotsIndirectReference);
                }
            }
            return;
        }
        final PdfDictionary form = new PdfDictionary();
        form.put(PdfName.DR, this.propagate(this.resources));
        if (this.needAppearances) {
            form.put(PdfName.NEEDAPPEARANCES, PdfBoolean.PDFTRUE);
        }
        form.put(PdfName.DA, new PdfString("/Helv 0 Tf 0 g "));
        this.tabOrder = new HashMap<PdfArray, ArrayList<Integer>>();
        this.calculationOrderRefs = new ArrayList<Object>(this.calculationOrder);
        form.put(PdfName.FIELDS, this.branchForm(this.fieldTree, null, ""));
        if (this.hasSignature) {
            form.put(PdfName.SIGFLAGS, new PdfNumber(3));
        }
        final PdfArray co = new PdfArray();
        for (int k = 0; k < this.calculationOrderRefs.size(); ++k) {
            final Object obj = this.calculationOrderRefs.get(k);
            if (obj instanceof PdfIndirectReference) {
                co.add((PdfObject)obj);
            }
        }
        if (co.size() > 0) {
            form.put(PdfName.CO, co);
        }
        this.acroForm = this.addToBody(form).getIndirectReference();
        for (final ImportedPage importedPage2 : this.importedPages) {
            this.addToBody(importedPage2.mergedFields, importedPage2.annotsIndirectReference);
        }
    }
    
    private void updateReferences(final PdfObject obj) {
        if (obj.isDictionary() || obj.isStream()) {
            final PdfDictionary dictionary = (PdfDictionary)obj;
            for (final PdfName key : dictionary.getKeys()) {
                final PdfObject o = dictionary.get(key);
                if (o.isIndirect()) {
                    final PdfReader reader = ((PRIndirectReference)o).getReader();
                    final HashMap<RefKey, IndirectReferences> indirects = this.indirectMap.get(reader);
                    final IndirectReferences indRef = indirects.get(new RefKey((PRIndirectReference)o));
                    if (indRef == null) {
                        continue;
                    }
                    dictionary.put(key, indRef.getRef());
                }
                else {
                    this.updateReferences(o);
                }
            }
        }
        else if (obj.isArray()) {
            final PdfArray array = (PdfArray)obj;
            for (int i = 0; i < array.size(); ++i) {
                final PdfObject o2 = array.getPdfObject(i);
                if (o2.isIndirect()) {
                    final PdfReader reader2 = ((PRIndirectReference)o2).getReader();
                    final HashMap<RefKey, IndirectReferences> indirects2 = this.indirectMap.get(reader2);
                    final IndirectReferences indRef2 = indirects2.get(new RefKey((PRIndirectReference)o2));
                    if (indRef2 != null) {
                        array.set(i, indRef2.getRef());
                    }
                }
                else {
                    this.updateReferences(o2);
                }
            }
        }
    }
    
    private PdfArray branchForm(final HashMap<String, Object> level, final PdfIndirectReference parent, final String fname) throws IOException, BadPdfFormatException {
        final PdfArray arr = new PdfArray();
        for (final Map.Entry<String, Object> entry : level.entrySet()) {
            final String name = entry.getKey();
            final Object obj = entry.getValue();
            final PdfIndirectReference ind = this.getPdfIndirectReference();
            final PdfDictionary dic = new PdfDictionary();
            if (parent != null) {
                dic.put(PdfName.PARENT, parent);
            }
            dic.put(PdfName.T, new PdfString(name, "UnicodeBig"));
            final String fname2 = fname + "." + name;
            final int coidx = this.calculationOrder.indexOf(fname2);
            if (coidx >= 0) {
                this.calculationOrderRefs.set(coidx, ind);
            }
            if (obj instanceof HashMap) {
                dic.put(PdfName.KIDS, this.branchForm((HashMap<String, Object>)obj, ind, fname2));
                arr.add(ind);
                this.addToBody(dic, ind, true);
            }
            else {
                final ArrayList<Object> list = (ArrayList<Object>)obj;
                dic.mergeDifferent(list.get(0));
                if (list.size() == 3) {
                    dic.mergeDifferent(list.get(2));
                    final int page = list.get(1);
                    final PdfArray annots = this.importedPages.get(page - 1).mergedFields;
                    final PdfNumber nn = (PdfNumber)dic.get(PdfCopy.iTextTag);
                    dic.remove(PdfCopy.iTextTag);
                    dic.put(PdfName.TYPE, PdfName.ANNOT);
                    this.adjustTabOrder(annots, ind, nn);
                }
                else {
                    final PdfDictionary field = list.get(0);
                    final PdfArray kids = new PdfArray();
                    for (int k = 1; k < list.size(); k += 2) {
                        final int page2 = list.get(k);
                        final PdfArray annots2 = this.importedPages.get(page2 - 1).mergedFields;
                        final PdfDictionary widget = new PdfDictionary();
                        widget.merge(list.get(k + 1));
                        widget.put(PdfName.PARENT, ind);
                        final PdfNumber nn2 = (PdfNumber)widget.get(PdfCopy.iTextTag);
                        widget.remove(PdfCopy.iTextTag);
                        if (isTextField(field)) {
                            final PdfString v = field.getAsString(PdfName.V);
                            final PdfObject ap = widget.getDirectObject(PdfName.AP);
                            if (v != null && ap != null) {
                                if (!this.mergedTextFields.containsKey(list)) {
                                    this.mergedTextFields.put(list, v);
                                }
                                else {
                                    try {
                                        final TextField tx = new TextField(this, null, null);
                                        this.fields.get(0).decodeGenericDictionary(widget, tx);
                                        Rectangle box = PdfReader.getNormalizedRectangle(widget.getAsArray(PdfName.RECT));
                                        if (tx.getRotation() == 90 || tx.getRotation() == 270) {
                                            box = box.rotate();
                                        }
                                        tx.setBox(box);
                                        tx.setText(this.mergedTextFields.get(list).toUnicodeString());
                                        final PdfAppearance app = tx.getAppearance();
                                        ((PdfDictionary)ap).put(PdfName.N, app.getIndirectReference());
                                    }
                                    catch (DocumentException ex) {}
                                }
                            }
                        }
                        else if (isCheckButton(field)) {
                            final PdfName v2 = field.getAsName(PdfName.V);
                            final PdfName as = widget.getAsName(PdfName.AS);
                            if (v2 != null && as != null) {
                                widget.put(PdfName.AS, v2);
                            }
                        }
                        else if (isRadioButton(field)) {
                            final PdfName v2 = field.getAsName(PdfName.V);
                            final PdfName as = widget.getAsName(PdfName.AS);
                            if (v2 != null && as != null && !as.equals(this.getOffStateName(widget))) {
                                if (!this.mergedRadioButtons.contains(list)) {
                                    this.mergedRadioButtons.add(list);
                                    widget.put(PdfName.AS, v2);
                                }
                                else {
                                    widget.put(PdfName.AS, this.getOffStateName(widget));
                                }
                            }
                        }
                        widget.put(PdfName.TYPE, PdfName.ANNOT);
                        final PdfIndirectReference wref = this.addToBody(widget, this.getPdfIndirectReference(), true).getIndirectReference();
                        this.adjustTabOrder(annots2, wref, nn2);
                        kids.add(wref);
                    }
                    dic.put(PdfName.KIDS, kids);
                }
                arr.add(ind);
                this.addToBody(dic, ind, true);
            }
        }
        return arr;
    }
    
    private void adjustTabOrder(final PdfArray annots, final PdfIndirectReference ind, final PdfNumber nn) {
        final int v = nn.intValue();
        ArrayList<Integer> t = this.tabOrder.get(annots);
        if (t == null) {
            t = new ArrayList<Integer>();
            for (int size = annots.size() - 1, k = 0; k < size; ++k) {
                t.add(PdfCopy.zero);
            }
            t.add(v);
            this.tabOrder.put(annots, t);
            annots.add(ind);
        }
        else {
            int size;
            int k;
            for (size = (k = t.size() - 1); k >= 0; --k) {
                if (t.get(k) <= v) {
                    t.add(k + 1, v);
                    annots.add(k + 1, ind);
                    size = -2;
                    break;
                }
            }
            if (size != -2) {
                t.add(0, v);
                annots.add(0, ind);
            }
        }
    }
    
    @Override
    protected PdfDictionary getCatalog(final PdfIndirectReference rootObj) {
        try {
            final PdfDictionary theCat = this.pdf.getCatalog(rootObj);
            this.buildStructTreeRootForTagged(theCat);
            if (this.fieldArray != null) {
                this.addFieldResources(theCat);
            }
            else if (this.mergeFields && this.acroForm != null) {
                theCat.put(PdfName.ACROFORM, this.acroForm);
            }
            return theCat;
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }
    
    protected boolean isStructTreeRootReference(final PdfIndirectReference prRef) {
        return prRef != null && this.structTreeRootReference != null && prRef.number == this.structTreeRootReference.number && prRef.generation == this.structTreeRootReference.generation;
    }
    
    private void addFieldResources(final PdfDictionary catalog) throws IOException {
        if (this.fieldArray == null) {
            return;
        }
        final PdfDictionary acroForm = new PdfDictionary();
        catalog.put(PdfName.ACROFORM, acroForm);
        acroForm.put(PdfName.FIELDS, this.fieldArray);
        acroForm.put(PdfName.DA, new PdfString("/Helv 0 Tf 0 g "));
        if (this.fieldTemplates.isEmpty()) {
            return;
        }
        final PdfDictionary dr = new PdfDictionary();
        acroForm.put(PdfName.DR, dr);
        for (final PdfTemplate template : this.fieldTemplates) {
            PdfFormField.mergeResources(dr, (PdfDictionary)template.getResources());
        }
        PdfDictionary fonts = dr.getAsDict(PdfName.FONT);
        if (fonts == null) {
            fonts = new PdfDictionary();
            dr.put(PdfName.FONT, fonts);
        }
        if (!fonts.contains(PdfName.HELV)) {
            final PdfDictionary dic = new PdfDictionary(PdfName.FONT);
            dic.put(PdfName.BASEFONT, PdfName.HELVETICA);
            dic.put(PdfName.ENCODING, PdfName.WIN_ANSI_ENCODING);
            dic.put(PdfName.NAME, PdfName.HELV);
            dic.put(PdfName.SUBTYPE, PdfName.TYPE1);
            fonts.put(PdfName.HELV, this.addToBody(dic).getIndirectReference());
        }
        if (!fonts.contains(PdfName.ZADB)) {
            final PdfDictionary dic = new PdfDictionary(PdfName.FONT);
            dic.put(PdfName.BASEFONT, PdfName.ZAPFDINGBATS);
            dic.put(PdfName.NAME, PdfName.ZADB);
            dic.put(PdfName.SUBTYPE, PdfName.TYPE1);
            fonts.put(PdfName.ZADB, this.addToBody(dic).getIndirectReference());
        }
    }
    
    @Override
    public void close() {
        if (this.open) {
            this.pdf.close();
            super.close();
        }
    }
    
    public PdfIndirectReference add(final PdfOutline outline) {
        return null;
    }
    
    @Override
    public void addAnnotation(final PdfAnnotation annot) {
    }
    
    @Override
    PdfIndirectReference add(final PdfPage page, final PdfContents contents) throws PdfException {
        return null;
    }
    
    @Override
    public void freeReader(final PdfReader reader) throws IOException {
        if (this.mergeFields) {
            throw new UnsupportedOperationException(MessageLocalization.getComposedMessage("it.is.not.possible.to.free.reader.in.merge.fields.mode", new Object[0]));
        }
        final PdfArray array = reader.trailer.getAsArray(PdfName.ID);
        if (array != null) {
            this.originalFileID = array.getAsString(0).getBytes();
        }
        this.indirectMap.remove(reader);
        this.currentPdfReaderInstance = null;
        super.freeReader(reader);
    }
    
    protected PdfName getOffStateName(final PdfDictionary widget) {
        return PdfName.Off;
    }
    
    static Integer getFlags(final PdfDictionary field) {
        final PdfName type = field.getAsName(PdfName.FT);
        if (!PdfName.BTN.equals(type)) {
            return null;
        }
        final PdfNumber flags = field.getAsNumber(PdfName.FF);
        if (flags == null) {
            return null;
        }
        return flags.intValue();
    }
    
    static boolean isCheckButton(final PdfDictionary field) {
        final Integer flags = getFlags(field);
        return flags == null || ((flags & 0x10000) == 0x0 && (flags & 0x8000) == 0x0);
    }
    
    static boolean isRadioButton(final PdfDictionary field) {
        final Integer flags = getFlags(field);
        return flags != null && (flags & 0x10000) == 0x0 && (flags & 0x8000) != 0x0;
    }
    
    static boolean isTextField(final PdfDictionary field) {
        final PdfName type = field.getAsName(PdfName.FT);
        return PdfName.TX.equals(type);
    }
    
    public PageStamp createPageStamp(final PdfImportedPage iPage) {
        final int pageNum = iPage.getPageNumber();
        final PdfReader reader = iPage.getPdfReaderInstance().getReader();
        if (this.isTagged()) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("creating.page.stamp.not.allowed.for.tagged.reader", new Object[0]));
        }
        final PdfDictionary pageN = reader.getPageN(pageNum);
        return new PageStamp(reader, pageN, this);
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(PdfCopy.class);
        PdfCopy.COUNTER = CounterFactory.getCounter(PdfCopy.class);
        annotId = new PdfName("iTextAnnotId");
        PdfCopy.annotIdCnt = 0;
        iTextTag = new PdfName("_iTextTag_");
        zero = 0;
        widgetKeys = new HashSet<PdfName>();
        fieldKeys = new HashSet<PdfName>();
        PdfCopy.widgetKeys.add(PdfName.SUBTYPE);
        PdfCopy.widgetKeys.add(PdfName.CONTENTS);
        PdfCopy.widgetKeys.add(PdfName.RECT);
        PdfCopy.widgetKeys.add(PdfName.NM);
        PdfCopy.widgetKeys.add(PdfName.M);
        PdfCopy.widgetKeys.add(PdfName.F);
        PdfCopy.widgetKeys.add(PdfName.BS);
        PdfCopy.widgetKeys.add(PdfName.BORDER);
        PdfCopy.widgetKeys.add(PdfName.AP);
        PdfCopy.widgetKeys.add(PdfName.AS);
        PdfCopy.widgetKeys.add(PdfName.C);
        PdfCopy.widgetKeys.add(PdfName.A);
        PdfCopy.widgetKeys.add(PdfName.STRUCTPARENT);
        PdfCopy.widgetKeys.add(PdfName.OC);
        PdfCopy.widgetKeys.add(PdfName.H);
        PdfCopy.widgetKeys.add(PdfName.MK);
        PdfCopy.widgetKeys.add(PdfName.DA);
        PdfCopy.widgetKeys.add(PdfName.Q);
        PdfCopy.widgetKeys.add(PdfName.P);
        PdfCopy.widgetKeys.add(PdfName.TYPE);
        PdfCopy.widgetKeys.add(PdfCopy.annotId);
        PdfCopy.fieldKeys.add(PdfName.AA);
        PdfCopy.fieldKeys.add(PdfName.FT);
        PdfCopy.fieldKeys.add(PdfName.TU);
        PdfCopy.fieldKeys.add(PdfName.TM);
        PdfCopy.fieldKeys.add(PdfName.FF);
        PdfCopy.fieldKeys.add(PdfName.V);
        PdfCopy.fieldKeys.add(PdfName.DV);
        PdfCopy.fieldKeys.add(PdfName.DS);
        PdfCopy.fieldKeys.add(PdfName.RV);
        PdfCopy.fieldKeys.add(PdfName.OPT);
        PdfCopy.fieldKeys.add(PdfName.MAXLEN);
        PdfCopy.fieldKeys.add(PdfName.TI);
        PdfCopy.fieldKeys.add(PdfName.I);
        PdfCopy.fieldKeys.add(PdfName.LOCK);
        PdfCopy.fieldKeys.add(PdfName.SV);
    }
    
    static class IndirectReferences
    {
        PdfIndirectReference theRef;
        boolean hasCopied;
        
        IndirectReferences(final PdfIndirectReference ref) {
            this.theRef = ref;
            this.hasCopied = false;
        }
        
        void setCopied() {
            this.hasCopied = true;
        }
        
        void setNotCopied() {
            this.hasCopied = false;
        }
        
        boolean getCopied() {
            return this.hasCopied;
        }
        
        PdfIndirectReference getRef() {
            return this.theRef;
        }
        
        @Override
        public String toString() {
            String ext = "";
            if (this.hasCopied) {
                ext += " Copied";
            }
            return this.getRef() + ext;
        }
    }
    
    protected static class ImportedPage
    {
        int pageNumber;
        PdfReader reader;
        PdfArray mergedFields;
        PdfIndirectReference annotsIndirectReference;
        
        ImportedPage(final PdfReader reader, final int pageNumber, final boolean keepFields) {
            this.pageNumber = pageNumber;
            this.reader = reader;
            if (keepFields) {
                this.mergedFields = new PdfArray();
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            if (!(o instanceof ImportedPage)) {
                return false;
            }
            final ImportedPage other = (ImportedPage)o;
            return this.pageNumber == other.pageNumber && this.reader.equals(other.reader);
        }
        
        @Override
        public String toString() {
            return Integer.toString(this.pageNumber);
        }
    }
    
    public static class PageStamp
    {
        PdfDictionary pageN;
        StampContent under;
        StampContent over;
        PageResources pageResources;
        PdfReader reader;
        PdfCopy cstp;
        
        PageStamp(final PdfReader reader, final PdfDictionary pageN, final PdfCopy cstp) {
            this.pageN = pageN;
            this.reader = reader;
            this.cstp = cstp;
        }
        
        public PdfContentByte getUnderContent() {
            if (this.under == null) {
                if (this.pageResources == null) {
                    this.pageResources = new PageResources();
                    final PdfDictionary resources = this.pageN.getAsDict(PdfName.RESOURCES);
                    this.pageResources.setOriginalResources(resources, this.cstp.namePtr);
                }
                this.under = new StampContent(this.cstp, this.pageResources);
            }
            return this.under;
        }
        
        public PdfContentByte getOverContent() {
            if (this.over == null) {
                if (this.pageResources == null) {
                    this.pageResources = new PageResources();
                    final PdfDictionary resources = this.pageN.getAsDict(PdfName.RESOURCES);
                    this.pageResources.setOriginalResources(resources, this.cstp.namePtr);
                }
                this.over = new StampContent(this.cstp, this.pageResources);
            }
            return this.over;
        }
        
        public void alterContents() throws IOException {
            if (this.over == null && this.under == null) {
                return;
            }
            PdfArray ar = null;
            final PdfObject content = PdfReader.getPdfObject(this.pageN.get(PdfName.CONTENTS), this.pageN);
            if (content == null) {
                ar = new PdfArray();
                this.pageN.put(PdfName.CONTENTS, ar);
            }
            else if (content.isArray()) {
                ar = (PdfArray)content;
            }
            else if (content.isStream()) {
                ar = new PdfArray();
                ar.add(this.pageN.get(PdfName.CONTENTS));
                this.pageN.put(PdfName.CONTENTS, ar);
            }
            else {
                ar = new PdfArray();
                this.pageN.put(PdfName.CONTENTS, ar);
            }
            final ByteBuffer out = new ByteBuffer();
            if (this.under != null) {
                out.append(PdfContents.SAVESTATE);
                this.applyRotation(this.pageN, out);
                out.append(this.under.getInternalBuffer());
                out.append(PdfContents.RESTORESTATE);
            }
            if (this.over != null) {
                out.append(PdfContents.SAVESTATE);
            }
            PdfStream stream = new PdfStream(out.toByteArray());
            stream.flateCompress(this.cstp.getCompressionLevel());
            final PdfIndirectReference ref1 = this.cstp.addToBody(stream).getIndirectReference();
            ar.addFirst(ref1);
            out.reset();
            if (this.over != null) {
                out.append(' ');
                out.append(PdfContents.RESTORESTATE);
                out.append(PdfContents.SAVESTATE);
                this.applyRotation(this.pageN, out);
                out.append(this.over.getInternalBuffer());
                out.append(PdfContents.RESTORESTATE);
                stream = new PdfStream(out.toByteArray());
                stream.flateCompress(this.cstp.getCompressionLevel());
                ar.add(this.cstp.addToBody(stream).getIndirectReference());
            }
            this.pageN.put(PdfName.RESOURCES, this.pageResources.getResources());
        }
        
        void applyRotation(final PdfDictionary pageN, final ByteBuffer out) {
            if (!this.cstp.rotateContents) {
                return;
            }
            final Rectangle page = this.reader.getPageSizeWithRotation(pageN);
            final int rotation = page.getRotation();
            switch (rotation) {
                case 90: {
                    out.append(PdfContents.ROTATE90);
                    out.append(page.getTop());
                    out.append(' ').append('0').append(PdfContents.ROTATEFINAL);
                    break;
                }
                case 180: {
                    out.append(PdfContents.ROTATE180);
                    out.append(page.getRight());
                    out.append(' ');
                    out.append(page.getTop());
                    out.append(PdfContents.ROTATEFINAL);
                    break;
                }
                case 270: {
                    out.append(PdfContents.ROTATE270);
                    out.append('0').append(' ');
                    out.append(page.getRight());
                    out.append(PdfContents.ROTATEFINAL);
                    break;
                }
            }
        }
        
        private void addDocumentField(final PdfIndirectReference ref) {
            if (this.cstp.fieldArray == null) {
                this.cstp.fieldArray = new PdfArray();
            }
            this.cstp.fieldArray.add(ref);
        }
        
        private void expandFields(final PdfFormField field, final ArrayList<PdfAnnotation> allAnnots) {
            allAnnots.add(field);
            final ArrayList<PdfFormField> kids = field.getKids();
            if (kids != null) {
                for (final PdfFormField f : kids) {
                    this.expandFields(f, allAnnots);
                }
            }
        }
        
        public void addAnnotation(PdfAnnotation annot) {
            try {
                final ArrayList<PdfAnnotation> allAnnots = new ArrayList<PdfAnnotation>();
                if (annot.isForm()) {
                    final PdfFormField field = (PdfFormField)annot;
                    if (field.getParent() != null) {
                        return;
                    }
                    this.expandFields(field, allAnnots);
                    if (this.cstp.fieldTemplates == null) {
                        this.cstp.fieldTemplates = new HashSet<PdfTemplate>();
                    }
                }
                else {
                    allAnnots.add(annot);
                }
                for (int k = 0; k < allAnnots.size(); ++k) {
                    annot = allAnnots.get(k);
                    if (annot.isForm()) {
                        if (!annot.isUsed()) {
                            final HashSet<PdfTemplate> templates = annot.getTemplates();
                            if (templates != null) {
                                this.cstp.fieldTemplates.addAll((Collection<?>)templates);
                            }
                        }
                        final PdfFormField field2 = (PdfFormField)annot;
                        if (field2.getParent() == null) {
                            this.addDocumentField(field2.getIndirectReference());
                        }
                    }
                    if (annot.isAnnotation()) {
                        final PdfObject pdfobj = PdfReader.getPdfObject(this.pageN.get(PdfName.ANNOTS), this.pageN);
                        PdfArray annots = null;
                        if (pdfobj == null || !pdfobj.isArray()) {
                            annots = new PdfArray();
                            this.pageN.put(PdfName.ANNOTS, annots);
                        }
                        else {
                            annots = (PdfArray)pdfobj;
                        }
                        annots.add(annot.getIndirectReference());
                        if (!annot.isUsed()) {
                            final PdfRectangle rect = (PdfRectangle)annot.get(PdfName.RECT);
                            if (rect != null && (rect.left() != 0.0f || rect.right() != 0.0f || rect.top() != 0.0f || rect.bottom() != 0.0f)) {
                                final int rotation = this.reader.getPageRotation(this.pageN);
                                final Rectangle pageSize = this.reader.getPageSizeWithRotation(this.pageN);
                                switch (rotation) {
                                    case 90: {
                                        annot.put(PdfName.RECT, new PdfRectangle(pageSize.getTop() - rect.bottom(), rect.left(), pageSize.getTop() - rect.top(), rect.right()));
                                        break;
                                    }
                                    case 180: {
                                        annot.put(PdfName.RECT, new PdfRectangle(pageSize.getRight() - rect.left(), pageSize.getTop() - rect.bottom(), pageSize.getRight() - rect.right(), pageSize.getTop() - rect.top()));
                                        break;
                                    }
                                    case 270: {
                                        annot.put(PdfName.RECT, new PdfRectangle(rect.bottom(), pageSize.getRight() - rect.left(), rect.top(), pageSize.getRight() - rect.right()));
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (!annot.isUsed()) {
                        annot.setUsed();
                        this.cstp.addToBody(annot, annot.getIndirectReference());
                    }
                }
            }
            catch (IOException e) {
                throw new ExceptionConverter(e);
            }
        }
    }
    
    public static class StampContent extends PdfContentByte
    {
        PageResources pageResources;
        
        StampContent(final PdfWriter writer, final PageResources pageResources) {
            super(writer);
            this.pageResources = pageResources;
        }
        
        @Override
        public PdfContentByte getDuplicate() {
            return new StampContent(this.writer, this.pageResources);
        }
        
        @Override
        PageResources getPageResources() {
            return this.pageResources;
        }
    }
}
