// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.error_messages.MessageLocalization;

public class PdfVisibilityExpression extends PdfArray
{
    public static final int OR = 0;
    public static final int AND = 1;
    public static final int NOT = -1;
    
    public PdfVisibilityExpression(final int type) {
        switch (type) {
            case 0: {
                super.add(PdfName.OR);
                break;
            }
            case 1: {
                super.add(PdfName.AND);
                break;
            }
            case -1: {
                super.add(PdfName.NOT);
                break;
            }
            default: {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("illegal.ve.value", new Object[0]));
            }
        }
    }
    
    @Override
    public void add(final int index, final PdfObject element) {
        throw new IllegalArgumentException(MessageLocalization.getComposedMessage("illegal.ve.value", new Object[0]));
    }
    
    @Override
    public boolean add(final PdfObject object) {
        if (object instanceof PdfLayer) {
            return super.add(((PdfLayer)object).getRef());
        }
        if (object instanceof PdfVisibilityExpression) {
            return super.add(object);
        }
        throw new IllegalArgumentException(MessageLocalization.getComposedMessage("illegal.ve.value", new Object[0]));
    }
    
    @Override
    public void addFirst(final PdfObject object) {
        throw new IllegalArgumentException(MessageLocalization.getComposedMessage("illegal.ve.value", new Object[0]));
    }
    
    @Override
    public boolean add(final float[] values) {
        throw new IllegalArgumentException(MessageLocalization.getComposedMessage("illegal.ve.value", new Object[0]));
    }
    
    @Override
    public boolean add(final int[] values) {
        throw new IllegalArgumentException(MessageLocalization.getComposedMessage("illegal.ve.value", new Object[0]));
    }
}
