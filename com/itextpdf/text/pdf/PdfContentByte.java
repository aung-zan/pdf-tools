// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.awt.FontMapper;
import com.itextpdf.awt.PdfPrinterGraphics2D;
import java.awt.print.PrinterJob;
import com.itextpdf.awt.PdfGraphics2D;
import java.awt.Graphics2D;
import java.util.Map;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.AccessibleElementId;
import java.util.Iterator;
import java.io.IOException;
import com.itextpdf.text.pdf.internal.PdfAnnotationsImp;
import com.itextpdf.text.Annotation;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import com.itextpdf.text.ImgJBIG2;
import com.itextpdf.awt.geom.Point2D;
import com.itextpdf.awt.geom.AffineTransform;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.exceptions.IllegalPdfSyntaxException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;
import java.util.HashMap;
import java.util.ArrayList;

public class PdfContentByte
{
    public static final int ALIGN_CENTER = 1;
    public static final int ALIGN_LEFT = 0;
    public static final int ALIGN_RIGHT = 2;
    public static final int LINE_CAP_BUTT = 0;
    public static final int LINE_CAP_ROUND = 1;
    public static final int LINE_CAP_PROJECTING_SQUARE = 2;
    public static final int LINE_JOIN_MITER = 0;
    public static final int LINE_JOIN_ROUND = 1;
    public static final int LINE_JOIN_BEVEL = 2;
    public static final int TEXT_RENDER_MODE_FILL = 0;
    public static final int TEXT_RENDER_MODE_STROKE = 1;
    public static final int TEXT_RENDER_MODE_FILL_STROKE = 2;
    public static final int TEXT_RENDER_MODE_INVISIBLE = 3;
    public static final int TEXT_RENDER_MODE_FILL_CLIP = 4;
    public static final int TEXT_RENDER_MODE_STROKE_CLIP = 5;
    public static final int TEXT_RENDER_MODE_FILL_STROKE_CLIP = 6;
    public static final int TEXT_RENDER_MODE_CLIP = 7;
    private static final float[] unitRect;
    protected ByteBuffer content;
    protected int markedContentSize;
    protected PdfWriter writer;
    protected PdfDocument pdf;
    protected GraphicState state;
    protected ArrayList<GraphicState> stateList;
    protected ArrayList<Integer> layerDepth;
    protected int separator;
    private int mcDepth;
    private boolean inText;
    private boolean suppressTagging;
    private static HashMap<PdfName, String> abrev;
    private ArrayList<IAccessibleElement> mcElements;
    protected PdfContentByte duplicatedFrom;
    
    public PdfContentByte(final PdfWriter wr) {
        this.content = new ByteBuffer();
        this.markedContentSize = 0;
        this.state = new GraphicState();
        this.stateList = new ArrayList<GraphicState>();
        this.separator = 10;
        this.mcDepth = 0;
        this.inText = false;
        this.suppressTagging = false;
        this.mcElements = new ArrayList<IAccessibleElement>();
        this.duplicatedFrom = null;
        if (wr != null) {
            this.writer = wr;
            this.pdf = this.writer.getPdfDocument();
        }
    }
    
    @Override
    public String toString() {
        return this.content.toString();
    }
    
    public boolean isTaggingSuppressed() {
        return this.suppressTagging;
    }
    
    public PdfContentByte setSuppressTagging(final boolean suppressTagging) {
        this.suppressTagging = suppressTagging;
        return this;
    }
    
    public boolean isTagged() {
        return this.writer != null && this.writer.isTagged() && !this.isTaggingSuppressed();
    }
    
    public ByteBuffer getInternalBuffer() {
        return this.content;
    }
    
    public byte[] toPdf(final PdfWriter writer) {
        this.sanityCheck();
        return this.content.toByteArray();
    }
    
    public void add(final PdfContentByte other) {
        if (other.writer != null && this.writer != other.writer) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("inconsistent.writers.are.you.mixing.two.documents", new Object[0]));
        }
        this.content.append(other.content);
        this.markedContentSize += other.markedContentSize;
    }
    
    public float getXTLM() {
        return this.state.xTLM;
    }
    
    public float getYTLM() {
        return this.state.yTLM;
    }
    
    public float getLeading() {
        return this.state.leading;
    }
    
    public float getCharacterSpacing() {
        return this.state.charSpace;
    }
    
    public float getWordSpacing() {
        return this.state.wordSpace;
    }
    
    public float getHorizontalScaling() {
        return this.state.scale;
    }
    
    public void setFlatness(final float flatness) {
        this.setFlatness((double)flatness);
    }
    
    public void setFlatness(final double flatness) {
        if (flatness >= 0.0 && flatness <= 100.0) {
            this.content.append(flatness).append(" i").append_i(this.separator);
        }
    }
    
    public void setLineCap(final int style) {
        if (style >= 0 && style <= 2) {
            this.content.append(style).append(" J").append_i(this.separator);
        }
    }
    
    public void setRenderingIntent(final PdfName ri) {
        this.content.append(ri.getBytes()).append(" ri").append_i(this.separator);
    }
    
    public void setLineDash(final float phase) {
        this.setLineDash((double)phase);
    }
    
    public void setLineDash(final double phase) {
        this.content.append("[] ").append(phase).append(" d").append_i(this.separator);
    }
    
    public void setLineDash(final float unitsOn, final float phase) {
        this.setLineDash(unitsOn, (double)phase);
    }
    
    public void setLineDash(final double unitsOn, final double phase) {
        this.content.append("[").append(unitsOn).append("] ").append(phase).append(" d").append_i(this.separator);
    }
    
    public void setLineDash(final float unitsOn, final float unitsOff, final float phase) {
        this.setLineDash(unitsOn, unitsOff, (double)phase);
    }
    
    public void setLineDash(final double unitsOn, final double unitsOff, final double phase) {
        this.content.append("[").append(unitsOn).append(' ').append(unitsOff).append("] ").append(phase).append(" d").append_i(this.separator);
    }
    
    public final void setLineDash(final float[] array, final float phase) {
        this.content.append("[");
        for (int i = 0; i < array.length; ++i) {
            this.content.append(array[i]);
            if (i < array.length - 1) {
                this.content.append(' ');
            }
        }
        this.content.append("] ").append(phase).append(" d").append_i(this.separator);
    }
    
    public final void setLineDash(final double[] array, final double phase) {
        this.content.append("[");
        for (int i = 0; i < array.length; ++i) {
            this.content.append(array[i]);
            if (i < array.length - 1) {
                this.content.append(' ');
            }
        }
        this.content.append("] ").append(phase).append(" d").append_i(this.separator);
    }
    
    public void setLineJoin(final int style) {
        if (style >= 0 && style <= 2) {
            this.content.append(style).append(" j").append_i(this.separator);
        }
    }
    
    public void setLineWidth(final float w) {
        this.setLineWidth((double)w);
    }
    
    public void setLineWidth(final double w) {
        this.content.append(w).append(" w").append_i(this.separator);
    }
    
    public void setMiterLimit(final float miterLimit) {
        this.setMiterLimit((double)miterLimit);
    }
    
    public void setMiterLimit(final double miterLimit) {
        if (miterLimit > 1.0) {
            this.content.append(miterLimit).append(" M").append_i(this.separator);
        }
    }
    
    public void clip() {
        if (this.inText && this.isTagged()) {
            this.endText();
        }
        this.content.append("W").append_i(this.separator);
    }
    
    public void eoClip() {
        if (this.inText && this.isTagged()) {
            this.endText();
        }
        this.content.append("W*").append_i(this.separator);
    }
    
    public void setGrayFill(final float gray) {
        this.saveColor(new GrayColor(gray), true);
        this.content.append(gray).append(" g").append_i(this.separator);
    }
    
    public void resetGrayFill() {
        this.saveColor(new GrayColor(0), true);
        this.content.append("0 g").append_i(this.separator);
    }
    
    public void setGrayStroke(final float gray) {
        this.saveColor(new GrayColor(gray), false);
        this.content.append(gray).append(" G").append_i(this.separator);
    }
    
    public void resetGrayStroke() {
        this.saveColor(new GrayColor(0), false);
        this.content.append("0 G").append_i(this.separator);
    }
    
    private void HelperRGB(float red, float green, float blue) {
        if (red < 0.0f) {
            red = 0.0f;
        }
        else if (red > 1.0f) {
            red = 1.0f;
        }
        if (green < 0.0f) {
            green = 0.0f;
        }
        else if (green > 1.0f) {
            green = 1.0f;
        }
        if (blue < 0.0f) {
            blue = 0.0f;
        }
        else if (blue > 1.0f) {
            blue = 1.0f;
        }
        this.content.append(red).append(' ').append(green).append(' ').append(blue);
    }
    
    public void setRGBColorFillF(final float red, final float green, final float blue) {
        this.saveColor(new BaseColor(red, green, blue), true);
        this.HelperRGB(red, green, blue);
        this.content.append(" rg").append_i(this.separator);
    }
    
    public void resetRGBColorFill() {
        this.resetGrayFill();
    }
    
    public void setRGBColorStrokeF(final float red, final float green, final float blue) {
        this.saveColor(new BaseColor(red, green, blue), false);
        this.HelperRGB(red, green, blue);
        this.content.append(" RG").append_i(this.separator);
    }
    
    public void resetRGBColorStroke() {
        this.resetGrayStroke();
    }
    
    private void HelperCMYK(float cyan, float magenta, float yellow, float black) {
        if (cyan < 0.0f) {
            cyan = 0.0f;
        }
        else if (cyan > 1.0f) {
            cyan = 1.0f;
        }
        if (magenta < 0.0f) {
            magenta = 0.0f;
        }
        else if (magenta > 1.0f) {
            magenta = 1.0f;
        }
        if (yellow < 0.0f) {
            yellow = 0.0f;
        }
        else if (yellow > 1.0f) {
            yellow = 1.0f;
        }
        if (black < 0.0f) {
            black = 0.0f;
        }
        else if (black > 1.0f) {
            black = 1.0f;
        }
        this.content.append(cyan).append(' ').append(magenta).append(' ').append(yellow).append(' ').append(black);
    }
    
    public void setCMYKColorFillF(final float cyan, final float magenta, final float yellow, final float black) {
        this.saveColor(new CMYKColor(cyan, magenta, yellow, black), true);
        this.HelperCMYK(cyan, magenta, yellow, black);
        this.content.append(" k").append_i(this.separator);
    }
    
    public void resetCMYKColorFill() {
        this.saveColor(new CMYKColor(0, 0, 0, 1), true);
        this.content.append("0 0 0 1 k").append_i(this.separator);
    }
    
    public void setCMYKColorStrokeF(final float cyan, final float magenta, final float yellow, final float black) {
        this.saveColor(new CMYKColor(cyan, magenta, yellow, black), false);
        this.HelperCMYK(cyan, magenta, yellow, black);
        this.content.append(" K").append_i(this.separator);
    }
    
    public void resetCMYKColorStroke() {
        this.saveColor(new CMYKColor(0, 0, 0, 1), false);
        this.content.append("0 0 0 1 K").append_i(this.separator);
    }
    
    public void moveTo(final float x, final float y) {
        this.moveTo(x, (double)y);
    }
    
    public void moveTo(final double x, final double y) {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        this.content.append(x).append(' ').append(y).append(" m").append_i(this.separator);
    }
    
    public void lineTo(final float x, final float y) {
        this.lineTo(x, (double)y);
    }
    
    public void lineTo(final double x, final double y) {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        this.content.append(x).append(' ').append(y).append(" l").append_i(this.separator);
    }
    
    public void curveTo(final float x1, final float y1, final float x2, final float y2, final float x3, final float y3) {
        this.curveTo(x1, y1, x2, y2, x3, (double)y3);
    }
    
    public void curveTo(final double x1, final double y1, final double x2, final double y2, final double x3, final double y3) {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        this.content.append(x1).append(' ').append(y1).append(' ').append(x2).append(' ').append(y2).append(' ').append(x3).append(' ').append(y3).append(" c").append_i(this.separator);
    }
    
    public void curveTo(final float x2, final float y2, final float x3, final float y3) {
        this.curveTo(x2, y2, x3, (double)y3);
    }
    
    public void curveTo(final double x2, final double y2, final double x3, final double y3) {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        this.content.append(x2).append(' ').append(y2).append(' ').append(x3).append(' ').append(y3).append(" v").append_i(this.separator);
    }
    
    public void curveFromTo(final float x1, final float y1, final float x3, final float y3) {
        this.curveFromTo(x1, y1, x3, (double)y3);
    }
    
    public void curveFromTo(final double x1, final double y1, final double x3, final double y3) {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        this.content.append(x1).append(' ').append(y1).append(' ').append(x3).append(' ').append(y3).append(" y").append_i(this.separator);
    }
    
    public void circle(final float x, final float y, final float r) {
        this.circle(x, y, (double)r);
    }
    
    public void circle(final double x, final double y, final double r) {
        final float b = 0.5523f;
        this.moveTo(x + r, y);
        this.curveTo(x + r, y + r * b, x + r * b, y + r, x, y + r);
        this.curveTo(x - r * b, y + r, x - r, y + r * b, x - r, y);
        this.curveTo(x - r, y - r * b, x - r * b, y - r, x, y - r);
        this.curveTo(x + r * b, y - r, x + r, y - r * b, x + r, y);
    }
    
    public void rectangle(final float x, final float y, final float w, final float h) {
        this.rectangle(x, y, w, (double)h);
    }
    
    public void rectangle(final double x, final double y, final double w, final double h) {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        this.content.append(x).append(' ').append(y).append(' ').append(w).append(' ').append(h).append(" re").append_i(this.separator);
    }
    
    private boolean compareColors(final BaseColor c1, final BaseColor c2) {
        if (c1 == null && c2 == null) {
            return true;
        }
        if (c1 == null || c2 == null) {
            return false;
        }
        if (c1 instanceof ExtendedColor) {
            return c1.equals(c2);
        }
        return c2.equals(c1);
    }
    
    public void variableRectangle(final Rectangle rect) {
        final float t = rect.getTop();
        final float b = rect.getBottom();
        final float r = rect.getRight();
        final float l = rect.getLeft();
        final float wt = rect.getBorderWidthTop();
        final float wb = rect.getBorderWidthBottom();
        final float wr = rect.getBorderWidthRight();
        final float wl = rect.getBorderWidthLeft();
        final BaseColor ct = rect.getBorderColorTop();
        final BaseColor cb = rect.getBorderColorBottom();
        final BaseColor cr = rect.getBorderColorRight();
        final BaseColor cl = rect.getBorderColorLeft();
        this.saveState();
        this.setLineCap(0);
        this.setLineJoin(0);
        float clw = 0.0f;
        boolean cdef = false;
        BaseColor ccol = null;
        boolean cdefi = false;
        BaseColor cfil = null;
        if (wt > 0.0f) {
            this.setLineWidth(clw = wt);
            cdef = true;
            if (ct == null) {
                this.resetRGBColorStroke();
            }
            else {
                this.setColorStroke(ct);
            }
            ccol = ct;
            this.moveTo(l, t - wt / 2.0f);
            this.lineTo(r, t - wt / 2.0f);
            this.stroke();
        }
        if (wb > 0.0f) {
            if (wb != clw) {
                this.setLineWidth(clw = wb);
            }
            if (!cdef || !this.compareColors(ccol, cb)) {
                cdef = true;
                if (cb == null) {
                    this.resetRGBColorStroke();
                }
                else {
                    this.setColorStroke(cb);
                }
                ccol = cb;
            }
            this.moveTo(r, b + wb / 2.0f);
            this.lineTo(l, b + wb / 2.0f);
            this.stroke();
        }
        if (wr > 0.0f) {
            if (wr != clw) {
                this.setLineWidth(clw = wr);
            }
            if (!cdef || !this.compareColors(ccol, cr)) {
                cdef = true;
                if (cr == null) {
                    this.resetRGBColorStroke();
                }
                else {
                    this.setColorStroke(cr);
                }
                ccol = cr;
            }
            final boolean bt = this.compareColors(ct, cr);
            final boolean bb = this.compareColors(cb, cr);
            this.moveTo(r - wr / 2.0f, bt ? t : (t - wt));
            this.lineTo(r - wr / 2.0f, bb ? b : (b + wb));
            this.stroke();
            if (!bt || !bb) {
                cdefi = true;
                if (cr == null) {
                    this.resetRGBColorFill();
                }
                else {
                    this.setColorFill(cr);
                }
                cfil = cr;
                if (!bt) {
                    this.moveTo(r, t);
                    this.lineTo(r, t - wt);
                    this.lineTo(r - wr, t - wt);
                    this.fill();
                }
                if (!bb) {
                    this.moveTo(r, b);
                    this.lineTo(r, b + wb);
                    this.lineTo(r - wr, b + wb);
                    this.fill();
                }
            }
        }
        if (wl > 0.0f) {
            if (wl != clw) {
                this.setLineWidth(wl);
            }
            if (!cdef || !this.compareColors(ccol, cl)) {
                if (cl == null) {
                    this.resetRGBColorStroke();
                }
                else {
                    this.setColorStroke(cl);
                }
            }
            final boolean bt = this.compareColors(ct, cl);
            final boolean bb = this.compareColors(cb, cl);
            this.moveTo(l + wl / 2.0f, bt ? t : (t - wt));
            this.lineTo(l + wl / 2.0f, bb ? b : (b + wb));
            this.stroke();
            if (!bt || !bb) {
                if (!cdefi || !this.compareColors(cfil, cl)) {
                    if (cl == null) {
                        this.resetRGBColorFill();
                    }
                    else {
                        this.setColorFill(cl);
                    }
                }
                if (!bt) {
                    this.moveTo(l, t);
                    this.lineTo(l, t - wt);
                    this.lineTo(l + wl, t - wt);
                    this.fill();
                }
                if (!bb) {
                    this.moveTo(l, b);
                    this.lineTo(l, b + wb);
                    this.lineTo(l + wl, b + wb);
                    this.fill();
                }
            }
        }
        this.restoreState();
    }
    
    public void rectangle(final Rectangle rectangle) {
        final float x1 = rectangle.getLeft();
        final float y1 = rectangle.getBottom();
        final float x2 = rectangle.getRight();
        final float y2 = rectangle.getTop();
        final BaseColor background = rectangle.getBackgroundColor();
        if (background != null) {
            this.saveState();
            this.setColorFill(background);
            this.rectangle(x1, y1, x2 - x1, y2 - y1);
            this.fill();
            this.restoreState();
        }
        if (!rectangle.hasBorders()) {
            return;
        }
        if (rectangle.isUseVariableBorders()) {
            this.variableRectangle(rectangle);
        }
        else {
            if (rectangle.getBorderWidth() != -1.0f) {
                this.setLineWidth(rectangle.getBorderWidth());
            }
            final BaseColor color = rectangle.getBorderColor();
            if (color != null) {
                this.setColorStroke(color);
            }
            if (rectangle.hasBorder(15)) {
                this.rectangle(x1, y1, x2 - x1, y2 - y1);
            }
            else {
                if (rectangle.hasBorder(8)) {
                    this.moveTo(x2, y1);
                    this.lineTo(x2, y2);
                }
                if (rectangle.hasBorder(4)) {
                    this.moveTo(x1, y1);
                    this.lineTo(x1, y2);
                }
                if (rectangle.hasBorder(2)) {
                    this.moveTo(x1, y1);
                    this.lineTo(x2, y1);
                }
                if (rectangle.hasBorder(1)) {
                    this.moveTo(x1, y2);
                    this.lineTo(x2, y2);
                }
            }
            this.stroke();
            if (color != null) {
                this.resetRGBColorStroke();
            }
        }
    }
    
    public void closePath() {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        this.content.append("h").append_i(this.separator);
    }
    
    public void newPath() {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        this.content.append("n").append_i(this.separator);
    }
    
    public void stroke() {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorStroke);
        PdfWriter.checkPdfIsoConformance(this.writer, 6, this.state.extGState);
        this.content.append("S").append_i(this.separator);
    }
    
    public void closePathStroke() {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorStroke);
        PdfWriter.checkPdfIsoConformance(this.writer, 6, this.state.extGState);
        this.content.append("s").append_i(this.separator);
    }
    
    public void fill() {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorFill);
        PdfWriter.checkPdfIsoConformance(this.writer, 6, this.state.extGState);
        this.content.append("f").append_i(this.separator);
    }
    
    public void eoFill() {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorFill);
        PdfWriter.checkPdfIsoConformance(this.writer, 6, this.state.extGState);
        this.content.append("f*").append_i(this.separator);
    }
    
    public void fillStroke() {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorFill);
        PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorStroke);
        PdfWriter.checkPdfIsoConformance(this.writer, 6, this.state.extGState);
        this.content.append("B").append_i(this.separator);
    }
    
    public void closePathFillStroke() {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorFill);
        PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorStroke);
        PdfWriter.checkPdfIsoConformance(this.writer, 6, this.state.extGState);
        this.content.append("b").append_i(this.separator);
    }
    
    public void eoFillStroke() {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorFill);
        PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorStroke);
        PdfWriter.checkPdfIsoConformance(this.writer, 6, this.state.extGState);
        this.content.append("B*").append_i(this.separator);
    }
    
    public void closePathEoFillStroke() {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("path.construction.operator.inside.text.object", new Object[0]));
            }
            this.endText();
        }
        PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorFill);
        PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorStroke);
        PdfWriter.checkPdfIsoConformance(this.writer, 6, this.state.extGState);
        this.content.append("b*").append_i(this.separator);
    }
    
    public void addImage(final Image image) throws DocumentException {
        this.addImage(image, false);
    }
    
    public void addImage(final Image image, final boolean inlineImage) throws DocumentException {
        if (!image.hasAbsoluteY()) {
            throw new DocumentException(MessageLocalization.getComposedMessage("the.image.must.have.absolute.positioning", new Object[0]));
        }
        final float[] matrix = image.matrix();
        matrix[4] = image.getAbsoluteX() - matrix[4];
        matrix[5] = image.getAbsoluteY() - matrix[5];
        this.addImage(image, matrix[0], matrix[1], matrix[2], matrix[3], matrix[4], matrix[5], inlineImage);
    }
    
    public void addImage(final Image image, final float a, final float b, final float c, final float d, final float e, final float f) throws DocumentException {
        this.addImage(image, a, b, c, d, e, f, false);
    }
    
    public void addImage(final Image image, final double a, final double b, final double c, final double d, final double e, final double f) throws DocumentException {
        this.addImage(image, a, b, c, d, e, f, false);
    }
    
    public void addImage(final Image image, final AffineTransform transform) throws DocumentException {
        final double[] matrix = new double[6];
        transform.getMatrix(matrix);
        this.addImage(image, matrix[0], matrix[1], matrix[2], matrix[3], matrix[4], matrix[5], false);
    }
    
    public void addImage(final Image image, final float a, final float b, final float c, final float d, final float e, final float f, final boolean inlineImage) throws DocumentException {
        this.addImage(image, a, b, c, d, e, (double)f, inlineImage);
    }
    
    public void addImage(final Image image, final double a, final double b, final double c, final double d, final double e, final double f, final boolean inlineImage) throws DocumentException {
        this.addImage(image, a, b, c, d, e, f, inlineImage, false);
    }
    
    protected void addImage(final Image image, final double a, final double b, final double c, final double d, final double e, final double f, final boolean inlineImage, final boolean isMCBlockOpened) throws DocumentException {
        try {
            final AffineTransform transform = new AffineTransform(a, b, c, d, e, f);
            if (image.getLayer() != null) {
                this.beginLayer(image.getLayer());
            }
            if (this.isTagged()) {
                if (this.inText) {
                    this.endText();
                }
                final Point2D[] src = { new Point2D.Float(0.0f, 0.0f), new Point2D.Float(1.0f, 0.0f), new Point2D.Float(1.0f, 1.0f), new Point2D.Float(0.0f, 1.0f) };
                final Point2D[] dst = new Point2D.Float[4];
                transform.transform(src, 0, dst, 0, 4);
                float left = Float.MAX_VALUE;
                float right = -3.4028235E38f;
                float bottom = Float.MAX_VALUE;
                float top = -3.4028235E38f;
                for (int i = 0; i < 4; ++i) {
                    if (dst[i].getX() < left) {
                        left = (float)dst[i].getX();
                    }
                    if (dst[i].getX() > right) {
                        right = (float)dst[i].getX();
                    }
                    if (dst[i].getY() < bottom) {
                        bottom = (float)dst[i].getY();
                    }
                    if (dst[i].getY() > top) {
                        top = (float)dst[i].getY();
                    }
                }
                image.setAccessibleAttribute(PdfName.BBOX, new PdfArray(new float[] { left, bottom, right, top }));
            }
            if (this.writer != null && image.isImgTemplate()) {
                this.writer.addDirectImageSimple(image);
                final PdfTemplate template = image.getTemplateData();
                if (image.getAccessibleAttributes() != null) {
                    for (final PdfName key : image.getAccessibleAttributes().keySet()) {
                        template.setAccessibleAttribute(key, image.getAccessibleAttribute(key));
                    }
                }
                final float w = template.getWidth();
                final float h = template.getHeight();
                this.addTemplate(template, a / w, b / w, c / h, d / h, e, f, false, false);
            }
            else {
                this.content.append("q ");
                if (!transform.isIdentity()) {
                    this.content.append(a).append(' ');
                    this.content.append(b).append(' ');
                    this.content.append(c).append(' ');
                    this.content.append(d).append(' ');
                    this.content.append(e).append(' ');
                    this.content.append(f).append(" cm");
                }
                if (inlineImage) {
                    this.content.append("\nBI\n");
                    final PdfImage pimage = new PdfImage(image, "", null);
                    if (image instanceof ImgJBIG2) {
                        final byte[] globals = ((ImgJBIG2)image).getGlobalBytes();
                        if (globals != null) {
                            final PdfDictionary decodeparms = new PdfDictionary();
                            decodeparms.put(PdfName.JBIG2GLOBALS, this.writer.getReferenceJBIG2Globals(globals));
                            pimage.put(PdfName.DECODEPARMS, decodeparms);
                        }
                    }
                    PdfWriter.checkPdfIsoConformance(this.writer, 17, pimage);
                    for (final Object element : pimage.getKeys()) {
                        final PdfName key2 = (PdfName)element;
                        PdfObject value = pimage.get(key2);
                        final String s = PdfContentByte.abrev.get(key2);
                        if (s == null) {
                            continue;
                        }
                        this.content.append(s);
                        boolean check = true;
                        if (key2.equals(PdfName.COLORSPACE) && value.isArray()) {
                            final PdfArray ar = (PdfArray)value;
                            if (ar.size() == 4 && PdfName.INDEXED.equals(ar.getAsName(0)) && ar.getPdfObject(1).isName() && ar.getPdfObject(2).isNumber() && ar.getPdfObject(3).isString()) {
                                check = false;
                            }
                        }
                        if (check && key2.equals(PdfName.COLORSPACE) && !value.isName()) {
                            final PdfName cs = this.writer.getColorspaceName();
                            final PageResources prs = this.getPageResources();
                            prs.addColor(cs, this.writer.addToBody(value).getIndirectReference());
                            value = cs;
                        }
                        value.toPdf(null, this.content);
                        this.content.append('\n');
                    }
                    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    pimage.writeContent(baos);
                    final byte[] imageBytes = baos.toByteArray();
                    this.content.append(String.format("/L %s\n", imageBytes.length));
                    this.content.append("ID\n");
                    this.content.append(imageBytes);
                    this.content.append("\nEI\nQ").append_i(this.separator);
                }
                else {
                    final PageResources prs2 = this.getPageResources();
                    final Image maskImage = image.getImageMask();
                    if (maskImage != null) {
                        final PdfName name = this.writer.addDirectImageSimple(maskImage);
                        prs2.addXObject(name, this.writer.getImageReference(name));
                    }
                    PdfName name = this.writer.addDirectImageSimple(image);
                    name = prs2.addXObject(name, this.writer.getImageReference(name));
                    this.content.append(' ').append(name.getBytes()).append(" Do Q").append_i(this.separator);
                }
            }
            if (image.hasBorders()) {
                this.saveState();
                final float w2 = image.getWidth();
                final float h2 = image.getHeight();
                this.concatCTM(a / w2, b / w2, c / h2, d / h2, e, f);
                this.rectangle(image);
                this.restoreState();
            }
            if (image.getLayer() != null) {
                this.endLayer();
            }
            Annotation annot = image.getAnnotation();
            if (annot == null) {
                return;
            }
            final double[] r = new double[PdfContentByte.unitRect.length];
            for (int k = 0; k < PdfContentByte.unitRect.length; k += 2) {
                r[k] = a * PdfContentByte.unitRect[k] + c * PdfContentByte.unitRect[k + 1] + e;
                r[k + 1] = b * PdfContentByte.unitRect[k] + d * PdfContentByte.unitRect[k + 1] + f;
            }
            double llx = r[0];
            double lly = r[1];
            double urx = llx;
            double ury = lly;
            for (int j = 2; j < r.length; j += 2) {
                llx = Math.min(llx, r[j]);
                lly = Math.min(lly, r[j + 1]);
                urx = Math.max(urx, r[j]);
                ury = Math.max(ury, r[j + 1]);
            }
            annot = new Annotation(annot);
            annot.setDimensions((float)llx, (float)lly, (float)urx, (float)ury);
            final PdfAnnotation an = PdfAnnotationsImp.convertAnnotation(this.writer, annot, new Rectangle((float)llx, (float)lly, (float)urx, (float)ury));
            if (an == null) {
                return;
            }
            this.addAnnotation(an);
        }
        catch (IOException ioe) {
            final String path = (image != null && image.getUrl() != null) ? image.getUrl().getPath() : MessageLocalization.getComposedMessage("unknown", new Object[0]);
            throw new DocumentException(MessageLocalization.getComposedMessage("add.image.exception", path), ioe);
        }
    }
    
    public void reset() {
        this.reset(true);
    }
    
    public void reset(final boolean validateContent) {
        this.content.reset();
        this.markedContentSize = 0;
        if (validateContent) {
            this.sanityCheck();
        }
        this.state = new GraphicState();
        this.stateList = new ArrayList<GraphicState>();
    }
    
    protected void beginText(final boolean restoreTM) {
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("unbalanced.begin.end.text.operators", new Object[0]));
            }
        }
        else {
            this.inText = true;
            this.content.append("BT").append_i(this.separator);
            if (restoreTM) {
                final float xTLM = this.state.xTLM;
                final float tx = this.state.tx;
                this.setTextMatrix(this.state.aTLM, this.state.bTLM, this.state.cTLM, this.state.dTLM, this.state.tx, this.state.yTLM);
                this.state.xTLM = xTLM;
                this.state.tx = tx;
            }
            else {
                this.state.xTLM = 0.0f;
                this.state.yTLM = 0.0f;
                this.state.tx = 0.0f;
            }
        }
    }
    
    public void beginText() {
        this.beginText(false);
    }
    
    public void endText() {
        if (!this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("unbalanced.begin.end.text.operators", new Object[0]));
            }
        }
        else {
            this.inText = false;
            this.content.append("ET").append_i(this.separator);
        }
    }
    
    public void saveState() {
        PdfWriter.checkPdfIsoConformance(this.writer, 12, "q");
        if (this.inText && this.isTagged()) {
            this.endText();
        }
        this.content.append("q").append_i(this.separator);
        this.stateList.add(new GraphicState(this.state));
    }
    
    public void restoreState() {
        PdfWriter.checkPdfIsoConformance(this.writer, 12, "Q");
        if (this.inText && this.isTagged()) {
            this.endText();
        }
        this.content.append("Q").append_i(this.separator);
        final int idx = this.stateList.size() - 1;
        if (idx < 0) {
            throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("unbalanced.save.restore.state.operators", new Object[0]));
        }
        this.state.restore(this.stateList.get(idx));
        this.stateList.remove(idx);
    }
    
    public void setCharacterSpacing(final float charSpace) {
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        this.state.charSpace = charSpace;
        this.content.append(charSpace).append(" Tc").append_i(this.separator);
    }
    
    public void setWordSpacing(final float wordSpace) {
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        this.state.wordSpace = wordSpace;
        this.content.append(wordSpace).append(" Tw").append_i(this.separator);
    }
    
    public void setHorizontalScaling(final float scale) {
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        this.state.scale = scale;
        this.content.append(scale).append(" Tz").append_i(this.separator);
    }
    
    public void setLeading(final float leading) {
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        this.state.leading = leading;
        this.content.append(leading).append(" TL").append_i(this.separator);
    }
    
    public void setFontAndSize(final BaseFont bf, final float size) {
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        this.checkWriter();
        if (size < 1.0E-4f && size > -1.0E-4f) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("font.size.too.small.1", String.valueOf(size)));
        }
        this.state.size = size;
        this.state.fontDetails = this.writer.addSimple(bf);
        final PageResources prs = this.getPageResources();
        PdfName name = this.state.fontDetails.getFontName();
        name = prs.addFont(name, this.state.fontDetails.getIndirectReference());
        this.content.append(name.getBytes()).append(' ').append(size).append(" Tf").append_i(this.separator);
    }
    
    public void setTextRenderingMode(final int rendering) {
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        this.state.textRenderMode = rendering;
        this.content.append(rendering).append(" Tr").append_i(this.separator);
    }
    
    public void setTextRise(final float rise) {
        this.setTextRise((double)rise);
    }
    
    public void setTextRise(final double rise) {
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        this.content.append(rise).append(" Ts").append_i(this.separator);
    }
    
    private void showText2(final String text) {
        if (this.state.fontDetails == null) {
            throw new NullPointerException(MessageLocalization.getComposedMessage("font.and.size.must.be.set.before.writing.any.text", new Object[0]));
        }
        final byte[] b = this.state.fontDetails.convertToBytes(text);
        StringUtils.escapeString(b, this.content);
    }
    
    public void showText(final String text) {
        this.checkState();
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        this.showText2(text);
        this.updateTx(text, 0.0f);
        this.content.append("Tj").append_i(this.separator);
    }
    
    public void showTextGid(final String gids) {
        this.checkState();
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        if (this.state.fontDetails == null) {
            throw new NullPointerException(MessageLocalization.getComposedMessage("font.and.size.must.be.set.before.writing.any.text", new Object[0]));
        }
        final Object[] objs = this.state.fontDetails.convertToBytesGid(gids);
        StringUtils.escapeString((byte[])objs[0], this.content);
        final GraphicState state = this.state;
        state.tx += (int)objs[2] * 0.001f * this.state.size;
        this.content.append("Tj").append_i(this.separator);
    }
    
    public static PdfTextArray getKernArray(final String text, final BaseFont font) {
        final PdfTextArray pa = new PdfTextArray();
        final StringBuffer acc = new StringBuffer();
        final int len = text.length() - 1;
        final char[] c = text.toCharArray();
        if (len >= 0) {
            acc.append(c, 0, 1);
        }
        for (int k = 0; k < len; ++k) {
            final char c2 = c[k + 1];
            final int kern = font.getKerning(c[k], c2);
            if (kern == 0) {
                acc.append(c2);
            }
            else {
                pa.add(acc.toString());
                acc.setLength(0);
                acc.append(c, k + 1, 1);
                pa.add((float)(-kern));
            }
        }
        pa.add(acc.toString());
        return pa;
    }
    
    public void showTextKerned(final String text) {
        if (this.state.fontDetails == null) {
            throw new NullPointerException(MessageLocalization.getComposedMessage("font.and.size.must.be.set.before.writing.any.text", new Object[0]));
        }
        final BaseFont bf = this.state.fontDetails.getBaseFont();
        if (bf.hasKernPairs()) {
            this.showText(getKernArray(text, bf));
        }
        else {
            this.showText(text);
        }
    }
    
    public void newlineShowText(final String text) {
        this.checkState();
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        final GraphicState state = this.state;
        state.yTLM -= this.state.leading;
        this.showText2(text);
        this.content.append("'").append_i(this.separator);
        this.state.tx = this.state.xTLM;
        this.updateTx(text, 0.0f);
    }
    
    public void newlineShowText(final float wordSpacing, final float charSpacing, final String text) {
        this.checkState();
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        final GraphicState state = this.state;
        state.yTLM -= this.state.leading;
        this.content.append(wordSpacing).append(' ').append(charSpacing);
        this.showText2(text);
        this.content.append("\"").append_i(this.separator);
        this.state.charSpace = charSpacing;
        this.state.wordSpace = wordSpacing;
        this.state.tx = this.state.xTLM;
        this.updateTx(text, 0.0f);
    }
    
    public void setTextMatrix(final float a, final float b, final float c, final float d, final float x, final float y) {
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        this.state.xTLM = x;
        this.state.yTLM = y;
        this.state.aTLM = a;
        this.state.bTLM = b;
        this.state.cTLM = c;
        this.state.dTLM = d;
        this.state.tx = this.state.xTLM;
        this.content.append(a).append(' ').append(b).append_i(32).append(c).append_i(32).append(d).append_i(32).append(x).append_i(32).append(y).append(" Tm").append_i(this.separator);
    }
    
    public void setTextMatrix(final AffineTransform transform) {
        final double[] matrix = new double[6];
        transform.getMatrix(matrix);
        this.setTextMatrix((float)matrix[0], (float)matrix[1], (float)matrix[2], (float)matrix[3], (float)matrix[4], (float)matrix[5]);
    }
    
    public void setTextMatrix(final float x, final float y) {
        this.setTextMatrix(1.0f, 0.0f, 0.0f, 1.0f, x, y);
    }
    
    public void moveText(final float x, final float y) {
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        final GraphicState state = this.state;
        state.xTLM += x;
        final GraphicState state2 = this.state;
        state2.yTLM += y;
        if (this.isTagged() && this.state.xTLM != this.state.tx) {
            this.setTextMatrix(this.state.aTLM, this.state.bTLM, this.state.cTLM, this.state.dTLM, this.state.xTLM, this.state.yTLM);
        }
        else {
            this.content.append(x).append(' ').append(y).append(" Td").append_i(this.separator);
        }
    }
    
    public void moveTextWithLeading(final float x, final float y) {
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        final GraphicState state = this.state;
        state.xTLM += x;
        final GraphicState state2 = this.state;
        state2.yTLM += y;
        this.state.leading = -y;
        if (this.isTagged() && this.state.xTLM != this.state.tx) {
            this.setTextMatrix(this.state.aTLM, this.state.bTLM, this.state.cTLM, this.state.dTLM, this.state.xTLM, this.state.yTLM);
        }
        else {
            this.content.append(x).append(' ').append(y).append(" TD").append_i(this.separator);
        }
    }
    
    public void newlineText() {
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        if (this.isTagged() && this.state.xTLM != this.state.tx) {
            this.setTextMatrix(this.state.aTLM, this.state.bTLM, this.state.cTLM, this.state.dTLM, this.state.xTLM, this.state.yTLM);
        }
        final GraphicState state = this.state;
        state.yTLM -= this.state.leading;
        this.content.append("T*").append_i(this.separator);
    }
    
    int size() {
        return this.size(true);
    }
    
    int size(final boolean includeMarkedContentSize) {
        if (includeMarkedContentSize) {
            return this.content.size();
        }
        return this.content.size() - this.markedContentSize;
    }
    
    public void addOutline(final PdfOutline outline, final String name) {
        this.checkWriter();
        this.pdf.addOutline(outline, name);
    }
    
    public PdfOutline getRootOutline() {
        this.checkWriter();
        return this.pdf.getRootOutline();
    }
    
    public float getEffectiveStringWidth(final String text, final boolean kerned) {
        final BaseFont bf = this.state.fontDetails.getBaseFont();
        float w;
        if (kerned) {
            w = bf.getWidthPointKerned(text, this.state.size);
        }
        else {
            w = bf.getWidthPoint(text, this.state.size);
        }
        if (this.state.charSpace != 0.0f && text.length() > 1) {
            w += this.state.charSpace * (text.length() - 1);
        }
        if (this.state.wordSpace != 0.0f && !bf.isVertical()) {
            for (int i = 0; i < text.length() - 1; ++i) {
                if (text.charAt(i) == ' ') {
                    w += this.state.wordSpace;
                }
            }
        }
        if (this.state.scale != 100.0) {
            w = w * this.state.scale / 100.0f;
        }
        return w;
    }
    
    private float getEffectiveStringWidth(final String text, final boolean kerned, final float kerning) {
        final BaseFont bf = this.state.fontDetails.getBaseFont();
        float w;
        if (kerned) {
            w = bf.getWidthPointKerned(text, this.state.size);
        }
        else {
            w = bf.getWidthPoint(text, this.state.size);
        }
        if (this.state.charSpace != 0.0f && text.length() > 0) {
            w += this.state.charSpace * text.length();
        }
        if (this.state.wordSpace != 0.0f && !bf.isVertical()) {
            for (int i = 0; i < text.length(); ++i) {
                if (text.charAt(i) == ' ') {
                    w += this.state.wordSpace;
                }
            }
        }
        w -= kerning / 1000.0f * this.state.size;
        if (this.state.scale != 100.0) {
            w = w * this.state.scale / 100.0f;
        }
        return w;
    }
    
    public void showTextAligned(final int alignment, final String text, final float x, final float y, final float rotation) {
        this.showTextAligned(alignment, text, x, y, rotation, false);
    }
    
    private void showTextAligned(final int alignment, final String text, float x, float y, final float rotation, final boolean kerned) {
        if (this.state.fontDetails == null) {
            throw new NullPointerException(MessageLocalization.getComposedMessage("font.and.size.must.be.set.before.writing.any.text", new Object[0]));
        }
        if (rotation == 0.0f) {
            switch (alignment) {
                case 1: {
                    x -= this.getEffectiveStringWidth(text, kerned) / 2.0f;
                    break;
                }
                case 2: {
                    x -= this.getEffectiveStringWidth(text, kerned);
                    break;
                }
            }
            this.setTextMatrix(x, y);
            if (kerned) {
                this.showTextKerned(text);
            }
            else {
                this.showText(text);
            }
        }
        else {
            final double alpha = rotation * 3.141592653589793 / 180.0;
            final float cos = (float)Math.cos(alpha);
            final float sin = (float)Math.sin(alpha);
            switch (alignment) {
                case 1: {
                    final float len = this.getEffectiveStringWidth(text, kerned) / 2.0f;
                    x -= len * cos;
                    y -= len * sin;
                    break;
                }
                case 2: {
                    final float len = this.getEffectiveStringWidth(text, kerned);
                    x -= len * cos;
                    y -= len * sin;
                    break;
                }
            }
            this.setTextMatrix(cos, sin, -sin, cos, x, y);
            if (kerned) {
                this.showTextKerned(text);
            }
            else {
                this.showText(text);
            }
            this.setTextMatrix(0.0f, 0.0f);
        }
    }
    
    public void showTextAlignedKerned(final int alignment, final String text, final float x, final float y, final float rotation) {
        this.showTextAligned(alignment, text, x, y, rotation, true);
    }
    
    public void concatCTM(final float a, final float b, final float c, final float d, final float e, final float f) {
        this.concatCTM(a, b, c, d, e, (double)f);
    }
    
    public void concatCTM(final double a, final double b, final double c, final double d, final double e, final double f) {
        if (this.inText && this.isTagged()) {
            this.endText();
        }
        this.state.CTM.concatenate(new AffineTransform(a, b, c, d, e, f));
        this.content.append(a).append(' ').append(b).append(' ').append(c).append(' ');
        this.content.append(d).append(' ').append(e).append(' ').append(f).append(" cm").append_i(this.separator);
    }
    
    public void concatCTM(final AffineTransform transform) {
        final double[] matrix = new double[6];
        transform.getMatrix(matrix);
        this.concatCTM(matrix[0], matrix[1], matrix[2], matrix[3], matrix[4], matrix[5]);
    }
    
    public static ArrayList<double[]> bezierArc(final float x1, final float y1, final float x2, final float y2, final float startAng, final float extent) {
        return bezierArc(x1, y1, x2, y2, startAng, (double)extent);
    }
    
    public static ArrayList<double[]> bezierArc(double x1, double y1, double x2, double y2, final double startAng, final double extent) {
        if (x1 > x2) {
            final double tmp = x1;
            x1 = x2;
            x2 = tmp;
        }
        if (y2 > y1) {
            final double tmp = y1;
            y1 = y2;
            y2 = tmp;
        }
        double fragAngle;
        int Nfrag;
        if (Math.abs(extent) <= 90.0) {
            fragAngle = extent;
            Nfrag = 1;
        }
        else {
            Nfrag = (int)Math.ceil(Math.abs(extent) / 90.0);
            fragAngle = extent / Nfrag;
        }
        final double x_cen = (x1 + x2) / 2.0;
        final double y_cen = (y1 + y2) / 2.0;
        final double rx = (x2 - x1) / 2.0;
        final double ry = (y2 - y1) / 2.0;
        final double halfAng = fragAngle * 3.141592653589793 / 360.0;
        final double kappa = Math.abs(1.3333333333333333 * (1.0 - Math.cos(halfAng)) / Math.sin(halfAng));
        final ArrayList<double[]> pointList = new ArrayList<double[]>();
        for (int i = 0; i < Nfrag; ++i) {
            final double theta0 = (startAng + i * fragAngle) * 3.141592653589793 / 180.0;
            final double theta2 = (startAng + (i + 1) * fragAngle) * 3.141592653589793 / 180.0;
            final double cos0 = Math.cos(theta0);
            final double cos2 = Math.cos(theta2);
            final double sin0 = Math.sin(theta0);
            final double sin2 = Math.sin(theta2);
            if (fragAngle > 0.0) {
                pointList.add(new double[] { x_cen + rx * cos0, y_cen - ry * sin0, x_cen + rx * (cos0 - kappa * sin0), y_cen - ry * (sin0 + kappa * cos0), x_cen + rx * (cos2 + kappa * sin2), y_cen - ry * (sin2 - kappa * cos2), x_cen + rx * cos2, y_cen - ry * sin2 });
            }
            else {
                pointList.add(new double[] { x_cen + rx * cos0, y_cen - ry * sin0, x_cen + rx * (cos0 + kappa * sin0), y_cen - ry * (sin0 - kappa * cos0), x_cen + rx * (cos2 - kappa * sin2), y_cen - ry * (sin2 + kappa * cos2), x_cen + rx * cos2, y_cen - ry * sin2 });
            }
        }
        return pointList;
    }
    
    public void arc(final float x1, final float y1, final float x2, final float y2, final float startAng, final float extent) {
        this.arc(x1, y1, x2, y2, startAng, (double)extent);
    }
    
    public void arc(final double x1, final double y1, final double x2, final double y2, final double startAng, final double extent) {
        final ArrayList<double[]> ar = bezierArc(x1, y1, x2, y2, startAng, extent);
        if (ar.isEmpty()) {
            return;
        }
        double[] pt = ar.get(0);
        this.moveTo(pt[0], pt[1]);
        for (int k = 0; k < ar.size(); ++k) {
            pt = ar.get(k);
            this.curveTo(pt[2], pt[3], pt[4], pt[5], pt[6], pt[7]);
        }
    }
    
    public void ellipse(final float x1, final float y1, final float x2, final float y2) {
        this.ellipse(x1, y1, x2, (double)y2);
    }
    
    public void ellipse(final double x1, final double y1, final double x2, final double y2) {
        this.arc(x1, y1, x2, y2, 0.0, 360.0);
    }
    
    public PdfPatternPainter createPattern(final float width, final float height, final float xstep, final float ystep) {
        this.checkWriter();
        if (xstep == 0.0f || ystep == 0.0f) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("xstep.or.ystep.can.not.be.zero", new Object[0]));
        }
        final PdfPatternPainter painter = new PdfPatternPainter(this.writer);
        painter.setWidth(width);
        painter.setHeight(height);
        painter.setXStep(xstep);
        painter.setYStep(ystep);
        this.writer.addSimplePattern(painter);
        return painter;
    }
    
    public PdfPatternPainter createPattern(final float width, final float height) {
        return this.createPattern(width, height, width, height);
    }
    
    public PdfPatternPainter createPattern(final float width, final float height, final float xstep, final float ystep, final BaseColor color) {
        this.checkWriter();
        if (xstep == 0.0f || ystep == 0.0f) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("xstep.or.ystep.can.not.be.zero", new Object[0]));
        }
        final PdfPatternPainter painter = new PdfPatternPainter(this.writer, color);
        painter.setWidth(width);
        painter.setHeight(height);
        painter.setXStep(xstep);
        painter.setYStep(ystep);
        this.writer.addSimplePattern(painter);
        return painter;
    }
    
    public PdfPatternPainter createPattern(final float width, final float height, final BaseColor color) {
        return this.createPattern(width, height, width, height, color);
    }
    
    public PdfTemplate createTemplate(final float width, final float height) {
        return this.createTemplate(width, height, null);
    }
    
    PdfTemplate createTemplate(final float width, final float height, final PdfName forcedName) {
        this.checkWriter();
        final PdfTemplate template = new PdfTemplate(this.writer);
        template.setWidth(width);
        template.setHeight(height);
        this.writer.addDirectTemplateSimple(template, forcedName);
        return template;
    }
    
    public PdfAppearance createAppearance(final float width, final float height) {
        return this.createAppearance(width, height, null);
    }
    
    PdfAppearance createAppearance(final float width, final float height, final PdfName forcedName) {
        this.checkWriter();
        final PdfAppearance template = new PdfAppearance(this.writer);
        template.setWidth(width);
        template.setHeight(height);
        this.writer.addDirectTemplateSimple(template, forcedName);
        return template;
    }
    
    public void addPSXObject(final PdfPSXObject psobject) {
        if (this.inText && this.isTagged()) {
            this.endText();
        }
        this.checkWriter();
        PdfName name = this.writer.addDirectTemplateSimple(psobject, null);
        final PageResources prs = this.getPageResources();
        name = prs.addXObject(name, psobject.getIndirectReference());
        this.content.append(name.getBytes()).append(" Do").append_i(this.separator);
    }
    
    public void addTemplate(final PdfTemplate template, final float a, final float b, final float c, final float d, final float e, final float f) {
        this.addTemplate(template, a, b, c, d, e, f, false);
    }
    
    public void addTemplate(final PdfTemplate template, final double a, final double b, final double c, final double d, final double e, final double f) {
        this.addTemplate(template, a, b, c, d, e, f, false);
    }
    
    public void addTemplate(final PdfTemplate template, final float a, final float b, final float c, final float d, final float e, final float f, final boolean tagContent) {
        this.addTemplate(template, a, b, c, d, e, (double)f, tagContent);
    }
    
    public void addTemplate(final PdfTemplate template, final double a, final double b, final double c, final double d, final double e, final double f, final boolean tagContent) {
        this.addTemplate(template, a, b, c, d, e, f, true, tagContent);
    }
    
    private void addTemplate(final PdfTemplate template, final double a, final double b, final double c, final double d, final double e, final double f, final boolean tagTemplate, final boolean tagContent) {
        this.checkWriter();
        this.checkNoPattern(template);
        PdfWriter.checkPdfIsoConformance(this.writer, 20, template);
        PdfName name = this.writer.addDirectTemplateSimple(template, null);
        final PageResources prs = this.getPageResources();
        name = prs.addXObject(name, template.getIndirectReference());
        if (this.isTagged() && tagTemplate) {
            if (this.inText) {
                this.endText();
            }
            if (template.isContentTagged() || (template.getPageReference() != null && tagContent)) {
                throw new RuntimeException(MessageLocalization.getComposedMessage("template.with.tagged.could.not.be.used.more.than.once", new Object[0]));
            }
            template.setPageReference(this.writer.getCurrentPage());
            if (tagContent) {
                template.setContentTagged(true);
                this.ensureDocumentTagIsOpen();
                final ArrayList<IAccessibleElement> allMcElements = this.getMcElements();
                if (allMcElements != null && allMcElements.size() > 0) {
                    template.getMcElements().add(allMcElements.get(allMcElements.size() - 1));
                }
            }
            else {
                this.openMCBlock(template);
            }
        }
        this.content.append("q ");
        this.content.append(a).append(' ');
        this.content.append(b).append(' ');
        this.content.append(c).append(' ');
        this.content.append(d).append(' ');
        this.content.append(e).append(' ');
        this.content.append(f).append(" cm ");
        this.content.append(name.getBytes()).append(" Do Q").append_i(this.separator);
        if (this.isTagged() && tagTemplate && !tagContent) {
            this.closeMCBlock(template);
            template.setId(null);
        }
    }
    
    public PdfName addFormXObj(final PdfStream formXObj, final PdfName name, final float a, final float b, final float c, final float d, final float e, final float f) throws IOException {
        return this.addFormXObj(formXObj, name, a, b, c, d, e, (double)f);
    }
    
    public PdfName addFormXObj(final PdfStream formXObj, final PdfName name, final double a, final double b, final double c, final double d, final double e, final double f) throws IOException {
        this.checkWriter();
        PdfWriter.checkPdfIsoConformance(this.writer, 9, formXObj);
        final PageResources prs = this.getPageResources();
        final PdfName translatedName = prs.addXObject(name, this.writer.addToBody(formXObj).getIndirectReference());
        PdfArtifact artifact = null;
        if (this.isTagged()) {
            if (this.inText) {
                this.endText();
            }
            artifact = new PdfArtifact();
            this.openMCBlock(artifact);
        }
        this.content.append("q ");
        this.content.append(a).append(' ');
        this.content.append(b).append(' ');
        this.content.append(c).append(' ');
        this.content.append(d).append(' ');
        this.content.append(e).append(' ');
        this.content.append(f).append(" cm ");
        this.content.append(translatedName.getBytes()).append(" Do Q").append_i(this.separator);
        if (this.isTagged()) {
            this.closeMCBlock(artifact);
        }
        return translatedName;
    }
    
    public void addTemplate(final PdfTemplate template, final AffineTransform transform) {
        this.addTemplate(template, transform, false);
    }
    
    public void addTemplate(final PdfTemplate template, final AffineTransform transform, final boolean tagContent) {
        final double[] matrix = new double[6];
        transform.getMatrix(matrix);
        this.addTemplate(template, matrix[0], matrix[1], matrix[2], matrix[3], matrix[4], matrix[5], tagContent);
    }
    
    void addTemplateReference(final PdfIndirectReference template, final PdfName name, final float a, final float b, final float c, final float d, final float e, final float f) {
        this.addTemplateReference(template, name, a, b, c, d, e, (double)f);
    }
    
    void addTemplateReference(final PdfIndirectReference template, PdfName name, final double a, final double b, final double c, final double d, final double e, final double f) {
        if (this.inText && this.isTagged()) {
            this.endText();
        }
        this.checkWriter();
        final PageResources prs = this.getPageResources();
        name = prs.addXObject(name, template);
        this.content.append("q ");
        this.content.append(a).append(' ');
        this.content.append(b).append(' ');
        this.content.append(c).append(' ');
        this.content.append(d).append(' ');
        this.content.append(e).append(' ');
        this.content.append(f).append(" cm ");
        this.content.append(name.getBytes()).append(" Do Q").append_i(this.separator);
    }
    
    public void addTemplate(final PdfTemplate template, final float x, final float y) {
        this.addTemplate(template, 1.0f, 0.0f, 0.0f, 1.0f, x, y);
    }
    
    public void addTemplate(final PdfTemplate template, final double x, final double y) {
        this.addTemplate(template, 1.0, 0.0, 0.0, 1.0, x, y);
    }
    
    public void addTemplate(final PdfTemplate template, final float x, final float y, final boolean tagContent) {
        this.addTemplate(template, 1.0f, 0.0f, 0.0f, 1.0f, x, y, tagContent);
    }
    
    public void addTemplate(final PdfTemplate template, final double x, final double y, final boolean tagContent) {
        this.addTemplate(template, 1.0, 0.0, 0.0, 1.0, x, y, tagContent);
    }
    
    public void setCMYKColorFill(final int cyan, final int magenta, final int yellow, final int black) {
        this.saveColor(new CMYKColor(cyan, magenta, yellow, black), true);
        this.content.append((cyan & 0xFF) / 255.0f);
        this.content.append(' ');
        this.content.append((magenta & 0xFF) / 255.0f);
        this.content.append(' ');
        this.content.append((yellow & 0xFF) / 255.0f);
        this.content.append(' ');
        this.content.append((black & 0xFF) / 255.0f);
        this.content.append(" k").append_i(this.separator);
    }
    
    public void setCMYKColorStroke(final int cyan, final int magenta, final int yellow, final int black) {
        this.saveColor(new CMYKColor(cyan, magenta, yellow, black), false);
        this.content.append((cyan & 0xFF) / 255.0f);
        this.content.append(' ');
        this.content.append((magenta & 0xFF) / 255.0f);
        this.content.append(' ');
        this.content.append((yellow & 0xFF) / 255.0f);
        this.content.append(' ');
        this.content.append((black & 0xFF) / 255.0f);
        this.content.append(" K").append_i(this.separator);
    }
    
    public void setRGBColorFill(final int red, final int green, final int blue) {
        this.saveColor(new BaseColor(red, green, blue), true);
        this.HelperRGB((red & 0xFF) / 255.0f, (green & 0xFF) / 255.0f, (blue & 0xFF) / 255.0f);
        this.content.append(" rg").append_i(this.separator);
    }
    
    public void setRGBColorStroke(final int red, final int green, final int blue) {
        this.saveColor(new BaseColor(red, green, blue), false);
        this.HelperRGB((red & 0xFF) / 255.0f, (green & 0xFF) / 255.0f, (blue & 0xFF) / 255.0f);
        this.content.append(" RG").append_i(this.separator);
    }
    
    public void setColorStroke(final BaseColor color) {
        final int type = ExtendedColor.getType(color);
        switch (type) {
            case 1: {
                this.setGrayStroke(((GrayColor)color).getGray());
                break;
            }
            case 2: {
                final CMYKColor cmyk = (CMYKColor)color;
                this.setCMYKColorStrokeF(cmyk.getCyan(), cmyk.getMagenta(), cmyk.getYellow(), cmyk.getBlack());
                break;
            }
            case 3: {
                final SpotColor spot = (SpotColor)color;
                this.setColorStroke(spot.getPdfSpotColor(), spot.getTint());
                break;
            }
            case 4: {
                final PatternColor pat = (PatternColor)color;
                this.setPatternStroke(pat.getPainter());
                break;
            }
            case 5: {
                final ShadingColor shading = (ShadingColor)color;
                this.setShadingStroke(shading.getPdfShadingPattern());
                break;
            }
            case 6: {
                final DeviceNColor devicen = (DeviceNColor)color;
                this.setColorStroke(devicen.getPdfDeviceNColor(), devicen.getTints());
                break;
            }
            case 7: {
                final LabColor lab = (LabColor)color;
                this.setColorStroke(lab.getLabColorSpace(), lab.getL(), lab.getA(), lab.getB());
                break;
            }
            default: {
                this.setRGBColorStroke(color.getRed(), color.getGreen(), color.getBlue());
                break;
            }
        }
        final int alpha = color.getAlpha();
        if (alpha < 255) {
            final PdfGState gState = new PdfGState();
            gState.setStrokeOpacity(alpha / 255.0f);
            this.setGState(gState);
        }
    }
    
    public void setColorFill(final BaseColor color) {
        final int type = ExtendedColor.getType(color);
        switch (type) {
            case 1: {
                this.setGrayFill(((GrayColor)color).getGray());
                break;
            }
            case 2: {
                final CMYKColor cmyk = (CMYKColor)color;
                this.setCMYKColorFillF(cmyk.getCyan(), cmyk.getMagenta(), cmyk.getYellow(), cmyk.getBlack());
                break;
            }
            case 3: {
                final SpotColor spot = (SpotColor)color;
                this.setColorFill(spot.getPdfSpotColor(), spot.getTint());
                break;
            }
            case 4: {
                final PatternColor pat = (PatternColor)color;
                this.setPatternFill(pat.getPainter());
                break;
            }
            case 5: {
                final ShadingColor shading = (ShadingColor)color;
                this.setShadingFill(shading.getPdfShadingPattern());
                break;
            }
            case 6: {
                final DeviceNColor devicen = (DeviceNColor)color;
                this.setColorFill(devicen.getPdfDeviceNColor(), devicen.getTints());
                break;
            }
            case 7: {
                final LabColor lab = (LabColor)color;
                this.setColorFill(lab.getLabColorSpace(), lab.getL(), lab.getA(), lab.getB());
                break;
            }
            default: {
                this.setRGBColorFill(color.getRed(), color.getGreen(), color.getBlue());
                break;
            }
        }
        final int alpha = color.getAlpha();
        if (alpha < 255) {
            final PdfGState gState = new PdfGState();
            gState.setFillOpacity(alpha / 255.0f);
            this.setGState(gState);
        }
    }
    
    public void setColorFill(final PdfSpotColor sp, final float tint) {
        this.checkWriter();
        this.state.colorDetails = this.writer.addSimple(sp);
        final PageResources prs = this.getPageResources();
        PdfName name = this.state.colorDetails.getColorSpaceName();
        name = prs.addColor(name, this.state.colorDetails.getIndirectReference());
        this.saveColor(new SpotColor(sp, tint), true);
        this.content.append(name.getBytes()).append(" cs ").append(tint).append(" scn").append_i(this.separator);
    }
    
    public void setColorFill(final PdfDeviceNColor dn, final float[] tints) {
        this.checkWriter();
        this.state.colorDetails = this.writer.addSimple(dn);
        final PageResources prs = this.getPageResources();
        PdfName name = this.state.colorDetails.getColorSpaceName();
        name = prs.addColor(name, this.state.colorDetails.getIndirectReference());
        this.saveColor(new DeviceNColor(dn, tints), true);
        this.content.append(name.getBytes()).append(" cs ");
        for (final float tint : tints) {
            this.content.append(tint + " ");
        }
        this.content.append("scn").append_i(this.separator);
    }
    
    public void setColorFill(final PdfLabColor lab, final float l, final float a, final float b) {
        this.checkWriter();
        this.state.colorDetails = this.writer.addSimple(lab);
        final PageResources prs = this.getPageResources();
        PdfName name = this.state.colorDetails.getColorSpaceName();
        name = prs.addColor(name, this.state.colorDetails.getIndirectReference());
        this.saveColor(new LabColor(lab, l, a, b), true);
        this.content.append(name.getBytes()).append(" cs ");
        this.content.append(l + " " + a + " " + b + " ");
        this.content.append("scn").append_i(this.separator);
    }
    
    public void setColorStroke(final PdfSpotColor sp, final float tint) {
        this.checkWriter();
        this.state.colorDetails = this.writer.addSimple(sp);
        final PageResources prs = this.getPageResources();
        PdfName name = this.state.colorDetails.getColorSpaceName();
        name = prs.addColor(name, this.state.colorDetails.getIndirectReference());
        this.saveColor(new SpotColor(sp, tint), false);
        this.content.append(name.getBytes()).append(" CS ").append(tint).append(" SCN").append_i(this.separator);
    }
    
    public void setColorStroke(final PdfDeviceNColor sp, final float[] tints) {
        this.checkWriter();
        this.state.colorDetails = this.writer.addSimple(sp);
        final PageResources prs = this.getPageResources();
        PdfName name = this.state.colorDetails.getColorSpaceName();
        name = prs.addColor(name, this.state.colorDetails.getIndirectReference());
        this.saveColor(new DeviceNColor(sp, tints), true);
        this.content.append(name.getBytes()).append(" CS ");
        for (final float tint : tints) {
            this.content.append(tint + " ");
        }
        this.content.append("SCN").append_i(this.separator);
    }
    
    public void setColorStroke(final PdfLabColor lab, final float l, final float a, final float b) {
        this.checkWriter();
        this.state.colorDetails = this.writer.addSimple(lab);
        final PageResources prs = this.getPageResources();
        PdfName name = this.state.colorDetails.getColorSpaceName();
        name = prs.addColor(name, this.state.colorDetails.getIndirectReference());
        this.saveColor(new LabColor(lab, l, a, b), true);
        this.content.append(name.getBytes()).append(" CS ");
        this.content.append(l + " " + a + " " + b + " ");
        this.content.append("SCN").append_i(this.separator);
    }
    
    public void setPatternFill(final PdfPatternPainter p) {
        if (p.isStencil()) {
            this.setPatternFill(p, p.getDefaultColor());
            return;
        }
        this.checkWriter();
        final PageResources prs = this.getPageResources();
        PdfName name = this.writer.addSimplePattern(p);
        name = prs.addPattern(name, p.getIndirectReference());
        this.saveColor(new PatternColor(p), true);
        this.content.append(PdfName.PATTERN.getBytes()).append(" cs ").append(name.getBytes()).append(" scn").append_i(this.separator);
    }
    
    void outputColorNumbers(final BaseColor color, final float tint) {
        PdfWriter.checkPdfIsoConformance(this.writer, 1, color);
        final int type = ExtendedColor.getType(color);
        switch (type) {
            case 0: {
                this.content.append(color.getRed() / 255.0f);
                this.content.append(' ');
                this.content.append(color.getGreen() / 255.0f);
                this.content.append(' ');
                this.content.append(color.getBlue() / 255.0f);
                break;
            }
            case 1: {
                this.content.append(((GrayColor)color).getGray());
                break;
            }
            case 2: {
                final CMYKColor cmyk = (CMYKColor)color;
                this.content.append(cmyk.getCyan()).append(' ').append(cmyk.getMagenta());
                this.content.append(' ').append(cmyk.getYellow()).append(' ').append(cmyk.getBlack());
                break;
            }
            case 3: {
                this.content.append(tint);
                break;
            }
            default: {
                throw new RuntimeException(MessageLocalization.getComposedMessage("invalid.color.type", new Object[0]));
            }
        }
    }
    
    public void setPatternFill(final PdfPatternPainter p, final BaseColor color) {
        if (ExtendedColor.getType(color) == 3) {
            this.setPatternFill(p, color, ((SpotColor)color).getTint());
        }
        else {
            this.setPatternFill(p, color, 0.0f);
        }
    }
    
    public void setPatternFill(final PdfPatternPainter p, final BaseColor color, final float tint) {
        this.checkWriter();
        if (!p.isStencil()) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("an.uncolored.pattern.was.expected", new Object[0]));
        }
        final PageResources prs = this.getPageResources();
        PdfName name = this.writer.addSimplePattern(p);
        name = prs.addPattern(name, p.getIndirectReference());
        final ColorDetails csDetail = this.writer.addSimplePatternColorspace(color);
        final PdfName cName = prs.addColor(csDetail.getColorSpaceName(), csDetail.getIndirectReference());
        this.saveColor(new UncoloredPattern(p, color, tint), true);
        this.content.append(cName.getBytes()).append(" cs").append_i(this.separator);
        this.outputColorNumbers(color, tint);
        this.content.append(' ').append(name.getBytes()).append(" scn").append_i(this.separator);
    }
    
    public void setPatternStroke(final PdfPatternPainter p, final BaseColor color) {
        if (ExtendedColor.getType(color) == 3) {
            this.setPatternStroke(p, color, ((SpotColor)color).getTint());
        }
        else {
            this.setPatternStroke(p, color, 0.0f);
        }
    }
    
    public void setPatternStroke(final PdfPatternPainter p, final BaseColor color, final float tint) {
        this.checkWriter();
        if (!p.isStencil()) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("an.uncolored.pattern.was.expected", new Object[0]));
        }
        final PageResources prs = this.getPageResources();
        PdfName name = this.writer.addSimplePattern(p);
        name = prs.addPattern(name, p.getIndirectReference());
        final ColorDetails csDetail = this.writer.addSimplePatternColorspace(color);
        final PdfName cName = prs.addColor(csDetail.getColorSpaceName(), csDetail.getIndirectReference());
        this.saveColor(new UncoloredPattern(p, color, tint), false);
        this.content.append(cName.getBytes()).append(" CS").append_i(this.separator);
        this.outputColorNumbers(color, tint);
        this.content.append(' ').append(name.getBytes()).append(" SCN").append_i(this.separator);
    }
    
    public void setPatternStroke(final PdfPatternPainter p) {
        if (p.isStencil()) {
            this.setPatternStroke(p, p.getDefaultColor());
            return;
        }
        this.checkWriter();
        final PageResources prs = this.getPageResources();
        PdfName name = this.writer.addSimplePattern(p);
        name = prs.addPattern(name, p.getIndirectReference());
        this.saveColor(new PatternColor(p), false);
        this.content.append(PdfName.PATTERN.getBytes()).append(" CS ").append(name.getBytes()).append(" SCN").append_i(this.separator);
    }
    
    public void paintShading(final PdfShading shading) {
        this.writer.addSimpleShading(shading);
        final PageResources prs = this.getPageResources();
        final PdfName name = prs.addShading(shading.getShadingName(), shading.getShadingReference());
        this.content.append(name.getBytes()).append(" sh").append_i(this.separator);
        final ColorDetails details = shading.getColorDetails();
        if (details != null) {
            prs.addColor(details.getColorSpaceName(), details.getIndirectReference());
        }
    }
    
    public void paintShading(final PdfShadingPattern shading) {
        this.paintShading(shading.getShading());
    }
    
    public void setShadingFill(final PdfShadingPattern shading) {
        this.writer.addSimpleShadingPattern(shading);
        final PageResources prs = this.getPageResources();
        final PdfName name = prs.addPattern(shading.getPatternName(), shading.getPatternReference());
        this.saveColor(new ShadingColor(shading), true);
        this.content.append(PdfName.PATTERN.getBytes()).append(" cs ").append(name.getBytes()).append(" scn").append_i(this.separator);
        final ColorDetails details = shading.getColorDetails();
        if (details != null) {
            prs.addColor(details.getColorSpaceName(), details.getIndirectReference());
        }
    }
    
    public void setShadingStroke(final PdfShadingPattern shading) {
        this.writer.addSimpleShadingPattern(shading);
        final PageResources prs = this.getPageResources();
        final PdfName name = prs.addPattern(shading.getPatternName(), shading.getPatternReference());
        this.saveColor(new ShadingColor(shading), false);
        this.content.append(PdfName.PATTERN.getBytes()).append(" CS ").append(name.getBytes()).append(" SCN").append_i(this.separator);
        final ColorDetails details = shading.getColorDetails();
        if (details != null) {
            prs.addColor(details.getColorSpaceName(), details.getIndirectReference());
        }
    }
    
    protected void checkWriter() {
        if (this.writer == null) {
            throw new NullPointerException(MessageLocalization.getComposedMessage("the.writer.in.pdfcontentbyte.is.null", new Object[0]));
        }
    }
    
    public void showText(final PdfTextArray text) {
        this.checkState();
        if (!this.inText && this.isTagged()) {
            this.beginText(true);
        }
        if (this.state.fontDetails == null) {
            throw new NullPointerException(MessageLocalization.getComposedMessage("font.and.size.must.be.set.before.writing.any.text", new Object[0]));
        }
        this.content.append("[");
        final ArrayList<Object> arrayList = text.getArrayList();
        boolean lastWasNumber = false;
        for (final Object obj : arrayList) {
            if (obj instanceof String) {
                this.showText2((String)obj);
                this.updateTx((String)obj, 0.0f);
                lastWasNumber = false;
            }
            else {
                if (lastWasNumber) {
                    this.content.append(' ');
                }
                else {
                    lastWasNumber = true;
                }
                this.content.append((float)obj);
                this.updateTx("", (float)obj);
            }
        }
        this.content.append("]TJ").append_i(this.separator);
    }
    
    public PdfWriter getPdfWriter() {
        return this.writer;
    }
    
    public PdfDocument getPdfDocument() {
        return this.pdf;
    }
    
    public void localGoto(final String name, final float llx, final float lly, final float urx, final float ury) {
        this.pdf.localGoto(name, llx, lly, urx, ury);
    }
    
    public boolean localDestination(final String name, final PdfDestination destination) {
        return this.pdf.localDestination(name, destination);
    }
    
    public PdfContentByte getDuplicate() {
        final PdfContentByte cb = new PdfContentByte(this.writer);
        cb.duplicatedFrom = this;
        return cb;
    }
    
    public PdfContentByte getDuplicate(final boolean inheritGraphicState) {
        final PdfContentByte cb = this.getDuplicate();
        if (inheritGraphicState) {
            cb.state = this.state;
            cb.stateList = this.stateList;
        }
        return cb;
    }
    
    public void inheritGraphicState(final PdfContentByte parentCanvas) {
        this.state = parentCanvas.state;
        this.stateList = parentCanvas.stateList;
    }
    
    public void remoteGoto(final String filename, final String name, final float llx, final float lly, final float urx, final float ury) {
        this.pdf.remoteGoto(filename, name, llx, lly, urx, ury);
    }
    
    public void remoteGoto(final String filename, final int page, final float llx, final float lly, final float urx, final float ury) {
        this.pdf.remoteGoto(filename, page, llx, lly, urx, ury);
    }
    
    public void roundRectangle(final float x, final float y, final float w, final float h, final float r) {
        this.roundRectangle(x, y, w, h, (double)r);
    }
    
    public void roundRectangle(double x, double y, double w, double h, double r) {
        if (w < 0.0) {
            x += w;
            w = -w;
        }
        if (h < 0.0) {
            y += h;
            h = -h;
        }
        if (r < 0.0) {
            r = -r;
        }
        final float b = 0.4477f;
        this.moveTo(x + r, y);
        this.lineTo(x + w - r, y);
        this.curveTo(x + w - r * b, y, x + w, y + r * b, x + w, y + r);
        this.lineTo(x + w, y + h - r);
        this.curveTo(x + w, y + h - r * b, x + w - r * b, y + h, x + w - r, y + h);
        this.lineTo(x + r, y + h);
        this.curveTo(x + r * b, y + h, x, y + h - r * b, x, y + h - r);
        this.lineTo(x, y + r);
        this.curveTo(x, y + r * b, x + r * b, y, x + r, y);
    }
    
    public void setAction(final PdfAction action, final float llx, final float lly, final float urx, final float ury) {
        this.pdf.setAction(action, llx, lly, urx, ury);
    }
    
    public void setLiteral(final String s) {
        this.content.append(s);
    }
    
    public void setLiteral(final char c) {
        this.content.append(c);
    }
    
    public void setLiteral(final float n) {
        this.content.append(n);
    }
    
    void checkNoPattern(final PdfTemplate t) {
        if (t.getType() == 3) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("invalid.use.of.a.pattern.a.template.was.expected", new Object[0]));
        }
    }
    
    public void drawRadioField(final float llx, final float lly, final float urx, final float ury, final boolean on) {
        this.drawRadioField(llx, lly, urx, (double)ury, on);
    }
    
    public void drawRadioField(double llx, double lly, double urx, double ury, final boolean on) {
        if (llx > urx) {
            final double x = llx;
            llx = urx;
            urx = x;
        }
        if (lly > ury) {
            final double y = lly;
            lly = ury;
            ury = y;
        }
        this.saveState();
        this.setLineWidth(1.0f);
        this.setLineCap(1);
        this.setColorStroke(new BaseColor(192, 192, 192));
        this.arc(llx + 1.0, lly + 1.0, urx - 1.0, ury - 1.0, 0.0, 360.0);
        this.stroke();
        this.setLineWidth(1.0f);
        this.setLineCap(1);
        this.setColorStroke(new BaseColor(160, 160, 160));
        this.arc(llx + 0.5, lly + 0.5, urx - 0.5, ury - 0.5, 45.0, 180.0);
        this.stroke();
        this.setLineWidth(1.0f);
        this.setLineCap(1);
        this.setColorStroke(new BaseColor(0, 0, 0));
        this.arc(llx + 1.5, lly + 1.5, urx - 1.5, ury - 1.5, 45.0, 180.0);
        this.stroke();
        if (on) {
            this.setLineWidth(1.0f);
            this.setLineCap(1);
            this.setColorFill(new BaseColor(0, 0, 0));
            this.arc(llx + 4.0, lly + 4.0, urx - 4.0, ury - 4.0, 0.0, 360.0);
            this.fill();
        }
        this.restoreState();
    }
    
    public void drawTextField(final float llx, final float lly, final float urx, final float ury) {
        this.drawTextField(llx, lly, urx, (double)ury);
    }
    
    public void drawTextField(double llx, double lly, double urx, double ury) {
        if (llx > urx) {
            final double x = llx;
            llx = urx;
            urx = x;
        }
        if (lly > ury) {
            final double y = lly;
            lly = ury;
            ury = y;
        }
        this.saveState();
        this.setColorStroke(new BaseColor(192, 192, 192));
        this.setLineWidth(1.0f);
        this.setLineCap(0);
        this.rectangle(llx, lly, urx - llx, ury - lly);
        this.stroke();
        this.setLineWidth(1.0f);
        this.setLineCap(0);
        this.setColorFill(new BaseColor(255, 255, 255));
        this.rectangle(llx + 0.5, lly + 0.5, urx - llx - 1.0, ury - lly - 1.0);
        this.fill();
        this.setColorStroke(new BaseColor(192, 192, 192));
        this.setLineWidth(1.0f);
        this.setLineCap(0);
        this.moveTo(llx + 1.0, lly + 1.5);
        this.lineTo(urx - 1.5, lly + 1.5);
        this.lineTo(urx - 1.5, ury - 1.0);
        this.stroke();
        this.setColorStroke(new BaseColor(160, 160, 160));
        this.setLineWidth(1.0f);
        this.setLineCap(0);
        this.moveTo(llx + 1.0, lly + 1.0);
        this.lineTo(llx + 1.0, ury - 1.0);
        this.lineTo(urx - 1.0, ury - 1.0);
        this.stroke();
        this.setColorStroke(new BaseColor(0, 0, 0));
        this.setLineWidth(1.0f);
        this.setLineCap(0);
        this.moveTo(llx + 2.0, lly + 2.0);
        this.lineTo(llx + 2.0, ury - 2.0);
        this.lineTo(urx - 2.0, ury - 2.0);
        this.stroke();
        this.restoreState();
    }
    
    public void drawButton(final float llx, final float lly, final float urx, final float ury, final String text, final BaseFont bf, final float size) {
        this.drawButton(llx, lly, urx, (double)ury, text, bf, size);
    }
    
    public void drawButton(double llx, double lly, double urx, double ury, final String text, final BaseFont bf, final float size) {
        if (llx > urx) {
            final double x = llx;
            llx = urx;
            urx = x;
        }
        if (lly > ury) {
            final double y = lly;
            lly = ury;
            ury = y;
        }
        this.saveState();
        this.setColorStroke(new BaseColor(0, 0, 0));
        this.setLineWidth(1.0f);
        this.setLineCap(0);
        this.rectangle(llx, lly, urx - llx, ury - lly);
        this.stroke();
        this.setLineWidth(1.0f);
        this.setLineCap(0);
        this.setColorFill(new BaseColor(192, 192, 192));
        this.rectangle(llx + 0.5, lly + 0.5, urx - llx - 1.0, ury - lly - 1.0);
        this.fill();
        this.setColorStroke(new BaseColor(255, 255, 255));
        this.setLineWidth(1.0f);
        this.setLineCap(0);
        this.moveTo(llx + 1.0, lly + 1.0);
        this.lineTo(llx + 1.0, ury - 1.0);
        this.lineTo(urx - 1.0, ury - 1.0);
        this.stroke();
        this.setColorStroke(new BaseColor(160, 160, 160));
        this.setLineWidth(1.0f);
        this.setLineCap(0);
        this.moveTo(llx + 1.0, lly + 1.0);
        this.lineTo(urx - 1.0, lly + 1.0);
        this.lineTo(urx - 1.0, ury - 1.0);
        this.stroke();
        this.resetRGBColorFill();
        this.beginText();
        this.setFontAndSize(bf, size);
        this.showTextAligned(1, text, (float)(llx + (urx - llx) / 2.0), (float)(lly + (ury - lly - size) / 2.0), 0.0f);
        this.endText();
        this.restoreState();
    }
    
    PageResources getPageResources() {
        return this.pdf.getPageResources();
    }
    
    public void setGState(final PdfGState gstate) {
        final PdfObject[] obj = this.writer.addSimpleExtGState(gstate);
        final PageResources prs = this.getPageResources();
        final PdfName name = prs.addExtGState((PdfName)obj[0], (PdfIndirectReference)obj[1]);
        this.state.extGState = gstate;
        this.content.append(name.getBytes()).append(" gs").append_i(this.separator);
    }
    
    public void beginLayer(final PdfOCG layer) {
        if (layer instanceof PdfLayer && ((PdfLayer)layer).getTitle() != null) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("a.title.is.not.a.layer", new Object[0]));
        }
        if (this.layerDepth == null) {
            this.layerDepth = new ArrayList<Integer>();
        }
        if (layer instanceof PdfLayerMembership) {
            this.layerDepth.add(1);
            this.beginLayer2(layer);
            return;
        }
        int n = 0;
        for (PdfLayer la = (PdfLayer)layer; la != null; la = la.getParent()) {
            if (la.getTitle() == null) {
                this.beginLayer2(la);
                ++n;
            }
        }
        this.layerDepth.add(n);
    }
    
    private void beginLayer2(final PdfOCG layer) {
        PdfName name = (PdfName)this.writer.addSimpleProperty(layer, layer.getRef())[0];
        final PageResources prs = this.getPageResources();
        name = prs.addProperty(name, layer.getRef());
        this.content.append("/OC ").append(name.getBytes()).append(" BDC").append_i(this.separator);
    }
    
    public void endLayer() {
        int n = 1;
        if (this.layerDepth != null && !this.layerDepth.isEmpty()) {
            n = this.layerDepth.get(this.layerDepth.size() - 1);
            this.layerDepth.remove(this.layerDepth.size() - 1);
            while (n-- > 0) {
                this.content.append("EMC").append_i(this.separator);
            }
            return;
        }
        throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("unbalanced.layer.operators", new Object[0]));
    }
    
    public void transform(final AffineTransform af) {
        if (this.inText && this.isTagged()) {
            this.endText();
        }
        final double[] matrix = new double[6];
        af.getMatrix(matrix);
        this.state.CTM.concatenate(af);
        this.content.append(matrix[0]).append(' ').append(matrix[1]).append(' ').append(matrix[2]).append(' ');
        this.content.append(matrix[3]).append(' ').append(matrix[4]).append(' ').append(matrix[5]).append(" cm").append_i(this.separator);
    }
    
    void addAnnotation(final PdfAnnotation annot) {
        final boolean needToTag = this.isTagged() && annot.getRole() != null && (!(annot instanceof PdfFormField) || ((PdfFormField)annot).getKids() == null);
        if (needToTag) {
            this.openMCBlock(annot);
        }
        this.writer.addAnnotation(annot);
        if (needToTag) {
            final PdfStructureElement strucElem = this.pdf.getStructElement(annot.getId());
            if (strucElem != null) {
                final int structParent = this.pdf.getStructParentIndex(annot);
                annot.put(PdfName.STRUCTPARENT, new PdfNumber(structParent));
                strucElem.setAnnotation(annot, this.getCurrentPage());
                this.writer.getStructureTreeRoot().setAnnotationMark(structParent, strucElem.getReference());
            }
            this.closeMCBlock(annot);
        }
    }
    
    public void addAnnotation(final PdfAnnotation annot, final boolean applyCTM) {
        if (applyCTM && this.state.CTM.getType() != 0) {
            annot.applyCTM(this.state.CTM);
        }
        this.addAnnotation(annot);
    }
    
    public void setDefaultColorspace(final PdfName name, final PdfObject obj) {
        final PageResources prs = this.getPageResources();
        prs.addDefaultColor(name, obj);
    }
    
    public void beginMarkedContentSequence(final PdfStructureElement struc) {
        this.beginMarkedContentSequence(struc, null);
    }
    
    private void beginMarkedContentSequence(final PdfStructureElement struc, final String expansion) {
        final PdfObject obj = struc.get(PdfName.K);
        final int[] structParentMarkPoint = this.pdf.getStructParentIndexAndNextMarkPoint(this.getCurrentPage());
        final int structParent = structParentMarkPoint[0];
        final int mark = structParentMarkPoint[1];
        if (obj != null) {
            PdfArray ar = null;
            if (obj.isNumber()) {
                ar = new PdfArray();
                ar.add(obj);
                struc.put(PdfName.K, ar);
            }
            else {
                if (!obj.isArray()) {
                    throw new IllegalArgumentException(MessageLocalization.getComposedMessage("unknown.object.at.k.1", obj.getClass().toString()));
                }
                ar = (PdfArray)obj;
            }
            if (ar.getAsNumber(0) != null) {
                final PdfDictionary dic = new PdfDictionary(PdfName.MCR);
                dic.put(PdfName.PG, this.getCurrentPage());
                dic.put(PdfName.MCID, new PdfNumber(mark));
                ar.add(dic);
            }
            struc.setPageMark(this.pdf.getStructParentIndex(this.getCurrentPage()), -1);
        }
        else {
            struc.setPageMark(structParent, mark);
            struc.put(PdfName.PG, this.getCurrentPage());
        }
        this.setMcDepth(this.getMcDepth() + 1);
        final int contentSize = this.content.size();
        this.content.append(struc.get(PdfName.S).getBytes()).append(" <</MCID ").append(mark);
        if (null != expansion) {
            this.content.append("/E (").append(expansion).append(")");
        }
        this.content.append(">> BDC").append_i(this.separator);
        this.markedContentSize += this.content.size() - contentSize;
    }
    
    protected PdfIndirectReference getCurrentPage() {
        return this.writer.getCurrentPage();
    }
    
    public void endMarkedContentSequence() {
        if (this.getMcDepth() == 0) {
            throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("unbalanced.begin.end.marked.content.operators", new Object[0]));
        }
        final int contentSize = this.content.size();
        this.setMcDepth(this.getMcDepth() - 1);
        this.content.append("EMC").append_i(this.separator);
        this.markedContentSize += this.content.size() - contentSize;
    }
    
    public void beginMarkedContentSequence(final PdfName tag, final PdfDictionary property, final boolean inline) {
        final int contentSize = this.content.size();
        if (property == null) {
            this.content.append(tag.getBytes()).append(" BMC").append_i(this.separator);
            this.setMcDepth(this.getMcDepth() + 1);
        }
        else {
            this.content.append(tag.getBytes()).append(' ');
            Label_0185: {
                if (inline) {
                    try {
                        property.toPdf(this.writer, this.content);
                        break Label_0185;
                    }
                    catch (Exception e) {
                        throw new ExceptionConverter(e);
                    }
                }
                PdfObject[] objs;
                if (this.writer.propertyExists(property)) {
                    objs = this.writer.addSimpleProperty(property, null);
                }
                else {
                    objs = this.writer.addSimpleProperty(property, this.writer.getPdfIndirectReference());
                }
                PdfName name = (PdfName)objs[0];
                final PageResources prs = this.getPageResources();
                name = prs.addProperty(name, (PdfIndirectReference)objs[1]);
                this.content.append(name.getBytes());
            }
            this.content.append(" BDC").append_i(this.separator);
            this.setMcDepth(this.getMcDepth() + 1);
        }
        this.markedContentSize += this.content.size() - contentSize;
    }
    
    public void beginMarkedContentSequence(final PdfName tag) {
        this.beginMarkedContentSequence(tag, null, false);
    }
    
    public void sanityCheck() {
        if (this.getMcDepth() != 0) {
            throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("unbalanced.marked.content.operators", new Object[0]));
        }
        if (this.inText) {
            if (!this.isTagged()) {
                throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("unbalanced.begin.end.text.operators", new Object[0]));
            }
            this.endText();
        }
        if (this.layerDepth != null && !this.layerDepth.isEmpty()) {
            throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("unbalanced.layer.operators", new Object[0]));
        }
        if (!this.stateList.isEmpty()) {
            throw new IllegalPdfSyntaxException(MessageLocalization.getComposedMessage("unbalanced.save.restore.state.operators", new Object[0]));
        }
    }
    
    public void openMCBlock(final IAccessibleElement element) {
        if (this.isTagged()) {
            this.ensureDocumentTagIsOpen();
            if (element != null && !this.getMcElements().contains(element)) {
                final PdfStructureElement structureElement = this.openMCBlockInt(element);
                this.getMcElements().add(element);
                if (structureElement != null) {
                    this.pdf.saveStructElement(element.getId(), structureElement);
                }
            }
        }
    }
    
    private PdfDictionary getParentStructureElement() {
        PdfDictionary parent = null;
        if (this.getMcElements().size() > 0) {
            parent = this.pdf.getStructElement(this.getMcElements().get(this.getMcElements().size() - 1).getId());
        }
        if (parent == null) {
            parent = this.writer.getStructureTreeRoot();
        }
        return parent;
    }
    
    private PdfStructureElement openMCBlockInt(final IAccessibleElement element) {
        PdfStructureElement structureElement = null;
        if (this.isTagged()) {
            IAccessibleElement parent = null;
            if (this.getMcElements().size() > 0) {
                parent = this.getMcElements().get(this.getMcElements().size() - 1);
            }
            this.writer.checkElementRole(element, parent);
            if (element.getRole() != null) {
                if (!PdfName.ARTIFACT.equals(element.getRole())) {
                    structureElement = this.pdf.getStructElement(element.getId());
                    if (structureElement == null) {
                        structureElement = new PdfStructureElement(this.getParentStructureElement(), element.getRole(), element.getId());
                    }
                }
                if (PdfName.ARTIFACT.equals(element.getRole())) {
                    final HashMap<PdfName, PdfObject> properties = element.getAccessibleAttributes();
                    PdfDictionary propertiesDict = null;
                    if (properties != null && !properties.isEmpty()) {
                        propertiesDict = new PdfDictionary();
                        for (final Map.Entry<PdfName, PdfObject> entry : properties.entrySet()) {
                            propertiesDict.put(entry.getKey(), entry.getValue());
                        }
                    }
                    final boolean inTextLocal = this.inText;
                    if (this.inText) {
                        this.endText();
                    }
                    this.beginMarkedContentSequence(element.getRole(), propertiesDict, true);
                    if (inTextLocal) {
                        this.beginText(true);
                    }
                }
                else if (this.writer.needToBeMarkedInContent(element)) {
                    final boolean inTextLocal2 = this.inText;
                    if (this.inText) {
                        this.endText();
                    }
                    if (null != element.getAccessibleAttributes() && null != element.getAccessibleAttribute(PdfName.E)) {
                        this.beginMarkedContentSequence(structureElement, element.getAccessibleAttribute(PdfName.E).toString());
                        element.setAccessibleAttribute(PdfName.E, null);
                    }
                    else {
                        this.beginMarkedContentSequence(structureElement);
                    }
                    if (inTextLocal2) {
                        this.beginText(true);
                    }
                }
            }
        }
        return structureElement;
    }
    
    public void closeMCBlock(final IAccessibleElement element) {
        if (this.isTagged() && element != null && this.getMcElements().contains(element)) {
            this.closeMCBlockInt(element);
            this.getMcElements().remove(element);
        }
    }
    
    private void closeMCBlockInt(final IAccessibleElement element) {
        if (this.isTagged() && element.getRole() != null) {
            final PdfStructureElement structureElement = this.pdf.getStructElement(element.getId());
            if (structureElement != null) {
                structureElement.writeAttributes(element);
            }
            if (this.writer.needToBeMarkedInContent(element)) {
                final boolean inTextLocal = this.inText;
                if (this.inText) {
                    this.endText();
                }
                this.endMarkedContentSequence();
                if (inTextLocal) {
                    this.beginText(true);
                }
            }
        }
    }
    
    private void ensureDocumentTagIsOpen() {
        if (this.pdf.openMCDocument) {
            this.pdf.openMCDocument = false;
            this.writer.getDirectContentUnder().openMCBlock(this.pdf);
        }
    }
    
    protected ArrayList<IAccessibleElement> saveMCBlocks() {
        ArrayList<IAccessibleElement> mc = new ArrayList<IAccessibleElement>();
        if (this.isTagged()) {
            mc = this.getMcElements();
            for (int i = 0; i < mc.size(); ++i) {
                this.closeMCBlockInt(mc.get(i));
            }
            this.setMcElements(new ArrayList<IAccessibleElement>());
        }
        return mc;
    }
    
    protected void restoreMCBlocks(final ArrayList<IAccessibleElement> mcElements) {
        if (this.isTagged() && mcElements != null) {
            this.setMcElements(mcElements);
            for (int i = 0; i < this.getMcElements().size(); ++i) {
                this.openMCBlockInt(this.getMcElements().get(i));
            }
        }
    }
    
    protected int getMcDepth() {
        if (this.duplicatedFrom != null) {
            return this.duplicatedFrom.getMcDepth();
        }
        return this.mcDepth;
    }
    
    protected void setMcDepth(final int value) {
        if (this.duplicatedFrom != null) {
            this.duplicatedFrom.setMcDepth(value);
        }
        else {
            this.mcDepth = value;
        }
    }
    
    protected ArrayList<IAccessibleElement> getMcElements() {
        if (this.duplicatedFrom != null) {
            return this.duplicatedFrom.getMcElements();
        }
        return this.mcElements;
    }
    
    protected void setMcElements(final ArrayList<IAccessibleElement> value) {
        if (this.duplicatedFrom != null) {
            this.duplicatedFrom.setMcElements(value);
        }
        else {
            this.mcElements = value;
        }
    }
    
    protected void updateTx(final String text, final float Tj) {
        final GraphicState state = this.state;
        state.tx += this.getEffectiveStringWidth(text, false, Tj);
    }
    
    private void saveColor(final BaseColor color, final boolean fill) {
        if (fill) {
            this.state.colorFill = color;
        }
        else {
            this.state.colorStroke = color;
        }
    }
    
    protected boolean getInText() {
        return this.inText;
    }
    
    protected void checkState() {
        boolean stroke = false;
        boolean fill = false;
        if (this.state.textRenderMode == 0) {
            fill = true;
        }
        else if (this.state.textRenderMode == 1) {
            stroke = true;
        }
        else if (this.state.textRenderMode == 2) {
            fill = true;
            stroke = true;
        }
        if (fill) {
            PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorFill);
        }
        if (stroke) {
            PdfWriter.checkPdfIsoConformance(this.writer, 1, this.state.colorStroke);
        }
        PdfWriter.checkPdfIsoConformance(this.writer, 6, this.state.extGState);
    }
    
    @Deprecated
    public Graphics2D createGraphicsShapes(final float width, final float height) {
        return new PdfGraphics2D(this, width, height, true);
    }
    
    @Deprecated
    public Graphics2D createPrinterGraphicsShapes(final float width, final float height, final PrinterJob printerJob) {
        return new PdfPrinterGraphics2D(this, width, height, true, printerJob);
    }
    
    @Deprecated
    public Graphics2D createGraphics(final float width, final float height) {
        return new PdfGraphics2D(this, width, height);
    }
    
    @Deprecated
    public Graphics2D createPrinterGraphics(final float width, final float height, final PrinterJob printerJob) {
        return new PdfPrinterGraphics2D(this, width, height, printerJob);
    }
    
    @Deprecated
    public Graphics2D createGraphics(final float width, final float height, final boolean convertImagesToJPEG, final float quality) {
        return new PdfGraphics2D(this, width, height, null, false, convertImagesToJPEG, quality);
    }
    
    @Deprecated
    public Graphics2D createPrinterGraphics(final float width, final float height, final boolean convertImagesToJPEG, final float quality, final PrinterJob printerJob) {
        return new PdfPrinterGraphics2D(this, width, height, null, false, convertImagesToJPEG, quality, printerJob);
    }
    
    @Deprecated
    public Graphics2D createGraphicsShapes(final float width, final float height, final boolean convertImagesToJPEG, final float quality) {
        return new PdfGraphics2D(this, width, height, null, true, convertImagesToJPEG, quality);
    }
    
    @Deprecated
    public Graphics2D createPrinterGraphicsShapes(final float width, final float height, final boolean convertImagesToJPEG, final float quality, final PrinterJob printerJob) {
        return new PdfPrinterGraphics2D(this, width, height, null, true, convertImagesToJPEG, quality, printerJob);
    }
    
    @Deprecated
    public Graphics2D createGraphics(final float width, final float height, final FontMapper fontMapper) {
        return new PdfGraphics2D(this, width, height, fontMapper);
    }
    
    @Deprecated
    public Graphics2D createPrinterGraphics(final float width, final float height, final FontMapper fontMapper, final PrinterJob printerJob) {
        return new PdfPrinterGraphics2D(this, width, height, fontMapper, printerJob);
    }
    
    @Deprecated
    public Graphics2D createGraphics(final float width, final float height, final FontMapper fontMapper, final boolean convertImagesToJPEG, final float quality) {
        return new PdfGraphics2D(this, width, height, fontMapper, false, convertImagesToJPEG, quality);
    }
    
    @Deprecated
    public Graphics2D createPrinterGraphics(final float width, final float height, final FontMapper fontMapper, final boolean convertImagesToJPEG, final float quality, final PrinterJob printerJob) {
        return new PdfPrinterGraphics2D(this, width, height, fontMapper, false, convertImagesToJPEG, quality, printerJob);
    }
    
    @Deprecated
    public void addImage(final Image image, final java.awt.geom.AffineTransform transform) throws DocumentException {
        final double[] matrix = new double[6];
        transform.getMatrix(matrix);
        this.addImage(image, new AffineTransform(matrix));
    }
    
    @Deprecated
    public void addTemplate(final PdfTemplate template, final java.awt.geom.AffineTransform transform) {
        final double[] matrix = new double[6];
        transform.getMatrix(matrix);
        this.addTemplate(template, new AffineTransform(matrix));
    }
    
    @Deprecated
    public void concatCTM(final java.awt.geom.AffineTransform transform) {
        final double[] matrix = new double[6];
        transform.getMatrix(matrix);
        this.concatCTM(new AffineTransform(matrix));
    }
    
    @Deprecated
    public void setTextMatrix(final java.awt.geom.AffineTransform transform) {
        final double[] matrix = new double[6];
        transform.getMatrix(matrix);
        this.setTextMatrix(new AffineTransform(matrix));
    }
    
    @Deprecated
    public void transform(final java.awt.geom.AffineTransform af) {
        final double[] matrix = new double[6];
        af.getMatrix(matrix);
        this.transform(new AffineTransform(matrix));
    }
    
    static {
        unitRect = new float[] { 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f };
        (PdfContentByte.abrev = new HashMap<PdfName, String>()).put(PdfName.BITSPERCOMPONENT, "/BPC ");
        PdfContentByte.abrev.put(PdfName.COLORSPACE, "/CS ");
        PdfContentByte.abrev.put(PdfName.DECODE, "/D ");
        PdfContentByte.abrev.put(PdfName.DECODEPARMS, "/DP ");
        PdfContentByte.abrev.put(PdfName.FILTER, "/F ");
        PdfContentByte.abrev.put(PdfName.HEIGHT, "/H ");
        PdfContentByte.abrev.put(PdfName.IMAGEMASK, "/IM ");
        PdfContentByte.abrev.put(PdfName.INTENT, "/Intent ");
        PdfContentByte.abrev.put(PdfName.INTERPOLATE, "/I ");
        PdfContentByte.abrev.put(PdfName.WIDTH, "/W ");
    }
    
    public static class GraphicState
    {
        FontDetails fontDetails;
        ColorDetails colorDetails;
        float size;
        protected float xTLM;
        protected float yTLM;
        protected float aTLM;
        protected float bTLM;
        protected float cTLM;
        protected float dTLM;
        protected float tx;
        protected float leading;
        protected float scale;
        protected float charSpace;
        protected float wordSpace;
        protected BaseColor colorFill;
        protected BaseColor colorStroke;
        protected int textRenderMode;
        protected AffineTransform CTM;
        protected PdfObject extGState;
        
        GraphicState() {
            this.xTLM = 0.0f;
            this.yTLM = 0.0f;
            this.aTLM = 1.0f;
            this.bTLM = 0.0f;
            this.cTLM = 0.0f;
            this.dTLM = 1.0f;
            this.tx = 0.0f;
            this.leading = 0.0f;
            this.scale = 100.0f;
            this.charSpace = 0.0f;
            this.wordSpace = 0.0f;
            this.colorFill = new GrayColor(0);
            this.colorStroke = new GrayColor(0);
            this.textRenderMode = 0;
            this.CTM = new AffineTransform();
            this.extGState = null;
        }
        
        GraphicState(final GraphicState cp) {
            this.xTLM = 0.0f;
            this.yTLM = 0.0f;
            this.aTLM = 1.0f;
            this.bTLM = 0.0f;
            this.cTLM = 0.0f;
            this.dTLM = 1.0f;
            this.tx = 0.0f;
            this.leading = 0.0f;
            this.scale = 100.0f;
            this.charSpace = 0.0f;
            this.wordSpace = 0.0f;
            this.colorFill = new GrayColor(0);
            this.colorStroke = new GrayColor(0);
            this.textRenderMode = 0;
            this.CTM = new AffineTransform();
            this.extGState = null;
            this.copyParameters(cp);
        }
        
        void copyParameters(final GraphicState cp) {
            this.fontDetails = cp.fontDetails;
            this.colorDetails = cp.colorDetails;
            this.size = cp.size;
            this.xTLM = cp.xTLM;
            this.yTLM = cp.yTLM;
            this.aTLM = cp.aTLM;
            this.bTLM = cp.bTLM;
            this.cTLM = cp.cTLM;
            this.dTLM = cp.dTLM;
            this.tx = cp.tx;
            this.leading = cp.leading;
            this.scale = cp.scale;
            this.charSpace = cp.charSpace;
            this.wordSpace = cp.wordSpace;
            this.colorFill = cp.colorFill;
            this.colorStroke = cp.colorStroke;
            this.CTM = new AffineTransform(cp.CTM);
            this.textRenderMode = cp.textRenderMode;
            this.extGState = cp.extGState;
        }
        
        void restore(final GraphicState restore) {
            this.copyParameters(restore);
        }
    }
    
    static class UncoloredPattern extends PatternColor
    {
        protected BaseColor color;
        protected float tint;
        
        protected UncoloredPattern(final PdfPatternPainter p, final BaseColor color, final float tint) {
            super(p);
            this.color = color;
            this.tint = tint;
        }
        
        @Override
        public boolean equals(final Object obj) {
            return obj instanceof UncoloredPattern && ((UncoloredPattern)obj).painter.equals(this.painter) && ((UncoloredPattern)obj).color.equals(this.color) && ((UncoloredPattern)obj).tint == this.tint;
        }
    }
}
