// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public class PdfXConformanceException extends PdfIsoConformanceException
{
    private static final long serialVersionUID = 9199144538884293397L;
    
    public PdfXConformanceException() {
    }
    
    public PdfXConformanceException(final String s) {
        super(s);
    }
}
