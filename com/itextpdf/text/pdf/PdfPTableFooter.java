// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public class PdfPTableFooter extends PdfPTableBody
{
    protected PdfName role;
    
    public PdfPTableFooter() {
        this.role = PdfName.TFOOT;
    }
    
    @Override
    public PdfName getRole() {
        return this.role;
    }
    
    @Override
    public void setRole(final PdfName role) {
        this.role = role;
    }
}
