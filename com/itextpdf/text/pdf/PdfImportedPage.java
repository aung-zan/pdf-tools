// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.error_messages.MessageLocalization;
import java.io.IOException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;

public class PdfImportedPage extends PdfTemplate
{
    PdfReaderInstance readerInstance;
    int pageNumber;
    int rotation;
    protected boolean toCopy;
    
    PdfImportedPage(final PdfReaderInstance readerInstance, final PdfWriter writer, final int pageNumber) {
        this.toCopy = true;
        this.readerInstance = readerInstance;
        this.pageNumber = pageNumber;
        this.writer = writer;
        this.rotation = readerInstance.getReader().getPageRotation(pageNumber);
        this.bBox = readerInstance.getReader().getPageSize(pageNumber);
        this.setMatrix(1.0f, 0.0f, 0.0f, 1.0f, -this.bBox.getLeft(), -this.bBox.getBottom());
        this.type = 2;
    }
    
    public PdfImportedPage getFromReader() {
        return this;
    }
    
    public int getPageNumber() {
        return this.pageNumber;
    }
    
    public int getRotation() {
        return this.rotation;
    }
    
    @Override
    public void addImage(final Image image, final float a, final float b, final float c, final float d, final float e, final float f) throws DocumentException {
        this.throwError();
    }
    
    @Override
    public void addTemplate(final PdfTemplate template, final float a, final float b, final float c, final float d, final float e, final float f) {
        this.throwError();
    }
    
    @Override
    public PdfContentByte getDuplicate() {
        this.throwError();
        return null;
    }
    
    @Override
    public PdfStream getFormXObject(final int compressionLevel) throws IOException {
        return this.readerInstance.getFormXObject(this.pageNumber, compressionLevel);
    }
    
    @Override
    public void setColorFill(final PdfSpotColor sp, final float tint) {
        this.throwError();
    }
    
    @Override
    public void setColorStroke(final PdfSpotColor sp, final float tint) {
        this.throwError();
    }
    
    @Override
    PdfObject getResources() {
        return this.readerInstance.getResources(this.pageNumber);
    }
    
    @Override
    public void setFontAndSize(final BaseFont bf, final float size) {
        this.throwError();
    }
    
    @Override
    public void setGroup(final PdfTransparencyGroup group) {
        this.throwError();
    }
    
    void throwError() {
        throw new RuntimeException(MessageLocalization.getComposedMessage("content.can.not.be.added.to.a.pdfimportedpage", new Object[0]));
    }
    
    PdfReaderInstance getPdfReaderInstance() {
        return this.readerInstance;
    }
    
    public boolean isToCopy() {
        return this.toCopy;
    }
    
    public void setCopied() {
        this.toCopy = false;
    }
}
