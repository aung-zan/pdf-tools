// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.HashMap;
import java.util.ArrayList;
import com.itextpdf.text.AccessibleElementId;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;

public class PdfPTableBody implements IAccessibleElement
{
    protected AccessibleElementId id;
    protected ArrayList<PdfPRow> rows;
    protected PdfName role;
    protected HashMap<PdfName, PdfObject> accessibleAttributes;
    
    public PdfPTableBody() {
        this.id = new AccessibleElementId();
        this.rows = null;
        this.role = PdfName.TBODY;
        this.accessibleAttributes = null;
    }
    
    @Override
    public PdfObject getAccessibleAttribute(final PdfName key) {
        if (this.accessibleAttributes != null) {
            return this.accessibleAttributes.get(key);
        }
        return null;
    }
    
    @Override
    public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
        if (this.accessibleAttributes == null) {
            this.accessibleAttributes = new HashMap<PdfName, PdfObject>();
        }
        this.accessibleAttributes.put(key, value);
    }
    
    @Override
    public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
        return this.accessibleAttributes;
    }
    
    @Override
    public PdfName getRole() {
        return this.role;
    }
    
    @Override
    public void setRole(final PdfName role) {
        this.role = role;
    }
    
    @Override
    public AccessibleElementId getId() {
        return this.id;
    }
    
    @Override
    public void setId(final AccessibleElementId id) {
        this.id = id;
    }
    
    @Override
    public boolean isInline() {
        return false;
    }
}
