// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.DocumentException;
import java.io.IOException;
import java.util.Calendar;
import com.itextpdf.text.pdf.security.XpathConstructor;
import com.itextpdf.text.pdf.security.XmlLocator;
import java.security.cert.Certificate;

public class XmlSignatureAppearance
{
    private PdfStamperImp writer;
    private PdfStamper stamper;
    private Certificate signCertificate;
    private XmlLocator xmlLocator;
    private XpathConstructor xpathConstructor;
    private Calendar signDate;
    private String description;
    private String mimeType;
    
    XmlSignatureAppearance(final PdfStamperImp writer) {
        this.mimeType = "text/xml";
        this.writer = writer;
    }
    
    public PdfStamperImp getWriter() {
        return this.writer;
    }
    
    public PdfStamper getStamper() {
        return this.stamper;
    }
    
    public void setStamper(final PdfStamper stamper) {
        this.stamper = stamper;
    }
    
    public void setCertificate(final Certificate signCertificate) {
        this.signCertificate = signCertificate;
    }
    
    public Certificate getCertificate() {
        return this.signCertificate;
    }
    
    public void setDescription(final String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public String getMimeType() {
        return this.mimeType;
    }
    
    public void setMimeType(final String mimeType) {
        this.mimeType = mimeType;
    }
    
    public Calendar getSignDate() {
        if (this.signDate == null) {
            this.signDate = Calendar.getInstance();
        }
        return this.signDate;
    }
    
    public void setSignDate(final Calendar signDate) {
        this.signDate = signDate;
    }
    
    public XmlLocator getXmlLocator() {
        return this.xmlLocator;
    }
    
    public void setXmlLocator(final XmlLocator xmlLocator) {
        this.xmlLocator = xmlLocator;
    }
    
    public XpathConstructor getXpathConstructor() {
        return this.xpathConstructor;
    }
    
    public void setXpathConstructor(final XpathConstructor xpathConstructor) {
        this.xpathConstructor = xpathConstructor;
    }
    
    public void close() throws IOException, DocumentException {
        this.writer.close(this.stamper.getMoreInfo());
    }
}
