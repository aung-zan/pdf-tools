// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.io.IOException;
import com.itextpdf.text.DocumentException;
import java.util.Collection;
import java.util.HashSet;
import com.itextpdf.text.log.LoggerFactory;
import java.util.Comparator;
import java.util.Arrays;
import java.util.HashMap;

public class TtfUnicodeWriter
{
    protected PdfWriter writer;
    
    public TtfUnicodeWriter(final PdfWriter writer) {
        this.writer = null;
        this.writer = writer;
    }
    
    public void writeFont(final TrueTypeFontUnicode font, final PdfIndirectReference ref, final Object[] params, final byte[] rotbits) throws DocumentException, IOException {
        final HashMap<Integer, int[]> longTag = (HashMap<Integer, int[]>)params[0];
        font.addRangeUni(longTag, true, font.subset);
        int[][] metrics = longTag.values().toArray(new int[0][]);
        Arrays.sort(metrics, font);
        PdfIndirectReference ind_font;
        if (font.cff) {
            byte[] b = font.readCffFont();
            if (font.subset || font.subsetRanges != null) {
                final CFFFontSubset cff = new CFFFontSubset(new RandomAccessFileOrArray(b), longTag);
                try {
                    b = cff.Process(cff.getNames()[0]);
                }
                catch (Exception e) {
                    LoggerFactory.getLogger(TtfUnicodeWriter.class).error("Issue in CFF font subsetting.Subsetting was disabled", e);
                    font.setSubset(false);
                    font.addRangeUni(longTag, true, font.subset);
                    metrics = longTag.values().toArray(new int[0][]);
                    Arrays.sort(metrics, font);
                }
            }
            final PdfObject pobj = new BaseFont.StreamFont(b, "CIDFontType0C", font.compressionLevel);
            final PdfIndirectObject obj = this.writer.addToBody(pobj);
            ind_font = obj.getIndirectReference();
        }
        else {
            byte[] b;
            if (font.subset || font.directoryOffset != 0) {
                synchronized (font.rf) {
                    final TrueTypeFontSubSet sb = new TrueTypeFontSubSet(font.fileName, new RandomAccessFileOrArray(font.rf), new HashSet<Integer>(longTag.keySet()), font.directoryOffset, true, false);
                    b = sb.process();
                }
            }
            else {
                b = font.getFullFont();
            }
            final int[] lengths = { b.length };
            final PdfObject pobj = new BaseFont.StreamFont(b, lengths, font.compressionLevel);
            final PdfIndirectObject obj = this.writer.addToBody(pobj);
            ind_font = obj.getIndirectReference();
        }
        String subsetPrefix = "";
        if (font.subset) {
            subsetPrefix = BaseFont.createSubsetPrefix();
        }
        final PdfDictionary dic = font.getFontDescriptor(ind_font, subsetPrefix, null);
        PdfIndirectObject obj = this.writer.addToBody(dic);
        ind_font = obj.getIndirectReference();
        PdfObject pobj = font.getCIDFontType2(ind_font, subsetPrefix, metrics);
        obj = this.writer.addToBody(pobj);
        ind_font = obj.getIndirectReference();
        pobj = font.getToUnicode(metrics);
        PdfIndirectReference toUnicodeRef = null;
        if (pobj != null) {
            obj = this.writer.addToBody(pobj);
            toUnicodeRef = obj.getIndirectReference();
        }
        pobj = font.getFontBaseType(ind_font, subsetPrefix, toUnicodeRef);
        this.writer.addToBody(pobj, ref);
    }
}
