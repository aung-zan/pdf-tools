// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.hyphenation;

public class HyphenationException extends Exception
{
    private static final long serialVersionUID = 4721513606846982325L;
    
    public HyphenationException(final String msg) {
        super(msg);
    }
}
