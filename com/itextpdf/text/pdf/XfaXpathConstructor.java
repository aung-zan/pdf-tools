// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.pdf.security.XpathConstructor;

public class XfaXpathConstructor implements XpathConstructor
{
    private final String CONFIG = "config";
    private final String CONNECTIONSET = "connectionSet";
    private final String DATASETS = "datasets";
    private final String LOCALESET = "localeSet";
    private final String PDF = "pdf";
    private final String SOURCESET = "sourceSet";
    private final String STYLESHEET = "stylesheet";
    private final String TEMPLATE = "template";
    private final String XDC = "xdc";
    private final String XFDF = "xfdf";
    private final String XMPMETA = "xmpmeta";
    private String xpathExpression;
    
    public XfaXpathConstructor() {
        this.xpathExpression = "";
    }
    
    public XfaXpathConstructor(final XdpPackage xdpPackage) {
        String strPackage = null;
        switch (xdpPackage) {
            case Config: {
                strPackage = "config";
                break;
            }
            case ConnectionSet: {
                strPackage = "connectionSet";
                break;
            }
            case Datasets: {
                strPackage = "datasets";
                break;
            }
            case LocaleSet: {
                strPackage = "localeSet";
                break;
            }
            case Pdf: {
                strPackage = "pdf";
                break;
            }
            case SourceSet: {
                strPackage = "sourceSet";
                break;
            }
            case Stylesheet: {
                strPackage = "stylesheet";
                break;
            }
            case Template: {
                strPackage = "template";
                break;
            }
            case Xdc: {
                strPackage = "xdc";
                break;
            }
            case Xfdf: {
                strPackage = "xfdf";
                break;
            }
            case Xmpmeta: {
                strPackage = "xmpmeta";
                break;
            }
            default: {
                this.xpathExpression = "";
                return;
            }
        }
        final StringBuilder builder = new StringBuilder("/xdp:xdp/*[local-name()='");
        builder.append(strPackage);
        builder.append("']");
        this.xpathExpression = builder.toString();
    }
    
    @Override
    public String getXpathExpression() {
        return this.xpathExpression;
    }
    
    public enum XdpPackage
    {
        Config, 
        ConnectionSet, 
        Datasets, 
        LocaleSet, 
        Pdf, 
        SourceSet, 
        Stylesheet, 
        Template, 
        Xdc, 
        Xfdf, 
        Xmpmeta;
    }
}
