// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.ExceptionConverter;

class PdfFont implements Comparable<PdfFont>
{
    private BaseFont font;
    private float size;
    protected float hScale;
    
    PdfFont(final BaseFont bf, final float size) {
        this.hScale = 1.0f;
        this.size = size;
        this.font = bf;
    }
    
    @Override
    public int compareTo(final PdfFont pdfFont) {
        if (pdfFont == null) {
            return -1;
        }
        try {
            if (this.font != pdfFont.font) {
                return 1;
            }
            if (this.size() != pdfFont.size()) {
                return 2;
            }
            return 0;
        }
        catch (ClassCastException cce) {
            return -2;
        }
    }
    
    float size() {
        return this.size;
    }
    
    float width() {
        return this.width(32);
    }
    
    float width(final int character) {
        return this.font.getWidthPoint(character, this.size) * this.hScale;
    }
    
    float width(final String s) {
        return this.font.getWidthPoint(s, this.size) * this.hScale;
    }
    
    BaseFont getFont() {
        return this.font;
    }
    
    static PdfFont getDefaultFont() {
        try {
            final BaseFont bf = BaseFont.createFont("Helvetica", "Cp1252", false);
            return new PdfFont(bf, 12.0f);
        }
        catch (Exception ee) {
            throw new ExceptionConverter(ee);
        }
    }
    
    void setHorizontalScaling(final float hScale) {
        this.hScale = hScale;
    }
    
    float getHorizontalScaling() {
        return this.hScale;
    }
}
