// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

class ColorDetails
{
    PdfIndirectReference indirectReference;
    PdfName colorSpaceName;
    ICachedColorSpace colorSpace;
    
    ColorDetails(final PdfName colorName, final PdfIndirectReference indirectReference, final ICachedColorSpace scolor) {
        this.colorSpaceName = colorName;
        this.indirectReference = indirectReference;
        this.colorSpace = scolor;
    }
    
    public PdfIndirectReference getIndirectReference() {
        return this.indirectReference;
    }
    
    PdfName getColorSpaceName() {
        return this.colorSpaceName;
    }
    
    public PdfObject getPdfObject(final PdfWriter writer) {
        return this.colorSpace.getPdfObject(writer);
    }
}
