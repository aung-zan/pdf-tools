// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.error_messages.MessageLocalization;

public final class BidiOrder
{
    private byte[] initialTypes;
    private byte[] embeddings;
    private byte paragraphEmbeddingLevel;
    private int textLength;
    private byte[] resultTypes;
    private byte[] resultLevels;
    public static final byte L = 0;
    public static final byte LRE = 1;
    public static final byte LRO = 2;
    public static final byte R = 3;
    public static final byte AL = 4;
    public static final byte RLE = 5;
    public static final byte RLO = 6;
    public static final byte PDF = 7;
    public static final byte EN = 8;
    public static final byte ES = 9;
    public static final byte ET = 10;
    public static final byte AN = 11;
    public static final byte CS = 12;
    public static final byte NSM = 13;
    public static final byte BN = 14;
    public static final byte B = 15;
    public static final byte S = 16;
    public static final byte WS = 17;
    public static final byte ON = 18;
    public static final byte TYPE_MIN = 0;
    public static final byte TYPE_MAX = 18;
    private static final byte[] rtypes;
    private static char[] baseTypes;
    
    public BidiOrder(final byte[] types) {
        this.paragraphEmbeddingLevel = -1;
        validateTypes(types);
        this.initialTypes = types.clone();
        this.runAlgorithm();
    }
    
    public BidiOrder(final byte[] types, final byte paragraphEmbeddingLevel) {
        this.paragraphEmbeddingLevel = -1;
        validateTypes(types);
        validateParagraphEmbeddingLevel(paragraphEmbeddingLevel);
        this.initialTypes = types.clone();
        this.paragraphEmbeddingLevel = paragraphEmbeddingLevel;
        this.runAlgorithm();
    }
    
    public BidiOrder(final char[] text, final int offset, final int length, final byte paragraphEmbeddingLevel) {
        this.paragraphEmbeddingLevel = -1;
        this.initialTypes = new byte[length];
        for (int k = 0; k < length; ++k) {
            this.initialTypes[k] = BidiOrder.rtypes[text[offset + k]];
        }
        validateParagraphEmbeddingLevel(paragraphEmbeddingLevel);
        this.paragraphEmbeddingLevel = paragraphEmbeddingLevel;
        this.runAlgorithm();
    }
    
    public static final byte getDirection(final char c) {
        return BidiOrder.rtypes[c];
    }
    
    private void runAlgorithm() {
        this.textLength = this.initialTypes.length;
        this.resultTypes = this.initialTypes.clone();
        if (this.paragraphEmbeddingLevel == -1) {
            this.determineParagraphEmbeddingLevel();
        }
        this.resultLevels = new byte[this.textLength];
        this.setLevels(0, this.textLength, this.paragraphEmbeddingLevel);
        this.determineExplicitEmbeddingLevels();
        this.textLength = this.removeExplicitCodes();
        byte prevLevel = this.paragraphEmbeddingLevel;
        int limit;
        for (int start = 0; start < this.textLength; start = limit) {
            final byte level = this.resultLevels[start];
            final byte prevType = typeForLevel(Math.max(prevLevel, level));
            for (limit = start + 1; limit < this.textLength && this.resultLevels[limit] == level; ++limit) {}
            final byte succLevel = (limit < this.textLength) ? this.resultLevels[limit] : this.paragraphEmbeddingLevel;
            final byte succType = typeForLevel(Math.max(succLevel, level));
            this.resolveWeakTypes(start, limit, level, prevType, succType);
            this.resolveNeutralTypes(start, limit, level, prevType, succType);
            this.resolveImplicitLevels(start, limit, level, prevType, succType);
            prevLevel = level;
        }
        this.textLength = this.reinsertExplicitCodes(this.textLength);
    }
    
    private void determineParagraphEmbeddingLevel() {
        byte strongType = -1;
        for (int i = 0; i < this.textLength; ++i) {
            final byte t = this.resultTypes[i];
            if (t == 0 || t == 4 || t == 3) {
                strongType = t;
                break;
            }
        }
        if (strongType == -1) {
            this.paragraphEmbeddingLevel = 0;
        }
        else if (strongType == 0) {
            this.paragraphEmbeddingLevel = 0;
        }
        else {
            this.paragraphEmbeddingLevel = 1;
        }
    }
    
    private void determineExplicitEmbeddingLevels() {
        this.embeddings = processEmbeddings(this.resultTypes, this.paragraphEmbeddingLevel);
        for (int i = 0; i < this.textLength; ++i) {
            byte level = this.embeddings[i];
            if ((level & 0x80) != 0x0) {
                level &= 0x7F;
                this.resultTypes[i] = typeForLevel(level);
            }
            this.resultLevels[i] = level;
        }
    }
    
    private int removeExplicitCodes() {
        int w = 0;
        for (int i = 0; i < this.textLength; ++i) {
            final byte t = this.initialTypes[i];
            if (t != 1 && t != 5 && t != 2 && t != 6 && t != 7 && t != 14) {
                this.embeddings[w] = this.embeddings[i];
                this.resultTypes[w] = this.resultTypes[i];
                this.resultLevels[w] = this.resultLevels[i];
                ++w;
            }
        }
        return w;
    }
    
    private int reinsertExplicitCodes(int textLength) {
        int i = this.initialTypes.length;
        while (--i >= 0) {
            final byte t = this.initialTypes[i];
            if (t == 1 || t == 5 || t == 2 || t == 6 || t == 7 || t == 14) {
                this.embeddings[i] = 0;
                this.resultTypes[i] = t;
                this.resultLevels[i] = -1;
            }
            else {
                --textLength;
                this.embeddings[i] = this.embeddings[textLength];
                this.resultTypes[i] = this.resultTypes[textLength];
                this.resultLevels[i] = this.resultLevels[textLength];
            }
        }
        if (this.resultLevels[0] == -1) {
            this.resultLevels[0] = this.paragraphEmbeddingLevel;
        }
        for (i = 1; i < this.initialTypes.length; ++i) {
            if (this.resultLevels[i] == -1) {
                this.resultLevels[i] = this.resultLevels[i - 1];
            }
        }
        return this.initialTypes.length;
    }
    
    private static byte[] processEmbeddings(final byte[] resultTypes, final byte paragraphEmbeddingLevel) {
        final int EXPLICIT_LEVEL_LIMIT = 62;
        final int textLength = resultTypes.length;
        final byte[] embeddings = new byte[textLength];
        final byte[] embeddingValueStack = new byte[62];
        int stackCounter = 0;
        int overflowAlmostCounter = 0;
        int overflowCounter = 0;
        byte currentEmbeddingLevel = paragraphEmbeddingLevel;
        byte currentEmbeddingValue = paragraphEmbeddingLevel;
        for (int i = 0; i < textLength; ++i) {
            embeddings[i] = currentEmbeddingValue;
            final byte t = resultTypes[i];
            switch (t) {
                case 1:
                case 2:
                case 5:
                case 6: {
                    if (overflowCounter == 0) {
                        byte newLevel;
                        if (t == 5 || t == 6) {
                            newLevel = (byte)(currentEmbeddingLevel + 1 | 0x1);
                        }
                        else {
                            newLevel = (byte)(currentEmbeddingLevel + 2 & 0xFFFFFFFE);
                        }
                        if (newLevel < 62) {
                            embeddingValueStack[stackCounter] = currentEmbeddingValue;
                            ++stackCounter;
                            currentEmbeddingLevel = newLevel;
                            if (t == 2 || t == 6) {
                                currentEmbeddingValue = (byte)(newLevel | 0x80);
                            }
                            else {
                                currentEmbeddingValue = newLevel;
                            }
                            embeddings[i] = currentEmbeddingValue;
                            break;
                        }
                        if (currentEmbeddingLevel == 60) {
                            ++overflowAlmostCounter;
                            break;
                        }
                    }
                    ++overflowCounter;
                    break;
                }
                case 7: {
                    if (overflowCounter > 0) {
                        --overflowCounter;
                        break;
                    }
                    if (overflowAlmostCounter > 0 && currentEmbeddingLevel != 61) {
                        --overflowAlmostCounter;
                        break;
                    }
                    if (stackCounter > 0) {
                        --stackCounter;
                        currentEmbeddingValue = embeddingValueStack[stackCounter];
                        currentEmbeddingLevel = (byte)(currentEmbeddingValue & 0x7F);
                        break;
                    }
                    break;
                }
                case 15: {
                    stackCounter = 0;
                    overflowCounter = 0;
                    overflowAlmostCounter = 0;
                    currentEmbeddingLevel = paragraphEmbeddingLevel;
                    currentEmbeddingValue = paragraphEmbeddingLevel;
                    embeddings[i] = paragraphEmbeddingLevel;
                    break;
                }
            }
        }
        return embeddings;
    }
    
    private void resolveWeakTypes(final int start, final int limit, final byte level, final byte sor, final byte eor) {
        byte preceedingCharacterType = sor;
        for (int i = start; i < limit; ++i) {
            final byte t = this.resultTypes[i];
            if (t == 13) {
                this.resultTypes[i] = preceedingCharacterType;
            }
            else {
                preceedingCharacterType = t;
            }
        }
        for (int i = start; i < limit; ++i) {
            if (this.resultTypes[i] == 8) {
                int j = i - 1;
                while (j >= start) {
                    final byte t2 = this.resultTypes[j];
                    if (t2 == 0 || t2 == 3 || t2 == 4) {
                        if (t2 == 4) {
                            this.resultTypes[i] = 11;
                            break;
                        }
                        break;
                    }
                    else {
                        --j;
                    }
                }
            }
        }
        for (int i = start; i < limit; ++i) {
            if (this.resultTypes[i] == 4) {
                this.resultTypes[i] = 3;
            }
        }
        for (int i = start + 1; i < limit - 1; ++i) {
            if (this.resultTypes[i] == 9 || this.resultTypes[i] == 12) {
                final byte prevSepType = this.resultTypes[i - 1];
                final byte succSepType = this.resultTypes[i + 1];
                if (prevSepType == 8 && succSepType == 8) {
                    this.resultTypes[i] = 8;
                }
                else if (this.resultTypes[i] == 12 && prevSepType == 11 && succSepType == 11) {
                    this.resultTypes[i] = 11;
                }
            }
        }
        for (int i = start; i < limit; ++i) {
            if (this.resultTypes[i] == 10) {
                final int runstart = i;
                final int runlimit = this.findRunLimit(runstart, limit, new byte[] { 10 });
                byte t3 = (runstart == start) ? sor : this.resultTypes[runstart - 1];
                if (t3 != 8) {
                    t3 = ((runlimit == limit) ? eor : this.resultTypes[runlimit]);
                }
                if (t3 == 8) {
                    this.setTypes(runstart, runlimit, (byte)8);
                }
                i = runlimit;
            }
        }
        for (int i = start; i < limit; ++i) {
            final byte t = this.resultTypes[i];
            if (t == 9 || t == 10 || t == 12) {
                this.resultTypes[i] = 18;
            }
        }
        for (int i = start; i < limit; ++i) {
            if (this.resultTypes[i] == 8) {
                byte prevStrongType = sor;
                for (int k = i - 1; k >= start; --k) {
                    final byte t3 = this.resultTypes[k];
                    if (t3 == 0 || t3 == 3) {
                        prevStrongType = t3;
                        break;
                    }
                }
                if (prevStrongType == 0) {
                    this.resultTypes[i] = 0;
                }
            }
        }
    }
    
    private void resolveNeutralTypes(final int start, final int limit, final byte level, final byte sor, final byte eor) {
        for (int i = start; i < limit; ++i) {
            final byte t = this.resultTypes[i];
            if (t == 17 || t == 18 || t == 15 || t == 16) {
                final int runstart = i;
                final int runlimit = this.findRunLimit(runstart, limit, new byte[] { 15, 16, 17, 18 });
                byte leadingType;
                if (runstart == start) {
                    leadingType = sor;
                }
                else {
                    leadingType = this.resultTypes[runstart - 1];
                    if (leadingType != 0) {
                        if (leadingType != 3) {
                            if (leadingType == 11) {
                                leadingType = 3;
                            }
                            else if (leadingType == 8) {
                                leadingType = 3;
                            }
                        }
                    }
                }
                byte trailingType;
                if (runlimit == limit) {
                    trailingType = eor;
                }
                else {
                    trailingType = this.resultTypes[runlimit];
                    if (trailingType != 0) {
                        if (trailingType != 3) {
                            if (trailingType == 11) {
                                trailingType = 3;
                            }
                            else if (trailingType == 8) {
                                trailingType = 3;
                            }
                        }
                    }
                }
                byte resolvedType;
                if (leadingType == trailingType) {
                    resolvedType = leadingType;
                }
                else {
                    resolvedType = typeForLevel(level);
                }
                this.setTypes(runstart, runlimit, resolvedType);
                i = runlimit;
            }
        }
    }
    
    private void resolveImplicitLevels(final int start, final int limit, final byte level, final byte sor, final byte eor) {
        if ((level & 0x1) == 0x0) {
            for (int i = start; i < limit; ++i) {
                final byte t = this.resultTypes[i];
                if (t != 0) {
                    if (t == 3) {
                        final byte[] resultLevels = this.resultLevels;
                        final int n = i;
                        ++resultLevels[n];
                    }
                    else {
                        final byte[] resultLevels2 = this.resultLevels;
                        final int n2 = i;
                        resultLevels2[n2] += 2;
                    }
                }
            }
        }
        else {
            for (int i = start; i < limit; ++i) {
                final byte t = this.resultTypes[i];
                if (t != 3) {
                    final byte[] resultLevels3 = this.resultLevels;
                    final int n3 = i;
                    ++resultLevels3[n3];
                }
            }
        }
    }
    
    public byte[] getLevels() {
        return this.getLevels(new int[] { this.textLength });
    }
    
    public byte[] getLevels(final int[] linebreaks) {
        validateLineBreaks(linebreaks, this.textLength);
        final byte[] result = this.resultLevels.clone();
        for (int i = 0; i < result.length; ++i) {
            final byte t = this.initialTypes[i];
            if (t == 15 || t == 16) {
                result[i] = this.paragraphEmbeddingLevel;
                for (int j = i - 1; j >= 0 && isWhitespace(this.initialTypes[j]); --j) {
                    result[j] = this.paragraphEmbeddingLevel;
                }
            }
        }
        int start = 0;
        for (int k = 0; k < linebreaks.length; ++k) {
            final int limit = linebreaks[k];
            for (int l = limit - 1; l >= start && isWhitespace(this.initialTypes[l]); --l) {
                result[l] = this.paragraphEmbeddingLevel;
            }
            start = limit;
        }
        return result;
    }
    
    public int[] getReordering(final int[] linebreaks) {
        validateLineBreaks(linebreaks, this.textLength);
        final byte[] levels = this.getLevels(linebreaks);
        return computeMultilineReordering(levels, linebreaks);
    }
    
    private static int[] computeMultilineReordering(final byte[] levels, final int[] linebreaks) {
        final int[] result = new int[levels.length];
        int start = 0;
        for (int i = 0; i < linebreaks.length; ++i) {
            final int limit = linebreaks[i];
            final byte[] templevels = new byte[limit - start];
            System.arraycopy(levels, start, templevels, 0, templevels.length);
            final int[] temporder = computeReordering(templevels);
            for (int j = 0; j < temporder.length; ++j) {
                result[start + j] = temporder[j] + start;
            }
            start = limit;
        }
        return result;
    }
    
    private static int[] computeReordering(final byte[] levels) {
        final int lineLength = levels.length;
        final int[] result = new int[lineLength];
        for (int i = 0; i < lineLength; ++i) {
            result[i] = i;
        }
        byte highestLevel = 0;
        byte lowestOddLevel = 63;
        for (final byte level : levels) {
            if (level > highestLevel) {
                highestLevel = level;
            }
            if ((level & 0x1) != 0x0 && level < lowestOddLevel) {
                lowestOddLevel = level;
            }
        }
        for (int level2 = highestLevel; level2 >= lowestOddLevel; --level2) {
            for (int k = 0; k < lineLength; ++k) {
                if (levels[k] >= level2) {
                    final int start = k;
                    int limit;
                    for (limit = k + 1; limit < lineLength && levels[limit] >= level2; ++limit) {}
                    for (int l = start, m = limit - 1; l < m; ++l, --m) {
                        final int temp = result[l];
                        result[l] = result[m];
                        result[m] = temp;
                    }
                    k = limit;
                }
            }
        }
        return result;
    }
    
    public byte getBaseLevel() {
        return this.paragraphEmbeddingLevel;
    }
    
    private static boolean isWhitespace(final byte biditype) {
        switch (biditype) {
            case 1:
            case 2:
            case 5:
            case 6:
            case 7:
            case 14:
            case 17: {
                return true;
            }
            default: {
                return false;
            }
        }
    }
    
    private static byte typeForLevel(final int level) {
        return (byte)(((level & 0x1) == 0x0) ? 0 : 3);
    }
    
    private int findRunLimit(int index, final int limit, final byte[] validSet) {
        --index;
    Label_0003:
        while (++index < limit) {
            final byte t = this.resultTypes[index];
            for (int i = 0; i < validSet.length; ++i) {
                if (t == validSet[i]) {
                    continue Label_0003;
                }
            }
            return index;
        }
        return limit;
    }
    
    private int findRunStart(int index, final byte[] validSet) {
    Label_0000:
        while (--index >= 0) {
            final byte t = this.resultTypes[index];
            for (int i = 0; i < validSet.length; ++i) {
                if (t == validSet[i]) {
                    continue Label_0000;
                }
            }
            return index + 1;
        }
        return 0;
    }
    
    private void setTypes(final int start, final int limit, final byte newType) {
        for (int i = start; i < limit; ++i) {
            this.resultTypes[i] = newType;
        }
    }
    
    private void setLevels(final int start, final int limit, final byte newLevel) {
        for (int i = start; i < limit; ++i) {
            this.resultLevels[i] = newLevel;
        }
    }
    
    private static void validateTypes(final byte[] types) {
        if (types == null) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("types.is.null", new Object[0]));
        }
        for (int i = 0; i < types.length; ++i) {
            if (types[i] < 0 || types[i] > 18) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("illegal.type.value.at.1.2", String.valueOf(i), String.valueOf(types[i])));
            }
        }
        for (int i = 0; i < types.length - 1; ++i) {
            if (types[i] == 15) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("b.type.before.end.of.paragraph.at.index.1", i));
            }
        }
    }
    
    private static void validateParagraphEmbeddingLevel(final byte paragraphEmbeddingLevel) {
        if (paragraphEmbeddingLevel != -1 && paragraphEmbeddingLevel != 0 && paragraphEmbeddingLevel != 1) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("illegal.paragraph.embedding.level.1", paragraphEmbeddingLevel));
        }
    }
    
    private static void validateLineBreaks(final int[] linebreaks, final int textLength) {
        int prev = 0;
        for (int i = 0; i < linebreaks.length; ++i) {
            final int next = linebreaks[i];
            if (next <= prev) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("bad.linebreak.1.at.index.2", String.valueOf(next), String.valueOf(i)));
            }
            prev = next;
        }
        if (prev != textLength) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("last.linebreak.must.be.at.1", textLength));
        }
    }
    
    static {
        rtypes = new byte[65536];
        BidiOrder.baseTypes = new char[] { '\0', '\b', '\u000e', '\t', '\t', '\u0010', '\n', '\n', '\u000f', '\u000b', '\u000b', '\u0010', '\f', '\f', '\u0011', '\r', '\r', '\u000f', '\u000e', '\u001b', '\u000e', '\u001c', '\u001e', '\u000f', '\u001f', '\u001f', '\u0010', ' ', ' ', '\u0011', '!', '\"', '\u0012', '#', '%', '\n', '&', '*', '\u0012', '+', '+', '\n', ',', ',', '\f', '-', '-', '\n', '.', '.', '\f', '/', '/', '\t', '0', '9', '\b', ':', ':', '\f', ';', '@', '\u0012', 'A', 'Z', '\0', '[', '`', '\u0012', 'a', 'z', '\0', '{', '~', '\u0012', '\u007f', '\u0084', '\u000e', '\u0085', '\u0085', '\u000f', '\u0086', '\u009f', '\u000e', ' ', ' ', '\f', '¡', '¡', '\u0012', '¢', '¥', '\n', '¦', '©', '\u0012', 'ª', 'ª', '\0', '«', '¯', '\u0012', '°', '±', '\n', '²', '³', '\b', '´', '´', '\u0012', 'µ', 'µ', '\0', '¶', '¸', '\u0012', '¹', '¹', '\b', 'º', 'º', '\0', '»', '¿', '\u0012', '\u00c0', '\u00d6', '\0', '\u00d7', '\u00d7', '\u0012', '\u00d8', '\u00f6', '\0', '\u00f7', '\u00f7', '\u0012', '\u00f8', '\u02b8', '\0', '\u02b9', '\u02ba', '\u0012', '\u02bb', '\u02c1', '\0', '\u02c2', '\u02cf', '\u0012', '\u02d0', '\u02d1', '\0', '\u02d2', '\u02df', '\u0012', '\u02e0', '\u02e4', '\0', '\u02e5', '\u02ed', '\u0012', '\u02ee', '\u02ee', '\0', '\u02ef', '\u02ff', '\u0012', '\u0300', '\u0357', '\r', '\u0358', '\u035c', '\0', '\u035d', '\u036f', '\r', '\u0370', '\u0373', '\0', '\u0374', '\u0375', '\u0012', '\u0376', '\u037d', '\0', '\u037e', '\u037e', '\u0012', '\u037f', '\u0383', '\0', '\u0384', '\u0385', '\u0012', '\u0386', '\u0386', '\0', '\u0387', '\u0387', '\u0012', '\u0388', '\u03f5', '\0', '\u03f6', '\u03f6', '\u0012', '\u03f7', '\u0482', '\0', '\u0483', '\u0486', '\r', '\u0487', '\u0487', '\0', '\u0488', '\u0489', '\r', '\u048a', '\u0589', '\0', '\u058a', '\u058a', '\u0012', '\u058b', '\u0590', '\0', '\u0591', '\u05a1', '\r', '\u05a2', '\u05a2', '\0', '\u05a3', '\u05b9', '\r', '\u05ba', '\u05ba', '\0', '\u05bb', '\u05bd', '\r', '\u05be', '\u05be', '\u0003', '\u05bf', '\u05bf', '\r', '\u05c0', '\u05c0', '\u0003', '\u05c1', '\u05c2', '\r', '\u05c3', '\u05c3', '\u0003', '\u05c4', '\u05c4', '\r', '\u05c5', '\u05cf', '\0', '\u05d0', '\u05ea', '\u0003', '\u05eb', '\u05ef', '\0', '\u05f0', '\u05f4', '\u0003', '\u05f5', '\u05ff', '\0', '\u0600', '\u0603', '\u0004', '\u0604', '\u060b', '\0', '\u060c', '\u060c', '\f', '\u060d', '\u060d', '\u0004', '\u060e', '\u060f', '\u0012', '\u0610', '\u0615', '\r', '\u0616', '\u061a', '\0', '\u061b', '\u061b', '\u0004', '\u061c', '\u061e', '\0', '\u061f', '\u061f', '\u0004', '\u0620', '\u0620', '\0', '\u0621', '\u063a', '\u0004', '\u063b', '\u063f', '\0', '\u0640', '\u064a', '\u0004', '\u064b', '\u0658', '\r', '\u0659', '\u065f', '\0', '\u0660', '\u0669', '\u000b', '\u066a', '\u066a', '\n', '\u066b', '\u066c', '\u000b', '\u066d', '\u066f', '\u0004', '\u0670', '\u0670', '\r', '\u0671', '\u06d5', '\u0004', '\u06d6', '\u06dc', '\r', '\u06dd', '\u06dd', '\u0004', '\u06de', '\u06e4', '\r', '\u06e5', '\u06e6', '\u0004', '\u06e7', '\u06e8', '\r', '\u06e9', '\u06e9', '\u0012', '\u06ea', '\u06ed', '\r', '\u06ee', '\u06ef', '\u0004', '\u06f0', '\u06f9', '\b', '\u06fa', '\u070d', '\u0004', '\u070e', '\u070e', '\0', '\u070f', '\u070f', '\u000e', '\u0710', '\u0710', '\u0004', '\u0711', '\u0711', '\r', '\u0712', '\u072f', '\u0004', '\u0730', '\u074a', '\r', '\u074b', '\u074c', '\0', '\u074d', '\u074f', '\u0004', '\u0750', '\u077f', '\0', '\u0780', '\u07a5', '\u0004', '\u07a6', '\u07b0', '\r', '\u07b1', '\u07b1', '\u0004', '\u07b2', '\u0900', '\0', '\u0901', '\u0902', '\r', '\u0903', '\u093b', '\0', '\u093c', '\u093c', '\r', '\u093d', '\u0940', '\0', '\u0941', '\u0948', '\r', '\u0949', '\u094c', '\0', '\u094d', '\u094d', '\r', '\u094e', '\u0950', '\0', '\u0951', '\u0954', '\r', '\u0955', '\u0961', '\0', '\u0962', '\u0963', '\r', '\u0964', '\u0980', '\0', '\u0981', '\u0981', '\r', '\u0982', '\u09bb', '\0', '\u09bc', '\u09bc', '\r', '\u09bd', '\u09c0', '\0', '\u09c1', '\u09c4', '\r', '\u09c5', '\u09cc', '\0', '\u09cd', '\u09cd', '\r', '\u09ce', '\u09e1', '\0', '\u09e2', '\u09e3', '\r', '\u09e4', '\u09f1', '\0', '\u09f2', '\u09f3', '\n', '\u09f4', '\u0a00', '\0', '\u0a01', '\u0a02', '\r', '\u0a03', '\u0a3b', '\0', '\u0a3c', '\u0a3c', '\r', '\u0a3d', '\u0a40', '\0', '\u0a41', '\u0a42', '\r', '\u0a43', '\u0a46', '\0', '\u0a47', '\u0a48', '\r', '\u0a49', '\u0a4a', '\0', '\u0a4b', '\u0a4d', '\r', '\u0a4e', '\u0a6f', '\0', '\u0a70', '\u0a71', '\r', '\u0a72', '\u0a80', '\0', '\u0a81', '\u0a82', '\r', '\u0a83', '\u0abb', '\0', '\u0abc', '\u0abc', '\r', '\u0abd', '\u0ac0', '\0', '\u0ac1', '\u0ac5', '\r', '\u0ac6', '\u0ac6', '\0', '\u0ac7', '\u0ac8', '\r', '\u0ac9', '\u0acc', '\0', '\u0acd', '\u0acd', '\r', '\u0ace', '\u0ae1', '\0', '\u0ae2', '\u0ae3', '\r', '\u0ae4', '\u0af0', '\0', '\u0af1', '\u0af1', '\n', '\u0af2', '\u0b00', '\0', '\u0b01', '\u0b01', '\r', '\u0b02', '\u0b3b', '\0', '\u0b3c', '\u0b3c', '\r', '\u0b3d', '\u0b3e', '\0', '\u0b3f', '\u0b3f', '\r', '\u0b40', '\u0b40', '\0', '\u0b41', '\u0b43', '\r', '\u0b44', '\u0b4c', '\0', '\u0b4d', '\u0b4d', '\r', '\u0b4e', '\u0b55', '\0', '\u0b56', '\u0b56', '\r', '\u0b57', '\u0b81', '\0', '\u0b82', '\u0b82', '\r', '\u0b83', '\u0bbf', '\0', '\u0bc0', '\u0bc0', '\r', '\u0bc1', '\u0bcc', '\0', '\u0bcd', '\u0bcd', '\r', '\u0bce', '\u0bf2', '\0', '\u0bf3', '\u0bf8', '\u0012', '\u0bf9', '\u0bf9', '\n', '\u0bfa', '\u0bfa', '\u0012', '\u0bfb', '\u0c3d', '\0', '\u0c3e', '\u0c40', '\r', '\u0c41', '\u0c45', '\0', '\u0c46', '\u0c48', '\r', '\u0c49', '\u0c49', '\0', '\u0c4a', '\u0c4d', '\r', '\u0c4e', '\u0c54', '\0', '\u0c55', '\u0c56', '\r', '\u0c57', '\u0cbb', '\0', '\u0cbc', '\u0cbc', '\r', '\u0cbd', '\u0ccb', '\0', '\u0ccc', '\u0ccd', '\r', '\u0cce', '\u0d40', '\0', '\u0d41', '\u0d43', '\r', '\u0d44', '\u0d4c', '\0', '\u0d4d', '\u0d4d', '\r', '\u0d4e', '\u0dc9', '\0', '\u0dca', '\u0dca', '\r', '\u0dcb', '\u0dd1', '\0', '\u0dd2', '\u0dd4', '\r', '\u0dd5', '\u0dd5', '\0', '\u0dd6', '\u0dd6', '\r', '\u0dd7', '\u0e30', '\0', '\u0e31', '\u0e31', '\r', '\u0e32', '\u0e33', '\0', '\u0e34', '\u0e3a', '\r', '\u0e3b', '\u0e3e', '\0', '\u0e3f', '\u0e3f', '\n', '\u0e40', '\u0e46', '\0', '\u0e47', '\u0e4e', '\r', '\u0e4f', '\u0eb0', '\0', '\u0eb1', '\u0eb1', '\r', '\u0eb2', '\u0eb3', '\0', '\u0eb4', '\u0eb9', '\r', '\u0eba', '\u0eba', '\0', '\u0ebb', '\u0ebc', '\r', '\u0ebd', '\u0ec7', '\0', '\u0ec8', '\u0ecd', '\r', '\u0ece', '\u0f17', '\0', '\u0f18', '\u0f19', '\r', '\u0f1a', '\u0f34', '\0', '\u0f35', '\u0f35', '\r', '\u0f36', '\u0f36', '\0', '\u0f37', '\u0f37', '\r', '\u0f38', '\u0f38', '\0', '\u0f39', '\u0f39', '\r', '\u0f3a', '\u0f3d', '\u0012', '\u0f3e', '\u0f70', '\0', '\u0f71', '\u0f7e', '\r', '\u0f7f', '\u0f7f', '\0', '\u0f80', '\u0f84', '\r', '\u0f85', '\u0f85', '\0', '\u0f86', '\u0f87', '\r', '\u0f88', '\u0f8f', '\0', '\u0f90', '\u0f97', '\r', '\u0f98', '\u0f98', '\0', '\u0f99', '\u0fbc', '\r', '\u0fbd', '\u0fc5', '\0', '\u0fc6', '\u0fc6', '\r', '\u0fc7', '\u102c', '\0', '\u102d', '\u1030', '\r', '\u1031', '\u1031', '\0', '\u1032', '\u1032', '\r', '\u1033', '\u1035', '\0', '\u1036', '\u1037', '\r', '\u1038', '\u1038', '\0', '\u1039', '\u1039', '\r', '\u103a', '\u1057', '\0', '\u1058', '\u1059', '\r', '\u105a', '\u167f', '\0', '\u1680', '\u1680', '\u0011', '\u1681', '\u169a', '\0', '\u169b', '\u169c', '\u0012', '\u169d', '\u1711', '\0', '\u1712', '\u1714', '\r', '\u1715', '\u1731', '\0', '\u1732', '\u1734', '\r', '\u1735', '\u1751', '\0', '\u1752', '\u1753', '\r', '\u1754', '\u1771', '\0', '\u1772', '\u1773', '\r', '\u1774', '\u17b6', '\0', '\u17b7', '\u17bd', '\r', '\u17be', '\u17c5', '\0', '\u17c6', '\u17c6', '\r', '\u17c7', '\u17c8', '\0', '\u17c9', '\u17d3', '\r', '\u17d4', '\u17da', '\0', '\u17db', '\u17db', '\n', '\u17dc', '\u17dc', '\0', '\u17dd', '\u17dd', '\r', '\u17de', '\u17ef', '\0', '\u17f0', '\u17f9', '\u0012', '\u17fa', '\u17ff', '\0', '\u1800', '\u180a', '\u0012', '\u180b', '\u180d', '\r', '\u180e', '\u180e', '\u0011', '\u180f', '\u18a8', '\0', '\u18a9', '\u18a9', '\r', '\u18aa', '\u191f', '\0', '\u1920', '\u1922', '\r', '\u1923', '\u1926', '\0', '\u1927', '\u192b', '\r', '\u192c', '\u1931', '\0', '\u1932', '\u1932', '\r', '\u1933', '\u1938', '\0', '\u1939', '\u193b', '\r', '\u193c', '\u193f', '\0', '\u1940', '\u1940', '\u0012', '\u1941', '\u1943', '\0', '\u1944', '\u1945', '\u0012', '\u1946', '\u19df', '\0', '\u19e0', '\u19ff', '\u0012', '\u1a00', '\u1fbc', '\0', '\u1fbd', '\u1fbd', '\u0012', '\u1fbe', '\u1fbe', '\0', '\u1fbf', '\u1fc1', '\u0012', '\u1fc2', '\u1fcc', '\0', '\u1fcd', '\u1fcf', '\u0012', '\u1fd0', '\u1fdc', '\0', '\u1fdd', '\u1fdf', '\u0012', '\u1fe0', '\u1fec', '\0', '\u1fed', '\u1fef', '\u0012', '\u1ff0', '\u1ffc', '\0', '\u1ffd', '\u1ffe', '\u0012', '\u1fff', '\u1fff', '\0', '\u2000', '\u200a', '\u0011', '\u200b', '\u200d', '\u000e', '\u200e', '\u200e', '\0', '\u200f', '\u200f', '\u0003', '\u2010', '\u2027', '\u0012', '\u2028', '\u2028', '\u0011', '\u2029', '\u2029', '\u000f', '\u202a', '\u202a', '\u0001', '\u202b', '\u202b', '\u0005', '\u202c', '\u202c', '\u0007', '\u202d', '\u202d', '\u0002', '\u202e', '\u202e', '\u0006', '\u202f', '\u202f', '\u0011', '\u2030', '\u2034', '\n', '\u2035', '\u2054', '\u0012', '\u2055', '\u2056', '\0', '\u2057', '\u2057', '\u0012', '\u2058', '\u205e', '\0', '\u205f', '\u205f', '\u0011', '\u2060', '\u2063', '\u000e', '\u2064', '\u2069', '\0', '\u206a', '\u206f', '\u000e', '\u2070', '\u2070', '\b', '\u2071', '\u2073', '\0', '\u2074', '\u2079', '\b', '\u207a', '\u207b', '\n', '\u207c', '\u207e', '\u0012', '\u207f', '\u207f', '\0', '\u2080', '\u2089', '\b', '\u208a', '\u208b', '\n', '\u208c', '\u208e', '\u0012', '\u208f', '\u209f', '\0', '\u20a0', '\u20b1', '\n', '\u20b2', '\u20cf', '\0', '\u20d0', '\u20ea', '\r', '\u20eb', '\u20ff', '\0', '\u2100', '\u2101', '\u0012', '\u2102', '\u2102', '\0', '\u2103', '\u2106', '\u0012', '\u2107', '\u2107', '\0', '\u2108', '\u2109', '\u0012', '\u210a', '\u2113', '\0', '\u2114', '\u2114', '\u0012', '\u2115', '\u2115', '\0', '\u2116', '\u2118', '\u0012', '\u2119', '\u211d', '\0', '\u211e', '\u2123', '\u0012', '\u2124', '\u2124', '\0', '\u2125', '\u2125', '\u0012', '\u2126', '\u2126', '\0', '\u2127', '\u2127', '\u0012', '\u2128', '\u2128', '\0', '\u2129', '\u2129', '\u0012', '\u212a', '\u212d', '\0', '\u212e', '\u212e', '\n', '\u212f', '\u2131', '\0', '\u2132', '\u2132', '\u0012', '\u2133', '\u2139', '\0', '\u213a', '\u213b', '\u0012', '\u213c', '\u213f', '\0', '\u2140', '\u2144', '\u0012', '\u2145', '\u2149', '\0', '\u214a', '\u214b', '\u0012', '\u214c', '\u2152', '\0', '\u2153', '\u215f', '\u0012', '\u2160', '\u218f', '\0', '\u2190', '\u2211', '\u0012', '\u2212', '\u2213', '\n', '\u2214', '\u2335', '\u0012', '\u2336', '\u237a', '\0', '\u237b', '\u2394', '\u0012', '\u2395', '\u2395', '\0', '\u2396', '\u23d0', '\u0012', '\u23d1', '\u23ff', '\0', '\u2400', '\u2426', '\u0012', '\u2427', '\u243f', '\0', '\u2440', '\u244a', '\u0012', '\u244b', '\u245f', '\0', '\u2460', '\u249b', '\b', '\u249c', '\u24e9', '\0', '\u24ea', '\u24ea', '\b', '\u24eb', '\u2617', '\u0012', '\u2618', '\u2618', '\0', '\u2619', '\u267d', '\u0012', '\u267e', '\u267f', '\0', '\u2680', '\u2691', '\u0012', '\u2692', '\u269f', '\0', '\u26a0', '\u26a1', '\u0012', '\u26a2', '\u2700', '\0', '\u2701', '\u2704', '\u0012', '\u2705', '\u2705', '\0', '\u2706', '\u2709', '\u0012', '\u270a', '\u270b', '\0', '\u270c', '\u2727', '\u0012', '\u2728', '\u2728', '\0', '\u2729', '\u274b', '\u0012', '\u274c', '\u274c', '\0', '\u274d', '\u274d', '\u0012', '\u274e', '\u274e', '\0', '\u274f', '\u2752', '\u0012', '\u2753', '\u2755', '\0', '\u2756', '\u2756', '\u0012', '\u2757', '\u2757', '\0', '\u2758', '\u275e', '\u0012', '\u275f', '\u2760', '\0', '\u2761', '\u2794', '\u0012', '\u2795', '\u2797', '\0', '\u2798', '\u27af', '\u0012', '\u27b0', '\u27b0', '\0', '\u27b1', '\u27be', '\u0012', '\u27bf', '\u27cf', '\0', '\u27d0', '\u27eb', '\u0012', '\u27ec', '\u27ef', '\0', '\u27f0', '\u2b0d', '\u0012', '\u2b0e', '\u2e7f', '\0', '\u2e80', '\u2e99', '\u0012', '\u2e9a', '\u2e9a', '\0', '\u2e9b', '\u2ef3', '\u0012', '\u2ef4', '\u2eff', '\0', '\u2f00', '\u2fd5', '\u0012', '\u2fd6', '\u2fef', '\0', '\u2ff0', '\u2ffb', '\u0012', '\u2ffc', '\u2fff', '\0', '\u3000', '\u3000', '\u0011', '\u3001', '\u3004', '\u0012', '\u3005', '\u3007', '\0', '\u3008', '\u3020', '\u0012', '\u3021', '\u3029', '\0', '\u302a', '\u302f', '\r', '\u3030', '\u3030', '\u0012', '\u3031', '\u3035', '\0', '\u3036', '\u3037', '\u0012', '\u3038', '\u303c', '\0', '\u303d', '\u303f', '\u0012', '\u3040', '\u3098', '\0', '\u3099', '\u309a', '\r', '\u309b', '\u309c', '\u0012', '\u309d', '\u309f', '\0', '\u30a0', '\u30a0', '\u0012', '\u30a1', '\u30fa', '\0', '\u30fb', '\u30fb', '\u0012', '\u30fc', '\u321c', '\0', '\u321d', '\u321e', '\u0012', '\u321f', '\u324f', '\0', '\u3250', '\u325f', '\u0012', '\u3260', '\u327b', '\0', '\u327c', '\u327d', '\u0012', '\u327e', '\u32b0', '\0', '\u32b1', '\u32bf', '\u0012', '\u32c0', '\u32cb', '\0', '\u32cc', '\u32cf', '\u0012', '\u32d0', '\u3376', '\0', '\u3377', '\u337a', '\u0012', '\u337b', '\u33dd', '\0', '\u33de', '\u33df', '\u0012', '\u33e0', '\u33fe', '\0', '\u33ff', '\u33ff', '\u0012', '\u3400', '\u4dbf', '\0', '\u4dc0', '\u4dff', '\u0012', '\u4e00', '\ua48f', '\0', '\ua490', '\ua4c6', '\u0012', '\ua4c7', '\ufb1c', '\0', '\ufb1d', '\ufb1d', '\u0003', '\ufb1e', '\ufb1e', '\r', '\ufb1f', '\ufb28', '\u0003', '\ufb29', '\ufb29', '\n', '\ufb2a', '\ufb36', '\u0003', '\ufb37', '\ufb37', '\0', '\ufb38', '\ufb3c', '\u0003', '\ufb3d', '\ufb3d', '\0', '\ufb3e', '\ufb3e', '\u0003', '\ufb3f', '\ufb3f', '\0', '\ufb40', '\ufb41', '\u0003', '\ufb42', '\ufb42', '\0', '\ufb43', '\ufb44', '\u0003', '\ufb45', '\ufb45', '\0', '\ufb46', '\ufb4f', '\u0003', '\ufb50', '\ufbb1', '\u0004', '\ufbb2', '\ufbd2', '\0', '\ufbd3', '\ufd3d', '\u0004', '\ufd3e', '\ufd3f', '\u0012', '\ufd40', '\ufd4f', '\0', '\ufd50', '\ufd8f', '\u0004', '\ufd90', '\ufd91', '\0', '\ufd92', '\ufdc7', '\u0004', '\ufdc8', '\ufdef', '\0', '\ufdf0', '\ufdfc', '\u0004', '\ufdfd', '\ufdfd', '\u0012', '\ufdfe', '\ufdff', '\0', '\ufe00', '\ufe0f', '\r', '\ufe10', '\ufe1f', '\0', '\ufe20', '\ufe23', '\r', '\ufe24', '\ufe2f', '\0', '\ufe30', '\ufe4f', '\u0012', '\ufe50', '\ufe50', '\f', '\ufe51', '\ufe51', '\u0012', '\ufe52', '\ufe52', '\f', '\ufe53', '\ufe53', '\0', '\ufe54', '\ufe54', '\u0012', '\ufe55', '\ufe55', '\f', '\ufe56', '\ufe5e', '\u0012', '\ufe5f', '\ufe5f', '\n', '\ufe60', '\ufe61', '\u0012', '\ufe62', '\ufe63', '\n', '\ufe64', '\ufe66', '\u0012', '\ufe67', '\ufe67', '\0', '\ufe68', '\ufe68', '\u0012', '\ufe69', '\ufe6a', '\n', '\ufe6b', '\ufe6b', '\u0012', '\ufe6c', '\ufe6f', '\0', '\ufe70', '\ufe74', '\u0004', '\ufe75', '\ufe75', '\0', '\ufe76', '\ufefc', '\u0004', '\ufefd', '\ufefe', '\0', '\ufeff', '\ufeff', '\u000e', '\uff00', '\uff00', '\0', '\uff01', '\uff02', '\u0012', '\uff03', '\uff05', '\n', '\uff06', '\uff0a', '\u0012', '\uff0b', '\uff0b', '\n', '\uff0c', '\uff0c', '\f', '\uff0d', '\uff0d', '\n', '\uff0e', '\uff0e', '\f', '\uff0f', '\uff0f', '\t', '\uff10', '\uff19', '\b', '\uff1a', '\uff1a', '\f', '\uff1b', '\uff20', '\u0012', '\uff21', '\uff3a', '\0', '\uff3b', '\uff40', '\u0012', '\uff41', '\uff5a', '\0', '\uff5b', '\uff65', '\u0012', '\uff66', '\uffdf', '\0', '\uffe0', '\uffe1', '\n', '\uffe2', '\uffe4', '\u0012', '\uffe5', '\uffe6', '\n', '\uffe7', '\uffe7', '\0', '\uffe8', '\uffee', '\u0012', '\uffef', '\ufff8', '\0', '\ufff9', '\ufffb', '\u000e', '\ufffc', '\ufffd', '\u0012', '\ufffe', '\uffff', '\0' };
        for (int k = 0; k < BidiOrder.baseTypes.length; ++k) {
            int start = BidiOrder.baseTypes[k];
            final int end = BidiOrder.baseTypes[++k];
            for (byte b = (byte)BidiOrder.baseTypes[++k]; start <= end; BidiOrder.rtypes[start++] = b) {}
        }
    }
}
