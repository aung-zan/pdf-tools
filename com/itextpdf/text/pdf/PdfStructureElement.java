// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import com.itextpdf.text.Rectangle;
import java.util.HashMap;
import com.itextpdf.text.BaseColor;
import java.util.Iterator;
import com.itextpdf.text.Document;
import com.itextpdf.text.ListBody;
import com.itextpdf.text.ListLabel;
import com.itextpdf.text.List;
import com.itextpdf.text.Image;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.AccessibleElementId;
import com.itextpdf.text.pdf.interfaces.IPdfStructureElement;

public class PdfStructureElement extends PdfDictionary implements IPdfStructureElement
{
    private transient PdfStructureElement parent;
    private transient PdfStructureTreeRoot top;
    private AccessibleElementId elementId;
    private PdfIndirectReference reference;
    private PdfName structureType;
    
    public PdfStructureElement(final PdfStructureElement parent, final PdfName structureType) {
        this.top = parent.top;
        this.init(parent, structureType);
        this.parent = parent;
        this.put(PdfName.P, parent.reference);
        this.put(PdfName.TYPE, PdfName.STRUCTELEM);
    }
    
    public PdfStructureElement(final PdfStructureTreeRoot root, final PdfName structureType) {
        this.init(this.top = root, structureType);
        this.put(PdfName.P, root.getReference());
        this.put(PdfName.TYPE, PdfName.STRUCTELEM);
    }
    
    protected PdfStructureElement(final PdfDictionary parent, final PdfName structureType, final AccessibleElementId elementId) {
        this.elementId = elementId;
        if (parent instanceof PdfStructureElement) {
            this.top = ((PdfStructureElement)parent).top;
            this.init(parent, structureType);
            this.parent = (PdfStructureElement)parent;
            this.put(PdfName.P, ((PdfStructureElement)parent).reference);
            this.put(PdfName.TYPE, PdfName.STRUCTELEM);
        }
        else if (parent instanceof PdfStructureTreeRoot) {
            this.top = (PdfStructureTreeRoot)parent;
            this.init(parent, structureType);
            this.put(PdfName.P, ((PdfStructureTreeRoot)parent).getReference());
            this.put(PdfName.TYPE, PdfName.STRUCTELEM);
        }
    }
    
    public PdfName getStructureType() {
        return this.structureType;
    }
    
    private void init(final PdfDictionary parent, final PdfName structureType) {
        if (!this.top.getWriter().getStandardStructElems().contains(structureType)) {
            final PdfDictionary roleMap = this.top.getAsDict(PdfName.ROLEMAP);
            if (roleMap == null || !roleMap.contains(structureType)) {
                throw new ExceptionConverter(new DocumentException(MessageLocalization.getComposedMessage("unknown.structure.element.role.1", structureType.toString())));
            }
            this.structureType = roleMap.getAsName(structureType);
        }
        else {
            this.structureType = structureType;
        }
        final PdfObject kido = parent.get(PdfName.K);
        PdfArray kids = null;
        if (kido == null) {
            kids = new PdfArray();
            parent.put(PdfName.K, kids);
        }
        else if (kido instanceof PdfArray) {
            kids = (PdfArray)kido;
        }
        else {
            kids = new PdfArray();
            kids.add(kido);
            parent.put(PdfName.K, kids);
        }
        if (kids.size() > 0) {
            if (kids.getAsNumber(0) != null) {
                kids.remove(0);
            }
            if (kids.size() > 0) {
                final PdfDictionary mcr = kids.getAsDict(0);
                if (mcr != null && PdfName.MCR.equals(mcr.getAsName(PdfName.TYPE))) {
                    kids.remove(0);
                }
            }
        }
        this.put(PdfName.S, structureType);
        kids.add(this.reference = this.top.getWriter().getPdfIndirectReference());
    }
    
    public PdfDictionary getParent() {
        return this.getParent(false);
    }
    
    public PdfDictionary getParent(final boolean includeStructTreeRoot) {
        if (this.parent == null && includeStructTreeRoot) {
            return this.top;
        }
        return this.parent;
    }
    
    void setPageMark(final int page, final int mark) {
        if (mark >= 0) {
            this.put(PdfName.K, new PdfNumber(mark));
        }
        this.top.setPageMark(page, this.reference);
    }
    
    void setAnnotation(final PdfAnnotation annot, final PdfIndirectReference currentPage) {
        PdfArray kArray = this.getAsArray(PdfName.K);
        if (kArray == null) {
            kArray = new PdfArray();
            final PdfObject k = this.get(PdfName.K);
            if (k != null) {
                kArray.add(k);
            }
            this.put(PdfName.K, kArray);
        }
        final PdfDictionary dict = new PdfDictionary();
        dict.put(PdfName.TYPE, PdfName.OBJR);
        dict.put(PdfName.OBJ, annot.getIndirectReference());
        if (annot.getRole() == PdfName.FORM) {
            dict.put(PdfName.PG, currentPage);
        }
        kArray.add(dict);
    }
    
    public PdfIndirectReference getReference() {
        return this.reference;
    }
    
    @Override
    public PdfObject getAttribute(final PdfName name) {
        final PdfDictionary attr = this.getAsDict(PdfName.A);
        if (attr != null && attr.contains(name)) {
            return attr.get(name);
        }
        final PdfDictionary parent = this.getParent();
        if (parent instanceof PdfStructureElement) {
            return ((PdfStructureElement)parent).getAttribute(name);
        }
        if (parent instanceof PdfStructureTreeRoot) {
            return ((PdfStructureTreeRoot)parent).getAttribute(name);
        }
        return new PdfNull();
    }
    
    @Override
    public void setAttribute(final PdfName name, final PdfObject obj) {
        PdfDictionary attr = this.getAsDict(PdfName.A);
        if (attr == null) {
            attr = new PdfDictionary();
            this.put(PdfName.A, attr);
        }
        attr.put(name, obj);
    }
    
    public void writeAttributes(final IAccessibleElement element) {
        if (element instanceof ListItem) {
            this.writeAttributes((ListItem)element);
        }
        else if (element instanceof Paragraph) {
            this.writeAttributes((Paragraph)element);
        }
        else if (element instanceof Chunk) {
            this.writeAttributes((Chunk)element);
        }
        else if (element instanceof Image) {
            this.writeAttributes((Image)element);
        }
        else if (element instanceof List) {
            this.writeAttributes((List)element);
        }
        else if (element instanceof ListLabel) {
            this.writeAttributes((ListLabel)element);
        }
        else if (element instanceof ListBody) {
            this.writeAttributes((ListBody)element);
        }
        else if (element instanceof PdfPTable) {
            this.writeAttributes((PdfPTable)element);
        }
        else if (element instanceof PdfPRow) {
            this.writeAttributes((PdfPRow)element);
        }
        else if (element instanceof PdfPHeaderCell) {
            this.writeAttributes((PdfPHeaderCell)element);
        }
        else if (element instanceof PdfPCell) {
            this.writeAttributes((PdfPCell)element);
        }
        else if (element instanceof PdfPTableHeader) {
            this.writeAttributes((PdfPTableHeader)element);
        }
        else if (element instanceof PdfPTableFooter) {
            this.writeAttributes((PdfPTableFooter)element);
        }
        else if (element instanceof PdfPTableBody) {
            this.writeAttributes((PdfPTableBody)element);
        }
        else if (element instanceof PdfDiv) {
            this.writeAttributes((PdfDiv)element);
        }
        else if (element instanceof PdfTemplate) {
            this.writeAttributes((PdfTemplate)element);
        }
        else if (element instanceof Document) {
            this.writeAttributes((Document)element);
        }
        if (element.getAccessibleAttributes() != null) {
            for (final PdfName key : element.getAccessibleAttributes().keySet()) {
                if (key.equals(PdfName.ID)) {
                    final PdfObject attr = element.getAccessibleAttribute(key);
                    this.put(key, attr);
                    this.top.putIDTree(attr.toString(), this.getReference());
                }
                else if (key.equals(PdfName.LANG) || key.equals(PdfName.ALT) || key.equals(PdfName.ACTUALTEXT) || key.equals(PdfName.E) || key.equals(PdfName.T)) {
                    this.put(key, element.getAccessibleAttribute(key));
                }
                else {
                    this.setAttribute(key, element.getAccessibleAttribute(key));
                }
            }
        }
    }
    
    private void writeAttributes(final Chunk chunk) {
        if (chunk != null) {
            if (chunk.getImage() != null) {
                this.writeAttributes(chunk.getImage());
            }
            else {
                final HashMap<String, Object> attr = chunk.getAttributes();
                if (attr != null) {
                    this.setAttribute(PdfName.O, PdfName.LAYOUT);
                    if (attr.containsKey("UNDERLINE")) {
                        this.setAttribute(PdfName.TEXTDECORATIONTYPE, PdfName.UNDERLINE);
                    }
                    if (attr.containsKey("BACKGROUND")) {
                        final Object[] back = attr.get("BACKGROUND");
                        final BaseColor color = (BaseColor)back[0];
                        this.setAttribute(PdfName.BACKGROUNDCOLOR, new PdfArray(new float[] { color.getRed() / 255.0f, color.getGreen() / 255.0f, color.getBlue() / 255.0f }));
                    }
                    final IPdfStructureElement parent = (IPdfStructureElement)this.getParent(true);
                    final PdfObject obj = this.getParentAttribute(parent, PdfName.COLOR);
                    if (chunk.getFont() != null && chunk.getFont().getColor() != null) {
                        final BaseColor c = chunk.getFont().getColor();
                        this.setColorAttribute(c, obj, PdfName.COLOR);
                    }
                    final PdfObject decorThickness = this.getParentAttribute(parent, PdfName.TEXTDECORATIONTHICKNESS);
                    final PdfObject decorColor = this.getParentAttribute(parent, PdfName.TEXTDECORATIONCOLOR);
                    if (attr.containsKey("UNDERLINE")) {
                        final Object[][] unders = attr.get("UNDERLINE");
                        final Object[] arr = unders[unders.length - 1];
                        final BaseColor color2 = (BaseColor)arr[0];
                        final float[] floats = (float[])arr[1];
                        final float thickness = floats[0];
                        if (decorThickness instanceof PdfNumber) {
                            final float t = ((PdfNumber)decorThickness).floatValue();
                            if (Float.compare(thickness, t) != 0) {
                                this.setAttribute(PdfName.TEXTDECORATIONTHICKNESS, new PdfNumber(thickness));
                            }
                        }
                        else {
                            this.setAttribute(PdfName.TEXTDECORATIONTHICKNESS, new PdfNumber(thickness));
                        }
                        if (color2 != null) {
                            this.setColorAttribute(color2, decorColor, PdfName.TEXTDECORATIONCOLOR);
                        }
                    }
                    if (attr.containsKey("LINEHEIGHT")) {
                        final float height = attr.get("LINEHEIGHT");
                        final PdfObject parentLH = this.getParentAttribute(parent, PdfName.LINEHEIGHT);
                        if (parentLH instanceof PdfNumber) {
                            final float pLH = ((PdfNumber)parentLH).floatValue();
                            if (Float.compare(pLH, height) != 0) {
                                this.setAttribute(PdfName.LINEHEIGHT, new PdfNumber(height));
                            }
                        }
                        else {
                            this.setAttribute(PdfName.LINEHEIGHT, new PdfNumber(height));
                        }
                    }
                }
            }
        }
    }
    
    private void writeAttributes(final Image image) {
        if (image != null) {
            this.setAttribute(PdfName.O, PdfName.LAYOUT);
            if (image.getWidth() > 0.0f) {
                this.setAttribute(PdfName.WIDTH, new PdfNumber(image.getWidth()));
            }
            if (image.getHeight() > 0.0f) {
                this.setAttribute(PdfName.HEIGHT, new PdfNumber(image.getHeight()));
            }
            final PdfRectangle rect = new PdfRectangle(image, image.getRotation());
            this.setAttribute(PdfName.BBOX, rect);
            if (image.getBackgroundColor() != null) {
                final BaseColor color = image.getBackgroundColor();
                this.setAttribute(PdfName.BACKGROUNDCOLOR, new PdfArray(new float[] { color.getRed() / 255.0f, color.getGreen() / 255.0f, color.getBlue() / 255.0f }));
            }
        }
    }
    
    private void writeAttributes(final PdfTemplate template) {
        if (template != null) {
            this.setAttribute(PdfName.O, PdfName.LAYOUT);
            if (template.getWidth() > 0.0f) {
                this.setAttribute(PdfName.WIDTH, new PdfNumber(template.getWidth()));
            }
            if (template.getHeight() > 0.0f) {
                this.setAttribute(PdfName.HEIGHT, new PdfNumber(template.getHeight()));
            }
            final PdfRectangle rect = new PdfRectangle(template.getBoundingBox());
            this.setAttribute(PdfName.BBOX, rect);
        }
    }
    
    private void writeAttributes(final Paragraph paragraph) {
        if (paragraph != null) {
            this.setAttribute(PdfName.O, PdfName.LAYOUT);
            if (Float.compare(paragraph.getSpacingBefore(), 0.0f) != 0) {
                this.setAttribute(PdfName.SPACEBEFORE, new PdfNumber(paragraph.getSpacingBefore()));
            }
            if (Float.compare(paragraph.getSpacingAfter(), 0.0f) != 0) {
                this.setAttribute(PdfName.SPACEAFTER, new PdfNumber(paragraph.getSpacingAfter()));
            }
            final IPdfStructureElement parent = (IPdfStructureElement)this.getParent(true);
            PdfObject obj = this.getParentAttribute(parent, PdfName.COLOR);
            if (paragraph.getFont() != null && paragraph.getFont().getColor() != null) {
                final BaseColor c = paragraph.getFont().getColor();
                this.setColorAttribute(c, obj, PdfName.COLOR);
            }
            obj = this.getParentAttribute(parent, PdfName.TEXTINDENT);
            if (Float.compare(paragraph.getFirstLineIndent(), 0.0f) != 0) {
                boolean writeIndent = true;
                if (obj instanceof PdfNumber && Float.compare(((PdfNumber)obj).floatValue(), new Float(paragraph.getFirstLineIndent())) == 0) {
                    writeIndent = false;
                }
                if (writeIndent) {
                    this.setAttribute(PdfName.TEXTINDENT, new PdfNumber(paragraph.getFirstLineIndent()));
                }
            }
            obj = this.getParentAttribute(parent, PdfName.STARTINDENT);
            if (obj instanceof PdfNumber) {
                final float startIndent = ((PdfNumber)obj).floatValue();
                if (Float.compare(startIndent, paragraph.getIndentationLeft()) != 0) {
                    this.setAttribute(PdfName.STARTINDENT, new PdfNumber(paragraph.getIndentationLeft()));
                }
            }
            else if (Math.abs(paragraph.getIndentationLeft()) > Float.MIN_VALUE) {
                this.setAttribute(PdfName.STARTINDENT, new PdfNumber(paragraph.getIndentationLeft()));
            }
            obj = this.getParentAttribute(parent, PdfName.ENDINDENT);
            if (obj instanceof PdfNumber) {
                final float endIndent = ((PdfNumber)obj).floatValue();
                if (Float.compare(endIndent, paragraph.getIndentationRight()) != 0) {
                    this.setAttribute(PdfName.ENDINDENT, new PdfNumber(paragraph.getIndentationRight()));
                }
            }
            else if (Float.compare(paragraph.getIndentationRight(), 0.0f) != 0) {
                this.setAttribute(PdfName.ENDINDENT, new PdfNumber(paragraph.getIndentationRight()));
            }
            this.setTextAlignAttribute(paragraph.getAlignment());
        }
    }
    
    private void writeAttributes(final List list) {
        if (list != null) {
            this.setAttribute(PdfName.O, PdfName.LIST);
            if (list.isAutoindent()) {
                if (list.isNumbered()) {
                    if (list.isLettered()) {
                        if (list.isLowercase()) {
                            this.setAttribute(PdfName.LISTNUMBERING, PdfName.LOWERROMAN);
                        }
                        else {
                            this.setAttribute(PdfName.LISTNUMBERING, PdfName.UPPERROMAN);
                        }
                    }
                    else {
                        this.setAttribute(PdfName.LISTNUMBERING, PdfName.DECIMAL);
                    }
                }
                else if (list.isLettered()) {
                    if (list.isLowercase()) {
                        this.setAttribute(PdfName.LISTNUMBERING, PdfName.LOWERALPHA);
                    }
                    else {
                        this.setAttribute(PdfName.LISTNUMBERING, PdfName.UPPERALPHA);
                    }
                }
            }
            PdfObject obj = this.getParentAttribute(this.parent, PdfName.STARTINDENT);
            if (obj instanceof PdfNumber) {
                final float startIndent = ((PdfNumber)obj).floatValue();
                if (Float.compare(startIndent, list.getIndentationLeft()) != 0) {
                    this.setAttribute(PdfName.STARTINDENT, new PdfNumber(list.getIndentationLeft()));
                }
            }
            else if (Math.abs(list.getIndentationLeft()) > Float.MIN_VALUE) {
                this.setAttribute(PdfName.STARTINDENT, new PdfNumber(list.getIndentationLeft()));
            }
            obj = this.getParentAttribute(this.parent, PdfName.ENDINDENT);
            if (obj instanceof PdfNumber) {
                final float endIndent = ((PdfNumber)obj).floatValue();
                if (Float.compare(endIndent, list.getIndentationRight()) != 0) {
                    this.setAttribute(PdfName.ENDINDENT, new PdfNumber(list.getIndentationRight()));
                }
            }
            else if (Float.compare(list.getIndentationRight(), 0.0f) != 0) {
                this.setAttribute(PdfName.ENDINDENT, new PdfNumber(list.getIndentationRight()));
            }
        }
    }
    
    private void writeAttributes(final ListItem listItem) {
        if (listItem != null) {
            PdfObject obj = this.getParentAttribute(this.parent, PdfName.STARTINDENT);
            if (obj instanceof PdfNumber) {
                final float startIndent = ((PdfNumber)obj).floatValue();
                if (Float.compare(startIndent, listItem.getIndentationLeft()) != 0) {
                    this.setAttribute(PdfName.STARTINDENT, new PdfNumber(listItem.getIndentationLeft()));
                }
            }
            else if (Math.abs(listItem.getIndentationLeft()) > Float.MIN_VALUE) {
                this.setAttribute(PdfName.STARTINDENT, new PdfNumber(listItem.getIndentationLeft()));
            }
            obj = this.getParentAttribute(this.parent, PdfName.ENDINDENT);
            if (obj instanceof PdfNumber) {
                final float endIndent = ((PdfNumber)obj).floatValue();
                if (Float.compare(endIndent, listItem.getIndentationRight()) != 0) {
                    this.setAttribute(PdfName.ENDINDENT, new PdfNumber(listItem.getIndentationRight()));
                }
            }
            else if (Float.compare(listItem.getIndentationRight(), 0.0f) != 0) {
                this.setAttribute(PdfName.ENDINDENT, new PdfNumber(listItem.getIndentationRight()));
            }
        }
    }
    
    private void writeAttributes(final ListBody listBody) {
        if (listBody != null) {}
    }
    
    private void writeAttributes(final ListLabel listLabel) {
        if (listLabel != null) {
            final PdfObject obj = this.getParentAttribute(this.parent, PdfName.STARTINDENT);
            if (obj instanceof PdfNumber) {
                final float startIndent = ((PdfNumber)obj).floatValue();
                if (Float.compare(startIndent, listLabel.getIndentation()) != 0) {
                    this.setAttribute(PdfName.STARTINDENT, new PdfNumber(listLabel.getIndentation()));
                }
            }
            else if (Math.abs(listLabel.getIndentation()) > Float.MIN_VALUE) {
                this.setAttribute(PdfName.STARTINDENT, new PdfNumber(listLabel.getIndentation()));
            }
        }
    }
    
    private void writeAttributes(final PdfPTable table) {
        if (table != null) {
            this.setAttribute(PdfName.O, PdfName.TABLE);
            if (Float.compare(table.getSpacingBefore(), 0.0f) != 0) {
                this.setAttribute(PdfName.SPACEBEFORE, new PdfNumber(table.getSpacingBefore()));
            }
            if (Float.compare(table.getSpacingAfter(), 0.0f) != 0) {
                this.setAttribute(PdfName.SPACEAFTER, new PdfNumber(table.getSpacingAfter()));
            }
            if (table.getTotalHeight() > 0.0f) {
                this.setAttribute(PdfName.HEIGHT, new PdfNumber(table.getTotalHeight()));
            }
            if (table.getTotalWidth() > 0.0f) {
                this.setAttribute(PdfName.WIDTH, new PdfNumber(table.getTotalWidth()));
            }
        }
    }
    
    private void writeAttributes(final PdfPRow row) {
        if (row != null) {
            this.setAttribute(PdfName.O, PdfName.TABLE);
        }
    }
    
    private void writeAttributes(final PdfPCell cell) {
        if (cell != null) {
            this.setAttribute(PdfName.O, PdfName.TABLE);
            if (cell.getColspan() != 1) {
                this.setAttribute(PdfName.COLSPAN, new PdfNumber(cell.getColspan()));
            }
            if (cell.getRowspan() != 1) {
                this.setAttribute(PdfName.ROWSPAN, new PdfNumber(cell.getRowspan()));
            }
            if (cell.getHeaders() != null) {
                final PdfArray headers = new PdfArray();
                final ArrayList<PdfPHeaderCell> list = cell.getHeaders();
                for (final PdfPHeaderCell header : list) {
                    if (header.getName() != null) {
                        headers.add(new PdfString(header.getName()));
                    }
                }
                if (!headers.isEmpty()) {
                    this.setAttribute(PdfName.HEADERS, headers);
                }
            }
            if (cell.getCalculatedHeight() > 0.0f) {
                this.setAttribute(PdfName.HEIGHT, new PdfNumber(cell.getCalculatedHeight()));
            }
            if (cell.getWidth() > 0.0f) {
                this.setAttribute(PdfName.WIDTH, new PdfNumber(cell.getWidth()));
            }
            if (cell.getBackgroundColor() != null) {
                final BaseColor color = cell.getBackgroundColor();
                this.setAttribute(PdfName.BACKGROUNDCOLOR, new PdfArray(new float[] { color.getRed() / 255.0f, color.getGreen() / 255.0f, color.getBlue() / 255.0f }));
            }
        }
    }
    
    private void writeAttributes(final PdfPHeaderCell headerCell) {
        if (headerCell != null) {
            if (headerCell.getScope() != 0) {
                switch (headerCell.getScope()) {
                    case 1: {
                        this.setAttribute(PdfName.SCOPE, PdfName.ROW);
                        break;
                    }
                    case 2: {
                        this.setAttribute(PdfName.SCOPE, PdfName.COLUMN);
                        break;
                    }
                    case 3: {
                        this.setAttribute(PdfName.SCOPE, PdfName.BOTH);
                        break;
                    }
                }
            }
            if (headerCell.getName() != null) {
                this.setAttribute(PdfName.NAME, new PdfName(headerCell.getName()));
            }
            this.writeAttributes((PdfPCell)headerCell);
        }
    }
    
    private void writeAttributes(final PdfPTableHeader header) {
        if (header != null) {
            this.setAttribute(PdfName.O, PdfName.TABLE);
        }
    }
    
    private void writeAttributes(final PdfPTableBody body) {
        if (body != null) {}
    }
    
    private void writeAttributes(final PdfPTableFooter footer) {
        if (footer != null) {}
    }
    
    private void writeAttributes(final PdfDiv div) {
        if (div != null) {
            if (div.getBackgroundColor() != null) {
                this.setColorAttribute(div.getBackgroundColor(), null, PdfName.BACKGROUNDCOLOR);
            }
            this.setTextAlignAttribute(div.getTextAlignment());
        }
    }
    
    private void writeAttributes(final Document document) {
        if (document != null) {}
    }
    
    private boolean colorsEqual(final PdfArray parentColor, final float[] color) {
        return Float.compare(color[0], parentColor.getAsNumber(0).floatValue()) == 0 && Float.compare(color[1], parentColor.getAsNumber(1).floatValue()) == 0 && Float.compare(color[2], parentColor.getAsNumber(2).floatValue()) == 0;
    }
    
    private void setColorAttribute(final BaseColor newColor, final PdfObject oldColor, final PdfName attributeName) {
        final float[] colorArr = { newColor.getRed() / 255.0f, newColor.getGreen() / 255.0f, newColor.getBlue() / 255.0f };
        if (oldColor != null && oldColor instanceof PdfArray) {
            final PdfArray oldC = (PdfArray)oldColor;
            if (this.colorsEqual(oldC, colorArr)) {
                this.setAttribute(attributeName, new PdfArray(colorArr));
            }
            else {
                this.setAttribute(attributeName, new PdfArray(colorArr));
            }
        }
        else {
            this.setAttribute(attributeName, new PdfArray(colorArr));
        }
    }
    
    private void setTextAlignAttribute(final int elementAlign) {
        PdfName align = null;
        switch (elementAlign) {
            case 0: {
                align = PdfName.START;
                break;
            }
            case 1: {
                align = PdfName.CENTER;
                break;
            }
            case 2: {
                align = PdfName.END;
                break;
            }
            case 3: {
                align = PdfName.JUSTIFY;
                break;
            }
        }
        final PdfObject obj = this.getParentAttribute(this.parent, PdfName.TEXTALIGN);
        if (obj instanceof PdfName) {
            final PdfName textAlign = (PdfName)obj;
            if (align != null && !textAlign.equals(align)) {
                this.setAttribute(PdfName.TEXTALIGN, align);
            }
        }
        else if (align != null && !PdfName.START.equals(align)) {
            this.setAttribute(PdfName.TEXTALIGN, align);
        }
    }
    
    @Override
    public void toPdf(final PdfWriter writer, final OutputStream os) throws IOException {
        PdfWriter.checkPdfIsoConformance(writer, 16, this);
        super.toPdf(writer, os);
    }
    
    private PdfObject getParentAttribute(final IPdfStructureElement parent, final PdfName name) {
        if (parent == null) {
            return null;
        }
        return parent.getAttribute(name);
    }
    
    protected void setStructureTreeRoot(final PdfStructureTreeRoot root) {
        this.top = root;
    }
    
    protected void setStructureElementParent(final PdfStructureElement parent) {
        this.parent = parent;
    }
    
    protected AccessibleElementId getElementId() {
        return this.elementId;
    }
}
