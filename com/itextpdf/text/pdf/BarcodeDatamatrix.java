// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.Hashtable;
import java.awt.image.ImageProducer;
import java.awt.image.MemoryImageSource;
import java.awt.Canvas;
import java.awt.Color;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.pdf.codec.CCITTG4Encoder;
import com.itextpdf.text.Image;
import java.util.Arrays;
import java.io.UnsupportedEncodingException;

public class BarcodeDatamatrix
{
    public static final int DM_NO_ERROR = 0;
    public static final int DM_ERROR_TEXT_TOO_BIG = 1;
    public static final int DM_ERROR_INVALID_SQUARE = 3;
    public static final int DM_ERROR_EXTENSION = 5;
    public static final int DM_AUTO = 0;
    public static final int DM_ASCII = 1;
    public static final int DM_C40 = 2;
    public static final int DM_TEXT = 3;
    public static final int DM_B256 = 4;
    public static final int DM_X12 = 5;
    @Deprecated
    public static final int DM_X21 = 5;
    public static final int DM_EDIFACT = 6;
    public static final int DM_RAW = 7;
    public static final int DM_EXTENSION = 32;
    public static final int DM_TEST = 64;
    public static final String DEFAULT_DATA_MATRIX_ENCODING = "iso-8859-1";
    private static final byte LATCH_B256 = -25;
    private static final byte LATCH_EDIFACT = -16;
    private static final byte LATCH_X12 = -18;
    private static final byte LATCH_TEXT = -17;
    private static final byte LATCH_C40 = -26;
    private static final byte UNLATCH = -2;
    private static final byte EXTENDED_ASCII = -21;
    private static final byte PADDING = -127;
    private String encoding;
    private static final DmParams[] dmSizes;
    private static final String X12 = "\r*> 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private int extOut;
    private short[] place;
    private byte[] image;
    private int height;
    private int width;
    private int ws;
    private int options;
    private static int[][] f;
    private static int[][] switchMode;
    private boolean forceSquareSize;
    
    public BarcodeDatamatrix() {
        this.forceSquareSize = false;
        this.encoding = "iso-8859-1";
    }
    
    public BarcodeDatamatrix(final String code) throws UnsupportedEncodingException {
        this.forceSquareSize = false;
        this.encoding = "iso-8859-1";
        this.generate(code);
    }
    
    public BarcodeDatamatrix(final String code, final String encoding) throws UnsupportedEncodingException {
        this.forceSquareSize = false;
        this.encoding = encoding;
        this.generate(code);
    }
    
    private void setBit(final int x, final int y, final int xByte) {
        final byte[] image = this.image;
        final int n = y * xByte + x / 8;
        image[n] |= (byte)(128 >> (x & 0x7));
    }
    
    private void draw(final byte[] data, final int dataSize, final DmParams dm) {
        final int xByte = (dm.width + this.ws * 2 + 7) / 8;
        Arrays.fill(this.image, (byte)0);
        for (int i = this.ws; i < dm.height + this.ws; i += dm.heightSection) {
            for (int j = this.ws; j < dm.width + this.ws; j += 2) {
                this.setBit(j, i, xByte);
            }
        }
        for (int i = dm.heightSection - 1 + this.ws; i < dm.height + this.ws; i += dm.heightSection) {
            for (int j = this.ws; j < dm.width + this.ws; ++j) {
                this.setBit(j, i, xByte);
            }
        }
        for (int i = this.ws; i < dm.width + this.ws; i += dm.widthSection) {
            for (int j = this.ws; j < dm.height + this.ws; ++j) {
                this.setBit(i, j, xByte);
            }
        }
        for (int i = dm.widthSection - 1 + this.ws; i < dm.width + this.ws; i += dm.widthSection) {
            for (int j = 1 + this.ws; j < dm.height + this.ws; j += 2) {
                this.setBit(i, j, xByte);
            }
        }
        int p = 0;
        for (int ys = 0; ys < dm.height; ys += dm.heightSection) {
            for (int y = 1; y < dm.heightSection - 1; ++y) {
                for (int xs = 0; xs < dm.width; xs += dm.widthSection) {
                    for (int x = 1; x < dm.widthSection - 1; ++x) {
                        final int z = this.place[p++];
                        if (z == 1 || (z > 1 && (data[z / 8 - 1] & 0xFF & 128 >> z % 8) != 0x0)) {
                            this.setBit(x + xs + this.ws, y + ys + this.ws, xByte);
                        }
                    }
                }
            }
        }
    }
    
    private static void makePadding(final byte[] data, int position, int count) {
        if (count <= 0) {
            return;
        }
        data[position++] = -127;
        while (--count > 0) {
            int t = 129 + (position + 1) * 149 % 253 + 1;
            if (t > 254) {
                t -= 254;
            }
            data[position++] = (byte)t;
        }
    }
    
    private static boolean isDigit(final int c) {
        return c >= 48 && c <= 57;
    }
    
    private static int asciiEncodation(final byte[] text, final int textOffset, int textLength, final byte[] data, final int dataOffset, int dataLength, final int symbolIndex, final int prevEnc, final int origDataOffset) {
        int ptrIn = textOffset;
        int ptrOut = dataOffset;
        textLength += textOffset;
        dataLength += dataOffset;
        while (ptrIn < textLength) {
            final int c = text[ptrIn++] & 0xFF;
            if (isDigit(c) && symbolIndex > 0 && prevEnc == 1 && isDigit(text[ptrIn - 2] & 0xFF) && data[dataOffset - 1] > 48 && data[dataOffset - 1] < 59) {
                data[ptrOut - 1] = (byte)(((text[ptrIn - 2] & 0xFF) - 48) * 10 + c - 48 + 130);
                return ptrOut - origDataOffset;
            }
            if (ptrOut >= dataLength) {
                return -1;
            }
            if (isDigit(c) && symbolIndex < 0 && ptrIn < textLength && isDigit(text[ptrIn] & 0xFF)) {
                data[ptrOut++] = (byte)((c - 48) * 10 + (text[ptrIn++] & 0xFF) - 48 + 130);
            }
            else if (c > 127) {
                if (ptrOut + 1 >= dataLength) {
                    return -1;
                }
                data[ptrOut++] = -21;
                data[ptrOut++] = (byte)(c - 128 + 1);
            }
            else {
                data[ptrOut++] = (byte)(c + 1);
            }
        }
        return ptrOut - origDataOffset;
    }
    
    private static int b256Encodation(final byte[] text, final int textOffset, int textLength, final byte[] data, final int dataOffset, final int dataLength, final int symbolIndex, final int prevEnc, final int origDataOffset) {
        if (textLength == 0) {
            return 0;
        }
        int simulatedDataOffset = dataOffset;
        if (prevEnc != 4) {
            if (textLength < 250 && textLength + 2 > dataLength) {
                return -1;
            }
            if (textLength >= 250 && textLength + 3 > dataLength) {
                return -1;
            }
            data[dataOffset] = -25;
        }
        else {
            int latestModeEntry;
            for (latestModeEntry = symbolIndex - 1; latestModeEntry > 0 && BarcodeDatamatrix.switchMode[3][latestModeEntry] == 4; --latestModeEntry) {}
            textLength = symbolIndex - latestModeEntry + 1;
            if (textLength != 250 && 1 > dataLength) {
                return -1;
            }
            if (textLength == 250 && 2 > dataLength) {
                return -1;
            }
            simulatedDataOffset -= textLength - 1 + ((textLength < 250) ? 2 : 3);
        }
        int minRequiredDataIncrement;
        if (textLength < 250) {
            data[simulatedDataOffset + 1] = (byte)textLength;
            minRequiredDataIncrement = ((prevEnc != 4) ? 2 : 0);
        }
        else if (textLength == 250 && prevEnc == 4) {
            data[simulatedDataOffset + 1] = (byte)(textLength / 250 + 249);
            for (int i = dataOffset + 1; i > simulatedDataOffset + 2; --i) {
                data[i] = data[i - 1];
            }
            data[simulatedDataOffset + 2] = (byte)(textLength % 250);
            minRequiredDataIncrement = 1;
        }
        else {
            data[simulatedDataOffset + 1] = (byte)(textLength / 250 + 249);
            data[simulatedDataOffset + 2] = (byte)(textLength % 250);
            minRequiredDataIncrement = ((prevEnc != 4) ? 3 : 0);
        }
        if (prevEnc == 4) {
            textLength = 1;
        }
        System.arraycopy(text, textOffset, data, minRequiredDataIncrement + dataOffset, textLength);
        for (int j = (prevEnc != 4) ? (dataOffset + 1) : dataOffset; j < minRequiredDataIncrement + textLength + dataOffset; ++j) {
            randomizationAlgorithm255(data, j);
        }
        if (prevEnc == 4) {
            randomizationAlgorithm255(data, simulatedDataOffset + 1);
        }
        return textLength + dataOffset + minRequiredDataIncrement - origDataOffset;
    }
    
    private static void randomizationAlgorithm255(final byte[] data, final int j) {
        final int c = data[j] & 0xFF;
        final int prn = 149 * (j + 1) % 255 + 1;
        int tv = c + prn;
        if (tv > 255) {
            tv -= 256;
        }
        data[j] = (byte)tv;
    }
    
    private static int X12Encodation(final byte[] text, int textOffset, int textLength, final byte[] data, int dataOffset, int dataLength, final int symbolIndex, final int prevEnc, final int origDataOffset) {
        boolean latch = true;
        if (textLength == 0) {
            return 0;
        }
        int ptrIn = 0;
        int ptrOut = 0;
        byte[] x = new byte[textLength];
        int count = 0;
        while (ptrIn < textLength) {
            final int i = "\r*> 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf((char)text[ptrIn + textOffset]);
            if (i >= 0) {
                x[ptrIn] = (byte)i;
                ++count;
            }
            else {
                x[ptrIn] = 100;
                if (count >= 6) {
                    count -= count / 3 * 3;
                }
                for (int k = 0; k < count; ++k) {
                    x[ptrIn - k - 1] = 100;
                }
                count = 0;
            }
            ++ptrIn;
        }
        if (count >= 6) {
            count -= count / 3 * 3;
        }
        for (int k = 0; k < count; ++k) {
            x[ptrIn - k - 1] = 100;
        }
        ptrIn = 0;
        byte c = 0;
        while (ptrIn < textLength) {
            c = x[ptrIn];
            if (ptrOut > dataLength) {
                break;
            }
            if (c < 40) {
                if ((ptrIn == 0 && latch) || (ptrIn > 0 && x[ptrIn - 1] > 40)) {
                    data[dataOffset + ptrOut++] = -18;
                }
                if (ptrOut + 2 > dataLength) {
                    break;
                }
                final int n = 1600 * x[ptrIn] + 40 * x[ptrIn + 1] + x[ptrIn + 2] + 1;
                data[dataOffset + ptrOut++] = (byte)(n / 256);
                data[dataOffset + ptrOut++] = (byte)n;
                ptrIn += 2;
            }
            else {
                boolean enterASCII = true;
                if (symbolIndex <= 0) {
                    if (ptrIn > 0 && x[ptrIn - 1] < 40) {
                        data[dataOffset + ptrOut++] = -2;
                    }
                }
                else if (symbolIndex > 4 && prevEnc == 5 && "\r*> 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf((char)text[textOffset]) >= 0 && "\r*> 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf((char)text[textOffset - 1]) >= 0) {
                    int latestModeEntry;
                    for (latestModeEntry = symbolIndex - 1; latestModeEntry > 0 && BarcodeDatamatrix.switchMode[4][latestModeEntry] == 5 && "\r*> 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf((char)text[textOffset - (symbolIndex - latestModeEntry + 1)]) >= 0; --latestModeEntry) {}
                    int unlatch = -1;
                    if (symbolIndex - latestModeEntry >= 5) {
                        for (int j = 1; j <= symbolIndex - latestModeEntry; ++j) {
                            if (data[dataOffset - j] == -2) {
                                unlatch = dataOffset - j;
                                break;
                            }
                        }
                        final int amountOfEncodedWithASCII = (unlatch >= 0) ? (dataOffset - unlatch - 1) : (symbolIndex - latestModeEntry);
                        if (amountOfEncodedWithASCII % 3 == 2) {
                            enterASCII = false;
                            textLength = amountOfEncodedWithASCII + 1;
                            textOffset -= amountOfEncodedWithASCII;
                            dataLength += ((unlatch < 0) ? amountOfEncodedWithASCII : (amountOfEncodedWithASCII + 1));
                            dataOffset -= ((unlatch < 0) ? amountOfEncodedWithASCII : (amountOfEncodedWithASCII + 1));
                            ptrIn = -1;
                            latch = (unlatch != dataOffset);
                            x = new byte[amountOfEncodedWithASCII + 1];
                            for (int l = 0; l <= amountOfEncodedWithASCII; ++l) {
                                x[l] = (byte)"\r*> 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf((char)text[textOffset + l]);
                            }
                        }
                        else {
                            x = new byte[] { 100 };
                        }
                    }
                }
                if (enterASCII) {
                    final int m = asciiEncodation(text, textOffset + ptrIn, 1, data, dataOffset + ptrOut, dataLength, -1, -1, origDataOffset);
                    if (m < 0) {
                        return -1;
                    }
                    if (data[dataOffset + ptrOut] == -21) {
                        ++ptrOut;
                    }
                    ++ptrOut;
                }
            }
            ++ptrIn;
        }
        c = 100;
        if (textLength > 0) {
            c = x[textLength - 1];
        }
        if (ptrIn != textLength) {
            return -1;
        }
        if (c < 40) {
            data[dataOffset + ptrOut++] = -2;
        }
        if (ptrOut > dataLength) {
            return -1;
        }
        return ptrOut + dataOffset - origDataOffset;
    }
    
    private static int EdifactEncodation(final byte[] text, int textOffset, int textLength, final byte[] data, int dataOffset, int dataLength, final int symbolIndex, final int prevEnc, final int origDataOffset, final boolean sizeFixed) {
        if (textLength == 0) {
            return 0;
        }
        int ptrIn = 0;
        int ptrOut = 0;
        int edi = 0;
        int pedi = 18;
        boolean ascii = true;
        int latestModeEntryActual = -1;
        int latestModeEntryC40orX12 = -1;
        int prevMode = -1;
        if (prevEnc == 6 && ((text[textOffset] & 0xFF & 0xE0) == 0x40 || (text[textOffset] & 0xFF & 0xE0) == 0x20) && (text[textOffset] & 0xFF) != 0x5F && ((text[textOffset - 1] & 0xFF & 0xE0) == 0x40 || (text[textOffset - 1] & 0xFF & 0xE0) == 0x20) && (text[textOffset - 1] & 0xFF) != 0x5F) {
            for (latestModeEntryActual = symbolIndex - 1; latestModeEntryActual > 0 && BarcodeDatamatrix.switchMode[5][latestModeEntryActual] == 6; --latestModeEntryActual) {
                final int c = text[textOffset - (symbolIndex - latestModeEntryActual + 1)] & 0xFF;
                if (((c & 0xE0) != 0x40 && (c & 0xE0) != 0x20) || c == 95) {
                    break;
                }
            }
            prevMode = ((BarcodeDatamatrix.switchMode[5][latestModeEntryActual] == 2 || BarcodeDatamatrix.switchMode[5][latestModeEntryActual] == 5) ? BarcodeDatamatrix.switchMode[5][latestModeEntryActual] : -1);
            if (prevMode > 0) {
                latestModeEntryC40orX12 = latestModeEntryActual;
            }
            while (prevMode > 0 && latestModeEntryC40orX12 > 0 && BarcodeDatamatrix.switchMode[prevMode - 1][latestModeEntryC40orX12] == prevMode) {
                final int c = text[textOffset - (symbolIndex - latestModeEntryC40orX12 + 1)] & 0xFF;
                if (((c & 0xE0) != 0x40 && (c & 0xE0) != 0x20) || c == 95) {
                    latestModeEntryC40orX12 = -1;
                    break;
                }
                --latestModeEntryC40orX12;
            }
        }
        int dataSize = dataOffset + dataLength;
        boolean asciiOneSymbol = false;
        if (symbolIndex != -1) {
            asciiOneSymbol = true;
        }
        int dataTaken = 0;
        int dataRequired = 0;
        if (latestModeEntryC40orX12 >= 0 && symbolIndex - latestModeEntryC40orX12 + 1 > 9) {
            textLength = symbolIndex - latestModeEntryC40orX12 + 1;
            dataTaken = 0;
            dataRequired = 0;
            dataRequired += 1 + textLength / 4 * 3;
            if (!sizeFixed && (symbolIndex == text.length - 1 || symbolIndex < 0) && textLength % 4 < 3) {
                dataSize = Integer.MAX_VALUE;
                for (int i = 0; i < BarcodeDatamatrix.dmSizes.length; ++i) {
                    if (BarcodeDatamatrix.dmSizes[i].dataSize >= dataRequired + textLength % 4) {
                        dataSize = BarcodeDatamatrix.dmSizes[i].dataSize;
                        break;
                    }
                }
            }
            if (dataSize - dataOffset - dataRequired <= 2 && textLength % 4 <= 2) {
                dataRequired += textLength % 4;
            }
            else {
                dataRequired += textLength % 4 + 1;
                if (textLength % 4 == 3) {
                    --dataRequired;
                }
            }
            for (int i = dataOffset - 1; i >= 0; --i) {
                ++dataTaken;
                if (data[i] == ((prevMode == 2) ? -26 : -18)) {
                    break;
                }
            }
            if (dataRequired <= dataTaken) {
                asciiOneSymbol = false;
                textOffset -= textLength - 1;
                dataOffset -= dataTaken;
                dataLength += dataTaken;
            }
        }
        else if (latestModeEntryActual >= 0 && symbolIndex - latestModeEntryActual + 1 > 9) {
            textLength = symbolIndex - latestModeEntryActual + 1;
            dataRequired += 1 + textLength / 4 * 3;
            if (dataSize - dataOffset - dataRequired <= 2 && textLength % 4 <= 2) {
                dataRequired += textLength % 4;
            }
            else {
                dataRequired += textLength % 4 + 1;
                if (textLength % 4 == 3) {
                    --dataRequired;
                }
            }
            int dataNewOffset = 0;
            int latchEdi = -1;
            for (int j = origDataOffset; j < dataOffset; ++j) {
                if (data[j] == -16 && dataOffset - j <= dataRequired) {
                    latchEdi = j;
                    break;
                }
            }
            if (latchEdi != -1) {
                dataTaken += dataOffset - latchEdi;
                if ((text[textOffset] & 0xFF) > 127) {
                    dataTaken += 2;
                }
                else {
                    if (isDigit(text[textOffset] & 0xFF) && isDigit(text[textOffset - 1] & 0xFF) && data[dataOffset - 1] >= 49 && data[dataOffset - 1] <= 58) {
                        --dataTaken;
                    }
                    ++dataTaken;
                }
                dataNewOffset = dataOffset - latchEdi;
            }
            else {
                for (int k = symbolIndex - latestModeEntryActual; k >= 0; --k) {
                    if ((text[textOffset - k] & 0xFF) > 127) {
                        dataTaken += 2;
                    }
                    else {
                        if (k > 0 && isDigit(text[textOffset - k] & 0xFF) && isDigit(text[textOffset - k + 1] & 0xFF)) {
                            if (k == 1) {
                                dataNewOffset = dataTaken;
                            }
                            --k;
                        }
                        ++dataTaken;
                    }
                    if (k == 1) {
                        dataNewOffset = dataTaken;
                    }
                }
            }
            if (dataRequired <= dataTaken) {
                asciiOneSymbol = false;
                textOffset -= textLength - 1;
                dataOffset -= dataNewOffset;
                dataLength += dataNewOffset;
            }
        }
        if (asciiOneSymbol) {
            final int c = text[textOffset] & 0xFF;
            if (isDigit(c) && textOffset + ptrIn > 0 && isDigit(text[textOffset - 1] & 0xFF) && prevEnc == 6 && data[dataOffset - 1] >= 49 && data[dataOffset - 1] <= 58) {
                data[dataOffset + ptrOut - 1] = (byte)(((text[textOffset - 1] & 0xFF) - 48) * 10 + c - 48 + 130);
                return dataOffset - origDataOffset;
            }
            return asciiEncodation(text, textOffset + ptrIn, 1, data, dataOffset + ptrOut, dataLength, -1, -1, origDataOffset);
        }
        else {
            while (ptrIn < textLength) {
                int c = text[ptrIn + textOffset] & 0xFF;
                if (((c & 0xE0) == 0x40 || (c & 0xE0) == 0x20) && c != 95) {
                    if (ascii) {
                        if (ptrOut + 1 > dataLength) {
                            break;
                        }
                        data[dataOffset + ptrOut++] = -16;
                        ascii = false;
                    }
                    c &= 0x3F;
                    edi |= c << pedi;
                    if (pedi == 0) {
                        if (ptrOut + 3 > dataLength) {
                            break;
                        }
                        data[dataOffset + ptrOut++] = (byte)(edi >> 16);
                        data[dataOffset + ptrOut++] = (byte)(edi >> 8);
                        data[dataOffset + ptrOut++] = (byte)edi;
                        edi = 0;
                        pedi = 18;
                    }
                    else {
                        pedi -= 6;
                    }
                }
                else {
                    if (!ascii) {
                        edi |= 31 << pedi;
                        if (ptrOut + 3 - pedi / 8 > dataLength) {
                            break;
                        }
                        data[dataOffset + ptrOut++] = (byte)(edi >> 16);
                        if (pedi <= 12) {
                            data[dataOffset + ptrOut++] = (byte)(edi >> 8);
                        }
                        if (pedi <= 6) {
                            data[dataOffset + ptrOut++] = (byte)edi;
                        }
                        ascii = true;
                        pedi = 18;
                        edi = 0;
                    }
                    if (isDigit(c) && textOffset + ptrIn > 0 && isDigit(text[textOffset + ptrIn - 1] & 0xFF) && prevEnc == 6 && data[dataOffset - 1] >= 49 && data[dataOffset - 1] <= 58) {
                        data[dataOffset + ptrOut - 1] = (byte)(((text[textOffset - 1] & 0xFF) - 48) * 10 + c - 48 + 130);
                        --ptrOut;
                    }
                    else {
                        final int i = asciiEncodation(text, textOffset + ptrIn, 1, data, dataOffset + ptrOut, dataLength, -1, -1, origDataOffset);
                        if (i < 0) {
                            return -1;
                        }
                        if (data[dataOffset + ptrOut] == -21) {
                            ++ptrOut;
                        }
                        ++ptrOut;
                    }
                }
                ++ptrIn;
            }
            if (ptrIn != textLength) {
                return -1;
            }
            if (!sizeFixed && (symbolIndex == text.length - 1 || symbolIndex < 0)) {
                dataSize = Integer.MAX_VALUE;
                for (int i = 0; i < BarcodeDatamatrix.dmSizes.length; ++i) {
                    if (BarcodeDatamatrix.dmSizes[i].dataSize >= dataOffset + ptrOut + (3 - pedi / 6)) {
                        dataSize = BarcodeDatamatrix.dmSizes[i].dataSize;
                        break;
                    }
                }
            }
            if (dataSize - dataOffset - ptrOut <= 2 && pedi >= 6) {
                if (pedi != 18 && ptrOut + 2 - pedi / 8 > dataLength) {
                    return -1;
                }
                if (pedi <= 12) {
                    byte val = (byte)(edi >> 18 & 0x3F);
                    if ((val & 0x20) == 0x0) {
                        val |= 0x40;
                    }
                    data[dataOffset + ptrOut++] = (byte)(val + 1);
                }
                if (pedi <= 6) {
                    byte val = (byte)(edi >> 12 & 0x3F);
                    if ((val & 0x20) == 0x0) {
                        val |= 0x40;
                    }
                    data[dataOffset + ptrOut++] = (byte)(val + 1);
                }
            }
            else if (!ascii) {
                edi |= 31 << pedi;
                if (ptrOut + 3 - pedi / 8 > dataLength) {
                    return -1;
                }
                data[dataOffset + ptrOut++] = (byte)(edi >> 16);
                if (pedi <= 12) {
                    data[dataOffset + ptrOut++] = (byte)(edi >> 8);
                }
                if (pedi <= 6) {
                    data[dataOffset + ptrOut++] = (byte)edi;
                }
            }
            return ptrOut + dataOffset - origDataOffset;
        }
    }
    
    private static int C40OrTextEncodation(final byte[] text, int textOffset, int textLength, final byte[] data, int dataOffset, int dataLength, final boolean c40, final int symbolIndex, final int prevEnc, final int origDataOffset) {
        if (textLength == 0) {
            return 0;
        }
        int ptrIn = 0;
        int ptrOut = 0;
        final String shift2 = "!\"#$%&'()*+,-./:;<=>?@[\\]^_";
        String basic;
        String shift3;
        if (c40) {
            basic = " 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            shift3 = "`abcdefghijklmnopqrstuvwxyz{|}~\u007f";
        }
        else {
            basic = " 0123456789abcdefghijklmnopqrstuvwxyz";
            shift3 = "`ABCDEFGHIJKLMNOPQRSTUVWXYZ{|}~\u007f";
        }
        boolean addLatch = true;
        boolean usingASCII = false;
        final int mode = c40 ? 2 : 3;
        if (prevEnc == mode) {
            usingASCII = true;
            int latestModeEntry;
            for (latestModeEntry = symbolIndex - 1; latestModeEntry > 0 && BarcodeDatamatrix.switchMode[mode - 1][latestModeEntry] == mode; --latestModeEntry) {}
            int unlatch = -1;
            int dataAmountOfEncodedWithASCII = 0;
            if (symbolIndex - latestModeEntry >= 5) {
                for (int i = symbolIndex - latestModeEntry; i > 0; --i) {
                    final int c41 = text[textOffset - i] & 0xFF;
                    if (c41 > 127) {
                        dataAmountOfEncodedWithASCII += 2;
                    }
                    else {
                        ++dataAmountOfEncodedWithASCII;
                    }
                }
                for (int i = 1; i <= dataAmountOfEncodedWithASCII; ++i) {
                    if (i > dataOffset) {
                        break;
                    }
                    if (data[dataOffset - i] == -2) {
                        unlatch = dataOffset - i;
                        break;
                    }
                }
                int amountOfEncodedWithASCII = 0;
                if (unlatch >= 0) {
                    for (int i = unlatch + 1; i < dataOffset; ++i) {
                        if (data[i] == -21) {
                            ++i;
                        }
                        if (data[i] >= -127 && data[i] <= -27) {
                            ++amountOfEncodedWithASCII;
                        }
                        ++amountOfEncodedWithASCII;
                    }
                }
                else {
                    amountOfEncodedWithASCII = symbolIndex - latestModeEntry;
                }
                int dataOffsetNew = 0;
                for (int i = amountOfEncodedWithASCII; i > 0; --i) {
                    int requiredCapacityForASCII = 0;
                    int requiredCapacityForC40orText = 0;
                    for (int j = i; j >= 0; --j) {
                        int c41 = text[textOffset - j] & 0xFF;
                        if (c41 > 127) {
                            c41 -= 128;
                            requiredCapacityForC40orText += 2;
                        }
                        requiredCapacityForC40orText += ((basic.indexOf((char)c41) >= 0) ? 1 : 2);
                        if (c41 > 127) {
                            requiredCapacityForASCII += 2;
                        }
                        else {
                            if (j > 0 && isDigit(c41) && isDigit(text[textOffset - j + 1] & 0xFF)) {
                                requiredCapacityForC40orText += ((basic.indexOf((char)text[textOffset - j + 1]) >= 0) ? 1 : 2);
                                --j;
                                dataOffsetNew = requiredCapacityForASCII + 1;
                            }
                            ++requiredCapacityForASCII;
                        }
                        if (j == 1) {
                            dataOffsetNew = requiredCapacityForASCII;
                        }
                    }
                    addLatch = (unlatch < 0 || dataOffset - requiredCapacityForASCII != unlatch);
                    if (requiredCapacityForC40orText % 3 == 0 && requiredCapacityForC40orText / 3 * 2 + (addLatch ? 2 : 0) < requiredCapacityForASCII) {
                        usingASCII = false;
                        textLength = i + 1;
                        textOffset -= i;
                        dataOffset -= (addLatch ? dataOffsetNew : (dataOffsetNew + 1));
                        dataLength += (addLatch ? dataOffsetNew : (dataOffsetNew + 1));
                        break;
                    }
                    if (isDigit(text[textOffset - i] & 0xFF) && isDigit(text[textOffset - i + 1] & 0xFF)) {
                        --i;
                    }
                }
            }
        }
        else if (symbolIndex != -1) {
            usingASCII = true;
        }
        if (usingASCII) {
            return asciiEncodation(text, textOffset, 1, data, dataOffset, dataLength, (prevEnc == mode) ? 1 : -1, 1, origDataOffset);
        }
        if (addLatch) {
            if (c40) {
                data[dataOffset + ptrOut++] = -26;
            }
            else {
                data[dataOffset + ptrOut++] = -17;
            }
        }
        final int[] enc = new int[textLength * 4 + 10];
        int encPtr = 0;
        int last0 = 0;
        int last2 = 0;
        while (ptrIn < textLength) {
            if (encPtr % 3 == 0) {
                last0 = ptrIn;
                last2 = encPtr;
            }
            int c41 = text[textOffset + ptrIn++] & 0xFF;
            if (c41 > 127) {
                c41 -= 128;
                enc[encPtr++] = 1;
                enc[encPtr++] = 30;
            }
            int idx = basic.indexOf((char)c41);
            if (idx >= 0) {
                enc[encPtr++] = idx + 3;
            }
            else if (c41 < 32) {
                enc[encPtr++] = 0;
                enc[encPtr++] = c41;
            }
            else if ((idx = shift2.indexOf((char)c41)) >= 0) {
                enc[encPtr++] = 1;
                enc[encPtr++] = idx;
            }
            else {
                if ((idx = shift3.indexOf((char)c41)) < 0) {
                    continue;
                }
                enc[encPtr++] = 2;
                enc[encPtr++] = idx;
            }
        }
        if (encPtr % 3 != 0) {
            ptrIn = last0;
            encPtr = last2;
        }
        if (encPtr / 3 * 2 > dataLength - 2) {
            return -1;
        }
        for (int i = 0; i < encPtr; i += 3) {
            final int a = 1600 * enc[i] + 40 * enc[i + 1] + enc[i + 2] + 1;
            data[dataOffset + ptrOut++] = (byte)(a / 256);
            data[dataOffset + ptrOut++] = (byte)a;
        }
        if (dataLength - ptrOut > 2) {
            data[dataOffset + ptrOut++] = -2;
        }
        if (symbolIndex < 0 && textLength > ptrIn) {
            final int i = asciiEncodation(text, textOffset + ptrIn, textLength - ptrIn, data, dataOffset + ptrOut, dataLength - ptrOut, -1, -1, origDataOffset);
            return i;
        }
        return ptrOut + dataOffset - origDataOffset;
    }
    
    private static int minValueInColumn(final int[][] array, final int column) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < 6; ++i) {
            if (array[i][column] < min && array[i][column] >= 0) {
                min = array[i][column];
            }
        }
        return (min != Integer.MAX_VALUE) ? min : -1;
    }
    
    private static int valuePositionInColumn(final int[][] array, final int column, final int value) {
        for (int i = 0; i < 6; ++i) {
            if (array[i][column] == value) {
                return i;
            }
        }
        return -1;
    }
    
    private static void solveFAndSwitchMode(final int[] forMin, final int mode, final int currIndex) {
        if (forMin[mode] >= 0 && BarcodeDatamatrix.f[mode][currIndex - 1] >= 0) {
            BarcodeDatamatrix.f[mode][currIndex] = forMin[mode];
            BarcodeDatamatrix.switchMode[mode][currIndex] = mode + 1;
        }
        else {
            BarcodeDatamatrix.f[mode][currIndex] = Integer.MAX_VALUE;
        }
        for (int i = 0; i < 6; ++i) {
            if (forMin[i] < BarcodeDatamatrix.f[mode][currIndex] && forMin[i] >= 0 && BarcodeDatamatrix.f[i][currIndex - 1] >= 0) {
                BarcodeDatamatrix.f[mode][currIndex] = forMin[i];
                BarcodeDatamatrix.switchMode[mode][currIndex] = i + 1;
            }
        }
        if (BarcodeDatamatrix.f[mode][currIndex] == Integer.MAX_VALUE) {
            BarcodeDatamatrix.f[mode][currIndex] = -1;
        }
    }
    
    private static int getEncodation(final byte[] text, final int textOffset, final int textSize, final byte[] data, final int dataOffset, final int dataSize, int options, final boolean sizeFixed) {
        if (dataSize < 0) {
            return -1;
        }
        options &= 0x7;
        if (options == 0) {
            if (textSize == 0) {
                return 0;
            }
            final byte[][] dataDynamic = new byte[6][data.length];
            for (int i = 0; i < 6; ++i) {
                System.arraycopy(data, 0, dataDynamic[i], 0, data.length);
                BarcodeDatamatrix.switchMode[i][0] = i + 1;
            }
            BarcodeDatamatrix.f[0][0] = asciiEncodation(text, textOffset, 1, dataDynamic[0], dataOffset, dataSize, 0, -1, dataOffset);
            BarcodeDatamatrix.f[1][0] = C40OrTextEncodation(text, textOffset, 1, dataDynamic[1], dataOffset, dataSize, true, 0, -1, dataOffset);
            BarcodeDatamatrix.f[2][0] = C40OrTextEncodation(text, textOffset, 1, dataDynamic[2], dataOffset, dataSize, false, 0, -1, dataOffset);
            BarcodeDatamatrix.f[3][0] = b256Encodation(text, textOffset, 1, dataDynamic[3], dataOffset, dataSize, 0, -1, dataOffset);
            BarcodeDatamatrix.f[4][0] = X12Encodation(text, textOffset, 1, dataDynamic[4], dataOffset, dataSize, 0, -1, dataOffset);
            BarcodeDatamatrix.f[5][0] = EdifactEncodation(text, textOffset, 1, dataDynamic[5], dataOffset, dataSize, 0, -1, dataOffset, sizeFixed);
            final int[] dataNewOffset = new int[6];
            for (int j = 1; j < textSize; ++j) {
                final int[] tempForMin = new int[6];
                for (int k = 0; k < 6; ++k) {
                    dataNewOffset[k] = ((BarcodeDatamatrix.f[k][j - 1] >= 0) ? BarcodeDatamatrix.f[k][j - 1] : Integer.MAX_VALUE);
                }
                for (int currEnc = 0; currEnc < 6; ++currEnc) {
                    final byte[][] dataDynamicInner = new byte[6][data.length];
                    for (int prevEnc = 0; prevEnc < 6; ++prevEnc) {
                        System.arraycopy(dataDynamic[prevEnc], 0, dataDynamicInner[prevEnc], 0, data.length);
                        if (currEnc == 0) {
                            tempForMin[prevEnc] = asciiEncodation(text, textOffset + j, 1, dataDynamicInner[prevEnc], dataNewOffset[prevEnc] + dataOffset, dataSize - dataNewOffset[prevEnc], j, prevEnc + 1, dataOffset);
                        }
                        if (currEnc == 1) {
                            tempForMin[prevEnc] = C40OrTextEncodation(text, textOffset + j, 1, dataDynamicInner[prevEnc], dataNewOffset[prevEnc] + dataOffset, dataSize - dataNewOffset[prevEnc], true, j, prevEnc + 1, dataOffset);
                        }
                        if (currEnc == 2) {
                            tempForMin[prevEnc] = C40OrTextEncodation(text, textOffset + j, 1, dataDynamicInner[prevEnc], dataNewOffset[prevEnc] + dataOffset, dataSize - dataNewOffset[prevEnc], false, j, prevEnc + 1, dataOffset);
                        }
                        if (currEnc == 3) {
                            tempForMin[prevEnc] = b256Encodation(text, textOffset + j, 1, dataDynamicInner[prevEnc], dataNewOffset[prevEnc] + dataOffset, dataSize - dataNewOffset[prevEnc], j, prevEnc + 1, dataOffset);
                        }
                        if (currEnc == 4) {
                            tempForMin[prevEnc] = X12Encodation(text, textOffset + j, 1, dataDynamicInner[prevEnc], dataNewOffset[prevEnc] + dataOffset, dataSize - dataNewOffset[prevEnc], j, prevEnc + 1, dataOffset);
                        }
                        if (currEnc == 5) {
                            tempForMin[prevEnc] = EdifactEncodation(text, textOffset + j, 1, dataDynamicInner[prevEnc], dataNewOffset[prevEnc] + dataOffset, dataSize - dataNewOffset[prevEnc], j, prevEnc + 1, dataOffset, sizeFixed);
                        }
                    }
                    solveFAndSwitchMode(tempForMin, currEnc, j);
                    if (BarcodeDatamatrix.switchMode[currEnc][j] != 0) {
                        System.arraycopy(dataDynamicInner[BarcodeDatamatrix.switchMode[currEnc][j] - 1], 0, dataDynamic[currEnc], 0, data.length);
                    }
                }
            }
            final int e = minValueInColumn(BarcodeDatamatrix.f, textSize - 1);
            if (e > dataSize || e < 0) {
                return -1;
            }
            final int bestDataDynamicResultIndex = valuePositionInColumn(BarcodeDatamatrix.f, textSize - 1, e);
            System.arraycopy(dataDynamic[bestDataDynamicResultIndex], 0, data, 0, data.length);
            return e;
        }
        else {
            switch (options) {
                case 1: {
                    return asciiEncodation(text, textOffset, textSize, data, dataOffset, dataSize, -1, -1, dataOffset);
                }
                case 2: {
                    return C40OrTextEncodation(text, textOffset, textSize, data, dataOffset, dataSize, true, -1, -1, dataOffset);
                }
                case 3: {
                    return C40OrTextEncodation(text, textOffset, textSize, data, dataOffset, dataSize, false, -1, -1, dataOffset);
                }
                case 4: {
                    return b256Encodation(text, textOffset, textSize, data, dataOffset, dataSize, -1, -1, dataOffset);
                }
                case 5: {
                    return X12Encodation(text, textOffset, textSize, data, dataOffset, dataSize, -1, -1, dataOffset);
                }
                case 6: {
                    return EdifactEncodation(text, textOffset, textSize, data, dataOffset, dataSize, -1, -1, dataOffset, sizeFixed);
                }
                case 7: {
                    if (textSize > dataSize) {
                        return -1;
                    }
                    System.arraycopy(text, textOffset, data, dataOffset, textSize);
                    return textSize;
                }
                default: {
                    return -1;
                }
            }
        }
    }
    
    private static int getNumber(final byte[] text, int ptrIn, final int n) {
        int v = 0;
        for (int j = 0; j < n; ++j) {
            final int c = text[ptrIn++] & 0xFF;
            if (c < 48 || c > 57) {
                return -1;
            }
            v = v * 10 + c - 48;
        }
        return v;
    }
    
    private int processExtensions(final byte[] text, final int textOffset, final int textSize, final byte[] data) {
        if ((this.options & 0x20) == 0x0) {
            return 0;
        }
        int order = 0;
        int ptrIn = 0;
        int ptrOut = 0;
        while (ptrIn < textSize) {
            if (order > 20) {
                return -1;
            }
            int c = text[textOffset + ptrIn++] & 0xFF;
            ++order;
            switch (c) {
                case 46: {
                    this.extOut = ptrIn;
                    return ptrOut;
                }
                case 101: {
                    if (ptrIn + 6 > textSize) {
                        return -1;
                    }
                    final int eci = getNumber(text, textOffset + ptrIn, 6);
                    if (eci < 0) {
                        return -1;
                    }
                    ptrIn += 6;
                    data[ptrOut++] = -15;
                    if (eci < 127) {
                        data[ptrOut++] = (byte)(eci + 1);
                        continue;
                    }
                    if (eci < 16383) {
                        data[ptrOut++] = (byte)((eci - 127) / 254 + 128);
                        data[ptrOut++] = (byte)((eci - 127) % 254 + 1);
                        continue;
                    }
                    data[ptrOut++] = (byte)((eci - 16383) / 64516 + 192);
                    data[ptrOut++] = (byte)((eci - 16383) / 254 % 254 + 1);
                    data[ptrOut++] = (byte)((eci - 16383) % 254 + 1);
                    continue;
                }
                case 115: {
                    if (order != 1) {
                        return -1;
                    }
                    if (ptrIn + 9 > textSize) {
                        return -1;
                    }
                    final int fn = getNumber(text, textOffset + ptrIn, 2);
                    if (fn <= 0 || fn > 16) {
                        return -1;
                    }
                    ptrIn += 2;
                    final int ft = getNumber(text, textOffset + ptrIn, 2);
                    if (ft <= 1 || ft > 16) {
                        return -1;
                    }
                    ptrIn += 2;
                    final int fi = getNumber(text, textOffset + ptrIn, 5);
                    if (fi < 0 || fn >= 64516) {
                        return -1;
                    }
                    ptrIn += 5;
                    data[ptrOut++] = -23;
                    data[ptrOut++] = (byte)(fn - 1 << 4 | 17 - ft);
                    data[ptrOut++] = (byte)(fi / 254 + 1);
                    data[ptrOut++] = (byte)(fi % 254 + 1);
                    continue;
                }
                case 112: {
                    if (order != 1) {
                        return -1;
                    }
                    data[ptrOut++] = -22;
                    continue;
                }
                case 109: {
                    if (order != 1) {
                        return -1;
                    }
                    if (ptrIn + 1 > textSize) {
                        return -1;
                    }
                    c = (text[textOffset + ptrIn++] & 0xFF);
                    if (c != 53 && c != 53) {
                        return -1;
                    }
                    data[ptrOut++] = -22;
                    data[ptrOut++] = (byte)((c == 53) ? 236 : 237);
                    continue;
                }
                case 102: {
                    if (order != 1 && (order != 2 || (text[textOffset] != 115 && text[textOffset] != 109))) {
                        return -1;
                    }
                    data[ptrOut++] = -24;
                    continue;
                }
            }
        }
        return -1;
    }
    
    public int generate(final String text) throws UnsupportedEncodingException {
        byte[] t;
        try {
            t = text.getBytes(this.encoding);
        }
        catch (UnsupportedEncodingException exc) {
            throw new IllegalArgumentException("text has to be encoded in iso-8859-1");
        }
        return this.generate(t, 0, t.length);
    }
    
    public int generate(final byte[] text, final int textOffset, final int textSize) {
        final byte[] data = new byte[2500];
        this.extOut = 0;
        final int extCount = this.processExtensions(text, textOffset, textSize, data);
        if (extCount < 0) {
            return 5;
        }
        int e = -1;
        BarcodeDatamatrix.f = new int[6][textSize - this.extOut];
        BarcodeDatamatrix.switchMode = new int[6][textSize - this.extOut];
        DmParams dm;
        if (this.height == 0 || this.width == 0) {
            final DmParams last = BarcodeDatamatrix.dmSizes[BarcodeDatamatrix.dmSizes.length - 1];
            e = getEncodation(text, textOffset + this.extOut, textSize - this.extOut, data, extCount, last.dataSize - extCount, this.options, false);
            if (e < 0) {
                return 1;
            }
            int k;
            for (e += extCount, k = 0; k < BarcodeDatamatrix.dmSizes.length && BarcodeDatamatrix.dmSizes[k].dataSize < e; ++k) {}
            dm = BarcodeDatamatrix.dmSizes[k];
            this.height = dm.height;
            this.width = dm.width;
        }
        else {
            int k;
            for (k = 0; k < BarcodeDatamatrix.dmSizes.length && (this.height != BarcodeDatamatrix.dmSizes[k].height || this.width != BarcodeDatamatrix.dmSizes[k].width); ++k) {}
            if (k == BarcodeDatamatrix.dmSizes.length) {
                return 3;
            }
            dm = BarcodeDatamatrix.dmSizes[k];
            e = getEncodation(text, textOffset + this.extOut, textSize - this.extOut, data, extCount, dm.dataSize - extCount, this.options, true);
            if (e < 0) {
                return 1;
            }
            e += extCount;
        }
        if ((this.options & 0x40) != 0x0) {
            return 0;
        }
        this.image = new byte[(dm.width + 2 * this.ws + 7) / 8 * (dm.height + 2 * this.ws)];
        makePadding(data, e, dm.dataSize - e);
        this.place = Placement.doPlacement(dm.height - dm.height / dm.heightSection * 2, dm.width - dm.width / dm.widthSection * 2);
        final int full = dm.dataSize + (dm.dataSize + 2) / dm.dataBlock * dm.errorBlock;
        ReedSolomon.generateECC(data, dm.dataSize, dm.dataBlock, dm.errorBlock);
        this.draw(data, full, dm);
        return 0;
    }
    
    public Image createImage() throws BadElementException {
        if (this.image == null) {
            return null;
        }
        final byte[] g4 = CCITTG4Encoder.compress(this.image, this.width + 2 * this.ws, this.height + 2 * this.ws);
        return Image.getInstance(this.width + 2 * this.ws, this.height + 2 * this.ws, false, 256, 0, g4, null);
    }
    
    public byte[] getImage() {
        return this.image;
    }
    
    public int getHeight() {
        return this.height;
    }
    
    public void setHeight(final int height) {
        this.height = height;
    }
    
    public int getWidth() {
        return this.width;
    }
    
    public void setWidth(final int width) {
        this.width = width;
    }
    
    public int getWs() {
        return this.ws;
    }
    
    public void setWs(final int ws) {
        this.ws = ws;
    }
    
    public int getOptions() {
        return this.options;
    }
    
    public void setOptions(final int options) {
        this.options = options;
    }
    
    public void setForceSquareSize(final boolean forceSquareSize) {
        this.forceSquareSize = forceSquareSize;
    }
    
    public void placeBarcode(final PdfContentByte cb, final BaseColor foreground, final float moduleHeight, final float moduleWidth) {
        final int w = this.width + 2 * this.ws;
        final int h = this.height + 2 * this.ws;
        final int stride = (w + 7) / 8;
        final int ptr = 0;
        cb.setColorFill(foreground);
        for (int k = 0; k < h; ++k) {
            final int p = k * stride;
            for (int j = 0; j < w; ++j) {
                int b = this.image[p + j / 8] & 0xFF;
                b <<= j % 8;
                if ((b & 0x80) != 0x0) {
                    cb.rectangle(j * moduleWidth, (h - k - 1) * moduleHeight, moduleWidth, moduleHeight);
                }
            }
        }
        cb.fill();
    }
    
    public java.awt.Image createAwtImage(final Color foreground, final Color background) {
        if (this.image == null) {
            return null;
        }
        final int f = foreground.getRGB();
        final int g = background.getRGB();
        final Canvas canvas = new Canvas();
        final int w = this.width + 2 * this.ws;
        final int h = this.height + 2 * this.ws;
        final int[] pix = new int[w * h];
        final int stride = (w + 7) / 8;
        int ptr = 0;
        for (int k = 0; k < h; ++k) {
            final int p = k * stride;
            for (int j = 0; j < w; ++j) {
                int b = this.image[p + j / 8] & 0xFF;
                b <<= j % 8;
                pix[ptr++] = (((b & 0x80) == 0x0) ? g : f);
            }
        }
        final java.awt.Image img = canvas.createImage(new MemoryImageSource(w, h, pix, 0, w));
        return img;
    }
    
    static {
        dmSizes = new DmParams[] { new DmParams(10, 10, 10, 10, 3, 3, 5), new DmParams(12, 12, 12, 12, 5, 5, 7), new DmParams(8, 18, 8, 18, 5, 5, 7), new DmParams(14, 14, 14, 14, 8, 8, 10), new DmParams(8, 32, 8, 16, 10, 10, 11), new DmParams(16, 16, 16, 16, 12, 12, 12), new DmParams(12, 26, 12, 26, 16, 16, 14), new DmParams(18, 18, 18, 18, 18, 18, 14), new DmParams(20, 20, 20, 20, 22, 22, 18), new DmParams(12, 36, 12, 18, 22, 22, 18), new DmParams(22, 22, 22, 22, 30, 30, 20), new DmParams(16, 36, 16, 18, 32, 32, 24), new DmParams(24, 24, 24, 24, 36, 36, 24), new DmParams(26, 26, 26, 26, 44, 44, 28), new DmParams(16, 48, 16, 24, 49, 49, 28), new DmParams(32, 32, 16, 16, 62, 62, 36), new DmParams(36, 36, 18, 18, 86, 86, 42), new DmParams(40, 40, 20, 20, 114, 114, 48), new DmParams(44, 44, 22, 22, 144, 144, 56), new DmParams(48, 48, 24, 24, 174, 174, 68), new DmParams(52, 52, 26, 26, 204, 102, 42), new DmParams(64, 64, 16, 16, 280, 140, 56), new DmParams(72, 72, 18, 18, 368, 92, 36), new DmParams(80, 80, 20, 20, 456, 114, 48), new DmParams(88, 88, 22, 22, 576, 144, 56), new DmParams(96, 96, 24, 24, 696, 174, 68), new DmParams(104, 104, 26, 26, 816, 136, 56), new DmParams(120, 120, 20, 20, 1050, 175, 68), new DmParams(132, 132, 22, 22, 1304, 163, 62), new DmParams(144, 144, 24, 24, 1558, 156, 62) };
    }
    
    private static class DmParams
    {
        int height;
        int width;
        int heightSection;
        int widthSection;
        int dataSize;
        int dataBlock;
        int errorBlock;
        
        DmParams(final int height, final int width, final int heightSection, final int widthSection, final int dataSize, final int dataBlock, final int errorBlock) {
            this.height = height;
            this.width = width;
            this.heightSection = heightSection;
            this.widthSection = widthSection;
            this.dataSize = dataSize;
            this.dataBlock = dataBlock;
            this.errorBlock = errorBlock;
        }
    }
    
    static class Placement
    {
        private int nrow;
        private int ncol;
        private short[] array;
        private static final Hashtable<Integer, short[]> cache;
        
        private Placement() {
        }
        
        static short[] doPlacement(final int nrow, final int ncol) {
            final Integer key = nrow * 1000 + ncol;
            final short[] pc = Placement.cache.get(key);
            if (pc != null) {
                return pc;
            }
            final Placement p = new Placement();
            p.nrow = nrow;
            p.ncol = ncol;
            p.array = new short[nrow * ncol];
            p.ecc200();
            Placement.cache.put(key, p.array);
            return p.array;
        }
        
        private void module(int row, int col, final int chr, final int bit) {
            if (row < 0) {
                row += this.nrow;
                col += 4 - (this.nrow + 4) % 8;
            }
            if (col < 0) {
                col += this.ncol;
                row += 4 - (this.ncol + 4) % 8;
            }
            this.array[row * this.ncol + col] = (short)(8 * chr + bit);
        }
        
        private void utah(final int row, final int col, final int chr) {
            this.module(row - 2, col - 2, chr, 0);
            this.module(row - 2, col - 1, chr, 1);
            this.module(row - 1, col - 2, chr, 2);
            this.module(row - 1, col - 1, chr, 3);
            this.module(row - 1, col, chr, 4);
            this.module(row, col - 2, chr, 5);
            this.module(row, col - 1, chr, 6);
            this.module(row, col, chr, 7);
        }
        
        private void corner1(final int chr) {
            this.module(this.nrow - 1, 0, chr, 0);
            this.module(this.nrow - 1, 1, chr, 1);
            this.module(this.nrow - 1, 2, chr, 2);
            this.module(0, this.ncol - 2, chr, 3);
            this.module(0, this.ncol - 1, chr, 4);
            this.module(1, this.ncol - 1, chr, 5);
            this.module(2, this.ncol - 1, chr, 6);
            this.module(3, this.ncol - 1, chr, 7);
        }
        
        private void corner2(final int chr) {
            this.module(this.nrow - 3, 0, chr, 0);
            this.module(this.nrow - 2, 0, chr, 1);
            this.module(this.nrow - 1, 0, chr, 2);
            this.module(0, this.ncol - 4, chr, 3);
            this.module(0, this.ncol - 3, chr, 4);
            this.module(0, this.ncol - 2, chr, 5);
            this.module(0, this.ncol - 1, chr, 6);
            this.module(1, this.ncol - 1, chr, 7);
        }
        
        private void corner3(final int chr) {
            this.module(this.nrow - 3, 0, chr, 0);
            this.module(this.nrow - 2, 0, chr, 1);
            this.module(this.nrow - 1, 0, chr, 2);
            this.module(0, this.ncol - 2, chr, 3);
            this.module(0, this.ncol - 1, chr, 4);
            this.module(1, this.ncol - 1, chr, 5);
            this.module(2, this.ncol - 1, chr, 6);
            this.module(3, this.ncol - 1, chr, 7);
        }
        
        private void corner4(final int chr) {
            this.module(this.nrow - 1, 0, chr, 0);
            this.module(this.nrow - 1, this.ncol - 1, chr, 1);
            this.module(0, this.ncol - 3, chr, 2);
            this.module(0, this.ncol - 2, chr, 3);
            this.module(0, this.ncol - 1, chr, 4);
            this.module(1, this.ncol - 3, chr, 5);
            this.module(1, this.ncol - 2, chr, 6);
            this.module(1, this.ncol - 1, chr, 7);
        }
        
        private void ecc200() {
            Arrays.fill(this.array, (short)0);
            int chr = 1;
            int row = 4;
            int col = 0;
            do {
                if (row == this.nrow && col == 0) {
                    this.corner1(chr++);
                }
                if (row == this.nrow - 2 && col == 0 && this.ncol % 4 != 0) {
                    this.corner2(chr++);
                }
                if (row == this.nrow - 2 && col == 0 && this.ncol % 8 == 4) {
                    this.corner3(chr++);
                }
                if (row == this.nrow + 4 && col == 2 && this.ncol % 8 == 0) {
                    this.corner4(chr++);
                }
                do {
                    if (row < this.nrow && col >= 0 && this.array[row * this.ncol + col] == 0) {
                        this.utah(row, col, chr++);
                    }
                    row -= 2;
                    col += 2;
                } while (row >= 0 && col < this.ncol);
                ++row;
                col += 3;
                do {
                    if (row >= 0 && col < this.ncol && this.array[row * this.ncol + col] == 0) {
                        this.utah(row, col, chr++);
                    }
                    row += 2;
                    col -= 2;
                } while (row < this.nrow && col >= 0);
                row += 3;
                ++col;
            } while (row < this.nrow || col < this.ncol);
            if (this.array[this.nrow * this.ncol - 1] == 0) {
                this.array[this.nrow * this.ncol - 1] = (this.array[this.nrow * this.ncol - this.ncol - 2] = 1);
            }
        }
        
        static {
            cache = new Hashtable<Integer, short[]>();
        }
    }
    
    static class ReedSolomon
    {
        private static final int[] log;
        private static final int[] alog;
        private static final int[] poly5;
        private static final int[] poly7;
        private static final int[] poly10;
        private static final int[] poly11;
        private static final int[] poly12;
        private static final int[] poly14;
        private static final int[] poly18;
        private static final int[] poly20;
        private static final int[] poly24;
        private static final int[] poly28;
        private static final int[] poly36;
        private static final int[] poly42;
        private static final int[] poly48;
        private static final int[] poly56;
        private static final int[] poly62;
        private static final int[] poly68;
        
        private static int[] getPoly(final int nc) {
            switch (nc) {
                case 5: {
                    return ReedSolomon.poly5;
                }
                case 7: {
                    return ReedSolomon.poly7;
                }
                case 10: {
                    return ReedSolomon.poly10;
                }
                case 11: {
                    return ReedSolomon.poly11;
                }
                case 12: {
                    return ReedSolomon.poly12;
                }
                case 14: {
                    return ReedSolomon.poly14;
                }
                case 18: {
                    return ReedSolomon.poly18;
                }
                case 20: {
                    return ReedSolomon.poly20;
                }
                case 24: {
                    return ReedSolomon.poly24;
                }
                case 28: {
                    return ReedSolomon.poly28;
                }
                case 36: {
                    return ReedSolomon.poly36;
                }
                case 42: {
                    return ReedSolomon.poly42;
                }
                case 48: {
                    return ReedSolomon.poly48;
                }
                case 56: {
                    return ReedSolomon.poly56;
                }
                case 62: {
                    return ReedSolomon.poly62;
                }
                case 68: {
                    return ReedSolomon.poly68;
                }
                default: {
                    return null;
                }
            }
        }
        
        private static void reedSolomonBlock(final byte[] wd, final int nd, final byte[] ncout, final int nc, final int[] c) {
            for (int i = 0; i <= nc; ++i) {
                ncout[i] = 0;
            }
            for (int i = 0; i < nd; ++i) {
                final int k = (ncout[0] ^ wd[i]) & 0xFF;
                for (int j = 0; j < nc; ++j) {
                    ncout[j] = (byte)(ncout[j + 1] ^ ((k == 0) ? 0 : ((byte)ReedSolomon.alog[(ReedSolomon.log[k] + ReedSolomon.log[c[nc - j - 1]]) % 255])));
                }
            }
        }
        
        static void generateECC(final byte[] wd, final int nd, final int datablock, final int nc) {
            final int blocks = (nd + 2) / datablock;
            final byte[] buf = new byte[256];
            final byte[] ecc = new byte[256];
            final int[] c = getPoly(nc);
            for (int b = 0; b < blocks; ++b) {
                int p = 0;
                for (int n = b; n < nd; n += blocks) {
                    buf[p++] = wd[n];
                }
                reedSolomonBlock(buf, p, ecc, nc, c);
                p = 0;
                for (int n = b; n < nc * blocks; n += blocks) {
                    wd[nd + n] = ecc[p++];
                }
            }
        }
        
        static {
            log = new int[] { 0, 255, 1, 240, 2, 225, 241, 53, 3, 38, 226, 133, 242, 43, 54, 210, 4, 195, 39, 114, 227, 106, 134, 28, 243, 140, 44, 23, 55, 118, 211, 234, 5, 219, 196, 96, 40, 222, 115, 103, 228, 78, 107, 125, 135, 8, 29, 162, 244, 186, 141, 180, 45, 99, 24, 49, 56, 13, 119, 153, 212, 199, 235, 91, 6, 76, 220, 217, 197, 11, 97, 184, 41, 36, 223, 253, 116, 138, 104, 193, 229, 86, 79, 171, 108, 165, 126, 145, 136, 34, 9, 74, 30, 32, 163, 84, 245, 173, 187, 204, 142, 81, 181, 190, 46, 88, 100, 159, 25, 231, 50, 207, 57, 147, 14, 67, 120, 128, 154, 248, 213, 167, 200, 63, 236, 110, 92, 176, 7, 161, 77, 124, 221, 102, 218, 95, 198, 90, 12, 152, 98, 48, 185, 179, 42, 209, 37, 132, 224, 52, 254, 239, 117, 233, 139, 22, 105, 27, 194, 113, 230, 206, 87, 158, 80, 189, 172, 203, 109, 175, 166, 62, 127, 247, 146, 66, 137, 192, 35, 252, 10, 183, 75, 216, 31, 83, 33, 73, 164, 144, 85, 170, 246, 65, 174, 61, 188, 202, 205, 157, 143, 169, 82, 72, 182, 215, 191, 251, 47, 178, 89, 151, 101, 94, 160, 123, 26, 112, 232, 21, 51, 238, 208, 131, 58, 69, 148, 18, 15, 16, 68, 17, 121, 149, 129, 19, 155, 59, 249, 70, 214, 250, 168, 71, 201, 156, 64, 60, 237, 130, 111, 20, 93, 122, 177, 150 };
            alog = new int[] { 1, 2, 4, 8, 16, 32, 64, 128, 45, 90, 180, 69, 138, 57, 114, 228, 229, 231, 227, 235, 251, 219, 155, 27, 54, 108, 216, 157, 23, 46, 92, 184, 93, 186, 89, 178, 73, 146, 9, 18, 36, 72, 144, 13, 26, 52, 104, 208, 141, 55, 110, 220, 149, 7, 14, 28, 56, 112, 224, 237, 247, 195, 171, 123, 246, 193, 175, 115, 230, 225, 239, 243, 203, 187, 91, 182, 65, 130, 41, 82, 164, 101, 202, 185, 95, 190, 81, 162, 105, 210, 137, 63, 126, 252, 213, 135, 35, 70, 140, 53, 106, 212, 133, 39, 78, 156, 21, 42, 84, 168, 125, 250, 217, 159, 19, 38, 76, 152, 29, 58, 116, 232, 253, 215, 131, 43, 86, 172, 117, 234, 249, 223, 147, 11, 22, 44, 88, 176, 77, 154, 25, 50, 100, 200, 189, 87, 174, 113, 226, 233, 255, 211, 139, 59, 118, 236, 245, 199, 163, 107, 214, 129, 47, 94, 188, 85, 170, 121, 242, 201, 191, 83, 166, 97, 194, 169, 127, 254, 209, 143, 51, 102, 204, 181, 71, 142, 49, 98, 196, 165, 103, 206, 177, 79, 158, 17, 34, 68, 136, 61, 122, 244, 197, 167, 99, 198, 161, 111, 222, 145, 15, 30, 60, 120, 240, 205, 183, 67, 134, 33, 66, 132, 37, 74, 148, 5, 10, 20, 40, 80, 160, 109, 218, 153, 31, 62, 124, 248, 221, 151, 3, 6, 12, 24, 48, 96, 192, 173, 119, 238, 241, 207, 179, 75, 150, 1 };
            poly5 = new int[] { 228, 48, 15, 111, 62 };
            poly7 = new int[] { 23, 68, 144, 134, 240, 92, 254 };
            poly10 = new int[] { 28, 24, 185, 166, 223, 248, 116, 255, 110, 61 };
            poly11 = new int[] { 175, 138, 205, 12, 194, 168, 39, 245, 60, 97, 120 };
            poly12 = new int[] { 41, 153, 158, 91, 61, 42, 142, 213, 97, 178, 100, 242 };
            poly14 = new int[] { 156, 97, 192, 252, 95, 9, 157, 119, 138, 45, 18, 186, 83, 185 };
            poly18 = new int[] { 83, 195, 100, 39, 188, 75, 66, 61, 241, 213, 109, 129, 94, 254, 225, 48, 90, 188 };
            poly20 = new int[] { 15, 195, 244, 9, 233, 71, 168, 2, 188, 160, 153, 145, 253, 79, 108, 82, 27, 174, 186, 172 };
            poly24 = new int[] { 52, 190, 88, 205, 109, 39, 176, 21, 155, 197, 251, 223, 155, 21, 5, 172, 254, 124, 12, 181, 184, 96, 50, 193 };
            poly28 = new int[] { 211, 231, 43, 97, 71, 96, 103, 174, 37, 151, 170, 53, 75, 34, 249, 121, 17, 138, 110, 213, 141, 136, 120, 151, 233, 168, 93, 255 };
            poly36 = new int[] { 245, 127, 242, 218, 130, 250, 162, 181, 102, 120, 84, 179, 220, 251, 80, 182, 229, 18, 2, 4, 68, 33, 101, 137, 95, 119, 115, 44, 175, 184, 59, 25, 225, 98, 81, 112 };
            poly42 = new int[] { 77, 193, 137, 31, 19, 38, 22, 153, 247, 105, 122, 2, 245, 133, 242, 8, 175, 95, 100, 9, 167, 105, 214, 111, 57, 121, 21, 1, 253, 57, 54, 101, 248, 202, 69, 50, 150, 177, 226, 5, 9, 5 };
            poly48 = new int[] { 245, 132, 172, 223, 96, 32, 117, 22, 238, 133, 238, 231, 205, 188, 237, 87, 191, 106, 16, 147, 118, 23, 37, 90, 170, 205, 131, 88, 120, 100, 66, 138, 186, 240, 82, 44, 176, 87, 187, 147, 160, 175, 69, 213, 92, 253, 225, 19 };
            poly56 = new int[] { 175, 9, 223, 238, 12, 17, 220, 208, 100, 29, 175, 170, 230, 192, 215, 235, 150, 159, 36, 223, 38, 200, 132, 54, 228, 146, 218, 234, 117, 203, 29, 232, 144, 238, 22, 150, 201, 117, 62, 207, 164, 13, 137, 245, 127, 67, 247, 28, 155, 43, 203, 107, 233, 53, 143, 46 };
            poly62 = new int[] { 242, 93, 169, 50, 144, 210, 39, 118, 202, 188, 201, 189, 143, 108, 196, 37, 185, 112, 134, 230, 245, 63, 197, 190, 250, 106, 185, 221, 175, 64, 114, 71, 161, 44, 147, 6, 27, 218, 51, 63, 87, 10, 40, 130, 188, 17, 163, 31, 176, 170, 4, 107, 232, 7, 94, 166, 224, 124, 86, 47, 11, 204 };
            poly68 = new int[] { 220, 228, 173, 89, 251, 149, 159, 56, 89, 33, 147, 244, 154, 36, 73, 127, 213, 136, 248, 180, 234, 197, 158, 177, 68, 122, 93, 213, 15, 160, 227, 236, 66, 139, 153, 185, 202, 167, 179, 25, 220, 232, 96, 210, 231, 136, 223, 239, 181, 241, 59, 52, 172, 25, 49, 232, 211, 189, 64, 54, 108, 153, 132, 63, 96, 103, 82, 186 };
        }
    }
}
