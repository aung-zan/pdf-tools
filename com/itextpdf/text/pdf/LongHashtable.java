// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.NoSuchElementException;
import java.util.Arrays;
import java.util.Iterator;
import com.itextpdf.text.error_messages.MessageLocalization;

public class LongHashtable implements Cloneable
{
    private transient Entry[] table;
    private transient int count;
    private int threshold;
    private float loadFactor;
    
    public LongHashtable() {
        this(150, 0.75f);
    }
    
    public LongHashtable(final int initialCapacity) {
        this(initialCapacity, 0.75f);
    }
    
    public LongHashtable(int initialCapacity, final float loadFactor) {
        if (initialCapacity < 0) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("illegal.capacity.1", initialCapacity));
        }
        if (loadFactor <= 0.0f) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("illegal.load.1", String.valueOf(loadFactor)));
        }
        if (initialCapacity == 0) {
            initialCapacity = 1;
        }
        this.loadFactor = loadFactor;
        this.table = new Entry[initialCapacity];
        this.threshold = (int)(initialCapacity * loadFactor);
    }
    
    public int size() {
        return this.count;
    }
    
    public boolean isEmpty() {
        return this.count == 0;
    }
    
    public boolean contains(final long value) {
        final Entry[] tab = this.table;
        int i = tab.length;
        while (i-- > 0) {
            for (Entry e = tab[i]; e != null; e = e.next) {
                if (e.value == value) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean containsValue(final long value) {
        return this.contains(value);
    }
    
    public boolean containsKey(final long key) {
        final Entry[] tab = this.table;
        final int hash = (int)(key ^ key >>> 32);
        final int index = (hash & Integer.MAX_VALUE) % tab.length;
        for (Entry e = tab[index]; e != null; e = e.next) {
            if (e.hash == hash && e.key == key) {
                return true;
            }
        }
        return false;
    }
    
    public long get(final long key) {
        final Entry[] tab = this.table;
        final int hash = (int)(key ^ key >>> 32);
        final int index = (hash & Integer.MAX_VALUE) % tab.length;
        for (Entry e = tab[index]; e != null; e = e.next) {
            if (e.hash == hash && e.key == key) {
                return e.value;
            }
        }
        return 0L;
    }
    
    protected void rehash() {
        final int oldCapacity = this.table.length;
        final Entry[] oldMap = this.table;
        final int newCapacity = oldCapacity * 2 + 1;
        final Entry[] newMap = new Entry[newCapacity];
        this.threshold = (int)(newCapacity * this.loadFactor);
        this.table = newMap;
        int i = oldCapacity;
        while (i-- > 0) {
            Entry e;
            int index;
            for (Entry old = oldMap[i]; old != null; old = old.next, index = (e.hash & Integer.MAX_VALUE) % newCapacity, e.next = newMap[index], newMap[index] = e) {
                e = old;
            }
        }
    }
    
    public long put(final long key, final long value) {
        Entry[] tab = this.table;
        final int hash = (int)(key ^ key >>> 32);
        int index = (hash & Integer.MAX_VALUE) % tab.length;
        for (Entry e = tab[index]; e != null; e = e.next) {
            if (e.hash == hash && e.key == key) {
                final long old = e.value;
                e.value = value;
                return old;
            }
        }
        if (this.count >= this.threshold) {
            this.rehash();
            tab = this.table;
            index = (hash & Integer.MAX_VALUE) % tab.length;
        }
        Entry e = new Entry(hash, key, value, tab[index]);
        tab[index] = e;
        ++this.count;
        return 0L;
    }
    
    public long remove(final long key) {
        final Entry[] tab = this.table;
        final int hash = (int)(key ^ key >>> 32);
        final int index = (hash & Integer.MAX_VALUE) % tab.length;
        Entry e = tab[index];
        Entry prev = null;
        while (e != null) {
            if (e.hash == hash && e.key == key) {
                if (prev != null) {
                    prev.next = e.next;
                }
                else {
                    tab[index] = e.next;
                }
                --this.count;
                final long oldValue = e.value;
                e.value = 0L;
                return oldValue;
            }
            prev = e;
            e = e.next;
        }
        return 0L;
    }
    
    public void clear() {
        final Entry[] tab = this.table;
        int index = tab.length;
        while (--index >= 0) {
            tab[index] = null;
        }
        this.count = 0;
    }
    
    public Iterator<Entry> getEntryIterator() {
        return new LongHashtableIterator(this.table);
    }
    
    public long[] toOrderedKeys() {
        final long[] res = this.getKeys();
        Arrays.sort(res);
        return res;
    }
    
    public long[] getKeys() {
        final long[] res = new long[this.count];
        int ptr = 0;
        int index = this.table.length;
        Entry entry = null;
        while (true) {
            if (entry == null) {
                while (index-- > 0 && (entry = this.table[index]) == null) {}
            }
            if (entry == null) {
                break;
            }
            final Entry e = entry;
            entry = e.next;
            res[ptr++] = e.key;
        }
        return res;
    }
    
    public long getOneKey() {
        if (this.count == 0) {
            return 0L;
        }
        int index = this.table.length;
        Entry entry = null;
        while (index-- > 0 && (entry = this.table[index]) == null) {}
        if (entry == null) {
            return 0L;
        }
        return entry.key;
    }
    
    public Object clone() {
        try {
            final LongHashtable t = (LongHashtable)super.clone();
            t.table = new Entry[this.table.length];
            int i = this.table.length;
            while (i-- > 0) {
                t.table[i] = ((this.table[i] != null) ? ((Entry)this.table[i].clone()) : null);
            }
            return t;
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }
    
    static class Entry
    {
        int hash;
        long key;
        long value;
        Entry next;
        
        protected Entry(final int hash, final long key, final long value, final Entry next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }
        
        public long getKey() {
            return this.key;
        }
        
        public long getValue() {
            return this.value;
        }
        
        @Override
        protected Object clone() {
            final Entry entry = new Entry(this.hash, this.key, this.value, (this.next != null) ? ((Entry)this.next.clone()) : null);
            return entry;
        }
    }
    
    static class LongHashtableIterator implements Iterator<Entry>
    {
        int index;
        Entry[] table;
        Entry entry;
        
        LongHashtableIterator(final Entry[] table) {
            this.table = table;
            this.index = table.length;
        }
        
        @Override
        public boolean hasNext() {
            if (this.entry != null) {
                return true;
            }
            while (this.index-- > 0) {
                if ((this.entry = this.table[this.index]) != null) {
                    return true;
                }
            }
            return false;
        }
        
        @Override
        public Entry next() {
            if (this.entry == null) {
                while (this.index-- > 0 && (this.entry = this.table[this.index]) == null) {}
            }
            if (this.entry != null) {
                final Entry e = this.entry;
                this.entry = e.next;
                return e;
            }
            throw new NoSuchElementException(MessageLocalization.getComposedMessage("inthashtableiterator", new Object[0]));
        }
        
        @Override
        public void remove() {
            throw new UnsupportedOperationException(MessageLocalization.getComposedMessage("remove.not.supported", new Object[0]));
        }
    }
}
