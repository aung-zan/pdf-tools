// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.events;

import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfAnnotation;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfRectangle;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import java.io.IOException;
import com.itextpdf.text.pdf.TextField;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfFormField;
import java.util.HashMap;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPageEventHelper;

public class FieldPositioningEvents extends PdfPageEventHelper implements PdfPCellEvent
{
    protected HashMap<String, PdfFormField> genericChunkFields;
    protected PdfFormField cellField;
    protected PdfWriter fieldWriter;
    protected PdfFormField parent;
    public float padding;
    
    public FieldPositioningEvents() {
        this.genericChunkFields = new HashMap<String, PdfFormField>();
        this.cellField = null;
        this.fieldWriter = null;
        this.parent = null;
    }
    
    public void addField(final String text, final PdfFormField field) {
        this.genericChunkFields.put(text, field);
    }
    
    public FieldPositioningEvents(final PdfWriter writer, final PdfFormField field) {
        this.genericChunkFields = new HashMap<String, PdfFormField>();
        this.cellField = null;
        this.fieldWriter = null;
        this.parent = null;
        this.cellField = field;
        this.fieldWriter = writer;
    }
    
    public FieldPositioningEvents(final PdfFormField parent, final PdfFormField field) {
        this.genericChunkFields = new HashMap<String, PdfFormField>();
        this.cellField = null;
        this.fieldWriter = null;
        this.parent = null;
        this.cellField = field;
        this.parent = parent;
    }
    
    public FieldPositioningEvents(final PdfWriter writer, final String text) throws IOException, DocumentException {
        this.genericChunkFields = new HashMap<String, PdfFormField>();
        this.cellField = null;
        this.fieldWriter = null;
        this.parent = null;
        this.fieldWriter = writer;
        final TextField tf = new TextField(writer, new Rectangle(0.0f, 0.0f), text);
        tf.setFontSize(14.0f);
        this.cellField = tf.getTextField();
    }
    
    public FieldPositioningEvents(final PdfWriter writer, final PdfFormField parent, final String text) throws IOException, DocumentException {
        this.genericChunkFields = new HashMap<String, PdfFormField>();
        this.cellField = null;
        this.fieldWriter = null;
        this.parent = null;
        this.parent = parent;
        final TextField tf = new TextField(writer, new Rectangle(0.0f, 0.0f), text);
        tf.setFontSize(14.0f);
        this.cellField = tf.getTextField();
    }
    
    public void setPadding(final float padding) {
        this.padding = padding;
    }
    
    public void setParent(final PdfFormField parent) {
        this.parent = parent;
    }
    
    @Override
    public void onGenericTag(final PdfWriter writer, final Document document, final Rectangle rect, final String text) {
        rect.setBottom(rect.getBottom() - 3.0f);
        PdfFormField field = this.genericChunkFields.get(text);
        if (field == null) {
            final TextField tf = new TextField(writer, new Rectangle(rect.getLeft(this.padding), rect.getBottom(this.padding), rect.getRight(this.padding), rect.getTop(this.padding)), text);
            tf.setFontSize(14.0f);
            try {
                field = tf.getTextField();
            }
            catch (Exception e) {
                throw new ExceptionConverter(e);
            }
        }
        else {
            field.put(PdfName.RECT, new PdfRectangle(rect.getLeft(this.padding), rect.getBottom(this.padding), rect.getRight(this.padding), rect.getTop(this.padding)));
        }
        if (this.parent == null) {
            writer.addAnnotation(field);
        }
        else {
            this.parent.addKid(field);
        }
    }
    
    @Override
    public void cellLayout(final PdfPCell cell, final Rectangle rect, final PdfContentByte[] canvases) {
        if (this.cellField == null || (this.fieldWriter == null && this.parent == null)) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("you.have.used.the.wrong.constructor.for.this.fieldpositioningevents.class", new Object[0]));
        }
        this.cellField.put(PdfName.RECT, new PdfRectangle(rect.getLeft(this.padding), rect.getBottom(this.padding), rect.getRight(this.padding), rect.getTop(this.padding)));
        if (this.parent == null) {
            this.fieldWriter.addAnnotation(this.cellField);
        }
        else {
            this.parent.addKid(this.cellField);
        }
    }
}
