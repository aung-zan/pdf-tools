// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.events;

import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTableEventSplit;
import java.util.Iterator;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPTableEvent;
import java.util.ArrayList;
import com.itextpdf.text.pdf.PdfPTableEventAfterSplit;

public class PdfPTableEventForwarder implements PdfPTableEventAfterSplit
{
    protected ArrayList<PdfPTableEvent> events;
    
    public PdfPTableEventForwarder() {
        this.events = new ArrayList<PdfPTableEvent>();
    }
    
    public void addTableEvent(final PdfPTableEvent event) {
        this.events.add(event);
    }
    
    @Override
    public void tableLayout(final PdfPTable table, final float[][] widths, final float[] heights, final int headerRows, final int rowStart, final PdfContentByte[] canvases) {
        for (final PdfPTableEvent event : this.events) {
            event.tableLayout(table, widths, heights, headerRows, rowStart, canvases);
        }
    }
    
    @Override
    public void splitTable(final PdfPTable table) {
        for (final PdfPTableEvent event : this.events) {
            if (event instanceof PdfPTableEventSplit) {
                ((PdfPTableEventSplit)event).splitTable(table);
            }
        }
    }
    
    @Override
    public void afterSplitTable(final PdfPTable table, final PdfPRow startRow, final int startIdx) {
        for (final PdfPTableEvent event : this.events) {
            if (event instanceof PdfPTableEventAfterSplit) {
                ((PdfPTableEventAfterSplit)event).afterSplitTable(table, startRow, startIdx);
            }
        }
    }
}
