// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.events;

import java.util.Iterator;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import java.util.ArrayList;
import com.itextpdf.text.pdf.PdfPCellEvent;

public class PdfPCellEventForwarder implements PdfPCellEvent
{
    protected ArrayList<PdfPCellEvent> events;
    
    public PdfPCellEventForwarder() {
        this.events = new ArrayList<PdfPCellEvent>();
    }
    
    public void addCellEvent(final PdfPCellEvent event) {
        this.events.add(event);
    }
    
    @Override
    public void cellLayout(final PdfPCell cell, final Rectangle position, final PdfContentByte[] canvases) {
        for (final PdfPCellEvent event : this.events) {
            event.cellLayout(cell, position, canvases);
        }
    }
}
