// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.Arrays;
import com.itextpdf.text.BaseColor;
import java.util.Locale;
import com.itextpdf.text.error_messages.MessageLocalization;

public class PdfDeviceNColor implements ICachedColorSpace, IPdfSpecialColorSpace
{
    PdfSpotColor[] spotColors;
    ColorDetails[] colorantsDetails;
    
    public PdfDeviceNColor(final PdfSpotColor[] spotColors) {
        this.spotColors = spotColors;
    }
    
    public int getNumberOfColorants() {
        return this.spotColors.length;
    }
    
    public PdfSpotColor[] getSpotColors() {
        return this.spotColors;
    }
    
    @Override
    public ColorDetails[] getColorantDetails(final PdfWriter writer) {
        if (this.colorantsDetails == null) {
            this.colorantsDetails = new ColorDetails[this.spotColors.length];
            int i = 0;
            for (final PdfSpotColor spotColorant : this.spotColors) {
                this.colorantsDetails[i] = writer.addSimple(spotColorant);
                ++i;
            }
        }
        return this.colorantsDetails;
    }
    
    @Override
    public PdfObject getPdfObject(final PdfWriter writer) {
        final PdfArray array = new PdfArray(PdfName.DEVICEN);
        final PdfArray colorants = new PdfArray();
        final float[] colorantsRanges = new float[this.spotColors.length * 2];
        final PdfDictionary colorantsDict = new PdfDictionary();
        String psFunFooter = "";
        final int numberOfColorants = this.spotColors.length;
        final float[][] CMYK = new float[4][numberOfColorants];
        for (int i = 0; i < numberOfColorants; ++i) {
            final PdfSpotColor spotColorant = this.spotColors[i];
            colorantsRanges[2 * i] = 0.0f;
            colorantsRanges[2 * i + 1] = 1.0f;
            colorants.add(spotColorant.getName());
            if (colorantsDict.get(spotColorant.getName()) != null) {
                throw new RuntimeException(MessageLocalization.getComposedMessage("devicen.component.names.shall.be.different", new Object[0]));
            }
            if (this.colorantsDetails != null) {
                colorantsDict.put(spotColorant.getName(), this.colorantsDetails[i].getIndirectReference());
            }
            else {
                colorantsDict.put(spotColorant.getName(), spotColorant.getPdfObject(writer));
            }
            final BaseColor color = spotColorant.getAlternativeCS();
            if (color instanceof ExtendedColor) {
                final int type = ((ExtendedColor)color).type;
                switch (type) {
                    case 1: {
                        CMYK[0][i] = 0.0f;
                        CMYK[1][i] = 0.0f;
                        CMYK[2][i] = 0.0f;
                        CMYK[3][i] = 1.0f - ((GrayColor)color).getGray();
                        break;
                    }
                    case 2: {
                        CMYK[0][i] = ((CMYKColor)color).getCyan();
                        CMYK[1][i] = ((CMYKColor)color).getMagenta();
                        CMYK[2][i] = ((CMYKColor)color).getYellow();
                        CMYK[3][i] = ((CMYKColor)color).getBlack();
                        break;
                    }
                    case 7: {
                        final CMYKColor cmyk = ((LabColor)color).toCmyk();
                        CMYK[0][i] = cmyk.getCyan();
                        CMYK[1][i] = cmyk.getMagenta();
                        CMYK[2][i] = cmyk.getYellow();
                        CMYK[3][i] = cmyk.getBlack();
                        break;
                    }
                    default: {
                        throw new RuntimeException(MessageLocalization.getComposedMessage("only.rgb.gray.and.cmyk.are.supported.as.alternative.color.spaces", new Object[0]));
                    }
                }
            }
            else {
                final float r = (float)color.getRed();
                final float g = (float)color.getGreen();
                final float b = (float)color.getBlue();
                float computedC = 0.0f;
                float computedM = 0.0f;
                float computedY = 0.0f;
                float computedK = 0.0f;
                if (r == 0.0f && g == 0.0f && b == 0.0f) {
                    computedK = 1.0f;
                }
                else {
                    computedC = 1.0f - r / 255.0f;
                    computedM = 1.0f - g / 255.0f;
                    computedY = 1.0f - b / 255.0f;
                    final float minCMY = Math.min(computedC, Math.min(computedM, computedY));
                    computedC = (computedC - minCMY) / (1.0f - minCMY);
                    computedM = (computedM - minCMY) / (1.0f - minCMY);
                    computedY = (computedY - minCMY) / (1.0f - minCMY);
                    computedK = minCMY;
                }
                CMYK[0][i] = computedC;
                CMYK[1][i] = computedM;
                CMYK[2][i] = computedY;
                CMYK[3][i] = computedK;
            }
            psFunFooter += "pop ";
        }
        array.add(colorants);
        String psFunHeader = String.format(Locale.US, "1.000000 %d 1 roll ", numberOfColorants + 1);
        array.add(PdfName.DEVICECMYK);
        psFunHeader = psFunHeader + psFunHeader + psFunHeader + psFunHeader;
        String psFun = "";
        for (int i = numberOfColorants + 4; i > numberOfColorants; --i) {
            psFun += String.format(Locale.US, "%d -1 roll ", i);
            for (int j = numberOfColorants; j > 0; --j) {
                psFun += String.format(Locale.US, "%d index %f mul 1.000000 cvr exch sub mul ", j, CMYK[numberOfColorants + 4 - i][numberOfColorants - j]);
            }
            psFun += String.format(Locale.US, "1.000000 cvr exch sub %d 1 roll ", i);
        }
        final PdfFunction func = PdfFunction.type4(writer, colorantsRanges, new float[] { 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f }, "{ " + psFunHeader + psFun + psFunFooter + "}");
        array.add(func.getReference());
        final PdfDictionary attr = new PdfDictionary();
        attr.put(PdfName.SUBTYPE, PdfName.NCHANNEL);
        attr.put(PdfName.COLORANTS, colorantsDict);
        array.add(attr);
        return array;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PdfDeviceNColor)) {
            return false;
        }
        final PdfDeviceNColor that = (PdfDeviceNColor)o;
        return Arrays.equals(this.spotColors, that.spotColors);
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.spotColors);
    }
}
