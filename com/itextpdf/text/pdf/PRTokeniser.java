// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.io.RandomAccessSourceFactory;
import com.itextpdf.text.exceptions.InvalidPdfException;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.io.IOException;

public class PRTokeniser
{
    private final StringBuilder outBuf;
    public static final boolean[] delims;
    static final String EMPTY = "";
    private final RandomAccessFileOrArray file;
    protected TokenType type;
    protected String stringValue;
    protected int reference;
    protected int generation;
    protected boolean hexString;
    
    public PRTokeniser(final RandomAccessFileOrArray file) {
        this.outBuf = new StringBuilder();
        this.file = file;
    }
    
    public void seek(final long pos) throws IOException {
        this.file.seek(pos);
    }
    
    public long getFilePointer() throws IOException {
        return this.file.getFilePointer();
    }
    
    public void close() throws IOException {
        this.file.close();
    }
    
    public long length() throws IOException {
        return this.file.length();
    }
    
    public int read() throws IOException {
        return this.file.read();
    }
    
    public RandomAccessFileOrArray getSafeFile() {
        return new RandomAccessFileOrArray(this.file);
    }
    
    public RandomAccessFileOrArray getFile() {
        return this.file;
    }
    
    public String readString(int size) throws IOException {
        final StringBuilder buf = new StringBuilder();
        while (size-- > 0) {
            final int ch = this.read();
            if (ch == -1) {
                break;
            }
            buf.append((char)ch);
        }
        return buf.toString();
    }
    
    public static final boolean isWhitespace(final int ch) {
        return isWhitespace(ch, true);
    }
    
    public static final boolean isWhitespace(final int ch, final boolean isWhitespace) {
        return (isWhitespace && ch == 0) || ch == 9 || ch == 10 || ch == 12 || ch == 13 || ch == 32;
    }
    
    public static final boolean isDelimiter(final int ch) {
        return ch == 40 || ch == 41 || ch == 60 || ch == 62 || ch == 91 || ch == 93 || ch == 47 || ch == 37;
    }
    
    public static final boolean isDelimiterWhitespace(final int ch) {
        return PRTokeniser.delims[ch + 1];
    }
    
    public TokenType getTokenType() {
        return this.type;
    }
    
    public String getStringValue() {
        return this.stringValue;
    }
    
    public int getReference() {
        return this.reference;
    }
    
    public int getGeneration() {
        return this.generation;
    }
    
    public void backOnePosition(final int ch) {
        if (ch != -1) {
            this.file.pushBack((byte)ch);
        }
    }
    
    public void throwError(final String error) throws IOException {
        throw new InvalidPdfException(MessageLocalization.getComposedMessage("1.at.file.pointer.2", error, String.valueOf(this.file.getFilePointer())));
    }
    
    public int getHeaderOffset() throws IOException {
        final String str = this.readString(1024);
        int idx = str.indexOf("%PDF-");
        if (idx < 0) {
            idx = str.indexOf("%FDF-");
            if (idx < 0) {
                throw new InvalidPdfException(MessageLocalization.getComposedMessage("pdf.header.not.found", new Object[0]));
            }
        }
        return idx;
    }
    
    public char checkPdfHeader() throws IOException {
        this.file.seek(0L);
        final String str = this.readString(1024);
        final int idx = str.indexOf("%PDF-");
        if (idx != 0) {
            throw new InvalidPdfException(MessageLocalization.getComposedMessage("pdf.header.not.found", new Object[0]));
        }
        return str.charAt(7);
    }
    
    public void checkFdfHeader() throws IOException {
        this.file.seek(0L);
        final String str = this.readString(1024);
        final int idx = str.indexOf("%FDF-");
        if (idx != 0) {
            throw new InvalidPdfException(MessageLocalization.getComposedMessage("fdf.header.not.found", new Object[0]));
        }
    }
    
    public long getStartxref() throws IOException {
        final int arrLength = 1024;
        final long fileLength = this.file.length();
        long pos = fileLength - arrLength;
        if (pos < 1L) {
            pos = 1L;
        }
        while (pos > 0L) {
            this.file.seek(pos);
            final String str = this.readString(arrLength);
            final int idx = str.lastIndexOf("startxref");
            if (idx >= 0) {
                return pos + idx;
            }
            pos = pos - arrLength + 9L;
        }
        throw new InvalidPdfException(MessageLocalization.getComposedMessage("pdf.startxref.not.found", new Object[0]));
    }
    
    public static int getHex(final int v) {
        if (v >= 48 && v <= 57) {
            return v - 48;
        }
        if (v >= 65 && v <= 70) {
            return v - 65 + 10;
        }
        if (v >= 97 && v <= 102) {
            return v - 97 + 10;
        }
        return -1;
    }
    
    public void nextValidToken() throws IOException {
        int level = 0;
        String n1 = null;
        String n2 = null;
        long ptr = 0L;
        while (this.nextToken()) {
            if (this.type == TokenType.COMMENT) {
                continue;
            }
            switch (level) {
                case 0: {
                    if (this.type != TokenType.NUMBER) {
                        return;
                    }
                    ptr = this.file.getFilePointer();
                    n1 = this.stringValue;
                    ++level;
                    continue;
                }
                case 1: {
                    if (this.type != TokenType.NUMBER) {
                        this.file.seek(ptr);
                        this.type = TokenType.NUMBER;
                        this.stringValue = n1;
                        return;
                    }
                    n2 = this.stringValue;
                    ++level;
                    continue;
                }
                default: {
                    if (this.type != TokenType.OTHER || !this.stringValue.equals("R")) {
                        this.file.seek(ptr);
                        this.type = TokenType.NUMBER;
                        this.stringValue = n1;
                        return;
                    }
                    this.type = TokenType.REF;
                    this.reference = Integer.parseInt(n1);
                    this.generation = Integer.parseInt(n2);
                    return;
                }
            }
        }
        if (level == 1) {
            this.type = TokenType.NUMBER;
        }
    }
    
    public boolean nextToken() throws IOException {
        int ch = 0;
        do {
            ch = this.file.read();
        } while (ch != -1 && isWhitespace(ch));
        if (ch == -1) {
            this.type = TokenType.ENDOFFILE;
            return false;
        }
        this.outBuf.setLength(0);
        this.stringValue = "";
        switch (ch) {
            case 91: {
                this.type = TokenType.START_ARRAY;
                break;
            }
            case 93: {
                this.type = TokenType.END_ARRAY;
                break;
            }
            case 47: {
                this.outBuf.setLength(0);
                this.type = TokenType.NAME;
                while (true) {
                    ch = this.file.read();
                    if (PRTokeniser.delims[ch + 1]) {
                        break;
                    }
                    if (ch == 35) {
                        ch = (getHex(this.file.read()) << 4) + getHex(this.file.read());
                    }
                    this.outBuf.append((char)ch);
                }
                this.backOnePosition(ch);
                break;
            }
            case 62: {
                ch = this.file.read();
                if (ch != 62) {
                    this.throwError(MessageLocalization.getComposedMessage("greaterthan.not.expected", new Object[0]));
                }
                this.type = TokenType.END_DIC;
                break;
            }
            case 60: {
                int v1 = this.file.read();
                if (v1 == 60) {
                    this.type = TokenType.START_DIC;
                    break;
                }
                this.outBuf.setLength(0);
                this.type = TokenType.STRING;
                this.hexString = true;
                int v2 = 0;
                while (true) {
                    if (isWhitespace(v1)) {
                        v1 = this.file.read();
                    }
                    else {
                        if (v1 == 62) {
                            break;
                        }
                        v1 = getHex(v1);
                        if (v1 < 0) {
                            break;
                        }
                        for (v2 = this.file.read(); isWhitespace(v2); v2 = this.file.read()) {}
                        if (v2 == 62) {
                            ch = v1 << 4;
                            this.outBuf.append((char)ch);
                            break;
                        }
                        v2 = getHex(v2);
                        if (v2 < 0) {
                            break;
                        }
                        ch = (v1 << 4) + v2;
                        this.outBuf.append((char)ch);
                        v1 = this.file.read();
                    }
                }
                if (v1 < 0 || v2 < 0) {
                    this.throwError(MessageLocalization.getComposedMessage("error.reading.string", new Object[0]));
                    break;
                }
                break;
            }
            case 37: {
                this.type = TokenType.COMMENT;
                do {
                    ch = this.file.read();
                    if (ch != -1 && ch != 13) {
                        continue;
                    }
                    break;
                } while (ch != 10);
                break;
            }
            case 40: {
                this.outBuf.setLength(0);
                this.type = TokenType.STRING;
                this.hexString = false;
                int nesting = 0;
                while (true) {
                    ch = this.file.read();
                    if (ch == -1) {
                        break;
                    }
                    if (ch == 40) {
                        ++nesting;
                    }
                    else if (ch == 41) {
                        --nesting;
                    }
                    else if (ch == 92) {
                        boolean lineBreak = false;
                        ch = this.file.read();
                        switch (ch) {
                            case 110: {
                                ch = 10;
                                break;
                            }
                            case 114: {
                                ch = 13;
                                break;
                            }
                            case 116: {
                                ch = 9;
                                break;
                            }
                            case 98: {
                                ch = 8;
                                break;
                            }
                            case 102: {
                                ch = 12;
                                break;
                            }
                            case 40:
                            case 41:
                            case 92: {
                                break;
                            }
                            case 13: {
                                lineBreak = true;
                                ch = this.file.read();
                                if (ch != 10) {
                                    this.backOnePosition(ch);
                                    break;
                                }
                                break;
                            }
                            case 10: {
                                lineBreak = true;
                                break;
                            }
                            default: {
                                if (ch < 48) {
                                    break;
                                }
                                if (ch > 55) {
                                    break;
                                }
                                int octal = ch - 48;
                                ch = this.file.read();
                                if (ch < 48 || ch > 55) {
                                    this.backOnePosition(ch);
                                    ch = octal;
                                    break;
                                }
                                octal = (octal << 3) + ch - 48;
                                ch = this.file.read();
                                if (ch < 48 || ch > 55) {
                                    this.backOnePosition(ch);
                                    ch = octal;
                                    break;
                                }
                                octal = (octal << 3) + ch - 48;
                                ch = (octal & 0xFF);
                                break;
                            }
                        }
                        if (lineBreak) {
                            continue;
                        }
                        if (ch < 0) {
                            break;
                        }
                    }
                    else if (ch == 13) {
                        ch = this.file.read();
                        if (ch < 0) {
                            break;
                        }
                        if (ch != 10) {
                            this.backOnePosition(ch);
                            ch = 10;
                        }
                    }
                    if (nesting == -1) {
                        break;
                    }
                    this.outBuf.append((char)ch);
                }
                if (ch == -1) {
                    this.throwError(MessageLocalization.getComposedMessage("error.reading.string", new Object[0]));
                    break;
                }
                break;
            }
            default: {
                this.outBuf.setLength(0);
                if (ch == 45 || ch == 43 || ch == 46 || (ch >= 48 && ch <= 57)) {
                    this.type = TokenType.NUMBER;
                    boolean isReal = false;
                    int numberOfMinuses = 0;
                    if (ch == 45) {
                        do {
                            ++numberOfMinuses;
                            ch = this.file.read();
                        } while (ch == 45);
                        this.outBuf.append('-');
                    }
                    else {
                        this.outBuf.append((char)ch);
                        ch = this.file.read();
                    }
                    while (ch != -1 && ((ch >= 48 && ch <= 57) || ch == 46)) {
                        if (ch == 46) {
                            isReal = true;
                        }
                        this.outBuf.append((char)ch);
                        ch = this.file.read();
                    }
                    if (numberOfMinuses > 1 && !isReal) {
                        this.outBuf.setLength(0);
                        this.outBuf.append('0');
                    }
                }
                else {
                    this.type = TokenType.OTHER;
                    do {
                        this.outBuf.append((char)ch);
                        ch = this.file.read();
                    } while (!PRTokeniser.delims[ch + 1]);
                }
                if (ch != -1) {
                    this.backOnePosition(ch);
                    break;
                }
                break;
            }
        }
        if (this.outBuf != null) {
            this.stringValue = this.outBuf.toString();
        }
        return true;
    }
    
    public long longValue() {
        return Long.parseLong(this.stringValue);
    }
    
    public int intValue() {
        return Integer.parseInt(this.stringValue);
    }
    
    public boolean readLineSegment(final byte[] input) throws IOException {
        return this.readLineSegment(input, true);
    }
    
    public boolean readLineSegment(final byte[] input, final boolean isNullWhitespace) throws IOException {
        int c = -1;
        boolean eol = false;
        int ptr = 0;
        final int len = input.length;
        if (ptr < len) {
            while (isWhitespace(c = this.read(), isNullWhitespace)) {}
        }
        while (!eol && ptr < len) {
            switch (c) {
                case -1:
                case 10: {
                    eol = true;
                    break;
                }
                case 13: {
                    eol = true;
                    final long cur = this.getFilePointer();
                    if (this.read() != 10) {
                        this.seek(cur);
                        break;
                    }
                    break;
                }
                default: {
                    input[ptr++] = (byte)c;
                    break;
                }
            }
            if (eol) {
                break;
            }
            if (len <= ptr) {
                break;
            }
            c = this.read();
        }
        if (ptr >= len) {
            eol = false;
            while (!eol) {
                switch (c = this.read()) {
                    case -1:
                    case 10: {
                        eol = true;
                        continue;
                    }
                    case 13: {
                        eol = true;
                        final long cur = this.getFilePointer();
                        if (this.read() != 10) {
                            this.seek(cur);
                            continue;
                        }
                        continue;
                    }
                }
            }
        }
        if (c == -1 && ptr == 0) {
            return false;
        }
        if (ptr + 2 <= len) {
            input[ptr++] = 32;
            input[ptr] = 88;
        }
        return true;
    }
    
    public static long[] checkObjectStart(final byte[] line) {
        try {
            final PRTokeniser tk = new PRTokeniser(new RandomAccessFileOrArray(new RandomAccessSourceFactory().createSource(line)));
            int num = 0;
            int gen = 0;
            if (!tk.nextToken() || tk.getTokenType() != TokenType.NUMBER) {
                return null;
            }
            num = tk.intValue();
            if (!tk.nextToken() || tk.getTokenType() != TokenType.NUMBER) {
                return null;
            }
            gen = tk.intValue();
            if (!tk.nextToken()) {
                return null;
            }
            if (!tk.getStringValue().equals("obj")) {
                return null;
            }
            return new long[] { num, gen };
        }
        catch (Exception ex) {
            return null;
        }
    }
    
    public boolean isHexString() {
        return this.hexString;
    }
    
    static {
        delims = new boolean[] { true, true, false, false, false, false, false, false, false, false, true, true, false, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, true, false, false, true, true, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
    }
    
    public enum TokenType
    {
        NUMBER, 
        STRING, 
        NAME, 
        COMMENT, 
        START_ARRAY, 
        END_ARRAY, 
        START_DIC, 
        END_DIC, 
        REF, 
        OTHER, 
        ENDOFFILE;
    }
}
