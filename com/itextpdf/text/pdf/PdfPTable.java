// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.pdf.events.PdfPTableEventForwarder;
import com.itextpdf.text.Element;
import com.itextpdf.text.ElementListener;
import com.itextpdf.text.Chunk;
import java.util.List;
import com.itextpdf.text.log.Level;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.DocumentException;
import java.util.Map;
import java.util.Iterator;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.log.LoggerFactory;
import com.itextpdf.text.AccessibleElementId;
import java.util.HashMap;
import java.util.ArrayList;
import com.itextpdf.text.log.Logger;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;
import com.itextpdf.text.api.Spaceable;
import com.itextpdf.text.LargeElement;

public class PdfPTable implements LargeElement, Spaceable, IAccessibleElement
{
    private final Logger LOGGER;
    public static final int BASECANVAS = 0;
    public static final int BACKGROUNDCANVAS = 1;
    public static final int LINECANVAS = 2;
    public static final int TEXTCANVAS = 3;
    protected ArrayList<PdfPRow> rows;
    protected float totalHeight;
    protected PdfPCell[] currentRow;
    protected int currentColIdx;
    protected PdfPCell defaultCell;
    protected float totalWidth;
    protected float[] relativeWidths;
    protected float[] absoluteWidths;
    protected PdfPTableEvent tableEvent;
    protected int headerRows;
    protected float widthPercentage;
    private int horizontalAlignment;
    private boolean skipFirstHeader;
    private boolean skipLastFooter;
    protected boolean isColspan;
    protected int runDirection;
    private boolean lockedWidth;
    private boolean splitRows;
    protected float spacingBefore;
    protected float spacingAfter;
    protected float paddingTop;
    private boolean[] extendLastRow;
    private boolean headersInEvent;
    private boolean splitLate;
    private boolean keepTogether;
    protected boolean complete;
    private int footerRows;
    protected boolean rowCompleted;
    protected boolean loopCheck;
    protected boolean rowsNotChecked;
    protected PdfName role;
    protected HashMap<PdfName, PdfObject> accessibleAttributes;
    protected AccessibleElementId id;
    private PdfPTableHeader header;
    private PdfPTableBody body;
    private PdfPTableFooter footer;
    private int numberOfWrittenRows;
    static final /* synthetic */ boolean $assertionsDisabled;
    
    protected PdfPTable() {
        this.LOGGER = LoggerFactory.getLogger(PdfPTable.class);
        this.rows = new ArrayList<PdfPRow>();
        this.totalHeight = 0.0f;
        this.currentColIdx = 0;
        this.defaultCell = new PdfPCell((Phrase)null);
        this.totalWidth = 0.0f;
        this.widthPercentage = 80.0f;
        this.horizontalAlignment = 1;
        this.skipFirstHeader = false;
        this.skipLastFooter = false;
        this.isColspan = false;
        this.runDirection = 1;
        this.lockedWidth = false;
        this.splitRows = true;
        this.extendLastRow = new boolean[] { false, false };
        this.splitLate = true;
        this.complete = true;
        this.rowCompleted = true;
        this.loopCheck = true;
        this.rowsNotChecked = true;
        this.role = PdfName.TABLE;
        this.accessibleAttributes = null;
        this.id = new AccessibleElementId();
        this.header = null;
        this.body = null;
        this.footer = null;
    }
    
    public PdfPTable(final float[] relativeWidths) {
        this.LOGGER = LoggerFactory.getLogger(PdfPTable.class);
        this.rows = new ArrayList<PdfPRow>();
        this.totalHeight = 0.0f;
        this.currentColIdx = 0;
        this.defaultCell = new PdfPCell((Phrase)null);
        this.totalWidth = 0.0f;
        this.widthPercentage = 80.0f;
        this.horizontalAlignment = 1;
        this.skipFirstHeader = false;
        this.skipLastFooter = false;
        this.isColspan = false;
        this.runDirection = 1;
        this.lockedWidth = false;
        this.splitRows = true;
        this.extendLastRow = new boolean[] { false, false };
        this.splitLate = true;
        this.complete = true;
        this.rowCompleted = true;
        this.loopCheck = true;
        this.rowsNotChecked = true;
        this.role = PdfName.TABLE;
        this.accessibleAttributes = null;
        this.id = new AccessibleElementId();
        this.header = null;
        this.body = null;
        this.footer = null;
        if (relativeWidths == null) {
            throw new NullPointerException(MessageLocalization.getComposedMessage("the.widths.array.in.pdfptable.constructor.can.not.be.null", new Object[0]));
        }
        if (relativeWidths.length == 0) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.widths.array.in.pdfptable.constructor.can.not.have.zero.length", new Object[0]));
        }
        System.arraycopy(relativeWidths, 0, this.relativeWidths = new float[relativeWidths.length], 0, relativeWidths.length);
        this.absoluteWidths = new float[relativeWidths.length];
        this.calculateWidths();
        this.currentRow = new PdfPCell[this.absoluteWidths.length];
        this.keepTogether = false;
    }
    
    public PdfPTable(final int numColumns) {
        this.LOGGER = LoggerFactory.getLogger(PdfPTable.class);
        this.rows = new ArrayList<PdfPRow>();
        this.totalHeight = 0.0f;
        this.currentColIdx = 0;
        this.defaultCell = new PdfPCell((Phrase)null);
        this.totalWidth = 0.0f;
        this.widthPercentage = 80.0f;
        this.horizontalAlignment = 1;
        this.skipFirstHeader = false;
        this.skipLastFooter = false;
        this.isColspan = false;
        this.runDirection = 1;
        this.lockedWidth = false;
        this.splitRows = true;
        this.extendLastRow = new boolean[] { false, false };
        this.splitLate = true;
        this.complete = true;
        this.rowCompleted = true;
        this.loopCheck = true;
        this.rowsNotChecked = true;
        this.role = PdfName.TABLE;
        this.accessibleAttributes = null;
        this.id = new AccessibleElementId();
        this.header = null;
        this.body = null;
        this.footer = null;
        if (numColumns <= 0) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.number.of.columns.in.pdfptable.constructor.must.be.greater.than.zero", new Object[0]));
        }
        this.relativeWidths = new float[numColumns];
        for (int k = 0; k < numColumns; ++k) {
            this.relativeWidths[k] = 1.0f;
        }
        this.absoluteWidths = new float[this.relativeWidths.length];
        this.calculateWidths();
        this.currentRow = new PdfPCell[this.absoluteWidths.length];
        this.keepTogether = false;
    }
    
    public PdfPTable(final PdfPTable table) {
        this.LOGGER = LoggerFactory.getLogger(PdfPTable.class);
        this.rows = new ArrayList<PdfPRow>();
        this.totalHeight = 0.0f;
        this.currentColIdx = 0;
        this.defaultCell = new PdfPCell((Phrase)null);
        this.totalWidth = 0.0f;
        this.widthPercentage = 80.0f;
        this.horizontalAlignment = 1;
        this.skipFirstHeader = false;
        this.skipLastFooter = false;
        this.isColspan = false;
        this.runDirection = 1;
        this.lockedWidth = false;
        this.splitRows = true;
        this.extendLastRow = new boolean[] { false, false };
        this.splitLate = true;
        this.complete = true;
        this.rowCompleted = true;
        this.loopCheck = true;
        this.rowsNotChecked = true;
        this.role = PdfName.TABLE;
        this.accessibleAttributes = null;
        this.id = new AccessibleElementId();
        this.header = null;
        this.body = null;
        this.footer = null;
        this.copyFormat(table);
        for (int k = 0; k < this.currentRow.length && table.currentRow[k] != null; ++k) {
            this.currentRow[k] = new PdfPCell(table.currentRow[k]);
        }
        for (int k = 0; k < table.rows.size(); ++k) {
            PdfPRow row = table.rows.get(k);
            if (row != null) {
                row = new PdfPRow(row);
            }
            this.rows.add(row);
        }
    }
    
    public void init() {
        this.LOGGER.info("Initialize row and cell heights");
        for (final PdfPRow row : this.getRows()) {
            if (row == null) {
                continue;
            }
            row.calculated = false;
            for (final PdfPCell cell : row.getCells()) {
                if (cell != null) {
                    cell.setCalculatedHeight(0.0f);
                }
            }
        }
    }
    
    public static PdfPTable shallowCopy(final PdfPTable table) {
        final PdfPTable nt = new PdfPTable();
        nt.copyFormat(table);
        return nt;
    }
    
    protected void copyFormat(final PdfPTable sourceTable) {
        this.rowsNotChecked = sourceTable.rowsNotChecked;
        this.relativeWidths = new float[sourceTable.getNumberOfColumns()];
        this.absoluteWidths = new float[sourceTable.getNumberOfColumns()];
        System.arraycopy(sourceTable.relativeWidths, 0, this.relativeWidths, 0, this.getNumberOfColumns());
        System.arraycopy(sourceTable.absoluteWidths, 0, this.absoluteWidths, 0, this.getNumberOfColumns());
        this.totalWidth = sourceTable.totalWidth;
        this.totalHeight = sourceTable.totalHeight;
        this.currentColIdx = 0;
        this.tableEvent = sourceTable.tableEvent;
        this.runDirection = sourceTable.runDirection;
        if (sourceTable.defaultCell instanceof PdfPHeaderCell) {
            this.defaultCell = new PdfPHeaderCell((PdfPHeaderCell)sourceTable.defaultCell);
        }
        else {
            this.defaultCell = new PdfPCell(sourceTable.defaultCell);
        }
        this.currentRow = new PdfPCell[sourceTable.currentRow.length];
        this.isColspan = sourceTable.isColspan;
        this.splitRows = sourceTable.splitRows;
        this.spacingAfter = sourceTable.spacingAfter;
        this.spacingBefore = sourceTable.spacingBefore;
        this.headerRows = sourceTable.headerRows;
        this.footerRows = sourceTable.footerRows;
        this.lockedWidth = sourceTable.lockedWidth;
        this.extendLastRow = sourceTable.extendLastRow;
        this.headersInEvent = sourceTable.headersInEvent;
        this.widthPercentage = sourceTable.widthPercentage;
        this.splitLate = sourceTable.splitLate;
        this.skipFirstHeader = sourceTable.skipFirstHeader;
        this.skipLastFooter = sourceTable.skipLastFooter;
        this.horizontalAlignment = sourceTable.horizontalAlignment;
        this.keepTogether = sourceTable.keepTogether;
        this.complete = sourceTable.complete;
        this.loopCheck = sourceTable.loopCheck;
        this.id = sourceTable.id;
        this.role = sourceTable.role;
        if (sourceTable.accessibleAttributes != null) {
            this.accessibleAttributes = new HashMap<PdfName, PdfObject>(sourceTable.accessibleAttributes);
        }
        this.header = sourceTable.getHeader();
        this.body = sourceTable.getBody();
        this.footer = sourceTable.getFooter();
    }
    
    public void setWidths(final float[] relativeWidths) throws DocumentException {
        if (relativeWidths.length != this.getNumberOfColumns()) {
            throw new DocumentException(MessageLocalization.getComposedMessage("wrong.number.of.columns", new Object[0]));
        }
        System.arraycopy(relativeWidths, 0, this.relativeWidths = new float[relativeWidths.length], 0, relativeWidths.length);
        this.absoluteWidths = new float[relativeWidths.length];
        this.totalHeight = 0.0f;
        this.calculateWidths();
        this.calculateHeights();
    }
    
    public void setWidths(final int[] relativeWidths) throws DocumentException {
        final float[] tb = new float[relativeWidths.length];
        for (int k = 0; k < relativeWidths.length; ++k) {
            tb[k] = (float)relativeWidths[k];
        }
        this.setWidths(tb);
    }
    
    protected void calculateWidths() {
        if (this.totalWidth <= 0.0f) {
            return;
        }
        float total = 0.0f;
        final int numCols = this.getNumberOfColumns();
        for (int k = 0; k < numCols; ++k) {
            total += this.relativeWidths[k];
        }
        for (int k = 0; k < numCols; ++k) {
            this.absoluteWidths[k] = this.totalWidth * this.relativeWidths[k] / total;
        }
    }
    
    public void setTotalWidth(final float totalWidth) {
        if (this.totalWidth == totalWidth) {
            return;
        }
        this.totalWidth = totalWidth;
        this.totalHeight = 0.0f;
        this.calculateWidths();
        this.calculateHeights();
    }
    
    public void setTotalWidth(final float[] columnWidth) throws DocumentException {
        if (columnWidth.length != this.getNumberOfColumns()) {
            throw new DocumentException(MessageLocalization.getComposedMessage("wrong.number.of.columns", new Object[0]));
        }
        this.totalWidth = 0.0f;
        for (int k = 0; k < columnWidth.length; ++k) {
            this.totalWidth += columnWidth[k];
        }
        this.setWidths(columnWidth);
    }
    
    public void setWidthPercentage(final float[] columnWidth, final Rectangle pageSize) throws DocumentException {
        if (columnWidth.length != this.getNumberOfColumns()) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("wrong.number.of.columns", new Object[0]));
        }
        this.setTotalWidth(columnWidth);
        this.widthPercentage = this.totalWidth / (pageSize.getRight() - pageSize.getLeft()) * 100.0f;
    }
    
    public float getTotalWidth() {
        return this.totalWidth;
    }
    
    public float calculateHeights() {
        if (this.totalWidth <= 0.0f) {
            return 0.0f;
        }
        this.totalHeight = 0.0f;
        for (int k = 0; k < this.rows.size(); ++k) {
            this.totalHeight += this.getRowHeight(k, true);
        }
        return this.totalHeight;
    }
    
    public void resetColumnCount(final int newColCount) {
        if (newColCount <= 0) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.number.of.columns.in.pdfptable.constructor.must.be.greater.than.zero", new Object[0]));
        }
        this.relativeWidths = new float[newColCount];
        for (int k = 0; k < newColCount; ++k) {
            this.relativeWidths[k] = 1.0f;
        }
        this.absoluteWidths = new float[this.relativeWidths.length];
        this.calculateWidths();
        this.currentRow = new PdfPCell[this.absoluteWidths.length];
        this.totalHeight = 0.0f;
    }
    
    public PdfPCell getDefaultCell() {
        return this.defaultCell;
    }
    
    public PdfPCell addCell(final PdfPCell cell) {
        this.rowCompleted = false;
        PdfPCell ncell;
        if (cell instanceof PdfPHeaderCell) {
            ncell = new PdfPHeaderCell((PdfPHeaderCell)cell);
        }
        else {
            ncell = new PdfPCell(cell);
        }
        int colspan = ncell.getColspan();
        colspan = Math.max(colspan, 1);
        colspan = Math.min(colspan, this.currentRow.length - this.currentColIdx);
        ncell.setColspan(colspan);
        if (colspan != 1) {
            this.isColspan = true;
        }
        final int rdir = ncell.getRunDirection();
        if (rdir == 1) {
            ncell.setRunDirection(this.runDirection);
        }
        this.skipColsWithRowspanAbove();
        boolean cellAdded = false;
        if (this.currentColIdx < this.currentRow.length) {
            this.currentRow[this.currentColIdx] = ncell;
            this.currentColIdx += colspan;
            cellAdded = true;
        }
        this.skipColsWithRowspanAbove();
        while (this.currentColIdx >= this.currentRow.length) {
            final int numCols = this.getNumberOfColumns();
            if (this.runDirection == 3) {
                final PdfPCell[] rtlRow = new PdfPCell[numCols];
                int rev = this.currentRow.length;
                int cspan;
                for (int k = 0; k < this.currentRow.length; k += cspan - 1, ++k) {
                    final PdfPCell rcell = this.currentRow[k];
                    cspan = rcell.getColspan();
                    rev -= cspan;
                    rtlRow[rev] = rcell;
                }
                this.currentRow = rtlRow;
            }
            final PdfPRow row = new PdfPRow(this.currentRow);
            if (this.totalWidth > 0.0f) {
                row.setWidths(this.absoluteWidths);
                this.totalHeight += row.getMaxHeights();
            }
            this.rows.add(row);
            this.currentRow = new PdfPCell[numCols];
            this.currentColIdx = 0;
            this.skipColsWithRowspanAbove();
            this.rowCompleted = true;
        }
        if (!cellAdded) {
            this.currentRow[this.currentColIdx] = ncell;
            this.currentColIdx += colspan;
        }
        return ncell;
    }
    
    private void skipColsWithRowspanAbove() {
        int direction = 1;
        if (this.runDirection == 3) {
            direction = -1;
        }
        while (this.rowSpanAbove(this.rows.size(), this.currentColIdx)) {
            this.currentColIdx += direction;
        }
    }
    
    PdfPCell cellAt(final int row, final int col) {
        final PdfPCell[] cells = this.rows.get(row).getCells();
        for (int i = 0; i < cells.length; ++i) {
            if (cells[i] != null && col >= i && col < i + cells[i].getColspan()) {
                return cells[i];
            }
        }
        return null;
    }
    
    boolean rowSpanAbove(final int currRow, final int currCol) {
        if (currCol >= this.getNumberOfColumns() || currCol < 0 || currRow < 1) {
            return false;
        }
        int row = currRow - 1;
        PdfPRow aboveRow = this.rows.get(row);
        if (aboveRow == null) {
            return false;
        }
        PdfPCell aboveCell;
        for (aboveCell = this.cellAt(row, currCol); aboveCell == null && row > 0; aboveCell = this.cellAt(row, currCol)) {
            aboveRow = this.rows.get(--row);
            if (aboveRow == null) {
                return false;
            }
        }
        int distance = currRow - row;
        if (aboveCell.getRowspan() == 1 && distance > 1) {
            int col = currCol - 1;
            aboveRow = this.rows.get(row + 1);
            --distance;
            for (aboveCell = aboveRow.getCells()[col]; aboveCell == null && col > 0; aboveCell = aboveRow.getCells()[--col]) {}
        }
        return aboveCell != null && aboveCell.getRowspan() > distance;
    }
    
    public void addCell(final String text) {
        this.addCell(new Phrase(text));
    }
    
    public void addCell(final PdfPTable table) {
        this.defaultCell.setTable(table);
        final PdfPCell newCell = this.addCell(this.defaultCell);
        newCell.id = new AccessibleElementId();
        this.defaultCell.setTable(null);
    }
    
    public void addCell(final Image image) {
        this.defaultCell.setImage(image);
        final PdfPCell newCell = this.addCell(this.defaultCell);
        newCell.id = new AccessibleElementId();
        this.defaultCell.setImage(null);
    }
    
    public void addCell(final Phrase phrase) {
        this.defaultCell.setPhrase(phrase);
        final PdfPCell newCell = this.addCell(this.defaultCell);
        newCell.id = new AccessibleElementId();
        this.defaultCell.setPhrase(null);
    }
    
    public float writeSelectedRows(final int rowStart, final int rowEnd, final float xPos, final float yPos, final PdfContentByte[] canvases) {
        return this.writeSelectedRows(0, -1, rowStart, rowEnd, xPos, yPos, canvases);
    }
    
    public float writeSelectedRows(final int colStart, final int colEnd, final int rowStart, final int rowEnd, final float xPos, final float yPos, final PdfContentByte[] canvases) {
        return this.writeSelectedRows(colStart, colEnd, rowStart, rowEnd, xPos, yPos, canvases, true);
    }
    
    public float writeSelectedRows(int colStart, int colEnd, int rowStart, int rowEnd, final float xPos, float yPos, final PdfContentByte[] canvases, final boolean reusable) {
        if (this.totalWidth <= 0.0f) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("the.table.width.must.be.greater.than.zero", new Object[0]));
        }
        final int totalRows = this.rows.size();
        if (rowStart < 0) {
            rowStart = 0;
        }
        if (rowEnd < 0) {
            rowEnd = totalRows;
        }
        else {
            rowEnd = Math.min(rowEnd, totalRows);
        }
        if (rowStart >= rowEnd) {
            return yPos;
        }
        final int totalCols = this.getNumberOfColumns();
        if (colStart < 0) {
            colStart = 0;
        }
        else {
            colStart = Math.min(colStart, totalCols);
        }
        if (colEnd < 0) {
            colEnd = totalCols;
        }
        else {
            colEnd = Math.min(colEnd, totalCols);
        }
        if (this.LOGGER.isLogging(Level.INFO)) {
            this.LOGGER.info(String.format("Writing row %s to %s; column %s to %s", rowStart, rowEnd, colStart, colEnd));
        }
        final float yPosStart = yPos;
        PdfPTableBody currentBlock = null;
        if (this.rowsNotChecked) {
            this.getFittingRows(Float.MAX_VALUE, rowStart);
        }
        final List<PdfPRow> rows = this.getRows(rowStart, rowEnd);
        int k = rowStart;
        for (final PdfPRow row : rows) {
            if (this.getHeader().rows != null && this.getHeader().rows.contains(row) && currentBlock == null) {
                currentBlock = this.openTableBlock(this.getHeader(), canvases[3]);
            }
            else if (this.getBody().rows != null && this.getBody().rows.contains(row) && currentBlock == null) {
                currentBlock = this.openTableBlock(this.getBody(), canvases[3]);
            }
            else if (this.getFooter().rows != null && this.getFooter().rows.contains(row) && currentBlock == null) {
                currentBlock = this.openTableBlock(this.getFooter(), canvases[3]);
            }
            if (row != null) {
                row.writeCells(colStart, colEnd, xPos, yPos, canvases, reusable);
                yPos -= row.getMaxHeights();
            }
            if (this.getHeader().rows != null && this.getHeader().rows.contains(row) && (k == rowEnd - 1 || !this.getHeader().rows.contains(rows.get(k + 1)))) {
                currentBlock = this.closeTableBlock(this.getHeader(), canvases[3]);
            }
            else if (this.getBody().rows != null && this.getBody().rows.contains(row) && (k == rowEnd - 1 || !this.getBody().rows.contains(rows.get(k + 1)))) {
                currentBlock = this.closeTableBlock(this.getBody(), canvases[3]);
            }
            else if (this.getFooter().rows != null && this.getFooter().rows.contains(row) && (k == rowEnd - 1 || !this.getFooter().rows.contains(rows.get(k + 1)))) {
                currentBlock = this.closeTableBlock(this.getFooter(), canvases[3]);
            }
            ++k;
        }
        if (this.tableEvent != null && colStart == 0 && colEnd == totalCols) {
            final float[] heights = new float[rowEnd - rowStart + 1];
            heights[0] = yPosStart;
            for (k = 0; k < rowEnd - rowStart; ++k) {
                final PdfPRow row = rows.get(k);
                float hr = 0.0f;
                if (row != null) {
                    hr = row.getMaxHeights();
                }
                heights[k + 1] = heights[k] - hr;
            }
            this.tableEvent.tableLayout(this, this.getEventWidths(xPos, rowStart, rowEnd, this.headersInEvent), heights, this.headersInEvent ? this.headerRows : 0, rowStart, canvases);
        }
        return yPos;
    }
    
    private PdfPTableBody openTableBlock(final PdfPTableBody block, final PdfContentByte canvas) {
        if (canvas.writer.getStandardStructElems().contains(block.getRole())) {
            canvas.openMCBlock(block);
            return block;
        }
        return null;
    }
    
    private PdfPTableBody closeTableBlock(final PdfPTableBody block, final PdfContentByte canvas) {
        if (canvas.writer.getStandardStructElems().contains(block.getRole())) {
            canvas.closeMCBlock(block);
        }
        return null;
    }
    
    public float writeSelectedRows(final int rowStart, final int rowEnd, final float xPos, final float yPos, final PdfContentByte canvas) {
        return this.writeSelectedRows(0, -1, rowStart, rowEnd, xPos, yPos, canvas);
    }
    
    public float writeSelectedRows(final int colStart, final int colEnd, final int rowStart, final int rowEnd, final float xPos, final float yPos, final PdfContentByte canvas) {
        return this.writeSelectedRows(colStart, colEnd, rowStart, rowEnd, xPos, yPos, canvas, true);
    }
    
    public float writeSelectedRows(int colStart, int colEnd, final int rowStart, final int rowEnd, final float xPos, final float yPos, final PdfContentByte canvas, final boolean reusable) {
        final int totalCols = this.getNumberOfColumns();
        if (colStart < 0) {
            colStart = 0;
        }
        else {
            colStart = Math.min(colStart, totalCols);
        }
        if (colEnd < 0) {
            colEnd = totalCols;
        }
        else {
            colEnd = Math.min(colEnd, totalCols);
        }
        final boolean clip = colStart != 0 || colEnd != totalCols;
        if (clip) {
            float w = 0.0f;
            for (int k = colStart; k < colEnd; ++k) {
                w += this.absoluteWidths[k];
            }
            canvas.saveState();
            final float lx = (colStart == 0) ? 10000.0f : 0.0f;
            final float rx = (colEnd == totalCols) ? 10000.0f : 0.0f;
            canvas.rectangle(xPos - lx, -10000.0f, w + lx + rx, 20000.0f);
            canvas.clip();
            canvas.newPath();
        }
        final PdfContentByte[] canvases = beginWritingRows(canvas);
        final float y = this.writeSelectedRows(colStart, colEnd, rowStart, rowEnd, xPos, yPos, canvases, reusable);
        endWritingRows(canvases);
        if (clip) {
            canvas.restoreState();
        }
        return y;
    }
    
    public static PdfContentByte[] beginWritingRows(final PdfContentByte canvas) {
        return new PdfContentByte[] { canvas, canvas.getDuplicate(), canvas.getDuplicate(), canvas.getDuplicate() };
    }
    
    public static void endWritingRows(final PdfContentByte[] canvases) {
        final PdfContentByte canvas = canvases[0];
        final PdfArtifact artifact = new PdfArtifact();
        canvas.openMCBlock(artifact);
        canvas.saveState();
        canvas.add(canvases[1]);
        canvas.restoreState();
        canvas.saveState();
        canvas.setLineCap(2);
        canvas.resetRGBColorStroke();
        canvas.add(canvases[2]);
        canvas.restoreState();
        canvas.closeMCBlock(artifact);
        canvas.add(canvases[3]);
    }
    
    public int size() {
        return this.rows.size();
    }
    
    public float getTotalHeight() {
        return this.totalHeight;
    }
    
    public float getRowHeight(final int idx) {
        return this.getRowHeight(idx, false);
    }
    
    protected float getRowHeight(final int idx, final boolean firsttime) {
        if (this.totalWidth <= 0.0f || idx < 0 || idx >= this.rows.size()) {
            return 0.0f;
        }
        final PdfPRow row = this.rows.get(idx);
        if (row == null) {
            return 0.0f;
        }
        if (firsttime) {
            row.setWidths(this.absoluteWidths);
        }
        float height = row.getMaxHeights();
        for (int i = 0; i < this.relativeWidths.length; ++i) {
            if (this.rowSpanAbove(idx, i)) {
                int rs;
                for (rs = 1; this.rowSpanAbove(idx - rs, i); ++rs) {}
                final PdfPRow tmprow = this.rows.get(idx - rs);
                final PdfPCell cell = tmprow.getCells()[i];
                float tmp = 0.0f;
                if (cell != null && cell.getRowspan() == rs + 1) {
                    tmp = cell.getMaxHeight();
                    while (rs > 0) {
                        tmp -= this.getRowHeight(idx - rs);
                        --rs;
                    }
                }
                if (tmp > height) {
                    height = tmp;
                }
            }
        }
        row.setMaxHeights(height);
        return height;
    }
    
    public float getRowspanHeight(final int rowIndex, final int cellIndex) {
        if (this.totalWidth <= 0.0f || rowIndex < 0 || rowIndex >= this.rows.size()) {
            return 0.0f;
        }
        final PdfPRow row = this.rows.get(rowIndex);
        if (row == null || cellIndex >= row.getCells().length) {
            return 0.0f;
        }
        final PdfPCell cell = row.getCells()[cellIndex];
        if (cell == null) {
            return 0.0f;
        }
        float rowspanHeight = 0.0f;
        for (int j = 0; j < cell.getRowspan(); ++j) {
            rowspanHeight += this.getRowHeight(rowIndex + j);
        }
        return rowspanHeight;
    }
    
    public boolean hasRowspan(final int rowIdx) {
        if (rowIdx < this.rows.size() && this.getRow(rowIdx).hasRowspan()) {
            return true;
        }
        final PdfPRow previousRow = (rowIdx > 0) ? this.getRow(rowIdx - 1) : null;
        if (previousRow != null && previousRow.hasRowspan()) {
            return true;
        }
        for (int i = 0; i < this.getNumberOfColumns(); ++i) {
            if (this.rowSpanAbove(rowIdx - 1, i)) {
                return true;
            }
        }
        return false;
    }
    
    public void normalizeHeadersFooters() {
        if (this.footerRows > this.headerRows) {
            this.footerRows = this.headerRows;
        }
    }
    
    public float getHeaderHeight() {
        float total = 0.0f;
        for (int size = Math.min(this.rows.size(), this.headerRows), k = 0; k < size; ++k) {
            final PdfPRow row = this.rows.get(k);
            if (row != null) {
                total += row.getMaxHeights();
            }
        }
        return total;
    }
    
    public float getFooterHeight() {
        float total = 0.0f;
        final int start = Math.max(0, this.headerRows - this.footerRows);
        for (int size = Math.min(this.rows.size(), this.headerRows), k = start; k < size; ++k) {
            final PdfPRow row = this.rows.get(k);
            if (row != null) {
                total += row.getMaxHeights();
            }
        }
        return total;
    }
    
    public boolean deleteRow(final int rowNumber) {
        if (rowNumber < 0 || rowNumber >= this.rows.size()) {
            return false;
        }
        if (this.totalWidth > 0.0f) {
            final PdfPRow row = this.rows.get(rowNumber);
            if (row != null) {
                this.totalHeight -= row.getMaxHeights();
            }
        }
        this.rows.remove(rowNumber);
        if (rowNumber < this.headerRows) {
            --this.headerRows;
            if (rowNumber >= this.headerRows - this.footerRows) {
                --this.footerRows;
            }
        }
        return true;
    }
    
    public boolean deleteLastRow() {
        return this.deleteRow(this.rows.size() - 1);
    }
    
    public void deleteBodyRows() {
        final ArrayList<PdfPRow> rows2 = new ArrayList<PdfPRow>();
        for (int k = 0; k < this.headerRows; ++k) {
            rows2.add(this.rows.get(k));
        }
        this.rows = rows2;
        this.totalHeight = 0.0f;
        if (this.totalWidth > 0.0f) {
            this.totalHeight = this.getHeaderHeight();
        }
    }
    
    public int getNumberOfColumns() {
        return this.relativeWidths.length;
    }
    
    public int getHeaderRows() {
        return this.headerRows;
    }
    
    public void setHeaderRows(int headerRows) {
        if (headerRows < 0) {
            headerRows = 0;
        }
        this.headerRows = headerRows;
    }
    
    @Override
    public List<Chunk> getChunks() {
        return new ArrayList<Chunk>();
    }
    
    @Override
    public int type() {
        return 23;
    }
    
    @Override
    public boolean isContent() {
        return true;
    }
    
    @Override
    public boolean isNestable() {
        return true;
    }
    
    @Override
    public boolean process(final ElementListener listener) {
        try {
            return listener.add(this);
        }
        catch (DocumentException de) {
            return false;
        }
    }
    
    public String getSummary() {
        return this.getAccessibleAttribute(PdfName.SUMMARY).toString();
    }
    
    public void setSummary(final String summary) {
        this.setAccessibleAttribute(PdfName.SUMMARY, new PdfString(summary));
    }
    
    public float getWidthPercentage() {
        return this.widthPercentage;
    }
    
    public void setWidthPercentage(final float widthPercentage) {
        this.widthPercentage = widthPercentage;
    }
    
    public int getHorizontalAlignment() {
        return this.horizontalAlignment;
    }
    
    public void setHorizontalAlignment(final int horizontalAlignment) {
        this.horizontalAlignment = horizontalAlignment;
    }
    
    public PdfPRow getRow(final int idx) {
        return this.rows.get(idx);
    }
    
    public ArrayList<PdfPRow> getRows() {
        return this.rows;
    }
    
    public int getLastCompletedRowIndex() {
        return this.rows.size() - 1;
    }
    
    public void setBreakPoints(final int... breakPoints) {
        this.keepRowsTogether(0, this.rows.size());
        for (int i = 0; i < breakPoints.length; ++i) {
            this.getRow(breakPoints[i]).setMayNotBreak(false);
        }
    }
    
    public void keepRowsTogether(final int[] rows) {
        for (int i = 0; i < rows.length; ++i) {
            this.getRow(rows[i]).setMayNotBreak(true);
        }
    }
    
    public void keepRowsTogether(int start, final int end) {
        if (start < end) {
            while (start < end) {
                this.getRow(start).setMayNotBreak(true);
                ++start;
            }
        }
    }
    
    public void keepRowsTogether(final int start) {
        this.keepRowsTogether(start, this.rows.size());
    }
    
    public ArrayList<PdfPRow> getRows(final int start, final int end) {
        final ArrayList<PdfPRow> list = new ArrayList<PdfPRow>();
        if (start < 0 || end > this.size()) {
            return list;
        }
        for (int i = start; i < end; ++i) {
            list.add(this.adjustCellsInRow(i, end));
        }
        return list;
    }
    
    protected PdfPRow adjustCellsInRow(final int start, final int end) {
        PdfPRow row = this.getRow(start);
        if (row.isAdjusted()) {
            return row;
        }
        row = new PdfPRow(row);
        final PdfPCell[] cells = row.getCells();
        for (int i = 0; i < cells.length; ++i) {
            final PdfPCell cell = cells[i];
            if (cell != null) {
                if (cell.getRowspan() != 1) {
                    final int stop = Math.min(end, start + cell.getRowspan());
                    float extra = 0.0f;
                    for (int k = start + 1; k < stop; ++k) {
                        extra += this.getRow(k).getMaxHeights();
                    }
                    row.setExtraHeight(i, extra);
                }
            }
        }
        row.setAdjusted(true);
        return row;
    }
    
    public void setTableEvent(final PdfPTableEvent event) {
        if (event == null) {
            this.tableEvent = null;
        }
        else if (this.tableEvent == null) {
            this.tableEvent = event;
        }
        else if (this.tableEvent instanceof PdfPTableEventForwarder) {
            ((PdfPTableEventForwarder)this.tableEvent).addTableEvent(event);
        }
        else {
            final PdfPTableEventForwarder forward = new PdfPTableEventForwarder();
            forward.addTableEvent(this.tableEvent);
            forward.addTableEvent(event);
            this.tableEvent = forward;
        }
    }
    
    public PdfPTableEvent getTableEvent() {
        return this.tableEvent;
    }
    
    public float[] getAbsoluteWidths() {
        return this.absoluteWidths;
    }
    
    float[][] getEventWidths(final float xPos, int firstRow, int lastRow, final boolean includeHeaders) {
        if (includeHeaders) {
            firstRow = Math.max(firstRow, this.headerRows);
            lastRow = Math.max(lastRow, this.headerRows);
        }
        final float[][] widths = new float[(includeHeaders ? this.headerRows : 0) + lastRow - firstRow][];
        if (this.isColspan) {
            int n = 0;
            if (includeHeaders) {
                for (int k = 0; k < this.headerRows; ++k) {
                    final PdfPRow row = this.rows.get(k);
                    if (row == null) {
                        ++n;
                    }
                    else {
                        widths[n++] = row.getEventWidth(xPos, this.absoluteWidths);
                    }
                }
            }
            while (firstRow < lastRow) {
                final PdfPRow row2 = this.rows.get(firstRow);
                if (row2 == null) {
                    ++n;
                }
                else {
                    widths[n++] = row2.getEventWidth(xPos, this.absoluteWidths);
                }
                ++firstRow;
            }
        }
        else {
            final int numCols = this.getNumberOfColumns();
            final float[] width = new float[numCols + 1];
            width[0] = xPos;
            for (int i = 0; i < numCols; ++i) {
                width[i + 1] = width[i] + this.absoluteWidths[i];
            }
            for (int i = 0; i < widths.length; ++i) {
                widths[i] = width;
            }
        }
        return widths;
    }
    
    public boolean isSkipFirstHeader() {
        return this.skipFirstHeader;
    }
    
    public boolean isSkipLastFooter() {
        return this.skipLastFooter;
    }
    
    public void setSkipFirstHeader(final boolean skipFirstHeader) {
        this.skipFirstHeader = skipFirstHeader;
    }
    
    public void setSkipLastFooter(final boolean skipLastFooter) {
        this.skipLastFooter = skipLastFooter;
    }
    
    public void setRunDirection(final int runDirection) {
        switch (runDirection) {
            case 0:
            case 1:
            case 2:
            case 3: {
                this.runDirection = runDirection;
            }
            default: {
                throw new RuntimeException(MessageLocalization.getComposedMessage("invalid.run.direction.1", runDirection));
            }
        }
    }
    
    public int getRunDirection() {
        return this.runDirection;
    }
    
    public boolean isLockedWidth() {
        return this.lockedWidth;
    }
    
    public void setLockedWidth(final boolean lockedWidth) {
        this.lockedWidth = lockedWidth;
    }
    
    public boolean isSplitRows() {
        return this.splitRows;
    }
    
    public void setSplitRows(final boolean splitRows) {
        this.splitRows = splitRows;
    }
    
    @Override
    public void setSpacingBefore(final float spacing) {
        this.spacingBefore = spacing;
    }
    
    @Override
    public void setSpacingAfter(final float spacing) {
        this.spacingAfter = spacing;
    }
    
    public float spacingBefore() {
        return this.spacingBefore;
    }
    
    public float spacingAfter() {
        return this.spacingAfter;
    }
    
    @Override
    public float getPaddingTop() {
        return this.paddingTop;
    }
    
    @Override
    public void setPaddingTop(final float paddingTop) {
        this.paddingTop = paddingTop;
    }
    
    public boolean isExtendLastRow() {
        return this.extendLastRow[0];
    }
    
    public void setExtendLastRow(final boolean extendLastRows) {
        this.extendLastRow[0] = extendLastRows;
        this.extendLastRow[1] = extendLastRows;
    }
    
    public void setExtendLastRow(final boolean extendLastRows, final boolean extendFinalRow) {
        this.extendLastRow[0] = extendLastRows;
        this.extendLastRow[1] = extendFinalRow;
    }
    
    public boolean isExtendLastRow(final boolean newPageFollows) {
        if (newPageFollows) {
            return this.extendLastRow[0];
        }
        return this.extendLastRow[1];
    }
    
    public boolean isHeadersInEvent() {
        return this.headersInEvent;
    }
    
    public void setHeadersInEvent(final boolean headersInEvent) {
        this.headersInEvent = headersInEvent;
    }
    
    public boolean isSplitLate() {
        return this.splitLate;
    }
    
    public void setSplitLate(final boolean splitLate) {
        this.splitLate = splitLate;
    }
    
    public void setKeepTogether(final boolean keepTogether) {
        this.keepTogether = keepTogether;
    }
    
    public boolean getKeepTogether() {
        return this.keepTogether;
    }
    
    public int getFooterRows() {
        return this.footerRows;
    }
    
    public void setFooterRows(int footerRows) {
        if (footerRows < 0) {
            footerRows = 0;
        }
        this.footerRows = footerRows;
    }
    
    public void completeRow() {
        while (!this.rowCompleted) {
            this.addCell(this.defaultCell);
        }
    }
    
    @Override
    public void flushContent() {
        this.deleteBodyRows();
        if (this.numberOfWrittenRows > 0) {
            this.setSkipFirstHeader(true);
        }
    }
    
    void addNumberOfRowsWritten(final int numberOfWrittenRows) {
        this.numberOfWrittenRows += numberOfWrittenRows;
    }
    
    @Override
    public boolean isComplete() {
        return this.complete;
    }
    
    @Override
    public void setComplete(final boolean complete) {
        this.complete = complete;
    }
    
    @Override
    public float getSpacingBefore() {
        return this.spacingBefore;
    }
    
    @Override
    public float getSpacingAfter() {
        return this.spacingAfter;
    }
    
    public boolean isLoopCheck() {
        return this.loopCheck;
    }
    
    public void setLoopCheck(final boolean loopCheck) {
        this.loopCheck = loopCheck;
    }
    
    @Override
    public PdfObject getAccessibleAttribute(final PdfName key) {
        if (this.accessibleAttributes != null) {
            return this.accessibleAttributes.get(key);
        }
        return null;
    }
    
    @Override
    public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
        if (this.accessibleAttributes == null) {
            this.accessibleAttributes = new HashMap<PdfName, PdfObject>();
        }
        this.accessibleAttributes.put(key, value);
    }
    
    @Override
    public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
        return this.accessibleAttributes;
    }
    
    @Override
    public PdfName getRole() {
        return this.role;
    }
    
    @Override
    public void setRole(final PdfName role) {
        this.role = role;
    }
    
    @Override
    public AccessibleElementId getId() {
        return this.id;
    }
    
    @Override
    public void setId(final AccessibleElementId id) {
        this.id = id;
    }
    
    @Override
    public boolean isInline() {
        return false;
    }
    
    public PdfPTableHeader getHeader() {
        if (this.header == null) {
            this.header = new PdfPTableHeader();
        }
        return this.header;
    }
    
    public PdfPTableBody getBody() {
        if (this.body == null) {
            this.body = new PdfPTableBody();
        }
        return this.body;
    }
    
    public PdfPTableFooter getFooter() {
        if (this.footer == null) {
            this.footer = new PdfPTableFooter();
        }
        return this.footer;
    }
    
    public int getCellStartRowIndex(final int rowIdx, final int colIdx) {
        int lastRow;
        for (lastRow = rowIdx; this.getRow(lastRow).getCells()[colIdx] == null && lastRow > 0; --lastRow) {}
        return lastRow;
    }
    
    public FittingRows getFittingRows(final float availableHeight, final int startIdx) {
        if (this.LOGGER.isLogging(Level.INFO)) {
            this.LOGGER.info(String.format("getFittingRows(%s, %s)", availableHeight, startIdx));
        }
        if (startIdx > 0 && startIdx < this.rows.size() && !PdfPTable.$assertionsDisabled && this.getRow(startIdx).getCells()[0] == null) {
            throw new AssertionError();
        }
        final int cols = this.getNumberOfColumns();
        final ColumnMeasurementState[] states = new ColumnMeasurementState[cols];
        for (int i = 0; i < cols; ++i) {
            states[i] = new ColumnMeasurementState();
        }
        float completedRowsHeight = 0.0f;
        float totalHeight = 0.0f;
        final Map<Integer, Float> correctedHeightsForLastRow = new HashMap<Integer, Float>();
        int k;
        for (k = startIdx; k < this.size(); ++k) {
            final PdfPRow row = this.getRow(k);
            final float rowHeight = row.getMaxRowHeightsWithoutCalculating();
            float maxCompletedRowsHeight = 0.0f;
            ColumnMeasurementState state;
            for (int j = 0; j < cols; j += state.colspan) {
                final PdfPCell cell = row.getCells()[j];
                state = states[j];
                if (cell == null) {
                    state.consumeRowspan(completedRowsHeight, rowHeight);
                }
                else {
                    state.beginCell(cell, completedRowsHeight, rowHeight);
                    if (this.LOGGER.isLogging(Level.INFO)) {
                        this.LOGGER.info(String.format("Height after beginCell: %s (cell: %s)", state.height, cell.getCachedMaxHeight()));
                    }
                }
                if (state.cellEnds() && state.height > maxCompletedRowsHeight) {
                    maxCompletedRowsHeight = state.height;
                }
                for (int l = 1; l < state.colspan; ++l) {
                    states[j + l].height = state.height;
                }
            }
            float maxTotalHeight = 0.0f;
            for (final ColumnMeasurementState state2 : states) {
                if (state2.height > maxTotalHeight) {
                    maxTotalHeight = state2.height;
                }
            }
            row.setFinalMaxHeights(maxCompletedRowsHeight - completedRowsHeight);
            final float remainingHeight = availableHeight - (this.isSplitLate() ? maxTotalHeight : maxCompletedRowsHeight);
            if (remainingHeight < 0.0f) {
                break;
            }
            correctedHeightsForLastRow.put(k, maxTotalHeight - completedRowsHeight);
            completedRowsHeight = maxCompletedRowsHeight;
            totalHeight = maxTotalHeight;
        }
        this.rowsNotChecked = false;
        return new FittingRows(startIdx, k - 1, totalHeight, completedRowsHeight, correctedHeightsForLastRow);
    }
    
    public static class FittingRows
    {
        public final int firstRow;
        public final int lastRow;
        public final float height;
        public final float completedRowsHeight;
        private final Map<Integer, Float> correctedHeightsForLastRow;
        
        public FittingRows(final int firstRow, final int lastRow, final float height, final float completedRowsHeight, final Map<Integer, Float> correctedHeightsForLastRow) {
            this.firstRow = firstRow;
            this.lastRow = lastRow;
            this.height = height;
            this.completedRowsHeight = completedRowsHeight;
            this.correctedHeightsForLastRow = correctedHeightsForLastRow;
        }
        
        public void correctLastRowChosen(final PdfPTable table, final int k) {
            final PdfPRow row = table.getRow(k);
            final Float value = this.correctedHeightsForLastRow.get(k);
            if (value != null) {
                row.setFinalMaxHeights(value);
            }
        }
    }
    
    public static class ColumnMeasurementState
    {
        public float height;
        public int rowspan;
        public int colspan;
        
        public ColumnMeasurementState() {
            this.height = 0.0f;
            this.rowspan = 1;
            this.colspan = 1;
        }
        
        public void beginCell(final PdfPCell cell, final float completedRowsHeight, final float rowHeight) {
            this.rowspan = cell.getRowspan();
            this.colspan = cell.getColspan();
            this.height = completedRowsHeight + Math.max(cell.hasCachedMaxHeight() ? cell.getCachedMaxHeight() : cell.getMaxHeight(), rowHeight);
        }
        
        public void consumeRowspan(final float completedRowsHeight, final float rowHeight) {
            --this.rowspan;
        }
        
        public boolean cellEnds() {
            return this.rowspan == 1;
        }
    }
}
