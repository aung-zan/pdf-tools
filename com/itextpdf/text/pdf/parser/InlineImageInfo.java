// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import com.itextpdf.text.pdf.PdfDictionary;

public class InlineImageInfo
{
    private final byte[] samples;
    private final PdfDictionary imageDictionary;
    
    public InlineImageInfo(final byte[] samples, final PdfDictionary imageDictionary) {
        this.samples = samples;
        this.imageDictionary = imageDictionary;
    }
    
    public PdfDictionary getImageDictionary() {
        return this.imageDictionary;
    }
    
    public byte[] getSamples() {
        return this.samples;
    }
}
