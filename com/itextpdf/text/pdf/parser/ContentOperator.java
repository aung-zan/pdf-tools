// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import com.itextpdf.text.pdf.PdfObject;
import java.util.ArrayList;
import com.itextpdf.text.pdf.PdfLiteral;

public interface ContentOperator
{
    void invoke(final PdfContentStreamProcessor p0, final PdfLiteral p1, final ArrayList<PdfObject> p2) throws Exception;
}
