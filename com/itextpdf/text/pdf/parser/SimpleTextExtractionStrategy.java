// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

public class SimpleTextExtractionStrategy implements TextExtractionStrategy
{
    private Vector lastStart;
    private Vector lastEnd;
    private final StringBuffer result;
    
    public SimpleTextExtractionStrategy() {
        this.result = new StringBuffer();
    }
    
    @Override
    public void beginTextBlock() {
    }
    
    @Override
    public void endTextBlock() {
    }
    
    @Override
    public String getResultantText() {
        return this.result.toString();
    }
    
    protected final void appendTextChunk(final CharSequence text) {
        this.result.append(text);
    }
    
    @Override
    public void renderText(final TextRenderInfo renderInfo) {
        final boolean firstRender = this.result.length() == 0;
        boolean hardReturn = false;
        final LineSegment segment = renderInfo.getBaseline();
        final Vector start = segment.getStartPoint();
        final Vector end = segment.getEndPoint();
        if (!firstRender) {
            final Vector x0 = start;
            final Vector x2 = this.lastStart;
            final Vector x3 = this.lastEnd;
            final float dist = x3.subtract(x2).cross(x2.subtract(x0)).lengthSquared() / x3.subtract(x2).lengthSquared();
            final float sameLineThreshold = 1.0f;
            if (dist > sameLineThreshold) {
                hardReturn = true;
            }
        }
        if (hardReturn) {
            this.appendTextChunk("\n");
        }
        else if (!firstRender && this.result.charAt(this.result.length() - 1) != ' ' && renderInfo.getText().length() > 0 && renderInfo.getText().charAt(0) != ' ') {
            final float spacing = this.lastEnd.subtract(start).length();
            if (spacing > renderInfo.getSingleSpaceWidth() / 2.0f) {
                this.appendTextChunk(" ");
            }
        }
        this.appendTextChunk(renderInfo.getText());
        this.lastStart = start;
        this.lastEnd = end;
    }
    
    @Override
    public void renderImage(final ImageRenderInfo renderInfo) {
    }
}
