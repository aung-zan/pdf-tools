// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

public class FilteredTextRenderListener extends FilteredRenderListener implements TextExtractionStrategy
{
    private final TextExtractionStrategy delegate;
    
    public FilteredTextRenderListener(final TextExtractionStrategy delegate, final RenderFilter... filters) {
        super(delegate, filters);
        this.delegate = delegate;
    }
    
    @Override
    public String getResultantText() {
        return this.delegate.getResultantText();
    }
}
