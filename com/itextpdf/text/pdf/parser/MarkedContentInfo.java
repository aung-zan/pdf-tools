// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;

public class MarkedContentInfo
{
    private final PdfName tag;
    private final PdfDictionary dictionary;
    
    public MarkedContentInfo(final PdfName tag, final PdfDictionary dictionary) {
        this.tag = tag;
        this.dictionary = ((dictionary != null) ? dictionary : new PdfDictionary());
    }
    
    public PdfName getTag() {
        return this.tag;
    }
    
    public boolean hasMcid() {
        return this.dictionary.contains(PdfName.MCID);
    }
    
    public int getMcid() {
        final PdfNumber id = this.dictionary.getAsNumber(PdfName.MCID);
        if (id == null) {
            throw new IllegalStateException("MarkedContentInfo does not contain MCID");
        }
        return id.intValue();
    }
}
