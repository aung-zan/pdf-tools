// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public class LocationTextExtractionStrategy implements TextExtractionStrategy
{
    static boolean DUMP_STATE;
    private final List<TextChunk> locationalResult;
    private final TextChunkLocationStrategy tclStrat;
    
    public LocationTextExtractionStrategy() {
        this(new TextChunkLocationStrategy() {
            @Override
            public TextChunkLocation createLocation(final TextRenderInfo renderInfo, final LineSegment baseline) {
                return new TextChunkLocationDefaultImp(baseline.getStartPoint(), baseline.getEndPoint(), renderInfo.getSingleSpaceWidth());
            }
        });
    }
    
    public LocationTextExtractionStrategy(final TextChunkLocationStrategy strat) {
        this.locationalResult = new ArrayList<TextChunk>();
        this.tclStrat = strat;
    }
    
    @Override
    public void beginTextBlock() {
    }
    
    @Override
    public void endTextBlock() {
    }
    
    private boolean startsWithSpace(final String str) {
        return str.length() != 0 && str.charAt(0) == ' ';
    }
    
    private boolean endsWithSpace(final String str) {
        return str.length() != 0 && str.charAt(str.length() - 1) == ' ';
    }
    
    private List<TextChunk> filterTextChunks(final List<TextChunk> textChunks, final TextChunkFilter filter) {
        if (filter == null) {
            return textChunks;
        }
        final List<TextChunk> filtered = new ArrayList<TextChunk>();
        for (final TextChunk textChunk : textChunks) {
            if (filter.accept(textChunk)) {
                filtered.add(textChunk);
            }
        }
        return filtered;
    }
    
    protected boolean isChunkAtWordBoundary(final TextChunk chunk, final TextChunk previousChunk) {
        return chunk.getLocation().isAtWordBoundary(previousChunk.getLocation());
    }
    
    public String getResultantText(final TextChunkFilter chunkFilter) {
        if (LocationTextExtractionStrategy.DUMP_STATE) {
            this.dumpState();
        }
        final List<TextChunk> filteredTextChunks = this.filterTextChunks(this.locationalResult, chunkFilter);
        Collections.sort(filteredTextChunks);
        final StringBuilder sb = new StringBuilder();
        TextChunk lastChunk = null;
        for (final TextChunk chunk : filteredTextChunks) {
            if (lastChunk == null) {
                sb.append(chunk.text);
            }
            else if (chunk.sameLine(lastChunk)) {
                if (this.isChunkAtWordBoundary(chunk, lastChunk) && !this.startsWithSpace(chunk.text) && !this.endsWithSpace(lastChunk.text)) {
                    sb.append(' ');
                }
                sb.append(chunk.text);
            }
            else {
                sb.append('\n');
                sb.append(chunk.text);
            }
            lastChunk = chunk;
        }
        return sb.toString();
    }
    
    @Override
    public String getResultantText() {
        return this.getResultantText(null);
    }
    
    private void dumpState() {
        for (final TextChunk location : this.locationalResult) {
            location.printDiagnostics();
            System.out.println();
        }
    }
    
    @Override
    public void renderText(final TextRenderInfo renderInfo) {
        LineSegment segment = renderInfo.getBaseline();
        if (renderInfo.getRise() != 0.0f) {
            final Matrix riseOffsetTransform = new Matrix(0.0f, -renderInfo.getRise());
            segment = segment.transformBy(riseOffsetTransform);
        }
        final TextChunk tc = new TextChunk(renderInfo.getText(), this.tclStrat.createLocation(renderInfo, segment));
        this.locationalResult.add(tc);
    }
    
    private static int compareInts(final int int1, final int int2) {
        return (int1 == int2) ? 0 : ((int1 < int2) ? -1 : 1);
    }
    
    @Override
    public void renderImage(final ImageRenderInfo renderInfo) {
    }
    
    static {
        LocationTextExtractionStrategy.DUMP_STATE = false;
    }
    
    public static class TextChunkLocationDefaultImp implements TextChunkLocation
    {
        private final Vector startLocation;
        private final Vector endLocation;
        private final Vector orientationVector;
        private final int orientationMagnitude;
        private final int distPerpendicular;
        private final float distParallelStart;
        private final float distParallelEnd;
        private final float charSpaceWidth;
        
        public TextChunkLocationDefaultImp(final Vector startLocation, final Vector endLocation, final float charSpaceWidth) {
            this.startLocation = startLocation;
            this.endLocation = endLocation;
            this.charSpaceWidth = charSpaceWidth;
            Vector oVector = endLocation.subtract(startLocation);
            if (oVector.length() == 0.0f) {
                oVector = new Vector(1.0f, 0.0f, 0.0f);
            }
            this.orientationVector = oVector.normalize();
            this.orientationMagnitude = (int)(Math.atan2(this.orientationVector.get(1), this.orientationVector.get(0)) * 1000.0);
            final Vector origin = new Vector(0.0f, 0.0f, 1.0f);
            this.distPerpendicular = (int)startLocation.subtract(origin).cross(this.orientationVector).get(2);
            this.distParallelStart = this.orientationVector.dot(startLocation);
            this.distParallelEnd = this.orientationVector.dot(endLocation);
        }
        
        @Override
        public int orientationMagnitude() {
            return this.orientationMagnitude;
        }
        
        @Override
        public int distPerpendicular() {
            return this.distPerpendicular;
        }
        
        @Override
        public float distParallelStart() {
            return this.distParallelStart;
        }
        
        @Override
        public float distParallelEnd() {
            return this.distParallelEnd;
        }
        
        @Override
        public Vector getStartLocation() {
            return this.startLocation;
        }
        
        @Override
        public Vector getEndLocation() {
            return this.endLocation;
        }
        
        @Override
        public float getCharSpaceWidth() {
            return this.charSpaceWidth;
        }
        
        @Override
        public boolean sameLine(final TextChunkLocation as) {
            return this.orientationMagnitude() == as.orientationMagnitude() && this.distPerpendicular() == as.distPerpendicular();
        }
        
        @Override
        public float distanceFromEndOf(final TextChunkLocation other) {
            final float distance = this.distParallelStart() - other.distParallelEnd();
            return distance;
        }
        
        @Override
        public boolean isAtWordBoundary(final TextChunkLocation previous) {
            if (this.getCharSpaceWidth() < 0.1f) {
                return false;
            }
            final float dist = this.distanceFromEndOf(previous);
            return dist < -this.getCharSpaceWidth() || dist > this.getCharSpaceWidth() / 2.0f;
        }
        
        @Override
        public int compareTo(final TextChunkLocation other) {
            if (this == other) {
                return 0;
            }
            int rslt = compareInts(this.orientationMagnitude(), other.orientationMagnitude());
            if (rslt != 0) {
                return rslt;
            }
            rslt = compareInts(this.distPerpendicular(), other.distPerpendicular());
            if (rslt != 0) {
                return rslt;
            }
            return Float.compare(this.distParallelStart(), other.distParallelStart());
        }
    }
    
    public static class TextChunk implements Comparable<TextChunk>
    {
        private final String text;
        private final TextChunkLocation location;
        
        public TextChunk(final String string, final Vector startLocation, final Vector endLocation, final float charSpaceWidth) {
            this(string, new TextChunkLocationDefaultImp(startLocation, endLocation, charSpaceWidth));
        }
        
        public TextChunk(final String string, final TextChunkLocation loc) {
            this.text = string;
            this.location = loc;
        }
        
        public String getText() {
            return this.text;
        }
        
        public TextChunkLocation getLocation() {
            return this.location;
        }
        
        public Vector getStartLocation() {
            return this.location.getStartLocation();
        }
        
        public Vector getEndLocation() {
            return this.location.getEndLocation();
        }
        
        public float getCharSpaceWidth() {
            return this.location.getCharSpaceWidth();
        }
        
        public float distanceFromEndOf(final TextChunk other) {
            return this.location.distanceFromEndOf(other.location);
        }
        
        private void printDiagnostics() {
            System.out.println("Text (@" + this.location.getStartLocation() + " -> " + this.location.getEndLocation() + "): " + this.text);
            System.out.println("orientationMagnitude: " + this.location.orientationMagnitude());
            System.out.println("distPerpendicular: " + this.location.distPerpendicular());
            System.out.println("distParallel: " + this.location.distParallelStart());
        }
        
        @Override
        public int compareTo(final TextChunk rhs) {
            return this.location.compareTo(rhs.location);
        }
        
        private boolean sameLine(final TextChunk lastChunk) {
            return this.getLocation().sameLine(lastChunk.getLocation());
        }
    }
    
    public interface TextChunkFilter
    {
        boolean accept(final TextChunk p0);
    }
    
    public interface TextChunkLocation extends Comparable<TextChunkLocation>
    {
        float distParallelEnd();
        
        float distParallelStart();
        
        int distPerpendicular();
        
        float getCharSpaceWidth();
        
        Vector getEndLocation();
        
        Vector getStartLocation();
        
        int orientationMagnitude();
        
        boolean sameLine(final TextChunkLocation p0);
        
        float distanceFromEndOf(final TextChunkLocation p0);
        
        boolean isAtWordBoundary(final TextChunkLocation p0);
    }
    
    public interface TextChunkLocationStrategy
    {
        TextChunkLocation createLocation(final TextRenderInfo p0, final LineSegment p1);
    }
}
