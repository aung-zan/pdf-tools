// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.io.InputStream;
import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import java.awt.image.BufferedImage;
import com.itextpdf.text.Version;
import com.itextpdf.text.pdf.codec.TiffWriter;
import com.itextpdf.text.exceptions.UnsupportedPdfException;
import java.io.OutputStream;
import com.itextpdf.text.pdf.codec.PngWriter;
import java.io.ByteArrayOutputStream;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfObject;
import java.util.Map;
import com.itextpdf.text.pdf.PdfName;
import java.util.HashMap;
import com.itextpdf.text.pdf.FilterHandlers;
import java.io.IOException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PRStream;
import com.itextpdf.text.pdf.PdfDictionary;

public class PdfImageObject
{
    private PdfDictionary dictionary;
    private byte[] imageBytes;
    private PdfDictionary colorSpaceDic;
    private int pngColorType;
    private int pngBitDepth;
    private int width;
    private int height;
    private int bpc;
    private byte[] palette;
    private byte[] icc;
    private int stride;
    private ImageBytesType streamContentType;
    
    public String getFileType() {
        return this.streamContentType.getFileExtension();
    }
    
    public ImageBytesType getImageBytesType() {
        return this.streamContentType;
    }
    
    public PdfImageObject(final PRStream stream) throws IOException {
        this(stream, PdfReader.getStreamBytesRaw(stream), null);
    }
    
    public PdfImageObject(final PRStream stream, final PdfDictionary colorSpaceDic) throws IOException {
        this(stream, PdfReader.getStreamBytesRaw(stream), colorSpaceDic);
    }
    
    protected PdfImageObject(final PdfDictionary dictionary, final byte[] samples, final PdfDictionary colorSpaceDic) throws IOException {
        this.pngColorType = -1;
        this.streamContentType = null;
        this.dictionary = dictionary;
        this.colorSpaceDic = colorSpaceDic;
        final TrackingFilter trackingFilter = new TrackingFilter();
        final Map<PdfName, FilterHandlers.FilterHandler> handlers = new HashMap<PdfName, FilterHandlers.FilterHandler>(FilterHandlers.getDefaultFilterHandlers());
        handlers.put(PdfName.JBIG2DECODE, trackingFilter);
        handlers.put(PdfName.DCTDECODE, trackingFilter);
        handlers.put(PdfName.JPXDECODE, trackingFilter);
        this.imageBytes = PdfReader.decodeBytes(samples, dictionary, handlers);
        if (trackingFilter.lastFilterName != null) {
            if (PdfName.JBIG2DECODE.equals(trackingFilter.lastFilterName)) {
                this.streamContentType = ImageBytesType.JBIG2;
            }
            else if (PdfName.DCTDECODE.equals(trackingFilter.lastFilterName)) {
                this.streamContentType = ImageBytesType.JPG;
            }
            else if (PdfName.JPXDECODE.equals(trackingFilter.lastFilterName)) {
                this.streamContentType = ImageBytesType.JP2;
            }
        }
        else {
            this.decodeImageBytes();
        }
    }
    
    public PdfObject get(final PdfName key) {
        return this.dictionary.get(key);
    }
    
    public PdfDictionary getDictionary() {
        return this.dictionary;
    }
    
    private void findColorspace(final PdfObject colorspace, final boolean allowIndexed) throws IOException {
        if (colorspace == null && this.bpc == 1) {
            this.stride = (this.width * this.bpc + 7) / 8;
            this.pngColorType = 0;
        }
        else if (PdfName.DEVICEGRAY.equals(colorspace)) {
            this.stride = (this.width * this.bpc + 7) / 8;
            this.pngColorType = 0;
        }
        else if (PdfName.DEVICERGB.equals(colorspace)) {
            if (this.bpc == 8 || this.bpc == 16) {
                this.stride = (this.width * this.bpc * 3 + 7) / 8;
                this.pngColorType = 2;
            }
        }
        else if (colorspace instanceof PdfArray) {
            final PdfArray ca = (PdfArray)colorspace;
            final PdfObject tyca = ca.getDirectObject(0);
            if (PdfName.CALGRAY.equals(tyca)) {
                this.stride = (this.width * this.bpc + 7) / 8;
                this.pngColorType = 0;
            }
            else if (PdfName.CALRGB.equals(tyca)) {
                if (this.bpc == 8 || this.bpc == 16) {
                    this.stride = (this.width * this.bpc * 3 + 7) / 8;
                    this.pngColorType = 2;
                }
            }
            else if (PdfName.ICCBASED.equals(tyca)) {
                final PRStream pr = (PRStream)ca.getDirectObject(1);
                final int n = pr.getAsNumber(PdfName.N).intValue();
                if (n == 1) {
                    this.stride = (this.width * this.bpc + 7) / 8;
                    this.pngColorType = 0;
                    this.icc = PdfReader.getStreamBytes(pr);
                }
                else if (n == 3) {
                    this.stride = (this.width * this.bpc * 3 + 7) / 8;
                    this.pngColorType = 2;
                    this.icc = PdfReader.getStreamBytes(pr);
                }
            }
            else if (allowIndexed && PdfName.INDEXED.equals(tyca)) {
                this.findColorspace(ca.getDirectObject(1), false);
                if (this.pngColorType == 2) {
                    final PdfObject id2 = ca.getDirectObject(3);
                    if (id2 instanceof PdfString) {
                        this.palette = ((PdfString)id2).getBytes();
                    }
                    else if (id2 instanceof PRStream) {
                        this.palette = PdfReader.getStreamBytes((PRStream)id2);
                    }
                    this.stride = (this.width * this.bpc + 7) / 8;
                    this.pngColorType = 3;
                }
            }
        }
    }
    
    private void decodeImageBytes() throws IOException {
        if (this.streamContentType != null) {
            throw new IllegalStateException(MessageLocalization.getComposedMessage("Decoding.can't.happen.on.this.type.of.stream.(.1.)", this.streamContentType));
        }
        this.pngColorType = -1;
        final PdfArray decode = this.dictionary.getAsArray(PdfName.DECODE);
        this.width = this.dictionary.getAsNumber(PdfName.WIDTH).intValue();
        this.height = this.dictionary.getAsNumber(PdfName.HEIGHT).intValue();
        this.bpc = this.dictionary.getAsNumber(PdfName.BITSPERCOMPONENT).intValue();
        this.pngBitDepth = this.bpc;
        PdfObject colorspace = this.dictionary.getDirectObject(PdfName.COLORSPACE);
        if (colorspace instanceof PdfName && this.colorSpaceDic != null) {
            final PdfObject csLookup = this.colorSpaceDic.getDirectObject((PdfName)colorspace);
            if (csLookup != null) {
                colorspace = csLookup;
            }
        }
        this.palette = null;
        this.icc = null;
        this.stride = 0;
        this.findColorspace(colorspace, true);
        final ByteArrayOutputStream ms = new ByteArrayOutputStream();
        if (this.pngColorType >= 0) {
            final PngWriter png = new PngWriter(ms);
            if (decode != null && this.pngBitDepth == 1 && decode.getAsNumber(0).intValue() == 1 && decode.getAsNumber(1).intValue() == 0) {
                for (int len = this.imageBytes.length, t = 0; t < len; ++t) {
                    final byte[] imageBytes = this.imageBytes;
                    final int n2 = t;
                    imageBytes[n2] ^= (byte)255;
                }
            }
            png.writeHeader(this.width, this.height, this.pngBitDepth, this.pngColorType);
            if (this.icc != null) {
                png.writeIccProfile(this.icc);
            }
            if (this.palette != null) {
                png.writePalette(this.palette);
            }
            png.writeData(this.imageBytes, this.stride);
            png.writeEnd();
            this.streamContentType = ImageBytesType.PNG;
            this.imageBytes = ms.toByteArray();
            return;
        }
        if (this.bpc != 8) {
            throw new UnsupportedPdfException(MessageLocalization.getComposedMessage("the.color.depth.1.is.not.supported", this.bpc));
        }
        if (!PdfName.DEVICECMYK.equals(colorspace)) {
            if (!(colorspace instanceof PdfArray)) {
                throw new UnsupportedPdfException(MessageLocalization.getComposedMessage("the.color.space.1.is.not.supported", colorspace));
            }
            final PdfArray ca = (PdfArray)colorspace;
            final PdfObject tyca = ca.getDirectObject(0);
            if (!PdfName.ICCBASED.equals(tyca)) {
                throw new UnsupportedPdfException(MessageLocalization.getComposedMessage("the.color.space.1.is.not.supported", colorspace));
            }
            final PRStream pr = (PRStream)ca.getDirectObject(1);
            final int n = pr.getAsNumber(PdfName.N).intValue();
            if (n != 4) {
                throw new UnsupportedPdfException(MessageLocalization.getComposedMessage("N.value.1.is.not.supported", n));
            }
            this.icc = PdfReader.getStreamBytes(pr);
        }
        this.stride = 4 * this.width;
        final TiffWriter wr = new TiffWriter();
        wr.addField(new TiffWriter.FieldShort(277, 4));
        wr.addField(new TiffWriter.FieldShort(258, new int[] { 8, 8, 8, 8 }));
        wr.addField(new TiffWriter.FieldShort(262, 5));
        wr.addField(new TiffWriter.FieldLong(256, this.width));
        wr.addField(new TiffWriter.FieldLong(257, this.height));
        wr.addField(new TiffWriter.FieldShort(259, 5));
        wr.addField(new TiffWriter.FieldShort(317, 2));
        wr.addField(new TiffWriter.FieldLong(278, this.height));
        wr.addField(new TiffWriter.FieldRational(282, new int[] { 300, 1 }));
        wr.addField(new TiffWriter.FieldRational(283, new int[] { 300, 1 }));
        wr.addField(new TiffWriter.FieldShort(296, 2));
        wr.addField(new TiffWriter.FieldAscii(305, Version.getInstance().getVersion()));
        final ByteArrayOutputStream comp = new ByteArrayOutputStream();
        TiffWriter.compressLZW(comp, 2, this.imageBytes, this.height, 4, this.stride);
        final byte[] buf = comp.toByteArray();
        wr.addField(new TiffWriter.FieldImage(buf));
        wr.addField(new TiffWriter.FieldLong(279, buf.length));
        if (this.icc != null) {
            wr.addField(new TiffWriter.FieldUndefined(34675, this.icc));
        }
        wr.writeFile(ms);
        this.streamContentType = ImageBytesType.CCITT;
        this.imageBytes = ms.toByteArray();
    }
    
    public byte[] getImageAsBytes() {
        return this.imageBytes;
    }
    
    public BufferedImage getBufferedImage() throws IOException {
        final byte[] img = this.getImageAsBytes();
        if (img == null) {
            return null;
        }
        return ImageIO.read(new ByteArrayInputStream(img));
    }
    
    public enum ImageBytesType
    {
        PNG("png"), 
        JPG("jpg"), 
        JP2("jp2"), 
        CCITT("tif"), 
        JBIG2("jbig2");
        
        private final String fileExtension;
        
        private ImageBytesType(final String fileExtension) {
            this.fileExtension = fileExtension;
        }
        
        public String getFileExtension() {
            return this.fileExtension;
        }
    }
    
    private static class TrackingFilter implements FilterHandlers.FilterHandler
    {
        public PdfName lastFilterName;
        
        private TrackingFilter() {
            this.lastFilterName = null;
        }
        
        @Override
        public byte[] decode(final byte[] b, final PdfName filterName, final PdfObject decodeParams, final PdfDictionary streamDictionary) throws IOException {
            this.lastFilterName = filterName;
            return b;
        }
    }
}
