// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import com.itextpdf.awt.geom.Rectangle2D;

public class LineSegment
{
    private final Vector startPoint;
    private final Vector endPoint;
    
    public LineSegment(final Vector startPoint, final Vector endPoint) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }
    
    public Vector getStartPoint() {
        return this.startPoint;
    }
    
    public Vector getEndPoint() {
        return this.endPoint;
    }
    
    public float getLength() {
        return this.endPoint.subtract(this.startPoint).length();
    }
    
    public Rectangle2D.Float getBoundingRectange() {
        final float x1 = this.getStartPoint().get(0);
        final float y1 = this.getStartPoint().get(1);
        final float x2 = this.getEndPoint().get(0);
        final float y2 = this.getEndPoint().get(1);
        return new Rectangle2D.Float(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
    }
    
    public LineSegment transformBy(final Matrix m) {
        final Vector newStart = this.startPoint.cross(m);
        final Vector newEnd = this.endPoint.cross(m);
        return new LineSegment(newStart, newEnd);
    }
}
