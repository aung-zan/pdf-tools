// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.util.Collection;
import java.util.ArrayList;
import com.itextpdf.awt.geom.Point2D;
import java.util.List;

public class BezierCurve implements Shape
{
    public static double curveCollinearityEpsilon;
    public static double distanceToleranceSquare;
    public static double distanceToleranceManhattan;
    private final List<Point2D> controlPoints;
    
    public BezierCurve(final List<Point2D> controlPoints) {
        this.controlPoints = new ArrayList<Point2D>(controlPoints);
    }
    
    @Override
    public List<Point2D> getBasePoints() {
        return this.controlPoints;
    }
    
    public List<Point2D> getPiecewiseLinearApproximation() {
        final List<Point2D> points = new ArrayList<Point2D>();
        points.add(this.controlPoints.get(0));
        this.recursiveApproximation(this.controlPoints.get(0).getX(), this.controlPoints.get(0).getY(), this.controlPoints.get(1).getX(), this.controlPoints.get(1).getY(), this.controlPoints.get(2).getX(), this.controlPoints.get(2).getY(), this.controlPoints.get(3).getX(), this.controlPoints.get(3).getY(), points);
        points.add(this.controlPoints.get(this.controlPoints.size() - 1));
        return points;
    }
    
    private void recursiveApproximation(final double x1, final double y1, final double x2, final double y2, final double x3, final double y3, final double x4, final double y4, final List<Point2D> points) {
        final double x5 = (x1 + x2) / 2.0;
        final double y5 = (y1 + y2) / 2.0;
        final double x6 = (x2 + x3) / 2.0;
        final double y6 = (y2 + y3) / 2.0;
        final double x7 = (x3 + x4) / 2.0;
        final double y7 = (y3 + y4) / 2.0;
        final double x8 = (x5 + x6) / 2.0;
        final double y8 = (y5 + y6) / 2.0;
        final double x9 = (x6 + x7) / 2.0;
        final double y9 = (y6 + y7) / 2.0;
        final double x10 = (x8 + x9) / 2.0;
        final double y10 = (y8 + y9) / 2.0;
        final double dx = x4 - x1;
        final double dy = y4 - y1;
        final double d2 = Math.abs((x2 - x4) * dy - (y2 - y4) * dx);
        final double d3 = Math.abs((x3 - x4) * dy - (y3 - y4) * dx);
        if (d2 > BezierCurve.curveCollinearityEpsilon || d3 > BezierCurve.curveCollinearityEpsilon) {
            if ((d2 + d3) * (d2 + d3) <= BezierCurve.distanceToleranceSquare * (dx * dx + dy * dy)) {
                points.add(new Point2D.Double(x10, y10));
                return;
            }
        }
        else if (Math.abs(x1 + x3 - x2 - x2) + Math.abs(y1 + y3 - y2 - y2) + Math.abs(x2 + x4 - x3 - x3) + Math.abs(y2 + y4 - y3 - y3) <= BezierCurve.distanceToleranceManhattan) {
            points.add(new Point2D.Double(x10, y10));
            return;
        }
        this.recursiveApproximation(x1, y1, x5, y5, x8, y8, x10, y10, points);
        this.recursiveApproximation(x10, y10, x9, y9, x7, y7, x4, y4, points);
    }
    
    static {
        BezierCurve.curveCollinearityEpsilon = 1.0E-30;
        BezierCurve.distanceToleranceSquare = 0.025;
        BezierCurve.distanceToleranceManhattan = 0.4;
    }
}
