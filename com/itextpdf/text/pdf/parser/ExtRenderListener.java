// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

public interface ExtRenderListener extends RenderListener
{
    void modifyPath(final PathConstructionRenderInfo p0);
    
    Path renderPath(final PathPaintingRenderInfo p0);
    
    void clipPath(final int p0);
}
