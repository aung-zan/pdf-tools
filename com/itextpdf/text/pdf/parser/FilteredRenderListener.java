// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

public class FilteredRenderListener implements RenderListener
{
    private final RenderListener delegate;
    private final RenderFilter[] filters;
    
    public FilteredRenderListener(final RenderListener delegate, final RenderFilter... filters) {
        this.delegate = delegate;
        this.filters = filters;
    }
    
    @Override
    public void renderText(final TextRenderInfo renderInfo) {
        for (final RenderFilter filter : this.filters) {
            if (!filter.allowText(renderInfo)) {
                return;
            }
        }
        this.delegate.renderText(renderInfo);
    }
    
    @Override
    public void beginTextBlock() {
        this.delegate.beginTextBlock();
    }
    
    @Override
    public void endTextBlock() {
        this.delegate.endTextBlock();
    }
    
    @Override
    public void renderImage(final ImageRenderInfo renderInfo) {
        for (final RenderFilter filter : this.filters) {
            if (!filter.allowImage(renderInfo)) {
                return;
            }
        }
        this.delegate.renderImage(renderInfo);
    }
}
