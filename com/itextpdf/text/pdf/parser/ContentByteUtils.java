// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import java.io.IOException;
import java.util.ListIterator;
import com.itextpdf.text.pdf.PdfArray;
import java.io.ByteArrayOutputStream;
import com.itextpdf.text.pdf.PRStream;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PRIndirectReference;
import com.itextpdf.text.pdf.PdfObject;

public class ContentByteUtils
{
    private ContentByteUtils() {
    }
    
    public static byte[] getContentBytesFromContentObject(final PdfObject contentObject) throws IOException {
        byte[] result = null;
        switch (contentObject.type()) {
            case 10: {
                final PRIndirectReference ref = (PRIndirectReference)contentObject;
                final PdfObject directObject = PdfReader.getPdfObjectRelease(ref);
                result = getContentBytesFromContentObject(directObject);
                break;
            }
            case 7: {
                final PRStream stream = (PRStream)PdfReader.getPdfObjectRelease(contentObject);
                result = PdfReader.getStreamBytes(stream);
                break;
            }
            case 5: {
                final ByteArrayOutputStream allBytes = new ByteArrayOutputStream();
                final PdfArray contentArray = (PdfArray)contentObject;
                final ListIterator<PdfObject> iter = contentArray.listIterator();
                while (iter.hasNext()) {
                    final PdfObject element = iter.next();
                    allBytes.write(getContentBytesFromContentObject(element));
                    allBytes.write(32);
                }
                result = allBytes.toByteArray();
                break;
            }
            default: {
                final String msg = "Unable to handle Content of type " + contentObject.getClass();
                throw new IllegalStateException(msg);
            }
        }
        return result;
    }
    
    public static byte[] getContentBytesForPage(final PdfReader reader, final int pageNum) throws IOException {
        final PdfDictionary pageDictionary = reader.getPageN(pageNum);
        final PdfObject contentObject = pageDictionary.get(PdfName.CONTENTS);
        if (contentObject == null) {
            return new byte[0];
        }
        final byte[] contentBytes = getContentBytesFromContentObject(contentObject);
        return contentBytes;
    }
}
