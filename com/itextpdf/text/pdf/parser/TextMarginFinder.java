// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import com.itextpdf.awt.geom.Rectangle2D;

public class TextMarginFinder implements RenderListener
{
    private Rectangle2D.Float textRectangle;
    
    public TextMarginFinder() {
        this.textRectangle = null;
    }
    
    @Override
    public void renderText(final TextRenderInfo renderInfo) {
        if (this.textRectangle == null) {
            this.textRectangle = renderInfo.getDescentLine().getBoundingRectange();
        }
        else {
            this.textRectangle.add(renderInfo.getDescentLine().getBoundingRectange());
        }
        this.textRectangle.add(renderInfo.getAscentLine().getBoundingRectange());
    }
    
    public float getLlx() {
        return this.textRectangle.x;
    }
    
    public float getLly() {
        return this.textRectangle.y;
    }
    
    public float getUrx() {
        return this.textRectangle.x + this.textRectangle.width;
    }
    
    public float getUry() {
        return this.textRectangle.y + this.textRectangle.height;
    }
    
    public float getWidth() {
        return this.textRectangle.width;
    }
    
    public float getHeight() {
        return this.textRectangle.height;
    }
    
    @Override
    public void beginTextBlock() {
    }
    
    @Override
    public void endTextBlock() {
    }
    
    @Override
    public void renderImage(final ImageRenderInfo renderInfo) {
    }
}
