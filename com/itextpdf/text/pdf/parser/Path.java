// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.util.Iterator;
import java.util.Arrays;
import java.util.Collection;
import java.util.ArrayList;
import com.itextpdf.awt.geom.Point2D;
import java.util.List;

public class Path
{
    private static final String START_PATH_ERR_MSG = "Path shall start with \"re\" or \"m\" operator";
    private List<Subpath> subpaths;
    private Point2D currentPoint;
    
    public Path() {
        this.subpaths = new ArrayList<Subpath>();
    }
    
    public Path(final List<? extends Subpath> subpaths) {
        this.subpaths = new ArrayList<Subpath>();
        this.addSubpaths(subpaths);
    }
    
    public List<Subpath> getSubpaths() {
        return this.subpaths;
    }
    
    public void addSubpath(final Subpath subpath) {
        this.subpaths.add(subpath);
        this.currentPoint = subpath.getLastPoint();
    }
    
    public void addSubpaths(final List<? extends Subpath> subpaths) {
        if (subpaths.size() > 0) {
            this.subpaths.addAll(subpaths);
            this.currentPoint = this.subpaths.get(subpaths.size() - 1).getLastPoint();
        }
    }
    
    public Point2D getCurrentPoint() {
        return this.currentPoint;
    }
    
    public void moveTo(final float x, final float y) {
        this.currentPoint = new Point2D.Float(x, y);
        final Subpath lastSubpath = this.getLastSubpath();
        if (lastSubpath != null && lastSubpath.isSinglePointOpen()) {
            lastSubpath.setStartPoint(this.currentPoint);
        }
        else {
            this.subpaths.add(new Subpath(this.currentPoint));
        }
    }
    
    public void lineTo(final float x, final float y) {
        if (this.currentPoint == null) {
            throw new RuntimeException("Path shall start with \"re\" or \"m\" operator");
        }
        final Point2D targetPoint = new Point2D.Float(x, y);
        this.getLastSubpath().addSegment(new Line(this.currentPoint, targetPoint));
        this.currentPoint = targetPoint;
    }
    
    public void curveTo(final float x1, final float y1, final float x2, final float y2, final float x3, final float y3) {
        if (this.currentPoint == null) {
            throw new RuntimeException("Path shall start with \"re\" or \"m\" operator");
        }
        final Point2D secondPoint = new Point2D.Float(x1, y1);
        final Point2D thirdPoint = new Point2D.Float(x2, y2);
        final Point2D fourthPoint = new Point2D.Float(x3, y3);
        final List<Point2D> controlPoints = new ArrayList<Point2D>(Arrays.asList(this.currentPoint, secondPoint, thirdPoint, fourthPoint));
        this.getLastSubpath().addSegment(new BezierCurve(controlPoints));
        this.currentPoint = fourthPoint;
    }
    
    public void curveTo(final float x2, final float y2, final float x3, final float y3) {
        if (this.currentPoint == null) {
            throw new RuntimeException("Path shall start with \"re\" or \"m\" operator");
        }
        this.curveTo((float)this.currentPoint.getX(), (float)this.currentPoint.getY(), x2, y2, x3, y3);
    }
    
    public void curveFromTo(final float x1, final float y1, final float x3, final float y3) {
        if (this.currentPoint == null) {
            throw new RuntimeException("Path shall start with \"re\" or \"m\" operator");
        }
        this.curveTo(x1, y1, x3, y3, x3, y3);
    }
    
    public void rectangle(final float x, final float y, final float w, final float h) {
        this.moveTo(x, y);
        this.lineTo(x + w, y);
        this.lineTo(x + w, y + h);
        this.lineTo(x, y + h);
        this.closeSubpath();
    }
    
    public void closeSubpath() {
        final Subpath lastSubpath = this.getLastSubpath();
        lastSubpath.setClosed(true);
        final Point2D startPoint = lastSubpath.getStartPoint();
        this.moveTo((float)startPoint.getX(), (float)startPoint.getY());
    }
    
    public void closeAllSubpaths() {
        for (final Subpath subpath : this.subpaths) {
            subpath.setClosed(true);
        }
    }
    
    public List<Integer> replaceCloseWithLine() {
        final List<Integer> modifiedSubpathsIndices = new ArrayList<Integer>();
        int i = 0;
        for (final Subpath subpath : this.subpaths) {
            if (subpath.isClosed()) {
                subpath.setClosed(false);
                subpath.addSegment(new Line(subpath.getLastPoint(), subpath.getStartPoint()));
                modifiedSubpathsIndices.add(i);
            }
            ++i;
        }
        return modifiedSubpathsIndices;
    }
    
    public boolean isEmpty() {
        return this.subpaths.size() == 0;
    }
    
    private Subpath getLastSubpath() {
        return (this.subpaths.size() > 0) ? this.subpaths.get(this.subpaths.size() - 1) : null;
    }
}
