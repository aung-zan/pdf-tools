// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.util.HashMap;
import com.itextpdf.text.log.LoggerFactory;
import com.itextpdf.text.exceptions.UnsupportedPdfException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.FilterHandlers;
import java.io.ByteArrayOutputStream;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PRTokeniser;
import java.io.IOException;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfContentParser;
import com.itextpdf.text.pdf.PdfName;
import java.util.Map;
import com.itextpdf.text.log.Logger;

public final class InlineImageUtils
{
    private static final Logger LOGGER;
    private static final Map<PdfName, PdfName> inlineImageEntryAbbreviationMap;
    private static final Map<PdfName, PdfName> inlineImageColorSpaceAbbreviationMap;
    private static final Map<PdfName, PdfName> inlineImageFilterAbbreviationMap;
    
    private InlineImageUtils() {
    }
    
    public static InlineImageInfo parseInlineImage(final PdfContentParser ps, final PdfDictionary colorSpaceDic) throws IOException {
        final PdfDictionary inlineImageDictionary = parseInlineImageDictionary(ps);
        final byte[] samples = parseInlineImageSamples(inlineImageDictionary, colorSpaceDic, ps);
        return new InlineImageInfo(samples, inlineImageDictionary);
    }
    
    private static PdfDictionary parseInlineImageDictionary(final PdfContentParser ps) throws IOException {
        final PdfDictionary dictionary = new PdfDictionary();
        for (PdfObject key = ps.readPRObject(); key != null && !"ID".equals(key.toString()); key = ps.readPRObject()) {
            final PdfObject value = ps.readPRObject();
            PdfName resolvedKey = InlineImageUtils.inlineImageEntryAbbreviationMap.get(key);
            if (resolvedKey == null) {
                resolvedKey = (PdfName)key;
            }
            dictionary.put(resolvedKey, getAlternateValue(resolvedKey, value));
        }
        final int ch = ps.getTokeniser().read();
        if (!PRTokeniser.isWhitespace(ch)) {
            throw new IOException("Unexpected character " + ch + " found after ID in inline image");
        }
        return dictionary;
    }
    
    private static PdfObject getAlternateValue(final PdfName key, final PdfObject value) {
        if (key == PdfName.FILTER) {
            if (value instanceof PdfName) {
                final PdfName altValue = InlineImageUtils.inlineImageFilterAbbreviationMap.get(value);
                if (altValue != null) {
                    return altValue;
                }
            }
            else if (value instanceof PdfArray) {
                final PdfArray array = (PdfArray)value;
                final PdfArray altArray = new PdfArray();
                for (int count = array.size(), i = 0; i < count; ++i) {
                    altArray.add(getAlternateValue(key, array.getPdfObject(i)));
                }
                return altArray;
            }
        }
        else if (key == PdfName.COLORSPACE) {
            final PdfName altValue = InlineImageUtils.inlineImageColorSpaceAbbreviationMap.get(value);
            if (altValue != null) {
                return altValue;
            }
        }
        return value;
    }
    
    private static int getComponentsPerPixel(final PdfName colorSpaceName, final PdfDictionary colorSpaceDic) {
        if (colorSpaceName == null) {
            return 1;
        }
        if (colorSpaceName.equals(PdfName.DEVICEGRAY)) {
            return 1;
        }
        if (colorSpaceName.equals(PdfName.DEVICERGB)) {
            return 3;
        }
        if (colorSpaceName.equals(PdfName.DEVICECMYK)) {
            return 4;
        }
        if (colorSpaceDic != null) {
            final PdfArray colorSpace = colorSpaceDic.getAsArray(colorSpaceName);
            if (colorSpace != null) {
                if (PdfName.INDEXED.equals(colorSpace.getAsName(0))) {
                    return 1;
                }
            }
            else {
                final PdfName tempName = colorSpaceDic.getAsName(colorSpaceName);
                if (tempName != null) {
                    return getComponentsPerPixel(tempName, colorSpaceDic);
                }
            }
        }
        throw new IllegalArgumentException("Unexpected color space " + colorSpaceName);
    }
    
    private static int computeBytesPerRow(final PdfDictionary imageDictionary, final PdfDictionary colorSpaceDic) {
        final PdfNumber wObj = imageDictionary.getAsNumber(PdfName.WIDTH);
        final PdfNumber bpcObj = imageDictionary.getAsNumber(PdfName.BITSPERCOMPONENT);
        final int cpp = getComponentsPerPixel(imageDictionary.getAsName(PdfName.COLORSPACE), colorSpaceDic);
        final int w = wObj.intValue();
        final int bpc = (bpcObj != null) ? bpcObj.intValue() : 1;
        final int bytesPerRow = (w * bpc * cpp + 7) / 8;
        return bytesPerRow;
    }
    
    private static byte[] parseUnfilteredSamples(final PdfDictionary imageDictionary, final PdfDictionary colorSpaceDic, final PdfContentParser ps) throws IOException {
        if (imageDictionary.contains(PdfName.FILTER)) {
            throw new IllegalArgumentException("Dictionary contains filters");
        }
        final PdfNumber h = imageDictionary.getAsNumber(PdfName.HEIGHT);
        final int bytesToRead = computeBytesPerRow(imageDictionary, colorSpaceDic) * h.intValue();
        final byte[] bytes = new byte[bytesToRead];
        final PRTokeniser tokeniser = ps.getTokeniser();
        final int shouldBeWhiteSpace = tokeniser.read();
        int startIndex = 0;
        if (!PRTokeniser.isWhitespace(shouldBeWhiteSpace) || shouldBeWhiteSpace == 0) {
            bytes[0] = (byte)shouldBeWhiteSpace;
            ++startIndex;
        }
        for (int i = startIndex; i < bytesToRead; ++i) {
            final int ch = tokeniser.read();
            if (ch == -1) {
                throw new InlineImageParseException("End of content stream reached before end of image data");
            }
            bytes[i] = (byte)ch;
        }
        final PdfObject ei = ps.readPRObject();
        if (!ei.toString().equals("EI")) {
            final PdfObject ei2 = ps.readPRObject();
            if (!ei2.toString().equals("EI")) {
                throw new InlineImageParseException("EI not found after end of image data");
            }
        }
        return bytes;
    }
    
    private static byte[] parseInlineImageSamples(final PdfDictionary imageDictionary, final PdfDictionary colorSpaceDic, final PdfContentParser ps) throws IOException {
        if (!imageDictionary.contains(PdfName.FILTER)) {
            return parseUnfilteredSamples(imageDictionary, colorSpaceDic, ps);
        }
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final ByteArrayOutputStream accumulated = new ByteArrayOutputStream();
        int found = 0;
        final PRTokeniser tokeniser = ps.getTokeniser();
        int ch;
        while ((ch = tokeniser.read()) != -1) {
            if (found == 0 && PRTokeniser.isWhitespace(ch)) {
                ++found;
                accumulated.write(ch);
            }
            else if (found == 1 && ch == 69) {
                ++found;
                accumulated.write(ch);
            }
            else if (found == 1 && PRTokeniser.isWhitespace(ch)) {
                baos.write(accumulated.toByteArray());
                accumulated.reset();
                accumulated.write(ch);
            }
            else if (found == 2 && ch == 73) {
                ++found;
                accumulated.write(ch);
            }
            else if (found == 3 && PRTokeniser.isWhitespace(ch)) {
                final byte[] tmp = baos.toByteArray();
                if (inlineImageStreamBytesAreComplete(tmp, imageDictionary)) {
                    return tmp;
                }
                baos.write(accumulated.toByteArray());
                accumulated.reset();
                baos.write(ch);
                found = 0;
            }
            else {
                baos.write(accumulated.toByteArray());
                accumulated.reset();
                baos.write(ch);
                found = 0;
            }
        }
        throw new InlineImageParseException("Could not find image data or EI");
    }
    
    private static boolean inlineImageStreamBytesAreComplete(final byte[] samples, final PdfDictionary imageDictionary) {
        try {
            PdfReader.decodeBytes(samples, imageDictionary, FilterHandlers.getDefaultFilterHandlers());
            return true;
        }
        catch (UnsupportedPdfException e) {
            InlineImageUtils.LOGGER.warn(e.getMessage());
            return true;
        }
        catch (IOException e2) {
            return false;
        }
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(InlineImageUtils.class.getName());
        (inlineImageEntryAbbreviationMap = new HashMap<PdfName, PdfName>()).put(PdfName.BITSPERCOMPONENT, PdfName.BITSPERCOMPONENT);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(PdfName.COLORSPACE, PdfName.COLORSPACE);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(PdfName.DECODE, PdfName.DECODE);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(PdfName.DECODEPARMS, PdfName.DECODEPARMS);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(PdfName.FILTER, PdfName.FILTER);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(PdfName.HEIGHT, PdfName.HEIGHT);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(PdfName.IMAGEMASK, PdfName.IMAGEMASK);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(PdfName.INTENT, PdfName.INTENT);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(PdfName.INTERPOLATE, PdfName.INTERPOLATE);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(PdfName.WIDTH, PdfName.WIDTH);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(new PdfName("BPC"), PdfName.BITSPERCOMPONENT);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(new PdfName("CS"), PdfName.COLORSPACE);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(new PdfName("D"), PdfName.DECODE);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(new PdfName("DP"), PdfName.DECODEPARMS);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(new PdfName("F"), PdfName.FILTER);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(new PdfName("H"), PdfName.HEIGHT);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(new PdfName("IM"), PdfName.IMAGEMASK);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(new PdfName("I"), PdfName.INTERPOLATE);
        InlineImageUtils.inlineImageEntryAbbreviationMap.put(new PdfName("W"), PdfName.WIDTH);
        (inlineImageColorSpaceAbbreviationMap = new HashMap<PdfName, PdfName>()).put(new PdfName("G"), PdfName.DEVICEGRAY);
        InlineImageUtils.inlineImageColorSpaceAbbreviationMap.put(new PdfName("RGB"), PdfName.DEVICERGB);
        InlineImageUtils.inlineImageColorSpaceAbbreviationMap.put(new PdfName("CMYK"), PdfName.DEVICECMYK);
        InlineImageUtils.inlineImageColorSpaceAbbreviationMap.put(new PdfName("I"), PdfName.INDEXED);
        (inlineImageFilterAbbreviationMap = new HashMap<PdfName, PdfName>()).put(new PdfName("AHx"), PdfName.ASCIIHEXDECODE);
        InlineImageUtils.inlineImageFilterAbbreviationMap.put(new PdfName("A85"), PdfName.ASCII85DECODE);
        InlineImageUtils.inlineImageFilterAbbreviationMap.put(new PdfName("LZW"), PdfName.LZWDECODE);
        InlineImageUtils.inlineImageFilterAbbreviationMap.put(new PdfName("Fl"), PdfName.FLATEDECODE);
        InlineImageUtils.inlineImageFilterAbbreviationMap.put(new PdfName("RL"), PdfName.RUNLENGTHDECODE);
        InlineImageUtils.inlineImageFilterAbbreviationMap.put(new PdfName("CCF"), PdfName.CCITTFAXDECODE);
        InlineImageUtils.inlineImageFilterAbbreviationMap.put(new PdfName("DCT"), PdfName.DCTDECODE);
    }
    
    public static class InlineImageParseException extends IOException
    {
        private static final long serialVersionUID = 233760879000268548L;
        
        public InlineImageParseException(final String message) {
            super(message);
        }
    }
}
