// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

public abstract class RenderFilter
{
    public boolean allowText(final TextRenderInfo renderInfo) {
        return true;
    }
    
    public boolean allowImage(final ImageRenderInfo renderInfo) {
        return true;
    }
}
