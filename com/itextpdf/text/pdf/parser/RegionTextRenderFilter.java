// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import com.itextpdf.text.Rectangle;
import com.itextpdf.awt.geom.Rectangle2D;

public class RegionTextRenderFilter extends RenderFilter
{
    private final Rectangle2D filterRect;
    
    public RegionTextRenderFilter(final Rectangle2D filterRect) {
        this.filterRect = filterRect;
    }
    
    public RegionTextRenderFilter(final Rectangle filterRect) {
        this.filterRect = new com.itextpdf.awt.geom.Rectangle(filterRect);
    }
    
    @Override
    public boolean allowText(final TextRenderInfo renderInfo) {
        final LineSegment segment = renderInfo.getBaseline();
        final Vector startPoint = segment.getStartPoint();
        final Vector endPoint = segment.getEndPoint();
        final float x1 = startPoint.get(0);
        final float y1 = startPoint.get(1);
        final float x2 = endPoint.get(0);
        final float y2 = endPoint.get(1);
        return this.filterRect.intersectsLine(x1, y1, x2, y2);
    }
}
