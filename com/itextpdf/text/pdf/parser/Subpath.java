// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.util.Set;
import java.util.Iterator;
import java.util.HashSet;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import com.itextpdf.awt.geom.Point2D;

public class Subpath
{
    private Point2D startPoint;
    private List<Shape> segments;
    private boolean closed;
    
    public Subpath() {
        this.segments = new ArrayList<Shape>();
    }
    
    public Subpath(final Subpath subpath) {
        this.segments = new ArrayList<Shape>();
        this.startPoint = subpath.startPoint;
        this.segments.addAll(subpath.getSegments());
        this.closed = subpath.closed;
    }
    
    public Subpath(final Point2D startPoint) {
        this((float)startPoint.getX(), (float)startPoint.getY());
    }
    
    public Subpath(final float startPointX, final float startPointY) {
        this.segments = new ArrayList<Shape>();
        this.startPoint = new Point2D.Float(startPointX, startPointY);
    }
    
    public void setStartPoint(final Point2D startPoint) {
        this.setStartPoint((float)startPoint.getX(), (float)startPoint.getY());
    }
    
    public void setStartPoint(final float x, final float y) {
        this.startPoint = new Point2D.Float(x, y);
    }
    
    public Point2D getStartPoint() {
        return this.startPoint;
    }
    
    public Point2D getLastPoint() {
        Point2D lastPoint = this.startPoint;
        if (this.segments.size() > 0 && !this.closed) {
            final Shape shape = this.segments.get(this.segments.size() - 1);
            lastPoint = shape.getBasePoints().get(shape.getBasePoints().size() - 1);
        }
        return lastPoint;
    }
    
    public void addSegment(final Shape segment) {
        if (this.closed) {
            return;
        }
        if (this.isSinglePointOpen()) {
            this.startPoint = segment.getBasePoints().get(0);
        }
        this.segments.add(segment);
    }
    
    public List<Shape> getSegments() {
        return this.segments;
    }
    
    public boolean isEmpty() {
        return this.startPoint == null;
    }
    
    public boolean isSinglePointOpen() {
        return this.segments.size() == 0 && !this.closed;
    }
    
    public boolean isSinglePointClosed() {
        return this.segments.size() == 0 && this.closed;
    }
    
    public boolean isClosed() {
        return this.closed;
    }
    
    public void setClosed(final boolean closed) {
        this.closed = closed;
    }
    
    public boolean isDegenerate() {
        if (this.segments.size() > 0 && this.closed) {
            return false;
        }
        for (final Shape segment : this.segments) {
            final Set<Point2D> points = new HashSet<Point2D>(segment.getBasePoints());
            if (points.size() != 1) {
                return false;
            }
        }
        return this.segments.size() > 0 || this.closed;
    }
    
    public List<Point2D> getPiecewiseLinearApproximation() {
        final List<Point2D> result = new ArrayList<Point2D>();
        if (this.segments.size() == 0) {
            return result;
        }
        if (this.segments.get(0) instanceof BezierCurve) {
            result.addAll(this.segments.get(0).getPiecewiseLinearApproximation());
        }
        else {
            result.addAll(this.segments.get(0).getBasePoints());
        }
        for (int i = 1; i < this.segments.size(); ++i) {
            List<Point2D> segApprox;
            if (this.segments.get(i) instanceof BezierCurve) {
                segApprox = this.segments.get(i).getPiecewiseLinearApproximation();
                segApprox = segApprox.subList(1, segApprox.size());
            }
            else {
                segApprox = this.segments.get(i).getBasePoints();
                segApprox = segApprox.subList(1, segApprox.size());
            }
            result.addAll(segApprox);
        }
        return result;
    }
}
