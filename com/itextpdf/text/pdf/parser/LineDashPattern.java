// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import com.itextpdf.text.pdf.PdfArray;

public class LineDashPattern
{
    private PdfArray dashArray;
    private float dashPhase;
    private int currentIndex;
    private int elemOrdinalNumber;
    private DashArrayElem currentElem;
    
    public LineDashPattern(final PdfArray dashArray, final float dashPhase) {
        this.elemOrdinalNumber = 1;
        this.dashArray = new PdfArray(dashArray);
        this.initFirst(this.dashPhase = dashPhase);
    }
    
    public PdfArray getDashArray() {
        return this.dashArray;
    }
    
    public void setDashArray(final PdfArray dashArray) {
        this.dashArray = dashArray;
    }
    
    public float getDashPhase() {
        return this.dashPhase;
    }
    
    public void setDashPhase(final float dashPhase) {
        this.dashPhase = dashPhase;
    }
    
    public DashArrayElem next() {
        final DashArrayElem ret = this.currentElem;
        if (this.dashArray.size() > 0) {
            this.currentIndex = (this.currentIndex + 1) % this.dashArray.size();
            this.currentElem = new DashArrayElem(this.dashArray.getAsNumber(this.currentIndex).floatValue(), this.isEven(++this.elemOrdinalNumber));
        }
        return ret;
    }
    
    public void reset() {
        this.currentIndex = 0;
        this.elemOrdinalNumber = 1;
        this.initFirst(this.dashPhase);
    }
    
    public boolean isSolid() {
        if (this.dashArray.size() % 2 != 0) {
            return false;
        }
        float unitsOffSum = 0.0f;
        for (int i = 1; i < this.dashArray.size(); i += 2) {
            unitsOffSum += this.dashArray.getAsNumber(i).floatValue();
        }
        return Float.compare(unitsOffSum, 0.0f) == 0;
    }
    
    private void initFirst(float phase) {
        if (this.dashArray.size() > 0) {
            while (phase > 0.0f) {
                phase -= this.dashArray.getAsNumber(this.currentIndex).floatValue();
                this.currentIndex = (this.currentIndex + 1) % this.dashArray.size();
                ++this.elemOrdinalNumber;
            }
            if (phase < 0.0f) {
                --this.elemOrdinalNumber;
                --this.currentIndex;
                this.currentElem = new DashArrayElem(-phase, this.isEven(this.elemOrdinalNumber));
            }
            else {
                this.currentElem = new DashArrayElem(this.dashArray.getAsNumber(this.currentIndex).floatValue(), this.isEven(this.elemOrdinalNumber));
            }
        }
    }
    
    private boolean isEven(final int num) {
        return num % 2 == 0;
    }
    
    public class DashArrayElem
    {
        private float val;
        private boolean isGap;
        
        public DashArrayElem(final float val, final boolean isGap) {
            this.val = val;
            this.isGap = isGap;
        }
        
        public float getVal() {
            return this.val;
        }
        
        public void setVal(final float val) {
            this.val = val;
        }
        
        public boolean isGap() {
            return this.isGap;
        }
        
        public void setGap(final boolean isGap) {
            this.isGap = isGap;
        }
    }
}
