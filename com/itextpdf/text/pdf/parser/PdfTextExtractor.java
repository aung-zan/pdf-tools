// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.util.HashMap;
import java.io.IOException;
import java.util.Map;
import com.itextpdf.text.pdf.PdfReader;

public final class PdfTextExtractor
{
    private PdfTextExtractor() {
    }
    
    public static String getTextFromPage(final PdfReader reader, final int pageNumber, final TextExtractionStrategy strategy, final Map<String, ContentOperator> additionalContentOperators) throws IOException {
        final PdfReaderContentParser parser = new PdfReaderContentParser(reader);
        return parser.processContent(pageNumber, strategy, additionalContentOperators).getResultantText();
    }
    
    public static String getTextFromPage(final PdfReader reader, final int pageNumber, final TextExtractionStrategy strategy) throws IOException {
        return getTextFromPage(reader, pageNumber, strategy, new HashMap<String, ContentOperator>());
    }
    
    public static String getTextFromPage(final PdfReader reader, final int pageNumber) throws IOException {
        return getTextFromPage(reader, pageNumber, new LocationTextExtractionStrategy());
    }
}
