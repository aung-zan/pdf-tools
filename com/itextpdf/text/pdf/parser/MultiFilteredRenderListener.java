// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public class MultiFilteredRenderListener implements RenderListener
{
    private final List<RenderListener> delegates;
    private final List<RenderFilter[]> filters;
    
    public MultiFilteredRenderListener() {
        this.delegates = new ArrayList<RenderListener>();
        this.filters = new ArrayList<RenderFilter[]>();
    }
    
    public <E extends RenderListener> E attachRenderListener(final E delegate, final RenderFilter... filterSet) {
        this.delegates.add(delegate);
        this.filters.add(filterSet);
        return delegate;
    }
    
    @Override
    public void beginTextBlock() {
        for (final RenderListener delegate : this.delegates) {
            delegate.beginTextBlock();
        }
    }
    
    @Override
    public void renderText(final TextRenderInfo renderInfo) {
        for (int i = 0; i < this.delegates.size(); ++i) {
            boolean filtersPassed = true;
            for (final RenderFilter filter : this.filters.get(i)) {
                if (!filter.allowText(renderInfo)) {
                    filtersPassed = false;
                    break;
                }
            }
            if (filtersPassed) {
                this.delegates.get(i).renderText(renderInfo);
            }
        }
    }
    
    @Override
    public void endTextBlock() {
        for (final RenderListener delegate : this.delegates) {
            delegate.endTextBlock();
        }
    }
    
    @Override
    public void renderImage(final ImageRenderInfo renderInfo) {
        for (int i = 0; i < this.delegates.size(); ++i) {
            boolean filtersPassed = true;
            for (final RenderFilter filter : this.filters.get(i)) {
                if (!filter.allowImage(renderInfo)) {
                    filtersPassed = false;
                    break;
                }
            }
            if (filtersPassed) {
                this.delegates.get(i).renderImage(renderInfo);
            }
        }
    }
}
