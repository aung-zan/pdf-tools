// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.util.Iterator;

public class GlyphRenderListener implements RenderListener
{
    private final RenderListener delegate;
    
    public GlyphRenderListener(final RenderListener delegate) {
        this.delegate = delegate;
    }
    
    @Override
    public void beginTextBlock() {
        this.delegate.beginTextBlock();
    }
    
    @Override
    public void renderText(final TextRenderInfo renderInfo) {
        for (final TextRenderInfo glyphInfo : renderInfo.getCharacterRenderInfos()) {
            this.delegate.renderText(glyphInfo);
        }
    }
    
    @Override
    public void endTextBlock() {
        this.delegate.endTextBlock();
    }
    
    @Override
    public void renderImage(final ImageRenderInfo renderInfo) {
        this.delegate.renderImage(renderInfo);
    }
}
