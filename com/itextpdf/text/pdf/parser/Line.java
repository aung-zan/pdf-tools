// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.util.ArrayList;
import java.util.List;
import com.itextpdf.awt.geom.Point2D;

public class Line implements Shape
{
    private final Point2D p1;
    private final Point2D p2;
    
    public Line() {
        this(0.0f, 0.0f, 0.0f, 0.0f);
    }
    
    public Line(final float x1, final float y1, final float x2, final float y2) {
        this.p1 = new Point2D.Float(x1, y1);
        this.p2 = new Point2D.Float(x2, y2);
    }
    
    public Line(final Point2D p1, final Point2D p2) {
        this((float)p1.getX(), (float)p1.getY(), (float)p2.getX(), (float)p2.getY());
    }
    
    @Override
    public List<Point2D> getBasePoints() {
        final List<Point2D> basePoints = new ArrayList<Point2D>(2);
        basePoints.add(this.p1);
        basePoints.add(this.p2);
        return basePoints;
    }
}
