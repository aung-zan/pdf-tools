// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

public interface TextExtractionStrategy extends RenderListener
{
    String getResultantText();
}
