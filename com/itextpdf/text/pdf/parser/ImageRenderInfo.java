// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.util.Iterator;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PRStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfIndirectReference;

public class ImageRenderInfo
{
    private final GraphicsState gs;
    private final PdfIndirectReference ref;
    private final InlineImageInfo inlineImageInfo;
    private final PdfDictionary colorSpaceDictionary;
    private PdfImageObject imageObject;
    private final Collection<MarkedContentInfo> markedContentInfos;
    
    private ImageRenderInfo(final GraphicsState gs, final PdfIndirectReference ref, final PdfDictionary colorSpaceDictionary, final Collection<MarkedContentInfo> markedContentInfo) {
        this.imageObject = null;
        this.gs = gs;
        this.ref = ref;
        this.inlineImageInfo = null;
        this.colorSpaceDictionary = colorSpaceDictionary;
        this.markedContentInfos = new ArrayList<MarkedContentInfo>(markedContentInfo);
    }
    
    private ImageRenderInfo(final GraphicsState gs, final InlineImageInfo inlineImageInfo, final PdfDictionary colorSpaceDictionary, final Collection<MarkedContentInfo> markedContentInfo) {
        this.imageObject = null;
        this.gs = gs;
        this.ref = null;
        this.inlineImageInfo = inlineImageInfo;
        this.colorSpaceDictionary = colorSpaceDictionary;
        this.markedContentInfos = new ArrayList<MarkedContentInfo>(markedContentInfo);
    }
    
    public static ImageRenderInfo createForXObject(final GraphicsState gs, final PdfIndirectReference ref, final PdfDictionary colorSpaceDictionary) {
        return new ImageRenderInfo(gs, ref, colorSpaceDictionary, null);
    }
    
    public static ImageRenderInfo createForXObject(final GraphicsState gs, final PdfIndirectReference ref, final PdfDictionary colorSpaceDictionary, final Collection<MarkedContentInfo> markedContentInfo) {
        return new ImageRenderInfo(gs, ref, colorSpaceDictionary, markedContentInfo);
    }
    
    protected static ImageRenderInfo createForEmbeddedImage(final GraphicsState gs, final InlineImageInfo inlineImageInfo, final PdfDictionary colorSpaceDictionary, final Collection<MarkedContentInfo> markedContentInfo) {
        final ImageRenderInfo renderInfo = new ImageRenderInfo(gs, inlineImageInfo, colorSpaceDictionary, markedContentInfo);
        return renderInfo;
    }
    
    public PdfImageObject getImage() throws IOException {
        this.prepareImageObject();
        return this.imageObject;
    }
    
    private void prepareImageObject() throws IOException {
        if (this.imageObject != null) {
            return;
        }
        if (this.ref != null) {
            final PRStream stream = (PRStream)PdfReader.getPdfObject(this.ref);
            this.imageObject = new PdfImageObject(stream, this.colorSpaceDictionary);
        }
        else if (this.inlineImageInfo != null) {
            this.imageObject = new PdfImageObject(this.inlineImageInfo.getImageDictionary(), this.inlineImageInfo.getSamples(), this.colorSpaceDictionary);
        }
    }
    
    public Vector getStartPoint() {
        return new Vector(0.0f, 0.0f, 1.0f).cross(this.gs.ctm);
    }
    
    public Matrix getImageCTM() {
        return this.gs.ctm;
    }
    
    public float getArea() {
        return this.gs.ctm.getDeterminant();
    }
    
    public PdfIndirectReference getRef() {
        return this.ref;
    }
    
    public BaseColor getCurrentFillColor() {
        return this.gs.fillColor;
    }
    
    public boolean hasMcid(final int mcid) {
        return this.hasMcid(mcid, false);
    }
    
    public boolean hasMcid(final int mcid, final boolean checkTheTopmostLevelOnly) {
        if (checkTheTopmostLevelOnly) {
            if (this.markedContentInfos instanceof ArrayList) {
                final Integer infoMcid = this.getMcid();
                return infoMcid != null && infoMcid == mcid;
            }
        }
        else {
            for (final MarkedContentInfo info : this.markedContentInfos) {
                if (info.hasMcid() && info.getMcid() == mcid) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public Integer getMcid() {
        if (this.markedContentInfos instanceof ArrayList) {
            final ArrayList<MarkedContentInfo> mci = (ArrayList<MarkedContentInfo>)(ArrayList)this.markedContentInfos;
            final MarkedContentInfo info = (mci.size() > 0) ? mci.get(mci.size() - 1) : null;
            return (info != null && info.hasMcid()) ? Integer.valueOf(info.getMcid()) : null;
        }
        return null;
    }
}
