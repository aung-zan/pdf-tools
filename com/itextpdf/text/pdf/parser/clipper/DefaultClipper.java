// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser.clipper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;
import java.util.logging.Logger;
import java.util.Comparator;
import java.util.List;

public class DefaultClipper extends ClipperBase
{
    protected final List<Path.OutRec> polyOuts;
    private Clipper.ClipType clipType;
    private Scanbeam scanbeam;
    private Path.Maxima maxima;
    private Edge activeEdges;
    private Edge sortedEdges;
    private final List<IntersectNode> intersectList;
    private final Comparator<IntersectNode> intersectNodeComparer;
    private Clipper.PolyFillType clipFillType;
    private Clipper.PolyFillType subjFillType;
    private final List<Path.Join> joins;
    private final List<Path.Join> ghostJoins;
    private boolean usingPolyTree;
    public Clipper.ZFillCallback zFillFunction;
    private final boolean reverseSolution;
    private final boolean strictlySimple;
    private static final Logger LOGGER;
    
    private static void getHorzDirection(final Edge HorzEdge, final Clipper.Direction[] Dir, final long[] Left, final long[] Right) {
        if (HorzEdge.getBot().getX() < HorzEdge.getTop().getX()) {
            Left[0] = HorzEdge.getBot().getX();
            Right[0] = HorzEdge.getTop().getX();
            Dir[0] = Clipper.Direction.LEFT_TO_RIGHT;
        }
        else {
            Left[0] = HorzEdge.getTop().getX();
            Right[0] = HorzEdge.getBot().getX();
            Dir[0] = Clipper.Direction.RIGHT_TO_LEFT;
        }
    }
    
    private static boolean getOverlap(final long a1, final long a2, final long b1, final long b2, final long[] Left, final long[] Right) {
        if (a1 < a2) {
            if (b1 < b2) {
                Left[0] = Math.max(a1, b1);
                Right[0] = Math.min(a2, b2);
            }
            else {
                Left[0] = Math.max(a1, b2);
                Right[0] = Math.min(a2, b1);
            }
        }
        else if (b1 < b2) {
            Left[0] = Math.max(a2, b1);
            Right[0] = Math.min(a1, b2);
        }
        else {
            Left[0] = Math.max(a2, b2);
            Right[0] = Math.min(a1, b1);
        }
        return Left[0] < Right[0];
    }
    
    private static boolean isParam1RightOfParam2(Path.OutRec outRec1, final Path.OutRec outRec2) {
        do {
            outRec1 = outRec1.firstLeft;
            if (outRec1 == outRec2) {
                return true;
            }
        } while (outRec1 != null);
        return false;
    }
    
    private static int isPointInPolygon(final Point.LongPoint pt, Path.OutPt op) {
        int result = 0;
        final Path.OutPt startOp = op;
        final long ptx = pt.getX();
        final long pty = pt.getY();
        long poly0x = op.getPt().getX();
        long poly0y = op.getPt().getY();
        do {
            op = op.next;
            final long poly1x = op.getPt().getX();
            final long poly1y = op.getPt().getY();
            if (poly1y == pty && (poly1x == ptx || (poly0y == pty && poly1x > ptx == poly0x < ptx))) {
                return -1;
            }
            if (poly0y < pty != poly1y < pty) {
                if (poly0x >= ptx) {
                    if (poly1x > ptx) {
                        result = 1 - result;
                    }
                    else {
                        final double d = (poly0x - ptx) * (double)(poly1y - pty) - (poly1x - ptx) * (double)(poly0y - pty);
                        if (d == 0.0) {
                            return -1;
                        }
                        if (d > 0.0 == poly1y > poly0y) {
                            result = 1 - result;
                        }
                    }
                }
                else if (poly1x > ptx) {
                    final double d = (poly0x - ptx) * (double)(poly1y - pty) - (poly1x - ptx) * (double)(poly0y - pty);
                    if (d == 0.0) {
                        return -1;
                    }
                    if (d > 0.0 == poly1y > poly0y) {
                        result = 1 - result;
                    }
                }
            }
            poly0x = poly1x;
            poly0y = poly1y;
        } while (startOp != op);
        return result;
    }
    
    private static boolean joinHorz(Path.OutPt op1, Path.OutPt op1b, Path.OutPt op2, Path.OutPt op2b, final Point.LongPoint Pt, final boolean DiscardLeft) {
        final Clipper.Direction Dir1 = (op1.getPt().getX() > op1b.getPt().getX()) ? Clipper.Direction.RIGHT_TO_LEFT : Clipper.Direction.LEFT_TO_RIGHT;
        final Clipper.Direction Dir2 = (op2.getPt().getX() > op2b.getPt().getX()) ? Clipper.Direction.RIGHT_TO_LEFT : Clipper.Direction.LEFT_TO_RIGHT;
        if (Dir1 == Dir2) {
            return false;
        }
        if (Dir1 == Clipper.Direction.LEFT_TO_RIGHT) {
            while (op1.next.getPt().getX() <= Pt.getX() && op1.next.getPt().getX() >= op1.getPt().getX() && op1.next.getPt().getY() == Pt.getY()) {
                op1 = op1.next;
            }
            if (DiscardLeft && op1.getPt().getX() != Pt.getX()) {
                op1 = op1.next;
            }
            op1b = op1.duplicate(!DiscardLeft);
            if (!op1b.getPt().equals(Pt)) {
                op1 = op1b;
                op1.setPt(Pt);
                op1b = op1.duplicate(!DiscardLeft);
            }
        }
        else {
            while (op1.next.getPt().getX() >= Pt.getX() && op1.next.getPt().getX() <= op1.getPt().getX() && op1.next.getPt().getY() == Pt.getY()) {
                op1 = op1.next;
            }
            if (!DiscardLeft && op1.getPt().getX() != Pt.getX()) {
                op1 = op1.next;
            }
            op1b = op1.duplicate(DiscardLeft);
            if (!op1b.getPt().equals(Pt)) {
                op1 = op1b;
                op1.setPt(Pt);
                op1b = op1.duplicate(DiscardLeft);
            }
        }
        if (Dir2 == Clipper.Direction.LEFT_TO_RIGHT) {
            while (op2.next.getPt().getX() <= Pt.getX() && op2.next.getPt().getX() >= op2.getPt().getX() && op2.next.getPt().getY() == Pt.getY()) {
                op2 = op2.next;
            }
            if (DiscardLeft && op2.getPt().getX() != Pt.getX()) {
                op2 = op2.next;
            }
            op2b = op2.duplicate(!DiscardLeft);
            if (!op2b.getPt().equals(Pt)) {
                op2 = op2b;
                op2.setPt(Pt);
                op2b = op2.duplicate(!DiscardLeft);
            }
        }
        else {
            while (op2.next.getPt().getX() >= Pt.getX() && op2.next.getPt().getX() <= op2.getPt().getX() && op2.next.getPt().getY() == Pt.getY()) {
                op2 = op2.next;
            }
            if (!DiscardLeft && op2.getPt().getX() != Pt.getX()) {
                op2 = op2.next;
            }
            op2b = op2.duplicate(DiscardLeft);
            if (!op2b.getPt().equals(Pt)) {
                op2 = op2b;
                op2.setPt(Pt);
                op2b = op2.duplicate(DiscardLeft);
            }
        }
        if (Dir1 == Clipper.Direction.LEFT_TO_RIGHT == DiscardLeft) {
            op1.prev = op2;
            op2.next = op1;
            op1b.next = op2b;
            op2b.prev = op1b;
        }
        else {
            op1.next = op2;
            op2.prev = op1;
            op1b.prev = op2b;
            op2b.next = op1b;
        }
        return true;
    }
    
    private boolean joinPoints(final Path.Join j, final Path.OutRec outRec1, final Path.OutRec outRec2) {
        Path.OutPt op1 = j.outPt1;
        Path.OutPt op2 = j.outPt2;
        final boolean isHorizontal = j.outPt1.getPt().getY() == j.getOffPt().getY();
        if (isHorizontal && j.getOffPt().equals(j.outPt1.getPt()) && j.getOffPt().equals(j.outPt2.getPt())) {
            if (outRec1 != outRec2) {
                return false;
            }
            Path.OutPt op1b;
            for (op1b = j.outPt1.next; op1b != op1 && op1b.getPt().equals(j.getOffPt()); op1b = op1b.next) {}
            final boolean reverse1 = op1b.getPt().getY() > j.getOffPt().getY();
            Path.OutPt op2b;
            for (op2b = j.outPt2.next; op2b != op2 && op2b.getPt().equals(j.getOffPt()); op2b = op2b.next) {}
            final boolean reverse2 = op2b.getPt().getY() > j.getOffPt().getY();
            if (reverse1 == reverse2) {
                return false;
            }
            if (reverse1) {
                op1b = op1.duplicate(false);
                op2b = op2.duplicate(true);
                op1.prev = op2;
                op2.next = op1;
                op1b.next = op2b;
                op2b.prev = op1b;
                j.outPt1 = op1;
                j.outPt2 = op1b;
                return true;
            }
            op1b = op1.duplicate(true);
            op2b = op2.duplicate(false);
            op1.next = op2;
            op2.prev = op1;
            op1b.prev = op2b;
            op2b.next = op1b;
            j.outPt1 = op1;
            j.outPt2 = op1b;
            return true;
        }
        else if (isHorizontal) {
            Path.OutPt op1b;
            for (op1b = op1; op1.prev.getPt().getY() == op1.getPt().getY() && op1.prev != op1b && op1.prev != op2; op1 = op1.prev) {}
            while (op1b.next.getPt().getY() == op1b.getPt().getY() && op1b.next != op1 && op1b.next != op2) {
                op1b = op1b.next;
            }
            if (op1b.next == op1 || op1b.next == op2) {
                return false;
            }
            Path.OutPt op2b;
            for (op2b = op2; op2.prev.getPt().getY() == op2.getPt().getY() && op2.prev != op2b && op2.prev != op1b; op2 = op2.prev) {}
            while (op2b.next.getPt().getY() == op2b.getPt().getY() && op2b.next != op2 && op2b.next != op1) {
                op2b = op2b.next;
            }
            if (op2b.next == op2 || op2b.next == op1) {
                return false;
            }
            final long[] LeftV = { 0L };
            final long[] RightV = { 0L };
            if (!getOverlap(op1.getPt().getX(), op1b.getPt().getX(), op2.getPt().getX(), op2b.getPt().getX(), LeftV, RightV)) {
                return false;
            }
            final long Left = LeftV[0];
            final long Right = RightV[0];
            Point.LongPoint Pt;
            boolean DiscardLeftSide;
            if (op1.getPt().getX() >= Left && op1.getPt().getX() <= Right) {
                Pt = new Point.LongPoint(op1.getPt());
                DiscardLeftSide = (op1.getPt().getX() > op1b.getPt().getX());
            }
            else if (op2.getPt().getX() >= Left && op2.getPt().getX() <= Right) {
                Pt = new Point.LongPoint(op2.getPt());
                DiscardLeftSide = (op2.getPt().getX() > op2b.getPt().getX());
            }
            else if (op1b.getPt().getX() >= Left && op1b.getPt().getX() <= Right) {
                Pt = new Point.LongPoint(op1b.getPt());
                DiscardLeftSide = (op1b.getPt().getX() > op1.getPt().getX());
            }
            else {
                Pt = new Point.LongPoint(op2b.getPt());
                DiscardLeftSide = (op2b.getPt().getX() > op2.getPt().getX());
            }
            j.outPt1 = op1;
            j.outPt2 = op2;
            return joinHorz(op1, op1b, op2, op2b, Pt, DiscardLeftSide);
        }
        else {
            Path.OutPt op1b;
            for (op1b = op1.next; op1b.getPt().equals(op1.getPt()) && op1b != op1; op1b = op1b.next) {}
            final boolean Reverse1 = op1b.getPt().getY() > op1.getPt().getY() || !Point.slopesEqual(op1.getPt(), op1b.getPt(), j.getOffPt(), this.useFullRange);
            if (Reverse1) {
                for (op1b = op1.prev; op1b.getPt().equals(op1.getPt()) && op1b != op1; op1b = op1b.prev) {}
                if (op1b.getPt().getY() > op1.getPt().getY() || !Point.slopesEqual(op1.getPt(), op1b.getPt(), j.getOffPt(), this.useFullRange)) {
                    return false;
                }
            }
            Path.OutPt op2b;
            for (op2b = op2.next; op2b.getPt().equals(op2.getPt()) && op2b != op2; op2b = op2b.next) {}
            final boolean Reverse2 = op2b.getPt().getY() > op2.getPt().getY() || !Point.slopesEqual(op2.getPt(), op2b.getPt(), j.getOffPt(), this.useFullRange);
            if (Reverse2) {
                for (op2b = op2.prev; op2b.getPt().equals(op2.getPt()) && op2b != op2; op2b = op2b.prev) {}
                if (op2b.getPt().getY() > op2.getPt().getY() || !Point.slopesEqual(op2.getPt(), op2b.getPt(), j.getOffPt(), this.useFullRange)) {
                    return false;
                }
            }
            if (op1b == op1 || op2b == op2 || op1b == op2b || (outRec1 == outRec2 && Reverse1 == Reverse2)) {
                return false;
            }
            if (Reverse1) {
                op1b = op1.duplicate(false);
                op2b = op2.duplicate(true);
                op1.prev = op2;
                op2.next = op1;
                op1b.next = op2b;
                op2b.prev = op1b;
                j.outPt1 = op1;
                j.outPt2 = op1b;
                return true;
            }
            op1b = op1.duplicate(true);
            op2b = op2.duplicate(false);
            op1.next = op2;
            op2.prev = op1;
            op1b.prev = op2b;
            op2b.next = op1b;
            j.outPt1 = op1;
            j.outPt2 = op1b;
            return true;
        }
    }
    
    private static Paths minkowski(final Path pattern, final Path path, final boolean IsSum, final boolean IsClosed) {
        final int delta = IsClosed ? 1 : 0;
        final int polyCnt = pattern.size();
        final int pathCnt = path.size();
        final Paths result = new Paths(pathCnt);
        if (IsSum) {
            for (int i = 0; i < pathCnt; ++i) {
                final Path p = new Path(polyCnt);
                for (final Point.LongPoint ip : pattern) {
                    p.add(new Point.LongPoint(path.get(i).getX() + ip.getX(), path.get(i).getY() + ip.getY(), 0L));
                }
                result.add(p);
            }
        }
        else {
            for (int i = 0; i < pathCnt; ++i) {
                final Path p = new Path(polyCnt);
                for (final Point.LongPoint ip : pattern) {
                    p.add(new Point.LongPoint(path.get(i).getX() - ip.getX(), path.get(i).getY() - ip.getY(), 0L));
                }
                result.add(p);
            }
        }
        final Paths quads = new Paths((pathCnt + delta) * (polyCnt + 1));
        for (int j = 0; j < pathCnt - 1 + delta; ++j) {
            for (int k = 0; k < polyCnt; ++k) {
                final Path quad = new Path(4);
                quad.add(result.get(j % pathCnt).get(k % polyCnt));
                quad.add(result.get((j + 1) % pathCnt).get(k % polyCnt));
                quad.add(result.get((j + 1) % pathCnt).get((k + 1) % polyCnt));
                quad.add(result.get(j % pathCnt).get((k + 1) % polyCnt));
                if (!quad.orientation()) {
                    Collections.reverse(quad);
                }
                quads.add(quad);
            }
        }
        return quads;
    }
    
    public static Paths minkowskiDiff(final Path poly1, final Path poly2) {
        final Paths paths = minkowski(poly1, poly2, false, true);
        final DefaultClipper c = new DefaultClipper();
        c.addPaths(paths, Clipper.PolyType.SUBJECT, true);
        c.execute(Clipper.ClipType.UNION, paths, Clipper.PolyFillType.NON_ZERO, Clipper.PolyFillType.NON_ZERO);
        return paths;
    }
    
    public static Paths minkowskiSum(final Path pattern, final Path path, final boolean pathIsClosed) {
        final Paths paths = minkowski(pattern, path, true, pathIsClosed);
        final DefaultClipper c = new DefaultClipper();
        c.addPaths(paths, Clipper.PolyType.SUBJECT, true);
        c.execute(Clipper.ClipType.UNION, paths, Clipper.PolyFillType.NON_ZERO, Clipper.PolyFillType.NON_ZERO);
        return paths;
    }
    
    public static Paths minkowskiSum(final Path pattern, final Paths paths, final boolean pathIsClosed) {
        final Paths solution = new Paths();
        final DefaultClipper c = new DefaultClipper();
        for (int i = 0; i < paths.size(); ++i) {
            final Paths tmp = minkowski(pattern, paths.get(i), true, pathIsClosed);
            c.addPaths(tmp, Clipper.PolyType.SUBJECT, true);
            if (pathIsClosed) {
                final Path path = paths.get(i).TranslatePath(pattern.get(0));
                c.addPath(path, Clipper.PolyType.CLIP, true);
            }
        }
        c.execute(Clipper.ClipType.UNION, solution, Clipper.PolyFillType.NON_ZERO, Clipper.PolyFillType.NON_ZERO);
        return solution;
    }
    
    private static boolean poly2ContainsPoly1(final Path.OutPt outPt1, final Path.OutPt outPt2) {
        Path.OutPt op = outPt1;
        do {
            final int res = isPointInPolygon(op.getPt(), outPt2);
            if (res >= 0) {
                return res > 0;
            }
            op = op.next;
        } while (op != outPt1);
        return true;
    }
    
    public static Paths simplifyPolygon(final Path poly) {
        return simplifyPolygon(poly, Clipper.PolyFillType.EVEN_ODD);
    }
    
    public static Paths simplifyPolygon(final Path poly, final Clipper.PolyFillType fillType) {
        final Paths result = new Paths();
        final DefaultClipper c = new DefaultClipper(2);
        c.addPath(poly, Clipper.PolyType.SUBJECT, true);
        c.execute(Clipper.ClipType.UNION, result, fillType, fillType);
        return result;
    }
    
    public static Paths simplifyPolygons(final Paths polys) {
        return simplifyPolygons(polys, Clipper.PolyFillType.EVEN_ODD);
    }
    
    public static Paths simplifyPolygons(final Paths polys, final Clipper.PolyFillType fillType) {
        final Paths result = new Paths();
        final DefaultClipper c = new DefaultClipper(2);
        c.addPaths(polys, Clipper.PolyType.SUBJECT, true);
        c.execute(Clipper.ClipType.UNION, result, fillType, fillType);
        return result;
    }
    
    public DefaultClipper() {
        this(0);
    }
    
    public DefaultClipper(final int InitOptions) {
        super((0x4 & InitOptions) != 0x0);
        this.scanbeam = null;
        this.maxima = null;
        this.activeEdges = null;
        this.sortedEdges = null;
        this.intersectList = new ArrayList<IntersectNode>();
        this.intersectNodeComparer = new Comparator<IntersectNode>() {
            @Override
            public int compare(final IntersectNode o1, final IntersectNode o2) {
                final long i = o2.getPt().getY() - o1.getPt().getY();
                if (i > 0L) {
                    return 1;
                }
                if (i < 0L) {
                    return -1;
                }
                return 0;
            }
        };
        this.usingPolyTree = false;
        this.polyOuts = new ArrayList<Path.OutRec>();
        this.joins = new ArrayList<Path.Join>();
        this.ghostJoins = new ArrayList<Path.Join>();
        this.reverseSolution = ((0x1 & InitOptions) != 0x0);
        this.strictlySimple = ((0x2 & InitOptions) != 0x0);
        this.zFillFunction = null;
    }
    
    private void insertScanbeam(final long Y) {
        if (this.scanbeam == null) {
            this.scanbeam = new Scanbeam();
            this.scanbeam.next = null;
            this.scanbeam.y = Y;
        }
        else if (Y > this.scanbeam.y) {
            final Scanbeam newSb = new Scanbeam();
            newSb.y = Y;
            newSb.next = this.scanbeam;
            this.scanbeam = newSb;
        }
        else {
            Scanbeam sb2;
            for (sb2 = this.scanbeam; sb2.next != null && Y <= sb2.next.y; sb2 = sb2.next) {}
            if (Y == sb2.y) {
                return;
            }
            final Scanbeam newSb2 = new Scanbeam();
            newSb2.y = Y;
            newSb2.next = sb2.next;
            sb2.next = newSb2;
        }
    }
    
    private void InsertMaxima(final long X) {
        final Path.Maxima newMax = new Path.Maxima();
        newMax.X = X;
        if (this.maxima == null) {
            this.maxima = newMax;
            this.maxima.Next = null;
            this.maxima.Prev = null;
        }
        else if (X < this.maxima.X) {
            newMax.Next = this.maxima;
            newMax.Prev = null;
            this.maxima = newMax;
        }
        else {
            Path.Maxima m;
            for (m = this.maxima; m.Next != null && X >= m.Next.X; m = m.Next) {}
            if (X == m.X) {
                return;
            }
            newMax.Next = m.Next;
            newMax.Prev = m;
            if (m.Next != null) {
                m.Next.Prev = newMax;
            }
            m.Next = newMax;
        }
    }
    
    private void addEdgeToSEL(final Edge edge) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "addEdgeToSEL");
        if (this.sortedEdges == null) {
            this.sortedEdges = edge;
            edge.prevInSEL = null;
            edge.nextInSEL = null;
        }
        else {
            edge.nextInSEL = this.sortedEdges;
            edge.prevInSEL = null;
            this.sortedEdges.prevInSEL = edge;
            this.sortedEdges = edge;
        }
    }
    
    private void addGhostJoin(final Path.OutPt Op, final Point.LongPoint OffPt) {
        final Path.Join j = new Path.Join();
        j.outPt1 = Op;
        j.setOffPt(OffPt);
        this.ghostJoins.add(j);
    }
    
    private void addJoin(final Path.OutPt Op1, final Path.OutPt Op2, final Point.LongPoint OffPt) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "addJoin");
        final Path.Join j = new Path.Join();
        j.outPt1 = Op1;
        j.outPt2 = Op2;
        j.setOffPt(OffPt);
        this.joins.add(j);
    }
    
    private void addLocalMaxPoly(final Edge e1, final Edge e2, final Point.LongPoint pt) {
        this.addOutPt(e1, pt);
        if (e2.windDelta == 0) {
            this.addOutPt(e2, pt);
        }
        if (e1.outIdx == e2.outIdx) {
            e1.outIdx = -1;
            e2.outIdx = -1;
        }
        else if (e1.outIdx < e2.outIdx) {
            this.appendPolygon(e1, e2);
        }
        else {
            this.appendPolygon(e2, e1);
        }
    }
    
    private Path.OutPt addLocalMinPoly(final Edge e1, final Edge e2, final Point.LongPoint pt) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "addLocalMinPoly");
        Path.OutPt result;
        Edge e3;
        Edge prevE;
        if (e2.isHorizontal() || e1.deltaX > e2.deltaX) {
            result = this.addOutPt(e1, pt);
            e2.outIdx = e1.outIdx;
            e1.side = Edge.Side.LEFT;
            e2.side = Edge.Side.RIGHT;
            e3 = e1;
            if (e3.prevInAEL == e2) {
                prevE = e2.prevInAEL;
            }
            else {
                prevE = e3.prevInAEL;
            }
        }
        else {
            result = this.addOutPt(e2, pt);
            e1.outIdx = e2.outIdx;
            e1.side = Edge.Side.RIGHT;
            e2.side = Edge.Side.LEFT;
            e3 = e2;
            if (e3.prevInAEL == e1) {
                prevE = e1.prevInAEL;
            }
            else {
                prevE = e3.prevInAEL;
            }
        }
        if (prevE != null && prevE.outIdx >= 0 && Edge.topX(prevE, pt.getY()) == Edge.topX(e3, pt.getY()) && Edge.slopesEqual(e3, prevE, this.useFullRange) && e3.windDelta != 0 && prevE.windDelta != 0) {
            final Path.OutPt outPt = this.addOutPt(prevE, pt);
            this.addJoin(result, outPt, e3.getTop());
        }
        return result;
    }
    
    private Path.OutPt addOutPt(final Edge e, final Point.LongPoint pt) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "addOutPt");
        if (e.outIdx < 0) {
            final Path.OutRec outRec = this.createOutRec();
            outRec.isOpen = (e.windDelta == 0);
            final Path.OutPt newOp = new Path.OutPt();
            outRec.pts = newOp;
            newOp.idx = outRec.Idx;
            newOp.pt = pt;
            newOp.next = newOp;
            newOp.prev = newOp;
            if (!outRec.isOpen) {
                this.setHoleState(e, outRec);
            }
            e.outIdx = outRec.Idx;
            return newOp;
        }
        final Path.OutRec outRec = this.polyOuts.get(e.outIdx);
        final Path.OutPt op = outRec.getPoints();
        final boolean ToFront = e.side == Edge.Side.LEFT;
        DefaultClipper.LOGGER.finest("op=" + op.getPointCount());
        DefaultClipper.LOGGER.finest(ToFront + " " + pt + " " + op.getPt());
        if (ToFront && pt.equals(op.getPt())) {
            return op;
        }
        if (!ToFront && pt.equals(op.prev.getPt())) {
            return op.prev;
        }
        final Path.OutPt newOp2 = new Path.OutPt();
        newOp2.idx = outRec.Idx;
        newOp2.setPt(new Point.LongPoint(pt));
        newOp2.next = op;
        newOp2.prev = op.prev;
        newOp2.prev.next = newOp2;
        op.prev = newOp2;
        if (ToFront) {
            outRec.setPoints(newOp2);
        }
        return newOp2;
    }
    
    private Path.OutPt GetLastOutPt(final Edge e) {
        final Path.OutRec outRec = this.polyOuts.get(e.outIdx);
        if (e.side == Edge.Side.LEFT) {
            return outRec.pts;
        }
        return outRec.pts.prev;
    }
    
    private void appendPolygon(final Edge e1, final Edge e2) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "appendPolygon");
        final Path.OutRec outRec1 = this.polyOuts.get(e1.outIdx);
        final Path.OutRec outRec2 = this.polyOuts.get(e2.outIdx);
        DefaultClipper.LOGGER.finest("" + e1.outIdx);
        DefaultClipper.LOGGER.finest("" + e2.outIdx);
        Path.OutRec holeStateRec;
        if (isParam1RightOfParam2(outRec1, outRec2)) {
            holeStateRec = outRec2;
        }
        else if (isParam1RightOfParam2(outRec2, outRec1)) {
            holeStateRec = outRec1;
        }
        else {
            holeStateRec = Path.OutPt.getLowerMostRec(outRec1, outRec2);
        }
        final Path.OutPt p1_lft = outRec1.getPoints();
        final Path.OutPt p1_rt = p1_lft.prev;
        final Path.OutPt p2_lft = outRec2.getPoints();
        final Path.OutPt p2_rt = p2_lft.prev;
        DefaultClipper.LOGGER.finest("p1_lft.getPointCount() = " + p1_lft.getPointCount());
        DefaultClipper.LOGGER.finest("p1_rt.getPointCount() = " + p1_rt.getPointCount());
        DefaultClipper.LOGGER.finest("p2_lft.getPointCount() = " + p2_lft.getPointCount());
        DefaultClipper.LOGGER.finest("p2_rt.getPointCount() = " + p2_rt.getPointCount());
        Edge.Side side;
        if (e1.side == Edge.Side.LEFT) {
            if (e2.side == Edge.Side.LEFT) {
                p2_lft.reversePolyPtLinks();
                p2_lft.next = p1_lft;
                p1_lft.prev = p2_lft;
                p1_rt.next = p2_rt;
                p2_rt.prev = p1_rt;
                outRec1.setPoints(p2_rt);
            }
            else {
                p2_rt.next = p1_lft;
                p1_lft.prev = p2_rt;
                p2_lft.prev = p1_rt;
                outRec1.setPoints(p1_rt.next = p2_lft);
            }
            side = Edge.Side.LEFT;
        }
        else {
            if (e2.side == Edge.Side.RIGHT) {
                p2_lft.reversePolyPtLinks();
                p1_rt.next = p2_rt;
                p2_rt.prev = p1_rt;
                p2_lft.next = p1_lft;
                p1_lft.prev = p2_lft;
            }
            else {
                p1_rt.next = p2_lft;
                p2_lft.prev = p1_rt;
                p1_lft.prev = p2_rt;
                p2_rt.next = p1_lft;
            }
            side = Edge.Side.RIGHT;
        }
        outRec1.bottomPt = null;
        if (holeStateRec.equals(outRec2)) {
            if (outRec2.firstLeft != outRec1) {
                outRec1.firstLeft = outRec2.firstLeft;
            }
            outRec1.isHole = outRec2.isHole;
        }
        outRec2.setPoints(null);
        outRec2.bottomPt = null;
        outRec2.firstLeft = outRec1;
        final int OKIdx = e1.outIdx;
        final int ObsoleteIdx = e2.outIdx;
        e1.outIdx = -1;
        e2.outIdx = -1;
        for (Edge e3 = this.activeEdges; e3 != null; e3 = e3.nextInAEL) {
            if (e3.outIdx == ObsoleteIdx) {
                e3.outIdx = OKIdx;
                e3.side = side;
                break;
            }
        }
        outRec2.Idx = outRec1.Idx;
    }
    
    private void buildIntersectList(final long topY) {
        if (this.activeEdges == null) {
            return;
        }
        Edge e = this.activeEdges;
        this.sortedEdges = e;
        while (e != null) {
            e.prevInSEL = e.prevInAEL;
            e.nextInSEL = e.nextInAEL;
            e.getCurrent().setX(Edge.topX(e, topY));
            e = e.nextInAEL;
        }
        boolean isModified = true;
        while (isModified && this.sortedEdges != null) {
            isModified = false;
            e = this.sortedEdges;
            while (e.nextInSEL != null) {
                final Edge eNext = e.nextInSEL;
                final Point.LongPoint[] pt = { null };
                if (e.getCurrent().getX() > eNext.getCurrent().getX()) {
                    this.intersectPoint(e, eNext, pt);
                    final IntersectNode newNode = new IntersectNode();
                    newNode.edge1 = e;
                    newNode.Edge2 = eNext;
                    newNode.setPt(pt[0]);
                    this.intersectList.add(newNode);
                    this.swapPositionsInSEL(e, eNext);
                    isModified = true;
                }
                else {
                    e = eNext;
                }
            }
            if (e.prevInSEL == null) {
                break;
            }
            e.prevInSEL.nextInSEL = null;
        }
        this.sortedEdges = null;
    }
    
    private void buildResult(final Paths polyg) {
        polyg.clear();
        for (int i = 0; i < this.polyOuts.size(); ++i) {
            final Path.OutRec outRec = this.polyOuts.get(i);
            if (outRec.getPoints() != null) {
                Path.OutPt p = outRec.getPoints().prev;
                final int cnt = p.getPointCount();
                DefaultClipper.LOGGER.finest("cnt = " + cnt);
                if (cnt >= 2) {
                    final Path pg = new Path(cnt);
                    for (int j = 0; j < cnt; ++j) {
                        pg.add(p.getPt());
                        p = p.prev;
                    }
                    polyg.add(pg);
                }
            }
        }
    }
    
    private void buildResult2(final PolyTree polytree) {
        polytree.Clear();
        for (int i = 0; i < this.polyOuts.size(); ++i) {
            final Path.OutRec outRec = this.polyOuts.get(i);
            final int cnt = (outRec.getPoints() != null) ? outRec.getPoints().getPointCount() : 0;
            if (!outRec.isOpen || cnt >= 2) {
                if (outRec.isOpen || cnt >= 3) {
                    outRec.fixHoleLinkage();
                    final PolyNode pn = new PolyNode();
                    polytree.getAllPolys().add(pn);
                    outRec.polyNode = pn;
                    Path.OutPt op = outRec.getPoints().prev;
                    for (int j = 0; j < cnt; ++j) {
                        pn.getPolygon().add(op.getPt());
                        op = op.prev;
                    }
                }
            }
        }
        for (int i = 0; i < this.polyOuts.size(); ++i) {
            final Path.OutRec outRec = this.polyOuts.get(i);
            if (outRec.polyNode != null) {
                if (outRec.isOpen) {
                    outRec.polyNode.setOpen(true);
                    polytree.addChild(outRec.polyNode);
                }
                else if (outRec.firstLeft != null && outRec.firstLeft.polyNode != null) {
                    outRec.firstLeft.polyNode.addChild(outRec.polyNode);
                }
                else {
                    polytree.addChild(outRec.polyNode);
                }
            }
        }
    }
    
    private void copyAELToSEL() {
        Edge e = this.activeEdges;
        this.sortedEdges = e;
        while (e != null) {
            e.prevInSEL = e.prevInAEL;
            e.nextInSEL = e.nextInAEL;
            e = e.nextInAEL;
        }
    }
    
    private Path.OutRec createOutRec() {
        final Path.OutRec result = new Path.OutRec();
        result.Idx = -1;
        result.isHole = false;
        result.isOpen = false;
        result.firstLeft = null;
        result.setPoints(null);
        result.bottomPt = null;
        result.polyNode = null;
        this.polyOuts.add(result);
        result.Idx = this.polyOuts.size() - 1;
        return result;
    }
    
    private void deleteFromAEL(final Edge e) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "deleteFromAEL");
        final Edge AelPrev = e.prevInAEL;
        final Edge AelNext = e.nextInAEL;
        if (AelPrev == null && AelNext == null && e != this.activeEdges) {
            return;
        }
        if (AelPrev != null) {
            AelPrev.nextInAEL = AelNext;
        }
        else {
            this.activeEdges = AelNext;
        }
        if (AelNext != null) {
            AelNext.prevInAEL = AelPrev;
        }
        e.nextInAEL = null;
        e.prevInAEL = null;
        DefaultClipper.LOGGER.exiting(DefaultClipper.class.getName(), "deleteFromAEL");
    }
    
    private void deleteFromSEL(final Edge e) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "deleteFromSEL");
        final Edge SelPrev = e.prevInSEL;
        final Edge SelNext = e.nextInSEL;
        if (SelPrev == null && SelNext == null && !e.equals(this.sortedEdges)) {
            return;
        }
        if (SelPrev != null) {
            SelPrev.nextInSEL = SelNext;
        }
        else {
            this.sortedEdges = SelNext;
        }
        if (SelNext != null) {
            SelNext.prevInSEL = SelPrev;
        }
        e.nextInSEL = null;
        e.prevInSEL = null;
    }
    
    private boolean doHorzSegmentsOverlap(long seg1a, long seg1b, long seg2a, long seg2b) {
        if (seg1a > seg1b) {
            final long tmp = seg1a;
            seg1a = seg1b;
            seg1b = tmp;
        }
        if (seg2a > seg2b) {
            final long tmp = seg2a;
            seg2a = seg2b;
            seg2b = tmp;
        }
        return seg1a < seg2b && seg2a < seg1b;
    }
    
    private void doMaxima(final Edge e) {
        final Edge eMaxPair = e.getMaximaPair();
        if (eMaxPair == null) {
            if (e.outIdx >= 0) {
                this.addOutPt(e, e.getTop());
            }
            this.deleteFromAEL(e);
            return;
        }
        for (Edge eNext = e.nextInAEL; eNext != null && eNext != eMaxPair; eNext = e.nextInAEL) {
            final Point.LongPoint tmp = new Point.LongPoint(e.getTop());
            this.intersectEdges(e, eNext, tmp);
            e.setTop(tmp);
            this.swapPositionsInAEL(e, eNext);
        }
        if (e.outIdx == -1 && eMaxPair.outIdx == -1) {
            this.deleteFromAEL(e);
            this.deleteFromAEL(eMaxPair);
        }
        else if (e.outIdx >= 0 && eMaxPair.outIdx >= 0) {
            if (e.outIdx >= 0) {
                this.addLocalMaxPoly(e, eMaxPair, e.getTop());
            }
            this.deleteFromAEL(e);
            this.deleteFromAEL(eMaxPair);
        }
        else {
            if (e.windDelta != 0) {
                throw new IllegalStateException("DoMaxima error");
            }
            if (e.outIdx >= 0) {
                this.addOutPt(e, e.getTop());
                e.outIdx = -1;
            }
            this.deleteFromAEL(e);
            if (eMaxPair.outIdx >= 0) {
                this.addOutPt(eMaxPair, e.getTop());
                eMaxPair.outIdx = -1;
            }
            this.deleteFromAEL(eMaxPair);
        }
    }
    
    private void doSimplePolygons() {
        int i = 0;
        while (i < this.polyOuts.size()) {
            final Path.OutRec outrec = this.polyOuts.get(i++);
            Path.OutPt op = outrec.getPoints();
            if (op != null) {
                if (outrec.isOpen) {
                    continue;
                }
                do {
                    for (Path.OutPt op2 = op.next; op2 != outrec.getPoints(); op2 = op2.next) {
                        if (op.getPt().equals(op2.getPt()) && !op2.next.equals(op) && !op2.prev.equals(op)) {
                            final Path.OutPt op3 = op.prev;
                            final Path.OutPt op4 = op2.prev;
                            op.prev = op4;
                            op4.next = op;
                            op2.prev = op3;
                            op3.next = op2;
                            outrec.setPoints(op);
                            final Path.OutRec outrec2 = this.createOutRec();
                            outrec2.setPoints(op2);
                            this.updateOutPtIdxs(outrec2);
                            if (poly2ContainsPoly1(outrec2.getPoints(), outrec.getPoints())) {
                                outrec2.isHole = !outrec.isHole;
                                outrec2.firstLeft = outrec;
                                if (this.usingPolyTree) {
                                    this.fixupFirstLefts2(outrec2, outrec);
                                }
                            }
                            else if (poly2ContainsPoly1(outrec.getPoints(), outrec2.getPoints())) {
                                outrec2.isHole = outrec.isHole;
                                outrec.isHole = !outrec2.isHole;
                                outrec2.firstLeft = outrec.firstLeft;
                                outrec.firstLeft = outrec2;
                                if (this.usingPolyTree) {
                                    this.fixupFirstLefts2(outrec, outrec2);
                                }
                            }
                            else {
                                outrec2.isHole = outrec.isHole;
                                outrec2.firstLeft = outrec.firstLeft;
                                if (this.usingPolyTree) {
                                    this.fixupFirstLefts1(outrec, outrec2);
                                }
                            }
                            op2 = op;
                        }
                    }
                    op = op.next;
                } while (op != outrec.getPoints());
            }
        }
    }
    
    private boolean EdgesAdjacent(final IntersectNode inode) {
        return inode.edge1.nextInSEL == inode.Edge2 || inode.edge1.prevInSEL == inode.Edge2;
    }
    
    public boolean execute(final Clipper.ClipType clipType, final Paths solution, final Clipper.PolyFillType FillType) {
        return this.execute(clipType, solution, FillType, FillType);
    }
    
    @Override
    public boolean execute(final Clipper.ClipType clipType, final PolyTree polytree) {
        return this.execute(clipType, polytree, Clipper.PolyFillType.EVEN_ODD);
    }
    
    public boolean execute(final Clipper.ClipType clipType, final PolyTree polytree, final Clipper.PolyFillType FillType) {
        return this.execute(clipType, polytree, FillType, FillType);
    }
    
    @Override
    public boolean execute(final Clipper.ClipType clipType, final Paths solution) {
        return this.execute(clipType, solution, Clipper.PolyFillType.EVEN_ODD);
    }
    
    @Override
    public boolean execute(final Clipper.ClipType clipType, final Paths solution, final Clipper.PolyFillType subjFillType, final Clipper.PolyFillType clipFillType) {
        synchronized (this) {
            if (this.hasOpenPaths) {
                throw new IllegalStateException("Error: PolyTree struct is needed for open path clipping.");
            }
            solution.clear();
            this.subjFillType = subjFillType;
            this.clipFillType = clipFillType;
            this.clipType = clipType;
            this.usingPolyTree = false;
            try {
                final boolean succeeded = this.executeInternal();
                if (succeeded) {
                    this.buildResult(solution);
                }
                return succeeded;
            }
            finally {
                this.polyOuts.clear();
            }
        }
    }
    
    @Override
    public boolean execute(final Clipper.ClipType clipType, final PolyTree polytree, final Clipper.PolyFillType subjFillType, final Clipper.PolyFillType clipFillType) {
        synchronized (this) {
            this.subjFillType = subjFillType;
            this.clipFillType = clipFillType;
            this.clipType = clipType;
            this.usingPolyTree = true;
            boolean succeeded;
            try {
                succeeded = this.executeInternal();
                if (succeeded) {
                    this.buildResult2(polytree);
                }
            }
            finally {
                this.polyOuts.clear();
            }
            return succeeded;
        }
    }
    
    private boolean executeInternal() {
        try {
            this.reset();
            if (this.currentLM == null) {
                return false;
            }
            long botY = this.popScanbeam();
            do {
                this.insertLocalMinimaIntoAEL(botY);
                this.processHorizontals();
                this.ghostJoins.clear();
                if (this.scanbeam == null) {
                    break;
                }
                final long topY = this.popScanbeam();
                if (!this.processIntersections(topY)) {
                    return false;
                }
                this.processEdgesAtTopOfScanbeam(topY);
                botY = topY;
            } while (this.scanbeam != null || this.currentLM != null);
            for (int i = 0; i < this.polyOuts.size(); ++i) {
                final Path.OutRec outRec = this.polyOuts.get(i);
                if (outRec.pts != null) {
                    if (!outRec.isOpen) {
                        if ((outRec.isHole ^ this.reverseSolution) == outRec.area() > 0.0) {
                            outRec.getPoints().reversePolyPtLinks();
                        }
                    }
                }
            }
            this.joinCommonEdges();
            for (int i = 0; i < this.polyOuts.size(); ++i) {
                final Path.OutRec outRec = this.polyOuts.get(i);
                if (outRec.getPoints() != null) {
                    if (outRec.isOpen) {
                        this.fixupOutPolyline(outRec);
                    }
                    else {
                        this.fixupOutPolygon(outRec);
                    }
                }
            }
            if (this.strictlySimple) {
                this.doSimplePolygons();
            }
            return true;
        }
        finally {
            this.joins.clear();
            this.ghostJoins.clear();
        }
    }
    
    private void fixupFirstLefts1(final Path.OutRec OldOutRec, final Path.OutRec NewOutRec) {
        for (int i = 0; i < this.polyOuts.size(); ++i) {
            final Path.OutRec outRec = this.polyOuts.get(i);
            if (outRec.getPoints() != null) {
                if (outRec.firstLeft != null) {
                    final Path.OutRec firstLeft = ClipperBase.parseFirstLeft(outRec.firstLeft);
                    if (firstLeft.equals(OldOutRec) && poly2ContainsPoly1(outRec.getPoints(), NewOutRec.getPoints())) {
                        outRec.firstLeft = NewOutRec;
                    }
                }
            }
        }
    }
    
    private void fixupFirstLefts2(final Path.OutRec OldOutRec, final Path.OutRec NewOutRec) {
        for (final Path.OutRec outRec : this.polyOuts) {
            if (outRec.firstLeft == OldOutRec) {
                outRec.firstLeft = NewOutRec;
            }
        }
    }
    
    private boolean fixupIntersectionOrder() {
        Collections.sort(this.intersectList, this.intersectNodeComparer);
        this.copyAELToSEL();
        for (int cnt = this.intersectList.size(), i = 0; i < cnt; ++i) {
            if (!this.EdgesAdjacent(this.intersectList.get(i))) {
                int j;
                for (j = i + 1; j < cnt && !this.EdgesAdjacent(this.intersectList.get(j)); ++j) {}
                if (j == cnt) {
                    return false;
                }
                final IntersectNode tmp = this.intersectList.get(i);
                this.intersectList.set(i, this.intersectList.get(j));
                this.intersectList.set(j, tmp);
            }
            this.swapPositionsInSEL(this.intersectList.get(i).edge1, this.intersectList.get(i).Edge2);
        }
        return true;
    }
    
    private void fixupOutPolyline(final Path.OutRec outrec) {
        Path.OutPt pp = outrec.pts;
        Path.OutPt tmpPP = null;
        for (Path.OutPt lastPP = pp.prev; pp != lastPP; pp = tmpPP) {
            pp = pp.next;
            if (pp.pt.equals(pp.prev.pt)) {
                if (pp == lastPP) {
                    lastPP = pp.prev;
                }
                tmpPP = pp.prev;
                tmpPP.next = pp.next;
                pp.next.prev = tmpPP;
            }
        }
        if (pp == pp.prev) {
            outrec.pts = null;
        }
    }
    
    private void fixupOutPolygon(final Path.OutRec outRec) {
        Path.OutPt lastOK = null;
        outRec.bottomPt = null;
        Path.OutPt pp = outRec.getPoints();
        final boolean preserveCol = this.preserveCollinear || this.strictlySimple;
        while (pp.prev != pp && pp.prev != pp.next) {
            if (pp.getPt().equals(pp.next.getPt()) || pp.getPt().equals(pp.prev.getPt()) || (Point.slopesEqual(pp.prev.getPt(), pp.getPt(), pp.next.getPt(), this.useFullRange) && (!preserveCol || !Point.isPt2BetweenPt1AndPt3(pp.prev.getPt(), pp.getPt(), pp.next.getPt())))) {
                lastOK = null;
                pp.prev.next = pp.next;
                pp.next.prev = pp.prev;
                pp = pp.prev;
            }
            else {
                if (pp == lastOK) {
                    outRec.setPoints(pp);
                    return;
                }
                if (lastOK == null) {
                    lastOK = pp;
                }
                pp = pp.next;
            }
        }
        outRec.setPoints(null);
    }
    
    private Path.OutRec getOutRec(final int idx) {
        Path.OutRec outrec;
        for (outrec = this.polyOuts.get(idx); outrec != this.polyOuts.get(outrec.Idx); outrec = this.polyOuts.get(outrec.Idx)) {}
        return outrec;
    }
    
    private void insertEdgeIntoAEL(final Edge edge, Edge startEdge) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "insertEdgeIntoAEL");
        if (this.activeEdges == null) {
            edge.prevInAEL = null;
            edge.nextInAEL = null;
            DefaultClipper.LOGGER.finest("Edge " + edge.outIdx + " -> " + (Object)null);
            this.activeEdges = edge;
        }
        else if (startEdge == null && Edge.doesE2InsertBeforeE1(this.activeEdges, edge)) {
            edge.prevInAEL = null;
            edge.nextInAEL = this.activeEdges;
            DefaultClipper.LOGGER.finest("Edge " + edge.outIdx + " -> " + edge.nextInAEL.outIdx);
            this.activeEdges.prevInAEL = edge;
            this.activeEdges = edge;
        }
        else {
            DefaultClipper.LOGGER.finest("activeEdges unchanged");
            if (startEdge == null) {
                startEdge = this.activeEdges;
            }
            while (startEdge.nextInAEL != null && !Edge.doesE2InsertBeforeE1(startEdge.nextInAEL, edge)) {
                startEdge = startEdge.nextInAEL;
            }
            edge.nextInAEL = startEdge.nextInAEL;
            if (startEdge.nextInAEL != null) {
                startEdge.nextInAEL.prevInAEL = edge;
            }
            edge.prevInAEL = startEdge;
            startEdge.nextInAEL = edge;
        }
    }
    
    private void insertLocalMinimaIntoAEL(final long botY) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "insertLocalMinimaIntoAEL");
        while (this.currentLM != null && this.currentLM.y == botY) {
            final Edge lb = this.currentLM.leftBound;
            final Edge rb = this.currentLM.rightBound;
            this.popLocalMinima();
            Path.OutPt Op1 = null;
            if (lb == null) {
                this.insertEdgeIntoAEL(rb, null);
                this.updateWindingCount(rb);
                if (rb.isContributing(this.clipFillType, this.subjFillType, this.clipType)) {
                    Op1 = this.addOutPt(rb, rb.getBot());
                }
            }
            else if (rb == null) {
                this.insertEdgeIntoAEL(lb, null);
                this.updateWindingCount(lb);
                if (lb.isContributing(this.clipFillType, this.subjFillType, this.clipType)) {
                    Op1 = this.addOutPt(lb, lb.getBot());
                }
                this.insertScanbeam(lb.getTop().getY());
            }
            else {
                this.insertEdgeIntoAEL(lb, null);
                this.insertEdgeIntoAEL(rb, lb);
                this.updateWindingCount(lb);
                rb.windCnt = lb.windCnt;
                rb.windCnt2 = lb.windCnt2;
                if (lb.isContributing(this.clipFillType, this.subjFillType, this.clipType)) {
                    Op1 = this.addLocalMinPoly(lb, rb, lb.getBot());
                }
                this.insertScanbeam(lb.getTop().getY());
            }
            if (rb != null) {
                if (rb.isHorizontal()) {
                    this.addEdgeToSEL(rb);
                }
                else {
                    this.insertScanbeam(rb.getTop().getY());
                }
            }
            if (lb != null) {
                if (rb == null) {
                    continue;
                }
                if (Op1 != null && rb.isHorizontal() && this.ghostJoins.size() > 0 && rb.windDelta != 0) {
                    for (int i = 0; i < this.ghostJoins.size(); ++i) {
                        final Path.Join j = this.ghostJoins.get(i);
                        if (this.doHorzSegmentsOverlap(j.outPt1.getPt().getX(), j.getOffPt().getX(), rb.getBot().getX(), rb.getTop().getX())) {
                            this.addJoin(j.outPt1, Op1, j.getOffPt());
                        }
                    }
                }
                if (lb.outIdx >= 0 && lb.prevInAEL != null && lb.prevInAEL.getCurrent().getX() == lb.getBot().getX() && lb.prevInAEL.outIdx >= 0 && Edge.slopesEqual(lb.prevInAEL, lb, this.useFullRange) && lb.windDelta != 0 && lb.prevInAEL.windDelta != 0) {
                    final Path.OutPt Op2 = this.addOutPt(lb.prevInAEL, lb.getBot());
                    this.addJoin(Op1, Op2, lb.getTop());
                }
                if (lb.nextInAEL == rb) {
                    continue;
                }
                if (rb.outIdx >= 0 && rb.prevInAEL.outIdx >= 0 && Edge.slopesEqual(rb.prevInAEL, rb, this.useFullRange) && rb.windDelta != 0 && rb.prevInAEL.windDelta != 0) {
                    final Path.OutPt Op2 = this.addOutPt(rb.prevInAEL, rb.getBot());
                    this.addJoin(Op1, Op2, rb.getTop());
                }
                Edge e = lb.nextInAEL;
                if (e == null) {
                    continue;
                }
                while (e != rb) {
                    this.intersectEdges(rb, e, lb.getCurrent());
                    e = e.nextInAEL;
                }
            }
        }
    }
    
    private void intersectEdges(final Edge e1, final Edge e2, final Point.LongPoint pt) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "insersectEdges");
        final boolean e1Contributing = e1.outIdx >= 0;
        final boolean e2Contributing = e2.outIdx >= 0;
        this.setZ(pt, e1, e2);
        if (e1.windDelta != 0 && e2.windDelta != 0) {
            if (e1.polyTyp == e2.polyTyp) {
                if (e1.isEvenOddFillType(this.clipFillType, this.subjFillType)) {
                    final int oldE1WindCnt = e1.windCnt;
                    e1.windCnt = e2.windCnt;
                    e2.windCnt = oldE1WindCnt;
                }
                else {
                    if (e1.windCnt + e2.windDelta == 0) {
                        e1.windCnt = -e1.windCnt;
                    }
                    else {
                        e1.windCnt += e2.windDelta;
                    }
                    if (e2.windCnt - e1.windDelta == 0) {
                        e2.windCnt = -e2.windCnt;
                    }
                    else {
                        e2.windCnt -= e1.windDelta;
                    }
                }
            }
            else {
                if (!e2.isEvenOddFillType(this.clipFillType, this.subjFillType)) {
                    e1.windCnt2 += e2.windDelta;
                }
                else {
                    e1.windCnt2 = ((e1.windCnt2 == 0) ? 1 : 0);
                }
                if (!e1.isEvenOddFillType(this.clipFillType, this.subjFillType)) {
                    e2.windCnt2 -= e1.windDelta;
                }
                else {
                    e2.windCnt2 = ((e2.windCnt2 == 0) ? 1 : 0);
                }
            }
            Clipper.PolyFillType e1FillType;
            Clipper.PolyFillType e1FillType2;
            if (e1.polyTyp == Clipper.PolyType.SUBJECT) {
                e1FillType = this.subjFillType;
                e1FillType2 = this.clipFillType;
            }
            else {
                e1FillType = this.clipFillType;
                e1FillType2 = this.subjFillType;
            }
            Clipper.PolyFillType e2FillType;
            Clipper.PolyFillType e2FillType2;
            if (e2.polyTyp == Clipper.PolyType.SUBJECT) {
                e2FillType = this.subjFillType;
                e2FillType2 = this.clipFillType;
            }
            else {
                e2FillType = this.clipFillType;
                e2FillType2 = this.subjFillType;
            }
            int e1Wc = 0;
            switch (e1FillType) {
                case POSITIVE: {
                    e1Wc = e1.windCnt;
                    break;
                }
                case NEGATIVE: {
                    e1Wc = -e1.windCnt;
                    break;
                }
                default: {
                    e1Wc = Math.abs(e1.windCnt);
                    break;
                }
            }
            int e2Wc = 0;
            switch (e2FillType) {
                case POSITIVE: {
                    e2Wc = e2.windCnt;
                    break;
                }
                case NEGATIVE: {
                    e2Wc = -e2.windCnt;
                    break;
                }
                default: {
                    e2Wc = Math.abs(e2.windCnt);
                    break;
                }
            }
            if (e1Contributing && e2Contributing) {
                if ((e1Wc != 0 && e1Wc != 1) || (e2Wc != 0 && e2Wc != 1) || (e1.polyTyp != e2.polyTyp && this.clipType != Clipper.ClipType.XOR)) {
                    this.addLocalMaxPoly(e1, e2, pt);
                }
                else {
                    this.addOutPt(e1, pt);
                    this.addOutPt(e2, pt);
                    Edge.swapSides(e1, e2);
                    Edge.swapPolyIndexes(e1, e2);
                }
            }
            else if (e1Contributing) {
                if (e2Wc == 0 || e2Wc == 1) {
                    this.addOutPt(e1, pt);
                    Edge.swapSides(e1, e2);
                    Edge.swapPolyIndexes(e1, e2);
                }
            }
            else if (e2Contributing) {
                if (e1Wc == 0 || e1Wc == 1) {
                    this.addOutPt(e2, pt);
                    Edge.swapSides(e1, e2);
                    Edge.swapPolyIndexes(e1, e2);
                }
            }
            else if ((e1Wc == 0 || e1Wc == 1) && (e2Wc == 0 || e2Wc == 1)) {
                int e1Wc2 = 0;
                switch (e1FillType2) {
                    case POSITIVE: {
                        e1Wc2 = e1.windCnt2;
                        break;
                    }
                    case NEGATIVE: {
                        e1Wc2 = -e1.windCnt2;
                        break;
                    }
                    default: {
                        e1Wc2 = Math.abs(e1.windCnt2);
                        break;
                    }
                }
                int e2Wc2 = 0;
                switch (e2FillType2) {
                    case POSITIVE: {
                        e2Wc2 = e2.windCnt2;
                        break;
                    }
                    case NEGATIVE: {
                        e2Wc2 = -e2.windCnt2;
                        break;
                    }
                    default: {
                        e2Wc2 = Math.abs(e2.windCnt2);
                        break;
                    }
                }
                if (e1.polyTyp != e2.polyTyp) {
                    this.addLocalMinPoly(e1, e2, pt);
                }
                else if (e1Wc == 1 && e2Wc == 1) {
                    switch (this.clipType) {
                        case INTERSECTION: {
                            if (e1Wc2 > 0 && e2Wc2 > 0) {
                                this.addLocalMinPoly(e1, e2, pt);
                                break;
                            }
                            break;
                        }
                        case UNION: {
                            if (e1Wc2 <= 0 && e2Wc2 <= 0) {
                                this.addLocalMinPoly(e1, e2, pt);
                                break;
                            }
                            break;
                        }
                        case DIFFERENCE: {
                            if ((e1.polyTyp == Clipper.PolyType.CLIP && e1Wc2 > 0 && e2Wc2 > 0) || (e1.polyTyp == Clipper.PolyType.SUBJECT && e1Wc2 <= 0 && e2Wc2 <= 0)) {
                                this.addLocalMinPoly(e1, e2, pt);
                                break;
                            }
                            break;
                        }
                        case XOR: {
                            this.addLocalMinPoly(e1, e2, pt);
                            break;
                        }
                    }
                }
                else {
                    Edge.swapSides(e1, e2);
                }
            }
            return;
        }
        if (e1.windDelta == 0 && e2.windDelta == 0) {
            return;
        }
        if (e1.polyTyp == e2.polyTyp && e1.windDelta != e2.windDelta && this.clipType == Clipper.ClipType.UNION) {
            if (e1.windDelta == 0) {
                if (e2Contributing) {
                    this.addOutPt(e1, pt);
                    if (e1Contributing) {
                        e1.outIdx = -1;
                    }
                }
            }
            else if (e1Contributing) {
                this.addOutPt(e2, pt);
                if (e2Contributing) {
                    e2.outIdx = -1;
                }
            }
        }
        else if (e1.polyTyp != e2.polyTyp) {
            if (e1.windDelta == 0 && Math.abs(e2.windCnt) == 1 && (this.clipType != Clipper.ClipType.UNION || e2.windCnt2 == 0)) {
                this.addOutPt(e1, pt);
                if (e1Contributing) {
                    e1.outIdx = -1;
                }
            }
            else if (e2.windDelta == 0 && Math.abs(e1.windCnt) == 1 && (this.clipType != Clipper.ClipType.UNION || e1.windCnt2 == 0)) {
                this.addOutPt(e2, pt);
                if (e2Contributing) {
                    e2.outIdx = -1;
                }
            }
        }
    }
    
    private void intersectPoint(final Edge edge1, final Edge edge2, final Point.LongPoint[] ipV) {
        final int n = 0;
        final Point.LongPoint longPoint = new Point.LongPoint();
        ipV[n] = longPoint;
        final Point.LongPoint ip = longPoint;
        if (edge1.deltaX == edge2.deltaX) {
            ip.setY(edge1.getCurrent().getY());
            ip.setX(Edge.topX(edge1, ip.getY()));
            return;
        }
        if (edge1.getDelta().getX() == 0L) {
            ip.setX(edge1.getBot().getX());
            if (edge2.isHorizontal()) {
                ip.setY(edge2.getBot().getY());
            }
            else {
                final double b2 = edge2.getBot().getY() - edge2.getBot().getX() / edge2.deltaX;
                ip.setY(Math.round(ip.getX() / edge2.deltaX + b2));
            }
        }
        else if (edge2.getDelta().getX() == 0L) {
            ip.setX(edge2.getBot().getX());
            if (edge1.isHorizontal()) {
                ip.setY(edge1.getBot().getY());
            }
            else {
                final double b3 = edge1.getBot().getY() - edge1.getBot().getX() / edge1.deltaX;
                ip.setY(Math.round(ip.getX() / edge1.deltaX + b3));
            }
        }
        else {
            final double b3 = edge1.getBot().getX() - edge1.getBot().getY() * edge1.deltaX;
            final double b2 = edge2.getBot().getX() - edge2.getBot().getY() * edge2.deltaX;
            final double q = (b2 - b3) / (edge1.deltaX - edge2.deltaX);
            ip.setY(Math.round(q));
            if (Math.abs(edge1.deltaX) < Math.abs(edge2.deltaX)) {
                ip.setX(Math.round(edge1.deltaX * q + b3));
            }
            else {
                ip.setX(Math.round(edge2.deltaX * q + b2));
            }
        }
        if (ip.getY() < edge1.getTop().getY() || ip.getY() < edge2.getTop().getY()) {
            if (edge1.getTop().getY() > edge2.getTop().getY()) {
                ip.setY(edge1.getTop().getY());
            }
            else {
                ip.setY(edge2.getTop().getY());
            }
            if (Math.abs(edge1.deltaX) < Math.abs(edge2.deltaX)) {
                ip.setX(Edge.topX(edge1, ip.getY()));
            }
            else {
                ip.setX(Edge.topX(edge2, ip.getY()));
            }
        }
        if (ip.getY() > edge1.getCurrent().getY()) {
            ip.setY(edge1.getCurrent().getY());
            if (Math.abs(edge1.deltaX) > Math.abs(edge2.deltaX)) {
                ip.setX(Edge.topX(edge2, ip.getY()));
            }
            else {
                ip.setX(Edge.topX(edge1, ip.getY()));
            }
        }
    }
    
    private void joinCommonEdges() {
        for (int i = 0; i < this.joins.size(); ++i) {
            final Path.Join join = this.joins.get(i);
            final Path.OutRec outRec1 = this.getOutRec(join.outPt1.idx);
            Path.OutRec outRec2 = this.getOutRec(join.outPt2.idx);
            if (outRec1.getPoints() != null) {
                if (outRec2.getPoints() != null) {
                    if (!outRec1.isOpen) {
                        if (!outRec2.isOpen) {
                            Path.OutRec holeStateRec;
                            if (outRec1 == outRec2) {
                                holeStateRec = outRec1;
                            }
                            else if (isParam1RightOfParam2(outRec1, outRec2)) {
                                holeStateRec = outRec2;
                            }
                            else if (isParam1RightOfParam2(outRec2, outRec1)) {
                                holeStateRec = outRec1;
                            }
                            else {
                                holeStateRec = Path.OutPt.getLowerMostRec(outRec1, outRec2);
                            }
                            if (this.joinPoints(join, outRec1, outRec2)) {
                                if (outRec1 == outRec2) {
                                    outRec1.setPoints(join.outPt1);
                                    outRec1.bottomPt = null;
                                    outRec2 = this.createOutRec();
                                    outRec2.setPoints(join.outPt2);
                                    this.updateOutPtIdxs(outRec2);
                                    if (this.usingPolyTree) {
                                        for (int j = 0; j < this.polyOuts.size() - 1; ++j) {
                                            final Path.OutRec oRec = this.polyOuts.get(j);
                                            if (oRec.getPoints() != null && ClipperBase.parseFirstLeft(oRec.firstLeft) == outRec1) {
                                                if (oRec.isHole != outRec1.isHole) {
                                                    if (poly2ContainsPoly1(oRec.getPoints(), join.outPt2)) {
                                                        oRec.firstLeft = outRec2;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (poly2ContainsPoly1(outRec2.getPoints(), outRec1.getPoints())) {
                                        outRec2.isHole = !outRec1.isHole;
                                        outRec2.firstLeft = outRec1;
                                        if (this.usingPolyTree) {
                                            this.fixupFirstLefts2(outRec2, outRec1);
                                        }
                                        if ((outRec2.isHole ^ this.reverseSolution) == outRec2.area() > 0.0) {
                                            outRec2.getPoints().reversePolyPtLinks();
                                        }
                                    }
                                    else if (poly2ContainsPoly1(outRec1.getPoints(), outRec2.getPoints())) {
                                        outRec2.isHole = outRec1.isHole;
                                        outRec1.isHole = !outRec2.isHole;
                                        outRec2.firstLeft = outRec1.firstLeft;
                                        outRec1.firstLeft = outRec2;
                                        if (this.usingPolyTree) {
                                            this.fixupFirstLefts2(outRec1, outRec2);
                                        }
                                        if ((outRec1.isHole ^ this.reverseSolution) == outRec1.area() > 0.0) {
                                            outRec1.getPoints().reversePolyPtLinks();
                                        }
                                    }
                                    else {
                                        outRec2.isHole = outRec1.isHole;
                                        outRec2.firstLeft = outRec1.firstLeft;
                                        if (this.usingPolyTree) {
                                            this.fixupFirstLefts1(outRec1, outRec2);
                                        }
                                    }
                                }
                                else {
                                    outRec2.setPoints(null);
                                    outRec2.bottomPt = null;
                                    outRec2.Idx = outRec1.Idx;
                                    outRec1.isHole = holeStateRec.isHole;
                                    if (holeStateRec == outRec2) {
                                        outRec1.firstLeft = outRec2.firstLeft;
                                    }
                                    outRec2.firstLeft = outRec1;
                                    if (this.usingPolyTree) {
                                        this.fixupFirstLefts2(outRec2, outRec1);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private long popScanbeam() {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "popBeam");
        final long y = this.scanbeam.y;
        this.scanbeam = this.scanbeam.next;
        return y;
    }
    
    private void processEdgesAtTopOfScanbeam(final long topY) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "processEdgesAtTopOfScanbeam");
        Edge e = this.activeEdges;
        while (e != null) {
            boolean IsMaximaEdge = e.isMaxima((double)topY);
            if (IsMaximaEdge) {
                final Edge eMaxPair = e.getMaximaPair();
                IsMaximaEdge = (eMaxPair == null || !eMaxPair.isHorizontal());
            }
            if (IsMaximaEdge) {
                if (this.strictlySimple) {
                    this.InsertMaxima(e.getTop().getX());
                }
                final Edge ePrev = e.prevInAEL;
                this.doMaxima(e);
                if (ePrev == null) {
                    e = this.activeEdges;
                }
                else {
                    e = ePrev.nextInAEL;
                }
            }
            else {
                if (e.isIntermediate((double)topY) && e.nextInLML.isHorizontal()) {
                    final Edge[] t = { e };
                    this.updateEdgeIntoAEL(t);
                    e = t[0];
                    if (e.outIdx >= 0) {
                        this.addOutPt(e, e.getBot());
                    }
                    this.addEdgeToSEL(e);
                }
                else {
                    e.getCurrent().setX(Edge.topX(e, topY));
                    e.getCurrent().setY(topY);
                }
                if (this.strictlySimple) {
                    final Edge ePrev = e.prevInAEL;
                    if (e.outIdx >= 0 && e.windDelta != 0 && ePrev != null && ePrev.outIdx >= 0 && ePrev.getCurrent().getX() == e.getCurrent().getX() && ePrev.windDelta != 0) {
                        final Point.LongPoint ip = new Point.LongPoint(e.getCurrent());
                        this.setZ(ip, ePrev, e);
                        final Path.OutPt op = this.addOutPt(ePrev, ip);
                        final Path.OutPt op2 = this.addOutPt(e, ip);
                        this.addJoin(op, op2, ip);
                    }
                }
                e = e.nextInAEL;
            }
        }
        this.processHorizontals();
        this.maxima = null;
        for (e = this.activeEdges; e != null; e = e.nextInAEL) {
            if (e.isIntermediate((double)topY)) {
                Path.OutPt op3 = null;
                if (e.outIdx >= 0) {
                    op3 = this.addOutPt(e, e.getTop());
                }
                final Edge[] t = { e };
                this.updateEdgeIntoAEL(t);
                e = t[0];
                final Edge ePrev2 = e.prevInAEL;
                final Edge eNext = e.nextInAEL;
                if (ePrev2 != null && ePrev2.getCurrent().getX() == e.getBot().getX() && ePrev2.getCurrent().getY() == e.getBot().getY() && op3 != null && ePrev2.outIdx >= 0 && ePrev2.getCurrent().getY() > ePrev2.getTop().getY() && Edge.slopesEqual(e, ePrev2, this.useFullRange) && e.windDelta != 0 && ePrev2.windDelta != 0) {
                    final Path.OutPt op2 = this.addOutPt(ePrev2, e.getBot());
                    this.addJoin(op3, op2, e.getTop());
                }
                else if (eNext != null && eNext.getCurrent().getX() == e.getBot().getX() && eNext.getCurrent().getY() == e.getBot().getY() && op3 != null && eNext.outIdx >= 0 && eNext.getCurrent().getY() > eNext.getTop().getY() && Edge.slopesEqual(e, eNext, this.useFullRange) && e.windDelta != 0 && eNext.windDelta != 0) {
                    final Path.OutPt op2 = this.addOutPt(eNext, e.getBot());
                    this.addJoin(op3, op2, e.getTop());
                }
            }
        }
        DefaultClipper.LOGGER.exiting(DefaultClipper.class.getName(), "processEdgesAtTopOfScanbeam");
    }
    
    private void processHorizontal(Edge horzEdge) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "isHorizontal");
        final Clipper.Direction[] dir = { null };
        final long[] horzLeft = { 0L };
        final long[] horzRight = { 0L };
        final boolean IsOpen = horzEdge.outIdx >= 0 && this.polyOuts.get(horzEdge.outIdx).isOpen;
        getHorzDirection(horzEdge, dir, horzLeft, horzRight);
        Edge eLastHorz = horzEdge;
        Edge eMaxPair = null;
        while (eLastHorz.nextInLML != null && eLastHorz.nextInLML.isHorizontal()) {
            eLastHorz = eLastHorz.nextInLML;
        }
        if (eLastHorz.nextInLML == null) {
            eMaxPair = eLastHorz.getMaximaPair();
        }
        Path.Maxima currMax = this.maxima;
        if (currMax != null) {
            if (dir[0] == Clipper.Direction.LEFT_TO_RIGHT) {
                while (currMax != null && currMax.X <= horzEdge.getBot().getX()) {
                    currMax = currMax.Next;
                }
                if (currMax != null && currMax.X >= eLastHorz.getBot().getX()) {
                    currMax = null;
                }
            }
            else {
                while (currMax.Next != null && currMax.Next.X < horzEdge.getBot().getX()) {
                    currMax = currMax.Next;
                }
                if (currMax.X <= eLastHorz.getTop().getX()) {
                    currMax = null;
                }
            }
        }
        Path.OutPt op1 = null;
        while (true) {
            final boolean IsLastHorz = horzEdge == eLastHorz;
            Edge eNext;
            for (Edge e = horzEdge.getNextInAEL(dir[0]); e != null; e = eNext) {
                if (currMax != null) {
                    if (dir[0] == Clipper.Direction.LEFT_TO_RIGHT) {
                        while (currMax != null && currMax.X < e.getCurrent().getX()) {
                            if (horzEdge.outIdx >= 0 && !IsOpen) {
                                this.addOutPt(horzEdge, new Point.LongPoint(currMax.X, horzEdge.getBot().getY()));
                            }
                            currMax = currMax.Next;
                        }
                    }
                    else {
                        while (currMax != null && currMax.X > e.getCurrent().getX()) {
                            if (horzEdge.outIdx >= 0 && !IsOpen) {
                                this.addOutPt(horzEdge, new Point.LongPoint(currMax.X, horzEdge.getBot().getY()));
                            }
                            currMax = currMax.Prev;
                        }
                    }
                }
                if (dir[0] == Clipper.Direction.LEFT_TO_RIGHT && e.getCurrent().getX() > horzRight[0]) {
                    break;
                }
                if (dir[0] == Clipper.Direction.RIGHT_TO_LEFT && e.getCurrent().getX() < horzLeft[0]) {
                    break;
                }
                if (e.getCurrent().getX() == horzEdge.getTop().getX() && horzEdge.nextInLML != null && e.deltaX < horzEdge.nextInLML.deltaX) {
                    break;
                }
                if (horzEdge.outIdx >= 0 && !IsOpen) {
                    op1 = this.addOutPt(horzEdge, e.getCurrent());
                    for (Edge eNextHorz = this.sortedEdges; eNextHorz != null; eNextHorz = eNextHorz.nextInSEL) {
                        if (eNextHorz.outIdx >= 0 && this.doHorzSegmentsOverlap(horzEdge.getBot().getX(), horzEdge.getTop().getX(), eNextHorz.getBot().getX(), eNextHorz.getTop().getX())) {
                            final Path.OutPt op2 = this.GetLastOutPt(eNextHorz);
                            this.addJoin(op2, op1, eNextHorz.getTop());
                        }
                    }
                    this.addGhostJoin(op1, horzEdge.getBot());
                }
                if (e == eMaxPair && IsLastHorz) {
                    if (horzEdge.outIdx >= 0) {
                        this.addLocalMaxPoly(horzEdge, eMaxPair, horzEdge.getTop());
                    }
                    this.deleteFromAEL(horzEdge);
                    this.deleteFromAEL(eMaxPair);
                    return;
                }
                if (dir[0] == Clipper.Direction.LEFT_TO_RIGHT) {
                    final Point.LongPoint Pt = new Point.LongPoint(e.getCurrent().getX(), horzEdge.getCurrent().getY());
                    this.intersectEdges(horzEdge, e, Pt);
                }
                else {
                    final Point.LongPoint Pt = new Point.LongPoint(e.getCurrent().getX(), horzEdge.getCurrent().getY());
                    this.intersectEdges(e, horzEdge, Pt);
                }
                eNext = e.getNextInAEL(dir[0]);
                this.swapPositionsInAEL(horzEdge, e);
            }
            if (horzEdge.nextInLML == null || !horzEdge.nextInLML.isHorizontal()) {
                if (horzEdge.outIdx >= 0 && op1 == null) {
                    op1 = this.GetLastOutPt(horzEdge);
                    for (Edge eNextHorz2 = this.sortedEdges; eNextHorz2 != null; eNextHorz2 = eNextHorz2.nextInSEL) {
                        if (eNextHorz2.outIdx >= 0 && this.doHorzSegmentsOverlap(horzEdge.getBot().getX(), horzEdge.getTop().getX(), eNextHorz2.getBot().getX(), eNextHorz2.getTop().getX())) {
                            final Path.OutPt op3 = this.GetLastOutPt(eNextHorz2);
                            this.addJoin(op3, op1, eNextHorz2.getTop());
                        }
                    }
                    this.addGhostJoin(op1, horzEdge.getTop());
                }
                if (horzEdge.nextInLML != null) {
                    if (horzEdge.outIdx >= 0) {
                        op1 = this.addOutPt(horzEdge, horzEdge.getTop());
                        final Edge[] t = { horzEdge };
                        this.updateEdgeIntoAEL(t);
                        horzEdge = t[0];
                        if (horzEdge.windDelta == 0) {
                            return;
                        }
                        final Edge ePrev = horzEdge.prevInAEL;
                        eNext = horzEdge.nextInAEL;
                        if (ePrev != null && ePrev.getCurrent().getX() == horzEdge.getBot().getX() && ePrev.getCurrent().getY() == horzEdge.getBot().getY() && ePrev.windDelta != 0 && ePrev.outIdx >= 0 && ePrev.getCurrent().getY() > ePrev.getTop().getY() && Edge.slopesEqual(horzEdge, ePrev, this.useFullRange)) {
                            final Path.OutPt op2 = this.addOutPt(ePrev, horzEdge.getBot());
                            this.addJoin(op1, op2, horzEdge.getTop());
                        }
                        else if (eNext != null && eNext.getCurrent().getX() == horzEdge.getBot().getX() && eNext.getCurrent().getY() == horzEdge.getBot().getY() && eNext.windDelta != 0 && eNext.outIdx >= 0 && eNext.getCurrent().getY() > eNext.getTop().getY() && Edge.slopesEqual(horzEdge, eNext, this.useFullRange)) {
                            final Path.OutPt op2 = this.addOutPt(eNext, horzEdge.getBot());
                            this.addJoin(op1, op2, horzEdge.getTop());
                        }
                    }
                    else {
                        final Edge[] t = { horzEdge };
                        this.updateEdgeIntoAEL(t);
                        horzEdge = t[0];
                    }
                }
                else {
                    if (horzEdge.outIdx >= 0) {
                        this.addOutPt(horzEdge, horzEdge.getTop());
                    }
                    this.deleteFromAEL(horzEdge);
                }
                return;
            }
            final Edge[] temp = { horzEdge };
            this.updateEdgeIntoAEL(temp);
            horzEdge = temp[0];
            if (horzEdge.outIdx >= 0) {
                this.addOutPt(horzEdge, horzEdge.getBot());
            }
            getHorzDirection(horzEdge, dir, horzLeft, horzRight);
        }
    }
    
    private void processHorizontals() {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "processHorizontals");
        for (Edge horzEdge = this.sortedEdges; horzEdge != null; horzEdge = this.sortedEdges) {
            this.deleteFromSEL(horzEdge);
            this.processHorizontal(horzEdge);
        }
    }
    
    private boolean processIntersections(final long topY) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "processIntersections");
        if (this.activeEdges == null) {
            return true;
        }
        try {
            this.buildIntersectList(topY);
            if (this.intersectList.size() == 0) {
                return true;
            }
            if (this.intersectList.size() != 1 && !this.fixupIntersectionOrder()) {
                return false;
            }
            this.processIntersectList();
        }
        catch (Exception e) {
            this.sortedEdges = null;
            this.intersectList.clear();
            throw new IllegalStateException("ProcessIntersections error", e);
        }
        this.sortedEdges = null;
        return true;
    }
    
    private void processIntersectList() {
        for (int i = 0; i < this.intersectList.size(); ++i) {
            final IntersectNode iNode = this.intersectList.get(i);
            this.intersectEdges(iNode.edge1, iNode.Edge2, iNode.getPt());
            this.swapPositionsInAEL(iNode.edge1, iNode.Edge2);
        }
        this.intersectList.clear();
    }
    
    @Override
    protected void reset() {
        super.reset();
        this.scanbeam = null;
        this.maxima = null;
        this.activeEdges = null;
        this.sortedEdges = null;
        for (LocalMinima lm = this.minimaList; lm != null; lm = lm.next) {
            this.insertScanbeam(lm.y);
        }
    }
    
    private void setHoleState(final Edge e, final Path.OutRec outRec) {
        boolean isHole = false;
        for (Edge e2 = e.prevInAEL; e2 != null; e2 = e2.prevInAEL) {
            if (e2.outIdx >= 0 && e2.windDelta != 0) {
                isHole = !isHole;
                if (outRec.firstLeft == null) {
                    outRec.firstLeft = this.polyOuts.get(e2.outIdx);
                }
            }
        }
        if (isHole) {
            outRec.isHole = true;
        }
    }
    
    private void setZ(final Point.LongPoint pt, final Edge e1, final Edge e2) {
        if (pt.getZ() != 0L || this.zFillFunction == null) {
            return;
        }
        if (pt.equals(e1.getBot())) {
            pt.setZ(e1.getBot().getZ());
        }
        else if (pt.equals(e1.getTop())) {
            pt.setZ(e1.getTop().getZ());
        }
        else if (pt.equals(e2.getBot())) {
            pt.setZ(e2.getBot().getZ());
        }
        else if (pt.equals(e2.getTop())) {
            pt.setZ(e2.getTop().getZ());
        }
        else {
            this.zFillFunction.zFill(e1.getBot(), e1.getTop(), e2.getBot(), e2.getTop(), pt);
        }
    }
    
    private void swapPositionsInAEL(final Edge edge1, final Edge edge2) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "swapPositionsInAEL");
        if (edge1.nextInAEL == edge1.prevInAEL || edge2.nextInAEL == edge2.prevInAEL) {
            return;
        }
        if (edge1.nextInAEL == edge2) {
            final Edge next = edge2.nextInAEL;
            if (next != null) {
                next.prevInAEL = edge1;
            }
            final Edge prev = edge1.prevInAEL;
            if (prev != null) {
                prev.nextInAEL = edge2;
            }
            edge2.prevInAEL = prev;
            edge2.nextInAEL = edge1;
            edge1.prevInAEL = edge2;
            edge1.nextInAEL = next;
        }
        else if (edge2.nextInAEL == edge1) {
            final Edge next = edge1.nextInAEL;
            if (next != null) {
                next.prevInAEL = edge2;
            }
            final Edge prev = edge2.prevInAEL;
            if (prev != null) {
                prev.nextInAEL = edge1;
            }
            edge1.prevInAEL = prev;
            edge1.nextInAEL = edge2;
            edge2.prevInAEL = edge1;
            edge2.nextInAEL = next;
        }
        else {
            final Edge next = edge1.nextInAEL;
            final Edge prev = edge1.prevInAEL;
            edge1.nextInAEL = edge2.nextInAEL;
            if (edge1.nextInAEL != null) {
                edge1.nextInAEL.prevInAEL = edge1;
            }
            edge1.prevInAEL = edge2.prevInAEL;
            if (edge1.prevInAEL != null) {
                edge1.prevInAEL.nextInAEL = edge1;
            }
            edge2.nextInAEL = next;
            if (edge2.nextInAEL != null) {
                edge2.nextInAEL.prevInAEL = edge2;
            }
            edge2.prevInAEL = prev;
            if (edge2.prevInAEL != null) {
                edge2.prevInAEL.nextInAEL = edge2;
            }
        }
        if (edge1.prevInAEL == null) {
            this.activeEdges = edge1;
        }
        else if (edge2.prevInAEL == null) {
            this.activeEdges = edge2;
        }
        DefaultClipper.LOGGER.exiting(DefaultClipper.class.getName(), "swapPositionsInAEL");
    }
    
    private void swapPositionsInSEL(final Edge edge1, final Edge edge2) {
        if (edge1.nextInSEL == null && edge1.prevInSEL == null) {
            return;
        }
        if (edge2.nextInSEL == null && edge2.prevInSEL == null) {
            return;
        }
        if (edge1.nextInSEL == edge2) {
            final Edge next = edge2.nextInSEL;
            if (next != null) {
                next.prevInSEL = edge1;
            }
            final Edge prev = edge1.prevInSEL;
            if (prev != null) {
                prev.nextInSEL = edge2;
            }
            edge2.prevInSEL = prev;
            edge2.nextInSEL = edge1;
            edge1.prevInSEL = edge2;
            edge1.nextInSEL = next;
        }
        else if (edge2.nextInSEL == edge1) {
            final Edge next = edge1.nextInSEL;
            if (next != null) {
                next.prevInSEL = edge2;
            }
            final Edge prev = edge2.prevInSEL;
            if (prev != null) {
                prev.nextInSEL = edge1;
            }
            edge1.prevInSEL = prev;
            edge1.nextInSEL = edge2;
            edge2.prevInSEL = edge1;
            edge2.nextInSEL = next;
        }
        else {
            final Edge next = edge1.nextInSEL;
            final Edge prev = edge1.prevInSEL;
            edge1.nextInSEL = edge2.nextInSEL;
            if (edge1.nextInSEL != null) {
                edge1.nextInSEL.prevInSEL = edge1;
            }
            edge1.prevInSEL = edge2.prevInSEL;
            if (edge1.prevInSEL != null) {
                edge1.prevInSEL.nextInSEL = edge1;
            }
            edge2.nextInSEL = next;
            if (edge2.nextInSEL != null) {
                edge2.nextInSEL.prevInSEL = edge2;
            }
            edge2.prevInSEL = prev;
            if (edge2.prevInSEL != null) {
                edge2.prevInSEL.nextInSEL = edge2;
            }
        }
        if (edge1.prevInSEL == null) {
            this.sortedEdges = edge1;
        }
        else if (edge2.prevInSEL == null) {
            this.sortedEdges = edge2;
        }
    }
    
    private void updateEdgeIntoAEL(final Edge[] eV) {
        Edge e = eV[0];
        if (e.nextInLML == null) {
            throw new IllegalStateException("UpdateEdgeIntoAEL: invalid call");
        }
        final Edge AelPrev = e.prevInAEL;
        final Edge AelNext = e.nextInAEL;
        e.nextInLML.outIdx = e.outIdx;
        if (AelPrev != null) {
            AelPrev.nextInAEL = e.nextInLML;
        }
        else {
            this.activeEdges = e.nextInLML;
        }
        if (AelNext != null) {
            AelNext.prevInAEL = e.nextInLML;
        }
        e.nextInLML.side = e.side;
        e.nextInLML.windDelta = e.windDelta;
        e.nextInLML.windCnt = e.windCnt;
        e.nextInLML.windCnt2 = e.windCnt2;
        e = (eV[0] = e.nextInLML);
        e.setCurrent(e.getBot());
        e.prevInAEL = AelPrev;
        e.nextInAEL = AelNext;
        if (!e.isHorizontal()) {
            this.insertScanbeam(e.getTop().getY());
        }
    }
    
    private void updateOutPtIdxs(final Path.OutRec outrec) {
        Path.OutPt op = outrec.getPoints();
        do {
            op.idx = outrec.Idx;
            op = op.prev;
        } while (op != outrec.getPoints());
    }
    
    private void updateWindingCount(final Edge edge) {
        DefaultClipper.LOGGER.entering(DefaultClipper.class.getName(), "updateWindingCount");
        Edge e;
        for (e = edge.prevInAEL; e != null && (e.polyTyp != edge.polyTyp || e.windDelta == 0); e = e.prevInAEL) {}
        if (e == null) {
            edge.windCnt = ((edge.windDelta == 0) ? 1 : edge.windDelta);
            edge.windCnt2 = 0;
            e = this.activeEdges;
        }
        else if (edge.windDelta == 0 && this.clipType != Clipper.ClipType.UNION) {
            edge.windCnt = 1;
            edge.windCnt2 = e.windCnt2;
            e = e.nextInAEL;
        }
        else if (edge.isEvenOddFillType(this.clipFillType, this.subjFillType)) {
            if (edge.windDelta == 0) {
                boolean Inside = true;
                for (Edge e2 = e.prevInAEL; e2 != null; e2 = e2.prevInAEL) {
                    if (e2.polyTyp == e.polyTyp && e2.windDelta != 0) {
                        Inside = !Inside;
                    }
                }
                edge.windCnt = (Inside ? 0 : 1);
            }
            else {
                edge.windCnt = edge.windDelta;
            }
            edge.windCnt2 = e.windCnt2;
            e = e.nextInAEL;
        }
        else {
            if (e.windCnt * e.windDelta < 0) {
                if (Math.abs(e.windCnt) > 1) {
                    if (e.windDelta * edge.windDelta < 0) {
                        edge.windCnt = e.windCnt;
                    }
                    else {
                        edge.windCnt = e.windCnt + edge.windDelta;
                    }
                }
                else {
                    edge.windCnt = ((edge.windDelta == 0) ? 1 : edge.windDelta);
                }
            }
            else if (edge.windDelta == 0) {
                edge.windCnt = ((e.windCnt < 0) ? (e.windCnt - 1) : (e.windCnt + 1));
            }
            else if (e.windDelta * edge.windDelta < 0) {
                edge.windCnt = e.windCnt;
            }
            else {
                edge.windCnt = e.windCnt + edge.windDelta;
            }
            edge.windCnt2 = e.windCnt2;
            e = e.nextInAEL;
        }
        if (edge.isEvenOddAltFillType(this.clipFillType, this.subjFillType)) {
            while (e != edge) {
                if (e.windDelta != 0) {
                    edge.windCnt2 = ((edge.windCnt2 == 0) ? 1 : 0);
                }
                e = e.nextInAEL;
            }
        }
        else {
            while (e != edge) {
                edge.windCnt2 += e.windDelta;
                e = e.nextInAEL;
            }
        }
    }
    
    static {
        LOGGER = Logger.getLogger(DefaultClipper.class.getName());
    }
    
    private class IntersectNode
    {
        Edge edge1;
        Edge Edge2;
        private Point.LongPoint pt;
        
        public Point.LongPoint getPt() {
            return this.pt;
        }
        
        public void setPt(final Point.LongPoint pt) {
            this.pt = pt;
        }
    }
}
