// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser.clipper;

import java.util.Collections;
import java.util.ArrayList;
import java.util.List;

public class PolyNode
{
    private PolyNode parent;
    private final Path polygon;
    private int index;
    private Clipper.JoinType joinType;
    private Clipper.EndType endType;
    protected final List<PolyNode> childs;
    private boolean isOpen;
    
    public PolyNode() {
        this.polygon = new Path();
        this.childs = new ArrayList<PolyNode>();
    }
    
    public void addChild(final PolyNode child) {
        final int cnt = this.childs.size();
        this.childs.add(child);
        child.parent = this;
        child.index = cnt;
    }
    
    public int getChildCount() {
        return this.childs.size();
    }
    
    public List<PolyNode> getChilds() {
        return Collections.unmodifiableList((List<? extends PolyNode>)this.childs);
    }
    
    public List<Point.LongPoint> getContour() {
        return this.polygon;
    }
    
    public Clipper.EndType getEndType() {
        return this.endType;
    }
    
    public Clipper.JoinType getJoinType() {
        return this.joinType;
    }
    
    public PolyNode getNext() {
        if (!this.childs.isEmpty()) {
            return this.childs.get(0);
        }
        return this.getNextSiblingUp();
    }
    
    private PolyNode getNextSiblingUp() {
        if (this.parent == null) {
            return null;
        }
        if (this.index == this.parent.childs.size() - 1) {
            return this.parent.getNextSiblingUp();
        }
        return this.parent.childs.get(this.index + 1);
    }
    
    public PolyNode getParent() {
        return this.parent;
    }
    
    public Path getPolygon() {
        return this.polygon;
    }
    
    public boolean isHole() {
        return this.isHoleNode();
    }
    
    private boolean isHoleNode() {
        boolean result = true;
        for (PolyNode node = this.parent; node != null; node = node.parent) {
            result = !result;
        }
        return result;
    }
    
    public boolean isOpen() {
        return this.isOpen;
    }
    
    public void setEndType(final Clipper.EndType value) {
        this.endType = value;
    }
    
    public void setJoinType(final Clipper.JoinType value) {
        this.joinType = value;
    }
    
    public void setOpen(final boolean isOpen) {
        this.isOpen = isOpen;
    }
    
    public void setParent(final PolyNode n) {
        this.parent = n;
    }
    
    enum NodeType
    {
        ANY, 
        OPEN, 
        CLOSED;
    }
}
