// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser.clipper;

import java.util.Comparator;
import java.math.BigInteger;

public abstract class Point<T extends Number>
{
    private static final NumberComparator NUMBER_COMPARATOR;
    protected T x;
    protected T y;
    protected T z;
    
    static boolean arePointsClose(final Point<? extends Number> pt1, final Point<? extends Number> pt2, final double distSqrd) {
        final double dx = ((Number)pt1.x).doubleValue() - ((Number)pt2.x).doubleValue();
        final double dy = ((Number)pt1.y).doubleValue() - ((Number)pt2.y).doubleValue();
        return dx * dx + dy * dy <= distSqrd;
    }
    
    static double distanceFromLineSqrd(final Point<? extends Number> pt, final Point<? extends Number> ln1, final Point<? extends Number> ln2) {
        final double A = ((Number)ln1.y).doubleValue() - ((Number)ln2.y).doubleValue();
        final double B = ((Number)ln2.x).doubleValue() - ((Number)ln1.x).doubleValue();
        double C = A * ((Number)ln1.x).doubleValue() + B * ((Number)ln1.y).doubleValue();
        C = A * ((Number)pt.x).doubleValue() + B * ((Number)pt.y).doubleValue() - C;
        return C * C / (A * A + B * B);
    }
    
    static DoublePoint getUnitNormal(final LongPoint pt1, final LongPoint pt2) {
        double dx = (double)((long)pt2.x - (long)pt1.x);
        double dy = (double)((long)pt2.y - (long)pt1.y);
        if (dx == 0.0 && dy == 0.0) {
            return new DoublePoint();
        }
        final double f = 1.0 / Math.sqrt(dx * dx + dy * dy);
        dx *= f;
        dy *= f;
        return new DoublePoint(dy, -dx);
    }
    
    protected static boolean isPt2BetweenPt1AndPt3(final LongPoint pt1, final LongPoint pt2, final LongPoint pt3) {
        if (pt1.equals(pt3) || pt1.equals(pt2) || pt3.equals(pt2)) {
            return false;
        }
        if (pt1.x != pt3.x) {
            return (long)pt2.x > (long)pt1.x == (long)pt2.x < (long)pt3.x;
        }
        return (long)pt2.y > (long)pt1.y == (long)pt2.y < (long)pt3.y;
    }
    
    protected static boolean slopesEqual(final LongPoint pt1, final LongPoint pt2, final LongPoint pt3, final boolean useFullRange) {
        if (useFullRange) {
            return BigInteger.valueOf(pt1.getY() - pt2.getY()).multiply(BigInteger.valueOf(pt2.getX() - pt3.getX())).equals(BigInteger.valueOf(pt1.getX() - pt2.getX()).multiply(BigInteger.valueOf(pt2.getY() - pt3.getY())));
        }
        return (pt1.getY() - pt2.getY()) * (pt2.getX() - pt3.getX()) - (pt1.getX() - pt2.getX()) * (pt2.getY() - pt3.getY()) == 0L;
    }
    
    protected static boolean slopesEqual(final LongPoint pt1, final LongPoint pt2, final LongPoint pt3, final LongPoint pt4, final boolean useFullRange) {
        if (useFullRange) {
            return BigInteger.valueOf(pt1.getY() - pt2.getY()).multiply(BigInteger.valueOf(pt3.getX() - pt4.getX())).equals(BigInteger.valueOf(pt1.getX() - pt2.getX()).multiply(BigInteger.valueOf(pt3.getY() - pt4.getY())));
        }
        return (pt1.getY() - pt2.getY()) * (pt3.getX() - pt4.getX()) - (pt1.getX() - pt2.getX()) * (pt3.getY() - pt4.getY()) == 0L;
    }
    
    static boolean slopesNearCollinear(final LongPoint pt1, final LongPoint pt2, final LongPoint pt3, final double distSqrd) {
        if (Math.abs((long)pt1.x - (long)pt2.x) > Math.abs((long)pt1.y - (long)pt2.y)) {
            if ((long)pt1.x > (long)pt2.x == (long)pt1.x < (long)pt3.x) {
                return distanceFromLineSqrd(pt1, pt2, pt3) < distSqrd;
            }
            if ((long)pt2.x > (long)pt1.x == (long)pt2.x < (long)pt3.x) {
                return distanceFromLineSqrd(pt2, pt1, pt3) < distSqrd;
            }
            return distanceFromLineSqrd(pt3, pt1, pt2) < distSqrd;
        }
        else {
            if ((long)pt1.y > (long)pt2.y == (long)pt1.y < (long)pt3.y) {
                return distanceFromLineSqrd(pt1, pt2, pt3) < distSqrd;
            }
            if ((long)pt2.y > (long)pt1.y == (long)pt2.y < (long)pt3.y) {
                return distanceFromLineSqrd(pt2, pt1, pt3) < distSqrd;
            }
            return distanceFromLineSqrd(pt3, pt1, pt2) < distSqrd;
        }
    }
    
    protected Point(final Point<T> pt) {
        this((Number)pt.x, (Number)pt.y, (Number)pt.z);
    }
    
    protected Point(final T x, final T y, final T z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof Point) {
            final Point<?> a = (Point<?>)obj;
            return Point.NUMBER_COMPARATOR.compare((Number)this.x, (Number)a.x) == 0 && Point.NUMBER_COMPARATOR.compare((Number)this.y, (Number)a.y) == 0;
        }
        return false;
    }
    
    public void set(final Point<T> other) {
        this.x = other.x;
        this.y = other.y;
        this.z = other.z;
    }
    
    public void setX(final T x) {
        this.x = x;
    }
    
    public void setY(final T y) {
        this.y = y;
    }
    
    public void setZ(final T z) {
        this.z = z;
    }
    
    @Override
    public String toString() {
        return "Point [x=" + this.x + ", y=" + this.y + ", z=" + this.z + "]";
    }
    
    static {
        NUMBER_COMPARATOR = new NumberComparator();
    }
    
    public static class DoublePoint extends Point<Double>
    {
        public DoublePoint() {
            this(0.0, 0.0);
        }
        
        public DoublePoint(final double x, final double y) {
            this(x, y, 0.0);
        }
        
        public DoublePoint(final double x, final double y, final double z) {
            super(x, y, z);
        }
        
        public DoublePoint(final DoublePoint other) {
            super((Point<Number>)other);
        }
        
        public double getX() {
            return (double)this.x;
        }
        
        public double getY() {
            return (double)this.y;
        }
        
        public double getZ() {
            return (double)this.z;
        }
    }
    
    public static class LongPoint extends Point<Long>
    {
        public static double getDeltaX(final LongPoint pt1, final LongPoint pt2) {
            if (pt1.getY() == pt2.getY()) {
                return -3.4E38;
            }
            return (pt2.getX() - pt1.getX()) / (double)(pt2.getY() - pt1.getY());
        }
        
        public LongPoint() {
            this(0L, 0L);
        }
        
        public LongPoint(final long x, final long y) {
            this(x, y, 0L);
        }
        
        public LongPoint(final double x, final double y) {
            this((long)x, (long)y);
        }
        
        public LongPoint(final long x, final long y, final long z) {
            super(x, y, z);
        }
        
        public LongPoint(final LongPoint other) {
            super((Point<Number>)other);
        }
        
        public long getX() {
            return (long)this.x;
        }
        
        public long getY() {
            return (long)this.y;
        }
        
        public long getZ() {
            return (long)this.z;
        }
    }
    
    private static class NumberComparator<T extends java.lang.Number> implements Comparator<T>
    {
        @Override
        public int compare(final T a, final T b) throws ClassCastException {
            return ((Comparable)a).compareTo(b);
        }
    }
}
