// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser.clipper;

import java.util.Iterator;
import java.util.ArrayList;

public class Paths extends ArrayList<Path>
{
    private static final long serialVersionUID = 1910552127810480852L;
    
    public static Paths closedPathsFromPolyTree(final PolyTree polytree) {
        final Paths result = new Paths();
        result.addPolyNode(polytree, PolyNode.NodeType.CLOSED);
        return result;
    }
    
    public static Paths makePolyTreeToPaths(final PolyTree polytree) {
        final Paths result = new Paths();
        result.addPolyNode(polytree, PolyNode.NodeType.ANY);
        return result;
    }
    
    public static Paths openPathsFromPolyTree(final PolyTree polytree) {
        final Paths result = new Paths();
        for (final PolyNode c : polytree.getChilds()) {
            if (c.isOpen()) {
                result.add(c.getPolygon());
            }
        }
        return result;
    }
    
    public Paths() {
    }
    
    public Paths(final int initialCapacity) {
        super(initialCapacity);
    }
    
    public void addPolyNode(final PolyNode polynode, final PolyNode.NodeType nt) {
        boolean match = true;
        switch (nt) {
            case OPEN: {
                return;
            }
            case CLOSED: {
                match = !polynode.isOpen();
                break;
            }
        }
        if (polynode.getPolygon().size() > 0 && match) {
            this.add(polynode.getPolygon());
        }
        for (final PolyNode pn : polynode.getChilds()) {
            this.addPolyNode(pn, nt);
        }
    }
    
    public Paths cleanPolygons() {
        return this.cleanPolygons(1.415);
    }
    
    public Paths cleanPolygons(final double distance) {
        final Paths result = new Paths(this.size());
        for (int i = 0; i < this.size(); ++i) {
            result.add(this.get(i).cleanPolygon(distance));
        }
        return result;
    }
    
    public LongRect getBounds() {
        int i = 0;
        final int cnt = this.size();
        final LongRect result = new LongRect();
        while (i < cnt && this.get(i).isEmpty()) {
            ++i;
        }
        if (i == cnt) {
            return result;
        }
        result.left = this.get(i).get(0).getX();
        result.right = result.left;
        result.top = this.get(i).get(0).getY();
        result.bottom = result.top;
        while (i < cnt) {
            for (int j = 0; j < this.get(i).size(); ++j) {
                if (this.get(i).get(j).getX() < result.left) {
                    result.left = this.get(i).get(j).getX();
                }
                else if (this.get(i).get(j).getX() > result.right) {
                    result.right = this.get(i).get(j).getX();
                }
                if (this.get(i).get(j).getY() < result.top) {
                    result.top = this.get(i).get(j).getY();
                }
                else if (this.get(i).get(j).getY() > result.bottom) {
                    result.bottom = this.get(i).get(j).getY();
                }
            }
            ++i;
        }
        return result;
    }
    
    public void reversePaths() {
        for (final Path poly : this) {
            poly.reverse();
        }
    }
}
