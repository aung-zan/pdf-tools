// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser.clipper;

public class LongRect
{
    public long left;
    public long top;
    public long right;
    public long bottom;
    
    public LongRect() {
    }
    
    public LongRect(final long l, final long t, final long r, final long b) {
        this.left = l;
        this.top = t;
        this.right = r;
        this.bottom = b;
    }
    
    public LongRect(final LongRect ir) {
        this.left = ir.left;
        this.top = ir.top;
        this.right = ir.right;
        this.bottom = ir.bottom;
    }
}
