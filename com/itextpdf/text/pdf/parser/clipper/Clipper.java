// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser.clipper;

public interface Clipper
{
    public static final int REVERSE_SOLUTION = 1;
    public static final int STRICTLY_SIMPLE = 2;
    public static final int PRESERVE_COLINEAR = 4;
    
    boolean addPath(final Path p0, final PolyType p1, final boolean p2);
    
    boolean addPaths(final Paths p0, final PolyType p1, final boolean p2);
    
    void clear();
    
    boolean execute(final ClipType p0, final Paths p1);
    
    boolean execute(final ClipType p0, final Paths p1, final PolyFillType p2, final PolyFillType p3);
    
    boolean execute(final ClipType p0, final PolyTree p1);
    
    boolean execute(final ClipType p0, final PolyTree p1, final PolyFillType p2, final PolyFillType p3);
    
    public enum ClipType
    {
        INTERSECTION, 
        UNION, 
        DIFFERENCE, 
        XOR;
    }
    
    public enum Direction
    {
        RIGHT_TO_LEFT, 
        LEFT_TO_RIGHT;
    }
    
    public enum EndType
    {
        CLOSED_POLYGON, 
        CLOSED_LINE, 
        OPEN_BUTT, 
        OPEN_SQUARE, 
        OPEN_ROUND;
    }
    
    public enum JoinType
    {
        BEVEL, 
        ROUND, 
        MITER;
    }
    
    public enum PolyFillType
    {
        EVEN_ODD, 
        NON_ZERO, 
        POSITIVE, 
        NEGATIVE;
    }
    
    public enum PolyType
    {
        SUBJECT, 
        CLIP;
    }
    
    public interface ZFillCallback
    {
        void zFill(final Point.LongPoint p0, final Point.LongPoint p1, final Point.LongPoint p2, final Point.LongPoint p3, final Point.LongPoint p4);
    }
}
