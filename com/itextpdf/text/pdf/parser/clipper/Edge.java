// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser.clipper;

import java.math.BigInteger;
import java.util.logging.Logger;

class Edge
{
    private final Point.LongPoint bot;
    private final Point.LongPoint current;
    private final Point.LongPoint top;
    private final Point.LongPoint delta;
    double deltaX;
    Clipper.PolyType polyTyp;
    Side side;
    int windDelta;
    int windCnt;
    int windCnt2;
    int outIdx;
    Edge next;
    Edge prev;
    Edge nextInLML;
    Edge nextInAEL;
    Edge prevInAEL;
    Edge nextInSEL;
    Edge prevInSEL;
    protected static final int SKIP = -2;
    protected static final int UNASSIGNED = -1;
    protected static final double HORIZONTAL = -3.4E38;
    private static final Logger LOGGER;
    
    static boolean doesE2InsertBeforeE1(final Edge e1, final Edge e2) {
        if (e2.current.getX() != e1.current.getX()) {
            return e2.current.getX() < e1.current.getX();
        }
        if (e2.top.getY() > e1.top.getY()) {
            return e2.top.getX() < topX(e1, e2.top.getY());
        }
        return e1.top.getX() > topX(e2, e1.top.getY());
    }
    
    static boolean slopesEqual(final Edge e1, final Edge e2, final boolean useFullRange) {
        if (useFullRange) {
            return BigInteger.valueOf(e1.getDelta().getY()).multiply(BigInteger.valueOf(e2.getDelta().getX())).equals(BigInteger.valueOf(e1.getDelta().getX()).multiply(BigInteger.valueOf(e2.getDelta().getY())));
        }
        return e1.getDelta().getY() * e2.getDelta().getX() == e1.getDelta().getX() * e2.getDelta().getY();
    }
    
    static void swapPolyIndexes(final Edge edge1, final Edge edge2) {
        final int outIdx = edge1.outIdx;
        edge1.outIdx = edge2.outIdx;
        edge2.outIdx = outIdx;
    }
    
    static void swapSides(final Edge edge1, final Edge edge2) {
        final Side side = edge1.side;
        edge1.side = edge2.side;
        edge2.side = side;
    }
    
    static long topX(final Edge edge, final long currentY) {
        if (currentY == edge.getTop().getY()) {
            return edge.getTop().getX();
        }
        return edge.getBot().getX() + Math.round(edge.deltaX * (currentY - edge.getBot().getY()));
    }
    
    public Edge() {
        this.delta = new Point.LongPoint();
        this.top = new Point.LongPoint();
        this.bot = new Point.LongPoint();
        this.current = new Point.LongPoint();
    }
    
    public Edge findNextLocMin() {
        Edge e = this;
        while (true) {
            if (!e.bot.equals(e.prev.bot) || e.current.equals(e.top)) {
                e = e.next;
            }
            else {
                if (e.deltaX != -3.4E38 && e.prev.deltaX != -3.4E38) {
                    break;
                }
                while (e.prev.deltaX == -3.4E38) {
                    e = e.prev;
                }
                final Edge e2 = e;
                while (e.deltaX == -3.4E38) {
                    e = e.next;
                }
                if (e.top.getY() == e.prev.bot.getY()) {
                    continue;
                }
                if (e2.prev.bot.getX() < e.bot.getX()) {
                    e = e2;
                    break;
                }
                break;
            }
        }
        return e;
    }
    
    public Point.LongPoint getBot() {
        return this.bot;
    }
    
    public Point.LongPoint getCurrent() {
        return this.current;
    }
    
    public Point.LongPoint getDelta() {
        return this.delta;
    }
    
    public Edge getMaximaPair() {
        Edge result = null;
        if (this.next.top.equals(this.top) && this.next.nextInLML == null) {
            result = this.next;
        }
        else if (this.prev.top.equals(this.top) && this.prev.nextInLML == null) {
            result = this.prev;
        }
        if (result != null && (result.outIdx == -2 || (result.nextInAEL == result.prevInAEL && !result.isHorizontal()))) {
            return null;
        }
        return result;
    }
    
    public Edge getNextInAEL(final Clipper.Direction direction) {
        return (direction == Clipper.Direction.LEFT_TO_RIGHT) ? this.nextInAEL : this.prevInAEL;
    }
    
    public Point.LongPoint getTop() {
        return this.top;
    }
    
    public boolean isContributing(final Clipper.PolyFillType clipFillType, final Clipper.PolyFillType subjFillType, final Clipper.ClipType clipType) {
        Edge.LOGGER.entering(Edge.class.getName(), "isContributing");
        Clipper.PolyFillType pft;
        Clipper.PolyFillType pft2;
        if (this.polyTyp == Clipper.PolyType.SUBJECT) {
            pft = subjFillType;
            pft2 = clipFillType;
        }
        else {
            pft = clipFillType;
            pft2 = subjFillType;
        }
        switch (pft) {
            case EVEN_ODD: {
                if (this.windDelta == 0 && this.windCnt != 1) {
                    return false;
                }
                break;
            }
            case NON_ZERO: {
                if (Math.abs(this.windCnt) != 1) {
                    return false;
                }
                break;
            }
            case POSITIVE: {
                if (this.windCnt != 1) {
                    return false;
                }
                break;
            }
            default: {
                if (this.windCnt != -1) {
                    return false;
                }
                break;
            }
        }
        switch (clipType) {
            case INTERSECTION: {
                switch (pft2) {
                    case EVEN_ODD:
                    case NON_ZERO: {
                        return this.windCnt2 != 0;
                    }
                    case POSITIVE: {
                        return this.windCnt2 > 0;
                    }
                    default: {
                        return this.windCnt2 < 0;
                    }
                }
                break;
            }
            case UNION: {
                switch (pft2) {
                    case EVEN_ODD:
                    case NON_ZERO: {
                        return this.windCnt2 == 0;
                    }
                    case POSITIVE: {
                        return this.windCnt2 <= 0;
                    }
                    default: {
                        return this.windCnt2 >= 0;
                    }
                }
                break;
            }
            case DIFFERENCE: {
                if (this.polyTyp == Clipper.PolyType.SUBJECT) {
                    switch (pft2) {
                        case EVEN_ODD:
                        case NON_ZERO: {
                            return this.windCnt2 == 0;
                        }
                        case POSITIVE: {
                            return this.windCnt2 <= 0;
                        }
                        default: {
                            return this.windCnt2 >= 0;
                        }
                    }
                }
                else {
                    switch (pft2) {
                        case EVEN_ODD:
                        case NON_ZERO: {
                            return this.windCnt2 != 0;
                        }
                        case POSITIVE: {
                            return this.windCnt2 > 0;
                        }
                        default: {
                            return this.windCnt2 < 0;
                        }
                    }
                }
                break;
            }
            case XOR: {
                if (this.windDelta != 0) {
                    return true;
                }
                switch (pft2) {
                    case EVEN_ODD:
                    case NON_ZERO: {
                        return this.windCnt2 == 0;
                    }
                    case POSITIVE: {
                        return this.windCnt2 <= 0;
                    }
                    default: {
                        return this.windCnt2 >= 0;
                    }
                }
                break;
            }
            default: {
                return true;
            }
        }
    }
    
    public boolean isEvenOddAltFillType(final Clipper.PolyFillType clipFillType, final Clipper.PolyFillType subjFillType) {
        if (this.polyTyp == Clipper.PolyType.SUBJECT) {
            return clipFillType == Clipper.PolyFillType.EVEN_ODD;
        }
        return subjFillType == Clipper.PolyFillType.EVEN_ODD;
    }
    
    public boolean isEvenOddFillType(final Clipper.PolyFillType clipFillType, final Clipper.PolyFillType subjFillType) {
        if (this.polyTyp == Clipper.PolyType.SUBJECT) {
            return subjFillType == Clipper.PolyFillType.EVEN_ODD;
        }
        return clipFillType == Clipper.PolyFillType.EVEN_ODD;
    }
    
    public boolean isHorizontal() {
        return this.delta.getY() == 0L;
    }
    
    public boolean isIntermediate(final double y) {
        return this.top.getY() == y && this.nextInLML != null;
    }
    
    public boolean isMaxima(final double Y) {
        return this.top.getY() == Y && this.nextInLML == null;
    }
    
    public void reverseHorizontal() {
        long temp = this.top.getX();
        this.top.setX(this.bot.getX());
        this.bot.setX(temp);
        temp = this.top.getZ();
        this.top.setZ(this.bot.getZ());
        this.bot.setZ(temp);
    }
    
    public void setBot(final Point.LongPoint bot) {
        this.bot.set(bot);
    }
    
    public void setCurrent(final Point.LongPoint current) {
        this.current.set(current);
    }
    
    public void setTop(final Point.LongPoint top) {
        this.top.set(top);
    }
    
    @Override
    public String toString() {
        return "TEdge [Bot=" + this.bot + ", Curr=" + this.current + ", Top=" + this.top + ", Delta=" + this.delta + ", Dx=" + this.deltaX + ", PolyTyp=" + this.polyTyp + ", Side=" + this.side + ", WindDelta=" + this.windDelta + ", WindCnt=" + this.windCnt + ", WindCnt2=" + this.windCnt2 + ", OutIdx=" + this.outIdx + ", Next=" + this.next + ", Prev=" + this.prev + ", NextInLML=" + this.nextInLML + ", NextInAEL=" + this.nextInAEL + ", PrevInAEL=" + this.prevInAEL + ", NextInSEL=" + this.nextInSEL + ", PrevInSEL=" + this.prevInSEL + "]";
    }
    
    public void updateDeltaX() {
        this.delta.setX(this.top.getX() - this.bot.getX());
        this.delta.setY(this.top.getY() - this.bot.getY());
        if (this.delta.getY() == 0L) {
            this.deltaX = -3.4E38;
        }
        else {
            this.deltaX = this.delta.getX() / (double)this.delta.getY();
        }
    }
    
    static {
        LOGGER = Logger.getLogger(Edge.class.getName());
    }
    
    enum Side
    {
        LEFT, 
        RIGHT;
    }
}
