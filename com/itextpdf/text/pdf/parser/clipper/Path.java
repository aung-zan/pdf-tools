// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser.clipper;

import java.util.List;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;

public class Path extends ArrayList<Point.LongPoint>
{
    private static final long serialVersionUID = -7120161578077546673L;
    
    private static OutPt excludeOp(final OutPt op) {
        final OutPt result = op.prev;
        result.next = op.next;
        op.next.prev = result;
        result.idx = 0;
        return result;
    }
    
    public Path() {
    }
    
    public Path(final Point.LongPoint[] points) {
        this();
        for (final Point.LongPoint point : points) {
            this.add(point);
        }
    }
    
    public Path(final int cnt) {
        super(cnt);
    }
    
    public Path(final Collection<? extends Point.LongPoint> c) {
        super(c);
    }
    
    public double area() {
        final int cnt = this.size();
        if (cnt < 3) {
            return 0.0;
        }
        double a = 0.0;
        int i = 0;
        int j = cnt - 1;
        while (i < cnt) {
            a += (this.get(j).getX() + (double)this.get(i).getX()) * (this.get(j).getY() - (double)this.get(i).getY());
            j = i;
            ++i;
        }
        return -a * 0.5;
    }
    
    public Path cleanPolygon() {
        return this.cleanPolygon(1.415);
    }
    
    public Path cleanPolygon(final double distance) {
        int cnt = this.size();
        if (cnt == 0) {
            return new Path();
        }
        OutPt[] outPts = new OutPt[cnt];
        for (int i = 0; i < cnt; ++i) {
            outPts[i] = new OutPt();
        }
        for (int i = 0; i < cnt; ++i) {
            outPts[i].pt = this.get(i);
            outPts[i].next = outPts[(i + 1) % cnt];
            outPts[i].next.prev = outPts[i];
            outPts[i].idx = 0;
        }
        final double distSqrd = distance * distance;
        OutPt op = outPts[0];
        while (op.idx == 0 && op.next != op.prev) {
            if (Point.arePointsClose(op.pt, op.prev.pt, distSqrd)) {
                op = excludeOp(op);
                --cnt;
            }
            else if (Point.arePointsClose(op.prev.pt, op.next.pt, distSqrd)) {
                excludeOp(op.next);
                op = excludeOp(op);
                cnt -= 2;
            }
            else if (Point.slopesNearCollinear(op.prev.pt, op.pt, op.next.pt, distSqrd)) {
                op = excludeOp(op);
                --cnt;
            }
            else {
                op.idx = 1;
                op = op.next;
            }
        }
        if (cnt < 3) {
            cnt = 0;
        }
        final Path result = new Path(cnt);
        for (int j = 0; j < cnt; ++j) {
            result.add(op.pt);
            op = op.next;
        }
        outPts = null;
        return result;
    }
    
    public int isPointInPolygon(final Point.LongPoint pt) {
        int result = 0;
        final int cnt = this.size();
        if (cnt < 3) {
            return 0;
        }
        Point.LongPoint ip = this.get(0);
        for (int i = 1; i <= cnt; ++i) {
            final Point.LongPoint ipNext = (i == cnt) ? this.get(0) : this.get(i);
            if (ipNext.getY() == pt.getY() && (ipNext.getX() == pt.getX() || (ip.getY() == pt.getY() && ipNext.getX() > pt.getX() == ip.getX() < pt.getX()))) {
                return -1;
            }
            if (ip.getY() < pt.getY() != ipNext.getY() < pt.getY()) {
                if (ip.getX() >= pt.getX()) {
                    if (ipNext.getX() > pt.getX()) {
                        result = 1 - result;
                    }
                    else {
                        final double d = (ip.getX() - pt.getX()) * (double)(ipNext.getY() - pt.getY()) - (ipNext.getX() - pt.getX()) * (double)(ip.getY() - pt.getY());
                        if (d == 0.0) {
                            return -1;
                        }
                        if (d > 0.0 == ipNext.getY() > ip.getY()) {
                            result = 1 - result;
                        }
                    }
                }
                else if (ipNext.getX() > pt.getX()) {
                    final double d = (ip.getX() - pt.getX()) * (double)(ipNext.getY() - pt.getY()) - (ipNext.getX() - pt.getX()) * (double)(ip.getY() - pt.getY());
                    if (d == 0.0) {
                        return -1;
                    }
                    if (d > 0.0 == ipNext.getY() > ip.getY()) {
                        result = 1 - result;
                    }
                }
            }
            ip = ipNext;
        }
        return result;
    }
    
    public boolean orientation() {
        return this.area() >= 0.0;
    }
    
    public void reverse() {
        Collections.reverse(this);
    }
    
    public Path TranslatePath(final Point.LongPoint delta) {
        final Path outPath = new Path(this.size());
        for (int i = 0; i < this.size(); ++i) {
            outPath.add(new Point.LongPoint(this.get(i).getX() + delta.getX(), this.get(i).getY() + delta.getY()));
        }
        return outPath;
    }
    
    static class Join
    {
        OutPt outPt1;
        OutPt outPt2;
        private Point.LongPoint offPt;
        
        public Point.LongPoint getOffPt() {
            return this.offPt;
        }
        
        public void setOffPt(final Point.LongPoint offPt) {
            this.offPt = offPt;
        }
    }
    
    static class OutPt
    {
        int idx;
        protected Point.LongPoint pt;
        OutPt next;
        OutPt prev;
        
        public static OutRec getLowerMostRec(final OutRec outRec1, final OutRec outRec2) {
            if (outRec1.bottomPt == null) {
                outRec1.bottomPt = outRec1.pts.getBottomPt();
            }
            if (outRec2.bottomPt == null) {
                outRec2.bottomPt = outRec2.pts.getBottomPt();
            }
            final OutPt bPt1 = outRec1.bottomPt;
            final OutPt bPt2 = outRec2.bottomPt;
            if (bPt1.getPt().getY() > bPt2.getPt().getY()) {
                return outRec1;
            }
            if (bPt1.getPt().getY() < bPt2.getPt().getY()) {
                return outRec2;
            }
            if (bPt1.getPt().getX() < bPt2.getPt().getX()) {
                return outRec1;
            }
            if (bPt1.getPt().getX() > bPt2.getPt().getX()) {
                return outRec2;
            }
            if (bPt1.next == bPt1) {
                return outRec2;
            }
            if (bPt2.next == bPt2) {
                return outRec1;
            }
            if (isFirstBottomPt(bPt1, bPt2)) {
                return outRec1;
            }
            return outRec2;
        }
        
        private static boolean isFirstBottomPt(final OutPt btmPt1, final OutPt btmPt2) {
            OutPt p;
            for (p = btmPt1.prev; p.getPt().equals(btmPt1.getPt()) && !p.equals(btmPt1); p = p.prev) {}
            final double dx1p = Math.abs(Point.LongPoint.getDeltaX(btmPt1.getPt(), p.getPt()));
            for (p = btmPt1.next; p.getPt().equals(btmPt1.getPt()) && !p.equals(btmPt1); p = p.next) {}
            final double dx1n = Math.abs(Point.LongPoint.getDeltaX(btmPt1.getPt(), p.getPt()));
            for (p = btmPt2.prev; p.getPt().equals(btmPt2.getPt()) && !p.equals(btmPt2); p = p.prev) {}
            final double dx2p = Math.abs(Point.LongPoint.getDeltaX(btmPt2.getPt(), p.getPt()));
            for (p = btmPt2.next; p.getPt().equals(btmPt2.getPt()) && p.equals(btmPt2); p = p.next) {}
            final double dx2n = Math.abs(Point.LongPoint.getDeltaX(btmPt2.getPt(), p.getPt()));
            return (dx1p >= dx2p && dx1p >= dx2n) || (dx1n >= dx2p && dx1n >= dx2n);
        }
        
        public OutPt duplicate(final boolean InsertAfter) {
            final OutPt result = new OutPt();
            result.setPt(new Point.LongPoint(this.getPt()));
            result.idx = this.idx;
            if (InsertAfter) {
                result.next = this.next;
                result.prev = this;
                this.next.prev = result;
                this.next = result;
            }
            else {
                result.prev = this.prev;
                result.next = this;
                this.prev.next = result;
                this.prev = result;
            }
            return result;
        }
        
        OutPt getBottomPt() {
            OutPt dups = null;
            OutPt p;
            OutPt pp;
            for (p = this.next, pp = this; p != pp; p = p.next) {
                if (p.getPt().getY() > pp.getPt().getY()) {
                    pp = p;
                    dups = null;
                }
                else if (p.getPt().getY() == pp.getPt().getY() && p.getPt().getX() <= pp.getPt().getX()) {
                    if (p.getPt().getX() < pp.getPt().getX()) {
                        dups = null;
                        pp = p;
                    }
                    else if (p.next != pp && p.prev != pp) {
                        dups = p;
                    }
                }
            }
            if (dups != null) {
                while (dups != p) {
                    if (!isFirstBottomPt(p, dups)) {
                        pp = dups;
                    }
                    for (dups = dups.next; !dups.getPt().equals(pp.getPt()); dups = dups.next) {}
                }
            }
            return pp;
        }
        
        public int getPointCount() {
            int result = 0;
            OutPt p = this;
            do {
                ++result;
                p = p.next;
            } while (p != this && p != null);
            return result;
        }
        
        public Point.LongPoint getPt() {
            return this.pt;
        }
        
        public void reversePolyPtLinks() {
            OutPt pp1 = this;
            do {
                final OutPt pp2 = pp1.next;
                pp1.next = pp1.prev;
                pp1.prev = pp2;
                pp1 = pp2;
            } while (pp1 != this);
        }
        
        public void setPt(final Point.LongPoint pt) {
            this.pt = pt;
        }
    }
    
    protected static class Maxima
    {
        protected long X;
        protected Maxima Next;
        protected Maxima Prev;
    }
    
    static class OutRec
    {
        int Idx;
        boolean isHole;
        boolean isOpen;
        OutRec firstLeft;
        protected OutPt pts;
        OutPt bottomPt;
        PolyNode polyNode;
        
        public double area() {
            OutPt op = this.pts;
            if (op == null) {
                return 0.0;
            }
            double a = 0.0;
            do {
                a += (op.prev.getPt().getX() + op.getPt().getX()) * (double)(op.prev.getPt().getY() - op.getPt().getY());
                op = op.next;
            } while (op != this.pts);
            return a * 0.5;
        }
        
        public void fixHoleLinkage() {
            if (this.firstLeft == null || (this.isHole != this.firstLeft.isHole && this.firstLeft.pts != null)) {
                return;
            }
            OutRec orfl;
            for (orfl = this.firstLeft; orfl != null && (orfl.isHole == this.isHole || orfl.pts == null); orfl = orfl.firstLeft) {}
            this.firstLeft = orfl;
        }
        
        public OutPt getPoints() {
            return this.pts;
        }
        
        public void setPoints(final OutPt pts) {
            this.pts = pts;
        }
    }
}
