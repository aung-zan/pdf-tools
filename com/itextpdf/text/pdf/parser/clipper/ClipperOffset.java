// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser.clipper;

import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public class ClipperOffset
{
    private Paths destPolys;
    private Path srcPoly;
    private Path destPoly;
    private final List<Point.DoublePoint> normals;
    private double delta;
    private double inA;
    private double sin;
    private double cos;
    private double miterLim;
    private double stepsPerRad;
    private Point.LongPoint lowest;
    private final PolyNode polyNodes;
    private final double arcTolerance;
    private final double miterLimit;
    private static final double TWO_PI = 6.283185307179586;
    private static final double DEFAULT_ARC_TOLERANCE = 0.25;
    private static final double TOLERANCE = 1.0E-20;
    
    private static boolean nearZero(final double val) {
        return val > -1.0E-20 && val < 1.0E-20;
    }
    
    public ClipperOffset() {
        this(2.0, 0.25);
    }
    
    public ClipperOffset(final double miterLimit) {
        this(miterLimit, 0.25);
    }
    
    public ClipperOffset(final double miterLimit, final double arcTolerance) {
        this.miterLimit = miterLimit;
        this.arcTolerance = arcTolerance;
        (this.lowest = new Point.LongPoint()).setX(-1L);
        this.polyNodes = new PolyNode();
        this.normals = new ArrayList<Point.DoublePoint>();
    }
    
    public void addPath(final Path path, final Clipper.JoinType joinType, final Clipper.EndType endType) {
        int highI = path.size() - 1;
        if (highI < 0) {
            return;
        }
        final PolyNode newNode = new PolyNode();
        newNode.setJoinType(joinType);
        newNode.setEndType(endType);
        if (endType == Clipper.EndType.CLOSED_LINE || endType == Clipper.EndType.CLOSED_POLYGON) {
            while (highI > 0 && path.get(0) == path.get(highI)) {
                --highI;
            }
        }
        newNode.getPolygon().add(path.get(0));
        int j = 0;
        int k = 0;
        for (int i = 1; i <= highI; ++i) {
            if (newNode.getPolygon().get(j) != path.get(i)) {
                ++j;
                newNode.getPolygon().add(path.get(i));
                if (path.get(i).getY() > newNode.getPolygon().get(k).getY() || (path.get(i).getY() == newNode.getPolygon().get(k).getY() && path.get(i).getX() < newNode.getPolygon().get(k).getX())) {
                    k = j;
                }
            }
        }
        if (endType == Clipper.EndType.CLOSED_POLYGON && j < 2) {
            return;
        }
        this.polyNodes.addChild(newNode);
        if (endType != Clipper.EndType.CLOSED_POLYGON) {
            return;
        }
        if (this.lowest.getX() < 0L) {
            this.lowest = new Point.LongPoint(this.polyNodes.getChildCount() - 1, k);
        }
        else {
            final Point.LongPoint ip = this.polyNodes.getChilds().get((int)this.lowest.getX()).getPolygon().get((int)this.lowest.getY());
            if (newNode.getPolygon().get(k).getY() > ip.getY() || (newNode.getPolygon().get(k).getY() == ip.getY() && newNode.getPolygon().get(k).getX() < ip.getX())) {
                this.lowest = new Point.LongPoint(this.polyNodes.getChildCount() - 1, k);
            }
        }
    }
    
    public void addPaths(final Paths paths, final Clipper.JoinType joinType, final Clipper.EndType endType) {
        for (final Path p : paths) {
            this.addPath(p, joinType, endType);
        }
    }
    
    public void clear() {
        this.polyNodes.getChilds().clear();
        this.lowest.setX(-1L);
    }
    
    private void doMiter(final int j, final int k, final double r) {
        final double q = this.delta / r;
        this.destPoly.add(new Point.LongPoint(Math.round(this.srcPoly.get(j).getX() + (this.normals.get(k).getX() + this.normals.get(j).getX()) * q), Math.round(this.srcPoly.get(j).getY() + (this.normals.get(k).getY() + this.normals.get(j).getY()) * q)));
    }
    
    private void doOffset(final double delta) {
        this.destPolys = new Paths();
        this.delta = delta;
        if (nearZero(delta)) {
            for (int i = 0; i < this.polyNodes.getChildCount(); ++i) {
                final PolyNode node = this.polyNodes.getChilds().get(i);
                if (node.getEndType() == Clipper.EndType.CLOSED_POLYGON) {
                    this.destPolys.add(node.getPolygon());
                }
            }
            return;
        }
        if (this.miterLimit > 2.0) {
            this.miterLim = 2.0 / (this.miterLimit * this.miterLimit);
        }
        else {
            this.miterLim = 0.5;
        }
        double y;
        if (this.arcTolerance <= 0.0) {
            y = 0.25;
        }
        else if (this.arcTolerance > Math.abs(delta) * 0.25) {
            y = Math.abs(delta) * 0.25;
        }
        else {
            y = this.arcTolerance;
        }
        final double steps = 3.141592653589793 / Math.acos(1.0 - y / Math.abs(delta));
        this.sin = Math.sin(6.283185307179586 / steps);
        this.cos = Math.cos(6.283185307179586 / steps);
        this.stepsPerRad = steps / 6.283185307179586;
        if (delta < 0.0) {
            this.sin = -this.sin;
        }
        for (int j = 0; j < this.polyNodes.getChildCount(); ++j) {
            final PolyNode node2 = this.polyNodes.getChilds().get(j);
            this.srcPoly = node2.getPolygon();
            final int len = this.srcPoly.size();
            if (len != 0) {
                if (delta <= 0.0) {
                    if (len < 3) {
                        continue;
                    }
                    if (node2.getEndType() != Clipper.EndType.CLOSED_POLYGON) {
                        continue;
                    }
                }
                this.destPoly = new Path();
                if (len == 1) {
                    if (node2.getJoinType() == Clipper.JoinType.ROUND) {
                        double X = 1.0;
                        double Y = 0.0;
                        for (int k = 1; k <= steps; ++k) {
                            this.destPoly.add(new Point.LongPoint(Math.round(this.srcPoly.get(0).getX() + X * delta), Math.round(this.srcPoly.get(0).getY() + Y * delta)));
                            final double X2 = X;
                            X = X * this.cos - this.sin * Y;
                            Y = X2 * this.sin + Y * this.cos;
                        }
                    }
                    else {
                        double X = -1.0;
                        double Y = -1.0;
                        for (int k = 0; k < 4; ++k) {
                            this.destPoly.add(new Point.LongPoint(Math.round(this.srcPoly.get(0).getX() + X * delta), Math.round(this.srcPoly.get(0).getY() + Y * delta)));
                            if (X < 0.0) {
                                X = 1.0;
                            }
                            else if (Y < 0.0) {
                                Y = 1.0;
                            }
                            else {
                                X = -1.0;
                            }
                        }
                    }
                    this.destPolys.add(this.destPoly);
                }
                else {
                    this.normals.clear();
                    for (int l = 0; l < len - 1; ++l) {
                        this.normals.add(Point.getUnitNormal(this.srcPoly.get(l), this.srcPoly.get(l + 1)));
                    }
                    if (node2.getEndType() == Clipper.EndType.CLOSED_LINE || node2.getEndType() == Clipper.EndType.CLOSED_POLYGON) {
                        this.normals.add(Point.getUnitNormal(this.srcPoly.get(len - 1), this.srcPoly.get(0)));
                    }
                    else {
                        this.normals.add(new Point.DoublePoint(this.normals.get(len - 2)));
                    }
                    if (node2.getEndType() == Clipper.EndType.CLOSED_POLYGON) {
                        final int[] m = { len - 1 };
                        for (int j2 = 0; j2 < len; ++j2) {
                            this.offsetPoint(j2, m, node2.getJoinType());
                        }
                        this.destPolys.add(this.destPoly);
                    }
                    else if (node2.getEndType() == Clipper.EndType.CLOSED_LINE) {
                        final int[] m = { len - 1 };
                        for (int j2 = 0; j2 < len; ++j2) {
                            this.offsetPoint(j2, m, node2.getJoinType());
                        }
                        this.destPolys.add(this.destPoly);
                        this.destPoly = new Path();
                        final Point.DoublePoint n = this.normals.get(len - 1);
                        for (int j3 = len - 1; j3 > 0; --j3) {
                            this.normals.set(j3, new Point.DoublePoint(-this.normals.get(j3 - 1).getX(), -this.normals.get(j3 - 1).getY()));
                        }
                        this.normals.set(0, new Point.DoublePoint(-n.getX(), -n.getY(), 0.0));
                        m[0] = 0;
                        for (int j3 = len - 1; j3 >= 0; --j3) {
                            this.offsetPoint(j3, m, node2.getJoinType());
                        }
                        this.destPolys.add(this.destPoly);
                    }
                    else {
                        final int[] m = { 0 };
                        for (int j2 = 1; j2 < len - 1; ++j2) {
                            this.offsetPoint(j2, m, node2.getJoinType());
                        }
                        if (node2.getEndType() == Clipper.EndType.OPEN_BUTT) {
                            final int j3 = len - 1;
                            Point.LongPoint pt1 = new Point.LongPoint(Math.round(this.srcPoly.get(j3).getX() + this.normals.get(j3).getX() * delta), Math.round(this.srcPoly.get(j3).getY() + this.normals.get(j3).getY() * delta), 0L);
                            this.destPoly.add(pt1);
                            pt1 = new Point.LongPoint(Math.round(this.srcPoly.get(j3).getX() - this.normals.get(j3).getX() * delta), Math.round(this.srcPoly.get(j3).getY() - this.normals.get(j3).getY() * delta), 0L);
                            this.destPoly.add(pt1);
                        }
                        else {
                            final int j3 = len - 1;
                            m[0] = len - 2;
                            this.inA = 0.0;
                            this.normals.set(j3, new Point.DoublePoint(-this.normals.get(j3).getX(), -this.normals.get(j3).getY()));
                            if (node2.getEndType() == Clipper.EndType.OPEN_SQUARE) {
                                this.doSquare(j3, m[0], true);
                            }
                            else {
                                this.doRound(j3, m[0]);
                            }
                        }
                        for (int j3 = len - 1; j3 > 0; --j3) {
                            this.normals.set(j3, new Point.DoublePoint(-this.normals.get(j3 - 1).getX(), -this.normals.get(j3 - 1).getY()));
                        }
                        this.normals.set(0, new Point.DoublePoint(-this.normals.get(1).getX(), -this.normals.get(1).getY()));
                        m[0] = len - 1;
                        for (int j3 = m[0] - 1; j3 > 0; --j3) {
                            this.offsetPoint(j3, m, node2.getJoinType());
                        }
                        if (node2.getEndType() == Clipper.EndType.OPEN_BUTT) {
                            Point.LongPoint pt1 = new Point.LongPoint(Math.round(this.srcPoly.get(0).getX() - this.normals.get(0).getX() * delta), Math.round(this.srcPoly.get(0).getY() - this.normals.get(0).getY() * delta));
                            this.destPoly.add(pt1);
                            pt1 = new Point.LongPoint(Math.round(this.srcPoly.get(0).getX() + this.normals.get(0).getX() * delta), Math.round(this.srcPoly.get(0).getY() + this.normals.get(0).getY() * delta));
                            this.destPoly.add(pt1);
                        }
                        else {
                            m[0] = 1;
                            this.inA = 0.0;
                            if (node2.getEndType() == Clipper.EndType.OPEN_SQUARE) {
                                this.doSquare(0, 1, true);
                            }
                            else {
                                this.doRound(0, 1);
                            }
                        }
                        this.destPolys.add(this.destPoly);
                    }
                }
            }
        }
    }
    
    private void doRound(final int j, final int k) {
        final double a = Math.atan2(this.inA, this.normals.get(k).getX() * this.normals.get(j).getX() + this.normals.get(k).getY() * this.normals.get(j).getY());
        final int steps = Math.max((int)Math.round(this.stepsPerRad * Math.abs(a)), 1);
        double X = this.normals.get(k).getX();
        double Y = this.normals.get(k).getY();
        for (int i = 0; i < steps; ++i) {
            this.destPoly.add(new Point.LongPoint(Math.round(this.srcPoly.get(j).getX() + X * this.delta), Math.round(this.srcPoly.get(j).getY() + Y * this.delta)));
            final double X2 = X;
            X = X * this.cos - this.sin * Y;
            Y = X2 * this.sin + Y * this.cos;
        }
        this.destPoly.add(new Point.LongPoint(Math.round(this.srcPoly.get(j).getX() + this.normals.get(j).getX() * this.delta), Math.round(this.srcPoly.get(j).getY() + this.normals.get(j).getY() * this.delta)));
    }
    
    private void doSquare(final int j, final int k, final boolean addExtra) {
        final double nkx = this.normals.get(k).getX();
        final double nky = this.normals.get(k).getY();
        final double njx = this.normals.get(j).getX();
        final double njy = this.normals.get(j).getY();
        final double sjx = (double)this.srcPoly.get(j).getX();
        final double sjy = (double)this.srcPoly.get(j).getY();
        final double dx = Math.tan(Math.atan2(this.inA, nkx * njx + nky * njy) / 4.0);
        this.destPoly.add(new Point.LongPoint(Math.round(sjx + this.delta * (nkx - (addExtra ? (nky * dx) : 0.0))), Math.round(sjy + this.delta * (nky + (addExtra ? (nkx * dx) : 0.0))), 0L));
        this.destPoly.add(new Point.LongPoint(Math.round(sjx + this.delta * (njx + (addExtra ? (njy * dx) : 0.0))), Math.round(sjy + this.delta * (njy - (addExtra ? (njx * dx) : 0.0))), 0L));
    }
    
    public void execute(final Paths solution, final double delta) {
        solution.clear();
        this.fixOrientations();
        this.doOffset(delta);
        final DefaultClipper clpr = new DefaultClipper(1);
        clpr.addPaths(this.destPolys, Clipper.PolyType.SUBJECT, true);
        if (delta > 0.0) {
            clpr.execute(Clipper.ClipType.UNION, solution, Clipper.PolyFillType.POSITIVE, Clipper.PolyFillType.POSITIVE);
        }
        else {
            final LongRect r = this.destPolys.getBounds();
            final Path outer = new Path(4);
            outer.add(new Point.LongPoint(r.left - 10L, r.bottom + 10L, 0L));
            outer.add(new Point.LongPoint(r.right + 10L, r.bottom + 10L, 0L));
            outer.add(new Point.LongPoint(r.right + 10L, r.top - 10L, 0L));
            outer.add(new Point.LongPoint(r.left - 10L, r.top - 10L, 0L));
            clpr.addPath(outer, Clipper.PolyType.SUBJECT, true);
            clpr.execute(Clipper.ClipType.UNION, solution, Clipper.PolyFillType.NEGATIVE, Clipper.PolyFillType.NEGATIVE);
            if (solution.size() > 0) {
                solution.remove(0);
            }
        }
    }
    
    public void execute(final PolyTree solution, final double delta) {
        solution.Clear();
        this.fixOrientations();
        this.doOffset(delta);
        final DefaultClipper clpr = new DefaultClipper(1);
        clpr.addPaths(this.destPolys, Clipper.PolyType.SUBJECT, true);
        if (delta > 0.0) {
            clpr.execute(Clipper.ClipType.UNION, solution, Clipper.PolyFillType.POSITIVE, Clipper.PolyFillType.POSITIVE);
        }
        else {
            final LongRect r = this.destPolys.getBounds();
            final Path outer = new Path(4);
            outer.add(new Point.LongPoint(r.left - 10L, r.bottom + 10L, 0L));
            outer.add(new Point.LongPoint(r.right + 10L, r.bottom + 10L, 0L));
            outer.add(new Point.LongPoint(r.right + 10L, r.top - 10L, 0L));
            outer.add(new Point.LongPoint(r.left - 10L, r.top - 10L, 0L));
            clpr.addPath(outer, Clipper.PolyType.SUBJECT, true);
            clpr.execute(Clipper.ClipType.UNION, solution, Clipper.PolyFillType.NEGATIVE, Clipper.PolyFillType.NEGATIVE);
            if (solution.getChildCount() == 1 && solution.getChilds().get(0).getChildCount() > 0) {
                final PolyNode outerNode = solution.getChilds().get(0);
                solution.getChilds().set(0, outerNode.getChilds().get(0));
                solution.getChilds().get(0).setParent(solution);
                for (int i = 1; i < outerNode.getChildCount(); ++i) {
                    solution.addChild(outerNode.getChilds().get(i));
                }
            }
            else {
                solution.Clear();
            }
        }
    }
    
    private void fixOrientations() {
        if (this.lowest.getX() >= 0L && !this.polyNodes.childs.get((int)this.lowest.getX()).getPolygon().orientation()) {
            for (int i = 0; i < this.polyNodes.getChildCount(); ++i) {
                final PolyNode node = this.polyNodes.childs.get(i);
                if (node.getEndType() == Clipper.EndType.CLOSED_POLYGON || (node.getEndType() == Clipper.EndType.CLOSED_LINE && node.getPolygon().orientation())) {
                    Collections.reverse(node.getPolygon());
                }
            }
        }
        else {
            for (int i = 0; i < this.polyNodes.getChildCount(); ++i) {
                final PolyNode node = this.polyNodes.childs.get(i);
                if (node.getEndType() == Clipper.EndType.CLOSED_LINE && !node.getPolygon().orientation()) {
                    Collections.reverse(node.getPolygon());
                }
            }
        }
    }
    
    private void offsetPoint(final int j, final int[] kV, final Clipper.JoinType jointype) {
        final int k = kV[0];
        final double nkx = this.normals.get(k).getX();
        final double nky = this.normals.get(k).getY();
        final double njy = this.normals.get(j).getY();
        final double njx = this.normals.get(j).getX();
        final long sjx = this.srcPoly.get(j).getX();
        final long sjy = this.srcPoly.get(j).getY();
        this.inA = nkx * njy - njx * nky;
        if (Math.abs(this.inA * this.delta) < 1.0) {
            final double cosA = nkx * njx + njy * nky;
            if (cosA > 0.0) {
                this.destPoly.add(new Point.LongPoint(Math.round(sjx + nkx * this.delta), Math.round(sjy + nky * this.delta), 0L));
                return;
            }
        }
        else if (this.inA > 1.0) {
            this.inA = 1.0;
        }
        else if (this.inA < -1.0) {
            this.inA = -1.0;
        }
        if (this.inA * this.delta < 0.0) {
            this.destPoly.add(new Point.LongPoint(Math.round(sjx + nkx * this.delta), Math.round(sjy + nky * this.delta)));
            this.destPoly.add(this.srcPoly.get(j));
            this.destPoly.add(new Point.LongPoint(Math.round(sjx + njx * this.delta), Math.round(sjy + njy * this.delta)));
        }
        else {
            switch (jointype) {
                case MITER: {
                    final double r = 1.0 + njx * nkx + njy * nky;
                    if (r >= this.miterLim) {
                        this.doMiter(j, k, r);
                        break;
                    }
                    this.doSquare(j, k, false);
                    break;
                }
                case BEVEL: {
                    this.doSquare(j, k, false);
                    break;
                }
                case ROUND: {
                    this.doRound(j, k);
                    break;
                }
            }
        }
        kV[0] = j;
    }
}
