// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import com.itextpdf.text.xml.XMLUtil;
import com.itextpdf.text.pdf.PdfNumber;
import java.util.Iterator;
import java.util.Set;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfDictionary;
import java.io.IOException;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.pdf.PdfName;
import java.io.Writer;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.io.PrintWriter;
import com.itextpdf.text.pdf.PdfReader;

public class TaggedPdfReaderTool
{
    protected PdfReader reader;
    protected PrintWriter out;
    
    public void convertToXml(final PdfReader reader, final OutputStream os, final String charset) throws IOException {
        this.reader = reader;
        final OutputStreamWriter outs = new OutputStreamWriter(os, charset);
        this.out = new PrintWriter(outs);
        final PdfDictionary catalog = reader.getCatalog();
        final PdfDictionary struct = catalog.getAsDict(PdfName.STRUCTTREEROOT);
        if (struct == null) {
            throw new IOException(MessageLocalization.getComposedMessage("no.structtreeroot.found", new Object[0]));
        }
        this.inspectChild(struct.getDirectObject(PdfName.K));
        this.out.flush();
        this.out.close();
    }
    
    public void convertToXml(final PdfReader reader, final OutputStream os) throws IOException {
        this.convertToXml(reader, os, "UTF-8");
    }
    
    public void inspectChild(final PdfObject k) throws IOException {
        if (k == null) {
            return;
        }
        if (k instanceof PdfArray) {
            this.inspectChildArray((PdfArray)k);
        }
        else if (k instanceof PdfDictionary) {
            this.inspectChildDictionary((PdfDictionary)k);
        }
    }
    
    public void inspectChildArray(final PdfArray k) throws IOException {
        if (k == null) {
            return;
        }
        for (int i = 0; i < k.size(); ++i) {
            this.inspectChild(k.getDirectObject(i));
        }
    }
    
    public void inspectChildDictionary(final PdfDictionary k) throws IOException {
        this.inspectChildDictionary(k, false);
    }
    
    public void inspectChildDictionary(final PdfDictionary k, final boolean inspectAttributes) throws IOException {
        if (k == null) {
            return;
        }
        final PdfName s = k.getAsName(PdfName.S);
        if (s != null) {
            final String tagN = PdfName.decodeName(s.toString());
            final String tag = fixTagName(tagN);
            this.out.print("<");
            this.out.print(tag);
            if (inspectAttributes) {
                final PdfDictionary a = k.getAsDict(PdfName.A);
                if (a != null) {
                    final Set<PdfName> keys = a.getKeys();
                    for (final PdfName key : keys) {
                        this.out.print(' ');
                        PdfObject value = a.get(key);
                        value = PdfReader.getPdfObject(value);
                        this.out.print(this.xmlName(key));
                        this.out.print("=\"");
                        this.out.print(value.toString());
                        this.out.print("\"");
                    }
                }
            }
            this.out.print(">");
            final PdfObject alt = k.get(PdfName.ALT);
            if (alt != null && alt.toString() != null) {
                this.out.print("<alt><![CDATA[");
                this.out.print(alt.toString().replaceAll("[\\000]*", ""));
                this.out.print("]]></alt>");
            }
            final PdfDictionary dict = k.getAsDict(PdfName.PG);
            if (dict != null) {
                this.parseTag(tagN, k.getDirectObject(PdfName.K), dict);
            }
            this.inspectChild(k.getDirectObject(PdfName.K));
            this.out.print("</");
            this.out.print(tag);
            this.out.println(">");
        }
        else {
            this.inspectChild(k.getDirectObject(PdfName.K));
        }
    }
    
    protected String xmlName(final PdfName name) {
        String xmlName = name.toString().replaceFirst("/", "");
        xmlName = Character.toLowerCase(xmlName.charAt(0)) + xmlName.substring(1);
        return xmlName;
    }
    
    private static String fixTagName(final String tag) {
        final StringBuilder sb = new StringBuilder();
        for (int k = 0; k < tag.length(); ++k) {
            char c = tag.charAt(k);
            final boolean nameStart = c == ':' || (c >= 'A' && c <= 'Z') || c == '_' || (c >= 'a' && c <= 'z') || (c >= '\u00c0' && c <= '\u00d6') || (c >= '\u00d8' && c <= '\u00f6') || (c >= '\u00f8' && c <= '\u02ff') || (c >= '\u0370' && c <= '\u037d') || (c >= '\u037f' && c <= '\u1fff') || (c >= '\u200c' && c <= '\u200d') || (c >= '\u2070' && c <= '\u218f') || (c >= '\u2c00' && c <= '\u2fef') || (c >= '\u3001' && c <= '\ud7ff') || (c >= '\uf900' && c <= '\ufdcf') || (c >= '\ufdf0' && c <= '\ufffd');
            final boolean nameMiddle = c == '-' || c == '.' || (c >= '0' && c <= '9') || c == '·' || (c >= '\u0300' && c <= '\u036f') || (c >= '\u203f' && c <= '\u2040') || nameStart;
            if (k == 0) {
                if (!nameStart) {
                    c = '_';
                }
            }
            else if (!nameMiddle) {
                c = '-';
            }
            sb.append(c);
        }
        return sb.toString();
    }
    
    public void parseTag(final String tag, final PdfObject object, final PdfDictionary page) throws IOException {
        if (object instanceof PdfNumber) {
            final PdfNumber mcid = (PdfNumber)object;
            final RenderFilter filter = new MarkedContentRenderFilter(mcid.intValue());
            final TextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
            final FilteredTextRenderListener listener = new FilteredTextRenderListener(strategy, new RenderFilter[] { filter });
            final PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
            processor.processContent(PdfReader.getPageContent(page), page.getAsDict(PdfName.RESOURCES));
            this.out.print(XMLUtil.escapeXML(listener.getResultantText(), true));
        }
        else if (object instanceof PdfArray) {
            final PdfArray arr = (PdfArray)object;
            for (int n = arr.size(), i = 0; i < n; ++i) {
                this.parseTag(tag, arr.getPdfObject(i), page);
                if (i < n - 1) {
                    this.out.println();
                }
            }
        }
        else if (object instanceof PdfDictionary) {
            final PdfDictionary mcr = (PdfDictionary)object;
            this.parseTag(tag, mcr.getDirectObject(PdfName.MCID), mcr.getAsDict(PdfName.PG));
        }
    }
}
