// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.io.UnsupportedEncodingException;
import java.util.List;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.pdf.DocumentFont;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collection;
import com.itextpdf.text.pdf.PdfString;

public class TextRenderInfo
{
    private final PdfString string;
    private String text;
    private final Matrix textToUserSpaceTransformMatrix;
    private final GraphicsState gs;
    private Float unscaledWidth;
    private double[] fontMatrix;
    private final Collection<MarkedContentInfo> markedContentInfos;
    
    TextRenderInfo(final PdfString string, final GraphicsState gs, final Matrix textMatrix, final Collection<MarkedContentInfo> markedContentInfo) {
        this.text = null;
        this.unscaledWidth = null;
        this.fontMatrix = null;
        this.string = string;
        this.textToUserSpaceTransformMatrix = textMatrix.multiply(gs.ctm);
        this.gs = gs;
        this.markedContentInfos = new ArrayList<MarkedContentInfo>(markedContentInfo);
        this.fontMatrix = gs.font.getFontMatrix();
    }
    
    private TextRenderInfo(final TextRenderInfo parent, final PdfString string, final float horizontalOffset) {
        this.text = null;
        this.unscaledWidth = null;
        this.fontMatrix = null;
        this.string = string;
        this.textToUserSpaceTransformMatrix = new Matrix(horizontalOffset, 0.0f).multiply(parent.textToUserSpaceTransformMatrix);
        this.gs = parent.gs;
        this.markedContentInfos = parent.markedContentInfos;
        this.fontMatrix = this.gs.font.getFontMatrix();
    }
    
    public String getText() {
        if (this.text == null) {
            this.text = this.decode(this.string);
        }
        return this.text;
    }
    
    public PdfString getPdfString() {
        return this.string;
    }
    
    public boolean hasMcid(final int mcid) {
        return this.hasMcid(mcid, false);
    }
    
    public boolean hasMcid(final int mcid, final boolean checkTheTopmostLevelOnly) {
        if (checkTheTopmostLevelOnly) {
            if (this.markedContentInfos instanceof ArrayList) {
                final Integer infoMcid = this.getMcid();
                return infoMcid != null && infoMcid == mcid;
            }
        }
        else {
            for (final MarkedContentInfo info : this.markedContentInfos) {
                if (info.hasMcid() && info.getMcid() == mcid) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public Integer getMcid() {
        if (this.markedContentInfos instanceof ArrayList) {
            final ArrayList<MarkedContentInfo> mci = (ArrayList<MarkedContentInfo>)(ArrayList)this.markedContentInfos;
            final MarkedContentInfo info = (mci.size() > 0) ? mci.get(mci.size() - 1) : null;
            return (info != null && info.hasMcid()) ? Integer.valueOf(info.getMcid()) : null;
        }
        return null;
    }
    
    float getUnscaledWidth() {
        if (this.unscaledWidth == null) {
            this.unscaledWidth = this.getPdfStringWidth(this.string, false);
        }
        return this.unscaledWidth;
    }
    
    public LineSegment getBaseline() {
        return this.getUnscaledBaselineWithOffset(0.0f + this.gs.rise).transformBy(this.textToUserSpaceTransformMatrix);
    }
    
    public LineSegment getUnscaledBaseline() {
        return this.getUnscaledBaselineWithOffset(0.0f + this.gs.rise);
    }
    
    public LineSegment getAscentLine() {
        final float ascent = this.gs.getFont().getFontDescriptor(1, this.gs.getFontSize());
        return this.getUnscaledBaselineWithOffset(ascent + this.gs.rise).transformBy(this.textToUserSpaceTransformMatrix);
    }
    
    public LineSegment getDescentLine() {
        final float descent = this.gs.getFont().getFontDescriptor(3, this.gs.getFontSize());
        return this.getUnscaledBaselineWithOffset(descent + this.gs.rise).transformBy(this.textToUserSpaceTransformMatrix);
    }
    
    private LineSegment getUnscaledBaselineWithOffset(final float yOffset) {
        final String unicodeStr = this.string.toUnicodeString();
        final float correctedUnscaledWidth = this.getUnscaledWidth() - (this.gs.characterSpacing + ((unicodeStr.length() > 0 && unicodeStr.charAt(unicodeStr.length() - 1) == ' ') ? this.gs.wordSpacing : 0.0f)) * this.gs.horizontalScaling;
        return new LineSegment(new Vector(0.0f, yOffset, 1.0f), new Vector(correctedUnscaledWidth, yOffset, 1.0f));
    }
    
    public DocumentFont getFont() {
        return this.gs.getFont();
    }
    
    public float getRise() {
        if (this.gs.rise == 0.0f) {
            return 0.0f;
        }
        return this.convertHeightFromTextSpaceToUserSpace(this.gs.rise);
    }
    
    private float convertWidthFromTextSpaceToUserSpace(final float width) {
        final LineSegment textSpace = new LineSegment(new Vector(0.0f, 0.0f, 1.0f), new Vector(width, 0.0f, 1.0f));
        final LineSegment userSpace = textSpace.transformBy(this.textToUserSpaceTransformMatrix);
        return userSpace.getLength();
    }
    
    private float convertHeightFromTextSpaceToUserSpace(final float height) {
        final LineSegment textSpace = new LineSegment(new Vector(0.0f, 0.0f, 1.0f), new Vector(0.0f, height, 1.0f));
        final LineSegment userSpace = textSpace.transformBy(this.textToUserSpaceTransformMatrix);
        return userSpace.getLength();
    }
    
    public float getSingleSpaceWidth() {
        return this.convertWidthFromTextSpaceToUserSpace(this.getUnscaledFontSpaceWidth());
    }
    
    public int getTextRenderMode() {
        return this.gs.renderMode;
    }
    
    public BaseColor getFillColor() {
        return this.gs.fillColor;
    }
    
    public BaseColor getStrokeColor() {
        return this.gs.strokeColor;
    }
    
    private float getUnscaledFontSpaceWidth() {
        char charToUse = ' ';
        if (this.gs.font.getWidth(charToUse) == 0) {
            charToUse = ' ';
        }
        return this.getStringWidth(String.valueOf(charToUse));
    }
    
    private float getStringWidth(final String string) {
        float totalWidth = 0.0f;
        for (int i = 0; i < string.length(); ++i) {
            final char c = string.charAt(i);
            final float w = this.gs.font.getWidth(c) / 1000.0f;
            final float wordSpacing = (c == ' ') ? this.gs.wordSpacing : 0.0f;
            totalWidth += (w * this.gs.fontSize + this.gs.characterSpacing + wordSpacing) * this.gs.horizontalScaling;
        }
        return totalWidth;
    }
    
    private float getPdfStringWidth(final PdfString string, final boolean singleCharString) {
        if (singleCharString) {
            final float[] widthAndWordSpacing = this.getWidthAndWordSpacing(string, singleCharString);
            return (widthAndWordSpacing[0] * this.gs.fontSize + this.gs.characterSpacing + widthAndWordSpacing[1]) * this.gs.horizontalScaling;
        }
        float totalWidth = 0.0f;
        for (final PdfString str : this.splitString(string)) {
            totalWidth += this.getPdfStringWidth(str, true);
        }
        return totalWidth;
    }
    
    public List<TextRenderInfo> getCharacterRenderInfos() {
        final List<TextRenderInfo> rslt = new ArrayList<TextRenderInfo>(this.string.length());
        final PdfString[] strings = this.splitString(this.string);
        float totalWidth = 0.0f;
        for (int i = 0; i < strings.length; ++i) {
            final float[] widthAndWordSpacing = this.getWidthAndWordSpacing(strings[i], true);
            final TextRenderInfo subInfo = new TextRenderInfo(this, strings[i], totalWidth);
            rslt.add(subInfo);
            totalWidth += (widthAndWordSpacing[0] * this.gs.fontSize + this.gs.characterSpacing + widthAndWordSpacing[1]) * this.gs.horizontalScaling;
        }
        for (final TextRenderInfo tri : rslt) {
            tri.getUnscaledWidth();
        }
        return rslt;
    }
    
    private float[] getWidthAndWordSpacing(final PdfString string, final boolean singleCharString) {
        if (!singleCharString) {
            throw new UnsupportedOperationException();
        }
        final float[] result = new float[2];
        final String decoded = this.decode(string);
        result[0] = (float)(this.gs.font.getWidth(this.getCharCode(decoded)) * this.fontMatrix[0]);
        result[1] = (decoded.equals(" ") ? this.gs.wordSpacing : 0.0f);
        return result;
    }
    
    private String decode(final PdfString in) {
        final byte[] bytes = in.getBytes();
        return this.gs.font.decode(bytes, 0, bytes.length);
    }
    
    private int getCharCode(final String string) {
        try {
            final byte[] b = string.getBytes("UTF-16BE");
            int value = 0;
            for (int i = 0; i < b.length - 1; ++i) {
                value += (b[i] & 0xFF);
                value <<= 8;
            }
            if (b.length > 0) {
                value += (b[b.length - 1] & 0xFF);
            }
            return value;
        }
        catch (UnsupportedEncodingException ex) {
            return 0;
        }
    }
    
    private PdfString[] splitString(final PdfString string) {
        final List<PdfString> strings = new ArrayList<PdfString>();
        final String stringValue = string.toString();
        for (int i = 0; i < stringValue.length(); ++i) {
            PdfString newString = new PdfString(stringValue.substring(i, i + 1), string.getEncoding());
            final String text = this.decode(newString);
            if (text.length() == 0 && i < stringValue.length() - 1) {
                newString = new PdfString(stringValue.substring(i, i + 2), string.getEncoding());
                ++i;
            }
            strings.add(newString);
        }
        return strings.toArray(new PdfString[strings.size()]);
    }
}
