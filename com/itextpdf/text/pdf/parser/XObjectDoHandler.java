// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.util.Stack;
import com.itextpdf.text.pdf.PdfIndirectReference;
import com.itextpdf.text.pdf.PdfStream;

public interface XObjectDoHandler
{
    void handleXObject(final PdfContentStreamProcessor p0, final PdfStream p1, final PdfIndirectReference p2);
    
    void handleXObject(final PdfContentStreamProcessor p0, final PdfStream p1, final PdfIndirectReference p2, final Stack<MarkedContentInfo> p3);
}
