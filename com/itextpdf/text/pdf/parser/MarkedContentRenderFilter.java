// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

public class MarkedContentRenderFilter extends RenderFilter
{
    private int mcid;
    
    public MarkedContentRenderFilter(final int mcid) {
        this.mcid = mcid;
    }
    
    @Override
    public boolean allowText(final TextRenderInfo renderInfo) {
        return renderInfo.hasMcid(this.mcid);
    }
}
