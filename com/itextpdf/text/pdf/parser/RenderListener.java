// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

public interface RenderListener
{
    void beginTextBlock();
    
    void renderText(final TextRenderInfo p0);
    
    void endTextBlock();
    
    void renderImage(final ImageRenderInfo p0);
}
