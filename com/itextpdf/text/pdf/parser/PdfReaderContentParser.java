// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import java.util.HashMap;
import java.io.IOException;
import java.util.Iterator;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import java.util.Map;
import com.itextpdf.text.pdf.PdfReader;

public class PdfReaderContentParser
{
    private final PdfReader reader;
    
    public PdfReaderContentParser(final PdfReader reader) {
        this.reader = reader;
    }
    
    public <E extends RenderListener> E processContent(final int pageNumber, final E renderListener, final Map<String, ContentOperator> additionalContentOperators) throws IOException {
        final PdfDictionary pageDic = this.reader.getPageN(pageNumber);
        final PdfDictionary resourcesDic = pageDic.getAsDict(PdfName.RESOURCES);
        final PdfContentStreamProcessor processor = new PdfContentStreamProcessor(renderListener);
        for (final Map.Entry<String, ContentOperator> entry : additionalContentOperators.entrySet()) {
            processor.registerContentOperator(entry.getKey(), entry.getValue());
        }
        processor.processContent(ContentByteUtils.getContentBytesForPage(this.reader, pageNumber), resourcesDic);
        return renderListener;
    }
    
    public <E extends RenderListener> E processContent(final int pageNumber, final E renderListener) throws IOException {
        return this.processContent(pageNumber, renderListener, new HashMap<String, ContentOperator>());
    }
}
