// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.parser;

import com.itextpdf.text.pdf.PdfIndirectReference;
import java.util.Arrays;
import java.util.Iterator;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.pdf.PdfContentParser;
import com.itextpdf.text.pdf.PRTokeniser;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import java.util.List;
import java.io.IOException;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.pdf.PdfStream;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfLiteral;
import java.util.ArrayList;
import java.util.Collection;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PRIndirectReference;
import java.util.HashMap;
import com.itextpdf.text.pdf.CMapAwareDocumentFont;
import java.lang.ref.WeakReference;
import com.itextpdf.text.pdf.PdfName;
import java.util.Stack;
import java.util.Map;

public class PdfContentStreamProcessor
{
    public static final String DEFAULTOPERATOR = "DefaultOperator";
    private final Map<String, ContentOperator> operators;
    private ResourceDictionary resources;
    private final Stack<GraphicsState> gsStack;
    private Matrix textMatrix;
    private Matrix textLineMatrix;
    private final RenderListener renderListener;
    private final Map<PdfName, XObjectDoHandler> xobjectDoHandlers;
    private final Map<Integer, WeakReference<CMapAwareDocumentFont>> cachedFonts;
    private final Stack<MarkedContentInfo> markedContentStack;
    
    public PdfContentStreamProcessor(final RenderListener renderListener) {
        this.gsStack = new Stack<GraphicsState>();
        this.cachedFonts = new HashMap<Integer, WeakReference<CMapAwareDocumentFont>>();
        this.markedContentStack = new Stack<MarkedContentInfo>();
        this.renderListener = renderListener;
        this.operators = new HashMap<String, ContentOperator>();
        this.populateOperators();
        this.xobjectDoHandlers = new HashMap<PdfName, XObjectDoHandler>();
        this.populateXObjectDoHandlers();
        this.reset();
    }
    
    private void populateXObjectDoHandlers() {
        this.registerXObjectDoHandler(PdfName.DEFAULT, new IgnoreXObjectDoHandler());
        this.registerXObjectDoHandler(PdfName.FORM, new FormXObjectDoHandler());
        this.registerXObjectDoHandler(PdfName.IMAGE, new ImageXObjectDoHandler());
    }
    
    public XObjectDoHandler registerXObjectDoHandler(final PdfName xobjectSubType, final XObjectDoHandler handler) {
        return this.xobjectDoHandlers.put(xobjectSubType, handler);
    }
    
    private CMapAwareDocumentFont getFont(final PRIndirectReference ind) {
        final Integer n = ind.getNumber();
        final WeakReference<CMapAwareDocumentFont> fontRef = this.cachedFonts.get(n);
        CMapAwareDocumentFont font = (fontRef == null) ? null : fontRef.get();
        if (font == null) {
            font = new CMapAwareDocumentFont(ind);
            this.cachedFonts.put(n, new WeakReference<CMapAwareDocumentFont>(font));
        }
        return font;
    }
    
    private CMapAwareDocumentFont getFont(final PdfDictionary fontResource) {
        return new CMapAwareDocumentFont(fontResource);
    }
    
    private void populateOperators() {
        this.registerContentOperator("DefaultOperator", new IgnoreOperatorContentOperator());
        this.registerContentOperator("q", new PushGraphicsState());
        this.registerContentOperator("Q", new PopGraphicsState());
        this.registerContentOperator("g", new SetGrayFill());
        this.registerContentOperator("G", new SetGrayStroke());
        this.registerContentOperator("rg", new SetRGBFill());
        this.registerContentOperator("RG", new SetRGBStroke());
        this.registerContentOperator("k", new SetCMYKFill());
        this.registerContentOperator("K", new SetCMYKStroke());
        this.registerContentOperator("cs", new SetColorSpaceFill());
        this.registerContentOperator("CS", new SetColorSpaceStroke());
        this.registerContentOperator("sc", new SetColorFill());
        this.registerContentOperator("SC", new SetColorStroke());
        this.registerContentOperator("scn", new SetColorFill());
        this.registerContentOperator("SCN", new SetColorStroke());
        this.registerContentOperator("cm", new ModifyCurrentTransformationMatrix());
        this.registerContentOperator("gs", new ProcessGraphicsStateResource());
        final SetTextCharacterSpacing tcOperator = new SetTextCharacterSpacing();
        this.registerContentOperator("Tc", tcOperator);
        final SetTextWordSpacing twOperator = new SetTextWordSpacing();
        this.registerContentOperator("Tw", twOperator);
        this.registerContentOperator("Tz", new SetTextHorizontalScaling());
        final SetTextLeading tlOperator = new SetTextLeading();
        this.registerContentOperator("TL", tlOperator);
        this.registerContentOperator("Tf", new SetTextFont());
        this.registerContentOperator("Tr", new SetTextRenderMode());
        this.registerContentOperator("Ts", new SetTextRise());
        this.registerContentOperator("BT", new BeginText());
        this.registerContentOperator("ET", new EndText());
        this.registerContentOperator("BMC", new BeginMarkedContent());
        this.registerContentOperator("BDC", new BeginMarkedContentDictionary());
        this.registerContentOperator("EMC", new EndMarkedContent());
        final TextMoveStartNextLine tdOperator = new TextMoveStartNextLine();
        this.registerContentOperator("Td", tdOperator);
        this.registerContentOperator("TD", new TextMoveStartNextLineWithLeading(tdOperator, tlOperator));
        this.registerContentOperator("Tm", new TextSetTextMatrix());
        final TextMoveNextLine tstarOperator = new TextMoveNextLine(tdOperator);
        this.registerContentOperator("T*", tstarOperator);
        final ShowText tjOperator = new ShowText();
        this.registerContentOperator("Tj", tjOperator);
        final MoveNextLineAndShowText tickOperator = new MoveNextLineAndShowText(tstarOperator, tjOperator);
        this.registerContentOperator("'", tickOperator);
        this.registerContentOperator("\"", new MoveNextLineAndShowTextWithSpacing(twOperator, tcOperator, tickOperator));
        this.registerContentOperator("TJ", new ShowTextArray());
        this.registerContentOperator("Do", new Do());
        this.registerContentOperator("w", new SetLineWidth());
        this.registerContentOperator("J", new SetLineCap());
        this.registerContentOperator("j", new SetLineJoin());
        this.registerContentOperator("M", new SetMiterLimit());
        this.registerContentOperator("d", new SetLineDashPattern());
        if (this.renderListener instanceof ExtRenderListener) {
            final int fillStroke = 3;
            this.registerContentOperator("m", new MoveTo());
            this.registerContentOperator("l", new LineTo());
            this.registerContentOperator("c", new Curve());
            this.registerContentOperator("v", new CurveFirstPointDuplicated());
            this.registerContentOperator("y", new CurveFourhPointDuplicated());
            this.registerContentOperator("h", new CloseSubpath());
            this.registerContentOperator("re", new Rectangle());
            this.registerContentOperator("S", new PaintPath(1, -1, false));
            this.registerContentOperator("s", new PaintPath(1, -1, true));
            this.registerContentOperator("f", new PaintPath(2, 1, false));
            this.registerContentOperator("F", new PaintPath(2, 1, false));
            this.registerContentOperator("f*", new PaintPath(2, 2, false));
            this.registerContentOperator("B", new PaintPath(fillStroke, 1, false));
            this.registerContentOperator("B*", new PaintPath(fillStroke, 2, false));
            this.registerContentOperator("b", new PaintPath(fillStroke, 1, true));
            this.registerContentOperator("b*", new PaintPath(fillStroke, 2, true));
            this.registerContentOperator("n", new PaintPath(0, -1, false));
            this.registerContentOperator("W", new ClipPath(1));
            this.registerContentOperator("W*", new ClipPath(2));
        }
    }
    
    public ContentOperator registerContentOperator(final String operatorString, final ContentOperator operator) {
        return this.operators.put(operatorString, operator);
    }
    
    public Collection<String> getRegisteredOperatorStrings() {
        return new ArrayList<String>(this.operators.keySet());
    }
    
    public void reset() {
        this.gsStack.removeAllElements();
        this.gsStack.add(new GraphicsState());
        this.textMatrix = null;
        this.textLineMatrix = null;
        this.resources = new ResourceDictionary();
    }
    
    public GraphicsState gs() {
        return this.gsStack.peek();
    }
    
    private void invokeOperator(final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
        ContentOperator op = this.operators.get(operator.toString());
        if (op == null) {
            op = this.operators.get("DefaultOperator");
        }
        op.invoke(this, operator, operands);
    }
    
    private void beginMarkedContent(final PdfName tag, final PdfDictionary dict) {
        this.markedContentStack.push(new MarkedContentInfo(tag, dict));
    }
    
    private void endMarkedContent() {
        this.markedContentStack.pop();
    }
    
    private void beginText() {
        this.renderListener.beginTextBlock();
    }
    
    private void endText() {
        this.renderListener.endTextBlock();
    }
    
    private void displayPdfString(final PdfString string) {
        final TextRenderInfo renderInfo = new TextRenderInfo(string, this.gs(), this.textMatrix, this.markedContentStack);
        this.renderListener.renderText(renderInfo);
        this.textMatrix = new Matrix(renderInfo.getUnscaledWidth(), 0.0f).multiply(this.textMatrix);
    }
    
    private void displayXObject(final PdfName xobjectName) throws IOException {
        final PdfDictionary xobjects = this.resources.getAsDict(PdfName.XOBJECT);
        final PdfObject xobject = PdfReader.getPdfObjectRelease(xobjects.get(xobjectName));
        final PdfStream xobjectStream = (PdfStream)xobject;
        final PdfName subType = xobjectStream.getAsName(PdfName.SUBTYPE);
        if (xobject.isStream()) {
            XObjectDoHandler handler = this.xobjectDoHandlers.get(subType);
            if (handler == null) {
                handler = this.xobjectDoHandlers.get(PdfName.DEFAULT);
            }
            handler.handleXObject(this, xobjectStream, xobjects.getAsIndirectObject(xobjectName), this.markedContentStack);
            return;
        }
        throw new IllegalStateException(MessageLocalization.getComposedMessage("XObject.1.is.not.a.stream", xobjectName));
    }
    
    private void paintPath(final int operation, final int rule, final boolean close) {
        if (close) {
            this.modifyPath(6, null);
        }
        final PathPaintingRenderInfo renderInfo = new PathPaintingRenderInfo(operation, rule, this.gs());
        ((ExtRenderListener)this.renderListener).renderPath(renderInfo);
    }
    
    private void modifyPath(final int operation, final List<Float> segmentData) {
        final PathConstructionRenderInfo renderInfo = new PathConstructionRenderInfo(operation, segmentData, this.gs().getCtm());
        ((ExtRenderListener)this.renderListener).modifyPath(renderInfo);
    }
    
    private void clipPath(final int rule) {
        ((ExtRenderListener)this.renderListener).clipPath(rule);
    }
    
    private void applyTextAdjust(final float tj) {
        final float adjustBy = -tj / 1000.0f * this.gs().fontSize * this.gs().horizontalScaling;
        this.textMatrix = new Matrix(adjustBy, 0.0f).multiply(this.textMatrix);
    }
    
    public void processContent(final byte[] contentBytes, final PdfDictionary resources) {
        this.resources.push(resources);
        try {
            final PRTokeniser tokeniser = new PRTokeniser(new RandomAccessFileOrArray(new RandomAccessSourceFactory().createSource(contentBytes)));
            final PdfContentParser ps = new PdfContentParser(tokeniser);
            final ArrayList<PdfObject> operands = new ArrayList<PdfObject>();
            while (ps.parse(operands).size() > 0) {
                final PdfLiteral operator = operands.get(operands.size() - 1);
                if ("BI".equals(operator.toString())) {
                    final PdfDictionary colorSpaceDic = (resources != null) ? resources.getAsDict(PdfName.COLORSPACE) : null;
                    this.handleInlineImage(InlineImageUtils.parseInlineImage(ps, colorSpaceDic), colorSpaceDic);
                }
                else {
                    this.invokeOperator(operator, operands);
                }
            }
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
        this.resources.pop();
    }
    
    protected void handleInlineImage(final InlineImageInfo info, final PdfDictionary colorSpaceDic) {
        final ImageRenderInfo renderInfo = ImageRenderInfo.createForEmbeddedImage(this.gs(), info, colorSpaceDic, this.markedContentStack);
        this.renderListener.renderImage(renderInfo);
    }
    
    public RenderListener getRenderListener() {
        return this.renderListener;
    }
    
    private static BaseColor getColor(final PdfName colorSpace, final List<PdfObject> operands) {
        if (PdfName.DEVICEGRAY.equals(colorSpace)) {
            return getColor(1, operands);
        }
        if (PdfName.DEVICERGB.equals(colorSpace)) {
            return getColor(3, operands);
        }
        if (PdfName.DEVICECMYK.equals(colorSpace)) {
            return getColor(4, operands);
        }
        return null;
    }
    
    private static BaseColor getColor(final int nOperands, final List<PdfObject> operands) {
        final float[] c = new float[nOperands];
        for (int i = 0; i < nOperands; ++i) {
            c[i] = operands.get(i).floatValue();
            if (c[i] > 1.0f) {
                c[i] = 1.0f;
            }
            else if (c[i] < 0.0f) {
                c[i] = 0.0f;
            }
        }
        switch (nOperands) {
            case 1: {
                return new GrayColor(c[0]);
            }
            case 3: {
                return new BaseColor(c[0], c[1], c[2]);
            }
            case 4: {
                return new CMYKColor(c[0], c[1], c[2], c[3]);
            }
            default: {
                return null;
            }
        }
    }
    
    private static class ResourceDictionary extends PdfDictionary
    {
        private final List<PdfDictionary> resourcesStack;
        
        public ResourceDictionary() {
            this.resourcesStack = new ArrayList<PdfDictionary>();
        }
        
        public void push(final PdfDictionary resources) {
            this.resourcesStack.add(resources);
        }
        
        public void pop() {
            this.resourcesStack.remove(this.resourcesStack.size() - 1);
        }
        
        @Override
        public PdfObject getDirectObject(final PdfName key) {
            for (int i = this.resourcesStack.size() - 1; i >= 0; --i) {
                final PdfDictionary subResource = this.resourcesStack.get(i);
                if (subResource != null) {
                    final PdfObject obj = subResource.getDirectObject(key);
                    if (obj != null) {
                        return obj;
                    }
                }
            }
            return super.getDirectObject(key);
        }
    }
    
    private static class IgnoreOperatorContentOperator implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
        }
    }
    
    private static class ShowTextArray implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final PdfArray array = operands.get(0);
            float tj = 0.0f;
            final Iterator<PdfObject> i = array.listIterator();
            while (i.hasNext()) {
                final PdfObject entryObj = i.next();
                if (entryObj instanceof PdfString) {
                    processor.displayPdfString((PdfString)entryObj);
                    tj = 0.0f;
                }
                else {
                    tj = ((PdfNumber)entryObj).floatValue();
                    processor.applyTextAdjust(tj);
                }
            }
        }
    }
    
    private static class MoveNextLineAndShowTextWithSpacing implements ContentOperator
    {
        private final SetTextWordSpacing setTextWordSpacing;
        private final SetTextCharacterSpacing setTextCharacterSpacing;
        private final MoveNextLineAndShowText moveNextLineAndShowText;
        
        public MoveNextLineAndShowTextWithSpacing(final SetTextWordSpacing setTextWordSpacing, final SetTextCharacterSpacing setTextCharacterSpacing, final MoveNextLineAndShowText moveNextLineAndShowText) {
            this.setTextWordSpacing = setTextWordSpacing;
            this.setTextCharacterSpacing = setTextCharacterSpacing;
            this.moveNextLineAndShowText = moveNextLineAndShowText;
        }
        
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final PdfNumber aw = operands.get(0);
            final PdfNumber ac = operands.get(1);
            final PdfString string = operands.get(2);
            final ArrayList<PdfObject> twOperands = new ArrayList<PdfObject>(1);
            twOperands.add(0, aw);
            this.setTextWordSpacing.invoke(processor, null, twOperands);
            final ArrayList<PdfObject> tcOperands = new ArrayList<PdfObject>(1);
            tcOperands.add(0, ac);
            this.setTextCharacterSpacing.invoke(processor, null, tcOperands);
            final ArrayList<PdfObject> tickOperands = new ArrayList<PdfObject>(1);
            tickOperands.add(0, string);
            this.moveNextLineAndShowText.invoke(processor, null, tickOperands);
        }
    }
    
    private static class MoveNextLineAndShowText implements ContentOperator
    {
        private final TextMoveNextLine textMoveNextLine;
        private final ShowText showText;
        
        public MoveNextLineAndShowText(final TextMoveNextLine textMoveNextLine, final ShowText showText) {
            this.textMoveNextLine = textMoveNextLine;
            this.showText = showText;
        }
        
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            this.textMoveNextLine.invoke(processor, null, new ArrayList<PdfObject>(0));
            this.showText.invoke(processor, null, operands);
        }
    }
    
    private static class ShowText implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final PdfString string = operands.get(0);
            processor.displayPdfString(string);
        }
    }
    
    private static class TextMoveNextLine implements ContentOperator
    {
        private final TextMoveStartNextLine moveStartNextLine;
        
        public TextMoveNextLine(final TextMoveStartNextLine moveStartNextLine) {
            this.moveStartNextLine = moveStartNextLine;
        }
        
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final ArrayList<PdfObject> tdoperands = new ArrayList<PdfObject>(2);
            tdoperands.add(0, new PdfNumber(0));
            tdoperands.add(1, new PdfNumber(-processor.gs().leading));
            this.moveStartNextLine.invoke(processor, null, tdoperands);
        }
    }
    
    private static class TextSetTextMatrix implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final float a = operands.get(0).floatValue();
            final float b = operands.get(1).floatValue();
            final float c = operands.get(2).floatValue();
            final float d = operands.get(3).floatValue();
            final float e = operands.get(4).floatValue();
            final float f = operands.get(5).floatValue();
            processor.textLineMatrix = new Matrix(a, b, c, d, e, f);
            processor.textMatrix = processor.textLineMatrix;
        }
    }
    
    private static class TextMoveStartNextLineWithLeading implements ContentOperator
    {
        private final TextMoveStartNextLine moveStartNextLine;
        private final SetTextLeading setTextLeading;
        
        public TextMoveStartNextLineWithLeading(final TextMoveStartNextLine moveStartNextLine, final SetTextLeading setTextLeading) {
            this.moveStartNextLine = moveStartNextLine;
            this.setTextLeading = setTextLeading;
        }
        
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final float ty = operands.get(1).floatValue();
            final ArrayList<PdfObject> tlOperands = new ArrayList<PdfObject>(1);
            tlOperands.add(0, new PdfNumber(-ty));
            this.setTextLeading.invoke(processor, null, tlOperands);
            this.moveStartNextLine.invoke(processor, null, operands);
        }
    }
    
    private static class TextMoveStartNextLine implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final float tx = operands.get(0).floatValue();
            final float ty = operands.get(1).floatValue();
            final Matrix translationMatrix = new Matrix(tx, ty);
            processor.textMatrix = translationMatrix.multiply(processor.textLineMatrix);
            processor.textLineMatrix = processor.textMatrix;
        }
    }
    
    private static class SetTextFont implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final PdfName fontResourceName = operands.get(0);
            final float size = operands.get(1).floatValue();
            final PdfDictionary fontsDictionary = processor.resources.getAsDict(PdfName.FONT);
            final PdfObject fontObject = fontsDictionary.get(fontResourceName);
            CMapAwareDocumentFont font;
            if (fontObject instanceof PdfDictionary) {
                font = processor.getFont((PdfDictionary)fontObject);
            }
            else {
                font = processor.getFont((PRIndirectReference)fontObject);
            }
            processor.gs().font = font;
            processor.gs().fontSize = size;
        }
    }
    
    private static class SetTextRenderMode implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final PdfNumber render = operands.get(0);
            processor.gs().renderMode = render.intValue();
        }
    }
    
    private static class SetTextRise implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final PdfNumber rise = operands.get(0);
            processor.gs().rise = rise.floatValue();
        }
    }
    
    private static class SetTextLeading implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final PdfNumber leading = operands.get(0);
            processor.gs().leading = leading.floatValue();
        }
    }
    
    private static class SetTextHorizontalScaling implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final PdfNumber scale = operands.get(0);
            processor.gs().horizontalScaling = scale.floatValue() / 100.0f;
        }
    }
    
    private static class SetTextCharacterSpacing implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final PdfNumber charSpace = operands.get(0);
            processor.gs().characterSpacing = charSpace.floatValue();
        }
    }
    
    private static class SetTextWordSpacing implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final PdfNumber wordSpace = operands.get(0);
            processor.gs().wordSpacing = wordSpace.floatValue();
        }
    }
    
    private static class ProcessGraphicsStateResource implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final PdfName dictionaryName = operands.get(0);
            final PdfDictionary extGState = processor.resources.getAsDict(PdfName.EXTGSTATE);
            if (extGState == null) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("resources.do.not.contain.extgstate.entry.unable.to.process.operator.1", operator));
            }
            final PdfDictionary gsDic = extGState.getAsDict(dictionaryName);
            if (gsDic == null) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("1.is.an.unknown.graphics.state.dictionary", dictionaryName));
            }
            final PdfArray fontParameter = gsDic.getAsArray(PdfName.FONT);
            if (fontParameter != null) {
                final CMapAwareDocumentFont font = processor.getFont((PRIndirectReference)fontParameter.getPdfObject(0));
                final float size = fontParameter.getAsNumber(1).floatValue();
                processor.gs().font = font;
                processor.gs().fontSize = size;
            }
        }
    }
    
    private static class PushGraphicsState implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final GraphicsState gs = processor.gsStack.peek();
            final GraphicsState copy = new GraphicsState(gs);
            processor.gsStack.push(copy);
        }
    }
    
    private static class ModifyCurrentTransformationMatrix implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            final float a = operands.get(0).floatValue();
            final float b = operands.get(1).floatValue();
            final float c = operands.get(2).floatValue();
            final float d = operands.get(3).floatValue();
            final float e = operands.get(4).floatValue();
            final float f = operands.get(5).floatValue();
            final Matrix matrix = new Matrix(a, b, c, d, e, f);
            final GraphicsState gs = processor.gsStack.peek();
            gs.ctm = matrix.multiply(gs.ctm);
        }
    }
    
    private static class SetGrayFill implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            processor.gs().fillColor = getColor(1, operands);
        }
    }
    
    private static class SetGrayStroke implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            processor.gs().strokeColor = getColor(1, operands);
        }
    }
    
    private static class SetRGBFill implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            processor.gs().fillColor = getColor(3, operands);
        }
    }
    
    private static class SetRGBStroke implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            processor.gs().strokeColor = getColor(3, operands);
        }
    }
    
    private static class SetCMYKFill implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            processor.gs().fillColor = getColor(4, operands);
        }
    }
    
    private static class SetCMYKStroke implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            processor.gs().strokeColor = getColor(4, operands);
        }
    }
    
    private static class SetColorSpaceFill implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            processor.gs().colorSpaceFill = operands.get(0);
        }
    }
    
    private static class SetColorSpaceStroke implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            processor.gs().colorSpaceStroke = operands.get(0);
        }
    }
    
    private static class SetColorFill implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            processor.gs().fillColor = getColor(processor.gs().colorSpaceFill, operands);
        }
    }
    
    private static class SetColorStroke implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            processor.gs().strokeColor = getColor(processor.gs().colorSpaceStroke, operands);
        }
    }
    
    private static class PopGraphicsState implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            processor.gsStack.pop();
        }
    }
    
    private static class BeginText implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            processor.textMatrix = new Matrix();
            processor.textLineMatrix = processor.textMatrix;
            processor.beginText();
        }
    }
    
    private static class EndText implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) {
            processor.textMatrix = null;
            processor.textLineMatrix = null;
            processor.endText();
        }
    }
    
    private static class BeginMarkedContent implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
            processor.beginMarkedContent(operands.get(0), new PdfDictionary());
        }
    }
    
    private static class BeginMarkedContentDictionary implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
            final PdfObject properties = operands.get(1);
            processor.beginMarkedContent(operands.get(0), this.getPropertiesDictionary(properties, processor.resources));
        }
        
        private PdfDictionary getPropertiesDictionary(final PdfObject operand1, final ResourceDictionary resources) {
            if (operand1.isDictionary()) {
                return (PdfDictionary)operand1;
            }
            final PdfName dictionaryName = (PdfName)operand1;
            return resources.getAsDict(dictionaryName);
        }
    }
    
    private static class EndMarkedContent implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
            processor.endMarkedContent();
        }
    }
    
    private static class Do implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws IOException {
            final PdfName xobjectName = operands.get(0);
            processor.displayXObject(xobjectName);
        }
    }
    
    private static class SetLineWidth implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral oper, final ArrayList<PdfObject> operands) {
            final float lineWidth = operands.get(0).floatValue();
            processor.gs().setLineWidth(lineWidth);
        }
    }
    
    private static class SetLineCap implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral oper, final ArrayList<PdfObject> operands) {
            final int lineCap = operands.get(0).intValue();
            processor.gs().setLineCapStyle(lineCap);
        }
    }
    
    private static class SetLineJoin implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral oper, final ArrayList<PdfObject> operands) {
            final int lineJoin = operands.get(0).intValue();
            processor.gs().setLineJoinStyle(lineJoin);
        }
    }
    
    private static class SetMiterLimit implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral oper, final ArrayList<PdfObject> operands) {
            final float miterLimit = operands.get(0).floatValue();
            processor.gs().setMiterLimit(miterLimit);
        }
    }
    
    private static class SetLineDashPattern implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral oper, final ArrayList<PdfObject> operands) {
            final LineDashPattern pattern = new LineDashPattern(operands.get(0), operands.get(1).floatValue());
            processor.gs().setLineDashPattern(pattern);
        }
    }
    
    private static class MoveTo implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
            final float x = operands.get(0).floatValue();
            final float y = operands.get(1).floatValue();
            processor.modifyPath(1, Arrays.asList(x, y));
        }
    }
    
    private static class LineTo implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
            final float x = operands.get(0).floatValue();
            final float y = operands.get(1).floatValue();
            processor.modifyPath(2, Arrays.asList(x, y));
        }
    }
    
    private static class Curve implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
            final float x1 = operands.get(0).floatValue();
            final float y1 = operands.get(1).floatValue();
            final float x2 = operands.get(2).floatValue();
            final float y2 = operands.get(3).floatValue();
            final float x3 = operands.get(4).floatValue();
            final float y3 = operands.get(5).floatValue();
            processor.modifyPath(3, Arrays.asList(x1, y1, x2, y2, x3, y3));
        }
    }
    
    private static class CurveFirstPointDuplicated implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
            final float x2 = operands.get(0).floatValue();
            final float y2 = operands.get(1).floatValue();
            final float x3 = operands.get(2).floatValue();
            final float y3 = operands.get(3).floatValue();
            processor.modifyPath(4, Arrays.asList(x2, y2, x3, y3));
        }
    }
    
    private static class CurveFourhPointDuplicated implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
            final float x1 = operands.get(0).floatValue();
            final float y1 = operands.get(1).floatValue();
            final float x2 = operands.get(2).floatValue();
            final float y2 = operands.get(3).floatValue();
            processor.modifyPath(5, Arrays.asList(x1, y1, x2, y2));
        }
    }
    
    private static class CloseSubpath implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
            processor.modifyPath(6, null);
        }
    }
    
    private static class Rectangle implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
            final float x = operands.get(0).floatValue();
            final float y = operands.get(1).floatValue();
            final float w = operands.get(2).floatValue();
            final float h = operands.get(3).floatValue();
            processor.modifyPath(7, Arrays.asList(x, y, w, h));
        }
    }
    
    private static class PaintPath implements ContentOperator
    {
        private int operation;
        private int rule;
        private boolean close;
        
        public PaintPath(final int operation, final int rule, final boolean close) {
            this.operation = operation;
            this.rule = rule;
            this.close = close;
        }
        
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
            processor.paintPath(this.operation, this.rule, this.close);
        }
    }
    
    private static class ClipPath implements ContentOperator
    {
        private int rule;
        
        public ClipPath(final int rule) {
            this.rule = rule;
        }
        
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
            processor.clipPath(this.rule);
        }
    }
    
    private static class EndPath implements ContentOperator
    {
        @Override
        public void invoke(final PdfContentStreamProcessor processor, final PdfLiteral operator, final ArrayList<PdfObject> operands) throws Exception {
            processor.paintPath(0, -1, false);
        }
    }
    
    private static class FormXObjectDoHandler implements XObjectDoHandler
    {
        @Override
        public void handleXObject(final PdfContentStreamProcessor processor, final PdfStream stream, final PdfIndirectReference ref) {
            this.handleXObject(processor, stream, ref, null);
        }
        
        @Override
        public void handleXObject(final PdfContentStreamProcessor processor, final PdfStream stream, final PdfIndirectReference ref, final Stack<MarkedContentInfo> markedContentStack) {
            final PdfDictionary resources = stream.getAsDict(PdfName.RESOURCES);
            byte[] contentBytes;
            try {
                contentBytes = ContentByteUtils.getContentBytesFromContentObject(stream);
            }
            catch (IOException e1) {
                throw new ExceptionConverter(e1);
            }
            final PdfArray matrix = stream.getAsArray(PdfName.MATRIX);
            new PushGraphicsState().invoke(processor, null, null);
            if (matrix != null) {
                final float a = matrix.getAsNumber(0).floatValue();
                final float b = matrix.getAsNumber(1).floatValue();
                final float c = matrix.getAsNumber(2).floatValue();
                final float d = matrix.getAsNumber(3).floatValue();
                final float e2 = matrix.getAsNumber(4).floatValue();
                final float f = matrix.getAsNumber(5).floatValue();
                final Matrix formMatrix = new Matrix(a, b, c, d, e2, f);
                processor.gs().ctm = formMatrix.multiply(processor.gs().ctm);
            }
            processor.processContent(contentBytes, resources);
            new PopGraphicsState().invoke(processor, null, null);
        }
    }
    
    private static class ImageXObjectDoHandler implements XObjectDoHandler
    {
        @Override
        public void handleXObject(final PdfContentStreamProcessor processor, final PdfStream xobjectStream, final PdfIndirectReference ref) {
            final PdfDictionary colorSpaceDic = processor.resources.getAsDict(PdfName.COLORSPACE);
            final ImageRenderInfo renderInfo = ImageRenderInfo.createForXObject(processor.gs(), ref, colorSpaceDic, null);
            processor.renderListener.renderImage(renderInfo);
        }
        
        @Override
        public void handleXObject(final PdfContentStreamProcessor processor, final PdfStream xobjectStream, final PdfIndirectReference ref, final Stack<MarkedContentInfo> markedContentStack) {
            final PdfDictionary colorSpaceDic = processor.resources.getAsDict(PdfName.COLORSPACE);
            final ImageRenderInfo renderInfo = ImageRenderInfo.createForXObject(processor.gs(), ref, colorSpaceDic, markedContentStack);
            processor.renderListener.renderImage(renderInfo);
        }
    }
    
    private static class IgnoreXObjectDoHandler implements XObjectDoHandler
    {
        @Override
        public void handleXObject(final PdfContentStreamProcessor processor, final PdfStream xobjectStream, final PdfIndirectReference ref) {
        }
        
        @Override
        public void handleXObject(final PdfContentStreamProcessor processor, final PdfStream xobjectStream, final PdfIndirectReference ref, final Stack<MarkedContentInfo> markedContentStack) {
        }
    }
}
