// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public interface PdfOCG
{
    PdfIndirectReference getRef();
    
    PdfObject getPdfObject();
}
