// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.util.Iterator;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Phrase;
import java.util.ArrayList;

public class VerticalText
{
    public static final int NO_MORE_TEXT = 1;
    public static final int NO_MORE_COLUMN = 2;
    protected ArrayList<PdfChunk> chunks;
    protected PdfContentByte text;
    protected int alignment;
    protected int currentChunkMarker;
    protected PdfChunk currentStandbyChunk;
    protected String splittedChunkText;
    protected float leading;
    protected float startX;
    protected float startY;
    protected int maxLines;
    protected float height;
    private Float curCharSpace;
    
    public VerticalText(final PdfContentByte text) {
        this.chunks = new ArrayList<PdfChunk>();
        this.alignment = 0;
        this.currentChunkMarker = -1;
        this.curCharSpace = 0.0f;
        this.text = text;
    }
    
    public void addText(final Phrase phrase) {
        for (final Chunk c : phrase.getChunks()) {
            this.chunks.add(new PdfChunk(c, null));
        }
    }
    
    public void addText(final Chunk chunk) {
        this.chunks.add(new PdfChunk(chunk, null));
    }
    
    public void setVerticalLayout(final float startX, final float startY, final float height, final int maxLines, final float leading) {
        this.startX = startX;
        this.startY = startY;
        this.height = height;
        this.maxLines = maxLines;
        this.setLeading(leading);
    }
    
    public void setLeading(final float leading) {
        this.leading = leading;
    }
    
    public float getLeading() {
        return this.leading;
    }
    
    protected PdfLine createLine(final float width) {
        if (this.chunks.isEmpty()) {
            return null;
        }
        this.splittedChunkText = null;
        this.currentStandbyChunk = null;
        final PdfLine line = new PdfLine(0.0f, width, this.alignment, 0.0f);
        this.currentChunkMarker = 0;
        while (this.currentChunkMarker < this.chunks.size()) {
            final PdfChunk original = this.chunks.get(this.currentChunkMarker);
            final String total = original.toString();
            this.currentStandbyChunk = line.add(original);
            if (this.currentStandbyChunk != null) {
                this.splittedChunkText = original.toString();
                original.setValue(total);
                return line;
            }
            ++this.currentChunkMarker;
        }
        return line;
    }
    
    protected void shortenChunkArray() {
        if (this.currentChunkMarker < 0) {
            return;
        }
        if (this.currentChunkMarker >= this.chunks.size()) {
            this.chunks.clear();
            return;
        }
        final PdfChunk split = this.chunks.get(this.currentChunkMarker);
        split.setValue(this.splittedChunkText);
        this.chunks.set(this.currentChunkMarker, this.currentStandbyChunk);
        for (int j = this.currentChunkMarker - 1; j >= 0; --j) {
            this.chunks.remove(j);
        }
    }
    
    public int go() {
        return this.go(false);
    }
    
    public int go(final boolean simulate) {
        boolean dirty = false;
        PdfContentByte graphics = null;
        if (this.text != null) {
            graphics = this.text.getDuplicate();
        }
        else if (!simulate) {
            throw new NullPointerException(MessageLocalization.getComposedMessage("verticaltext.go.with.simulate.eq.eq.false.and.text.eq.eq.null", new Object[0]));
        }
        int status = 0;
        while (true) {
            while (this.maxLines > 0) {
                if (this.chunks.isEmpty()) {
                    status = 1;
                    if (dirty) {
                        this.text.endText();
                        this.text.add(graphics);
                    }
                    return status;
                }
                final PdfLine line = this.createLine(this.height);
                if (!simulate && !dirty) {
                    this.text.beginText();
                    dirty = true;
                }
                this.shortenChunkArray();
                if (!simulate) {
                    this.text.setTextMatrix(this.startX, this.startY - line.indentLeft());
                    this.writeLine(line, this.text, graphics);
                }
                --this.maxLines;
                this.startX -= this.leading;
            }
            status = 2;
            if (this.chunks.isEmpty()) {
                status |= 0x1;
            }
            continue;
        }
    }
    
    void writeLine(final PdfLine line, final PdfContentByte text, final PdfContentByte graphics) {
        PdfFont currentFont = null;
        for (final PdfChunk chunk : line) {
            if (!chunk.isImage() && chunk.font().compareTo(currentFont) != 0) {
                currentFont = chunk.font();
                text.setFontAndSize(currentFont.getFont(), currentFont.size());
            }
            final Object[] textRender = (Object[])chunk.getAttribute("TEXTRENDERMODE");
            int tr = 0;
            float strokeWidth = 1.0f;
            final BaseColor color = chunk.color();
            BaseColor strokeColor = null;
            if (textRender != null) {
                tr = ((int)textRender[0] & 0x3);
                if (tr != 0) {
                    text.setTextRenderingMode(tr);
                }
                if (tr == 1 || tr == 2) {
                    strokeWidth = (float)textRender[1];
                    if (strokeWidth != 1.0f) {
                        text.setLineWidth(strokeWidth);
                    }
                    strokeColor = (BaseColor)textRender[2];
                    if (strokeColor == null) {
                        strokeColor = color;
                    }
                    if (strokeColor != null) {
                        text.setColorStroke(strokeColor);
                    }
                }
            }
            final Float charSpace = (Float)chunk.getAttribute("CHAR_SPACING");
            if (charSpace != null && !this.curCharSpace.equals(charSpace)) {
                this.curCharSpace = charSpace;
                text.setCharacterSpacing(this.curCharSpace);
            }
            if (color != null) {
                text.setColorFill(color);
            }
            text.showText(chunk.toString());
            if (color != null) {
                text.resetRGBColorFill();
            }
            if (tr != 0) {
                text.setTextRenderingMode(0);
            }
            if (strokeColor != null) {
                text.resetRGBColorStroke();
            }
            if (strokeWidth != 1.0f) {
                text.setLineWidth(1.0f);
            }
        }
    }
    
    public void setOrigin(final float startX, final float startY) {
        this.startX = startX;
        this.startY = startY;
    }
    
    public float getOriginX() {
        return this.startX;
    }
    
    public float getOriginY() {
        return this.startY;
    }
    
    public int getMaxLines() {
        return this.maxLines;
    }
    
    public void setMaxLines(final int maxLines) {
        this.maxLines = maxLines;
    }
    
    public float getHeight() {
        return this.height;
    }
    
    public void setHeight(final float height) {
        this.height = height;
    }
    
    public void setAlignment(final int alignment) {
        this.alignment = alignment;
    }
    
    public int getAlignment() {
        return this.alignment;
    }
}
