// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.util.HashMap;

public class PdfPage extends PdfDictionary
{
    private static final String[] boxStrings;
    private static final PdfName[] boxNames;
    public static final PdfNumber PORTRAIT;
    public static final PdfNumber LANDSCAPE;
    public static final PdfNumber INVERTEDPORTRAIT;
    public static final PdfNumber SEASCAPE;
    PdfRectangle mediaBox;
    
    PdfPage(final PdfRectangle mediaBox, final HashMap<String, PdfRectangle> boxSize, final PdfDictionary resources, final int rotate) throws DocumentException {
        super(PdfPage.PAGE);
        this.mediaBox = mediaBox;
        if (mediaBox != null && (mediaBox.width() > 14400.0f || mediaBox.height() > 14400.0f)) {
            throw new DocumentException(MessageLocalization.getComposedMessage("the.page.size.must.be.smaller.than.14400.by.14400.its.1.by.2", mediaBox.width(), mediaBox.height()));
        }
        this.put(PdfName.MEDIABOX, mediaBox);
        this.put(PdfName.RESOURCES, resources);
        if (rotate != 0) {
            this.put(PdfName.ROTATE, new PdfNumber(rotate));
        }
        for (int k = 0; k < PdfPage.boxStrings.length; ++k) {
            final PdfObject rect = boxSize.get(PdfPage.boxStrings[k]);
            if (rect != null) {
                this.put(PdfPage.boxNames[k], rect);
            }
        }
    }
    
    PdfPage(final PdfRectangle mediaBox, final HashMap<String, PdfRectangle> boxSize, final PdfDictionary resources) throws DocumentException {
        this(mediaBox, boxSize, resources, 0);
    }
    
    public boolean isParent() {
        return false;
    }
    
    void add(final PdfIndirectReference contents) {
        this.put(PdfName.CONTENTS, contents);
    }
    
    PdfRectangle rotateMediaBox() {
        this.mediaBox = this.mediaBox.rotate();
        this.put(PdfName.MEDIABOX, this.mediaBox);
        return this.mediaBox;
    }
    
    PdfRectangle getMediaBox() {
        return this.mediaBox;
    }
    
    static {
        boxStrings = new String[] { "crop", "trim", "art", "bleed" };
        boxNames = new PdfName[] { PdfName.CROPBOX, PdfName.TRIMBOX, PdfName.ARTBOX, PdfName.BLEEDBOX };
        PORTRAIT = new PdfNumber(0);
        LANDSCAPE = new PdfNumber(90);
        INVERTEDPORTRAIT = new PdfNumber(180);
        SEASCAPE = new PdfNumber(270);
    }
}
