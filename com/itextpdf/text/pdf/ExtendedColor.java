// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.BaseColor;

public abstract class ExtendedColor extends BaseColor
{
    private static final long serialVersionUID = 2722660170712380080L;
    public static final int TYPE_RGB = 0;
    public static final int TYPE_GRAY = 1;
    public static final int TYPE_CMYK = 2;
    public static final int TYPE_SEPARATION = 3;
    public static final int TYPE_PATTERN = 4;
    public static final int TYPE_SHADING = 5;
    public static final int TYPE_DEVICEN = 6;
    public static final int TYPE_LAB = 7;
    protected int type;
    
    public ExtendedColor(final int type) {
        super(0, 0, 0);
        this.type = type;
    }
    
    public ExtendedColor(final int type, final float red, final float green, final float blue) {
        super(normalize(red), normalize(green), normalize(blue));
        this.type = type;
    }
    
    public ExtendedColor(final int type, final int red, final int green, final int blue, final int alpha) {
        super(normalize(red / 255.0f), normalize(green / 255.0f), normalize(blue / 255.0f), normalize(alpha / 255.0f));
        this.type = type;
    }
    
    public int getType() {
        return this.type;
    }
    
    public static int getType(final BaseColor color) {
        if (color instanceof ExtendedColor) {
            return ((ExtendedColor)color).getType();
        }
        return 0;
    }
    
    static final float normalize(final float value) {
        if (value < 0.0f) {
            return 0.0f;
        }
        if (value > 1.0f) {
            return 1.0f;
        }
        return value;
    }
}
