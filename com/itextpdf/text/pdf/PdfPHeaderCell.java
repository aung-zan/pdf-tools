// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public class PdfPHeaderCell extends PdfPCell
{
    public static final int NONE = 0;
    public static final int ROW = 1;
    public static final int COLUMN = 2;
    public static final int BOTH = 3;
    protected int scope;
    protected String name;
    
    public PdfPHeaderCell() {
        this.scope = 0;
        this.name = null;
        this.role = PdfName.TH;
    }
    
    public PdfPHeaderCell(final PdfPHeaderCell headerCell) {
        super(headerCell);
        this.scope = 0;
        this.name = null;
        this.role = headerCell.role;
        this.scope = headerCell.scope;
        this.name = headerCell.getName();
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    @Override
    public PdfName getRole() {
        return this.role;
    }
    
    @Override
    public void setRole(final PdfName role) {
        this.role = role;
    }
    
    public void setScope(final int scope) {
        this.scope = scope;
    }
    
    public int getScope() {
        return this.scope;
    }
}
