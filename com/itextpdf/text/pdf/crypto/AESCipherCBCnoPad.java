// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.crypto;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.BlockCipher;

public class AESCipherCBCnoPad
{
    private BlockCipher cbc;
    
    public AESCipherCBCnoPad(final boolean forEncryption, final byte[] key) {
        final BlockCipher aes = new AESFastEngine();
        this.cbc = new CBCBlockCipher(aes);
        final KeyParameter kp = new KeyParameter(key);
        this.cbc.init(forEncryption, kp);
    }
    
    public byte[] processBlock(final byte[] inp, int inpOff, int inpLen) {
        if (inpLen % this.cbc.getBlockSize() != 0) {
            throw new IllegalArgumentException("Not multiple of block: " + inpLen);
        }
        final byte[] outp = new byte[inpLen];
        for (int baseOffset = 0; inpLen > 0; inpLen -= this.cbc.getBlockSize(), baseOffset += this.cbc.getBlockSize(), inpOff += this.cbc.getBlockSize()) {
            this.cbc.processBlock(inp, inpOff, outp, baseOffset);
        }
        return outp;
    }
}
