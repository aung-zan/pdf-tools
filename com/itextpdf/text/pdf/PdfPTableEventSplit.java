// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public interface PdfPTableEventSplit extends PdfPTableEvent
{
    void splitTable(final PdfPTable p0);
}
