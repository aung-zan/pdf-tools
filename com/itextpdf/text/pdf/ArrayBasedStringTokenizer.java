// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.regex.Matcher;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class ArrayBasedStringTokenizer
{
    private final Pattern regex;
    
    public ArrayBasedStringTokenizer(final String[] tokens) {
        this.regex = Pattern.compile(this.getRegexFromTokens(tokens));
    }
    
    public String[] tokenize(final String text) {
        final List<String> tokens = new ArrayList<String>();
        final Matcher matcher = this.regex.matcher(text);
        int endIndexOfpreviousMatch = 0;
        while (matcher.find()) {
            final int startIndexOfMatch = matcher.start();
            final String previousToken = text.substring(endIndexOfpreviousMatch, startIndexOfMatch);
            if (previousToken.length() > 0) {
                tokens.add(previousToken);
            }
            final String currentMatch = matcher.group();
            tokens.add(currentMatch);
            endIndexOfpreviousMatch = matcher.end();
        }
        final String tail = text.substring(endIndexOfpreviousMatch, text.length());
        if (tail.length() > 0) {
            tokens.add(tail);
        }
        return tokens.toArray(new String[0]);
    }
    
    private String getRegexFromTokens(final String[] tokens) {
        final StringBuilder regexBuilder = new StringBuilder(100);
        for (final String token : tokens) {
            regexBuilder.append("(").append(token).append(")|");
        }
        regexBuilder.setLength(regexBuilder.length() - 1);
        final String regex = regexBuilder.toString();
        return regex;
    }
}
