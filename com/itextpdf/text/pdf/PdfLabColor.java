// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.Arrays;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.error_messages.MessageLocalization;

public class PdfLabColor implements ICachedColorSpace
{
    float[] whitePoint;
    float[] blackPoint;
    float[] range;
    
    public PdfLabColor() {
        this.whitePoint = new float[] { 0.9505f, 1.0f, 1.089f };
        this.blackPoint = null;
        this.range = null;
    }
    
    public PdfLabColor(final float[] whitePoint) {
        this.whitePoint = new float[] { 0.9505f, 1.0f, 1.089f };
        this.blackPoint = null;
        this.range = null;
        if (whitePoint == null || whitePoint.length != 3 || whitePoint[0] < 1.0E-6f || whitePoint[2] < 1.0E-6f || whitePoint[1] < 0.999999f || whitePoint[1] > 1.000001f) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("lab.cs.white.point", new Object[0]));
        }
        this.whitePoint = whitePoint;
    }
    
    public PdfLabColor(final float[] whitePoint, final float[] blackPoint) {
        this(whitePoint);
        this.blackPoint = blackPoint;
    }
    
    public PdfLabColor(final float[] whitePoint, final float[] blackPoint, final float[] range) {
        this(whitePoint, blackPoint);
        this.range = range;
    }
    
    @Override
    public PdfObject getPdfObject(final PdfWriter writer) {
        final PdfArray array = new PdfArray(PdfName.LAB);
        final PdfDictionary dictionary = new PdfDictionary();
        if (this.whitePoint == null || this.whitePoint.length != 3 || this.whitePoint[0] < 1.0E-6f || this.whitePoint[2] < 1.0E-6f || this.whitePoint[1] < 0.999999f || this.whitePoint[1] > 1.000001f) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("lab.cs.white.point", new Object[0]));
        }
        dictionary.put(PdfName.WHITEPOINT, new PdfArray(this.whitePoint));
        if (this.blackPoint != null) {
            if (this.blackPoint.length != 3 || this.blackPoint[0] < -1.0E-6f || this.blackPoint[1] < -1.0E-6f || this.blackPoint[2] < -1.0E-6f) {
                throw new RuntimeException(MessageLocalization.getComposedMessage("lab.cs.black.point", new Object[0]));
            }
            dictionary.put(PdfName.BLACKPOINT, new PdfArray(this.blackPoint));
        }
        if (this.range != null) {
            if (this.range.length != 4 || this.range[0] > this.range[1] || this.range[2] > this.range[3]) {
                throw new RuntimeException(MessageLocalization.getComposedMessage("lab.cs.range", new Object[0]));
            }
            dictionary.put(PdfName.RANGE, new PdfArray(this.range));
        }
        array.add(dictionary);
        return array;
    }
    
    public BaseColor lab2Rgb(final float l, final float a, final float b) {
        final double[] clinear = this.lab2RgbLinear(l, a, b);
        return new BaseColor((float)clinear[0], (float)clinear[1], (float)clinear[2]);
    }
    
    CMYKColor lab2Cmyk(final float l, final float a, final float b) {
        final double[] clinear = this.lab2RgbLinear(l, a, b);
        final double r = clinear[0];
        final double g = clinear[1];
        final double bee = clinear[2];
        double computedC = 0.0;
        double computedM = 0.0;
        double computedY = 0.0;
        double computedK = 0.0;
        if (r == 0.0 && g == 0.0 && b == 0.0f) {
            computedK = 1.0;
        }
        else {
            computedC = 1.0 - r;
            computedM = 1.0 - g;
            computedY = 1.0 - bee;
            final double minCMY = Math.min(computedC, Math.min(computedM, computedY));
            computedC = (computedC - minCMY) / (1.0 - minCMY);
            computedM = (computedM - minCMY) / (1.0 - minCMY);
            computedY = (computedY - minCMY) / (1.0 - minCMY);
            computedK = minCMY;
        }
        return new CMYKColor((float)computedC, (float)computedM, (float)computedY, (float)computedK);
    }
    
    protected double[] lab2RgbLinear(final float l, float a, float b) {
        if (this.range != null && this.range.length == 4) {
            if (a < this.range[0]) {
                a = this.range[0];
            }
            if (a > this.range[1]) {
                a = this.range[1];
            }
            if (b < this.range[2]) {
                b = this.range[2];
            }
            if (b > this.range[3]) {
                b = this.range[3];
            }
        }
        final double theta = 0.20689655172413793;
        final double fy = (l + 16.0f) / 116.0;
        final double fx = fy + a / 500.0;
        final double fz = fy - b / 200.0;
        final double x = (fx > theta) ? (this.whitePoint[0] * (fx * fx * fx)) : ((fx - 0.13793103448275862) * 3.0 * (theta * theta) * this.whitePoint[0]);
        final double y = (fy > theta) ? (this.whitePoint[1] * (fy * fy * fy)) : ((fy - 0.13793103448275862) * 3.0 * (theta * theta) * this.whitePoint[1]);
        final double z = (fz > theta) ? (this.whitePoint[2] * (fz * fz * fz)) : ((fz - 0.13793103448275862) * 3.0 * (theta * theta) * this.whitePoint[2]);
        final double[] clinear = { x * 3.241 - y * 1.5374 - z * 0.4986, -x * 0.9692 + y * 1.876 - z * 0.0416, x * 0.0556 - y * 0.204 + z * 1.057 };
        for (int i = 0; i < 3; ++i) {
            clinear[i] = ((clinear[i] <= 0.0031308) ? (12.92 * clinear[i]) : (1.055 * Math.pow(clinear[i], 0.4166666666666667) - 0.055));
            if (clinear[i] < 0.0) {
                clinear[i] = 0.0;
            }
            else if (clinear[i] > 1.0) {
                clinear[i] = 1.0;
            }
        }
        return clinear;
    }
    
    public LabColor rgb2lab(final BaseColor baseColor) {
        final double rLinear = baseColor.getRed() / 255.0f;
        final double gLinear = baseColor.getGreen() / 255.0f;
        final double bLinear = baseColor.getBlue() / 255.0f;
        final double r = (rLinear > 0.04045) ? Math.pow((rLinear + 0.055) / 1.055, 2.2) : (rLinear / 12.92);
        final double g = (gLinear > 0.04045) ? Math.pow((gLinear + 0.055) / 1.055, 2.2) : (gLinear / 12.92);
        final double b = (bLinear > 0.04045) ? Math.pow((bLinear + 0.055) / 1.055, 2.2) : (bLinear / 12.92);
        final double x = r * 0.4124 + g * 0.3576 + b * 0.1805;
        final double y = r * 0.2126 + g * 0.7152 + b * 0.0722;
        final double z = r * 0.0193 + g * 0.1192 + b * 0.9505;
        final float l = Math.round((116.0 * fXyz(y / this.whitePoint[1]) - 16.0) * 1000.0) / 1000.0f;
        final float a = Math.round(500.0 * (fXyz(x / this.whitePoint[0]) - fXyz(y / this.whitePoint[1])) * 1000.0) / 1000.0f;
        final float bee = Math.round(200.0 * (fXyz(y / this.whitePoint[1]) - fXyz(z / this.whitePoint[2])) * 1000.0) / 1000.0f;
        return new LabColor(this, l, a, bee);
    }
    
    private static double fXyz(final double t) {
        return (t > 0.008856) ? Math.pow(t, 0.3333333333333333) : (7.787 * t + 0.13793103448275862);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PdfLabColor)) {
            return false;
        }
        final PdfLabColor that = (PdfLabColor)o;
        return Arrays.equals(this.blackPoint, that.blackPoint) && Arrays.equals(this.range, that.range) && Arrays.equals(this.whitePoint, that.whitePoint);
    }
    
    @Override
    public int hashCode() {
        int result = Arrays.hashCode(this.whitePoint);
        result = 31 * result + ((this.blackPoint != null) ? Arrays.hashCode(this.blackPoint) : 0);
        result = 31 * result + ((this.range != null) ? Arrays.hashCode(this.range) : 0);
        return result;
    }
}
