// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public class PdfSigLockDictionary extends PdfDictionary
{
    public PdfSigLockDictionary() {
        super(PdfName.SIGFIELDLOCK);
        this.put(PdfName.ACTION, LockAction.ALL.getValue());
    }
    
    public PdfSigLockDictionary(final LockPermissions p) {
        this();
        this.put(PdfName.P, p.getValue());
    }
    
    public PdfSigLockDictionary(final LockAction action, final String... fields) {
        this(action, (LockPermissions)null, fields);
    }
    
    public PdfSigLockDictionary(final LockAction action, final LockPermissions p, final String... fields) {
        super(PdfName.SIGFIELDLOCK);
        this.put(PdfName.ACTION, action.getValue());
        if (p != null) {
            this.put(PdfName.P, p.getValue());
        }
        final PdfArray fieldsArray = new PdfArray();
        for (final String field : fields) {
            fieldsArray.add(new PdfString(field));
        }
        this.put(PdfName.FIELDS, fieldsArray);
    }
    
    public enum LockAction
    {
        ALL(PdfName.ALL), 
        INCLUDE(PdfName.INCLUDE), 
        EXCLUDE(PdfName.EXCLUDE);
        
        private PdfName name;
        
        private LockAction(final PdfName name) {
            this.name = name;
        }
        
        public PdfName getValue() {
            return this.name;
        }
    }
    
    public enum LockPermissions
    {
        NO_CHANGES_ALLOWED(1), 
        FORM_FILLING(2), 
        FORM_FILLING_AND_ANNOTATION(3);
        
        private PdfNumber number;
        
        private LockPermissions(final int p) {
            this.number = new PdfNumber(p);
        }
        
        public PdfNumber getValue() {
            return this.number;
        }
    }
}
