// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public interface ICachedColorSpace
{
    PdfObject getPdfObject(final PdfWriter p0);
    
    boolean equals(final Object p0);
    
    int hashCode();
}
