// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

interface PdfPageElement
{
    void setParent(final PdfIndirectReference p0);
    
    boolean isParent();
}
