// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.Version;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import com.itextpdf.text.Font;
import com.itextpdf.text.TabStop;
import com.itextpdf.text.ImgTemplate;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.ListLabel;
import java.util.Iterator;
import java.util.Map;
import java.io.IOException;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.api.WriterOperation;
import com.itextpdf.text.MarkedObject;
import com.itextpdf.text.MarkedSection;
import com.itextpdf.text.pdf.draw.DrawInterface;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.List;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Annotation;
import com.itextpdf.text.ElementListener;
import com.itextpdf.text.Anchor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Meta;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.internal.PdfAnnotationsImp;
import com.itextpdf.text.pdf.collection.PdfCollection;
import java.text.DecimalFormat;
import java.util.TreeMap;
import com.itextpdf.text.pdf.internal.PdfViewerPreferencesImp;
import java.util.ArrayList;
import java.util.Stack;
import com.itextpdf.text.TabSettings;
import com.itextpdf.text.io.TempFileCache;
import com.itextpdf.text.AccessibleElementId;
import java.util.HashMap;
import com.itextpdf.text.Document;

public class PdfDocument extends Document
{
    protected PdfWriter writer;
    private HashMap<AccessibleElementId, PdfStructureElement> structElements;
    private TempFileCache externalCache;
    private HashMap<AccessibleElementId, TempFileCache.ObjectPosition> externallyStoredStructElements;
    private HashMap<AccessibleElementId, AccessibleElementId> elementsParents;
    private boolean isToUseExternalCache;
    protected boolean openMCDocument;
    protected HashMap<Object, int[]> structParentIndices;
    protected HashMap<Object, Integer> markPoints;
    protected PdfContentByte text;
    protected PdfContentByte graphics;
    protected float leading;
    protected int alignment;
    protected float currentHeight;
    protected boolean isSectionTitle;
    protected PdfAction anchorAction;
    protected TabSettings tabSettings;
    private Stack<Float> leadingStack;
    private PdfBody body;
    protected int textEmptySize;
    protected float nextMarginLeft;
    protected float nextMarginRight;
    protected float nextMarginTop;
    protected float nextMarginBottom;
    protected boolean firstPageEvent;
    protected PdfLine line;
    protected ArrayList<PdfLine> lines;
    protected int lastElementType;
    static final String hangingPunctuation = ".,;:'";
    protected Indentation indentation;
    protected PdfInfo info;
    protected PdfOutline rootOutline;
    protected PdfOutline currentOutline;
    protected PdfViewerPreferencesImp viewerPreferences;
    protected PdfPageLabels pageLabels;
    protected TreeMap<String, Destination> localDestinations;
    int jsCounter;
    protected HashMap<String, PdfObject> documentLevelJS;
    protected static final DecimalFormat SIXTEEN_DIGITS;
    protected HashMap<String, PdfObject> documentFileAttachment;
    protected String openActionName;
    protected PdfAction openActionAction;
    protected PdfDictionary additionalActions;
    protected PdfCollection collection;
    PdfAnnotationsImp annotationsImp;
    protected PdfString language;
    protected Rectangle nextPageSize;
    protected HashMap<String, PdfRectangle> thisBoxSize;
    protected HashMap<String, PdfRectangle> boxSize;
    private boolean pageEmpty;
    protected PdfDictionary pageAA;
    protected PageResources pageResources;
    protected boolean strictImageSequence;
    protected float imageEnd;
    protected Image imageWait;
    private ArrayList<Element> floatingElements;
    
    public PdfDocument() {
        this.structElements = new HashMap<AccessibleElementId, PdfStructureElement>();
        this.externallyStoredStructElements = new HashMap<AccessibleElementId, TempFileCache.ObjectPosition>();
        this.elementsParents = new HashMap<AccessibleElementId, AccessibleElementId>();
        this.isToUseExternalCache = false;
        this.openMCDocument = false;
        this.structParentIndices = new HashMap<Object, int[]>();
        this.markPoints = new HashMap<Object, Integer>();
        this.leading = 0.0f;
        this.alignment = 0;
        this.currentHeight = 0.0f;
        this.isSectionTitle = false;
        this.anchorAction = null;
        this.leadingStack = new Stack<Float>();
        this.firstPageEvent = true;
        this.line = null;
        this.lines = new ArrayList<PdfLine>();
        this.lastElementType = -1;
        this.indentation = new Indentation();
        this.info = new PdfInfo();
        this.viewerPreferences = new PdfViewerPreferencesImp();
        this.localDestinations = new TreeMap<String, Destination>();
        this.documentLevelJS = new HashMap<String, PdfObject>();
        this.documentFileAttachment = new HashMap<String, PdfObject>();
        this.nextPageSize = null;
        this.thisBoxSize = new HashMap<String, PdfRectangle>();
        this.boxSize = new HashMap<String, PdfRectangle>();
        this.pageEmpty = true;
        this.pageAA = null;
        this.strictImageSequence = false;
        this.imageEnd = -1.0f;
        this.imageWait = null;
        this.floatingElements = new ArrayList<Element>();
        this.addProducer();
        this.addCreationDate();
    }
    
    public void addWriter(final PdfWriter writer) throws DocumentException {
        if (this.writer == null) {
            this.writer = writer;
            this.annotationsImp = new PdfAnnotationsImp(writer);
            return;
        }
        throw new DocumentException(MessageLocalization.getComposedMessage("you.can.only.add.a.writer.to.a.pdfdocument.once", new Object[0]));
    }
    
    public float getLeading() {
        return this.leading;
    }
    
    void setLeading(final float leading) {
        this.leading = leading;
    }
    
    protected void pushLeading() {
        this.leadingStack.push(this.leading);
    }
    
    protected void popLeading() {
        this.leading = this.leadingStack.pop();
        if (this.leadingStack.size() > 0) {
            this.leading = this.leadingStack.peek();
        }
    }
    
    public TabSettings getTabSettings() {
        return this.tabSettings;
    }
    
    public void setTabSettings(final TabSettings tabSettings) {
        this.tabSettings = tabSettings;
    }
    
    @Override
    public boolean add(final Element element) throws DocumentException {
        if (this.writer != null && this.writer.isPaused()) {
            return false;
        }
        try {
            if (element.type() != 37) {
                this.flushFloatingElements();
            }
            Label_2455: {
                switch (element.type()) {
                    case 0: {
                        this.info.addkey(((Meta)element).getName(), ((Meta)element).getContent());
                        break Label_2455;
                    }
                    case 1: {
                        this.info.addTitle(((Meta)element).getContent());
                        break Label_2455;
                    }
                    case 2: {
                        this.info.addSubject(((Meta)element).getContent());
                        break Label_2455;
                    }
                    case 3: {
                        this.info.addKeywords(((Meta)element).getContent());
                        break Label_2455;
                    }
                    case 4: {
                        this.info.addAuthor(((Meta)element).getContent());
                        break Label_2455;
                    }
                    case 7: {
                        this.info.addCreator(((Meta)element).getContent());
                        break Label_2455;
                    }
                    case 8: {
                        this.setLanguage(((Meta)element).getContent());
                        break Label_2455;
                    }
                    case 5: {
                        this.info.addProducer();
                        break Label_2455;
                    }
                    case 6: {
                        this.info.addCreationDate();
                        break Label_2455;
                    }
                    case 10: {
                        if (this.line == null) {
                            this.carriageReturn();
                        }
                        PdfChunk chunk = new PdfChunk((Chunk)element, this.anchorAction, this.tabSettings);
                        PdfChunk overflow;
                        while ((overflow = this.line.add(chunk, this.leading)) != null) {
                            this.carriageReturn();
                            final boolean newlineSplit = chunk.isNewlineSplit();
                            chunk = overflow;
                            if (!newlineSplit) {
                                chunk.trimFirstSpace();
                            }
                        }
                        this.pageEmpty = false;
                        if (chunk.isAttribute("NEWPAGE")) {
                            this.newPage();
                        }
                        break Label_2455;
                    }
                    case 17: {
                        final Anchor anchor = (Anchor)element;
                        final String url = anchor.getReference();
                        this.leading = anchor.getLeading();
                        this.pushLeading();
                        if (url != null) {
                            this.anchorAction = new PdfAction(url);
                        }
                        element.process(this);
                        this.anchorAction = null;
                        this.popLeading();
                        break Label_2455;
                    }
                    case 29: {
                        if (this.line == null) {
                            this.carriageReturn();
                        }
                        final Annotation annot = (Annotation)element;
                        Rectangle rect = new Rectangle(0.0f, 0.0f);
                        if (this.line != null) {
                            rect = new Rectangle(annot.llx(this.indentRight() - this.line.widthLeft()), annot.ury(this.indentTop() - this.currentHeight - 20.0f), annot.urx(this.indentRight() - this.line.widthLeft() + 20.0f), annot.lly(this.indentTop() - this.currentHeight));
                        }
                        final PdfAnnotation an = PdfAnnotationsImp.convertAnnotation(this.writer, annot, rect);
                        this.annotationsImp.addPlainAnnotation(an);
                        this.pageEmpty = false;
                        break Label_2455;
                    }
                    case 11: {
                        final TabSettings backupTabSettings = this.tabSettings;
                        if (((Phrase)element).getTabSettings() != null) {
                            this.tabSettings = ((Phrase)element).getTabSettings();
                        }
                        this.leading = ((Phrase)element).getTotalLeading();
                        this.pushLeading();
                        element.process(this);
                        this.tabSettings = backupTabSettings;
                        this.popLeading();
                        break Label_2455;
                    }
                    case 12: {
                        final TabSettings backupTabSettings = this.tabSettings;
                        if (((Phrase)element).getTabSettings() != null) {
                            this.tabSettings = ((Phrase)element).getTabSettings();
                        }
                        final Paragraph paragraph = (Paragraph)element;
                        if (isTagged(this.writer)) {
                            this.flushLines();
                            this.text.openMCBlock(paragraph);
                        }
                        this.addSpacing(paragraph.getSpacingBefore(), this.leading, paragraph.getFont());
                        this.alignment = paragraph.getAlignment();
                        this.leading = paragraph.getTotalLeading();
                        this.pushLeading();
                        this.carriageReturn();
                        if (this.currentHeight + this.calculateLineHeight() > this.indentTop() - this.indentBottom()) {
                            this.newPage();
                        }
                        final Indentation indentation = this.indentation;
                        indentation.indentLeft += paragraph.getIndentationLeft();
                        final Indentation indentation2 = this.indentation;
                        indentation2.indentRight += paragraph.getIndentationRight();
                        this.carriageReturn();
                        final PdfPageEvent pageEvent = this.writer.getPageEvent();
                        if (pageEvent != null && !this.isSectionTitle) {
                            pageEvent.onParagraph(this.writer, this, this.indentTop() - this.currentHeight);
                        }
                        if (paragraph.getKeepTogether()) {
                            this.carriageReturn();
                            final PdfPTable table = new PdfPTable(1);
                            table.setKeepTogether(paragraph.getKeepTogether());
                            table.setWidthPercentage(100.0f);
                            final PdfPCell cell = new PdfPCell();
                            cell.addElement(paragraph);
                            cell.setBorder(0);
                            cell.setPadding(0.0f);
                            table.addCell(cell);
                            final Indentation indentation3 = this.indentation;
                            indentation3.indentLeft -= paragraph.getIndentationLeft();
                            final Indentation indentation4 = this.indentation;
                            indentation4.indentRight -= paragraph.getIndentationRight();
                            this.add(table);
                            final Indentation indentation5 = this.indentation;
                            indentation5.indentLeft += paragraph.getIndentationLeft();
                            final Indentation indentation6 = this.indentation;
                            indentation6.indentRight += paragraph.getIndentationRight();
                        }
                        else {
                            this.line.setExtraIndent(paragraph.getFirstLineIndent());
                            final float oldHeight = this.currentHeight;
                            element.process(this);
                            this.carriageReturn();
                            if (oldHeight != this.currentHeight || this.lines.size() > 0) {
                                this.addSpacing(paragraph.getSpacingAfter(), paragraph.getTotalLeading(), paragraph.getFont(), true);
                            }
                        }
                        if (pageEvent != null && !this.isSectionTitle) {
                            pageEvent.onParagraphEnd(this.writer, this, this.indentTop() - this.currentHeight);
                        }
                        this.alignment = 0;
                        if (this.floatingElements != null && this.floatingElements.size() != 0) {
                            this.flushFloatingElements();
                        }
                        final Indentation indentation7 = this.indentation;
                        indentation7.indentLeft -= paragraph.getIndentationLeft();
                        final Indentation indentation8 = this.indentation;
                        indentation8.indentRight -= paragraph.getIndentationRight();
                        this.carriageReturn();
                        this.tabSettings = backupTabSettings;
                        this.popLeading();
                        if (isTagged(this.writer)) {
                            this.flushLines();
                            this.text.closeMCBlock(paragraph);
                        }
                        break Label_2455;
                    }
                    case 13:
                    case 16: {
                        final Section section = (Section)element;
                        final PdfPageEvent pageEvent2 = this.writer.getPageEvent();
                        final boolean hasTitle = section.isNotAddedYet() && section.getTitle() != null;
                        if (section.isTriggerNewPage()) {
                            this.newPage();
                        }
                        if (hasTitle) {
                            float fith = this.indentTop() - this.currentHeight;
                            final int rotation = this.pageSize.getRotation();
                            if (rotation == 90 || rotation == 180) {
                                fith = this.pageSize.getHeight() - fith;
                            }
                            final PdfDestination destination = new PdfDestination(2, fith);
                            while (this.currentOutline.level() >= section.getDepth()) {
                                this.currentOutline = this.currentOutline.parent();
                            }
                            final PdfOutline outline = new PdfOutline(this.currentOutline, destination, section.getBookmarkTitle(), section.isBookmarkOpen());
                            this.currentOutline = outline;
                        }
                        this.carriageReturn();
                        final Indentation indentation9 = this.indentation;
                        indentation9.sectionIndentLeft += section.getIndentationLeft();
                        final Indentation indentation10 = this.indentation;
                        indentation10.sectionIndentRight += section.getIndentationRight();
                        if (section.isNotAddedYet() && pageEvent2 != null) {
                            if (element.type() == 16) {
                                pageEvent2.onChapter(this.writer, this, this.indentTop() - this.currentHeight, section.getTitle());
                            }
                            else {
                                pageEvent2.onSection(this.writer, this, this.indentTop() - this.currentHeight, section.getDepth(), section.getTitle());
                            }
                        }
                        if (hasTitle) {
                            this.isSectionTitle = true;
                            this.add(section.getTitle());
                            this.isSectionTitle = false;
                        }
                        final Indentation indentation11 = this.indentation;
                        indentation11.sectionIndentLeft += section.getIndentation();
                        element.process(this);
                        this.flushLines();
                        final Indentation indentation12 = this.indentation;
                        indentation12.sectionIndentLeft -= section.getIndentationLeft() + section.getIndentation();
                        final Indentation indentation13 = this.indentation;
                        indentation13.sectionIndentRight -= section.getIndentationRight();
                        if (!section.isComplete() || pageEvent2 == null) {
                            break Label_2455;
                        }
                        if (element.type() == 16) {
                            pageEvent2.onChapterEnd(this.writer, this, this.indentTop() - this.currentHeight);
                            break Label_2455;
                        }
                        pageEvent2.onSectionEnd(this.writer, this, this.indentTop() - this.currentHeight);
                        break Label_2455;
                    }
                    case 14: {
                        final List list = (List)element;
                        if (isTagged(this.writer)) {
                            this.flushLines();
                            this.text.openMCBlock(list);
                        }
                        if (list.isAlignindent()) {
                            list.normalizeIndentation();
                        }
                        final Indentation indentation14 = this.indentation;
                        indentation14.listIndentLeft += list.getIndentationLeft();
                        final Indentation indentation15 = this.indentation;
                        indentation15.indentRight += list.getIndentationRight();
                        element.process(this);
                        final Indentation indentation16 = this.indentation;
                        indentation16.listIndentLeft -= list.getIndentationLeft();
                        final Indentation indentation17 = this.indentation;
                        indentation17.indentRight -= list.getIndentationRight();
                        this.carriageReturn();
                        if (isTagged(this.writer)) {
                            this.flushLines();
                            this.text.closeMCBlock(list);
                        }
                        break Label_2455;
                    }
                    case 15: {
                        final ListItem listItem = (ListItem)element;
                        if (isTagged(this.writer)) {
                            this.flushLines();
                            this.text.openMCBlock(listItem);
                        }
                        this.addSpacing(listItem.getSpacingBefore(), this.leading, listItem.getFont());
                        this.alignment = listItem.getAlignment();
                        final Indentation indentation18 = this.indentation;
                        indentation18.listIndentLeft += listItem.getIndentationLeft();
                        final Indentation indentation19 = this.indentation;
                        indentation19.indentRight += listItem.getIndentationRight();
                        this.leading = listItem.getTotalLeading();
                        this.pushLeading();
                        this.carriageReturn();
                        this.line.setListItem(listItem);
                        element.process(this);
                        this.addSpacing(listItem.getSpacingAfter(), listItem.getTotalLeading(), listItem.getFont(), true);
                        if (this.line.hasToBeJustified()) {
                            this.line.resetAlignment();
                        }
                        this.carriageReturn();
                        final Indentation indentation20 = this.indentation;
                        indentation20.listIndentLeft -= listItem.getIndentationLeft();
                        final Indentation indentation21 = this.indentation;
                        indentation21.indentRight -= listItem.getIndentationRight();
                        this.popLeading();
                        if (isTagged(this.writer)) {
                            this.flushLines();
                            this.text.closeMCBlock(listItem.getListBody());
                            this.text.closeMCBlock(listItem);
                        }
                        break Label_2455;
                    }
                    case 30: {
                        final Rectangle rectangle = (Rectangle)element;
                        this.graphics.rectangle(rectangle);
                        this.pageEmpty = false;
                        break Label_2455;
                    }
                    case 23: {
                        final PdfPTable ptable = (PdfPTable)element;
                        if (ptable.size() <= ptable.getHeaderRows()) {
                            break Label_2455;
                        }
                        this.ensureNewLine();
                        this.flushLines();
                        this.addPTable(ptable);
                        this.pageEmpty = false;
                        this.newLine();
                        break Label_2455;
                    }
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36: {
                        if (isTagged(this.writer) && !((Image)element).isImgTemplate()) {
                            this.flushLines();
                            this.text.openMCBlock((IAccessibleElement)element);
                        }
                        this.add((Image)element);
                        if (isTagged(this.writer) && !((Image)element).isImgTemplate()) {
                            this.flushLines();
                            this.text.closeMCBlock((IAccessibleElement)element);
                        }
                        break Label_2455;
                    }
                    case 55: {
                        final DrawInterface zh = (DrawInterface)element;
                        zh.draw(this.graphics, this.indentLeft(), this.indentBottom(), this.indentRight(), this.indentTop(), this.indentTop() - this.currentHeight - ((this.leadingStack.size() > 0) ? this.leading : 0.0f));
                        this.pageEmpty = false;
                        break Label_2455;
                    }
                    case 50: {
                        if (element instanceof MarkedSection) {
                            final MarkedObject mo = ((MarkedSection)element).getTitle();
                            if (mo != null) {
                                mo.process(this);
                            }
                        }
                        final MarkedObject mo = (MarkedObject)element;
                        mo.process(this);
                        break Label_2455;
                    }
                    case 666: {
                        if (null != this.writer) {
                            ((WriterOperation)element).write(this.writer, this);
                        }
                        break Label_2455;
                    }
                    case 37: {
                        this.ensureNewLine();
                        this.flushLines();
                        this.addDiv((PdfDiv)element);
                        this.pageEmpty = false;
                        break Label_2455;
                    }
                    case 38: {
                        this.body = (PdfBody)element;
                        this.graphics.rectangle(this.body);
                        break;
                    }
                }
                return false;
            }
            this.lastElementType = element.type();
            return true;
        }
        catch (Exception e) {
            throw new DocumentException(e);
        }
    }
    
    @Override
    public void open() {
        if (!this.open) {
            super.open();
            this.writer.open();
            this.rootOutline = new PdfOutline(this.writer);
            this.currentOutline = this.rootOutline;
        }
        try {
            if (isTagged(this.writer)) {
                this.openMCDocument = true;
            }
            this.initPage();
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    @Override
    public void close() {
        if (this.close) {
            return;
        }
        try {
            if (isTagged(this.writer)) {
                this.flushFloatingElements();
                this.flushLines();
                this.writer.flushAcroFields();
                this.writer.flushTaggedObjects();
                if (this.isPageEmpty()) {
                    final int pageReferenceCount = this.writer.pageReferences.size();
                    if (pageReferenceCount > 0 && this.writer.currentPageNumber == pageReferenceCount) {
                        this.writer.pageReferences.remove(pageReferenceCount - 1);
                    }
                }
            }
            else {
                this.writer.flushAcroFields();
            }
            if (this.imageWait != null) {
                this.newPage();
            }
            this.endPage();
            if (isTagged(this.writer)) {
                this.writer.getDirectContent().closeMCBlock(this);
            }
            if (this.annotationsImp.hasUnusedAnnotations()) {
                throw new RuntimeException(MessageLocalization.getComposedMessage("not.all.annotations.could.be.added.to.the.document.the.document.doesn.t.have.enough.pages", new Object[0]));
            }
            final PdfPageEvent pageEvent = this.writer.getPageEvent();
            if (pageEvent != null) {
                pageEvent.onCloseDocument(this.writer, this);
            }
            super.close();
            this.writer.addLocalDestinations(this.localDestinations);
            this.calculateOutlineCount();
            this.writeOutlines();
        }
        catch (Exception e) {
            throw ExceptionConverter.convertException(e);
        }
        this.writer.close();
    }
    
    public void setXmpMetadata(final byte[] xmpMetadata) throws IOException {
        final PdfStream xmp = new PdfStream(xmpMetadata);
        xmp.put(PdfName.TYPE, PdfName.METADATA);
        xmp.put(PdfName.SUBTYPE, PdfName.XML);
        final PdfEncryption crypto = this.writer.getEncryption();
        if (crypto != null && !crypto.isMetadataEncrypted()) {
            final PdfArray ar = new PdfArray();
            ar.add(PdfName.CRYPT);
            xmp.put(PdfName.FILTER, ar);
        }
        this.writer.addPageDictEntry(PdfName.METADATA, this.writer.addToBody(xmp).getIndirectReference());
    }
    
    @Override
    public boolean newPage() {
        if (this.isPageEmpty()) {
            this.setNewPageSizeAndMargins();
            return false;
        }
        if (!this.open || this.close) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("the.document.is.not.open", new Object[0]));
        }
        final ArrayList<IAccessibleElement> savedMcBlocks = this.endPage();
        super.newPage();
        this.indentation.imageIndentLeft = 0.0f;
        this.indentation.imageIndentRight = 0.0f;
        try {
            if (isTagged(this.writer)) {
                this.flushStructureElementsOnNewPage();
                this.writer.getDirectContentUnder().restoreMCBlocks(savedMcBlocks);
            }
            this.initPage();
            if (this.body != null && this.body.getBackgroundColor() != null) {
                this.graphics.rectangle(this.body);
            }
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
        return true;
    }
    
    protected ArrayList<IAccessibleElement> endPage() {
        if (this.isPageEmpty()) {
            return null;
        }
        ArrayList<IAccessibleElement> savedMcBlocks = null;
        try {
            this.flushFloatingElements();
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
        this.lastElementType = -1;
        final PdfPageEvent pageEvent = this.writer.getPageEvent();
        if (pageEvent != null) {
            pageEvent.onEndPage(this.writer, this);
        }
        try {
            this.flushLines();
            final int rotation = this.pageSize.getRotation();
            if (this.writer.isPdfIso()) {
                if (this.thisBoxSize.containsKey("art") && this.thisBoxSize.containsKey("trim")) {
                    throw new PdfXConformanceException(MessageLocalization.getComposedMessage("only.one.of.artbox.or.trimbox.can.exist.in.the.page", new Object[0]));
                }
                if (!this.thisBoxSize.containsKey("art") && !this.thisBoxSize.containsKey("trim")) {
                    if (this.thisBoxSize.containsKey("crop")) {
                        this.thisBoxSize.put("trim", this.thisBoxSize.get("crop"));
                    }
                    else {
                        this.thisBoxSize.put("trim", new PdfRectangle(this.pageSize, this.pageSize.getRotation()));
                    }
                }
            }
            this.pageResources.addDefaultColorDiff(this.writer.getDefaultColorspace());
            if (this.writer.isRgbTransparencyBlending()) {
                final PdfDictionary dcs = new PdfDictionary();
                dcs.put(PdfName.CS, PdfName.DEVICERGB);
                this.pageResources.addDefaultColorDiff(dcs);
            }
            final PdfDictionary resources = this.pageResources.getResources();
            final PdfPage page = new PdfPage(new PdfRectangle(this.pageSize, rotation), this.thisBoxSize, resources, rotation);
            if (isTagged(this.writer)) {
                page.put(PdfName.TABS, PdfName.S);
            }
            else {
                page.put(PdfName.TABS, this.writer.getTabs());
            }
            page.putAll(this.writer.getPageDictEntries());
            this.writer.resetPageDictEntries();
            if (this.pageAA != null) {
                page.put(PdfName.AA, this.writer.addToBody(this.pageAA).getIndirectReference());
                this.pageAA = null;
            }
            if (this.annotationsImp.hasUnusedAnnotations()) {
                final PdfArray array = this.annotationsImp.rotateAnnotations(this.writer, this.pageSize);
                if (array.size() != 0) {
                    page.put(PdfName.ANNOTS, array);
                }
            }
            if (isTagged(this.writer)) {
                page.put(PdfName.STRUCTPARENTS, new PdfNumber(this.getStructParentIndex(this.writer.getCurrentPage())));
            }
            if (this.text.size() > this.textEmptySize || isTagged(this.writer)) {
                this.text.endText();
            }
            else {
                this.text = null;
            }
            if (isTagged(this.writer)) {
                savedMcBlocks = this.writer.getDirectContent().saveMCBlocks();
            }
            this.writer.add(page, new PdfContents(this.writer.getDirectContentUnder(), this.graphics, isTagged(this.writer) ? null : this.text, this.writer.getDirectContent(), this.pageSize));
            this.annotationsImp.resetAnnotations();
            this.writer.resetContent();
        }
        catch (DocumentException de2) {
            throw new ExceptionConverter(de2);
        }
        catch (IOException ioe) {
            throw new ExceptionConverter(ioe);
        }
        return savedMcBlocks;
    }
    
    @Override
    public boolean setPageSize(final Rectangle pageSize) {
        if (this.writer != null && this.writer.isPaused()) {
            return false;
        }
        this.nextPageSize = new Rectangle(pageSize);
        return true;
    }
    
    @Override
    public boolean setMargins(final float marginLeft, final float marginRight, final float marginTop, final float marginBottom) {
        if (this.writer != null && this.writer.isPaused()) {
            return false;
        }
        this.nextMarginLeft = marginLeft;
        this.nextMarginRight = marginRight;
        this.nextMarginTop = marginTop;
        this.nextMarginBottom = marginBottom;
        return true;
    }
    
    @Override
    public boolean setMarginMirroring(final boolean MarginMirroring) {
        return (this.writer == null || !this.writer.isPaused()) && super.setMarginMirroring(MarginMirroring);
    }
    
    @Override
    public boolean setMarginMirroringTopBottom(final boolean MarginMirroringTopBottom) {
        return (this.writer == null || !this.writer.isPaused()) && super.setMarginMirroringTopBottom(MarginMirroringTopBottom);
    }
    
    @Override
    public void setPageCount(final int pageN) {
        if (this.writer != null && this.writer.isPaused()) {
            return;
        }
        super.setPageCount(pageN);
    }
    
    @Override
    public void resetPageCount() {
        if (this.writer != null && this.writer.isPaused()) {
            return;
        }
        super.resetPageCount();
    }
    
    protected void initPage() throws DocumentException {
        ++this.pageN;
        this.pageResources = new PageResources();
        if (isTagged(this.writer)) {
            this.graphics = this.writer.getDirectContentUnder().getDuplicate();
            this.writer.getDirectContent().duplicatedFrom = this.graphics;
        }
        else {
            this.graphics = new PdfContentByte(this.writer);
        }
        this.setNewPageSizeAndMargins();
        this.imageEnd = -1.0f;
        this.indentation.imageIndentRight = 0.0f;
        this.indentation.imageIndentLeft = 0.0f;
        this.indentation.indentBottom = 0.0f;
        this.indentation.indentTop = 0.0f;
        this.currentHeight = 0.0f;
        this.thisBoxSize = new HashMap<String, PdfRectangle>(this.boxSize);
        if (this.pageSize.getBackgroundColor() != null || this.pageSize.hasBorders() || this.pageSize.getBorderColor() != null) {
            this.add(this.pageSize);
        }
        final float oldleading = this.leading;
        final int oldAlignment = this.alignment;
        this.pageEmpty = true;
        try {
            if (this.imageWait != null) {
                this.add(this.imageWait);
                this.imageWait = null;
            }
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
        this.leading = oldleading;
        this.alignment = oldAlignment;
        this.carriageReturn();
        final PdfPageEvent pageEvent = this.writer.getPageEvent();
        if (pageEvent != null) {
            if (this.firstPageEvent) {
                pageEvent.onOpenDocument(this.writer, this);
            }
            pageEvent.onStartPage(this.writer, this);
        }
        this.firstPageEvent = false;
    }
    
    protected void newLine() throws DocumentException {
        this.lastElementType = -1;
        this.carriageReturn();
        if (this.lines != null && !this.lines.isEmpty()) {
            this.lines.add(this.line);
            this.currentHeight += this.line.height();
        }
        this.line = new PdfLine(this.indentLeft(), this.indentRight(), this.alignment, this.leading);
    }
    
    protected float calculateLineHeight() {
        float tempHeight = this.line.height();
        if (tempHeight != this.leading) {
            tempHeight += this.leading;
        }
        return tempHeight;
    }
    
    protected void carriageReturn() {
        if (this.lines == null) {
            this.lines = new ArrayList<PdfLine>();
        }
        if (this.line != null && this.line.size() > 0) {
            if (this.currentHeight + this.calculateLineHeight() > this.indentTop() - this.indentBottom() && this.currentHeight != 0.0f) {
                final PdfLine overflowLine = this.line;
                this.line = null;
                this.newPage();
                this.line = overflowLine;
                overflowLine.left = this.indentLeft();
            }
            this.currentHeight += this.line.height();
            this.lines.add(this.line);
            this.pageEmpty = false;
        }
        if (this.imageEnd > -1.0f && this.currentHeight > this.imageEnd) {
            this.imageEnd = -1.0f;
            this.indentation.imageIndentRight = 0.0f;
            this.indentation.imageIndentLeft = 0.0f;
        }
        this.line = new PdfLine(this.indentLeft(), this.indentRight(), this.alignment, this.leading);
    }
    
    public float getVerticalPosition(final boolean ensureNewLine) {
        if (ensureNewLine) {
            this.ensureNewLine();
        }
        return this.top() - this.currentHeight - this.indentation.indentTop;
    }
    
    protected void ensureNewLine() {
        try {
            if (this.lastElementType == 11 || this.lastElementType == 10) {
                this.newLine();
                this.flushLines();
            }
        }
        catch (DocumentException ex) {
            throw new ExceptionConverter(ex);
        }
    }
    
    protected float flushLines() throws DocumentException {
        if (this.lines == null) {
            return 0.0f;
        }
        if (this.line != null && this.line.size() > 0) {
            this.lines.add(this.line);
            this.line = new PdfLine(this.indentLeft(), this.indentRight(), this.alignment, this.leading);
        }
        if (this.lines.isEmpty()) {
            return 0.0f;
        }
        final Object[] currentValues = new Object[2];
        PdfFont currentFont = null;
        float displacement = 0.0f;
        final Float lastBaseFactor = new Float(0.0f);
        currentValues[1] = lastBaseFactor;
        for (final PdfLine l : this.lines) {
            final float moveTextX = l.indentLeft() - this.indentLeft() + this.indentation.indentLeft + this.indentation.listIndentLeft + this.indentation.sectionIndentLeft;
            this.text.moveText(moveTextX, -l.height());
            l.flush();
            if (l.listSymbol() != null) {
                ListLabel lbl = null;
                Chunk symbol = l.listSymbol();
                if (isTagged(this.writer)) {
                    lbl = l.listItem().getListLabel();
                    this.graphics.openMCBlock(lbl);
                    symbol = new Chunk(symbol);
                    symbol.setRole(null);
                }
                ColumnText.showTextAligned(this.graphics, 0, new Phrase(symbol), this.text.getXTLM() - l.listIndent(), this.text.getYTLM(), 0.0f);
                if (lbl != null) {
                    this.graphics.closeMCBlock(lbl);
                }
            }
            currentValues[0] = currentFont;
            if (isTagged(this.writer) && l.listItem() != null) {
                this.text.openMCBlock(l.listItem().getListBody());
            }
            this.writeLineToContent(l, this.text, this.graphics, currentValues, this.writer.getSpaceCharRatio());
            currentFont = (PdfFont)currentValues[0];
            displacement += l.height();
            this.text.moveText(-moveTextX, 0.0f);
        }
        this.lines = new ArrayList<PdfLine>();
        return displacement;
    }
    
    float writeLineToContent(final PdfLine line, final PdfContentByte text, final PdfContentByte graphics, final Object[] currentValues, final float ratio) throws DocumentException {
        PdfFont currentFont = (PdfFont)currentValues[0];
        float lastBaseFactor = (float)currentValues[1];
        float hangingCorrection = 0.0f;
        float hScale = 1.0f;
        float lastHScale = Float.NaN;
        float baseWordSpacing = 0.0f;
        float baseCharacterSpacing = 0.0f;
        float glueWidth = 0.0f;
        float lastX = text.getXTLM() + line.getOriginalWidth();
        final int numberOfSpaces = line.numberOfSpaces();
        final int lineLen = line.getLineLengthUtf32();
        final boolean isJustified = line.hasToBeJustified() && (numberOfSpaces != 0 || lineLen > 1);
        final int separatorCount = line.getSeparatorCount();
        if (separatorCount > 0) {
            glueWidth = line.widthLeft() / separatorCount;
        }
        else if (isJustified && separatorCount == 0) {
            if (line.isNewlineSplit() && line.widthLeft() >= lastBaseFactor * (ratio * numberOfSpaces + lineLen - 1.0f)) {
                if (line.isRTL()) {
                    text.moveText(line.widthLeft() - lastBaseFactor * (ratio * numberOfSpaces + lineLen - 1.0f), 0.0f);
                }
                baseWordSpacing = ratio * lastBaseFactor;
                baseCharacterSpacing = lastBaseFactor;
            }
            else {
                float width = line.widthLeft();
                final PdfChunk last = line.getChunk(line.size() - 1);
                if (last != null) {
                    final String s = last.toString();
                    final char c;
                    if (s.length() > 0 && ".,;:'".indexOf(c = s.charAt(s.length() - 1)) >= 0) {
                        final float oldWidth = width;
                        width += last.font().width(c) * 0.4f;
                        hangingCorrection = width - oldWidth;
                    }
                }
                final float baseFactor = width / (ratio * numberOfSpaces + lineLen - 1.0f);
                baseWordSpacing = ratio * baseFactor;
                baseCharacterSpacing = baseFactor;
                lastBaseFactor = baseFactor;
            }
        }
        else if (line.alignment == 0 || line.alignment == -1) {
            lastX -= line.widthLeft();
        }
        final int lastChunkStroke = line.getLastStrokeChunk();
        int chunkStrokeIdx = 0;
        final float baseXMarker;
        float xMarker = baseXMarker = text.getXTLM();
        final float yMarker = text.getYTLM();
        boolean adjustMatrix = false;
        float tabPosition = 0.0f;
        boolean isMCBlockOpened = false;
        for (final PdfChunk chunk : line) {
            if (isTagged(this.writer) && chunk.accessibleElement != null) {
                text.openMCBlock(chunk.accessibleElement);
                isMCBlockOpened = true;
            }
            final BaseColor color = chunk.color();
            float fontSize = chunk.font().size();
            float ascender;
            float descender;
            if (chunk.isImage()) {
                ascender = chunk.height();
                fontSize = chunk.height();
                descender = 0.0f;
            }
            else {
                ascender = chunk.font().getFont().getFontDescriptor(1, fontSize);
                descender = chunk.font().getFont().getFontDescriptor(3, fontSize);
            }
            hScale = 1.0f;
            if (chunkStrokeIdx <= lastChunkStroke) {
                float width2;
                if (isJustified) {
                    width2 = chunk.getWidthCorrected(baseCharacterSpacing, baseWordSpacing);
                }
                else {
                    width2 = chunk.width();
                }
                if (chunk.isStroked()) {
                    final PdfChunk nextChunk = line.getChunk(chunkStrokeIdx + 1);
                    if (chunk.isSeparator()) {
                        width2 = glueWidth;
                        final Object[] sep = (Object[])chunk.getAttribute("SEPARATOR");
                        final DrawInterface di = (DrawInterface)sep[0];
                        final Boolean vertical = (Boolean)sep[1];
                        if (vertical) {
                            di.draw(graphics, baseXMarker, yMarker + descender, baseXMarker + line.getOriginalWidth(), ascender - descender, yMarker);
                        }
                        else {
                            di.draw(graphics, xMarker, yMarker + descender, xMarker + width2, ascender - descender, yMarker);
                        }
                    }
                    if (chunk.isTab()) {
                        if (chunk.isAttribute("TABSETTINGS")) {
                            final TabStop tabStop = chunk.getTabStop();
                            if (tabStop != null) {
                                tabPosition = tabStop.getPosition() + baseXMarker;
                                if (tabStop.getLeader() != null) {
                                    tabStop.getLeader().draw(graphics, xMarker, yMarker + descender, tabPosition, ascender - descender, yMarker);
                                }
                            }
                            else {
                                tabPosition = xMarker;
                            }
                        }
                        else {
                            final Object[] tab = (Object[])chunk.getAttribute("TAB");
                            final DrawInterface di = (DrawInterface)tab[0];
                            tabPosition = (float)tab[1] + (float)tab[3];
                            if (tabPosition > xMarker) {
                                di.draw(graphics, xMarker, yMarker + descender, tabPosition, ascender - descender, yMarker);
                            }
                        }
                        final float tmp = xMarker;
                        xMarker = tabPosition;
                        tabPosition = tmp;
                    }
                    if (chunk.isAttribute("BACKGROUND")) {
                        final Object[] bgr = (Object[])chunk.getAttribute("BACKGROUND");
                        if (bgr[0] != null) {
                            final boolean inText = graphics.getInText();
                            if (inText && isTagged(this.writer)) {
                                graphics.endText();
                            }
                            graphics.saveState();
                            float subtract = lastBaseFactor;
                            if (nextChunk != null && nextChunk.isAttribute("BACKGROUND")) {
                                subtract = 0.0f;
                            }
                            if (nextChunk == null) {
                                subtract += hangingCorrection;
                            }
                            final BaseColor c2 = (BaseColor)bgr[0];
                            graphics.setColorFill(c2);
                            final float[] extra = (float[])bgr[1];
                            graphics.rectangle(xMarker - extra[0], yMarker + descender - extra[1] + chunk.getTextRise(), width2 - subtract + extra[0] + extra[2], ascender - descender + extra[1] + extra[3]);
                            graphics.fill();
                            graphics.setGrayFill(0.0f);
                            graphics.restoreState();
                            if (inText && isTagged(this.writer)) {
                                graphics.beginText(true);
                            }
                        }
                    }
                    if (chunk.isAttribute("UNDERLINE")) {
                        final boolean inText2 = graphics.getInText();
                        if (inText2 && isTagged(this.writer)) {
                            graphics.endText();
                        }
                        float subtract2 = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("UNDERLINE")) {
                            subtract2 = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract2 += hangingCorrection;
                        }
                        final Object[][] unders = (Object[][])chunk.getAttribute("UNDERLINE");
                        BaseColor scolor = null;
                        for (int k = 0; k < unders.length; ++k) {
                            final Object[] obj = unders[k];
                            scolor = (BaseColor)obj[0];
                            final float[] ps = (float[])obj[1];
                            if (scolor == null) {
                                scolor = color;
                            }
                            if (scolor != null) {
                                graphics.setColorStroke(scolor);
                            }
                            graphics.setLineWidth(ps[0] + chunk.font().size() * ps[1]);
                            final float shift = ps[2] + chunk.font().size() * ps[3];
                            final int cap2 = (int)ps[4];
                            if (cap2 != 0) {
                                graphics.setLineCap(cap2);
                            }
                            graphics.moveTo(xMarker, yMarker + shift);
                            graphics.lineTo(xMarker + width2 - subtract2, yMarker + shift);
                            graphics.stroke();
                            if (scolor != null) {
                                graphics.resetGrayStroke();
                            }
                            if (cap2 != 0) {
                                graphics.setLineCap(0);
                            }
                        }
                        graphics.setLineWidth(1.0f);
                        if (inText2 && isTagged(this.writer)) {
                            graphics.beginText(true);
                        }
                    }
                    if (chunk.isAttribute("ACTION")) {
                        float subtract3 = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("ACTION")) {
                            subtract3 = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract3 += hangingCorrection;
                        }
                        PdfAnnotation annot = null;
                        if (chunk.isImage()) {
                            annot = this.writer.createAnnotation(xMarker, yMarker + chunk.getImageOffsetY(), xMarker + width2 - subtract3, yMarker + chunk.getImageHeight() + chunk.getImageOffsetY(), (PdfAction)chunk.getAttribute("ACTION"), null);
                        }
                        else {
                            annot = this.writer.createAnnotation(xMarker, yMarker + descender + chunk.getTextRise(), xMarker + width2 - subtract3, yMarker + ascender + chunk.getTextRise(), (PdfAction)chunk.getAttribute("ACTION"), null);
                        }
                        text.addAnnotation(annot, true);
                        if (isTagged(this.writer) && chunk.accessibleElement != null) {
                            final PdfStructureElement strucElem = this.getStructElement(chunk.accessibleElement.getId());
                            if (strucElem != null) {
                                final int structParent = this.getStructParentIndex(annot);
                                annot.put(PdfName.STRUCTPARENT, new PdfNumber(structParent));
                                strucElem.setAnnotation(annot, this.writer.getCurrentPage());
                                this.writer.getStructureTreeRoot().setAnnotationMark(structParent, strucElem.getReference());
                            }
                        }
                    }
                    if (chunk.isAttribute("REMOTEGOTO")) {
                        float subtract3 = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("REMOTEGOTO")) {
                            subtract3 = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract3 += hangingCorrection;
                        }
                        final Object[] obj2 = (Object[])chunk.getAttribute("REMOTEGOTO");
                        final String filename = (String)obj2[0];
                        if (obj2[1] instanceof String) {
                            this.remoteGoto(filename, (String)obj2[1], xMarker, yMarker + descender + chunk.getTextRise(), xMarker + width2 - subtract3, yMarker + ascender + chunk.getTextRise());
                        }
                        else {
                            this.remoteGoto(filename, (int)obj2[1], xMarker, yMarker + descender + chunk.getTextRise(), xMarker + width2 - subtract3, yMarker + ascender + chunk.getTextRise());
                        }
                    }
                    if (chunk.isAttribute("LOCALGOTO")) {
                        float subtract3 = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("LOCALGOTO")) {
                            subtract3 = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract3 += hangingCorrection;
                        }
                        this.localGoto((String)chunk.getAttribute("LOCALGOTO"), xMarker, yMarker, xMarker + width2 - subtract3, yMarker + fontSize);
                    }
                    if (chunk.isAttribute("LOCALDESTINATION")) {
                        this.localDestination((String)chunk.getAttribute("LOCALDESTINATION"), new PdfDestination(0, xMarker, yMarker + fontSize, 0.0f));
                    }
                    if (chunk.isAttribute("GENERICTAG")) {
                        float subtract3 = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("GENERICTAG")) {
                            subtract3 = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract3 += hangingCorrection;
                        }
                        final Rectangle rect = new Rectangle(xMarker, yMarker, xMarker + width2 - subtract3, yMarker + fontSize);
                        final PdfPageEvent pev = this.writer.getPageEvent();
                        if (pev != null) {
                            pev.onGenericTag(this.writer, this, rect, (String)chunk.getAttribute("GENERICTAG"));
                        }
                    }
                    if (chunk.isAttribute("PDFANNOTATION")) {
                        float subtract3 = lastBaseFactor;
                        if (nextChunk != null && nextChunk.isAttribute("PDFANNOTATION")) {
                            subtract3 = 0.0f;
                        }
                        if (nextChunk == null) {
                            subtract3 += hangingCorrection;
                        }
                        final PdfAnnotation annot = PdfFormField.shallowDuplicate((PdfAnnotation)chunk.getAttribute("PDFANNOTATION"));
                        annot.put(PdfName.RECT, new PdfRectangle(xMarker, yMarker + descender, xMarker + width2 - subtract3, yMarker + ascender));
                        text.addAnnotation(annot, true);
                    }
                    final float[] params = (float[])chunk.getAttribute("SKEW");
                    final Float hs = (Float)chunk.getAttribute("HSCALE");
                    if (params != null || hs != null) {
                        float b = 0.0f;
                        float c3 = 0.0f;
                        if (params != null) {
                            b = params[0];
                            c3 = params[1];
                        }
                        if (hs != null) {
                            hScale = hs;
                        }
                        text.setTextMatrix(hScale, b, c3, 1.0f, xMarker, yMarker);
                    }
                    if (!isJustified) {
                        if (chunk.isAttribute("WORD_SPACING")) {
                            final Float ws = (Float)chunk.getAttribute("WORD_SPACING");
                            text.setWordSpacing(ws);
                        }
                        if (chunk.isAttribute("CHAR_SPACING")) {
                            final Float cs = (Float)chunk.getAttribute("CHAR_SPACING");
                            text.setCharacterSpacing(cs);
                        }
                    }
                    if (chunk.isImage()) {
                        final Image image = chunk.getImage();
                        width2 = chunk.getImageWidth();
                        final float[] matrix = image.matrix(chunk.getImageScalePercentage());
                        matrix[4] = xMarker + chunk.getImageOffsetX() - matrix[4];
                        matrix[5] = yMarker + chunk.getImageOffsetY() - matrix[5];
                        boolean wasIntext = false;
                        if (graphics.getInText() && !(image instanceof ImgTemplate)) {
                            wasIntext = true;
                            graphics.endText();
                        }
                        graphics.addImage(image, matrix[0], matrix[1], matrix[2], matrix[3], matrix[4], matrix[5], false, isMCBlockOpened);
                        if (wasIntext) {
                            graphics.beginText(true);
                        }
                        text.moveText(xMarker + lastBaseFactor + chunk.getImageWidth() - text.getXTLM(), 0.0f);
                    }
                }
                xMarker += width2;
                ++chunkStrokeIdx;
            }
            if (!chunk.isImage() && chunk.font().compareTo(currentFont) != 0) {
                currentFont = chunk.font();
                text.setFontAndSize(currentFont.getFont(), currentFont.size());
            }
            float rise = 0.0f;
            final Object[] textRender = (Object[])chunk.getAttribute("TEXTRENDERMODE");
            int tr = 0;
            float strokeWidth = 1.0f;
            BaseColor strokeColor = null;
            final Float fr = (Float)chunk.getAttribute("SUBSUPSCRIPT");
            if (textRender != null) {
                tr = ((int)textRender[0] & 0x3);
                if (tr != 0) {
                    text.setTextRenderingMode(tr);
                }
                if (tr == 1 || tr == 2) {
                    strokeWidth = (float)textRender[1];
                    if (strokeWidth != 1.0f) {
                        text.setLineWidth(strokeWidth);
                    }
                    strokeColor = (BaseColor)textRender[2];
                    if (strokeColor == null) {
                        strokeColor = color;
                    }
                    if (strokeColor != null) {
                        text.setColorStroke(strokeColor);
                    }
                }
            }
            if (fr != null) {
                rise = fr;
            }
            if (color != null) {
                text.setColorFill(color);
            }
            if (rise != 0.0f) {
                text.setTextRise(rise);
            }
            if (chunk.isImage()) {
                adjustMatrix = true;
            }
            else if (chunk.isHorizontalSeparator()) {
                final PdfTextArray array = new PdfTextArray();
                array.add(-glueWidth * 1000.0f / chunk.font.size() / hScale);
                text.showText(array);
            }
            else if (chunk.isTab() && tabPosition != xMarker) {
                final PdfTextArray array = new PdfTextArray();
                array.add((tabPosition - xMarker) * 1000.0f / chunk.font.size() / hScale);
                text.showText(array);
            }
            else if (isJustified && numberOfSpaces > 0 && chunk.isSpecialEncoding()) {
                if (hScale != lastHScale) {
                    lastHScale = hScale;
                    text.setWordSpacing(baseWordSpacing / hScale);
                    text.setCharacterSpacing(baseCharacterSpacing / hScale + text.getCharacterSpacing());
                }
                final String s2 = chunk.toString();
                int idx = s2.indexOf(32);
                if (idx < 0) {
                    text.showText(s2);
                }
                else {
                    final float spaceCorrection = -baseWordSpacing * 1000.0f / chunk.font.size() / hScale;
                    final PdfTextArray textArray = new PdfTextArray(s2.substring(0, idx));
                    int lastIdx;
                    for (lastIdx = idx; (idx = s2.indexOf(32, lastIdx + 1)) >= 0; lastIdx = idx) {
                        textArray.add(spaceCorrection);
                        textArray.add(s2.substring(lastIdx, idx));
                    }
                    textArray.add(spaceCorrection);
                    textArray.add(s2.substring(lastIdx));
                    text.showText(textArray);
                }
            }
            else {
                if (isJustified && hScale != lastHScale) {
                    lastHScale = hScale;
                    text.setWordSpacing(baseWordSpacing / hScale);
                    text.setCharacterSpacing(baseCharacterSpacing / hScale + text.getCharacterSpacing());
                }
                text.showText(chunk.toString());
            }
            if (rise != 0.0f) {
                text.setTextRise(0.0f);
            }
            if (color != null) {
                text.resetRGBColorFill();
            }
            if (tr != 0) {
                text.setTextRenderingMode(0);
            }
            if (strokeColor != null) {
                text.resetRGBColorStroke();
            }
            if (strokeWidth != 1.0f) {
                text.setLineWidth(1.0f);
            }
            if (chunk.isAttribute("SKEW") || chunk.isAttribute("HSCALE")) {
                adjustMatrix = true;
                text.setTextMatrix(xMarker, yMarker);
            }
            if (!isJustified) {
                if (chunk.isAttribute("CHAR_SPACING")) {
                    text.setCharacterSpacing(baseCharacterSpacing);
                }
                if (chunk.isAttribute("WORD_SPACING")) {
                    text.setWordSpacing(baseWordSpacing);
                }
            }
            if (isTagged(this.writer) && chunk.accessibleElement != null) {
                text.closeMCBlock(chunk.accessibleElement);
            }
        }
        if (isJustified) {
            text.setWordSpacing(0.0f);
            text.setCharacterSpacing(0.0f);
            if (line.isNewlineSplit()) {
                lastBaseFactor = 0.0f;
            }
        }
        if (adjustMatrix) {
            text.moveText(baseXMarker - text.getXTLM(), 0.0f);
        }
        currentValues[0] = currentFont;
        currentValues[1] = new Float(lastBaseFactor);
        return lastX;
    }
    
    protected float indentLeft() {
        return this.left(this.indentation.indentLeft + this.indentation.listIndentLeft + this.indentation.imageIndentLeft + this.indentation.sectionIndentLeft);
    }
    
    protected float indentRight() {
        return this.right(this.indentation.indentRight + this.indentation.sectionIndentRight + this.indentation.imageIndentRight);
    }
    
    protected float indentTop() {
        return this.top(this.indentation.indentTop);
    }
    
    float indentBottom() {
        return this.bottom(this.indentation.indentBottom);
    }
    
    protected void addSpacing(final float extraspace, final float oldleading, final Font f) {
        this.addSpacing(extraspace, oldleading, f, false);
    }
    
    protected void addSpacing(final float extraspace, final float oldleading, Font f, final boolean spacingAfter) {
        if (extraspace == 0.0f) {
            return;
        }
        if (this.pageEmpty) {
            return;
        }
        final float height = spacingAfter ? extraspace : this.calculateLineHeight();
        if (this.currentHeight + height > this.indentTop() - this.indentBottom()) {
            this.newPage();
            return;
        }
        this.leading = extraspace;
        this.carriageReturn();
        if (f.isUnderlined() || f.isStrikethru()) {
            f = new Font(f);
            int style = f.getStyle();
            style &= 0xFFFFFFFB;
            style &= 0xFFFFFFF7;
            f.setStyle(style);
        }
        Chunk space = new Chunk(" ", f);
        if (spacingAfter && this.pageEmpty) {
            space = new Chunk("", f);
        }
        space.process(this);
        this.carriageReturn();
        this.leading = oldleading;
    }
    
    PdfInfo getInfo() {
        return this.info;
    }
    
    PdfCatalog getCatalog(final PdfIndirectReference pages) {
        final PdfCatalog catalog = new PdfCatalog(pages, this.writer);
        if (this.rootOutline.getKids().size() > 0) {
            catalog.put(PdfName.PAGEMODE, PdfName.USEOUTLINES);
            catalog.put(PdfName.OUTLINES, this.rootOutline.indirectReference());
        }
        this.writer.getPdfVersion().addToCatalog(catalog);
        this.viewerPreferences.addToCatalog(catalog);
        if (this.pageLabels != null) {
            catalog.put(PdfName.PAGELABELS, this.pageLabels.getDictionary(this.writer));
        }
        catalog.addNames(this.localDestinations, this.getDocumentLevelJS(), this.documentFileAttachment, this.writer);
        if (this.openActionName != null) {
            final PdfAction action = this.getLocalGotoAction(this.openActionName);
            catalog.setOpenAction(action);
        }
        else if (this.openActionAction != null) {
            catalog.setOpenAction(this.openActionAction);
        }
        if (this.additionalActions != null) {
            catalog.setAdditionalActions(this.additionalActions);
        }
        if (this.collection != null) {
            catalog.put(PdfName.COLLECTION, this.collection);
        }
        if (this.annotationsImp.hasValidAcroForm()) {
            try {
                catalog.put(PdfName.ACROFORM, this.writer.addToBody(this.annotationsImp.getAcroForm()).getIndirectReference());
            }
            catch (IOException e) {
                throw new ExceptionConverter(e);
            }
        }
        if (this.language != null) {
            catalog.put(PdfName.LANG, this.language);
        }
        return catalog;
    }
    
    void addOutline(final PdfOutline outline, final String name) {
        this.localDestination(name, outline.getPdfDestination());
    }
    
    public PdfOutline getRootOutline() {
        return this.rootOutline;
    }
    
    void calculateOutlineCount() {
        if (this.rootOutline.getKids().size() == 0) {
            return;
        }
        this.traverseOutlineCount(this.rootOutline);
    }
    
    void traverseOutlineCount(final PdfOutline outline) {
        final ArrayList<PdfOutline> kids = outline.getKids();
        final PdfOutline parent = outline.parent();
        if (kids.isEmpty()) {
            if (parent != null) {
                parent.setCount(parent.getCount() + 1);
            }
        }
        else {
            for (int k = 0; k < kids.size(); ++k) {
                this.traverseOutlineCount(kids.get(k));
            }
            if (parent != null) {
                if (outline.isOpen()) {
                    parent.setCount(outline.getCount() + parent.getCount() + 1);
                }
                else {
                    parent.setCount(parent.getCount() + 1);
                    outline.setCount(-outline.getCount());
                }
            }
        }
    }
    
    void writeOutlines() throws IOException {
        if (this.rootOutline.getKids().size() == 0) {
            return;
        }
        this.outlineTree(this.rootOutline);
        this.writer.addToBody(this.rootOutline, this.rootOutline.indirectReference());
    }
    
    void outlineTree(final PdfOutline outline) throws IOException {
        outline.setIndirectReference(this.writer.getPdfIndirectReference());
        if (outline.parent() != null) {
            outline.put(PdfName.PARENT, outline.parent().indirectReference());
        }
        final ArrayList<PdfOutline> kids = outline.getKids();
        final int size = kids.size();
        for (int k = 0; k < size; ++k) {
            this.outlineTree(kids.get(k));
        }
        for (int k = 0; k < size; ++k) {
            if (k > 0) {
                kids.get(k).put(PdfName.PREV, kids.get(k - 1).indirectReference());
            }
            if (k < size - 1) {
                kids.get(k).put(PdfName.NEXT, kids.get(k + 1).indirectReference());
            }
        }
        if (size > 0) {
            outline.put(PdfName.FIRST, kids.get(0).indirectReference());
            outline.put(PdfName.LAST, kids.get(size - 1).indirectReference());
        }
        for (int k = 0; k < size; ++k) {
            final PdfOutline kid = kids.get(k);
            this.writer.addToBody(kid, kid.indirectReference());
        }
    }
    
    void setViewerPreferences(final int preferences) {
        this.viewerPreferences.setViewerPreferences(preferences);
    }
    
    void addViewerPreference(final PdfName key, final PdfObject value) {
        this.viewerPreferences.addViewerPreference(key, value);
    }
    
    void setPageLabels(final PdfPageLabels pageLabels) {
        this.pageLabels = pageLabels;
    }
    
    public PdfPageLabels getPageLabels() {
        return this.pageLabels;
    }
    
    void localGoto(final String name, final float llx, final float lly, final float urx, final float ury) {
        final PdfAction action = this.getLocalGotoAction(name);
        this.annotationsImp.addPlainAnnotation(this.writer.createAnnotation(llx, lly, urx, ury, action, null));
    }
    
    void remoteGoto(final String filename, final String name, final float llx, final float lly, final float urx, final float ury) {
        this.annotationsImp.addPlainAnnotation(this.writer.createAnnotation(llx, lly, urx, ury, new PdfAction(filename, name), null));
    }
    
    void remoteGoto(final String filename, final int page, final float llx, final float lly, final float urx, final float ury) {
        this.addAnnotation(this.writer.createAnnotation(llx, lly, urx, ury, new PdfAction(filename, page), null));
    }
    
    void setAction(final PdfAction action, final float llx, final float lly, final float urx, final float ury) {
        this.addAnnotation(this.writer.createAnnotation(llx, lly, urx, ury, action, null));
    }
    
    PdfAction getLocalGotoAction(final String name) {
        Destination dest = this.localDestinations.get(name);
        if (dest == null) {
            dest = new Destination();
        }
        PdfAction action;
        if (dest.action == null) {
            if (dest.reference == null) {
                dest.reference = this.writer.getPdfIndirectReference();
            }
            action = new PdfAction(dest.reference);
            dest.action = action;
            this.localDestinations.put(name, dest);
        }
        else {
            action = dest.action;
        }
        return action;
    }
    
    boolean localDestination(final String name, final PdfDestination destination) {
        Destination dest = this.localDestinations.get(name);
        if (dest == null) {
            dest = new Destination();
        }
        if (dest.destination != null) {
            return false;
        }
        dest.destination = destination;
        this.localDestinations.put(name, dest);
        if (!destination.hasPage()) {
            destination.addPage(this.writer.getCurrentPage());
        }
        return true;
    }
    
    void addJavaScript(final PdfAction js) {
        if (js.get(PdfName.JS) == null) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("only.javascript.actions.are.allowed", new Object[0]));
        }
        try {
            this.documentLevelJS.put(PdfDocument.SIXTEEN_DIGITS.format(this.jsCounter++), this.writer.addToBody(js).getIndirectReference());
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }
    
    void addJavaScript(final String name, final PdfAction js) {
        if (js.get(PdfName.JS) == null) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("only.javascript.actions.are.allowed", new Object[0]));
        }
        try {
            this.documentLevelJS.put(name, this.writer.addToBody(js).getIndirectReference());
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }
    
    HashMap<String, PdfObject> getDocumentLevelJS() {
        return this.documentLevelJS;
    }
    
    void addFileAttachment(String description, final PdfFileSpecification fs) throws IOException {
        if (description == null) {
            final PdfString desc = (PdfString)fs.get(PdfName.DESC);
            if (desc == null) {
                description = "";
            }
            else {
                description = PdfEncodings.convertToString(desc.getBytes(), null);
            }
        }
        fs.addDescription(description, true);
        if (description.length() == 0) {
            description = "Unnamed";
        }
        String fn = PdfEncodings.convertToString(new PdfString(description, "UnicodeBig").getBytes(), null);
        for (int k = 0; this.documentFileAttachment.containsKey(fn); fn = PdfEncodings.convertToString(new PdfString(description + " " + k, "UnicodeBig").getBytes(), null)) {
            ++k;
        }
        this.documentFileAttachment.put(fn, fs.getReference());
    }
    
    HashMap<String, PdfObject> getDocumentFileAttachment() {
        return this.documentFileAttachment;
    }
    
    void setOpenAction(final String name) {
        this.openActionName = name;
        this.openActionAction = null;
    }
    
    void setOpenAction(final PdfAction action) {
        this.openActionAction = action;
        this.openActionName = null;
    }
    
    void addAdditionalAction(final PdfName actionType, final PdfAction action) {
        if (this.additionalActions == null) {
            this.additionalActions = new PdfDictionary();
        }
        if (action == null) {
            this.additionalActions.remove(actionType);
        }
        else {
            this.additionalActions.put(actionType, action);
        }
        if (this.additionalActions.size() == 0) {
            this.additionalActions = null;
        }
    }
    
    public void setCollection(final PdfCollection collection) {
        this.collection = collection;
    }
    
    PdfAcroForm getAcroForm() {
        return this.annotationsImp.getAcroForm();
    }
    
    void setSigFlags(final int f) {
        this.annotationsImp.setSigFlags(f);
    }
    
    void addCalculationOrder(final PdfFormField formField) {
        this.annotationsImp.addCalculationOrder(formField);
    }
    
    void addAnnotation(final PdfAnnotation annot) {
        this.pageEmpty = false;
        this.annotationsImp.addAnnotation(annot);
    }
    
    void setLanguage(final String language) {
        this.language = new PdfString(language);
    }
    
    void setCropBoxSize(final Rectangle crop) {
        this.setBoxSize("crop", crop);
    }
    
    void setBoxSize(final String boxName, final Rectangle size) {
        if (size == null) {
            this.boxSize.remove(boxName);
        }
        else {
            this.boxSize.put(boxName, new PdfRectangle(size));
        }
    }
    
    protected void setNewPageSizeAndMargins() {
        this.pageSize = this.nextPageSize;
        if (this.marginMirroring && (this.getPageNumber() & 0x1) == 0x0) {
            this.marginRight = this.nextMarginLeft;
            this.marginLeft = this.nextMarginRight;
        }
        else {
            this.marginLeft = this.nextMarginLeft;
            this.marginRight = this.nextMarginRight;
        }
        if (this.marginMirroringTopBottom && (this.getPageNumber() & 0x1) == 0x0) {
            this.marginTop = this.nextMarginBottom;
            this.marginBottom = this.nextMarginTop;
        }
        else {
            this.marginTop = this.nextMarginTop;
            this.marginBottom = this.nextMarginBottom;
        }
        if (!isTagged(this.writer)) {
            (this.text = new PdfContentByte(this.writer)).reset();
        }
        else {
            this.text = this.graphics;
        }
        this.text.beginText();
        this.text.moveText(this.left(), this.top());
        if (isTagged(this.writer)) {
            this.textEmptySize = this.text.size();
        }
    }
    
    Rectangle getBoxSize(final String boxName) {
        final PdfRectangle r = this.thisBoxSize.get(boxName);
        if (r != null) {
            return r.getRectangle();
        }
        return null;
    }
    
    void setPageEmpty(final boolean pageEmpty) {
        this.pageEmpty = pageEmpty;
    }
    
    boolean isPageEmpty() {
        if (isTagged(this.writer)) {
            return this.writer == null || (this.writer.getDirectContent().size(false) == 0 && this.writer.getDirectContentUnder().size(false) == 0 && this.text.size(false) - this.textEmptySize == 0 && (this.pageEmpty || this.writer.isPaused()));
        }
        return this.writer == null || (this.writer.getDirectContent().size() == 0 && this.writer.getDirectContentUnder().size() == 0 && (this.pageEmpty || this.writer.isPaused()));
    }
    
    void setDuration(final int seconds) {
        if (seconds > 0) {
            this.writer.addPageDictEntry(PdfName.DUR, new PdfNumber(seconds));
        }
    }
    
    void setTransition(final PdfTransition transition) {
        this.writer.addPageDictEntry(PdfName.TRANS, transition.getTransitionDictionary());
    }
    
    void setPageAction(final PdfName actionType, final PdfAction action) {
        if (this.pageAA == null) {
            this.pageAA = new PdfDictionary();
        }
        this.pageAA.put(actionType, action);
    }
    
    void setThumbnail(final Image image) throws PdfException, DocumentException {
        this.writer.addPageDictEntry(PdfName.THUMB, this.writer.getImageReference(this.writer.addDirectImageSimple(image)));
    }
    
    PageResources getPageResources() {
        return this.pageResources;
    }
    
    boolean isStrictImageSequence() {
        return this.strictImageSequence;
    }
    
    void setStrictImageSequence(final boolean strictImageSequence) {
        this.strictImageSequence = strictImageSequence;
    }
    
    public void clearTextWrap() {
        float tmpHeight = this.imageEnd - this.currentHeight;
        if (this.line != null) {
            tmpHeight += this.line.height();
        }
        if (this.imageEnd > -1.0f && tmpHeight > 0.0f) {
            this.carriageReturn();
            this.currentHeight += tmpHeight;
        }
    }
    
    public int getStructParentIndex(final Object obj) {
        int[] i = this.structParentIndices.get(obj);
        if (i == null) {
            i = new int[] { this.structParentIndices.size(), 0 };
            this.structParentIndices.put(obj, i);
        }
        return i[0];
    }
    
    public int getNextMarkPoint(final Object obj) {
        int[] i = this.structParentIndices.get(obj);
        if (i == null) {
            i = new int[] { this.structParentIndices.size(), 0 };
            this.structParentIndices.put(obj, i);
        }
        final int markPoint = i[1];
        final int[] array = i;
        final int n = 1;
        ++array[n];
        return markPoint;
    }
    
    public int[] getStructParentIndexAndNextMarkPoint(final Object obj) {
        int[] i = this.structParentIndices.get(obj);
        if (i == null) {
            i = new int[] { this.structParentIndices.size(), 0 };
            this.structParentIndices.put(obj, i);
        }
        final int markPoint = i[1];
        final int[] array = i;
        final int n = 1;
        ++array[n];
        return new int[] { i[0], markPoint };
    }
    
    protected void add(final Image image) throws PdfException, DocumentException {
        if (image.hasAbsoluteY()) {
            this.graphics.addImage(image);
            this.pageEmpty = false;
            return;
        }
        if (this.currentHeight != 0.0f && this.indentTop() - this.currentHeight - image.getScaledHeight() < this.indentBottom()) {
            if (!this.strictImageSequence && this.imageWait == null) {
                this.imageWait = image;
                return;
            }
            this.newPage();
            if (this.currentHeight != 0.0f && this.indentTop() - this.currentHeight - image.getScaledHeight() < this.indentBottom()) {
                this.imageWait = image;
                return;
            }
        }
        this.pageEmpty = false;
        if (image == this.imageWait) {
            this.imageWait = null;
        }
        final boolean textwrap = (image.getAlignment() & 0x4) == 0x4 && (image.getAlignment() & 0x1) != 0x1;
        final boolean underlying = (image.getAlignment() & 0x8) == 0x8;
        float diff = this.leading / 2.0f;
        if (textwrap) {
            diff += this.leading;
        }
        final float lowerleft = this.indentTop() - this.currentHeight - image.getScaledHeight() - diff;
        final float[] mt = image.matrix();
        float startPosition = this.indentLeft() - mt[4];
        if ((image.getAlignment() & 0x2) == 0x2) {
            startPosition = this.indentRight() - image.getScaledWidth() - mt[4];
        }
        if ((image.getAlignment() & 0x1) == 0x1) {
            startPosition = this.indentLeft() + (this.indentRight() - this.indentLeft() - image.getScaledWidth()) / 2.0f - mt[4];
        }
        if (image.hasAbsoluteX()) {
            startPosition = image.getAbsoluteX();
        }
        if (textwrap) {
            if (this.imageEnd < 0.0f || this.imageEnd < this.currentHeight + image.getScaledHeight() + diff) {
                this.imageEnd = this.currentHeight + image.getScaledHeight() + diff;
            }
            if ((image.getAlignment() & 0x2) == 0x2) {
                final Indentation indentation = this.indentation;
                indentation.imageIndentRight += image.getScaledWidth() + image.getIndentationLeft();
            }
            else {
                final Indentation indentation2 = this.indentation;
                indentation2.imageIndentLeft += image.getScaledWidth() + image.getIndentationRight();
            }
        }
        else if ((image.getAlignment() & 0x2) == 0x2) {
            startPosition -= image.getIndentationRight();
        }
        else if ((image.getAlignment() & 0x1) == 0x1) {
            startPosition += image.getIndentationLeft() - image.getIndentationRight();
        }
        else {
            startPosition += image.getIndentationLeft();
        }
        this.graphics.addImage(image, mt[0], mt[1], mt[2], mt[3], startPosition, lowerleft - mt[5]);
        if (!textwrap && !underlying) {
            this.currentHeight += image.getScaledHeight() + diff;
            this.flushLines();
            this.text.moveText(0.0f, -(image.getScaledHeight() + diff));
            this.newLine();
        }
    }
    
    void addPTable(final PdfPTable ptable) throws DocumentException {
        final ColumnText ct = new ColumnText(isTagged(this.writer) ? this.text : this.writer.getDirectContent());
        ct.setRunDirection(ptable.getRunDirection());
        if (ptable.getKeepTogether() && !this.fitsPage(ptable, 0.0f) && this.currentHeight > 0.0f) {
            this.newPage();
            if (isTagged(this.writer)) {
                ct.setCanvas(this.text);
            }
        }
        if (this.currentHeight == 0.0f) {
            ct.setAdjustFirstLine(false);
        }
        ct.addElement(ptable);
        final boolean he = ptable.isHeadersInEvent();
        ptable.setHeadersInEvent(true);
        int loop = 0;
        while (true) {
            ct.setSimpleColumn(this.indentLeft(), this.indentBottom(), this.indentRight(), this.indentTop() - this.currentHeight);
            final int status = ct.go();
            if ((status & 0x1) != 0x0) {
                if (isTagged(this.writer)) {
                    this.text.setTextMatrix(this.indentLeft(), ct.getYLine());
                }
                else {
                    this.text.moveText(0.0f, ct.getYLine() - this.indentTop() + this.currentHeight);
                }
                this.currentHeight = this.indentTop() - ct.getYLine();
                ptable.setHeadersInEvent(he);
                return;
            }
            if (this.indentTop() - this.currentHeight == ct.getYLine()) {
                ++loop;
            }
            else {
                loop = 0;
            }
            if (loop == 3) {
                throw new DocumentException(MessageLocalization.getComposedMessage("infinite.table.loop", new Object[0]));
            }
            this.currentHeight = this.indentTop() - ct.getYLine();
            this.newPage();
            ptable.setSkipFirstHeader(false);
            if (!isTagged(this.writer)) {
                continue;
            }
            ct.setCanvas(this.text);
        }
    }
    
    private void addDiv(final PdfDiv div) throws DocumentException {
        if (this.floatingElements == null) {
            this.floatingElements = new ArrayList<Element>();
        }
        this.floatingElements.add(div);
    }
    
    private void flushFloatingElements() throws DocumentException {
        if (this.floatingElements != null && !this.floatingElements.isEmpty()) {
            final ArrayList<Element> cachedFloatingElements = this.floatingElements;
            this.floatingElements = null;
            final FloatLayout fl = new FloatLayout(cachedFloatingElements, false);
            int loop = 0;
            while (true) {
                final float left = this.indentLeft();
                fl.setSimpleColumn(this.indentLeft(), this.indentBottom(), this.indentRight(), this.indentTop() - this.currentHeight);
                try {
                    final int status = fl.layout(isTagged(this.writer) ? this.text : this.writer.getDirectContent(), false);
                    if ((status & 0x1) != 0x0) {
                        if (isTagged(this.writer)) {
                            this.text.setTextMatrix(this.indentLeft(), fl.getYLine());
                        }
                        else {
                            this.text.moveText(0.0f, fl.getYLine() - this.indentTop() + this.currentHeight);
                        }
                        this.currentHeight = this.indentTop() - fl.getYLine();
                        break;
                    }
                }
                catch (Exception exc) {
                    return;
                }
                if (this.indentTop() - this.currentHeight == fl.getYLine() || this.isPageEmpty()) {
                    ++loop;
                }
                else {
                    loop = 0;
                }
                if (loop == 2) {
                    return;
                }
                this.newPage();
            }
        }
    }
    
    boolean fitsPage(final PdfPTable table, final float margin) {
        if (!table.isLockedWidth()) {
            final float totalWidth = (this.indentRight() - this.indentLeft()) * table.getWidthPercentage() / 100.0f;
            table.setTotalWidth(totalWidth);
        }
        this.ensureNewLine();
        final Float spaceNeeded = table.isSkipFirstHeader() ? (table.getTotalHeight() - table.getHeaderHeight()) : table.getTotalHeight();
        return spaceNeeded + ((this.currentHeight > 0.0f) ? table.spacingBefore() : 0.0f) <= this.indentTop() - this.currentHeight - this.indentBottom() - margin;
    }
    
    private static boolean isTagged(final PdfWriter writer) {
        return writer != null && writer.isTagged();
    }
    
    private PdfLine getLastLine() {
        if (this.lines.size() > 0) {
            return this.lines.get(this.lines.size() - 1);
        }
        return null;
    }
    
    protected void useExternalCache(final TempFileCache externalCache) {
        this.isToUseExternalCache = true;
        this.externalCache = externalCache;
    }
    
    protected void saveStructElement(final AccessibleElementId id, final PdfStructureElement element) {
        this.structElements.put(id, element);
    }
    
    protected PdfStructureElement getStructElement(final AccessibleElementId id) {
        return this.getStructElement(id, true);
    }
    
    protected PdfStructureElement getStructElement(final AccessibleElementId id, final boolean toSaveFetchedElement) {
        PdfStructureElement element = this.structElements.get(id);
        if (this.isToUseExternalCache && element == null) {
            final TempFileCache.ObjectPosition pos = this.externallyStoredStructElements.get(id);
            if (pos != null) {
                try {
                    element = (PdfStructureElement)this.externalCache.get(pos);
                    element.setStructureTreeRoot(this.writer.getStructureTreeRoot());
                    element.setStructureElementParent(this.getStructElement(this.elementsParents.get(element.getElementId()), toSaveFetchedElement));
                    if (toSaveFetchedElement) {
                        this.externallyStoredStructElements.remove(id);
                        this.structElements.put(id, element);
                    }
                }
                catch (IOException e) {
                    throw new ExceptionConverter(e);
                }
                catch (ClassNotFoundException e2) {
                    throw new ExceptionConverter(e2);
                }
            }
        }
        return element;
    }
    
    protected void flushStructureElementsOnNewPage() {
        if (!this.isToUseExternalCache) {
            return;
        }
        final Iterator<Map.Entry<AccessibleElementId, PdfStructureElement>> iterator = this.structElements.entrySet().iterator();
        while (iterator.hasNext()) {
            final Map.Entry<AccessibleElementId, PdfStructureElement> entry = iterator.next();
            if (entry.getValue().getStructureType().equals(PdfName.DOCUMENT)) {
                continue;
            }
            try {
                final PdfStructureElement el = entry.getValue();
                final PdfDictionary parentDict = el.getParent();
                PdfStructureElement parent = null;
                if (parentDict instanceof PdfStructureElement) {
                    parent = (PdfStructureElement)parentDict;
                }
                if (parent != null) {
                    this.elementsParents.put(entry.getKey(), parent.getElementId());
                }
                final TempFileCache.ObjectPosition pos = this.externalCache.put(el);
                this.externallyStoredStructElements.put(entry.getKey(), pos);
                iterator.remove();
                continue;
            }
            catch (IOException e) {
                throw new ExceptionConverter(e);
            }
            break;
        }
    }
    
    public Set<AccessibleElementId> getStructElements() {
        final Set<AccessibleElementId> elements = new HashSet<AccessibleElementId>();
        elements.addAll(this.externallyStoredStructElements.keySet());
        elements.addAll(this.structElements.keySet());
        return elements;
    }
    
    static {
        SIXTEEN_DIGITS = new DecimalFormat("0000000000000000");
    }
    
    public static class PdfInfo extends PdfDictionary
    {
        PdfInfo() {
            this.addProducer();
            this.addCreationDate();
        }
        
        PdfInfo(final String author, final String title, final String subject) {
            this();
            this.addTitle(title);
            this.addSubject(subject);
            this.addAuthor(author);
        }
        
        void addTitle(final String title) {
            this.put(PdfName.TITLE, new PdfString(title, "UnicodeBig"));
        }
        
        void addSubject(final String subject) {
            this.put(PdfName.SUBJECT, new PdfString(subject, "UnicodeBig"));
        }
        
        void addKeywords(final String keywords) {
            this.put(PdfName.KEYWORDS, new PdfString(keywords, "UnicodeBig"));
        }
        
        void addAuthor(final String author) {
            this.put(PdfName.AUTHOR, new PdfString(author, "UnicodeBig"));
        }
        
        void addCreator(final String creator) {
            this.put(PdfName.CREATOR, new PdfString(creator, "UnicodeBig"));
        }
        
        void addProducer() {
            this.put(PdfName.PRODUCER, new PdfString(Version.getInstance().getVersion()));
        }
        
        void addCreationDate() {
            final PdfString date = new PdfDate();
            this.put(PdfName.CREATIONDATE, date);
            this.put(PdfName.MODDATE, date);
        }
        
        void addkey(final String key, final String value) {
            if (key.equals("Producer") || key.equals("CreationDate")) {
                return;
            }
            this.put(new PdfName(key), new PdfString(value, "UnicodeBig"));
        }
    }
    
    static class PdfCatalog extends PdfDictionary
    {
        PdfWriter writer;
        
        PdfCatalog(final PdfIndirectReference pages, final PdfWriter writer) {
            super(PdfCatalog.CATALOG);
            this.writer = writer;
            this.put(PdfName.PAGES, pages);
        }
        
        void addNames(final TreeMap<String, Destination> localDestinations, final HashMap<String, PdfObject> documentLevelJS, final HashMap<String, PdfObject> documentFileAttachment, final PdfWriter writer) {
            if (localDestinations.isEmpty() && documentLevelJS.isEmpty() && documentFileAttachment.isEmpty()) {
                return;
            }
            try {
                final PdfDictionary names = new PdfDictionary();
                if (!localDestinations.isEmpty()) {
                    final HashMap<String, PdfObject> destmap = new HashMap<String, PdfObject>();
                    for (final Map.Entry<String, Destination> entry : localDestinations.entrySet()) {
                        final String name = entry.getKey();
                        final Destination dest = entry.getValue();
                        if (dest.destination == null) {
                            continue;
                        }
                        destmap.put(name, dest.reference);
                    }
                    if (destmap.size() > 0) {
                        names.put(PdfName.DESTS, writer.addToBody(PdfNameTree.writeTree(destmap, writer)).getIndirectReference());
                    }
                }
                if (!documentLevelJS.isEmpty()) {
                    final PdfDictionary tree = PdfNameTree.writeTree(documentLevelJS, writer);
                    names.put(PdfName.JAVASCRIPT, writer.addToBody(tree).getIndirectReference());
                }
                if (!documentFileAttachment.isEmpty()) {
                    names.put(PdfName.EMBEDDEDFILES, writer.addToBody(PdfNameTree.writeTree(documentFileAttachment, writer)).getIndirectReference());
                }
                if (names.size() > 0) {
                    this.put(PdfName.NAMES, writer.addToBody(names).getIndirectReference());
                }
            }
            catch (IOException e) {
                throw new ExceptionConverter(e);
            }
        }
        
        void setOpenAction(final PdfAction action) {
            this.put(PdfName.OPENACTION, action);
        }
        
        void setAdditionalActions(final PdfDictionary actions) {
            try {
                this.put(PdfName.AA, this.writer.addToBody(actions).getIndirectReference());
            }
            catch (Exception e) {
                throw new ExceptionConverter(e);
            }
        }
    }
    
    public static class Indentation
    {
        float indentLeft;
        float sectionIndentLeft;
        float listIndentLeft;
        float imageIndentLeft;
        float indentRight;
        float sectionIndentRight;
        float imageIndentRight;
        float indentTop;
        float indentBottom;
        
        public Indentation() {
            this.indentLeft = 0.0f;
            this.sectionIndentLeft = 0.0f;
            this.listIndentLeft = 0.0f;
            this.imageIndentLeft = 0.0f;
            this.indentRight = 0.0f;
            this.sectionIndentRight = 0.0f;
            this.imageIndentRight = 0.0f;
            this.indentTop = 0.0f;
            this.indentBottom = 0.0f;
        }
    }
    
    public class Destination
    {
        public PdfAction action;
        public PdfIndirectReference reference;
        public PdfDestination destination;
    }
}
