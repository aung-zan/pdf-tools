// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.io.ByteArrayOutputStream;
import com.itextpdf.text.pdf.codec.TIFFFaxDecoder;
import com.itextpdf.text.pdf.codec.TIFFFaxDecompressor;
import com.itextpdf.text.exceptions.UnsupportedPdfException;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class FilterHandlers
{
    private static final Map<PdfName, FilterHandler> defaults;
    
    public static Map<PdfName, FilterHandler> getDefaultFilterHandlers() {
        return FilterHandlers.defaults;
    }
    
    static {
        final HashMap<PdfName, FilterHandler> map = new HashMap<PdfName, FilterHandler>();
        map.put(PdfName.FLATEDECODE, new Filter_FLATEDECODE());
        map.put(PdfName.FL, new Filter_FLATEDECODE());
        map.put(PdfName.ASCIIHEXDECODE, new Filter_ASCIIHEXDECODE());
        map.put(PdfName.AHX, new Filter_ASCIIHEXDECODE());
        map.put(PdfName.ASCII85DECODE, new Filter_ASCII85DECODE());
        map.put(PdfName.A85, new Filter_ASCII85DECODE());
        map.put(PdfName.LZWDECODE, new Filter_LZWDECODE());
        map.put(PdfName.CCITTFAXDECODE, new Filter_CCITTFAXDECODE());
        map.put(PdfName.CRYPT, new Filter_DoNothing());
        map.put(PdfName.RUNLENGTHDECODE, new Filter_RUNLENGTHDECODE());
        defaults = Collections.unmodifiableMap((Map<? extends PdfName, ? extends FilterHandler>)map);
    }
    
    private static class Filter_FLATEDECODE implements FilterHandler
    {
        @Override
        public byte[] decode(byte[] b, final PdfName filterName, final PdfObject decodeParams, final PdfDictionary streamDictionary) throws IOException {
            b = PdfReader.FlateDecode(b);
            b = PdfReader.decodePredictor(b, decodeParams);
            return b;
        }
    }
    
    private static class Filter_ASCIIHEXDECODE implements FilterHandler
    {
        @Override
        public byte[] decode(byte[] b, final PdfName filterName, final PdfObject decodeParams, final PdfDictionary streamDictionary) throws IOException {
            b = PdfReader.ASCIIHexDecode(b);
            return b;
        }
    }
    
    private static class Filter_ASCII85DECODE implements FilterHandler
    {
        @Override
        public byte[] decode(byte[] b, final PdfName filterName, final PdfObject decodeParams, final PdfDictionary streamDictionary) throws IOException {
            b = PdfReader.ASCII85Decode(b);
            return b;
        }
    }
    
    private static class Filter_LZWDECODE implements FilterHandler
    {
        @Override
        public byte[] decode(byte[] b, final PdfName filterName, final PdfObject decodeParams, final PdfDictionary streamDictionary) throws IOException {
            b = PdfReader.LZWDecode(b);
            b = PdfReader.decodePredictor(b, decodeParams);
            return b;
        }
    }
    
    private static class Filter_CCITTFAXDECODE implements FilterHandler
    {
        @Override
        public byte[] decode(byte[] b, final PdfName filterName, final PdfObject decodeParams, final PdfDictionary streamDictionary) throws IOException {
            final PdfNumber wn = (PdfNumber)PdfReader.getPdfObjectRelease(streamDictionary.get(PdfName.WIDTH));
            final PdfNumber hn = (PdfNumber)PdfReader.getPdfObjectRelease(streamDictionary.get(PdfName.HEIGHT));
            if (wn == null || hn == null) {
                throw new UnsupportedPdfException(MessageLocalization.getComposedMessage("filter.ccittfaxdecode.is.only.supported.for.images", new Object[0]));
            }
            final int width = wn.intValue();
            final int height = hn.intValue();
            final PdfDictionary param = (decodeParams instanceof PdfDictionary) ? ((PdfDictionary)decodeParams) : null;
            int k = 0;
            boolean blackIs1 = false;
            boolean byteAlign = false;
            if (param != null) {
                final PdfNumber kn = param.getAsNumber(PdfName.K);
                if (kn != null) {
                    k = kn.intValue();
                }
                PdfBoolean bo = param.getAsBoolean(PdfName.BLACKIS1);
                if (bo != null) {
                    blackIs1 = bo.booleanValue();
                }
                bo = param.getAsBoolean(PdfName.ENCODEDBYTEALIGN);
                if (bo != null) {
                    byteAlign = bo.booleanValue();
                }
            }
            byte[] outBuf = new byte[(width + 7) / 8 * height];
            final TIFFFaxDecompressor decoder = new TIFFFaxDecompressor();
            if (k == 0 || k > 0) {
                int tiffT4Options = (k > 0) ? 1 : 0;
                tiffT4Options |= (byteAlign ? 4 : 0);
                decoder.SetOptions(1, 3, tiffT4Options, 0);
                decoder.decodeRaw(outBuf, b, width, height);
                if (decoder.fails > 0) {
                    final byte[] outBuf2 = new byte[(width + 7) / 8 * height];
                    final int oldFails = decoder.fails;
                    decoder.SetOptions(1, 2, tiffT4Options, 0);
                    decoder.decodeRaw(outBuf2, b, width, height);
                    if (decoder.fails < oldFails) {
                        outBuf = outBuf2;
                    }
                }
            }
            else {
                final TIFFFaxDecoder deca = new TIFFFaxDecoder(1L, width, height);
                deca.decodeT6(outBuf, b, 0, height, 0L);
            }
            if (!blackIs1) {
                for (int len = outBuf.length, t = 0; t < len; ++t) {
                    final byte[] array = outBuf;
                    final int n = t;
                    array[n] ^= (byte)255;
                }
            }
            b = outBuf;
            return b;
        }
    }
    
    private static class Filter_DoNothing implements FilterHandler
    {
        @Override
        public byte[] decode(final byte[] b, final PdfName filterName, final PdfObject decodeParams, final PdfDictionary streamDictionary) throws IOException {
            return b;
        }
    }
    
    private static class Filter_RUNLENGTHDECODE implements FilterHandler
    {
        @Override
        public byte[] decode(final byte[] b, final PdfName filterName, final PdfObject decodeParams, final PdfDictionary streamDictionary) throws IOException {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte dupCount = -1;
            for (int i = 0; i < b.length; ++i) {
                dupCount = b[i];
                if (dupCount == -128) {
                    break;
                }
                if (dupCount >= 0 && dupCount <= 127) {
                    final int bytesToCopy = dupCount + 1;
                    baos.write(b, i, bytesToCopy);
                    i += bytesToCopy;
                }
                else {
                    ++i;
                    for (int j = 0; j < 1 - dupCount; ++j) {
                        baos.write(b[i]);
                    }
                }
            }
            return baos.toByteArray();
        }
    }
    
    public interface FilterHandler
    {
        byte[] decode(final byte[] p0, final PdfName p1, final PdfObject p2, final PdfDictionary p3) throws IOException;
    }
}
