// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.Chunk;
import java.util.Iterator;
import com.itextpdf.text.Image;
import com.itextpdf.text.TabStop;
import com.itextpdf.text.ListItem;
import java.util.ArrayList;

public class PdfLine
{
    protected ArrayList<PdfChunk> line;
    protected float left;
    protected float width;
    protected int alignment;
    protected float height;
    protected boolean newlineSplit;
    protected float originalWidth;
    protected boolean isRTL;
    protected ListItem listItem;
    protected TabStop tabStop;
    protected float tabStopAnchorPosition;
    protected float tabPosition;
    
    PdfLine(final float left, final float right, final int alignment, final float height) {
        this.newlineSplit = false;
        this.isRTL = false;
        this.listItem = null;
        this.tabStop = null;
        this.tabStopAnchorPosition = Float.NaN;
        this.tabPosition = Float.NaN;
        this.left = left;
        this.width = right - left;
        this.originalWidth = this.width;
        this.alignment = alignment;
        this.height = height;
        this.line = new ArrayList<PdfChunk>();
    }
    
    PdfLine(final float left, final float originalWidth, final float remainingWidth, final int alignment, final boolean newlineSplit, final ArrayList<PdfChunk> line, final boolean isRTL) {
        this.newlineSplit = false;
        this.isRTL = false;
        this.listItem = null;
        this.tabStop = null;
        this.tabStopAnchorPosition = Float.NaN;
        this.tabPosition = Float.NaN;
        this.left = left;
        this.originalWidth = originalWidth;
        this.width = remainingWidth;
        this.alignment = alignment;
        this.line = line;
        this.newlineSplit = newlineSplit;
        this.isRTL = isRTL;
    }
    
    PdfChunk add(final PdfChunk chunk, final float currentLeading) {
        if (chunk != null && !chunk.toString().equals("") && !chunk.toString().equals(" ") && (this.height < currentLeading || this.line.isEmpty())) {
            this.height = currentLeading;
        }
        return this.add(chunk);
    }
    
    PdfChunk add(PdfChunk chunk) {
        if (chunk == null || chunk.toString().equals("")) {
            return null;
        }
        PdfChunk overflow = chunk.split(this.width);
        this.newlineSplit = (chunk.isNewlineSplit() || overflow == null);
        if (chunk.isTab()) {
            final Object[] tab = (Object[])chunk.getAttribute("TAB");
            if (chunk.isAttribute("TABSETTINGS")) {
                final boolean isWhiteSpace = (boolean)tab[1];
                if (isWhiteSpace && this.line.isEmpty()) {
                    return null;
                }
                this.flush();
                this.tabStopAnchorPosition = Float.NaN;
                this.tabStop = PdfChunk.getTabStop(chunk, this.originalWidth - this.width);
                if (this.tabStop.getPosition() > this.originalWidth) {
                    if (isWhiteSpace) {
                        overflow = null;
                    }
                    else if (Math.abs(this.originalWidth - this.width) < 0.001) {
                        this.addToLine(chunk);
                        overflow = null;
                    }
                    else {
                        overflow = chunk;
                    }
                    this.width = 0.0f;
                }
                else {
                    chunk.setTabStop(this.tabStop);
                    if (!this.isRTL && this.tabStop.getAlignment() == TabStop.Alignment.LEFT) {
                        this.width = this.originalWidth - this.tabStop.getPosition();
                        this.tabStop = null;
                        this.tabPosition = Float.NaN;
                    }
                    else {
                        this.tabPosition = this.originalWidth - this.width;
                    }
                    this.addToLine(chunk);
                }
            }
            else {
                final Float tabStopPosition = (Float)tab[1];
                final boolean newline = (boolean)tab[2];
                if (newline && tabStopPosition < this.originalWidth - this.width) {
                    return chunk;
                }
                chunk.adjustLeft(this.left);
                this.width = this.originalWidth - tabStopPosition;
                this.addToLine(chunk);
            }
        }
        else if (chunk.length() > 0 || chunk.isImage()) {
            if (overflow != null) {
                chunk.trimLastSpace();
            }
            this.width -= chunk.width();
            this.addToLine(chunk);
        }
        else if (this.line.size() < 1) {
            chunk = overflow;
            overflow = chunk.truncate(this.width);
            this.width -= chunk.width();
            if (chunk.length() > 0) {
                this.addToLine(chunk);
                return overflow;
            }
            if (overflow != null) {
                this.addToLine(overflow);
            }
            return null;
        }
        else {
            this.width += this.line.get(this.line.size() - 1).trimLastSpace();
        }
        return overflow;
    }
    
    private void addToLine(final PdfChunk chunk) {
        if (chunk.changeLeading) {
            float f;
            if (chunk.isImage()) {
                final Image img = chunk.getImage();
                f = chunk.getImageHeight() + chunk.getImageOffsetY() + img.getBorderWidthTop() + img.getSpacingBefore();
            }
            else {
                f = chunk.getLeading();
            }
            if (f > this.height) {
                this.height = f;
            }
        }
        if (this.tabStop != null && this.tabStop.getAlignment() == TabStop.Alignment.ANCHOR && Float.isNaN(this.tabStopAnchorPosition)) {
            final String value = chunk.toString();
            final int anchorIndex = value.indexOf(this.tabStop.getAnchorChar());
            if (anchorIndex != -1) {
                final float subWidth = chunk.width(value.substring(anchorIndex, value.length()));
                this.tabStopAnchorPosition = this.originalWidth - this.width - subWidth;
            }
        }
        this.line.add(chunk);
    }
    
    public int size() {
        return this.line.size();
    }
    
    public Iterator<PdfChunk> iterator() {
        return this.line.iterator();
    }
    
    float height() {
        return this.height;
    }
    
    float indentLeft() {
        if (!this.isRTL) {
            if (this.getSeparatorCount() <= 0) {
                switch (this.alignment) {
                    case 2: {
                        return this.left + this.width;
                    }
                    case 1: {
                        return this.left + this.width / 2.0f;
                    }
                }
            }
            return this.left;
        }
        switch (this.alignment) {
            case 1: {
                return this.left + this.width / 2.0f;
            }
            case 2: {
                return this.left;
            }
            case 3: {
                return this.left + (this.hasToBeJustified() ? 0.0f : this.width);
            }
            default: {
                return this.left + this.width;
            }
        }
    }
    
    public boolean hasToBeJustified() {
        return ((this.alignment == 3 && !this.newlineSplit) || this.alignment == 8) && this.width != 0.0f;
    }
    
    public void resetAlignment() {
        if (this.alignment == 3) {
            this.alignment = 0;
        }
    }
    
    void setExtraIndent(final float extra) {
        this.left += extra;
        this.width -= extra;
        this.originalWidth -= extra;
    }
    
    float widthLeft() {
        return this.width;
    }
    
    int numberOfSpaces() {
        int numberOfSpaces = 0;
        for (final PdfChunk pdfChunk : this.line) {
            final String tmp = pdfChunk.toString();
            for (int length = tmp.length(), i = 0; i < length; ++i) {
                if (tmp.charAt(i) == ' ') {
                    ++numberOfSpaces;
                }
            }
        }
        return numberOfSpaces;
    }
    
    public void setListItem(final ListItem listItem) {
        this.listItem = listItem;
    }
    
    public Chunk listSymbol() {
        return (this.listItem != null) ? this.listItem.getListSymbol() : null;
    }
    
    public float listIndent() {
        return (this.listItem != null) ? this.listItem.getIndentationLeft() : 0.0f;
    }
    
    public ListItem listItem() {
        return this.listItem;
    }
    
    @Override
    public String toString() {
        final StringBuffer tmp = new StringBuffer();
        for (final PdfChunk pdfChunk : this.line) {
            tmp.append(pdfChunk.toString());
        }
        return tmp.toString();
    }
    
    public int getLineLengthUtf32() {
        int total = 0;
        for (final Object element : this.line) {
            total += ((PdfChunk)element).lengthUtf32();
        }
        return total;
    }
    
    public boolean isNewlineSplit() {
        return this.newlineSplit && this.alignment != 8;
    }
    
    public int getLastStrokeChunk() {
        int lastIdx;
        for (lastIdx = this.line.size() - 1; lastIdx >= 0; --lastIdx) {
            final PdfChunk chunk = this.line.get(lastIdx);
            if (chunk.isStroked()) {
                break;
            }
        }
        return lastIdx;
    }
    
    public PdfChunk getChunk(final int idx) {
        if (idx < 0 || idx >= this.line.size()) {
            return null;
        }
        return this.line.get(idx);
    }
    
    public float getOriginalWidth() {
        return this.originalWidth;
    }
    
    float[] getMaxSize(final float fixedLeading, final float multipliedLeading) {
        float normal_leading = 0.0f;
        float image_leading = -10000.0f;
        for (int k = 0; k < this.line.size(); ++k) {
            final PdfChunk chunk = this.line.get(k);
            if (chunk.isImage()) {
                final Image img = chunk.getImage();
                if (chunk.changeLeading()) {
                    final float height = chunk.getImageHeight() + chunk.getImageOffsetY() + img.getSpacingBefore();
                    image_leading = Math.max(height, image_leading);
                }
            }
            else if (chunk.changeLeading()) {
                normal_leading = Math.max(chunk.getLeading(), normal_leading);
            }
            else {
                normal_leading = Math.max(fixedLeading + multipliedLeading * chunk.font().size(), normal_leading);
            }
        }
        return new float[] { (normal_leading > 0.0f) ? normal_leading : fixedLeading, image_leading };
    }
    
    boolean isRTL() {
        return this.isRTL;
    }
    
    int getSeparatorCount() {
        int s = 0;
        for (final Object element : this.line) {
            final PdfChunk ck = (PdfChunk)element;
            if (ck.isTab()) {
                if (ck.isAttribute("TABSETTINGS")) {
                    continue;
                }
                return -1;
            }
            else {
                if (!ck.isHorizontalSeparator()) {
                    continue;
                }
                ++s;
            }
        }
        return s;
    }
    
    public float getWidthCorrected(final float charSpacing, final float wordSpacing) {
        float total = 0.0f;
        for (int k = 0; k < this.line.size(); ++k) {
            final PdfChunk ck = this.line.get(k);
            total += ck.getWidthCorrected(charSpacing, wordSpacing);
        }
        return total;
    }
    
    public float getAscender() {
        float ascender = 0.0f;
        for (int k = 0; k < this.line.size(); ++k) {
            final PdfChunk ck = this.line.get(k);
            if (ck.isImage()) {
                ascender = Math.max(ascender, ck.getImageHeight() + ck.getImageOffsetY());
            }
            else {
                final PdfFont font = ck.font();
                final float textRise = ck.getTextRise();
                ascender = Math.max(ascender, ((textRise > 0.0f) ? textRise : 0.0f) + font.getFont().getFontDescriptor(1, font.size()));
            }
        }
        return ascender;
    }
    
    public float getDescender() {
        float descender = 0.0f;
        for (int k = 0; k < this.line.size(); ++k) {
            final PdfChunk ck = this.line.get(k);
            if (ck.isImage()) {
                descender = Math.min(descender, ck.getImageOffsetY());
            }
            else {
                final PdfFont font = ck.font();
                final float textRise = ck.getTextRise();
                descender = Math.min(descender, ((textRise < 0.0f) ? textRise : 0.0f) + font.getFont().getFontDescriptor(3, font.size()));
            }
        }
        return descender;
    }
    
    public void flush() {
        if (this.tabStop != null) {
            final float textWidth = this.originalWidth - this.width - this.tabPosition;
            float tabStopPosition = this.tabStop.getPosition(this.tabPosition, this.originalWidth - this.width, this.tabStopAnchorPosition);
            this.width = this.originalWidth - tabStopPosition - textWidth;
            if (this.width < 0.0f) {
                tabStopPosition += this.width;
            }
            if (!this.isRTL) {
                this.tabStop.setPosition(tabStopPosition);
            }
            else {
                this.tabStop.setPosition(this.originalWidth - this.width - this.tabPosition);
            }
            this.tabStop = null;
            this.tabPosition = Float.NaN;
        }
    }
}
