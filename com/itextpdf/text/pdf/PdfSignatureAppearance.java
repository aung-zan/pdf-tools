// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.io.EOFException;
import java.util.Arrays;
import java.util.Map;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import java.text.SimpleDateFormat;
import com.itextpdf.text.pdf.security.CertificateInfo;
import java.security.cert.X509Certificate;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.util.Iterator;
import com.itextpdf.text.io.RandomAccessSource;
import java.io.IOException;
import com.itextpdf.text.io.RASInputStream;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import java.io.InputStream;
import com.itextpdf.text.Version;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.io.File;
import java.io.OutputStream;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import java.security.cert.Certificate;
import java.io.RandomAccessFile;
import java.util.Calendar;

public class PdfSignatureAppearance
{
    public static final int NOT_CERTIFIED = 0;
    public static final int CERTIFIED_NO_CHANGES_ALLOWED = 1;
    public static final int CERTIFIED_FORM_FILLING = 2;
    public static final int CERTIFIED_FORM_FILLING_AND_ANNOTATIONS = 3;
    private int certificationLevel;
    private String reasonCaption;
    private String locationCaption;
    private String reason;
    private String location;
    private Calendar signDate;
    private String signatureCreator;
    private String contact;
    private RandomAccessFile raf;
    private byte[] bout;
    private long[] range;
    private Certificate signCertificate;
    private PdfDictionary cryptoDictionary;
    private SignatureEvent signatureEvent;
    private String fieldName;
    private int page;
    private Rectangle rect;
    private Rectangle pageRect;
    private RenderingMode renderingMode;
    private Image signatureGraphic;
    private boolean acro6Layers;
    private PdfTemplate[] app;
    private boolean reuseAppearance;
    public static final String questionMark = "% DSUnknown\nq\n1 G\n1 g\n0.1 0 0 0.1 9 0 cm\n0 J 0 j 4 M []0 d\n1 i \n0 g\n313 292 m\n313 404 325 453 432 529 c\n478 561 504 597 504 645 c\n504 736 440 760 391 760 c\n286 760 271 681 265 626 c\n265 625 l\n100 625 l\n100 828 253 898 381 898 c\n451 898 679 878 679 650 c\n679 555 628 499 538 435 c\n488 399 467 376 467 292 c\n313 292 l\nh\n308 214 170 -164 re\nf\n0.44 G\n1.2 w\n1 1 0.4 rg\n287 318 m\n287 430 299 479 406 555 c\n451 587 478 623 478 671 c\n478 762 414 786 365 786 c\n260 786 245 707 239 652 c\n239 651 l\n74 651 l\n74 854 227 924 355 924 c\n425 924 653 904 653 676 c\n653 581 602 525 512 461 c\n462 425 441 402 441 318 c\n287 318 l\nh\n282 240 170 -164 re\nB\nQ\n";
    private Image image;
    private float imageScale;
    private String layer2Text;
    private Font layer2Font;
    private int runDirection;
    private String layer4Text;
    private PdfTemplate frm;
    private static final float TOP_SECTION = 0.3f;
    private static final float MARGIN = 2.0f;
    private PdfStamper stamper;
    private PdfStamperImp writer;
    private ByteBuffer sigout;
    private OutputStream originalout;
    private File tempFile;
    private HashMap<PdfName, PdfLiteral> exclusionLocations;
    private int boutLen;
    private boolean preClosed;
    private PdfSigLockDictionary fieldLock;
    
    PdfSignatureAppearance(final PdfStamperImp writer) {
        this.certificationLevel = 0;
        this.reasonCaption = "Reason: ";
        this.locationCaption = "Location: ";
        this.page = 1;
        this.renderingMode = RenderingMode.DESCRIPTION;
        this.signatureGraphic = null;
        this.acro6Layers = true;
        this.app = new PdfTemplate[5];
        this.reuseAppearance = false;
        this.runDirection = 1;
        this.preClosed = false;
        this.writer = writer;
        this.signDate = new GregorianCalendar();
        this.fieldName = this.getNewSigName();
        this.signatureCreator = Version.getInstance().getVersion();
    }
    
    public void setCertificationLevel(final int certificationLevel) {
        this.certificationLevel = certificationLevel;
    }
    
    public int getCertificationLevel() {
        return this.certificationLevel;
    }
    
    public String getReason() {
        return this.reason;
    }
    
    public void setReason(final String reason) {
        this.reason = reason;
    }
    
    public void setReasonCaption(final String reasonCaption) {
        this.reasonCaption = reasonCaption;
    }
    
    public String getLocation() {
        return this.location;
    }
    
    public void setLocation(final String location) {
        this.location = location;
    }
    
    public void setLocationCaption(final String locationCaption) {
        this.locationCaption = locationCaption;
    }
    
    public String getSignatureCreator() {
        return this.signatureCreator;
    }
    
    public void setSignatureCreator(final String signatureCreator) {
        this.signatureCreator = signatureCreator;
    }
    
    public String getContact() {
        return this.contact;
    }
    
    public void setContact(final String contact) {
        this.contact = contact;
    }
    
    public Calendar getSignDate() {
        return this.signDate;
    }
    
    public void setSignDate(final Calendar signDate) {
        this.signDate = signDate;
    }
    
    public InputStream getRangeStream() throws IOException {
        final RandomAccessSourceFactory fac = new RandomAccessSourceFactory();
        return new RASInputStream(fac.createRanged(this.getUnderlyingSource(), this.range));
    }
    
    private RandomAccessSource getUnderlyingSource() throws IOException {
        final RandomAccessSourceFactory fac = new RandomAccessSourceFactory();
        return (this.raf == null) ? fac.createSource(this.bout) : fac.createSource(this.raf);
    }
    
    public void addDeveloperExtension(final PdfDeveloperExtension de) {
        this.writer.addDeveloperExtension(de);
    }
    
    public PdfDictionary getCryptoDictionary() {
        return this.cryptoDictionary;
    }
    
    public void setCryptoDictionary(final PdfDictionary cryptoDictionary) {
        this.cryptoDictionary = cryptoDictionary;
    }
    
    public void setCertificate(final Certificate signCertificate) {
        this.signCertificate = signCertificate;
    }
    
    public Certificate getCertificate() {
        return this.signCertificate;
    }
    
    public SignatureEvent getSignatureEvent() {
        return this.signatureEvent;
    }
    
    public void setSignatureEvent(final SignatureEvent signatureEvent) {
        this.signatureEvent = signatureEvent;
    }
    
    public String getFieldName() {
        return this.fieldName;
    }
    
    public String getNewSigName() {
        final AcroFields af = this.writer.getAcroFields();
        String name = "Signature";
        int step = 0;
        boolean found = false;
        while (!found) {
            ++step;
            String n1 = name + step;
            if (af.getFieldItem(n1) != null) {
                continue;
            }
            n1 += ".";
            found = true;
            for (final Object element : af.getFields().keySet()) {
                final String fn = (String)element;
                if (fn.startsWith(n1)) {
                    found = false;
                    break;
                }
            }
        }
        name += step;
        return name;
    }
    
    public int getPage() {
        return this.page;
    }
    
    public Rectangle getRect() {
        return this.rect;
    }
    
    public Rectangle getPageRect() {
        return this.pageRect;
    }
    
    public boolean isInvisible() {
        return this.rect == null || this.rect.getWidth() == 0.0f || this.rect.getHeight() == 0.0f;
    }
    
    public void setVisibleSignature(final Rectangle pageRect, final int page, final String fieldName) {
        if (fieldName != null) {
            if (fieldName.indexOf(46) >= 0) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("field.names.cannot.contain.a.dot", new Object[0]));
            }
            final AcroFields af = this.writer.getAcroFields();
            final AcroFields.Item item = af.getFieldItem(fieldName);
            if (item != null) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.field.1.already.exists", fieldName));
            }
            this.fieldName = fieldName;
        }
        if (page < 1 || page > this.writer.reader.getNumberOfPages()) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("invalid.page.number.1", page));
        }
        (this.pageRect = new Rectangle(pageRect)).normalize();
        this.rect = new Rectangle(this.pageRect.getWidth(), this.pageRect.getHeight());
        this.page = page;
    }
    
    public void setVisibleSignature(final String fieldName) {
        final AcroFields af = this.writer.getAcroFields();
        final AcroFields.Item item = af.getFieldItem(fieldName);
        if (item == null) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.field.1.does.not.exist", fieldName));
        }
        final PdfDictionary merged = item.getMerged(0);
        if (!PdfName.SIG.equals(PdfReader.getPdfObject(merged.get(PdfName.FT)))) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.field.1.is.not.a.signature.field", fieldName));
        }
        this.fieldName = fieldName;
        final PdfArray r = merged.getAsArray(PdfName.RECT);
        final float llx = r.getAsNumber(0).floatValue();
        final float lly = r.getAsNumber(1).floatValue();
        final float urx = r.getAsNumber(2).floatValue();
        final float ury = r.getAsNumber(3).floatValue();
        (this.pageRect = new Rectangle(llx, lly, urx, ury)).normalize();
        this.page = item.getPage(0);
        final int rotation = this.writer.reader.getPageRotation(this.page);
        final Rectangle pageSize = this.writer.reader.getPageSizeWithRotation(this.page);
        switch (rotation) {
            case 90: {
                this.pageRect = new Rectangle(this.pageRect.getBottom(), pageSize.getTop() - this.pageRect.getLeft(), this.pageRect.getTop(), pageSize.getTop() - this.pageRect.getRight());
                break;
            }
            case 180: {
                this.pageRect = new Rectangle(pageSize.getRight() - this.pageRect.getLeft(), pageSize.getTop() - this.pageRect.getBottom(), pageSize.getRight() - this.pageRect.getRight(), pageSize.getTop() - this.pageRect.getTop());
                break;
            }
            case 270: {
                this.pageRect = new Rectangle(pageSize.getRight() - this.pageRect.getBottom(), this.pageRect.getLeft(), pageSize.getRight() - this.pageRect.getTop(), this.pageRect.getRight());
                break;
            }
        }
        if (rotation != 0) {
            this.pageRect.normalize();
        }
        this.rect = new Rectangle(this.pageRect.getWidth(), this.pageRect.getHeight());
    }
    
    public RenderingMode getRenderingMode() {
        return this.renderingMode;
    }
    
    public void setRenderingMode(final RenderingMode renderingMode) {
        this.renderingMode = renderingMode;
    }
    
    public Image getSignatureGraphic() {
        return this.signatureGraphic;
    }
    
    public void setSignatureGraphic(final Image signatureGraphic) {
        this.signatureGraphic = signatureGraphic;
    }
    
    public boolean isAcro6Layers() {
        return this.acro6Layers;
    }
    
    @Deprecated
    public void setAcro6Layers(final boolean acro6Layers) {
        this.acro6Layers = acro6Layers;
    }
    
    public PdfTemplate getLayer(final int layer) {
        if (layer < 0 || layer >= this.app.length) {
            return null;
        }
        PdfTemplate t = this.app[layer];
        if (t == null) {
            final PdfTemplate[] app = this.app;
            final PdfTemplate pdfTemplate = new PdfTemplate(this.writer);
            app[layer] = pdfTemplate;
            t = pdfTemplate;
            t.setBoundingBox(this.rect);
            this.writer.addDirectTemplateSimple(t, new PdfName("n" + layer));
        }
        return t;
    }
    
    public void setReuseAppearance(final boolean reuseAppearance) {
        this.reuseAppearance = reuseAppearance;
    }
    
    public Image getImage() {
        return this.image;
    }
    
    public void setImage(final Image image) {
        this.image = image;
    }
    
    public float getImageScale() {
        return this.imageScale;
    }
    
    public void setImageScale(final float imageScale) {
        this.imageScale = imageScale;
    }
    
    public void setLayer2Text(final String text) {
        this.layer2Text = text;
    }
    
    public String getLayer2Text() {
        return this.layer2Text;
    }
    
    public Font getLayer2Font() {
        return this.layer2Font;
    }
    
    public void setLayer2Font(final Font layer2Font) {
        this.layer2Font = layer2Font;
    }
    
    public void setRunDirection(final int runDirection) {
        if (runDirection < 0 || runDirection > 3) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("invalid.run.direction.1", runDirection));
        }
        this.runDirection = runDirection;
    }
    
    public int getRunDirection() {
        return this.runDirection;
    }
    
    public void setLayer4Text(final String text) {
        this.layer4Text = text;
    }
    
    public String getLayer4Text() {
        return this.layer4Text;
    }
    
    public PdfTemplate getTopLayer() {
        if (this.frm == null) {
            (this.frm = new PdfTemplate(this.writer)).setBoundingBox(this.rect);
            this.writer.addDirectTemplateSimple(this.frm, new PdfName("FRM"));
        }
        return this.frm;
    }
    
    public PdfTemplate getAppearance() throws DocumentException {
        if (this.isInvisible()) {
            final PdfTemplate t = new PdfTemplate(this.writer);
            t.setBoundingBox(new Rectangle(0.0f, 0.0f));
            this.writer.addDirectTemplateSimple(t, null);
            return t;
        }
        if (this.app[0] == null && !this.reuseAppearance) {
            this.createBlankN0();
        }
        if (this.app[1] == null && !this.acro6Layers) {
            final PdfTemplate[] app = this.app;
            final int n2 = 1;
            final PdfTemplate pdfTemplate = new PdfTemplate(this.writer);
            app[n2] = pdfTemplate;
            final PdfTemplate t = pdfTemplate;
            t.setBoundingBox(new Rectangle(100.0f, 100.0f));
            this.writer.addDirectTemplateSimple(t, new PdfName("n1"));
            t.setLiteral("% DSUnknown\nq\n1 G\n1 g\n0.1 0 0 0.1 9 0 cm\n0 J 0 j 4 M []0 d\n1 i \n0 g\n313 292 m\n313 404 325 453 432 529 c\n478 561 504 597 504 645 c\n504 736 440 760 391 760 c\n286 760 271 681 265 626 c\n265 625 l\n100 625 l\n100 828 253 898 381 898 c\n451 898 679 878 679 650 c\n679 555 628 499 538 435 c\n488 399 467 376 467 292 c\n313 292 l\nh\n308 214 170 -164 re\nf\n0.44 G\n1.2 w\n1 1 0.4 rg\n287 318 m\n287 430 299 479 406 555 c\n451 587 478 623 478 671 c\n478 762 414 786 365 786 c\n260 786 245 707 239 652 c\n239 651 l\n74 651 l\n74 854 227 924 355 924 c\n425 924 653 904 653 676 c\n653 581 602 525 512 461 c\n462 425 441 402 441 318 c\n287 318 l\nh\n282 240 170 -164 re\nB\nQ\n");
        }
        if (this.app[2] == null) {
            String text;
            if (this.layer2Text == null) {
                final StringBuilder buf = new StringBuilder();
                buf.append("Digitally signed by ");
                String name = null;
                final CertificateInfo.X500Name x500name = CertificateInfo.getSubjectFields((X509Certificate)this.signCertificate);
                if (x500name != null) {
                    name = x500name.getField("CN");
                    if (name == null) {
                        name = x500name.getField("E");
                    }
                }
                if (name == null) {
                    name = "";
                }
                buf.append(name).append('\n');
                final SimpleDateFormat sd = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");
                buf.append("Date: ").append(sd.format(this.signDate.getTime()));
                if (this.reason != null) {
                    buf.append('\n').append(this.reasonCaption).append(this.reason);
                }
                if (this.location != null) {
                    buf.append('\n').append(this.locationCaption).append(this.location);
                }
                text = buf.toString();
            }
            else {
                text = this.layer2Text;
            }
            final PdfTemplate[] app2 = this.app;
            final int n3 = 2;
            final PdfTemplate pdfTemplate2 = new PdfTemplate(this.writer);
            app2[n3] = pdfTemplate2;
            final PdfTemplate t2 = pdfTemplate2;
            t2.setBoundingBox(this.rect);
            this.writer.addDirectTemplateSimple(t2, new PdfName("n2"));
            if (this.image != null) {
                if (this.imageScale == 0.0f) {
                    t2.addImage(this.image, this.rect.getWidth(), 0.0f, 0.0f, this.rect.getHeight(), 0.0f, 0.0f);
                }
                else {
                    float usableScale = this.imageScale;
                    if (this.imageScale < 0.0f) {
                        usableScale = Math.min(this.rect.getWidth() / this.image.getWidth(), this.rect.getHeight() / this.image.getHeight());
                    }
                    final float w = this.image.getWidth() * usableScale;
                    final float h = this.image.getHeight() * usableScale;
                    final float x = (this.rect.getWidth() - w) / 2.0f;
                    final float y = (this.rect.getHeight() - h) / 2.0f;
                    t2.addImage(this.image, w, 0.0f, 0.0f, h, x, y);
                }
            }
            Font font;
            if (this.layer2Font == null) {
                font = new Font();
            }
            else {
                font = new Font(this.layer2Font);
            }
            float size = font.getSize();
            Rectangle dataRect = null;
            Rectangle signatureRect = null;
            if (this.renderingMode == RenderingMode.NAME_AND_DESCRIPTION || (this.renderingMode == RenderingMode.GRAPHIC_AND_DESCRIPTION && this.signatureGraphic != null)) {
                signatureRect = new Rectangle(2.0f, 2.0f, this.rect.getWidth() / 2.0f - 2.0f, this.rect.getHeight() - 2.0f);
                dataRect = new Rectangle(this.rect.getWidth() / 2.0f + 1.0f, 2.0f, this.rect.getWidth() - 1.0f, this.rect.getHeight() - 2.0f);
                if (this.rect.getHeight() > this.rect.getWidth()) {
                    signatureRect = new Rectangle(2.0f, this.rect.getHeight() / 2.0f, this.rect.getWidth() - 2.0f, this.rect.getHeight());
                    dataRect = new Rectangle(2.0f, 2.0f, this.rect.getWidth() - 2.0f, this.rect.getHeight() / 2.0f - 2.0f);
                }
            }
            else if (this.renderingMode == RenderingMode.GRAPHIC) {
                if (this.signatureGraphic == null) {
                    throw new IllegalStateException(MessageLocalization.getComposedMessage("a.signature.image.should.be.present.when.rendering.mode.is.graphic.only", new Object[0]));
                }
                signatureRect = new Rectangle(2.0f, 2.0f, this.rect.getWidth() - 2.0f, this.rect.getHeight() - 2.0f);
            }
            else {
                dataRect = new Rectangle(2.0f, 2.0f, this.rect.getWidth() - 2.0f, this.rect.getHeight() * 0.7f - 2.0f);
            }
            switch (this.renderingMode) {
                case NAME_AND_DESCRIPTION: {
                    String signedBy = CertificateInfo.getSubjectFields((X509Certificate)this.signCertificate).getField("CN");
                    if (signedBy == null) {
                        signedBy = CertificateInfo.getSubjectFields((X509Certificate)this.signCertificate).getField("E");
                    }
                    if (signedBy == null) {
                        signedBy = "";
                    }
                    final Rectangle sr2 = new Rectangle(signatureRect.getWidth() - 2.0f, signatureRect.getHeight() - 2.0f);
                    final float signedSize = ColumnText.fitText(font, signedBy, sr2, -1.0f, this.runDirection);
                    final ColumnText ct2 = new ColumnText(t2);
                    ct2.setRunDirection(this.runDirection);
                    ct2.setSimpleColumn(new Phrase(signedBy, font), signatureRect.getLeft(), signatureRect.getBottom(), signatureRect.getRight(), signatureRect.getTop(), signedSize, 0);
                    ct2.go();
                    break;
                }
                case GRAPHIC_AND_DESCRIPTION: {
                    if (this.signatureGraphic == null) {
                        throw new IllegalStateException(MessageLocalization.getComposedMessage("a.signature.image.should.be.present.when.rendering.mode.is.graphic.and.description", new Object[0]));
                    }
                    final ColumnText ct2 = new ColumnText(t2);
                    ct2.setRunDirection(this.runDirection);
                    ct2.setSimpleColumn(signatureRect.getLeft(), signatureRect.getBottom(), signatureRect.getRight(), signatureRect.getTop(), 0.0f, 2);
                    final Image im = Image.getInstance(this.signatureGraphic);
                    im.scaleToFit(signatureRect.getWidth(), signatureRect.getHeight());
                    final Paragraph p = new Paragraph();
                    float x2 = 0.0f;
                    float y2 = -im.getScaledHeight() + 15.0f;
                    x2 += (signatureRect.getWidth() - im.getScaledWidth()) / 2.0f;
                    y2 -= (signatureRect.getHeight() - im.getScaledHeight()) / 2.0f;
                    p.add(new Chunk(im, x2 + (signatureRect.getWidth() - im.getScaledWidth()) / 2.0f, y2, false));
                    ct2.addElement(p);
                    ct2.go();
                    break;
                }
                case GRAPHIC: {
                    final ColumnText ct2 = new ColumnText(t2);
                    ct2.setRunDirection(this.runDirection);
                    ct2.setSimpleColumn(signatureRect.getLeft(), signatureRect.getBottom(), signatureRect.getRight(), signatureRect.getTop(), 0.0f, 2);
                    final Image im = Image.getInstance(this.signatureGraphic);
                    im.scaleToFit(signatureRect.getWidth(), signatureRect.getHeight());
                    final Paragraph p = new Paragraph(signatureRect.getHeight());
                    final float x2 = (signatureRect.getWidth() - im.getScaledWidth()) / 2.0f;
                    final float y2 = (signatureRect.getHeight() - im.getScaledHeight()) / 2.0f;
                    p.add(new Chunk(im, x2, y2, false));
                    ct2.addElement(p);
                    ct2.go();
                    break;
                }
            }
            if (this.renderingMode != RenderingMode.GRAPHIC) {
                if (size <= 0.0f) {
                    final Rectangle sr3 = new Rectangle(dataRect.getWidth(), dataRect.getHeight());
                    size = ColumnText.fitText(font, text, sr3, 12.0f, this.runDirection);
                }
                final ColumnText ct3 = new ColumnText(t2);
                ct3.setRunDirection(this.runDirection);
                ct3.setSimpleColumn(new Phrase(text, font), dataRect.getLeft(), dataRect.getBottom(), dataRect.getRight(), dataRect.getTop(), size, 0);
                ct3.go();
            }
        }
        if (this.app[3] == null && !this.acro6Layers) {
            final PdfTemplate[] app3 = this.app;
            final int n4 = 3;
            final PdfTemplate pdfTemplate3 = new PdfTemplate(this.writer);
            app3[n4] = pdfTemplate3;
            final PdfTemplate t = pdfTemplate3;
            t.setBoundingBox(new Rectangle(100.0f, 100.0f));
            this.writer.addDirectTemplateSimple(t, new PdfName("n3"));
            t.setLiteral("% DSBlank\n");
        }
        if (this.app[4] == null && !this.acro6Layers) {
            final PdfTemplate[] app4 = this.app;
            final int n5 = 4;
            final PdfTemplate pdfTemplate4 = new PdfTemplate(this.writer);
            app4[n5] = pdfTemplate4;
            final PdfTemplate t = pdfTemplate4;
            t.setBoundingBox(new Rectangle(0.0f, this.rect.getHeight() * 0.7f, this.rect.getRight(), this.rect.getTop()));
            this.writer.addDirectTemplateSimple(t, new PdfName("n4"));
            Font font2;
            if (this.layer2Font == null) {
                font2 = new Font();
            }
            else {
                font2 = new Font(this.layer2Font);
            }
            String text2 = "Signature Not Verified";
            if (this.layer4Text != null) {
                text2 = this.layer4Text;
            }
            final Rectangle sr4 = new Rectangle(this.rect.getWidth() - 4.0f, this.rect.getHeight() * 0.3f - 4.0f);
            final float size2 = ColumnText.fitText(font2, text2, sr4, 15.0f, this.runDirection);
            final ColumnText ct4 = new ColumnText(t);
            ct4.setRunDirection(this.runDirection);
            ct4.setSimpleColumn(new Phrase(text2, font2), 2.0f, 0.0f, this.rect.getWidth() - 2.0f, this.rect.getHeight() - 2.0f, size2, 0);
            ct4.go();
        }
        final int rotation = this.writer.reader.getPageRotation(this.page);
        Rectangle rotated = new Rectangle(this.rect);
        for (int n = rotation; n > 0; n -= 90) {
            rotated = rotated.rotate();
        }
        if (this.frm == null) {
            (this.frm = new PdfTemplate(this.writer)).setBoundingBox(rotated);
            this.writer.addDirectTemplateSimple(this.frm, new PdfName("FRM"));
            float scale = Math.min(this.rect.getWidth(), this.rect.getHeight()) * 0.9f;
            final float x3 = (this.rect.getWidth() - scale) / 2.0f;
            final float y3 = (this.rect.getHeight() - scale) / 2.0f;
            scale /= 100.0f;
            if (rotation == 90) {
                this.frm.concatCTM(0.0f, 1.0f, -1.0f, 0.0f, this.rect.getHeight(), 0.0f);
            }
            else if (rotation == 180) {
                this.frm.concatCTM(-1.0f, 0.0f, 0.0f, -1.0f, this.rect.getWidth(), this.rect.getHeight());
            }
            else if (rotation == 270) {
                this.frm.concatCTM(0.0f, -1.0f, 1.0f, 0.0f, 0.0f, this.rect.getWidth());
            }
            if (this.reuseAppearance) {
                final AcroFields af = this.writer.getAcroFields();
                final PdfIndirectReference ref = af.getNormalAppearance(this.getFieldName());
                if (ref != null) {
                    this.frm.addTemplateReference(ref, new PdfName("n0"), 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);
                }
                else {
                    this.reuseAppearance = false;
                    if (this.app[0] == null) {
                        this.createBlankN0();
                    }
                }
            }
            if (!this.reuseAppearance) {
                this.frm.addTemplate(this.app[0], 0.0f, 0.0f);
            }
            if (!this.acro6Layers) {
                this.frm.addTemplate(this.app[1], scale, 0.0f, 0.0f, scale, x3, y3);
            }
            this.frm.addTemplate(this.app[2], 0.0f, 0.0f);
            if (!this.acro6Layers) {
                this.frm.addTemplate(this.app[3], scale, 0.0f, 0.0f, scale, x3, y3);
                this.frm.addTemplate(this.app[4], 0.0f, 0.0f);
            }
        }
        final PdfTemplate napp = new PdfTemplate(this.writer);
        napp.setBoundingBox(rotated);
        this.writer.addDirectTemplateSimple(napp, null);
        napp.addTemplate(this.frm, 0.0f, 0.0f);
        return napp;
    }
    
    private void createBlankN0() {
        final PdfTemplate[] app = this.app;
        final int n = 0;
        final PdfTemplate pdfTemplate = new PdfTemplate(this.writer);
        app[n] = pdfTemplate;
        final PdfTemplate t = pdfTemplate;
        t.setBoundingBox(new Rectangle(100.0f, 100.0f));
        this.writer.addDirectTemplateSimple(t, new PdfName("n0"));
        t.setLiteral("% DSBlank\n");
    }
    
    public PdfStamper getStamper() {
        return this.stamper;
    }
    
    void setStamper(final PdfStamper stamper) {
        this.stamper = stamper;
    }
    
    ByteBuffer getSigout() {
        return this.sigout;
    }
    
    void setSigout(final ByteBuffer sigout) {
        this.sigout = sigout;
    }
    
    OutputStream getOriginalout() {
        return this.originalout;
    }
    
    void setOriginalout(final OutputStream originalout) {
        this.originalout = originalout;
    }
    
    public File getTempFile() {
        return this.tempFile;
    }
    
    void setTempFile(final File tempFile) {
        this.tempFile = tempFile;
    }
    
    public PdfSigLockDictionary getFieldLockDict() {
        return this.fieldLock;
    }
    
    public void setFieldLockDict(final PdfSigLockDictionary fieldLock) {
        this.fieldLock = fieldLock;
    }
    
    public boolean isPreClosed() {
        return this.preClosed;
    }
    
    public void preClose(final HashMap<PdfName, Integer> exclusionSizes) throws IOException, DocumentException {
        if (this.preClosed) {
            throw new DocumentException(MessageLocalization.getComposedMessage("document.already.pre.closed", new Object[0]));
        }
        this.stamper.mergeVerification();
        this.preClosed = true;
        final AcroFields af = this.writer.getAcroFields();
        final String name = this.getFieldName();
        final boolean fieldExists = af.doesSignatureFieldExist(name);
        final PdfIndirectReference refSig = this.writer.getPdfIndirectReference();
        this.writer.setSigFlags(3);
        PdfDictionary fieldLock = null;
        if (fieldExists) {
            final PdfDictionary widget = af.getFieldItem(name).getWidget(0);
            this.writer.markUsed(widget);
            fieldLock = widget.getAsDict(PdfName.LOCK);
            if (fieldLock == null && this.fieldLock != null) {
                widget.put(PdfName.LOCK, this.writer.addToBody(this.fieldLock).getIndirectReference());
                fieldLock = this.fieldLock;
            }
            widget.put(PdfName.P, this.writer.getPageReference(this.getPage()));
            widget.put(PdfName.V, refSig);
            final PdfObject obj = PdfReader.getPdfObjectRelease(widget.get(PdfName.F));
            int flags = 0;
            if (obj != null && obj.isNumber()) {
                flags = ((PdfNumber)obj).intValue();
            }
            flags |= 0x80;
            widget.put(PdfName.F, new PdfNumber(flags));
            final PdfDictionary ap = new PdfDictionary();
            ap.put(PdfName.N, this.getAppearance().getIndirectReference());
            widget.put(PdfName.AP, ap);
        }
        else {
            final PdfFormField sigField = PdfFormField.createSignature(this.writer);
            sigField.setFieldName(name);
            sigField.put(PdfName.V, refSig);
            sigField.setFlags(132);
            if (this.fieldLock != null) {
                sigField.put(PdfName.LOCK, this.writer.addToBody(this.fieldLock).getIndirectReference());
                fieldLock = this.fieldLock;
            }
            final int pagen = this.getPage();
            if (!this.isInvisible()) {
                sigField.setWidget(this.getPageRect(), null);
            }
            else {
                sigField.setWidget(new Rectangle(0.0f, 0.0f), null);
            }
            sigField.setAppearance(PdfAnnotation.APPEARANCE_NORMAL, this.getAppearance());
            sigField.setPage(pagen);
            this.writer.addAnnotation(sigField, pagen);
        }
        this.exclusionLocations = new HashMap<PdfName, PdfLiteral>();
        if (this.cryptoDictionary == null) {
            throw new DocumentException("No crypto dictionary defined.");
        }
        PdfLiteral lit = new PdfLiteral(80);
        this.exclusionLocations.put(PdfName.BYTERANGE, lit);
        this.cryptoDictionary.put(PdfName.BYTERANGE, lit);
        for (final Map.Entry<PdfName, Integer> entry : exclusionSizes.entrySet()) {
            final PdfName key = entry.getKey();
            final Integer v = entry.getValue();
            lit = new PdfLiteral(v);
            this.exclusionLocations.put(key, lit);
            this.cryptoDictionary.put(key, lit);
        }
        if (this.certificationLevel > 0) {
            this.addDocMDP(this.cryptoDictionary);
        }
        if (fieldLock != null) {
            this.addFieldMDP(this.cryptoDictionary, fieldLock);
        }
        if (this.signatureEvent != null) {
            this.signatureEvent.getSignatureDictionary(this.cryptoDictionary);
        }
        this.writer.addToBody(this.cryptoDictionary, refSig, false);
        if (this.certificationLevel > 0) {
            final PdfDictionary docmdp = new PdfDictionary();
            docmdp.put(new PdfName("DocMDP"), refSig);
            this.writer.reader.getCatalog().put(new PdfName("Perms"), docmdp);
        }
        this.writer.close(this.stamper.getMoreInfo());
        this.range = new long[this.exclusionLocations.size() * 2];
        final long byteRangePosition = this.exclusionLocations.get(PdfName.BYTERANGE).getPosition();
        this.exclusionLocations.remove(PdfName.BYTERANGE);
        int idx = 1;
        for (final PdfLiteral lit2 : this.exclusionLocations.values()) {
            final long n = lit2.getPosition();
            this.range[idx++] = n;
            this.range[idx++] = lit2.getPosLength() + n;
        }
        Arrays.sort(this.range, 1, this.range.length - 1);
        for (int k = 3; k < this.range.length - 2; k += 2) {
            final long[] range = this.range;
            final int n2 = k;
            range[n2] -= this.range[k - 1];
        }
        if (this.tempFile == null) {
            this.bout = this.sigout.getBuffer();
            this.boutLen = this.sigout.size();
            this.range[this.range.length - 1] = this.boutLen - this.range[this.range.length - 2];
            final ByteBuffer bf = new ByteBuffer();
            bf.append('[');
            for (int i = 0; i < this.range.length; ++i) {
                bf.append(this.range[i]).append(' ');
            }
            bf.append(']');
            System.arraycopy(bf.getBuffer(), 0, this.bout, (int)byteRangePosition, bf.size());
        }
        else {
            try {
                this.raf = new RandomAccessFile(this.tempFile, "rw");
                final long len = this.raf.length();
                this.range[this.range.length - 1] = len - this.range[this.range.length - 2];
                final ByteBuffer bf2 = new ByteBuffer();
                bf2.append('[');
                for (int j = 0; j < this.range.length; ++j) {
                    bf2.append(this.range[j]).append(' ');
                }
                bf2.append(']');
                this.raf.seek(byteRangePosition);
                this.raf.write(bf2.getBuffer(), 0, bf2.size());
            }
            catch (IOException e) {
                try {
                    this.raf.close();
                }
                catch (Exception ex) {}
                try {
                    this.tempFile.delete();
                }
                catch (Exception ex2) {}
                throw e;
            }
        }
    }
    
    private void addDocMDP(final PdfDictionary crypto) {
        final PdfDictionary reference = new PdfDictionary();
        final PdfDictionary transformParams = new PdfDictionary();
        transformParams.put(PdfName.P, new PdfNumber(this.certificationLevel));
        transformParams.put(PdfName.V, new PdfName("1.2"));
        transformParams.put(PdfName.TYPE, PdfName.TRANSFORMPARAMS);
        reference.put(PdfName.TRANSFORMMETHOD, PdfName.DOCMDP);
        reference.put(PdfName.TYPE, PdfName.SIGREF);
        reference.put(PdfName.TRANSFORMPARAMS, transformParams);
        if (this.writer.getPdfVersion().getVersion() < '6') {
            reference.put(new PdfName("DigestValue"), new PdfString("aa"));
            final PdfArray loc = new PdfArray();
            loc.add(new PdfNumber(0));
            loc.add(new PdfNumber(0));
            reference.put(new PdfName("DigestLocation"), loc);
            reference.put(new PdfName("DigestMethod"), new PdfName("MD5"));
        }
        reference.put(PdfName.DATA, this.writer.reader.getTrailer().get(PdfName.ROOT));
        final PdfArray types = new PdfArray();
        types.add(reference);
        crypto.put(PdfName.REFERENCE, types);
    }
    
    private void addFieldMDP(final PdfDictionary crypto, final PdfDictionary fieldLock) {
        final PdfDictionary reference = new PdfDictionary();
        final PdfDictionary transformParams = new PdfDictionary();
        transformParams.putAll(fieldLock);
        transformParams.put(PdfName.TYPE, PdfName.TRANSFORMPARAMS);
        transformParams.put(PdfName.V, new PdfName("1.2"));
        reference.put(PdfName.TRANSFORMMETHOD, PdfName.FIELDMDP);
        reference.put(PdfName.TYPE, PdfName.SIGREF);
        reference.put(PdfName.TRANSFORMPARAMS, transformParams);
        reference.put(new PdfName("DigestValue"), new PdfString("aa"));
        final PdfArray loc = new PdfArray();
        loc.add(new PdfNumber(0));
        loc.add(new PdfNumber(0));
        reference.put(new PdfName("DigestLocation"), loc);
        reference.put(new PdfName("DigestMethod"), new PdfName("MD5"));
        reference.put(PdfName.DATA, this.writer.reader.getTrailer().get(PdfName.ROOT));
        PdfArray types = crypto.getAsArray(PdfName.REFERENCE);
        if (types == null) {
            types = new PdfArray();
        }
        types.add(reference);
        crypto.put(PdfName.REFERENCE, types);
    }
    
    public void close(final PdfDictionary update) throws IOException, DocumentException {
        try {
            if (!this.preClosed) {
                throw new DocumentException(MessageLocalization.getComposedMessage("preclose.must.be.called.first", new Object[0]));
            }
            final ByteBuffer bf = new ByteBuffer();
            for (final PdfName key : update.getKeys()) {
                final PdfObject obj = update.get(key);
                final PdfLiteral lit = this.exclusionLocations.get(key);
                if (lit == null) {
                    throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.key.1.didn.t.reserve.space.in.preclose", key.toString()));
                }
                bf.reset();
                obj.toPdf(null, bf);
                if (bf.size() > lit.getPosLength()) {
                    throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.key.1.is.too.big.is.2.reserved.3", key.toString(), String.valueOf(bf.size()), String.valueOf(lit.getPosLength())));
                }
                if (this.tempFile == null) {
                    System.arraycopy(bf.getBuffer(), 0, this.bout, (int)lit.getPosition(), bf.size());
                }
                else {
                    this.raf.seek(lit.getPosition());
                    this.raf.write(bf.getBuffer(), 0, bf.size());
                }
            }
            if (update.size() != this.exclusionLocations.size()) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.update.dictionary.has.less.keys.than.required", new Object[0]));
            }
            if (this.tempFile == null) {
                this.originalout.write(this.bout, 0, this.boutLen);
            }
            else if (this.originalout != null) {
                this.raf.seek(0L);
                long length = this.raf.length();
                final byte[] buf = new byte[8192];
                while (length > 0L) {
                    final int r = this.raf.read(buf, 0, (int)Math.min(buf.length, length));
                    if (r < 0) {
                        throw new EOFException(MessageLocalization.getComposedMessage("unexpected.eof", new Object[0]));
                    }
                    this.originalout.write(buf, 0, r);
                    length -= r;
                }
            }
        }
        finally {
            this.writer.reader.close();
            if (this.tempFile != null) {
                try {
                    this.raf.close();
                }
                catch (Exception ex) {}
                if (this.originalout != null) {
                    try {
                        this.tempFile.delete();
                    }
                    catch (Exception ex2) {}
                }
            }
            if (this.originalout != null) {
                try {
                    this.originalout.close();
                }
                catch (Exception ex3) {}
            }
        }
    }
    
    public enum RenderingMode
    {
        DESCRIPTION, 
        NAME_AND_DESCRIPTION, 
        GRAPHIC_AND_DESCRIPTION, 
        GRAPHIC;
    }
    
    public interface SignatureEvent
    {
        void getSignatureDictionary(final PdfDictionary p0);
    }
}
