// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.pdf.fonts.otf.Language;
import com.itextpdf.text.pdf.languages.BanglaGlyphRepositioner;
import java.util.Collections;
import com.itextpdf.text.pdf.languages.GlyphRepositioner;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.TreeSet;
import com.itextpdf.text.pdf.languages.IndicCompositeCharacterComparator;
import java.io.UnsupportedEncodingException;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Utilities;
import java.util.HashMap;

class FontDetails
{
    PdfIndirectReference indirectReference;
    PdfName fontName;
    BaseFont baseFont;
    TrueTypeFontUnicode ttu;
    CJKFont cjkFont;
    byte[] shortTag;
    HashMap<Integer, int[]> longTag;
    IntHashtable cjkTag;
    int fontType;
    boolean symbolic;
    protected boolean subset;
    
    FontDetails(final PdfName fontName, final PdfIndirectReference indirectReference, final BaseFont baseFont) {
        this.subset = true;
        this.fontName = fontName;
        this.indirectReference = indirectReference;
        this.baseFont = baseFont;
        switch (this.fontType = baseFont.getFontType()) {
            case 0:
            case 1: {
                this.shortTag = new byte[256];
                break;
            }
            case 2: {
                this.cjkTag = new IntHashtable();
                this.cjkFont = (CJKFont)baseFont;
                break;
            }
            case 3: {
                this.longTag = new HashMap<Integer, int[]>();
                this.ttu = (TrueTypeFontUnicode)baseFont;
                this.symbolic = baseFont.isFontSpecific();
                break;
            }
        }
    }
    
    PdfIndirectReference getIndirectReference() {
        return this.indirectReference;
    }
    
    PdfName getFontName() {
        return this.fontName;
    }
    
    BaseFont getBaseFont() {
        return this.baseFont;
    }
    
    Object[] convertToBytesGid(final String gids) {
        if (this.fontType != 3) {
            throw new IllegalArgumentException("GID require TT Unicode");
        }
        try {
            final StringBuilder sb = new StringBuilder();
            int totalWidth = 0;
            for (final char gid : gids.toCharArray()) {
                final int width = this.ttu.getGlyphWidth(gid);
                totalWidth += width;
                final int vchar = this.ttu.GetCharFromGlyphId(gid);
                if (vchar != 0) {
                    sb.append(Utilities.convertFromUtf32(vchar));
                }
                final Integer gl = (int)gid;
                if (!this.longTag.containsKey(gl)) {
                    this.longTag.put(gl, new int[] { gid, width, vchar });
                }
            }
            return new Object[] { gids.getBytes("UnicodeBigUnmarked"), sb.toString(), totalWidth };
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    byte[] convertToBytes(final String text) {
        byte[] b = null;
        switch (this.fontType) {
            case 5: {
                return this.baseFont.convertToBytes(text);
            }
            case 0:
            case 1: {
                b = this.baseFont.convertToBytes(text);
                for (int len = b.length, k = 0; k < len; ++k) {
                    this.shortTag[b[k] & 0xFF] = 1;
                }
                break;
            }
            case 2: {
                final int len = text.length();
                if (this.cjkFont.isIdentity()) {
                    for (int k = 0; k < len; ++k) {
                        this.cjkTag.put(text.charAt(k), 0);
                    }
                }
                else {
                    for (int k = 0; k < len; ++k) {
                        int val;
                        if (Utilities.isSurrogatePair(text, k)) {
                            val = Utilities.convertToUtf32(text, k);
                            ++k;
                        }
                        else {
                            val = text.charAt(k);
                        }
                        this.cjkTag.put(this.cjkFont.getCidCode(val), 0);
                    }
                }
                b = this.cjkFont.convertToBytes(text);
                break;
            }
            case 4: {
                b = this.baseFont.convertToBytes(text);
                break;
            }
            case 3: {
                try {
                    int len = text.length();
                    int[] metrics = null;
                    char[] glyph = new char[len];
                    int i = 0;
                    if (this.symbolic) {
                        b = PdfEncodings.convertToBytes(text, "symboltt");
                        len = b.length;
                        for (int j = 0; j < len; ++j) {
                            metrics = this.ttu.getMetricsTT(b[j] & 0xFF);
                            if (metrics != null) {
                                this.longTag.put(metrics[0], new int[] { metrics[0], metrics[1], this.ttu.getUnicodeDifferences(b[j] & 0xFF) });
                                glyph[i++] = (char)metrics[0];
                            }
                        }
                    }
                    else {
                        if (this.canApplyGlyphSubstitution()) {
                            return this.convertToBytesAfterGlyphSubstitution(text);
                        }
                        for (int j = 0; j < len; ++j) {
                            int val2;
                            if (Utilities.isSurrogatePair(text, j)) {
                                val2 = Utilities.convertToUtf32(text, j);
                                ++j;
                            }
                            else {
                                val2 = text.charAt(j);
                            }
                            metrics = this.ttu.getMetricsTT(val2);
                            if (metrics != null) {
                                final int m0 = metrics[0];
                                final Integer gl = m0;
                                if (!this.longTag.containsKey(gl)) {
                                    this.longTag.put(gl, new int[] { m0, metrics[1], val2 });
                                }
                                glyph[i++] = (char)m0;
                            }
                        }
                    }
                    glyph = Utilities.copyOfRange(glyph, 0, i);
                    b = StringUtils.convertCharsToBytes(glyph);
                }
                catch (UnsupportedEncodingException e) {
                    throw new ExceptionConverter(e);
                }
                break;
            }
        }
        return b;
    }
    
    private boolean canApplyGlyphSubstitution() {
        return this.fontType == 3 && this.ttu.getGlyphSubstitutionMap() != null;
    }
    
    private byte[] convertToBytesAfterGlyphSubstitution(final String text) throws UnsupportedEncodingException {
        if (!this.canApplyGlyphSubstitution()) {
            throw new IllegalArgumentException("Make sure the font type if TTF Unicode and a valid GlyphSubstitutionTable exists!");
        }
        final Map<String, Glyph> glyphSubstitutionMap = this.ttu.getGlyphSubstitutionMap();
        final Set<String> compositeCharacters = new TreeSet<String>(new IndicCompositeCharacterComparator());
        compositeCharacters.addAll(glyphSubstitutionMap.keySet());
        final ArrayBasedStringTokenizer tokenizer = new ArrayBasedStringTokenizer(compositeCharacters.toArray(new String[0]));
        final String[] tokens = tokenizer.tokenize(text);
        final List<Glyph> glyphList = new ArrayList<Glyph>(50);
        for (final String token : tokens) {
            final Glyph subsGlyph = glyphSubstitutionMap.get(token);
            if (subsGlyph != null) {
                glyphList.add(subsGlyph);
            }
            else {
                for (final char c : token.toCharArray()) {
                    final int[] metrics = this.ttu.getMetricsTT(c);
                    final int glyphCode = metrics[0];
                    final int glyphWidth = metrics[1];
                    glyphList.add(new Glyph(glyphCode, glyphWidth, String.valueOf(c)));
                }
            }
        }
        final GlyphRepositioner glyphRepositioner = this.getGlyphRepositioner();
        if (glyphRepositioner != null) {
            glyphRepositioner.repositionGlyphs(glyphList);
        }
        final char[] charEncodedGlyphCodes = new char[glyphList.size()];
        for (int i = 0; i < glyphList.size(); ++i) {
            final Glyph glyph = glyphList.get(i);
            charEncodedGlyphCodes[i] = (char)glyph.code;
            final Integer glyphCode2 = glyph.code;
            if (!this.longTag.containsKey(glyphCode2)) {
                this.longTag.put(glyphCode2, new int[] { glyph.code, glyph.width, glyph.chars.charAt(0) });
            }
        }
        return new String(charEncodedGlyphCodes).getBytes("UnicodeBigUnmarked");
    }
    
    private GlyphRepositioner getGlyphRepositioner() {
        final Language language = this.ttu.getSupportedLanguage();
        if (language == null) {
            throw new IllegalArgumentException("The supported language field cannot be null in " + this.ttu.getClass().getName());
        }
        switch (language) {
            case BENGALI: {
                return new BanglaGlyphRepositioner(Collections.unmodifiableMap((Map<? extends Integer, ? extends int[]>)this.ttu.cmap31), this.ttu.getGlyphSubstitutionMap());
            }
            default: {
                return null;
            }
        }
    }
    
    public void writeFont(final PdfWriter writer) {
        try {
            switch (this.fontType) {
                case 5: {
                    this.baseFont.writeFont(writer, this.indirectReference, null);
                    break;
                }
                case 0:
                case 1: {
                    int firstChar;
                    for (firstChar = 0; firstChar < 256 && this.shortTag[firstChar] == 0; ++firstChar) {}
                    int lastChar;
                    for (lastChar = 255; lastChar >= firstChar && this.shortTag[lastChar] == 0; --lastChar) {}
                    if (firstChar > 255) {
                        firstChar = 255;
                        lastChar = 255;
                    }
                    this.baseFont.writeFont(writer, this.indirectReference, new Object[] { firstChar, lastChar, this.shortTag, this.subset });
                    break;
                }
                case 2: {
                    this.baseFont.writeFont(writer, this.indirectReference, new Object[] { this.cjkTag });
                    break;
                }
                case 3: {
                    this.baseFont.writeFont(writer, this.indirectReference, new Object[] { this.longTag, this.subset });
                    break;
                }
            }
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public boolean isSubset() {
        return this.subset;
    }
    
    public void setSubset(final boolean subset) {
        this.subset = subset;
    }
}
