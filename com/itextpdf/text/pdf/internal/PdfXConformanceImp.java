// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.internal;

import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfImage;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.pdf.PatternColor;
import com.itextpdf.text.pdf.ShadingColor;
import com.itextpdf.text.pdf.SpotColor;
import com.itextpdf.text.pdf.PdfXConformanceException;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.pdf.ExtendedColor;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.interfaces.PdfXConformance;

public class PdfXConformanceImp implements PdfXConformance
{
    protected int pdfxConformance;
    protected PdfWriter writer;
    
    public PdfXConformanceImp(final PdfWriter writer) {
        this.pdfxConformance = 0;
        this.writer = writer;
    }
    
    @Override
    public void setPDFXConformance(final int pdfxConformance) {
        this.pdfxConformance = pdfxConformance;
    }
    
    @Override
    public int getPDFXConformance() {
        return this.pdfxConformance;
    }
    
    @Override
    public boolean isPdfIso() {
        return this.isPdfX();
    }
    
    @Override
    public boolean isPdfX() {
        return this.pdfxConformance != 0;
    }
    
    public boolean isPdfX1A2001() {
        return this.pdfxConformance == 1;
    }
    
    public boolean isPdfX32002() {
        return this.pdfxConformance == 2;
    }
    
    @Override
    public void checkPdfIsoConformance(final int key, final Object obj1) {
        if (this.writer == null || !this.writer.isPdfX()) {
            return;
        }
        final int conf = this.writer.getPDFXConformance();
        switch (key) {
            case 1: {
                switch (conf) {
                    case 1: {
                        if (obj1 instanceof ExtendedColor) {
                            final ExtendedColor ec = (ExtendedColor)obj1;
                            switch (ec.getType()) {
                                case 1:
                                case 2: {
                                    return;
                                }
                                case 0: {
                                    throw new PdfXConformanceException(MessageLocalization.getComposedMessage("colorspace.rgb.is.not.allowed", new Object[0]));
                                }
                                case 3: {
                                    final SpotColor sc = (SpotColor)ec;
                                    this.checkPdfIsoConformance(1, sc.getPdfSpotColor().getAlternativeCS());
                                    break;
                                }
                                case 5: {
                                    final ShadingColor xc = (ShadingColor)ec;
                                    this.checkPdfIsoConformance(1, xc.getPdfShadingPattern().getShading().getColorSpace());
                                    break;
                                }
                                case 4: {
                                    final PatternColor pc = (PatternColor)ec;
                                    this.checkPdfIsoConformance(1, pc.getPainter().getDefaultColor());
                                    break;
                                }
                            }
                            break;
                        }
                        if (obj1 instanceof BaseColor) {
                            throw new PdfXConformanceException(MessageLocalization.getComposedMessage("colorspace.rgb.is.not.allowed", new Object[0]));
                        }
                        break;
                    }
                }
            }
            case 3: {
                if (conf == 1) {
                    throw new PdfXConformanceException(MessageLocalization.getComposedMessage("colorspace.rgb.is.not.allowed", new Object[0]));
                }
                break;
            }
            case 4: {
                if (!((BaseFont)obj1).isEmbedded()) {
                    throw new PdfXConformanceException(MessageLocalization.getComposedMessage("all.the.fonts.must.be.embedded.this.one.isn.t.1", ((BaseFont)obj1).getPostscriptFontName()));
                }
                break;
            }
            case 5: {
                final PdfImage image = (PdfImage)obj1;
                if (image.get(PdfName.SMASK) != null) {
                    throw new PdfXConformanceException(MessageLocalization.getComposedMessage("the.smask.key.is.not.allowed.in.images", new Object[0]));
                }
                switch (conf) {
                    case 1: {
                        final PdfObject cs = image.get(PdfName.COLORSPACE);
                        if (cs == null) {
                            return;
                        }
                        if (cs.isName()) {
                            if (PdfName.DEVICERGB.equals(cs)) {
                                throw new PdfXConformanceException(MessageLocalization.getComposedMessage("colorspace.rgb.is.not.allowed", new Object[0]));
                            }
                            break;
                        }
                        else {
                            if (cs.isArray() && PdfName.CALRGB.equals(((PdfArray)cs).getPdfObject(0))) {
                                throw new PdfXConformanceException(MessageLocalization.getComposedMessage("colorspace.calrgb.is.not.allowed", new Object[0]));
                            }
                            break;
                        }
                        break;
                    }
                }
                break;
            }
            case 6: {
                final PdfDictionary gs = (PdfDictionary)obj1;
                if (gs == null) {
                    break;
                }
                PdfObject obj2 = gs.get(PdfName.BM);
                if (obj2 != null && !PdfGState.BM_NORMAL.equals(obj2) && !PdfGState.BM_COMPATIBLE.equals(obj2)) {
                    throw new PdfXConformanceException(MessageLocalization.getComposedMessage("blend.mode.1.not.allowed", obj2.toString()));
                }
                obj2 = gs.get(PdfName.CA);
                double v = 0.0;
                if (obj2 != null && (v = ((PdfNumber)obj2).doubleValue()) != 1.0) {
                    throw new PdfXConformanceException(MessageLocalization.getComposedMessage("transparency.is.not.allowed.ca.eq.1", String.valueOf(v)));
                }
                obj2 = gs.get(PdfName.ca);
                v = 0.0;
                if (obj2 != null && (v = ((PdfNumber)obj2).doubleValue()) != 1.0) {
                    throw new PdfXConformanceException(MessageLocalization.getComposedMessage("transparency.is.not.allowed.ca.eq.1", String.valueOf(v)));
                }
                break;
            }
            case 7: {
                throw new PdfXConformanceException(MessageLocalization.getComposedMessage("layers.are.not.allowed", new Object[0]));
            }
        }
    }
}
