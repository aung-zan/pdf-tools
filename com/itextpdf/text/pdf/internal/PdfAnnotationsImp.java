// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.internal;

import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfFileSpecification;
import com.itextpdf.text.pdf.PdfAction;
import java.net.URL;
import com.itextpdf.text.Annotation;
import com.itextpdf.text.pdf.PdfTemplate;
import java.util.HashSet;
import java.io.IOException;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.pdf.PdfRectangle;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfAnnotation;
import java.util.ArrayList;
import com.itextpdf.text.pdf.PdfAcroForm;

public class PdfAnnotationsImp
{
    protected PdfAcroForm acroForm;
    protected ArrayList<PdfAnnotation> annotations;
    protected ArrayList<PdfAnnotation> delayedAnnotations;
    
    public PdfAnnotationsImp(final PdfWriter writer) {
        this.annotations = new ArrayList<PdfAnnotation>();
        this.delayedAnnotations = new ArrayList<PdfAnnotation>();
        this.acroForm = new PdfAcroForm(writer);
    }
    
    public boolean hasValidAcroForm() {
        return this.acroForm.isValid();
    }
    
    public PdfAcroForm getAcroForm() {
        return this.acroForm;
    }
    
    public void setSigFlags(final int f) {
        this.acroForm.setSigFlags(f);
    }
    
    public void addCalculationOrder(final PdfFormField formField) {
        this.acroForm.addCalculationOrder(formField);
    }
    
    public void addAnnotation(final PdfAnnotation annot) {
        if (annot.isForm()) {
            final PdfFormField field = (PdfFormField)annot;
            if (field.getParent() == null) {
                this.addFormFieldRaw(field);
            }
        }
        else {
            this.annotations.add(annot);
        }
    }
    
    public void addPlainAnnotation(final PdfAnnotation annot) {
        this.annotations.add(annot);
    }
    
    void addFormFieldRaw(final PdfFormField field) {
        this.annotations.add(field);
        final ArrayList<PdfFormField> kids = field.getKids();
        if (kids != null) {
            for (int k = 0; k < kids.size(); ++k) {
                final PdfFormField kid = kids.get(k);
                if (!kid.isUsed()) {
                    this.addFormFieldRaw(kid);
                }
            }
        }
    }
    
    public boolean hasUnusedAnnotations() {
        return !this.annotations.isEmpty();
    }
    
    public void resetAnnotations() {
        this.annotations = this.delayedAnnotations;
        this.delayedAnnotations = new ArrayList<PdfAnnotation>();
    }
    
    public PdfArray rotateAnnotations(final PdfWriter writer, final Rectangle pageSize) {
        final PdfArray array = new PdfArray();
        final int rotation = pageSize.getRotation() % 360;
        final int currentPage = writer.getCurrentPageNumber();
        for (int k = 0; k < this.annotations.size(); ++k) {
            final PdfAnnotation dic = this.annotations.get(k);
            final int page = dic.getPlaceInPage();
            if (page > currentPage) {
                this.delayedAnnotations.add(dic);
            }
            else {
                if (dic.isForm()) {
                    if (!dic.isUsed()) {
                        final HashSet<PdfTemplate> templates = dic.getTemplates();
                        if (templates != null) {
                            this.acroForm.addFieldTemplates(templates);
                        }
                    }
                    final PdfFormField field = (PdfFormField)dic;
                    if (field.getParent() == null) {
                        this.acroForm.addDocumentField(field.getIndirectReference());
                    }
                }
                if (dic.isAnnotation()) {
                    array.add(dic.getIndirectReference());
                    if (!dic.isUsed()) {
                        final PdfArray tmp = dic.getAsArray(PdfName.RECT);
                        PdfRectangle rect;
                        if (tmp.size() == 4) {
                            rect = new PdfRectangle(tmp.getAsNumber(0).floatValue(), tmp.getAsNumber(1).floatValue(), tmp.getAsNumber(2).floatValue(), tmp.getAsNumber(3).floatValue());
                        }
                        else {
                            rect = new PdfRectangle(tmp.getAsNumber(0).floatValue(), tmp.getAsNumber(1).floatValue());
                        }
                        switch (rotation) {
                            case 90: {
                                dic.put(PdfName.RECT, new PdfRectangle(pageSize.getTop() - rect.bottom(), rect.left(), pageSize.getTop() - rect.top(), rect.right()));
                                break;
                            }
                            case 180: {
                                dic.put(PdfName.RECT, new PdfRectangle(pageSize.getRight() - rect.left(), pageSize.getTop() - rect.bottom(), pageSize.getRight() - rect.right(), pageSize.getTop() - rect.top()));
                                break;
                            }
                            case 270: {
                                dic.put(PdfName.RECT, new PdfRectangle(rect.bottom(), pageSize.getRight() - rect.left(), rect.top(), pageSize.getRight() - rect.right()));
                                break;
                            }
                        }
                    }
                }
                if (!dic.isUsed()) {
                    dic.setUsed();
                    try {
                        writer.addToBody(dic, dic.getIndirectReference());
                    }
                    catch (IOException e) {
                        throw new ExceptionConverter(e);
                    }
                }
            }
        }
        return array;
    }
    
    public static PdfAnnotation convertAnnotation(final PdfWriter writer, final Annotation annot, final Rectangle defaultRect) throws IOException {
        switch (annot.annotationType()) {
            case 1: {
                return writer.createAnnotation(annot.llx(), annot.lly(), annot.urx(), annot.ury(), new PdfAction(annot.attributes().get("url")), null);
            }
            case 2: {
                return writer.createAnnotation(annot.llx(), annot.lly(), annot.urx(), annot.ury(), new PdfAction(annot.attributes().get("file")), null);
            }
            case 3: {
                return writer.createAnnotation(annot.llx(), annot.lly(), annot.urx(), annot.ury(), new PdfAction(annot.attributes().get("file"), annot.attributes().get("destination")), null);
            }
            case 7: {
                final boolean[] sparams = annot.attributes().get("parameters");
                final String fname = annot.attributes().get("file");
                final String mimetype = annot.attributes().get("mime");
                PdfFileSpecification fs;
                if (sparams[0]) {
                    fs = PdfFileSpecification.fileEmbedded(writer, fname, fname, null);
                }
                else {
                    fs = PdfFileSpecification.fileExtern(writer, fname);
                }
                final PdfAnnotation ann = PdfAnnotation.createScreen(writer, new Rectangle(annot.llx(), annot.lly(), annot.urx(), annot.ury()), fname, fs, mimetype, sparams[1]);
                return ann;
            }
            case 4: {
                return writer.createAnnotation(annot.llx(), annot.lly(), annot.urx(), annot.ury(), new PdfAction(annot.attributes().get("file"), annot.attributes().get("page")), null);
            }
            case 5: {
                return writer.createAnnotation(annot.llx(), annot.lly(), annot.urx(), annot.ury(), new PdfAction(annot.attributes().get("named")), null);
            }
            case 6: {
                return writer.createAnnotation(annot.llx(), annot.lly(), annot.urx(), annot.ury(), new PdfAction(annot.attributes().get("application"), annot.attributes().get("parameters"), annot.attributes().get("operation"), annot.attributes().get("defaultdir")), null);
            }
            default: {
                return writer.createAnnotation(defaultRect.getLeft(), defaultRect.getBottom(), defaultRect.getRight(), defaultRect.getTop(), new PdfString(annot.title(), "UnicodeBig"), new PdfString(annot.content(), "UnicodeBig"), null);
            }
        }
    }
}
