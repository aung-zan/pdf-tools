// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.pdf.draw.DrawInterface;
import java.util.Stack;
import com.itextpdf.text.List;
import com.itextpdf.text.Font;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.ListLabel;
import com.itextpdf.text.ListBody;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Image;
import com.itextpdf.text.Chunk;
import java.util.Iterator;
import java.util.Collection;
import com.itextpdf.text.log.LoggerFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Element;
import java.util.LinkedList;
import java.util.ArrayList;
import com.itextpdf.text.log.Logger;

public class ColumnText
{
    private final Logger LOGGER;
    public static final int AR_NOVOWEL = 1;
    public static final int AR_COMPOSEDTASHKEEL = 4;
    public static final int AR_LIG = 8;
    public static final int DIGITS_EN2AN = 32;
    public static final int DIGITS_AN2EN = 64;
    public static final int DIGITS_EN2AN_INIT_LR = 96;
    public static final int DIGITS_EN2AN_INIT_AL = 128;
    public static final int DIGIT_TYPE_AN = 0;
    public static final int DIGIT_TYPE_AN_EXTENDED = 256;
    protected int runDirection;
    public static final float GLOBAL_SPACE_CHAR_RATIO = 0.0f;
    public static final int START_COLUMN = 0;
    public static final int NO_MORE_TEXT = 1;
    public static final int NO_MORE_COLUMN = 2;
    protected static final int LINE_STATUS_OK = 0;
    protected static final int LINE_STATUS_OFFLIMITS = 1;
    protected static final int LINE_STATUS_NOLINE = 2;
    protected float maxY;
    protected float minY;
    protected float leftX;
    protected float rightX;
    protected int alignment;
    protected ArrayList<float[]> leftWall;
    protected ArrayList<float[]> rightWall;
    protected BidiLine bidiLine;
    protected boolean isWordSplit;
    protected float yLine;
    protected float lastX;
    protected float currentLeading;
    protected float fixedLeading;
    protected float multipliedLeading;
    protected PdfContentByte canvas;
    protected PdfContentByte[] canvases;
    protected int lineStatus;
    protected float indent;
    protected float followingIndent;
    protected float rightIndent;
    protected float extraParagraphSpace;
    protected float rectangularWidth;
    protected boolean rectangularMode;
    private float spaceCharRatio;
    private boolean lastWasNewline;
    private boolean repeatFirstLineIndent;
    private int linesWritten;
    private float firstLineY;
    private boolean firstLineYDone;
    private int arabicOptions;
    protected float descender;
    protected boolean composite;
    protected ColumnText compositeColumn;
    protected LinkedList<Element> compositeElements;
    protected int listIdx;
    protected int rowIdx;
    private int splittedRow;
    protected Phrase waitPhrase;
    private boolean useAscender;
    private float filledWidth;
    private boolean adjustFirstLine;
    private boolean inheritGraphicState;
    private boolean ignoreSpacingBefore;
    
    public ColumnText(final PdfContentByte canvas) {
        this.LOGGER = LoggerFactory.getLogger(ColumnText.class);
        this.runDirection = 1;
        this.alignment = 0;
        this.currentLeading = 16.0f;
        this.fixedLeading = 16.0f;
        this.multipliedLeading = 0.0f;
        this.indent = 0.0f;
        this.followingIndent = 0.0f;
        this.rightIndent = 0.0f;
        this.extraParagraphSpace = 0.0f;
        this.rectangularWidth = -1.0f;
        this.rectangularMode = false;
        this.spaceCharRatio = 0.0f;
        this.lastWasNewline = true;
        this.repeatFirstLineIndent = true;
        this.firstLineYDone = false;
        this.arabicOptions = 0;
        this.composite = false;
        this.listIdx = 0;
        this.rowIdx = 0;
        this.splittedRow = -2;
        this.useAscender = false;
        this.adjustFirstLine = true;
        this.inheritGraphicState = false;
        this.ignoreSpacingBefore = true;
        this.canvas = canvas;
    }
    
    public static ColumnText duplicate(final ColumnText org) {
        final ColumnText ct = new ColumnText(null);
        ct.setACopy(org);
        return ct;
    }
    
    public ColumnText setACopy(final ColumnText org) {
        if (org != null) {
            this.setSimpleVars(org);
            if (org.bidiLine != null) {
                this.bidiLine = new BidiLine(org.bidiLine);
            }
        }
        return this;
    }
    
    protected void setSimpleVars(final ColumnText org) {
        this.maxY = org.maxY;
        this.minY = org.minY;
        this.alignment = org.alignment;
        this.leftWall = null;
        if (org.leftWall != null) {
            this.leftWall = new ArrayList<float[]>(org.leftWall);
        }
        this.rightWall = null;
        if (org.rightWall != null) {
            this.rightWall = new ArrayList<float[]>(org.rightWall);
        }
        this.yLine = org.yLine;
        this.currentLeading = org.currentLeading;
        this.fixedLeading = org.fixedLeading;
        this.multipliedLeading = org.multipliedLeading;
        this.canvas = org.canvas;
        this.canvases = org.canvases;
        this.lineStatus = org.lineStatus;
        this.indent = org.indent;
        this.followingIndent = org.followingIndent;
        this.rightIndent = org.rightIndent;
        this.extraParagraphSpace = org.extraParagraphSpace;
        this.rectangularWidth = org.rectangularWidth;
        this.rectangularMode = org.rectangularMode;
        this.spaceCharRatio = org.spaceCharRatio;
        this.lastWasNewline = org.lastWasNewline;
        this.repeatFirstLineIndent = org.repeatFirstLineIndent;
        this.linesWritten = org.linesWritten;
        this.arabicOptions = org.arabicOptions;
        this.runDirection = org.runDirection;
        this.descender = org.descender;
        this.composite = org.composite;
        this.splittedRow = org.splittedRow;
        if (org.composite) {
            this.compositeElements = new LinkedList<Element>();
            for (final Element element : org.compositeElements) {
                if (element instanceof PdfPTable) {
                    this.compositeElements.add(new PdfPTable((PdfPTable)element));
                }
                else {
                    this.compositeElements.add(element);
                }
            }
            if (org.compositeColumn != null) {
                this.compositeColumn = duplicate(org.compositeColumn);
            }
        }
        this.listIdx = org.listIdx;
        this.rowIdx = org.rowIdx;
        this.firstLineY = org.firstLineY;
        this.leftX = org.leftX;
        this.rightX = org.rightX;
        this.firstLineYDone = org.firstLineYDone;
        this.waitPhrase = org.waitPhrase;
        this.useAscender = org.useAscender;
        this.filledWidth = org.filledWidth;
        this.adjustFirstLine = org.adjustFirstLine;
        this.inheritGraphicState = org.inheritGraphicState;
        this.ignoreSpacingBefore = org.ignoreSpacingBefore;
    }
    
    private void addWaitingPhrase() {
        if (this.bidiLine == null && this.waitPhrase != null) {
            this.bidiLine = new BidiLine();
            for (final Chunk c : this.waitPhrase.getChunks()) {
                this.bidiLine.addChunk(new PdfChunk(c, null, this.waitPhrase.getTabSettings()));
            }
            this.waitPhrase = null;
        }
    }
    
    public void addText(final Phrase phrase) {
        if (phrase == null || this.composite) {
            return;
        }
        this.addWaitingPhrase();
        if (this.bidiLine == null) {
            this.waitPhrase = phrase;
            return;
        }
        for (final Object element : phrase.getChunks()) {
            this.bidiLine.addChunk(new PdfChunk((Chunk)element, null, phrase.getTabSettings()));
        }
    }
    
    public void setText(final Phrase phrase) {
        this.bidiLine = null;
        this.composite = false;
        this.compositeColumn = null;
        this.compositeElements = null;
        this.listIdx = 0;
        this.rowIdx = 0;
        this.splittedRow = -1;
        this.waitPhrase = phrase;
    }
    
    public void addText(final Chunk chunk) {
        if (chunk == null || this.composite) {
            return;
        }
        this.addText(new Phrase(chunk));
    }
    
    public void addElement(Element element) {
        if (element == null) {
            return;
        }
        if (element instanceof Image) {
            final Image img = (Image)element;
            final PdfPTable t = new PdfPTable(1);
            final float w = img.getWidthPercentage();
            if (w == 0.0f) {
                t.setTotalWidth(img.getScaledWidth());
                t.setLockedWidth(true);
            }
            else {
                t.setWidthPercentage(w);
            }
            t.setSpacingAfter(img.getSpacingAfter());
            t.setSpacingBefore(img.getSpacingBefore());
            switch (img.getAlignment()) {
                case 0: {
                    t.setHorizontalAlignment(0);
                    break;
                }
                case 2: {
                    t.setHorizontalAlignment(2);
                    break;
                }
                default: {
                    t.setHorizontalAlignment(1);
                    break;
                }
            }
            final PdfPCell c = new PdfPCell(img, true);
            c.setPadding(0.0f);
            c.setBorder(img.getBorder());
            c.setBorderColor(img.getBorderColor());
            c.setBorderWidth(img.getBorderWidth());
            c.setBackgroundColor(img.getBackgroundColor());
            t.addCell(c);
            element = t;
        }
        if (element.type() == 10) {
            element = new Paragraph((Chunk)element);
        }
        else if (element.type() == 11) {
            element = new Paragraph((Phrase)element);
        }
        else if (element.type() == 23) {
            ((PdfPTable)element).init();
        }
        if (element.type() != 12 && element.type() != 14 && element.type() != 23 && element.type() != 55 && element.type() != 37) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("element.not.allowed", new Object[0]));
        }
        if (!this.composite) {
            this.composite = true;
            this.compositeElements = new LinkedList<Element>();
            this.bidiLine = null;
            this.waitPhrase = null;
        }
        if (element.type() == 12) {
            final Paragraph p = (Paragraph)element;
            this.compositeElements.addAll(p.breakUp());
            return;
        }
        this.compositeElements.add(element);
    }
    
    public static boolean isAllowedElement(final Element element) {
        final int type = element.type();
        return type == 10 || type == 11 || type == 37 || type == 12 || type == 14 || type == 55 || type == 23 || element instanceof Image;
    }
    
    protected ArrayList<float[]> convertColumn(final float[] cLine) {
        if (cLine.length < 4) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("no.valid.column.line.found", new Object[0]));
        }
        final ArrayList<float[]> cc = new ArrayList<float[]>();
        for (int k = 0; k < cLine.length - 2; k += 2) {
            final float x1 = cLine[k];
            final float y1 = cLine[k + 1];
            final float x2 = cLine[k + 2];
            final float y2 = cLine[k + 3];
            if (y1 != y2) {
                final float a = (x1 - x2) / (y1 - y2);
                final float b = x1 - a * y1;
                final float[] r = { Math.min(y1, y2), Math.max(y1, y2), a, b };
                cc.add(r);
                this.maxY = Math.max(this.maxY, r[1]);
                this.minY = Math.min(this.minY, r[0]);
            }
        }
        if (cc.isEmpty()) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("no.valid.column.line.found", new Object[0]));
        }
        return cc;
    }
    
    protected float findLimitsPoint(final ArrayList<float[]> wall) {
        this.lineStatus = 0;
        if (this.yLine < this.minY || this.yLine > this.maxY) {
            this.lineStatus = 1;
            return 0.0f;
        }
        for (int k = 0; k < wall.size(); ++k) {
            final float[] r = wall.get(k);
            if (this.yLine >= r[0] && this.yLine <= r[1]) {
                return r[2] * this.yLine + r[3];
            }
        }
        this.lineStatus = 2;
        return 0.0f;
    }
    
    protected float[] findLimitsOneLine() {
        final float x1 = this.findLimitsPoint(this.leftWall);
        if (this.lineStatus == 1 || this.lineStatus == 2) {
            return null;
        }
        final float x2 = this.findLimitsPoint(this.rightWall);
        if (this.lineStatus == 2) {
            return null;
        }
        return new float[] { x1, x2 };
    }
    
    protected float[] findLimitsTwoLines() {
        boolean repeat = false;
        while (!repeat || this.currentLeading != 0.0f) {
            repeat = true;
            final float[] x1 = this.findLimitsOneLine();
            if (this.lineStatus == 1) {
                return null;
            }
            this.yLine -= this.currentLeading;
            if (this.lineStatus == 2) {
                continue;
            }
            final float[] x2 = this.findLimitsOneLine();
            if (this.lineStatus == 1) {
                return null;
            }
            if (this.lineStatus == 2) {
                this.yLine -= this.currentLeading;
            }
            else {
                if (x1[0] >= x2[1]) {
                    continue;
                }
                if (x2[0] >= x1[1]) {
                    continue;
                }
                return new float[] { x1[0], x1[1], x2[0], x2[1] };
            }
        }
        return null;
    }
    
    public void setColumns(final float[] leftLine, final float[] rightLine) {
        this.maxY = -1.0E21f;
        this.minY = 1.0E21f;
        this.setYLine(Math.max(leftLine[1], leftLine[leftLine.length - 1]));
        this.rightWall = this.convertColumn(rightLine);
        this.leftWall = this.convertColumn(leftLine);
        this.rectangularWidth = -1.0f;
        this.rectangularMode = false;
    }
    
    public void setSimpleColumn(final Phrase phrase, final float llx, final float lly, final float urx, final float ury, final float leading, final int alignment) {
        this.addText(phrase);
        this.setSimpleColumn(llx, lly, urx, ury, leading, alignment);
    }
    
    public void setSimpleColumn(final float llx, final float lly, final float urx, final float ury, final float leading, final int alignment) {
        this.setLeading(leading);
        this.alignment = alignment;
        this.setSimpleColumn(llx, lly, urx, ury);
    }
    
    public void setSimpleColumn(final float llx, final float lly, final float urx, final float ury) {
        this.leftX = Math.min(llx, urx);
        this.maxY = Math.max(lly, ury);
        this.minY = Math.min(lly, ury);
        this.rightX = Math.max(llx, urx);
        this.yLine = this.maxY;
        this.rectangularWidth = this.rightX - this.leftX;
        if (this.rectangularWidth < 0.0f) {
            this.rectangularWidth = 0.0f;
        }
        this.rectangularMode = true;
    }
    
    public void setSimpleColumn(final Rectangle rect) {
        this.setSimpleColumn(rect.getLeft(), rect.getBottom(), rect.getRight(), rect.getTop());
    }
    
    public void setLeading(final float leading) {
        this.fixedLeading = leading;
        this.multipliedLeading = 0.0f;
    }
    
    public void setLeading(final float fixedLeading, final float multipliedLeading) {
        this.fixedLeading = fixedLeading;
        this.multipliedLeading = multipliedLeading;
    }
    
    public float getLeading() {
        return this.fixedLeading;
    }
    
    public float getMultipliedLeading() {
        return this.multipliedLeading;
    }
    
    public void setYLine(final float yLine) {
        this.yLine = yLine;
    }
    
    public float getYLine() {
        return this.yLine;
    }
    
    public int getRowsDrawn() {
        return this.rowIdx;
    }
    
    public void setAlignment(final int alignment) {
        this.alignment = alignment;
    }
    
    public int getAlignment() {
        return this.alignment;
    }
    
    public void setIndent(final float indent) {
        this.setIndent(indent, true);
    }
    
    public void setIndent(final float indent, final boolean repeatFirstLineIndent) {
        this.indent = indent;
        this.lastWasNewline = true;
        this.repeatFirstLineIndent = repeatFirstLineIndent;
    }
    
    public float getIndent() {
        return this.indent;
    }
    
    public void setFollowingIndent(final float indent) {
        this.followingIndent = indent;
        this.lastWasNewline = true;
    }
    
    public float getFollowingIndent() {
        return this.followingIndent;
    }
    
    public void setRightIndent(final float indent) {
        this.rightIndent = indent;
        this.lastWasNewline = true;
    }
    
    public float getRightIndent() {
        return this.rightIndent;
    }
    
    public float getCurrentLeading() {
        return this.currentLeading;
    }
    
    public boolean getInheritGraphicState() {
        return this.inheritGraphicState;
    }
    
    public void setInheritGraphicState(final boolean inheritGraphicState) {
        this.inheritGraphicState = inheritGraphicState;
    }
    
    public boolean isIgnoreSpacingBefore() {
        return this.ignoreSpacingBefore;
    }
    
    public void setIgnoreSpacingBefore(final boolean ignoreSpacingBefore) {
        this.ignoreSpacingBefore = ignoreSpacingBefore;
    }
    
    public int go() throws DocumentException {
        return this.go(false);
    }
    
    public int go(final boolean simulate) throws DocumentException {
        return this.go(simulate, null);
    }
    
    public int go(final boolean simulate, final IAccessibleElement elementToGo) throws DocumentException {
        this.isWordSplit = false;
        if (this.composite) {
            return this.goComposite(simulate);
        }
        ListBody lBody = null;
        if (isTagged(this.canvas) && elementToGo instanceof ListItem) {
            lBody = ((ListItem)elementToGo).getListBody();
        }
        this.addWaitingPhrase();
        if (this.bidiLine == null) {
            return 1;
        }
        this.descender = 0.0f;
        this.linesWritten = 0;
        this.lastX = 0.0f;
        boolean dirty = false;
        float ratio = this.spaceCharRatio;
        final Object[] currentValues = new Object[2];
        PdfFont currentFont = null;
        final Float lastBaseFactor = new Float(0.0f);
        currentValues[1] = lastBaseFactor;
        PdfDocument pdf = null;
        PdfContentByte graphics = null;
        PdfContentByte text = null;
        this.firstLineY = Float.NaN;
        final int localRunDirection = this.runDirection;
        if (this.canvas != null) {
            graphics = this.canvas;
            pdf = this.canvas.getPdfDocument();
            if (!isTagged(this.canvas)) {
                text = this.canvas.getDuplicate(this.inheritGraphicState);
            }
            else {
                text = this.canvas;
            }
        }
        else if (!simulate) {
            throw new NullPointerException(MessageLocalization.getComposedMessage("columntext.go.with.simulate.eq.eq.false.and.text.eq.eq.null", new Object[0]));
        }
        if (!simulate) {
            if (ratio == 0.0f) {
                ratio = text.getPdfWriter().getSpaceCharRatio();
            }
            else if (ratio < 0.001f) {
                ratio = 0.001f;
            }
        }
        if (!this.rectangularMode) {
            float max = 0.0f;
            for (final PdfChunk c : this.bidiLine.chunks) {
                max = Math.max(max, c.height());
            }
            this.currentLeading = this.fixedLeading + max * this.multipliedLeading;
        }
        float firstIndent = 0.0f;
        int status = 0;
        boolean rtl = false;
        while (true) {
            firstIndent = (this.lastWasNewline ? this.indent : this.followingIndent);
            PdfLine line;
            float x1;
            if (this.rectangularMode) {
                if (this.rectangularWidth <= firstIndent + this.rightIndent) {
                    status = 2;
                    if (this.bidiLine.isEmpty()) {
                        status |= 0x1;
                        break;
                    }
                    break;
                }
                else {
                    if (this.bidiLine.isEmpty()) {
                        status = 1;
                        break;
                    }
                    line = this.bidiLine.processLine(this.leftX, this.rectangularWidth - firstIndent - this.rightIndent, this.alignment, localRunDirection, this.arabicOptions, this.minY, this.yLine, this.descender);
                    this.isWordSplit |= this.bidiLine.isWordSplit();
                    if (line == null) {
                        status = 1;
                        break;
                    }
                    final float[] maxSize = line.getMaxSize(this.fixedLeading, this.multipliedLeading);
                    if (this.isUseAscender() && Float.isNaN(this.firstLineY)) {
                        this.currentLeading = line.getAscender();
                    }
                    else {
                        this.currentLeading = Math.max(maxSize[0], maxSize[1] - this.descender);
                    }
                    if (this.yLine > this.maxY || this.yLine - this.currentLeading < this.minY) {
                        status = 2;
                        this.bidiLine.restore();
                        break;
                    }
                    this.yLine -= this.currentLeading;
                    if (!simulate && !dirty) {
                        if (line.isRTL && this.canvas.isTagged()) {
                            this.canvas.beginMarkedContentSequence(PdfName.REVERSEDCHARS);
                            rtl = true;
                        }
                        text.beginText();
                        dirty = true;
                    }
                    if (Float.isNaN(this.firstLineY)) {
                        this.firstLineY = this.yLine;
                    }
                    this.updateFilledWidth(this.rectangularWidth - line.widthLeft());
                    x1 = this.leftX;
                }
            }
            else {
                final float yTemp = this.yLine - this.currentLeading;
                final float[] xx = this.findLimitsTwoLines();
                if (xx == null) {
                    status = 2;
                    if (this.bidiLine.isEmpty()) {
                        status |= 0x1;
                    }
                    this.yLine = yTemp;
                    break;
                }
                if (this.bidiLine.isEmpty()) {
                    status = 1;
                    this.yLine = yTemp;
                    break;
                }
                x1 = Math.max(xx[0], xx[2]);
                final float x2 = Math.min(xx[1], xx[3]);
                if (x2 - x1 <= firstIndent + this.rightIndent) {
                    continue;
                }
                line = this.bidiLine.processLine(x1, x2 - x1 - firstIndent - this.rightIndent, this.alignment, localRunDirection, this.arabicOptions, this.minY, this.yLine, this.descender);
                if (!simulate && !dirty) {
                    if (line.isRTL && this.canvas.isTagged()) {
                        this.canvas.beginMarkedContentSequence(PdfName.REVERSEDCHARS);
                        rtl = true;
                    }
                    text.beginText();
                    dirty = true;
                }
                if (line == null) {
                    status = 1;
                    this.yLine = yTemp;
                    break;
                }
            }
            if (isTagged(this.canvas) && elementToGo instanceof ListItem && !Float.isNaN(this.firstLineY) && !this.firstLineYDone) {
                if (!simulate) {
                    final ListLabel lbl = ((ListItem)elementToGo).getListLabel();
                    this.canvas.openMCBlock(lbl);
                    final Chunk symbol = new Chunk(((ListItem)elementToGo).getListSymbol());
                    symbol.setRole(null);
                    showTextAligned(this.canvas, 0, new Phrase(symbol), this.leftX + lbl.getIndentation(), this.firstLineY, 0.0f);
                    this.canvas.closeMCBlock(lbl);
                }
                this.firstLineYDone = true;
            }
            if (!simulate) {
                if (lBody != null) {
                    this.canvas.openMCBlock(lBody);
                    lBody = null;
                }
                currentValues[0] = currentFont;
                text.setTextMatrix(x1 + (line.isRTL() ? this.rightIndent : firstIndent) + line.indentLeft(), this.yLine);
                this.lastX = pdf.writeLineToContent(line, text, graphics, currentValues, ratio);
                currentFont = (PdfFont)currentValues[0];
            }
            this.lastWasNewline = (this.repeatFirstLineIndent && line.isNewlineSplit());
            this.yLine -= (line.isNewlineSplit() ? this.extraParagraphSpace : 0.0f);
            ++this.linesWritten;
            this.descender = line.getDescender();
        }
        if (dirty) {
            text.endText();
            if (this.canvas != text) {
                this.canvas.add(text);
            }
            if (rtl && this.canvas.isTagged()) {
                this.canvas.endMarkedContentSequence();
            }
        }
        return status;
    }
    
    public boolean isWordSplit() {
        return this.isWordSplit;
    }
    
    public float getExtraParagraphSpace() {
        return this.extraParagraphSpace;
    }
    
    public void setExtraParagraphSpace(final float extraParagraphSpace) {
        this.extraParagraphSpace = extraParagraphSpace;
    }
    
    public void clearChunks() {
        if (this.bidiLine != null) {
            this.bidiLine.clearChunks();
        }
    }
    
    public float getSpaceCharRatio() {
        return this.spaceCharRatio;
    }
    
    public void setSpaceCharRatio(final float spaceCharRatio) {
        this.spaceCharRatio = spaceCharRatio;
    }
    
    public void setRunDirection(final int runDirection) {
        if (runDirection < 0 || runDirection > 3) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("invalid.run.direction.1", runDirection));
        }
        this.runDirection = runDirection;
    }
    
    public int getRunDirection() {
        return this.runDirection;
    }
    
    public int getLinesWritten() {
        return this.linesWritten;
    }
    
    public float getLastX() {
        return this.lastX;
    }
    
    public int getArabicOptions() {
        return this.arabicOptions;
    }
    
    public void setArabicOptions(final int arabicOptions) {
        this.arabicOptions = arabicOptions;
    }
    
    public float getDescender() {
        return this.descender;
    }
    
    public static float getWidth(final Phrase phrase, final int runDirection, final int arabicOptions) {
        final ColumnText ct = new ColumnText(null);
        ct.addText(phrase);
        ct.addWaitingPhrase();
        final PdfLine line = ct.bidiLine.processLine(0.0f, 20000.0f, 0, runDirection, arabicOptions, 0.0f, 0.0f, 0.0f);
        if (line == null) {
            return 0.0f;
        }
        return 20000.0f - line.widthLeft();
    }
    
    public static float getWidth(final Phrase phrase) {
        return getWidth(phrase, 1, 0);
    }
    
    public static void showTextAligned(final PdfContentByte canvas, int alignment, final Phrase phrase, final float x, final float y, final float rotation, final int runDirection, final int arabicOptions) {
        if (alignment != 0 && alignment != 1 && alignment != 2) {
            alignment = 0;
        }
        canvas.saveState();
        final ColumnText ct = new ColumnText(canvas);
        float lly = -1.0f;
        float ury = 2.0f;
        float llx = 0.0f;
        float urx = 0.0f;
        switch (alignment) {
            case 0: {
                llx = 0.0f;
                urx = 20000.0f;
                break;
            }
            case 2: {
                llx = -20000.0f;
                urx = 0.0f;
                break;
            }
            default: {
                llx = -20000.0f;
                urx = 20000.0f;
                break;
            }
        }
        if (rotation == 0.0f) {
            llx += x;
            lly += y;
            urx += x;
            ury += y;
        }
        else {
            final double alpha = rotation * 3.141592653589793 / 180.0;
            final float cos = (float)Math.cos(alpha);
            final float sin = (float)Math.sin(alpha);
            canvas.concatCTM(cos, sin, -sin, cos, x, y);
        }
        ct.setSimpleColumn(phrase, llx, lly, urx, ury, 2.0f, alignment);
        if (runDirection == 3) {
            if (alignment == 0) {
                alignment = 2;
            }
            else if (alignment == 2) {
                alignment = 0;
            }
        }
        ct.setAlignment(alignment);
        ct.setArabicOptions(arabicOptions);
        ct.setRunDirection(runDirection);
        try {
            ct.go();
        }
        catch (DocumentException e) {
            throw new ExceptionConverter(e);
        }
        canvas.restoreState();
    }
    
    public static void showTextAligned(final PdfContentByte canvas, final int alignment, final Phrase phrase, final float x, final float y, final float rotation) {
        showTextAligned(canvas, alignment, phrase, x, y, rotation, 1, 0);
    }
    
    public static float fitText(final Font font, final String text, final Rectangle rect, float maxFontSize, final int runDirection) {
        try {
            ColumnText ct = null;
            int status = 0;
            if (maxFontSize <= 0.0f) {
                int cr = 0;
                int lf = 0;
                final char[] t = text.toCharArray();
                for (int k = 0; k < t.length; ++k) {
                    if (t[k] == '\n') {
                        ++lf;
                    }
                    else if (t[k] == '\r') {
                        ++cr;
                    }
                }
                final int minLines = Math.max(cr, lf) + 1;
                maxFontSize = Math.abs(rect.getHeight()) / minLines - 0.001f;
            }
            font.setSize(maxFontSize);
            final Phrase ph = new Phrase(text, font);
            ct = new ColumnText(null);
            ct.setSimpleColumn(ph, rect.getLeft(), rect.getBottom(), rect.getRight(), rect.getTop(), maxFontSize, 0);
            ct.setRunDirection(runDirection);
            status = ct.go(true);
            if ((status & 0x1) != 0x0) {
                return maxFontSize;
            }
            final float precision = 0.1f;
            float min = 0.0f;
            float max = maxFontSize;
            float size = maxFontSize;
            for (int i = 0; i < 50; ++i) {
                size = (min + max) / 2.0f;
                ct = new ColumnText(null);
                font.setSize(size);
                ct.setSimpleColumn(new Phrase(text, font), rect.getLeft(), rect.getBottom(), rect.getRight(), rect.getTop(), size, 0);
                ct.setRunDirection(runDirection);
                status = ct.go(true);
                if ((status & 0x1) != 0x0) {
                    if (max - min < size * precision) {
                        return size;
                    }
                    min = size;
                }
                else {
                    max = size;
                }
            }
            return size;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    protected int goComposite(final boolean simulate) throws DocumentException {
        PdfDocument pdf = null;
        if (this.canvas != null) {
            pdf = this.canvas.pdf;
        }
        if (!this.rectangularMode) {
            throw new DocumentException(MessageLocalization.getComposedMessage("irregular.columns.are.not.supported.in.composite.mode", new Object[0]));
        }
        this.linesWritten = 0;
        this.descender = 0.0f;
        boolean firstPass = true;
        boolean isRTL = this.runDirection == 3;
        while (!this.compositeElements.isEmpty()) {
            Element element = this.compositeElements.getFirst();
            if (element.type() == 12) {
                final Paragraph para = (Paragraph)element;
                int status = 0;
                for (int keep = 0; keep < 2; ++keep) {
                    final float lastY = this.yLine;
                    boolean createHere = false;
                    if (this.compositeColumn == null) {
                        (this.compositeColumn = new ColumnText(this.canvas)).setAlignment(para.getAlignment());
                        this.compositeColumn.setIndent(para.getIndentationLeft() + para.getFirstLineIndent(), false);
                        this.compositeColumn.setExtraParagraphSpace(para.getExtraParagraphSpace());
                        this.compositeColumn.setFollowingIndent(para.getIndentationLeft());
                        this.compositeColumn.setRightIndent(para.getIndentationRight());
                        this.compositeColumn.setLeading(para.getLeading(), para.getMultipliedLeading());
                        this.compositeColumn.setRunDirection(this.runDirection);
                        this.compositeColumn.setArabicOptions(this.arabicOptions);
                        this.compositeColumn.setSpaceCharRatio(this.spaceCharRatio);
                        this.compositeColumn.addText(para);
                        if (!firstPass || !this.adjustFirstLine) {
                            this.yLine -= para.getSpacingBefore();
                        }
                        createHere = true;
                    }
                    this.compositeColumn.setUseAscender((firstPass || this.descender == 0.0f) && this.adjustFirstLine && this.useAscender);
                    this.compositeColumn.setInheritGraphicState(this.inheritGraphicState);
                    this.compositeColumn.leftX = this.leftX;
                    this.compositeColumn.rightX = this.rightX;
                    this.compositeColumn.yLine = this.yLine;
                    this.compositeColumn.rectangularWidth = this.rectangularWidth;
                    this.compositeColumn.rectangularMode = this.rectangularMode;
                    this.compositeColumn.minY = this.minY;
                    this.compositeColumn.maxY = this.maxY;
                    final boolean keepCandidate = para.getKeepTogether() && createHere && (!firstPass || !this.adjustFirstLine);
                    final boolean s = simulate || (keepCandidate && keep == 0);
                    if (isTagged(this.canvas) && !s) {
                        this.canvas.openMCBlock(para);
                    }
                    status = this.compositeColumn.go(s);
                    if (isTagged(this.canvas) && !s) {
                        this.canvas.closeMCBlock(para);
                    }
                    this.lastX = this.compositeColumn.getLastX();
                    this.updateFilledWidth(this.compositeColumn.filledWidth);
                    if ((status & 0x1) == 0x0 && keepCandidate) {
                        this.compositeColumn = null;
                        this.yLine = lastY;
                        return 2;
                    }
                    if (simulate) {
                        break;
                    }
                    if (!keepCandidate) {
                        break;
                    }
                    if (keep == 0) {
                        this.compositeColumn = null;
                        this.yLine = lastY;
                    }
                }
                firstPass = false;
                if (this.compositeColumn.getLinesWritten() > 0) {
                    this.yLine = this.compositeColumn.yLine;
                    this.linesWritten += this.compositeColumn.linesWritten;
                    this.descender = this.compositeColumn.descender;
                    this.isWordSplit |= this.compositeColumn.isWordSplit();
                }
                this.currentLeading = this.compositeColumn.currentLeading;
                if ((status & 0x1) != 0x0) {
                    this.compositeColumn = null;
                    this.compositeElements.removeFirst();
                    this.yLine -= para.getSpacingAfter();
                }
                if ((status & 0x2) != 0x0) {
                    return 2;
                }
                continue;
            }
            else if (element.type() == 14) {
                List list = (List)element;
                ArrayList<Element> items = list.getItems();
                ListItem item = null;
                float listIndentation = list.getIndentationLeft();
                int count = 0;
                final Stack<Object[]> stack = new Stack<Object[]>();
                for (int k = 0; k < items.size(); ++k) {
                    final Object obj = items.get(k);
                    if (obj instanceof ListItem) {
                        if (count == this.listIdx) {
                            item = (ListItem)obj;
                            break;
                        }
                        ++count;
                    }
                    else if (obj instanceof List) {
                        stack.push(new Object[] { list, k, new Float(listIndentation) });
                        list = (List)obj;
                        items = list.getItems();
                        listIndentation += list.getIndentationLeft();
                        k = -1;
                        continue;
                    }
                    while (k == items.size() - 1 && !stack.isEmpty()) {
                        final Object[] objs = stack.pop();
                        list = (List)objs[0];
                        items = list.getItems();
                        k = (int)objs[1];
                        listIndentation = (float)objs[2];
                    }
                }
                int status2 = 0;
                boolean keepTogetherAndDontFit = false;
                for (int keep2 = 0; keep2 < 2; ++keep2) {
                    final float lastY2 = this.yLine;
                    boolean createHere2 = false;
                    if (this.compositeColumn == null) {
                        if (item == null) {
                            this.listIdx = 0;
                            this.compositeElements.removeFirst();
                            break;
                        }
                        (this.compositeColumn = new ColumnText(this.canvas)).setUseAscender((firstPass || this.descender == 0.0f) && this.adjustFirstLine && this.useAscender);
                        this.compositeColumn.setInheritGraphicState(this.inheritGraphicState);
                        this.compositeColumn.setAlignment(item.getAlignment());
                        this.compositeColumn.setIndent(item.getIndentationLeft() + listIndentation + item.getFirstLineIndent(), false);
                        this.compositeColumn.setExtraParagraphSpace(item.getExtraParagraphSpace());
                        this.compositeColumn.setFollowingIndent(this.compositeColumn.getIndent());
                        this.compositeColumn.setRightIndent(item.getIndentationRight() + list.getIndentationRight());
                        this.compositeColumn.setLeading(item.getLeading(), item.getMultipliedLeading());
                        this.compositeColumn.setRunDirection(this.runDirection);
                        this.compositeColumn.setArabicOptions(this.arabicOptions);
                        this.compositeColumn.setSpaceCharRatio(this.spaceCharRatio);
                        this.compositeColumn.addText(item);
                        if (!firstPass || !this.adjustFirstLine) {
                            this.yLine -= item.getSpacingBefore();
                        }
                        createHere2 = true;
                    }
                    this.compositeColumn.leftX = this.leftX;
                    this.compositeColumn.rightX = this.rightX;
                    this.compositeColumn.yLine = this.yLine;
                    this.compositeColumn.rectangularWidth = this.rectangularWidth;
                    this.compositeColumn.rectangularMode = this.rectangularMode;
                    this.compositeColumn.minY = this.minY;
                    this.compositeColumn.maxY = this.maxY;
                    final boolean keepCandidate2 = item.getKeepTogether() && createHere2 && (!firstPass || !this.adjustFirstLine);
                    final boolean s2 = simulate || (keepCandidate2 && keep2 == 0);
                    if (isTagged(this.canvas) && !s2) {
                        item.getListLabel().setIndentation(listIndentation);
                        if (list.getFirstItem() == item || (this.compositeColumn != null && this.compositeColumn.bidiLine != null)) {
                            this.canvas.openMCBlock(list);
                        }
                        this.canvas.openMCBlock(item);
                    }
                    status2 = this.compositeColumn.go(s2, item);
                    if (isTagged(this.canvas) && !s2) {
                        this.canvas.closeMCBlock(item.getListBody());
                        this.canvas.closeMCBlock(item);
                    }
                    this.lastX = this.compositeColumn.getLastX();
                    this.updateFilledWidth(this.compositeColumn.filledWidth);
                    if ((status2 & 0x1) == 0x0 && keepCandidate2) {
                        keepTogetherAndDontFit = true;
                        this.compositeColumn = null;
                        this.yLine = lastY2;
                    }
                    if (simulate || !keepCandidate2) {
                        break;
                    }
                    if (keepTogetherAndDontFit) {
                        break;
                    }
                    if (keep2 == 0) {
                        this.compositeColumn = null;
                        this.yLine = lastY2;
                    }
                }
                if (isTagged(this.canvas) && !simulate && (item == null || (list.getLastItem() == item && (status2 & 0x1) != 0x0) || (status2 & 0x2) != 0x0)) {
                    this.canvas.closeMCBlock(list);
                }
                if (keepTogetherAndDontFit) {
                    return 2;
                }
                if (item == null) {
                    continue;
                }
                firstPass = false;
                this.yLine = this.compositeColumn.yLine;
                this.linesWritten += this.compositeColumn.linesWritten;
                this.descender = this.compositeColumn.descender;
                this.currentLeading = this.compositeColumn.currentLeading;
                if (!isTagged(this.canvas) && !Float.isNaN(this.compositeColumn.firstLineY) && !this.compositeColumn.firstLineYDone) {
                    if (!simulate) {
                        if (isRTL) {
                            showTextAligned(this.canvas, 2, new Phrase(item.getListSymbol()), this.compositeColumn.lastX + item.getIndentationLeft(), this.compositeColumn.firstLineY, 0.0f, this.runDirection, this.arabicOptions);
                        }
                        else {
                            showTextAligned(this.canvas, 0, new Phrase(item.getListSymbol()), this.compositeColumn.leftX + listIndentation, this.compositeColumn.firstLineY, 0.0f);
                        }
                    }
                    this.compositeColumn.firstLineYDone = true;
                }
                if ((status2 & 0x1) != 0x0) {
                    this.compositeColumn = null;
                    ++this.listIdx;
                    this.yLine -= item.getSpacingAfter();
                }
                if ((status2 & 0x2) != 0x0) {
                    return 2;
                }
                continue;
            }
            else if (element.type() == 23) {
                PdfPTable table = (PdfPTable)element;
                final int backedUpRunDir = this.runDirection;
                this.runDirection = table.getRunDirection();
                isRTL = (this.runDirection == 3);
                if (table.size() <= table.getHeaderRows()) {
                    this.compositeElements.removeFirst();
                }
                else {
                    float yTemp = this.yLine;
                    yTemp += this.descender;
                    if (this.rowIdx == 0 && this.adjustFirstLine) {
                        yTemp -= table.spacingBefore();
                    }
                    if (yTemp < this.minY || yTemp > this.maxY) {
                        return 2;
                    }
                    final float yLineWrite = yTemp;
                    float x1 = this.leftX;
                    this.currentLeading = 0.0f;
                    float tableWidth;
                    if (table.isLockedWidth()) {
                        tableWidth = table.getTotalWidth();
                        this.updateFilledWidth(tableWidth);
                    }
                    else {
                        tableWidth = this.rectangularWidth * table.getWidthPercentage() / 100.0f;
                        table.setTotalWidth(tableWidth);
                    }
                    table.normalizeHeadersFooters();
                    final int headerRows = table.getHeaderRows();
                    int footerRows = table.getFooterRows();
                    final int realHeaderRows = headerRows - footerRows;
                    final float footerHeight = table.getFooterHeight();
                    final float headerHeight = table.getHeaderHeight() - footerHeight;
                    final boolean skipHeader = table.isSkipFirstHeader() && this.rowIdx <= realHeaderRows && (table.isComplete() || this.rowIdx != realHeaderRows);
                    if (!skipHeader) {
                        yTemp -= headerHeight;
                    }
                    int i = 0;
                    if (this.rowIdx < headerRows) {
                        this.rowIdx = headerRows;
                    }
                    PdfPTable.FittingRows fittingRows = null;
                    if (table.isSkipLastFooter()) {
                        fittingRows = table.getFittingRows(yTemp - this.minY, this.rowIdx);
                    }
                    if (!table.isSkipLastFooter() || fittingRows.lastRow < table.size() - 1) {
                        yTemp -= footerHeight;
                        fittingRows = table.getFittingRows(yTemp - this.minY, this.rowIdx);
                    }
                    if (yTemp < this.minY || yTemp > this.maxY) {
                        return 2;
                    }
                    i = fittingRows.lastRow + 1;
                    yTemp -= fittingRows.height;
                    this.LOGGER.info("Want to split at row " + i);
                    int kTemp;
                    for (kTemp = i; kTemp > this.rowIdx && kTemp < table.size() && table.getRow(kTemp).isMayNotBreak(); --kTemp) {}
                    if (kTemp < table.size() - 1 && !table.getRow(kTemp).isMayNotBreak()) {
                        ++kTemp;
                    }
                    if ((kTemp > this.rowIdx && kTemp < i) || (kTemp == headerRows && table.getRow(headerRows).isMayNotBreak() && table.isLoopCheck())) {
                        yTemp = this.minY;
                        i = kTemp;
                        table.setLoopCheck(false);
                    }
                    this.LOGGER.info("Will split at row " + i);
                    if (table.isSplitLate() && i > 0) {
                        fittingRows.correctLastRowChosen(table, i - 1);
                    }
                    if (!table.isComplete()) {
                        yTemp += footerHeight;
                    }
                    if (!table.isSplitRows()) {
                        if (this.splittedRow != -1) {
                            this.splittedRow = -1;
                        }
                        else if (i == this.rowIdx) {
                            if (i == table.size()) {
                                this.compositeElements.removeFirst();
                                continue;
                            }
                            if (table.isComplete() || i != 1) {
                                table.getRows().remove(i);
                            }
                            return 2;
                        }
                    }
                    else if (table.isSplitLate() && (this.rowIdx < i || (this.splittedRow == -2 && (table.getHeaderRows() == 0 || table.isSkipFirstHeader())))) {
                        this.splittedRow = -1;
                    }
                    else if (i < table.size()) {
                        yTemp -= fittingRows.completedRowsHeight - fittingRows.height;
                        final float h = yTemp - this.minY;
                        final PdfPRow newRow = table.getRow(i).splitRow(table, i, h);
                        if (newRow == null) {
                            this.LOGGER.info("Didn't split row!");
                            this.splittedRow = -1;
                            if (this.rowIdx == i) {
                                return 2;
                            }
                        }
                        else {
                            if (i != this.splittedRow) {
                                this.splittedRow = i + 1;
                                table = new PdfPTable(table);
                                this.compositeElements.set(0, table);
                                final ArrayList<PdfPRow> rows = table.getRows();
                                for (int j = headerRows; j < this.rowIdx; ++j) {
                                    rows.set(j, null);
                                }
                            }
                            yTemp = this.minY;
                            table.getRows().add(++i, newRow);
                            this.LOGGER.info("Inserting row at position " + i);
                        }
                    }
                    firstPass = false;
                    if (!simulate) {
                        switch (table.getHorizontalAlignment()) {
                            case 2: {
                                if (!isRTL) {
                                    x1 += this.rectangularWidth - tableWidth;
                                    break;
                                }
                                break;
                            }
                            case 1: {
                                x1 += (this.rectangularWidth - tableWidth) / 2.0f;
                                break;
                            }
                            default: {
                                if (isRTL) {
                                    x1 += this.rectangularWidth - tableWidth;
                                    break;
                                }
                                break;
                            }
                        }
                        final PdfPTable nt = PdfPTable.shallowCopy(table);
                        final ArrayList<PdfPRow> sub = nt.getRows();
                        if (!skipHeader && realHeaderRows > 0) {
                            final ArrayList<PdfPRow> rows = table.getRows(0, realHeaderRows);
                            if (isTagged(this.canvas)) {
                                nt.getHeader().rows = rows;
                            }
                            sub.addAll(rows);
                        }
                        else {
                            nt.setHeaderRows(footerRows);
                        }
                        final ArrayList<PdfPRow> rows = table.getRows(this.rowIdx, i);
                        if (isTagged(this.canvas)) {
                            nt.getBody().rows = rows;
                        }
                        sub.addAll(rows);
                        boolean showFooter = !table.isSkipLastFooter();
                        boolean newPageFollows = false;
                        if (i < table.size()) {
                            nt.setComplete(true);
                            showFooter = true;
                            newPageFollows = true;
                        }
                        if (footerRows > 0 && nt.isComplete() && showFooter) {
                            final ArrayList<PdfPRow> rows2 = table.getRows(realHeaderRows, realHeaderRows + footerRows);
                            if (isTagged(this.canvas)) {
                                nt.getFooter().rows = rows2;
                            }
                            sub.addAll(rows2);
                        }
                        else {
                            footerRows = 0;
                        }
                        if (sub.size() - footerRows > 0) {
                            float rowHeight = 0.0f;
                            final int lastIdx = sub.size() - 1 - footerRows;
                            final PdfPRow last = sub.get(lastIdx);
                            if (table.isExtendLastRow(newPageFollows)) {
                                rowHeight = last.getMaxHeights();
                                last.setMaxHeights(yTemp - this.minY + rowHeight);
                                yTemp = this.minY;
                            }
                            if (newPageFollows) {
                                final PdfPTableEvent tableEvent = table.getTableEvent();
                                if (tableEvent instanceof PdfPTableEventSplit) {
                                    ((PdfPTableEventSplit)tableEvent).splitTable(table);
                                }
                            }
                            if (this.canvases != null) {
                                if (isTagged(this.canvases[3])) {
                                    this.canvases[3].openMCBlock(table);
                                }
                                nt.writeSelectedRows(0, -1, 0, -1, x1, yLineWrite, this.canvases, false);
                                if (isTagged(this.canvases[3])) {
                                    this.canvases[3].closeMCBlock(table);
                                }
                            }
                            else {
                                if (isTagged(this.canvas)) {
                                    this.canvas.openMCBlock(table);
                                }
                                nt.writeSelectedRows(0, -1, 0, -1, x1, yLineWrite, this.canvas, false);
                                if (isTagged(this.canvas)) {
                                    this.canvas.closeMCBlock(table);
                                }
                            }
                            if (!table.isComplete()) {
                                table.addNumberOfRowsWritten(i);
                            }
                            if (this.splittedRow == i && i < table.size()) {
                                final PdfPRow splitted = table.getRows().get(i);
                                splitted.copyRowContent(nt, lastIdx);
                            }
                            else if (i > 0 && i < table.size()) {
                                final PdfPRow row = table.getRow(i);
                                row.splitRowspans(table, i - 1, nt, lastIdx);
                            }
                            if (table.isExtendLastRow(newPageFollows)) {
                                last.setMaxHeights(rowHeight);
                            }
                            if (newPageFollows) {
                                final PdfPTableEvent tableEvent = table.getTableEvent();
                                if (tableEvent instanceof PdfPTableEventAfterSplit) {
                                    final PdfPRow row2 = table.getRow(i);
                                    ((PdfPTableEventAfterSplit)tableEvent).afterSplitTable(table, row2, i);
                                }
                            }
                        }
                    }
                    else if (table.isExtendLastRow() && this.minY > -1.07374182E9f) {
                        yTemp = this.minY;
                    }
                    this.yLine = yTemp;
                    this.descender = 0.0f;
                    this.currentLeading = 0.0f;
                    if (!skipHeader && !table.isComplete()) {
                        this.yLine += footerHeight;
                    }
                    while (i < table.size() && table.getRowHeight(i) <= 0.0f && !table.hasRowspan(i)) {
                        ++i;
                    }
                    if (i < table.size()) {
                        if (this.splittedRow > -1) {
                            final ArrayList<PdfPRow> rows3 = table.getRows();
                            for (int l = this.rowIdx; l < i; ++l) {
                                rows3.set(l, null);
                            }
                        }
                        this.rowIdx = i;
                        return 2;
                    }
                    if (this.yLine - table.spacingAfter() < this.minY) {
                        this.yLine = this.minY;
                    }
                    else {
                        this.yLine -= table.spacingAfter();
                    }
                    this.compositeElements.removeFirst();
                    this.splittedRow = -1;
                    this.rowIdx = 0;
                    this.runDirection = backedUpRunDir;
                    isRTL = (this.runDirection == 3);
                }
            }
            else if (element.type() == 55) {
                if (!simulate) {
                    final DrawInterface zh = (DrawInterface)element;
                    zh.draw(this.canvas, this.leftX, this.minY, this.rightX, this.maxY, this.yLine);
                }
                this.compositeElements.removeFirst();
            }
            else if (element.type() == 37) {
                final ArrayList<Element> floatingElements = new ArrayList<Element>();
                do {
                    floatingElements.add(element);
                    this.compositeElements.removeFirst();
                    element = (this.compositeElements.isEmpty() ? null : this.compositeElements.getFirst());
                } while (element != null && element.type() == 37);
                final FloatLayout fl = new FloatLayout(floatingElements, this.useAscender);
                fl.setSimpleColumn(this.leftX, this.minY, this.rightX, this.yLine);
                fl.compositeColumn.setIgnoreSpacingBefore(this.isIgnoreSpacingBefore());
                final int status3 = fl.layout(this.canvas, simulate);
                this.yLine = fl.getYLine();
                this.descender = 0.0f;
                if ((status3 & 0x1) == 0x0) {
                    this.compositeElements.addAll(floatingElements);
                    return status3;
                }
                continue;
            }
            else {
                this.compositeElements.removeFirst();
            }
        }
        return 1;
    }
    
    public PdfContentByte getCanvas() {
        return this.canvas;
    }
    
    public void setCanvas(final PdfContentByte canvas) {
        this.canvas = canvas;
        this.canvases = null;
        if (this.compositeColumn != null) {
            this.compositeColumn.setCanvas(canvas);
        }
    }
    
    public void setCanvases(final PdfContentByte[] canvases) {
        this.canvases = canvases;
        this.canvas = canvases[3];
        if (this.compositeColumn != null) {
            this.compositeColumn.setCanvases(canvases);
        }
    }
    
    public PdfContentByte[] getCanvases() {
        return this.canvases;
    }
    
    public boolean zeroHeightElement() {
        return this.composite && !this.compositeElements.isEmpty() && this.compositeElements.getFirst().type() == 55;
    }
    
    public java.util.List<Element> getCompositeElements() {
        return this.compositeElements;
    }
    
    public boolean isUseAscender() {
        return this.useAscender;
    }
    
    public void setUseAscender(final boolean useAscender) {
        this.useAscender = useAscender;
    }
    
    public static boolean hasMoreText(final int status) {
        return (status & 0x1) == 0x0;
    }
    
    public float getFilledWidth() {
        return this.filledWidth;
    }
    
    public void setFilledWidth(final float filledWidth) {
        this.filledWidth = filledWidth;
    }
    
    public void updateFilledWidth(final float w) {
        if (w > this.filledWidth) {
            this.filledWidth = w;
        }
    }
    
    public boolean isAdjustFirstLine() {
        return this.adjustFirstLine;
    }
    
    public void setAdjustFirstLine(final boolean adjustFirstLine) {
        this.adjustFirstLine = adjustFirstLine;
    }
    
    private static boolean isTagged(final PdfContentByte canvas) {
        return canvas != null && canvas.pdf != null && canvas.writer != null && canvas.writer.isTagged();
    }
}
