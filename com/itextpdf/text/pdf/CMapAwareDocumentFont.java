// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.log.LoggerFactory;
import com.itextpdf.text.Utilities;
import com.itextpdf.text.pdf.fonts.cmaps.CMapSequence;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.util.Iterator;
import com.itextpdf.text.pdf.fonts.cmaps.IdentityToUnicode;
import java.io.IOException;
import com.itextpdf.text.pdf.fonts.cmaps.CidLocation;
import com.itextpdf.text.pdf.fonts.cmaps.AbstractCMap;
import com.itextpdf.text.pdf.fonts.cmaps.CMapParserEx;
import com.itextpdf.text.pdf.fonts.cmaps.CidLocationFromByte;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.pdf.fonts.cmaps.CMapCache;
import java.util.Map;
import com.itextpdf.text.pdf.fonts.cmaps.CMapCidUni;
import com.itextpdf.text.pdf.fonts.cmaps.CMapByteCid;
import com.itextpdf.text.pdf.fonts.cmaps.CMapToUnicode;
import com.itextpdf.text.log.Logger;

public class CMapAwareDocumentFont extends DocumentFont
{
    private static final Logger LOGGER;
    private PdfDictionary fontDic;
    private int spaceWidth;
    private CMapToUnicode toUnicodeCmap;
    private CMapByteCid byteCid;
    private CMapCidUni cidUni;
    private char[] cidbyte2uni;
    private Map<Integer, Integer> uni2cid;
    
    public CMapAwareDocumentFont(final PdfDictionary font) {
        super(font);
        this.fontDic = font;
        this.initFont();
    }
    
    public CMapAwareDocumentFont(final PRIndirectReference refFont) {
        super(refFont);
        this.fontDic = (PdfDictionary)PdfReader.getPdfObjectRelease(refFont);
        this.initFont();
    }
    
    private void initFont() {
        this.processToUnicode();
        try {
            this.processUni2Byte();
            this.spaceWidth = super.getWidth(32);
            if (this.spaceWidth == 0) {
                this.spaceWidth = this.computeAverageWidth();
            }
            if (this.cjkEncoding != null) {
                this.byteCid = CMapCache.getCachedCMapByteCid(this.cjkEncoding);
                this.cidUni = CMapCache.getCachedCMapCidUni(this.uniMap);
            }
        }
        catch (Exception ex) {
            throw new ExceptionConverter(ex);
        }
    }
    
    private void processToUnicode() {
        final PdfObject toUni = PdfReader.getPdfObjectRelease(this.fontDic.get(PdfName.TOUNICODE));
        if (toUni instanceof PRStream) {
            try {
                final byte[] touni = PdfReader.getStreamBytes((PRStream)toUni);
                final CidLocationFromByte lb = new CidLocationFromByte(touni);
                CMapParserEx.parseCid("", this.toUnicodeCmap = new CMapToUnicode(), lb);
                this.uni2cid = this.toUnicodeCmap.createReverseMapping();
            }
            catch (IOException e) {
                this.toUnicodeCmap = null;
                this.uni2cid = null;
            }
        }
        else if (this.isType0) {
            try {
                final PdfName encodingName = this.fontDic.getAsName(PdfName.ENCODING);
                if (encodingName == null) {
                    return;
                }
                final String enc = PdfName.decodeName(encodingName.toString());
                if (!enc.equals("Identity-H")) {
                    return;
                }
                final PdfArray df = (PdfArray)PdfReader.getPdfObjectRelease(this.fontDic.get(PdfName.DESCENDANTFONTS));
                final PdfDictionary cidft = (PdfDictionary)PdfReader.getPdfObjectRelease(df.getPdfObject(0));
                final PdfDictionary cidinfo = cidft.getAsDict(PdfName.CIDSYSTEMINFO);
                if (cidinfo == null) {
                    return;
                }
                final PdfString ordering = cidinfo.getAsString(PdfName.ORDERING);
                if (ordering == null) {
                    return;
                }
                final CMapToUnicode touni2 = IdentityToUnicode.GetMapFromOrdering(ordering.toUnicodeString());
                if (touni2 == null) {
                    return;
                }
                this.toUnicodeCmap = touni2;
                this.uni2cid = this.toUnicodeCmap.createReverseMapping();
            }
            catch (IOException e) {
                this.toUnicodeCmap = null;
                this.uni2cid = null;
            }
        }
    }
    
    private void processUni2Byte() throws IOException {
        final IntHashtable byte2uni = this.getByte2Uni();
        int[] e = byte2uni.toOrderedKeys();
        if (e.length == 0) {
            return;
        }
        this.cidbyte2uni = new char[256];
        for (int k = 0; k < e.length; ++k) {
            final int key = e[k];
            if (key <= 255) {
                this.cidbyte2uni[key] = (char)byte2uni.get(key);
            }
            else {
                CMapAwareDocumentFont.LOGGER.warn("Font has illegal differences array.");
            }
        }
        if (this.toUnicodeCmap != null) {
            final Map<Integer, Integer> dm = this.toUnicodeCmap.createDirectMapping();
            for (final Map.Entry<Integer, Integer> kv : dm.entrySet()) {
                if (kv.getKey() < 256) {
                    this.cidbyte2uni[kv.getKey()] = (char)(int)kv.getValue();
                }
            }
        }
        final IntHashtable diffmap = this.getDiffmap();
        if (diffmap != null) {
            e = diffmap.toOrderedKeys();
            for (int i = 0; i < e.length; ++i) {
                final int n = diffmap.get(e[i]);
                if (n < 256) {
                    this.cidbyte2uni[n] = (char)e[i];
                }
            }
        }
    }
    
    private int computeAverageWidth() {
        int count = 0;
        int total = 0;
        for (int i = 0; i < super.widths.length; ++i) {
            if (super.widths[i] != 0) {
                total += super.widths[i];
                ++count;
            }
        }
        return (count != 0) ? (total / count) : 0;
    }
    
    @Override
    public int getWidth(final int char1) {
        if (char1 == 32) {
            return (this.spaceWidth != 0) ? this.spaceWidth : this.defaultWidth;
        }
        return super.getWidth(char1);
    }
    
    private String decodeSingleCID(final byte[] bytes, final int offset, final int len) {
        if (this.toUnicodeCmap != null) {
            if (offset + len > bytes.length) {
                throw new ArrayIndexOutOfBoundsException(MessageLocalization.getComposedMessage("invalid.index.1", offset + len));
            }
            final String s = this.toUnicodeCmap.lookup(bytes, offset, len);
            if (s != null) {
                return s;
            }
            if (len != 1 || this.cidbyte2uni == null) {
                return null;
            }
        }
        if (len != 1) {
            throw new Error("Multi-byte glyphs not implemented yet");
        }
        if (this.cidbyte2uni == null) {
            return "";
        }
        return new String(this.cidbyte2uni, 0xFF & bytes[offset], 1);
    }
    
    public String decode(final byte[] cidbytes, final int offset, final int len) {
        final StringBuilder sb = new StringBuilder();
        if (this.toUnicodeCmap == null && this.byteCid != null) {
            final CMapSequence seq = new CMapSequence(cidbytes, offset, len);
            final String cid = this.byteCid.decodeSequence(seq);
            for (int k = 0; k < cid.length(); ++k) {
                final int c = this.cidUni.lookup(cid.charAt(k));
                if (c > 0) {
                    sb.append(Utilities.convertFromUtf32(c));
                }
            }
        }
        else {
            for (int i = offset; i < offset + len; ++i) {
                String rslt = this.decodeSingleCID(cidbytes, i, 1);
                if (rslt == null && i < offset + len - 1) {
                    rslt = this.decodeSingleCID(cidbytes, i, 2);
                    ++i;
                }
                if (rslt != null) {
                    sb.append(rslt);
                }
            }
        }
        return sb.toString();
    }
    
    @Deprecated
    public String encode(final byte[] bytes, final int offset, final int len) {
        return this.decode(bytes, offset, len);
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(CMapAwareDocumentFont.class);
    }
}
