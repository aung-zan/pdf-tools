// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.io.Reader;
import java.io.StringReader;
import java.util.Iterator;
import java.util.EmptyStackException;
import com.itextpdf.text.ExceptionConverter;
import org.xml.sax.InputSource;
import java.io.FileInputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.io.OutputStream;
import com.itextpdf.text.xml.XmlDomWriter;
import org.w3c.dom.Element;
import java.util.HashMap;
import org.w3c.dom.NodeList;
import java.util.Map;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.xml.sax.EntityResolver;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayOutputStream;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class XfaForm
{
    private Xml2SomTemplate templateSom;
    private Node templateNode;
    private Xml2SomDatasets datasetsSom;
    private Node datasetsNode;
    private AcroFieldsSearch acroFieldsSom;
    private PdfReader reader;
    private boolean xfaPresent;
    private Document domDocument;
    private boolean changed;
    public static final String XFA_DATA_SCHEMA = "http://www.xfa.org/schema/xfa-data/1.0/";
    
    public XfaForm() {
    }
    
    public static PdfObject getXfaObject(final PdfReader reader) {
        final PdfDictionary af = (PdfDictionary)PdfReader.getPdfObjectRelease(reader.getCatalog().get(PdfName.ACROFORM));
        if (af == null) {
            return null;
        }
        return PdfReader.getPdfObjectRelease(af.get(PdfName.XFA));
    }
    
    public XfaForm(final PdfReader reader) throws IOException, ParserConfigurationException, SAXException {
        this.reader = reader;
        final PdfObject xfa = getXfaObject(reader);
        if (xfa == null) {
            this.xfaPresent = false;
            return;
        }
        this.xfaPresent = true;
        final ByteArrayOutputStream bout = new ByteArrayOutputStream();
        if (xfa.isArray()) {
            final PdfArray ar = (PdfArray)xfa;
            for (int k = 1; k < ar.size(); k += 2) {
                final PdfObject ob = ar.getDirectObject(k);
                if (ob instanceof PRStream) {
                    final byte[] b = PdfReader.getStreamBytes((PRStream)ob);
                    bout.write(b);
                }
            }
        }
        else if (xfa instanceof PRStream) {
            final byte[] b2 = PdfReader.getStreamBytes((PRStream)xfa);
            bout.write(b2);
        }
        bout.close();
        final DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
        fact.setNamespaceAware(true);
        final DocumentBuilder db = fact.newDocumentBuilder();
        db.setEntityResolver(new SafeEmptyEntityResolver());
        this.domDocument = db.parse(new ByteArrayInputStream(bout.toByteArray()));
        this.extractNodes();
    }
    
    private void extractNodes() {
        final Map<String, Node> xfaNodes = extractXFANodes(this.domDocument);
        if (xfaNodes.containsKey("template")) {
            this.templateNode = xfaNodes.get("template");
            this.templateSom = new Xml2SomTemplate(this.templateNode);
        }
        if (xfaNodes.containsKey("datasets")) {
            this.datasetsNode = xfaNodes.get("datasets");
            final Node dataNode = this.findDataNode(this.datasetsNode);
            this.datasetsSom = new Xml2SomDatasets((dataNode != null) ? dataNode : this.datasetsNode.getFirstChild());
        }
        if (this.datasetsNode == null) {
            this.createDatasetsNode(this.domDocument.getFirstChild());
        }
    }
    
    private Node findDataNode(final Node datasetsNode) {
        final NodeList childNodes = datasetsNode.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); ++i) {
            if (childNodes.item(i).getNodeName().equals("xfa:data")) {
                return childNodes.item(i);
            }
        }
        return null;
    }
    
    public static Map<String, Node> extractXFANodes(final Document domDocument) {
        final Map<String, Node> xfaNodes = new HashMap<String, Node>();
        Node n;
        for (n = domDocument.getFirstChild(); n.getChildNodes().getLength() == 0; n = n.getNextSibling()) {}
        for (n = n.getFirstChild(); n != null; n = n.getNextSibling()) {
            if (n.getNodeType() == 1) {
                final String s = n.getLocalName();
                xfaNodes.put(s, n);
            }
        }
        return xfaNodes;
    }
    
    private void createDatasetsNode(Node n) {
        while (n.getChildNodes().getLength() == 0) {
            n = n.getNextSibling();
        }
        if (n != null) {
            final Element e = n.getOwnerDocument().createElement("xfa:datasets");
            e.setAttribute("xmlns:xfa", "http://www.xfa.org/schema/xfa-data/1.0/");
            n.appendChild(this.datasetsNode = e);
        }
    }
    
    public static void setXfa(final XfaForm form, final PdfReader reader, final PdfWriter writer) throws IOException {
        final PdfDictionary af = (PdfDictionary)PdfReader.getPdfObjectRelease(reader.getCatalog().get(PdfName.ACROFORM));
        if (af == null) {
            return;
        }
        final PdfObject xfa = getXfaObject(reader);
        if (xfa.isArray()) {
            final PdfArray ar = (PdfArray)xfa;
            int t = -1;
            int d = -1;
            for (int k = 0; k < ar.size(); k += 2) {
                final PdfString s = ar.getAsString(k);
                if ("template".equals(s.toString())) {
                    t = k + 1;
                }
                if ("datasets".equals(s.toString())) {
                    d = k + 1;
                }
            }
            if (t > -1 && d > -1) {
                reader.killXref(ar.getAsIndirectObject(t));
                reader.killXref(ar.getAsIndirectObject(d));
                final PdfStream tStream = new PdfStream(serializeDoc(form.templateNode));
                tStream.flateCompress(writer.getCompressionLevel());
                ar.set(t, writer.addToBody(tStream).getIndirectReference());
                final PdfStream dStream = new PdfStream(serializeDoc(form.datasetsNode));
                dStream.flateCompress(writer.getCompressionLevel());
                ar.set(d, writer.addToBody(dStream).getIndirectReference());
                af.put(PdfName.XFA, new PdfArray(ar));
                return;
            }
        }
        reader.killXref(af.get(PdfName.XFA));
        final PdfStream str = new PdfStream(serializeDoc(form.domDocument));
        str.flateCompress(writer.getCompressionLevel());
        final PdfIndirectReference ref = writer.addToBody(str).getIndirectReference();
        af.put(PdfName.XFA, ref);
    }
    
    public void setXfa(final PdfWriter writer) throws IOException {
        setXfa(this, this.reader, writer);
    }
    
    public static byte[] serializeDoc(final Node n) throws IOException {
        final XmlDomWriter xw = new XmlDomWriter();
        final ByteArrayOutputStream fout = new ByteArrayOutputStream();
        xw.setOutput(fout, null);
        xw.setCanonical(false);
        xw.write(n);
        fout.close();
        return fout.toByteArray();
    }
    
    public boolean isXfaPresent() {
        return this.xfaPresent;
    }
    
    public Document getDomDocument() {
        return this.domDocument;
    }
    
    public String findFieldName(final String name, final AcroFields af) {
        final Map<String, AcroFields.Item> items = af.getFields();
        if (items.containsKey(name)) {
            return name;
        }
        if (this.acroFieldsSom == null) {
            if (items.isEmpty() && this.xfaPresent) {
                this.acroFieldsSom = new AcroFieldsSearch(this.datasetsSom.getName2Node().keySet());
            }
            else {
                this.acroFieldsSom = new AcroFieldsSearch(items.keySet());
            }
        }
        if (this.acroFieldsSom.getAcroShort2LongName().containsKey(name)) {
            return this.acroFieldsSom.getAcroShort2LongName().get(name);
        }
        return this.acroFieldsSom.inverseSearchGlobal(Xml2Som.splitParts(name));
    }
    
    public String findDatasetsName(final String name) {
        if (this.datasetsSom.getName2Node().containsKey(name)) {
            return name;
        }
        return this.datasetsSom.inverseSearchGlobal(Xml2Som.splitParts(name));
    }
    
    public Node findDatasetsNode(String name) {
        if (name == null) {
            return null;
        }
        name = this.findDatasetsName(name);
        if (name == null) {
            return null;
        }
        return this.datasetsSom.getName2Node().get(name);
    }
    
    public static String getNodeText(final Node n) {
        if (n == null) {
            return "";
        }
        return getNodeText(n, "");
    }
    
    private static String getNodeText(final Node n, String name) {
        for (Node n2 = n.getFirstChild(); n2 != null; n2 = n2.getNextSibling()) {
            if (n2.getNodeType() == 1) {
                name = getNodeText(n2, name);
            }
            else if (n2.getNodeType() == 3) {
                name += n2.getNodeValue();
            }
        }
        return name;
    }
    
    public void setNodeText(final Node n, final String text) {
        if (n == null) {
            return;
        }
        Node nc = null;
        while ((nc = n.getFirstChild()) != null) {
            n.removeChild(nc);
        }
        if (n.getAttributes().getNamedItemNS("http://www.xfa.org/schema/xfa-data/1.0/", "dataNode") != null) {
            n.getAttributes().removeNamedItemNS("http://www.xfa.org/schema/xfa-data/1.0/", "dataNode");
        }
        n.appendChild(this.domDocument.createTextNode(text));
        this.changed = true;
    }
    
    public void setXfaPresent(final boolean xfaPresent) {
        this.xfaPresent = xfaPresent;
    }
    
    public void setDomDocument(final Document domDocument) {
        this.domDocument = domDocument;
        this.extractNodes();
    }
    
    public PdfReader getReader() {
        return this.reader;
    }
    
    public void setReader(final PdfReader reader) {
        this.reader = reader;
    }
    
    public boolean isChanged() {
        return this.changed;
    }
    
    public void setChanged(final boolean changed) {
        this.changed = changed;
    }
    
    public Xml2SomTemplate getTemplateSom() {
        return this.templateSom;
    }
    
    public void setTemplateSom(final Xml2SomTemplate templateSom) {
        this.templateSom = templateSom;
    }
    
    public Xml2SomDatasets getDatasetsSom() {
        return this.datasetsSom;
    }
    
    public void setDatasetsSom(final Xml2SomDatasets datasetsSom) {
        this.datasetsSom = datasetsSom;
    }
    
    public AcroFieldsSearch getAcroFieldsSom() {
        return this.acroFieldsSom;
    }
    
    public void setAcroFieldsSom(final AcroFieldsSearch acroFieldsSom) {
        this.acroFieldsSom = acroFieldsSom;
    }
    
    public Node getDatasetsNode() {
        return this.datasetsNode;
    }
    
    public void fillXfaForm(final File file) throws IOException {
        this.fillXfaForm(file, false);
    }
    
    public void fillXfaForm(final File file, final boolean readOnly) throws IOException {
        this.fillXfaForm(new FileInputStream(file), readOnly);
    }
    
    public void fillXfaForm(final InputStream is) throws IOException {
        this.fillXfaForm(is, false);
    }
    
    public void fillXfaForm(final InputStream is, final boolean readOnly) throws IOException {
        this.fillXfaForm(new InputSource(is), readOnly);
    }
    
    public void fillXfaForm(final InputSource is) throws IOException {
        this.fillXfaForm(is, false);
    }
    
    public void fillXfaForm(final InputSource is, final boolean readOnly) throws IOException {
        final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            final DocumentBuilder db = dbf.newDocumentBuilder();
            db.setEntityResolver(new SafeEmptyEntityResolver());
            final Document newdoc = db.parse(is);
            this.fillXfaForm(newdoc.getDocumentElement(), readOnly);
        }
        catch (ParserConfigurationException e) {
            throw new ExceptionConverter(e);
        }
        catch (SAXException e2) {
            throw new ExceptionConverter(e2);
        }
    }
    
    public void fillXfaForm(final Node node) {
        this.fillXfaForm(node, false);
    }
    
    public void fillXfaForm(final Node node, final boolean readOnly) {
        if (readOnly) {
            final NodeList nodeList = this.domDocument.getElementsByTagName("field");
            for (int i = 0; i < nodeList.getLength(); ++i) {
                ((Element)nodeList.item(i)).setAttribute("access", "readOnly");
            }
        }
        final NodeList allChilds = this.datasetsNode.getChildNodes();
        final int len = allChilds.getLength();
        Node data = null;
        for (int k = 0; k < len; ++k) {
            final Node n = allChilds.item(k);
            if (n.getNodeType() == 1 && n.getLocalName().equals("data") && "http://www.xfa.org/schema/xfa-data/1.0/".equals(n.getNamespaceURI())) {
                data = n;
                break;
            }
        }
        if (data == null) {
            data = this.datasetsNode.getOwnerDocument().createElementNS("http://www.xfa.org/schema/xfa-data/1.0/", "xfa:data");
            this.datasetsNode.appendChild(data);
        }
        final NodeList list = data.getChildNodes();
        if (list.getLength() == 0) {
            data.appendChild(this.domDocument.importNode(node, true));
        }
        else {
            final Node firstNode = this.getFirstElementNode(data);
            if (firstNode != null) {
                data.replaceChild(this.domDocument.importNode(node, true), firstNode);
            }
        }
        this.extractNodes();
        this.setChanged(true);
    }
    
    private Node getFirstElementNode(final Node src) {
        Node result = null;
        final NodeList list = src.getChildNodes();
        for (int i = 0; i < list.getLength(); ++i) {
            if (list.item(i).getNodeType() == 1) {
                result = list.item(i);
                break;
            }
        }
        return result;
    }
    
    public static class InverseStore
    {
        protected ArrayList<String> part;
        protected ArrayList<Object> follow;
        
        public InverseStore() {
            this.part = new ArrayList<String>();
            this.follow = new ArrayList<Object>();
        }
        
        public String getDefaultName() {
            InverseStore store = this;
            Object obj;
            while (true) {
                obj = store.follow.get(0);
                if (obj instanceof String) {
                    break;
                }
                store = (InverseStore)obj;
            }
            return (String)obj;
        }
        
        public boolean isSimilar(String name) {
            final int idx = name.indexOf(91);
            name = name.substring(0, idx + 1);
            for (int k = 0; k < this.part.size(); ++k) {
                if (this.part.get(k).startsWith(name)) {
                    return true;
                }
            }
            return false;
        }
    }
    
    public static class Stack2<T> extends ArrayList<T>
    {
        private static final long serialVersionUID = -7451476576174095212L;
        
        public T peek() {
            if (this.size() == 0) {
                throw new EmptyStackException();
            }
            return this.get(this.size() - 1);
        }
        
        public T pop() {
            if (this.size() == 0) {
                throw new EmptyStackException();
            }
            final T ret = this.get(this.size() - 1);
            this.remove(this.size() - 1);
            return ret;
        }
        
        public T push(final T item) {
            this.add(item);
            return item;
        }
        
        public boolean empty() {
            return this.size() == 0;
        }
    }
    
    public static class Xml2Som
    {
        protected ArrayList<String> order;
        protected HashMap<String, Node> name2Node;
        protected HashMap<String, InverseStore> inverseSearch;
        protected Stack2<String> stack;
        protected int anform;
        
        public static String escapeSom(final String s) {
            if (s == null) {
                return "";
            }
            int idx = s.indexOf(46);
            if (idx < 0) {
                return s;
            }
            final StringBuffer sb = new StringBuffer();
            int last = 0;
            while (idx >= 0) {
                sb.append(s.substring(last, idx));
                sb.append('\\');
                last = idx;
                idx = s.indexOf(46, idx + 1);
            }
            sb.append(s.substring(last));
            return sb.toString();
        }
        
        public static String unescapeSom(final String s) {
            int idx = s.indexOf(92);
            if (idx < 0) {
                return s;
            }
            final StringBuffer sb = new StringBuffer();
            int last = 0;
            while (idx >= 0) {
                sb.append(s.substring(last, idx));
                last = idx + 1;
                idx = s.indexOf(92, idx + 1);
            }
            sb.append(s.substring(last));
            return sb.toString();
        }
        
        protected String printStack() {
            if (this.stack.empty()) {
                return "";
            }
            final StringBuffer s = new StringBuffer();
            for (int k = 0; k < this.stack.size(); ++k) {
                s.append('.').append(this.stack.get(k));
            }
            return s.substring(1);
        }
        
        public static String getShortName(final String s) {
            int idx = s.indexOf(".#subform[");
            if (idx < 0) {
                return s;
            }
            int last = 0;
            final StringBuffer sb = new StringBuffer();
            while (idx >= 0) {
                sb.append(s.substring(last, idx));
                idx = s.indexOf("]", idx + 10);
                if (idx < 0) {
                    return sb.toString();
                }
                last = idx + 1;
                idx = s.indexOf(".#subform[", last);
            }
            sb.append(s.substring(last));
            return sb.toString();
        }
        
        public void inverseSearchAdd(final String unstack) {
            inverseSearchAdd(this.inverseSearch, this.stack, unstack);
        }
        
        public static void inverseSearchAdd(final HashMap<String, InverseStore> inverseSearch, final Stack2<String> stack, final String unstack) {
            String last = stack.peek();
            InverseStore store = inverseSearch.get(last);
            if (store == null) {
                store = new InverseStore();
                inverseSearch.put(last, store);
            }
            for (int k = stack.size() - 2; k >= 0; --k) {
                last = stack.get(k);
                final int idx = store.part.indexOf(last);
                InverseStore store2;
                if (idx < 0) {
                    store.part.add(last);
                    store2 = new InverseStore();
                    store.follow.add(store2);
                }
                else {
                    store2 = store.follow.get(idx);
                }
                store = store2;
            }
            store.part.add("");
            store.follow.add(unstack);
        }
        
        public String inverseSearchGlobal(final ArrayList<String> parts) {
            if (parts.isEmpty()) {
                return null;
            }
            InverseStore store = this.inverseSearch.get(parts.get(parts.size() - 1));
            if (store == null) {
                return null;
            }
            int k = parts.size() - 2;
            while (k >= 0) {
                final String part = parts.get(k);
                final int idx = store.part.indexOf(part);
                if (idx < 0) {
                    if (store.isSimilar(part)) {
                        return null;
                    }
                    return store.getDefaultName();
                }
                else {
                    store = store.follow.get(idx);
                    --k;
                }
            }
            return store.getDefaultName();
        }
        
        public static Stack2<String> splitParts(String name) {
            while (name.startsWith(".")) {
                name = name.substring(1);
            }
            final Stack2<String> parts = new Stack2<String>();
            int last = 0;
            int pos = 0;
            while (true) {
                pos = last;
                while (true) {
                    pos = name.indexOf(46, pos);
                    if (pos < 0) {
                        break;
                    }
                    if (name.charAt(pos - 1) != '\\') {
                        break;
                    }
                    ++pos;
                }
                if (pos < 0) {
                    break;
                }
                String part = name.substring(last, pos);
                if (!part.endsWith("]")) {
                    part += "[0]";
                }
                parts.add(part);
                last = pos + 1;
            }
            String part = name.substring(last);
            if (!part.endsWith("]")) {
                part += "[0]";
            }
            parts.add(part);
            return parts;
        }
        
        public ArrayList<String> getOrder() {
            return this.order;
        }
        
        public void setOrder(final ArrayList<String> order) {
            this.order = order;
        }
        
        public HashMap<String, Node> getName2Node() {
            return this.name2Node;
        }
        
        public void setName2Node(final HashMap<String, Node> name2Node) {
            this.name2Node = name2Node;
        }
        
        public HashMap<String, InverseStore> getInverseSearch() {
            return this.inverseSearch;
        }
        
        public void setInverseSearch(final HashMap<String, InverseStore> inverseSearch) {
            this.inverseSearch = inverseSearch;
        }
    }
    
    public static class Xml2SomDatasets extends Xml2Som
    {
        public Xml2SomDatasets(final Node n) {
            this.order = new ArrayList<String>();
            this.name2Node = new HashMap<String, Node>();
            this.stack = new Stack2<String>();
            this.anform = 0;
            this.inverseSearch = new HashMap<String, InverseStore>();
            this.processDatasetsInternal(n);
        }
        
        public Node insertNode(Node n, final String shortName) {
            final Stack2<String> stack = Xml2Som.splitParts(shortName);
            final Document doc = n.getOwnerDocument();
            Node n2 = null;
            for (n = n.getFirstChild(); n.getNodeType() != 1; n = n.getNextSibling()) {}
            for (int k = 0; k < stack.size(); ++k) {
                final String part = stack.get(k);
                int idx = part.lastIndexOf(91);
                final String name = part.substring(0, idx);
                idx = Integer.parseInt(part.substring(idx + 1, part.length() - 1));
                int found = -1;
                for (n2 = n.getFirstChild(); n2 != null; n2 = n2.getNextSibling()) {
                    if (n2.getNodeType() == 1) {
                        final String s = Xml2Som.escapeSom(n2.getLocalName());
                        if (s.equals(name) && ++found == idx) {
                            break;
                        }
                    }
                }
                while (found < idx) {
                    n2 = doc.createElementNS(null, name);
                    n2 = n.appendChild(n2);
                    final Node attr = doc.createAttributeNS("http://www.xfa.org/schema/xfa-data/1.0/", "dataNode");
                    attr.setNodeValue("dataGroup");
                    n2.getAttributes().setNamedItemNS(attr);
                    ++found;
                }
                n = n2;
            }
            Xml2Som.inverseSearchAdd(this.inverseSearch, stack, shortName);
            this.name2Node.put(shortName, n2);
            this.order.add(shortName);
            return n2;
        }
        
        private static boolean hasChildren(final Node n) {
            final Node dataNodeN = n.getAttributes().getNamedItemNS("http://www.xfa.org/schema/xfa-data/1.0/", "dataNode");
            if (dataNodeN != null) {
                final String dataNode = dataNodeN.getNodeValue();
                if ("dataGroup".equals(dataNode)) {
                    return true;
                }
                if ("dataValue".equals(dataNode)) {
                    return false;
                }
            }
            if (!n.hasChildNodes()) {
                return false;
            }
            for (Node n2 = n.getFirstChild(); n2 != null; n2 = n2.getNextSibling()) {
                if (n2.getNodeType() == 1) {
                    return true;
                }
            }
            return false;
        }
        
        private void processDatasetsInternal(final Node n) {
            if (n != null) {
                final HashMap<String, Integer> ss = new HashMap<String, Integer>();
                for (Node n2 = n.getFirstChild(); n2 != null; n2 = n2.getNextSibling()) {
                    if (n2.getNodeType() == 1) {
                        final String s = Xml2Som.escapeSom(n2.getLocalName());
                        Integer i = ss.get(s);
                        if (i == null) {
                            i = 0;
                        }
                        else {
                            ++i;
                        }
                        ss.put(s, i);
                        this.stack.push(s + "[" + i.toString() + "]");
                        if (hasChildren(n2)) {
                            this.processDatasetsInternal(n2);
                        }
                        final String unstack = this.printStack();
                        this.order.add(unstack);
                        this.inverseSearchAdd(unstack);
                        this.name2Node.put(unstack, n2);
                        this.stack.pop();
                    }
                }
            }
        }
    }
    
    public static class AcroFieldsSearch extends Xml2Som
    {
        private HashMap<String, String> acroShort2LongName;
        
        public AcroFieldsSearch(final Collection<String> items) {
            this.inverseSearch = new HashMap<String, InverseStore>();
            this.acroShort2LongName = new HashMap<String, String>();
            for (final String itemName : items) {
                final String string = itemName;
                final String itemShort = Xml2Som.getShortName(itemName);
                this.acroShort2LongName.put(itemShort, itemName);
                Xml2Som.inverseSearchAdd(this.inverseSearch, Xml2Som.splitParts(itemShort), itemName);
            }
        }
        
        public HashMap<String, String> getAcroShort2LongName() {
            return this.acroShort2LongName;
        }
        
        public void setAcroShort2LongName(final HashMap<String, String> acroShort2LongName) {
            this.acroShort2LongName = acroShort2LongName;
        }
    }
    
    public static class Xml2SomTemplate extends Xml2Som
    {
        private boolean dynamicForm;
        private int templateLevel;
        
        public Xml2SomTemplate(final Node n) {
            this.order = new ArrayList<String>();
            this.name2Node = new HashMap<String, Node>();
            this.stack = new Stack2<String>();
            this.anform = 0;
            this.templateLevel = 0;
            this.inverseSearch = new HashMap<String, InverseStore>();
            this.processTemplate(n, null);
        }
        
        public String getFieldType(final String s) {
            final Node n = this.name2Node.get(s);
            if (n == null) {
                return null;
            }
            if ("exclGroup".equals(n.getLocalName())) {
                return "exclGroup";
            }
            Node ui;
            for (ui = n.getFirstChild(); ui != null && (ui.getNodeType() != 1 || !"ui".equals(ui.getLocalName())); ui = ui.getNextSibling()) {}
            if (ui == null) {
                return null;
            }
            for (Node type = ui.getFirstChild(); type != null; type = type.getNextSibling()) {
                if (type.getNodeType() == 1 && (!"extras".equals(type.getLocalName()) || !"picture".equals(type.getLocalName()))) {
                    return type.getLocalName();
                }
            }
            return null;
        }
        
        private void processTemplate(final Node n, HashMap<String, Integer> ff) {
            if (ff == null) {
                ff = new HashMap<String, Integer>();
            }
            final HashMap<String, Integer> ss = new HashMap<String, Integer>();
            for (Node n2 = n.getFirstChild(); n2 != null; n2 = n2.getNextSibling()) {
                if (n2.getNodeType() == 1) {
                    final String s = n2.getLocalName();
                    if ("subform".equals(s)) {
                        final Node name = n2.getAttributes().getNamedItem("name");
                        String nn = "#subform";
                        boolean annon = true;
                        if (name != null) {
                            nn = Xml2Som.escapeSom(name.getNodeValue());
                            annon = false;
                        }
                        Integer i;
                        if (annon) {
                            i = this.anform;
                            ++this.anform;
                        }
                        else {
                            i = ss.get(nn);
                            if (i == null) {
                                i = 0;
                            }
                            else {
                                ++i;
                            }
                            ss.put(nn, i);
                        }
                        this.stack.push(nn + "[" + i.toString() + "]");
                        ++this.templateLevel;
                        if (annon) {
                            this.processTemplate(n2, ff);
                        }
                        else {
                            this.processTemplate(n2, null);
                        }
                        --this.templateLevel;
                        this.stack.pop();
                    }
                    else if ("field".equals(s) || "exclGroup".equals(s)) {
                        final Node name = n2.getAttributes().getNamedItem("name");
                        if (name != null) {
                            final String nn = Xml2Som.escapeSom(name.getNodeValue());
                            Integer j = ff.get(nn);
                            if (j == null) {
                                j = 0;
                            }
                            else {
                                ++j;
                            }
                            ff.put(nn, j);
                            this.stack.push(nn + "[" + j.toString() + "]");
                            final String unstack = this.printStack();
                            this.order.add(unstack);
                            this.inverseSearchAdd(unstack);
                            this.name2Node.put(unstack, n2);
                            this.stack.pop();
                        }
                    }
                    else if (!this.dynamicForm && this.templateLevel > 0 && "occur".equals(s)) {
                        int initial = 1;
                        int min = 1;
                        int max = 1;
                        Node a = n2.getAttributes().getNamedItem("initial");
                        if (a != null) {
                            try {
                                initial = Integer.parseInt(a.getNodeValue().trim());
                            }
                            catch (Exception ex) {}
                        }
                        a = n2.getAttributes().getNamedItem("min");
                        if (a != null) {
                            try {
                                min = Integer.parseInt(a.getNodeValue().trim());
                            }
                            catch (Exception ex2) {}
                        }
                        a = n2.getAttributes().getNamedItem("max");
                        if (a != null) {
                            try {
                                max = Integer.parseInt(a.getNodeValue().trim());
                            }
                            catch (Exception ex3) {}
                        }
                        if (initial != min || min != max) {
                            this.dynamicForm = true;
                        }
                    }
                }
            }
        }
        
        public boolean isDynamicForm() {
            return this.dynamicForm;
        }
        
        public void setDynamicForm(final boolean dynamicForm) {
            this.dynamicForm = dynamicForm;
        }
    }
    
    private static class SafeEmptyEntityResolver implements EntityResolver
    {
        @Override
        public InputSource resolveEntity(final String publicId, final String systemId) throws SAXException, IOException {
            return new InputSource(new StringReader(""));
        }
    }
}
