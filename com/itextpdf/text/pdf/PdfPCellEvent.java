// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.Rectangle;

public interface PdfPCellEvent
{
    void cellLayout(final PdfPCell p0, final Rectangle p1, final PdfContentByte[] p2);
}
