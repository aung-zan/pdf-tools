// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.io.IOException;
import java.io.OutputStream;

public class PdfGState extends PdfDictionary
{
    public static final PdfName BM_NORMAL;
    public static final PdfName BM_COMPATIBLE;
    public static final PdfName BM_MULTIPLY;
    public static final PdfName BM_SCREEN;
    public static final PdfName BM_OVERLAY;
    public static final PdfName BM_DARKEN;
    public static final PdfName BM_LIGHTEN;
    public static final PdfName BM_COLORDODGE;
    public static final PdfName BM_COLORBURN;
    public static final PdfName BM_HARDLIGHT;
    public static final PdfName BM_SOFTLIGHT;
    public static final PdfName BM_DIFFERENCE;
    public static final PdfName BM_EXCLUSION;
    
    public void setOverPrintStroking(final boolean op) {
        this.put(PdfName.OP, op ? PdfBoolean.PDFTRUE : PdfBoolean.PDFFALSE);
    }
    
    public void setOverPrintNonStroking(final boolean op) {
        this.put(PdfName.op, op ? PdfBoolean.PDFTRUE : PdfBoolean.PDFFALSE);
    }
    
    public void setOverPrintMode(final int opm) {
        this.put(PdfName.OPM, new PdfNumber((int)((opm != 0) ? 1 : 0)));
    }
    
    public void setStrokeOpacity(final float ca) {
        this.put(PdfName.CA, new PdfNumber(ca));
    }
    
    public void setFillOpacity(final float ca) {
        this.put(PdfName.ca, new PdfNumber(ca));
    }
    
    public void setAlphaIsShape(final boolean ais) {
        this.put(PdfName.AIS, ais ? PdfBoolean.PDFTRUE : PdfBoolean.PDFFALSE);
    }
    
    public void setTextKnockout(final boolean tk) {
        this.put(PdfName.TK, tk ? PdfBoolean.PDFTRUE : PdfBoolean.PDFFALSE);
    }
    
    public void setBlendMode(final PdfName bm) {
        this.put(PdfName.BM, bm);
    }
    
    public void setRenderingIntent(final PdfName ri) {
        this.put(PdfName.RI, ri);
    }
    
    @Override
    public void toPdf(final PdfWriter writer, final OutputStream os) throws IOException {
        PdfWriter.checkPdfIsoConformance(writer, 6, this);
        super.toPdf(writer, os);
    }
    
    static {
        BM_NORMAL = new PdfName("Normal");
        BM_COMPATIBLE = new PdfName("Compatible");
        BM_MULTIPLY = new PdfName("Multiply");
        BM_SCREEN = new PdfName("Screen");
        BM_OVERLAY = new PdfName("Overlay");
        BM_DARKEN = new PdfName("Darken");
        BM_LIGHTEN = new PdfName("Lighten");
        BM_COLORDODGE = new PdfName("ColorDodge");
        BM_COLORBURN = new PdfName("ColorBurn");
        BM_HARDLIGHT = new PdfName("HardLight");
        BM_SOFTLIGHT = new PdfName("SoftLight");
        BM_DIFFERENCE = new PdfName("Difference");
        BM_EXCLUSION = new PdfName("Exclusion");
    }
}
