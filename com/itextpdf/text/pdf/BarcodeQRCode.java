// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.BaseColor;
import java.awt.image.ImageProducer;
import java.awt.image.MemoryImageSource;
import java.awt.Canvas;
import java.awt.Color;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.pdf.codec.CCITTG4Encoder;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.qrcode.WriterException;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.pdf.qrcode.QRCodeWriter;
import com.itextpdf.text.pdf.qrcode.EncodeHintType;
import java.util.Map;
import com.itextpdf.text.pdf.qrcode.ByteMatrix;

public class BarcodeQRCode
{
    ByteMatrix bm;
    
    public BarcodeQRCode(final String content, final int width, final int height, final Map<EncodeHintType, Object> hints) {
        try {
            final QRCodeWriter qc = new QRCodeWriter();
            this.bm = qc.encode(content, width, height, hints);
        }
        catch (WriterException ex) {
            throw new ExceptionConverter(ex);
        }
    }
    
    private byte[] getBitMatrix() {
        final int width = this.bm.getWidth();
        final int height = this.bm.getHeight();
        final int stride = (width + 7) / 8;
        final byte[] b = new byte[stride * height];
        final byte[][] mt = this.bm.getArray();
        for (int y = 0; y < height; ++y) {
            final byte[] line = mt[y];
            for (int x = 0; x < width; ++x) {
                if (line[x] != 0) {
                    final int offset = stride * y + x / 8;
                    final byte[] array = b;
                    final int n = offset;
                    array[n] |= (byte)(128 >> x % 8);
                }
            }
        }
        return b;
    }
    
    public Image getImage() throws BadElementException {
        final byte[] b = this.getBitMatrix();
        final byte[] g4 = CCITTG4Encoder.compress(b, this.bm.getWidth(), this.bm.getHeight());
        return Image.getInstance(this.bm.getWidth(), this.bm.getHeight(), false, 256, 1, g4, null);
    }
    
    public java.awt.Image createAwtImage(final Color foreground, final Color background) {
        final int f = foreground.getRGB();
        final int g = background.getRGB();
        final Canvas canvas = new Canvas();
        final int width = this.bm.getWidth();
        final int height = this.bm.getHeight();
        final int[] pix = new int[width * height];
        final byte[][] mt = this.bm.getArray();
        for (int y = 0; y < height; ++y) {
            final byte[] line = mt[y];
            for (int x = 0; x < width; ++x) {
                pix[y * width + x] = ((line[x] == 0) ? f : g);
            }
        }
        final java.awt.Image img = canvas.createImage(new MemoryImageSource(width, height, pix, 0, width));
        return img;
    }
    
    public void placeBarcode(final PdfContentByte cb, final BaseColor foreground, final float moduleSide) {
        final int width = this.bm.getWidth();
        final int height = this.bm.getHeight();
        final byte[][] mt = this.bm.getArray();
        cb.setColorFill(foreground);
        for (int y = 0; y < height; ++y) {
            final byte[] line = mt[y];
            for (int x = 0; x < width; ++x) {
                if (line[x] == 0) {
                    cb.rectangle(x * moduleSide, (height - y - 1) * moduleSide, moduleSide, moduleSide);
                }
            }
        }
        cb.fill();
    }
    
    public Rectangle getBarcodeSize() {
        return new Rectangle(0.0f, 0.0f, (float)this.bm.getWidth(), (float)this.bm.getHeight());
    }
}
