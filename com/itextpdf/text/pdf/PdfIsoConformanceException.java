// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public class PdfIsoConformanceException extends RuntimeException
{
    private static final long serialVersionUID = -8972376258066225871L;
    
    public PdfIsoConformanceException() {
    }
    
    public PdfIsoConformanceException(final String s) {
        super(s);
    }
}
