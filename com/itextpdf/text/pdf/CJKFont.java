// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.ExceptionConverter;
import java.io.ByteArrayOutputStream;
import java.util.Enumeration;
import java.util.StringTokenizer;
import com.itextpdf.text.Utilities;
import java.util.Map;
import com.itextpdf.text.pdf.fonts.cmaps.CMapCache;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import com.itextpdf.text.io.StreamUtil;
import java.util.Iterator;
import com.itextpdf.text.pdf.fonts.cmaps.CMapCidUni;
import com.itextpdf.text.pdf.fonts.cmaps.CMapUniCid;
import com.itextpdf.text.pdf.fonts.cmaps.CMapCidByte;
import java.util.Set;
import java.util.HashMap;
import java.util.Properties;

class CJKFont extends BaseFont
{
    static final String CJK_ENCODING = "UnicodeBigUnmarked";
    private static final int FIRST = 0;
    private static final int BRACKET = 1;
    private static final int SERIAL = 2;
    private static final int V1Y = 880;
    static Properties cjkFonts;
    static Properties cjkEncodings;
    private static final HashMap<String, HashMap<String, Object>> allFonts;
    private static boolean propertiesLoaded;
    public static final String RESOURCE_PATH_CMAP = "com/itextpdf/text/pdf/fonts/cmaps/";
    private static final HashMap<String, Set<String>> registryNames;
    private CMapCidByte cidByte;
    private CMapUniCid uniCid;
    private CMapCidUni cidUni;
    private String uniMap;
    private String fontName;
    private String style;
    private String CMap;
    private boolean cidDirect;
    private IntHashtable vMetrics;
    private IntHashtable hMetrics;
    private HashMap<String, Object> fontDesc;
    
    private static void loadProperties() {
        if (CJKFont.propertiesLoaded) {
            return;
        }
        synchronized (CJKFont.allFonts) {
            if (CJKFont.propertiesLoaded) {
                return;
            }
            try {
                loadRegistry();
                for (final String font : CJKFont.registryNames.get("fonts")) {
                    CJKFont.allFonts.put(font, readFontProperties(font));
                }
            }
            catch (Exception ex) {}
            CJKFont.propertiesLoaded = true;
        }
    }
    
    private static void loadRegistry() throws IOException {
        final InputStream is = StreamUtil.getResourceStream("com/itextpdf/text/pdf/fonts/cmaps/cjk_registry.properties");
        final Properties p = new Properties();
        p.load(is);
        is.close();
        for (final Object key : p.keySet()) {
            final String value = p.getProperty((String)key);
            final String[] sp = value.split(" ");
            final Set<String> hs = new HashSet<String>();
            for (final String s : sp) {
                if (s.length() > 0) {
                    hs.add(s);
                }
            }
            CJKFont.registryNames.put((String)key, hs);
        }
    }
    
    CJKFont(String fontName, final String enc, final boolean emb) throws DocumentException {
        this.style = "";
        this.cidDirect = false;
        loadProperties();
        this.fontType = 2;
        final String nameBase = BaseFont.getBaseName(fontName);
        if (!isCJKFont(nameBase, enc)) {
            throw new DocumentException(MessageLocalization.getComposedMessage("font.1.with.2.encoding.is.not.a.cjk.font", fontName, enc));
        }
        if (nameBase.length() < fontName.length()) {
            this.style = fontName.substring(nameBase.length());
            fontName = nameBase;
        }
        this.fontName = fontName;
        this.encoding = "UnicodeBigUnmarked";
        this.vertical = enc.endsWith("V");
        this.CMap = enc;
        if (enc.equals("Identity-H") || enc.equals("Identity-V")) {
            this.cidDirect = true;
        }
        this.loadCMaps();
    }
    
    String getUniMap() {
        return this.uniMap;
    }
    
    private void loadCMaps() throws DocumentException {
        try {
            this.fontDesc = CJKFont.allFonts.get(this.fontName);
            this.hMetrics = this.fontDesc.get("W");
            this.vMetrics = this.fontDesc.get("W2");
            final String registry = this.fontDesc.get("Registry");
            this.uniMap = "";
            for (final String name : CJKFont.registryNames.get(registry + "_Uni")) {
                this.uniMap = name;
                if (name.endsWith("V") && this.vertical) {
                    break;
                }
                if (!name.endsWith("V") && !this.vertical) {
                    break;
                }
            }
            if (this.cidDirect) {
                this.cidUni = CMapCache.getCachedCMapCidUni(this.uniMap);
            }
            else {
                this.uniCid = CMapCache.getCachedCMapUniCid(this.uniMap);
                this.cidByte = CMapCache.getCachedCMapCidByte(this.CMap);
            }
        }
        catch (Exception ex) {
            throw new DocumentException(ex);
        }
    }
    
    public static String GetCompatibleFont(final String enc) {
        loadProperties();
        String registry = null;
        for (final Map.Entry<String, Set<String>> e : CJKFont.registryNames.entrySet()) {
            if (e.getValue().contains(enc)) {
                registry = e.getKey();
                for (final Map.Entry<String, HashMap<String, Object>> e2 : CJKFont.allFonts.entrySet()) {
                    if (registry.equals(e2.getValue().get("Registry"))) {
                        return e2.getKey();
                    }
                }
            }
        }
        return null;
    }
    
    public static boolean isCJKFont(final String fontName, final String enc) {
        loadProperties();
        if (!CJKFont.registryNames.containsKey("fonts")) {
            return false;
        }
        if (!CJKFont.registryNames.get("fonts").contains(fontName)) {
            return false;
        }
        if (enc.equals("Identity-H") || enc.equals("Identity-V")) {
            return true;
        }
        final String registry = CJKFont.allFonts.get(fontName).get("Registry");
        final Set<String> encodings = CJKFont.registryNames.get(registry);
        return encodings != null && encodings.contains(enc);
    }
    
    @Override
    public int getWidth(final int char1) {
        int c = char1;
        if (!this.cidDirect) {
            c = this.uniCid.lookup(char1);
        }
        int v;
        if (this.vertical) {
            v = this.vMetrics.get(c);
        }
        else {
            v = this.hMetrics.get(c);
        }
        if (v > 0) {
            return v;
        }
        return 1000;
    }
    
    @Override
    public int getWidth(final String text) {
        int total = 0;
        if (this.cidDirect) {
            for (int k = 0; k < text.length(); ++k) {
                total += this.getWidth(text.charAt(k));
            }
        }
        else {
            for (int k = 0; k < text.length(); ++k) {
                int val;
                if (Utilities.isSurrogatePair(text, k)) {
                    val = Utilities.convertToUtf32(text, k);
                    ++k;
                }
                else {
                    val = text.charAt(k);
                }
                total += this.getWidth(val);
            }
        }
        return total;
    }
    
    @Override
    int getRawWidth(final int c, final String name) {
        return 0;
    }
    
    @Override
    public int getKerning(final int char1, final int char2) {
        return 0;
    }
    
    private PdfDictionary getFontDescriptor() {
        final PdfDictionary dic = new PdfDictionary(PdfName.FONTDESCRIPTOR);
        dic.put(PdfName.ASCENT, new PdfLiteral(this.fontDesc.get("Ascent")));
        dic.put(PdfName.CAPHEIGHT, new PdfLiteral(this.fontDesc.get("CapHeight")));
        dic.put(PdfName.DESCENT, new PdfLiteral(this.fontDesc.get("Descent")));
        dic.put(PdfName.FLAGS, new PdfLiteral(this.fontDesc.get("Flags")));
        dic.put(PdfName.FONTBBOX, new PdfLiteral(this.fontDesc.get("FontBBox")));
        dic.put(PdfName.FONTNAME, new PdfName(this.fontName + this.style));
        dic.put(PdfName.ITALICANGLE, new PdfLiteral(this.fontDesc.get("ItalicAngle")));
        dic.put(PdfName.STEMV, new PdfLiteral(this.fontDesc.get("StemV")));
        final PdfDictionary pdic = new PdfDictionary();
        pdic.put(PdfName.PANOSE, new PdfString(this.fontDesc.get("Panose"), null));
        dic.put(PdfName.STYLE, pdic);
        return dic;
    }
    
    private PdfDictionary getCIDFont(final PdfIndirectReference fontDescriptor, final IntHashtable cjkTag) {
        final PdfDictionary dic = new PdfDictionary(PdfName.FONT);
        dic.put(PdfName.SUBTYPE, PdfName.CIDFONTTYPE0);
        dic.put(PdfName.BASEFONT, new PdfName(this.fontName + this.style));
        dic.put(PdfName.FONTDESCRIPTOR, fontDescriptor);
        final int[] keys = cjkTag.toOrderedKeys();
        String w = convertToHCIDMetrics(keys, this.hMetrics);
        if (w != null) {
            dic.put(PdfName.W, new PdfLiteral(w));
        }
        if (this.vertical) {
            w = convertToVCIDMetrics(keys, this.vMetrics, this.hMetrics);
            if (w != null) {
                dic.put(PdfName.W2, new PdfLiteral(w));
            }
        }
        else {
            dic.put(PdfName.DW, new PdfNumber(1000));
        }
        final PdfDictionary cdic = new PdfDictionary();
        if (this.cidDirect) {
            cdic.put(PdfName.REGISTRY, new PdfString(this.cidUni.getRegistry(), null));
            cdic.put(PdfName.ORDERING, new PdfString(this.cidUni.getOrdering(), null));
            cdic.put(PdfName.SUPPLEMENT, new PdfNumber(this.cidUni.getSupplement()));
        }
        else {
            cdic.put(PdfName.REGISTRY, new PdfString(this.cidByte.getRegistry(), null));
            cdic.put(PdfName.ORDERING, new PdfString(this.cidByte.getOrdering(), null));
            cdic.put(PdfName.SUPPLEMENT, new PdfNumber(this.cidByte.getSupplement()));
        }
        dic.put(PdfName.CIDSYSTEMINFO, cdic);
        return dic;
    }
    
    private PdfDictionary getFontBaseType(final PdfIndirectReference CIDFont) {
        final PdfDictionary dic = new PdfDictionary(PdfName.FONT);
        dic.put(PdfName.SUBTYPE, PdfName.TYPE0);
        String name = this.fontName;
        if (this.style.length() > 0) {
            name = name + "-" + this.style.substring(1);
        }
        name = name + "-" + this.CMap;
        dic.put(PdfName.BASEFONT, new PdfName(name));
        dic.put(PdfName.ENCODING, new PdfName(this.CMap));
        dic.put(PdfName.DESCENDANTFONTS, new PdfArray(CIDFont));
        return dic;
    }
    
    @Override
    void writeFont(final PdfWriter writer, final PdfIndirectReference ref, final Object[] params) throws DocumentException, IOException {
        final IntHashtable cjkTag = (IntHashtable)params[0];
        PdfIndirectReference ind_font = null;
        PdfObject pobj = null;
        PdfIndirectObject obj = null;
        pobj = this.getFontDescriptor();
        if (pobj != null) {
            obj = writer.addToBody(pobj);
            ind_font = obj.getIndirectReference();
        }
        pobj = this.getCIDFont(ind_font, cjkTag);
        if (pobj != null) {
            obj = writer.addToBody(pobj);
            ind_font = obj.getIndirectReference();
        }
        pobj = this.getFontBaseType(ind_font);
        writer.addToBody(pobj, ref);
    }
    
    public PdfStream getFullFontStream() {
        return null;
    }
    
    private float getDescNumber(final String name) {
        return (float)Integer.parseInt(this.fontDesc.get(name));
    }
    
    private float getBBox(final int idx) {
        final String s = this.fontDesc.get("FontBBox");
        final StringTokenizer tk = new StringTokenizer(s, " []\r\n\t\f");
        String ret = tk.nextToken();
        for (int k = 0; k < idx; ++k) {
            ret = tk.nextToken();
        }
        return (float)Integer.parseInt(ret);
    }
    
    @Override
    public float getFontDescriptor(final int key, final float fontSize) {
        switch (key) {
            case 1:
            case 9: {
                return this.getDescNumber("Ascent") * fontSize / 1000.0f;
            }
            case 2: {
                return this.getDescNumber("CapHeight") * fontSize / 1000.0f;
            }
            case 3:
            case 10: {
                return this.getDescNumber("Descent") * fontSize / 1000.0f;
            }
            case 4: {
                return this.getDescNumber("ItalicAngle");
            }
            case 5: {
                return fontSize * this.getBBox(0) / 1000.0f;
            }
            case 6: {
                return fontSize * this.getBBox(1) / 1000.0f;
            }
            case 7: {
                return fontSize * this.getBBox(2) / 1000.0f;
            }
            case 8: {
                return fontSize * this.getBBox(3) / 1000.0f;
            }
            case 11: {
                return 0.0f;
            }
            case 12: {
                return fontSize * (this.getBBox(2) - this.getBBox(0)) / 1000.0f;
            }
            default: {
                return 0.0f;
            }
        }
    }
    
    @Override
    public String getPostscriptFontName() {
        return this.fontName;
    }
    
    @Override
    public String[][] getFullFontName() {
        return new String[][] { { "", "", "", this.fontName } };
    }
    
    @Override
    public String[][] getAllNameEntries() {
        return new String[][] { { "4", "", "", "", this.fontName } };
    }
    
    @Override
    public String[][] getFamilyFontName() {
        return this.getFullFontName();
    }
    
    static IntHashtable createMetric(final String s) {
        final IntHashtable h = new IntHashtable();
        final StringTokenizer tk = new StringTokenizer(s);
        while (tk.hasMoreTokens()) {
            final int n1 = Integer.parseInt(tk.nextToken());
            h.put(n1, Integer.parseInt(tk.nextToken()));
        }
        return h;
    }
    
    static String convertToHCIDMetrics(final int[] keys, final IntHashtable h) {
        if (keys.length == 0) {
            return null;
        }
        int lastCid = 0;
        int lastValue = 0;
        int start;
        for (start = 0; start < keys.length; ++start) {
            lastCid = keys[start];
            lastValue = h.get(lastCid);
            if (lastValue != 0) {
                ++start;
                break;
            }
        }
        if (lastValue == 0) {
            return null;
        }
        final StringBuilder buf = new StringBuilder();
        buf.append('[');
        buf.append(lastCid);
        int state = 0;
        for (int k = start; k < keys.length; ++k) {
            final int cid = keys[k];
            final int value = h.get(cid);
            if (value != 0) {
                switch (state) {
                    case 0: {
                        if (cid == lastCid + 1 && value == lastValue) {
                            state = 2;
                            break;
                        }
                        if (cid == lastCid + 1) {
                            state = 1;
                            buf.append('[').append(lastValue);
                            break;
                        }
                        buf.append('[').append(lastValue).append(']').append(cid);
                        break;
                    }
                    case 1: {
                        if (cid == lastCid + 1 && value == lastValue) {
                            state = 2;
                            buf.append(']').append(lastCid);
                            break;
                        }
                        if (cid == lastCid + 1) {
                            buf.append(' ').append(lastValue);
                            break;
                        }
                        state = 0;
                        buf.append(' ').append(lastValue).append(']').append(cid);
                        break;
                    }
                    case 2: {
                        if (cid != lastCid + 1 || value != lastValue) {
                            buf.append(' ').append(lastCid).append(' ').append(lastValue).append(' ').append(cid);
                            state = 0;
                            break;
                        }
                        break;
                    }
                }
                lastValue = value;
                lastCid = cid;
            }
        }
        switch (state) {
            case 0: {
                buf.append('[').append(lastValue).append("]]");
                break;
            }
            case 1: {
                buf.append(' ').append(lastValue).append("]]");
                break;
            }
            case 2: {
                buf.append(' ').append(lastCid).append(' ').append(lastValue).append(']');
                break;
            }
        }
        return buf.toString();
    }
    
    static String convertToVCIDMetrics(final int[] keys, final IntHashtable v, final IntHashtable h) {
        if (keys.length == 0) {
            return null;
        }
        int lastCid = 0;
        int lastValue = 0;
        int lastHValue = 0;
        int start;
        for (start = 0; start < keys.length; ++start) {
            lastCid = keys[start];
            lastValue = v.get(lastCid);
            if (lastValue != 0) {
                ++start;
                break;
            }
            lastHValue = h.get(lastCid);
        }
        if (lastValue == 0) {
            return null;
        }
        if (lastHValue == 0) {
            lastHValue = 1000;
        }
        final StringBuilder buf = new StringBuilder();
        buf.append('[');
        buf.append(lastCid);
        int state = 0;
        for (int k = start; k < keys.length; ++k) {
            final int cid = keys[k];
            final int value = v.get(cid);
            if (value != 0) {
                int hValue = h.get(lastCid);
                if (hValue == 0) {
                    hValue = 1000;
                }
                switch (state) {
                    case 0: {
                        if (cid == lastCid + 1 && value == lastValue && hValue == lastHValue) {
                            state = 2;
                            break;
                        }
                        buf.append(' ').append(lastCid).append(' ').append(-lastValue).append(' ').append(lastHValue / 2).append(' ').append(880).append(' ').append(cid);
                        break;
                    }
                    case 2: {
                        if (cid != lastCid + 1 || value != lastValue || hValue != lastHValue) {
                            buf.append(' ').append(lastCid).append(' ').append(-lastValue).append(' ').append(lastHValue / 2).append(' ').append(880).append(' ').append(cid);
                            state = 0;
                            break;
                        }
                        break;
                    }
                }
                lastValue = value;
                lastCid = cid;
                lastHValue = hValue;
            }
        }
        buf.append(' ').append(lastCid).append(' ').append(-lastValue).append(' ').append(lastHValue / 2).append(' ').append(880).append(" ]");
        return buf.toString();
    }
    
    private static HashMap<String, Object> readFontProperties(String name) throws IOException {
        name += ".properties";
        final InputStream is = StreamUtil.getResourceStream("com/itextpdf/text/pdf/fonts/cmaps/" + name);
        final Properties p = new Properties();
        p.load(is);
        is.close();
        final IntHashtable W = createMetric(p.getProperty("W"));
        p.remove("W");
        final IntHashtable W2 = createMetric(p.getProperty("W2"));
        p.remove("W2");
        final HashMap<String, Object> map = new HashMap<String, Object>();
        final Enumeration<Object> e = p.keys();
        while (e.hasMoreElements()) {
            final Object obj = e.nextElement();
            map.put((String)obj, p.getProperty((String)obj));
        }
        map.put("W", W);
        map.put("W2", W2);
        return map;
    }
    
    @Override
    public int getUnicodeEquivalent(final int c) {
        if (!this.cidDirect) {
            return c;
        }
        if (c == 32767) {
            return 10;
        }
        return this.cidUni.lookup(c);
    }
    
    @Override
    public int getCidCode(final int c) {
        if (this.cidDirect) {
            return c;
        }
        return this.uniCid.lookup(c);
    }
    
    @Override
    public boolean hasKernPairs() {
        return false;
    }
    
    @Override
    public boolean charExists(final int c) {
        return this.cidDirect || this.cidByte.lookup(this.uniCid.lookup(c)).length > 0;
    }
    
    @Override
    public boolean setCharAdvance(final int c, final int advance) {
        return false;
    }
    
    @Override
    public void setPostscriptFontName(final String name) {
        this.fontName = name;
    }
    
    @Override
    public boolean setKerning(final int char1, final int char2, final int kern) {
        return false;
    }
    
    @Override
    public int[] getCharBBox(final int c) {
        return null;
    }
    
    @Override
    protected int[] getRawCharBBox(final int c, final String name) {
        return null;
    }
    
    @Override
    public byte[] convertToBytes(final String text) {
        if (this.cidDirect) {
            return super.convertToBytes(text);
        }
        try {
            if (text.length() == 1) {
                return this.convertToBytes(text.charAt(0));
            }
            final ByteArrayOutputStream bout = new ByteArrayOutputStream();
            for (int k = 0; k < text.length(); ++k) {
                int val;
                if (Utilities.isSurrogatePair(text, k)) {
                    val = Utilities.convertToUtf32(text, k);
                    ++k;
                }
                else {
                    val = text.charAt(k);
                }
                bout.write(this.convertToBytes(val));
            }
            return bout.toByteArray();
        }
        catch (Exception ex) {
            throw new ExceptionConverter(ex);
        }
    }
    
    @Override
    byte[] convertToBytes(final int char1) {
        if (this.cidDirect) {
            return super.convertToBytes(char1);
        }
        return this.cidByte.lookup(this.uniCid.lookup(char1));
    }
    
    public boolean isIdentity() {
        return this.cidDirect;
    }
    
    static {
        CJKFont.cjkFonts = new Properties();
        CJKFont.cjkEncodings = new Properties();
        allFonts = new HashMap<String, HashMap<String, Object>>();
        CJKFont.propertiesLoaded = false;
        registryNames = new HashMap<String, Set<String>>();
    }
}
