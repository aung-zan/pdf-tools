// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.PageSize;
import java.util.HashSet;
import com.itextpdf.text.log.CounterFactory;
import com.itextpdf.text.log.LoggerFactory;
import java.util.List;
import java.util.Stack;
import java.util.Set;
import java.util.Arrays;
import java.io.OutputStream;
import java.util.zip.InflaterInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.ByteArrayInputStream;
import java.util.Map;
import java.util.Collections;
import com.itextpdf.text.log.Level;
import java.security.MessageDigest;
import java.security.PrivateKey;
import org.bouncycastle.cms.RecipientInformation;
import org.bouncycastle.cms.CMSEnvelopedData;
import com.itextpdf.text.ExceptionConverter;
import org.bouncycastle.cert.X509CertificateHolder;
import com.itextpdf.text.exceptions.UnsupportedPdfException;
import com.itextpdf.text.DocWriter;
import com.itextpdf.text.exceptions.BadPasswordException;
import com.itextpdf.text.exceptions.InvalidPdfException;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.util.Iterator;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.io.WindowRandomAccessSource;
import java.util.Collection;
import java.net.URL;
import java.io.InputStream;
import com.itextpdf.text.Document;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import java.io.IOException;
import com.itextpdf.text.io.RandomAccessSource;
import com.itextpdf.text.log.Counter;
import com.itextpdf.text.pdf.internal.PdfViewerPreferencesImp;
import com.itextpdf.text.pdf.security.ExternalDecryptionProcess;
import java.security.cert.Certificate;
import java.security.Key;
import java.util.ArrayList;
import java.util.HashMap;
import com.itextpdf.text.log.Logger;
import com.itextpdf.text.pdf.interfaces.PdfViewerPreferences;

public class PdfReader implements PdfViewerPreferences
{
    public static boolean unethicalreading;
    public static boolean debugmode;
    private static final Logger LOGGER;
    static final PdfName[] pageInhCandidates;
    static final byte[] endstream;
    static final byte[] endobj;
    protected PRTokeniser tokens;
    protected long[] xref;
    protected HashMap<Integer, IntHashtable> objStmMark;
    protected LongHashtable objStmToOffset;
    protected boolean newXrefType;
    protected ArrayList<PdfObject> xrefObj;
    PdfDictionary rootPages;
    protected PdfDictionary trailer;
    protected PdfDictionary catalog;
    protected PageRefs pageRefs;
    protected PRAcroForm acroForm;
    protected boolean acroFormParsed;
    protected boolean encrypted;
    protected boolean rebuilt;
    protected int freeXref;
    protected boolean tampered;
    protected long lastXref;
    protected long eofPos;
    protected char pdfVersion;
    protected PdfEncryption decrypt;
    protected byte[] password;
    protected Key certificateKey;
    protected Certificate certificate;
    protected String certificateKeyProvider;
    protected ExternalDecryptionProcess externalDecryptionProcess;
    private boolean ownerPasswordUsed;
    protected ArrayList<PdfString> strings;
    protected boolean sharedStreams;
    protected boolean consolidateNamedDestinations;
    protected boolean remoteToLocalNamedDestinations;
    protected int rValue;
    protected long pValue;
    private int objNum;
    private int objGen;
    private long fileLength;
    private boolean hybridXref;
    private int lastXrefPartial;
    private boolean partial;
    private PRIndirectReference cryptoRef;
    private final PdfViewerPreferencesImp viewerPreferences;
    private boolean encryptionError;
    private boolean appendable;
    protected static Counter COUNTER;
    private int readDepth;
    
    protected Counter getCounter() {
        return PdfReader.COUNTER;
    }
    
    private PdfReader(final RandomAccessSource byteSource, final boolean partialRead, final byte[] ownerPassword, final Certificate certificate, final Key certificateKey, final String certificateKeyProvider, final ExternalDecryptionProcess externalDecryptionProcess, final boolean closeSourceOnConstructorError) throws IOException {
        this.acroForm = null;
        this.acroFormParsed = false;
        this.encrypted = false;
        this.rebuilt = false;
        this.tampered = false;
        this.password = null;
        this.certificateKey = null;
        this.certificate = null;
        this.certificateKeyProvider = null;
        this.externalDecryptionProcess = null;
        this.strings = new ArrayList<PdfString>();
        this.sharedStreams = true;
        this.consolidateNamedDestinations = false;
        this.remoteToLocalNamedDestinations = false;
        this.lastXrefPartial = -1;
        this.viewerPreferences = new PdfViewerPreferencesImp();
        this.readDepth = 0;
        this.certificate = certificate;
        this.certificateKey = certificateKey;
        this.certificateKeyProvider = certificateKeyProvider;
        this.externalDecryptionProcess = externalDecryptionProcess;
        this.password = ownerPassword;
        this.partial = partialRead;
        try {
            this.tokens = getOffsetTokeniser(byteSource);
            if (partialRead) {
                this.readPdfPartial();
            }
            else {
                this.readPdf();
            }
        }
        catch (IOException e) {
            if (closeSourceOnConstructorError) {
                byteSource.close();
            }
            throw e;
        }
        this.getCounter().read(this.fileLength);
    }
    
    public PdfReader(final String filename) throws IOException {
        this(filename, null);
    }
    
    public PdfReader(final String filename, final byte[] ownerPassword) throws IOException {
        this(filename, ownerPassword, false);
    }
    
    public PdfReader(final String filename, final byte[] ownerPassword, final boolean partial) throws IOException {
        this(new RandomAccessSourceFactory().setForceRead(false).setUsePlainRandomAccess(Document.plainRandomAccess).createBestSource(filename), partial, ownerPassword, null, null, null, null, true);
    }
    
    public PdfReader(final byte[] pdfIn) throws IOException {
        this(pdfIn, null);
    }
    
    public PdfReader(final byte[] pdfIn, final byte[] ownerPassword) throws IOException {
        this(new RandomAccessSourceFactory().createSource(pdfIn), false, ownerPassword, null, null, null, null, true);
    }
    
    public PdfReader(final String filename, final Certificate certificate, final Key certificateKey, final String certificateKeyProvider) throws IOException {
        this(new RandomAccessSourceFactory().setForceRead(false).setUsePlainRandomAccess(Document.plainRandomAccess).createBestSource(filename), false, null, certificate, certificateKey, certificateKeyProvider, null, true);
    }
    
    public PdfReader(final String filename, final Certificate certificate, final ExternalDecryptionProcess externalDecryptionProcess) throws IOException {
        this(new RandomAccessSourceFactory().setForceRead(false).setUsePlainRandomAccess(Document.plainRandomAccess).createBestSource(filename), false, null, certificate, null, null, externalDecryptionProcess, true);
    }
    
    public PdfReader(final byte[] pdfIn, final Certificate certificate, final ExternalDecryptionProcess externalDecryptionProcess) throws IOException {
        this(new RandomAccessSourceFactory().setForceRead(false).setUsePlainRandomAccess(Document.plainRandomAccess).createSource(pdfIn), false, null, certificate, null, null, externalDecryptionProcess, true);
    }
    
    public PdfReader(final InputStream inputStream, final Certificate certificate, final ExternalDecryptionProcess externalDecryptionProcess) throws IOException {
        this(new RandomAccessSourceFactory().setForceRead(false).setUsePlainRandomAccess(Document.plainRandomAccess).createSource(inputStream), false, null, certificate, null, null, externalDecryptionProcess, true);
    }
    
    public PdfReader(final URL url) throws IOException {
        this(url, null);
    }
    
    public PdfReader(final URL url, final byte[] ownerPassword) throws IOException {
        this(new RandomAccessSourceFactory().createSource(url), false, ownerPassword, null, null, null, null, true);
    }
    
    public PdfReader(final InputStream is, final byte[] ownerPassword) throws IOException {
        this(new RandomAccessSourceFactory().createSource(is), false, ownerPassword, null, null, null, null, false);
    }
    
    public PdfReader(final InputStream is) throws IOException {
        this(is, null);
    }
    
    public PdfReader(final RandomAccessFileOrArray raf, final byte[] ownerPassword) throws IOException {
        this(raf, ownerPassword, true);
    }
    
    public PdfReader(final RandomAccessFileOrArray raf, final byte[] ownerPassword, final boolean partial) throws IOException {
        this(raf.getByteSource(), partial, ownerPassword, null, null, null, null, false);
    }
    
    public PdfReader(final PdfReader reader) {
        this.acroForm = null;
        this.acroFormParsed = false;
        this.encrypted = false;
        this.rebuilt = false;
        this.tampered = false;
        this.password = null;
        this.certificateKey = null;
        this.certificate = null;
        this.certificateKeyProvider = null;
        this.externalDecryptionProcess = null;
        this.strings = new ArrayList<PdfString>();
        this.sharedStreams = true;
        this.consolidateNamedDestinations = false;
        this.remoteToLocalNamedDestinations = false;
        this.lastXrefPartial = -1;
        this.viewerPreferences = new PdfViewerPreferencesImp();
        this.readDepth = 0;
        this.appendable = reader.appendable;
        this.consolidateNamedDestinations = reader.consolidateNamedDestinations;
        this.encrypted = reader.encrypted;
        this.rebuilt = reader.rebuilt;
        this.sharedStreams = reader.sharedStreams;
        this.tampered = reader.tampered;
        this.password = reader.password;
        this.pdfVersion = reader.pdfVersion;
        this.eofPos = reader.eofPos;
        this.freeXref = reader.freeXref;
        this.lastXref = reader.lastXref;
        this.newXrefType = reader.newXrefType;
        this.tokens = new PRTokeniser(reader.tokens.getSafeFile());
        if (reader.decrypt != null) {
            this.decrypt = new PdfEncryption(reader.decrypt);
        }
        this.pValue = reader.pValue;
        this.rValue = reader.rValue;
        this.xrefObj = new ArrayList<PdfObject>(reader.xrefObj);
        for (int k = 0; k < reader.xrefObj.size(); ++k) {
            this.xrefObj.set(k, duplicatePdfObject(reader.xrefObj.get(k), this));
        }
        this.pageRefs = new PageRefs(reader.pageRefs, this);
        this.trailer = (PdfDictionary)duplicatePdfObject(reader.trailer, this);
        this.catalog = this.trailer.getAsDict(PdfName.ROOT);
        this.rootPages = this.catalog.getAsDict(PdfName.PAGES);
        this.fileLength = reader.fileLength;
        this.partial = reader.partial;
        this.hybridXref = reader.hybridXref;
        this.objStmToOffset = reader.objStmToOffset;
        this.xref = reader.xref;
        this.cryptoRef = (PRIndirectReference)duplicatePdfObject(reader.cryptoRef, this);
        this.ownerPasswordUsed = reader.ownerPasswordUsed;
    }
    
    private static PRTokeniser getOffsetTokeniser(final RandomAccessSource byteSource) throws IOException {
        PRTokeniser tok = new PRTokeniser(new RandomAccessFileOrArray(byteSource));
        final int offset = tok.getHeaderOffset();
        if (offset != 0) {
            final RandomAccessSource offsetSource = new WindowRandomAccessSource(byteSource, offset);
            tok = new PRTokeniser(new RandomAccessFileOrArray(offsetSource));
        }
        return tok;
    }
    
    public RandomAccessFileOrArray getSafeFile() {
        return this.tokens.getSafeFile();
    }
    
    protected PdfReaderInstance getPdfReaderInstance(final PdfWriter writer) {
        return new PdfReaderInstance(this, writer);
    }
    
    public int getNumberOfPages() {
        return this.pageRefs.size();
    }
    
    public PdfDictionary getCatalog() {
        return this.catalog;
    }
    
    public PRAcroForm getAcroForm() {
        if (!this.acroFormParsed) {
            this.acroFormParsed = true;
            final PdfObject form = this.catalog.get(PdfName.ACROFORM);
            if (form != null) {
                try {
                    (this.acroForm = new PRAcroForm(this)).readAcroForm((PdfDictionary)getPdfObject(form));
                }
                catch (Exception e) {
                    this.acroForm = null;
                }
            }
        }
        return this.acroForm;
    }
    
    public int getPageRotation(final int index) {
        return this.getPageRotation(this.pageRefs.getPageNRelease(index));
    }
    
    int getPageRotation(final PdfDictionary page) {
        final PdfNumber rotate = page.getAsNumber(PdfName.ROTATE);
        if (rotate == null) {
            return 0;
        }
        int n = rotate.intValue();
        n %= 360;
        return (n < 0) ? (n + 360) : n;
    }
    
    public Rectangle getPageSizeWithRotation(final int index) {
        return this.getPageSizeWithRotation(this.pageRefs.getPageNRelease(index));
    }
    
    public Rectangle getPageSizeWithRotation(final PdfDictionary page) {
        Rectangle rect = this.getPageSize(page);
        for (int rotation = this.getPageRotation(page); rotation > 0; rotation -= 90) {
            rect = rect.rotate();
        }
        return rect;
    }
    
    public Rectangle getPageSize(final int index) {
        return this.getPageSize(this.pageRefs.getPageNRelease(index));
    }
    
    public Rectangle getPageSize(final PdfDictionary page) {
        final PdfArray mediaBox = page.getAsArray(PdfName.MEDIABOX);
        return getNormalizedRectangle(mediaBox);
    }
    
    public Rectangle getCropBox(final int index) {
        final PdfDictionary page = this.pageRefs.getPageNRelease(index);
        final PdfArray cropBox = (PdfArray)getPdfObjectRelease(page.get(PdfName.CROPBOX));
        if (cropBox == null) {
            return this.getPageSize(page);
        }
        return getNormalizedRectangle(cropBox);
    }
    
    public Rectangle getBoxSize(final int index, final String boxName) {
        final PdfDictionary page = this.pageRefs.getPageNRelease(index);
        PdfArray box = null;
        if (boxName.equals("trim")) {
            box = (PdfArray)getPdfObjectRelease(page.get(PdfName.TRIMBOX));
        }
        else if (boxName.equals("art")) {
            box = (PdfArray)getPdfObjectRelease(page.get(PdfName.ARTBOX));
        }
        else if (boxName.equals("bleed")) {
            box = (PdfArray)getPdfObjectRelease(page.get(PdfName.BLEEDBOX));
        }
        else if (boxName.equals("crop")) {
            box = (PdfArray)getPdfObjectRelease(page.get(PdfName.CROPBOX));
        }
        else if (boxName.equals("media")) {
            box = (PdfArray)getPdfObjectRelease(page.get(PdfName.MEDIABOX));
        }
        if (box == null) {
            return null;
        }
        return getNormalizedRectangle(box);
    }
    
    public HashMap<String, String> getInfo() {
        final HashMap<String, String> map = new HashMap<String, String>();
        final PdfDictionary info = this.trailer.getAsDict(PdfName.INFO);
        if (info == null) {
            return map;
        }
        for (final Object element : info.getKeys()) {
            final PdfName key = (PdfName)element;
            final PdfObject obj = getPdfObject(info.get(key));
            if (obj == null) {
                continue;
            }
            String value = obj.toString();
            switch (obj.type()) {
                case 3: {
                    value = ((PdfString)obj).toUnicodeString();
                    break;
                }
                case 4: {
                    value = PdfName.decodeName(value);
                    break;
                }
            }
            map.put(PdfName.decodeName(key.toString()), value);
        }
        return map;
    }
    
    public static Rectangle getNormalizedRectangle(final PdfArray box) {
        final float llx = ((PdfNumber)getPdfObjectRelease(box.getPdfObject(0))).floatValue();
        final float lly = ((PdfNumber)getPdfObjectRelease(box.getPdfObject(1))).floatValue();
        final float urx = ((PdfNumber)getPdfObjectRelease(box.getPdfObject(2))).floatValue();
        final float ury = ((PdfNumber)getPdfObjectRelease(box.getPdfObject(3))).floatValue();
        return new Rectangle(Math.min(llx, urx), Math.min(lly, ury), Math.max(llx, urx), Math.max(lly, ury));
    }
    
    public boolean isTagged() {
        final PdfDictionary markInfo = this.catalog.getAsDict(PdfName.MARKINFO);
        return markInfo != null && PdfBoolean.PDFTRUE.equals(markInfo.getAsBoolean(PdfName.MARKED)) && this.catalog.getAsDict(PdfName.STRUCTTREEROOT) != null;
    }
    
    protected void readPdf() throws IOException {
        this.fileLength = this.tokens.getFile().length();
        this.pdfVersion = this.tokens.checkPdfHeader();
        try {
            this.readXref();
        }
        catch (Exception e) {
            try {
                this.rebuilt = true;
                this.rebuildXref();
                this.lastXref = -1L;
            }
            catch (Exception ne) {
                throw new InvalidPdfException(MessageLocalization.getComposedMessage("rebuild.failed.1.original.message.2", ne.getMessage(), e.getMessage()));
            }
        }
        try {
            this.readDocObj();
        }
        catch (Exception e) {
            if (e instanceof BadPasswordException) {
                throw new BadPasswordException(e.getMessage());
            }
            if (this.rebuilt || this.encryptionError) {
                throw new InvalidPdfException(e.getMessage());
            }
            this.rebuilt = true;
            this.encrypted = false;
            try {
                this.rebuildXref();
                this.lastXref = -1L;
                this.readDocObj();
            }
            catch (Exception ne) {
                throw new InvalidPdfException(MessageLocalization.getComposedMessage("rebuild.failed.1.original.message.2", ne.getMessage(), e.getMessage()));
            }
        }
        this.strings.clear();
        this.readPages();
        this.removeUnusedObjects();
    }
    
    protected void readPdfPartial() throws IOException {
        this.fileLength = this.tokens.getFile().length();
        this.pdfVersion = this.tokens.checkPdfHeader();
        try {
            this.readXref();
        }
        catch (Exception e) {
            try {
                this.rebuilt = true;
                this.rebuildXref();
                this.lastXref = -1L;
            }
            catch (Exception ne) {
                throw new InvalidPdfException(MessageLocalization.getComposedMessage("rebuild.failed.1.original.message.2", ne.getMessage(), e.getMessage()), ne);
            }
        }
        this.readDocObjPartial();
        this.readPages();
    }
    
    private boolean equalsArray(final byte[] ar1, final byte[] ar2, final int size) {
        for (int k = 0; k < size; ++k) {
            if (ar1[k] != ar2[k]) {
                return false;
            }
        }
        return true;
    }
    
    private void readDecryptedDocObj() throws IOException {
        if (this.encrypted) {
            return;
        }
        final PdfObject encDic = this.trailer.get(PdfName.ENCRYPT);
        if (encDic == null || encDic.toString().equals("null")) {
            return;
        }
        this.encryptionError = true;
        byte[] encryptionKey = null;
        this.encrypted = true;
        final PdfDictionary enc = (PdfDictionary)getPdfObject(encDic);
        final PdfDictionary cfDict = enc.getAsDict(PdfName.CF);
        if (cfDict != null) {
            final PdfDictionary stdCFDict = cfDict.getAsDict(PdfName.STDCF);
            if (stdCFDict != null) {
                final PdfName authEvent = stdCFDict.getAsName(PdfName.AUTHEVENT);
                if (authEvent != null && authEvent.compareTo(PdfName.EFOPEN) == 0 && !this.ownerPasswordUsed) {
                    return;
                }
            }
        }
        final PdfArray documentIDs = this.trailer.getAsArray(PdfName.ID);
        byte[] documentID = null;
        if (documentIDs != null) {
            final PdfObject o = documentIDs.getPdfObject(0);
            this.strings.remove(o);
            final String s = o.toString();
            documentID = DocWriter.getISOBytes(s);
            if (documentIDs.size() > 1) {
                this.strings.remove(documentIDs.getPdfObject(1));
            }
        }
        if (documentID == null) {
            documentID = new byte[0];
        }
        byte[] uValue = null;
        byte[] oValue = null;
        int cryptoMode = 0;
        int lengthValue = 0;
        final PdfObject filter = getPdfObjectRelease(enc.get(PdfName.FILTER));
        if (filter.equals(PdfName.STANDARD)) {
            String s = enc.get(PdfName.U).toString();
            this.strings.remove(enc.get(PdfName.U));
            uValue = DocWriter.getISOBytes(s);
            s = enc.get(PdfName.O).toString();
            this.strings.remove(enc.get(PdfName.O));
            oValue = DocWriter.getISOBytes(s);
            if (enc.contains(PdfName.OE)) {
                this.strings.remove(enc.get(PdfName.OE));
            }
            if (enc.contains(PdfName.UE)) {
                this.strings.remove(enc.get(PdfName.UE));
            }
            if (enc.contains(PdfName.PERMS)) {
                this.strings.remove(enc.get(PdfName.PERMS));
            }
            PdfObject o = enc.get(PdfName.P);
            if (!o.isNumber()) {
                throw new InvalidPdfException(MessageLocalization.getComposedMessage("illegal.p.value", new Object[0]));
            }
            this.pValue = ((PdfNumber)o).longValue();
            o = enc.get(PdfName.R);
            if (!o.isNumber()) {
                throw new InvalidPdfException(MessageLocalization.getComposedMessage("illegal.r.value", new Object[0]));
            }
            switch (this.rValue = ((PdfNumber)o).intValue()) {
                case 2: {
                    cryptoMode = 0;
                    break;
                }
                case 3: {
                    o = enc.get(PdfName.LENGTH);
                    if (!o.isNumber()) {
                        throw new InvalidPdfException(MessageLocalization.getComposedMessage("illegal.length.value", new Object[0]));
                    }
                    lengthValue = ((PdfNumber)o).intValue();
                    if (lengthValue > 128 || lengthValue < 40 || lengthValue % 8 != 0) {
                        throw new InvalidPdfException(MessageLocalization.getComposedMessage("illegal.length.value", new Object[0]));
                    }
                    cryptoMode = 1;
                    break;
                }
                case 4: {
                    PdfDictionary dic = (PdfDictionary)enc.get(PdfName.CF);
                    if (dic == null) {
                        throw new InvalidPdfException(MessageLocalization.getComposedMessage("cf.not.found.encryption", new Object[0]));
                    }
                    dic = (PdfDictionary)dic.get(PdfName.STDCF);
                    if (dic == null) {
                        throw new InvalidPdfException(MessageLocalization.getComposedMessage("stdcf.not.found.encryption", new Object[0]));
                    }
                    if (PdfName.V2.equals(dic.get(PdfName.CFM))) {
                        cryptoMode = 1;
                    }
                    else {
                        if (!PdfName.AESV2.equals(dic.get(PdfName.CFM))) {
                            throw new UnsupportedPdfException(MessageLocalization.getComposedMessage("no.compatible.encryption.found", new Object[0]));
                        }
                        cryptoMode = 2;
                    }
                    final PdfObject em = enc.get(PdfName.ENCRYPTMETADATA);
                    if (em != null && em.toString().equals("false")) {
                        cryptoMode |= 0x8;
                        break;
                    }
                    break;
                }
                case 5: {
                    cryptoMode = 3;
                    final PdfObject em2 = enc.get(PdfName.ENCRYPTMETADATA);
                    if (em2 != null && em2.toString().equals("false")) {
                        cryptoMode |= 0x8;
                        break;
                    }
                    break;
                }
                default: {
                    throw new UnsupportedPdfException(MessageLocalization.getComposedMessage("unknown.encryption.type.r.eq.1", this.rValue));
                }
            }
        }
        else if (filter.equals(PdfName.PUBSEC)) {
            boolean foundRecipient = false;
            byte[] envelopedData = null;
            PdfArray recipients = null;
            PdfObject o = enc.get(PdfName.V);
            if (!o.isNumber()) {
                throw new InvalidPdfException(MessageLocalization.getComposedMessage("illegal.v.value", new Object[0]));
            }
            final int vValue = ((PdfNumber)o).intValue();
            switch (vValue) {
                case 1: {
                    cryptoMode = 0;
                    lengthValue = 40;
                    recipients = (PdfArray)enc.get(PdfName.RECIPIENTS);
                    break;
                }
                case 2: {
                    o = enc.get(PdfName.LENGTH);
                    if (!o.isNumber()) {
                        throw new InvalidPdfException(MessageLocalization.getComposedMessage("illegal.length.value", new Object[0]));
                    }
                    lengthValue = ((PdfNumber)o).intValue();
                    if (lengthValue > 128 || lengthValue < 40 || lengthValue % 8 != 0) {
                        throw new InvalidPdfException(MessageLocalization.getComposedMessage("illegal.length.value", new Object[0]));
                    }
                    cryptoMode = 1;
                    recipients = (PdfArray)enc.get(PdfName.RECIPIENTS);
                    break;
                }
                case 4:
                case 5: {
                    PdfDictionary dic2 = (PdfDictionary)enc.get(PdfName.CF);
                    if (dic2 == null) {
                        throw new InvalidPdfException(MessageLocalization.getComposedMessage("cf.not.found.encryption", new Object[0]));
                    }
                    dic2 = (PdfDictionary)dic2.get(PdfName.DEFAULTCRYPTFILTER);
                    if (dic2 == null) {
                        throw new InvalidPdfException(MessageLocalization.getComposedMessage("defaultcryptfilter.not.found.encryption", new Object[0]));
                    }
                    if (PdfName.V2.equals(dic2.get(PdfName.CFM))) {
                        cryptoMode = 1;
                        lengthValue = 128;
                    }
                    else if (PdfName.AESV2.equals(dic2.get(PdfName.CFM))) {
                        cryptoMode = 2;
                        lengthValue = 128;
                    }
                    else {
                        if (!PdfName.AESV3.equals(dic2.get(PdfName.CFM))) {
                            throw new UnsupportedPdfException(MessageLocalization.getComposedMessage("no.compatible.encryption.found", new Object[0]));
                        }
                        cryptoMode = 3;
                        lengthValue = 256;
                    }
                    final PdfObject em3 = dic2.get(PdfName.ENCRYPTMETADATA);
                    if (em3 != null && em3.toString().equals("false")) {
                        cryptoMode |= 0x8;
                    }
                    recipients = (PdfArray)dic2.get(PdfName.RECIPIENTS);
                    break;
                }
                default: {
                    throw new UnsupportedPdfException(MessageLocalization.getComposedMessage("unknown.encryption.type.v.eq.1", vValue));
                }
            }
            X509CertificateHolder certHolder;
            try {
                certHolder = new X509CertificateHolder(this.certificate.getEncoded());
            }
            catch (Exception f) {
                throw new ExceptionConverter(f);
            }
            if (this.externalDecryptionProcess == null) {
                for (int i = 0; i < recipients.size(); ++i) {
                    final PdfObject recipient = recipients.getPdfObject(i);
                    this.strings.remove(recipient);
                    CMSEnvelopedData data = null;
                    try {
                        data = new CMSEnvelopedData(recipient.getBytes());
                        for (final RecipientInformation recipientInfo : data.getRecipientInfos().getRecipients()) {
                            if (recipientInfo.getRID().match(certHolder) && !foundRecipient) {
                                envelopedData = PdfEncryptor.getContent(recipientInfo, (PrivateKey)this.certificateKey, this.certificateKeyProvider);
                                foundRecipient = true;
                            }
                        }
                    }
                    catch (Exception f2) {
                        throw new ExceptionConverter(f2);
                    }
                }
            }
            else {
                for (int i = 0; i < recipients.size(); ++i) {
                    final PdfObject recipient = recipients.getPdfObject(i);
                    this.strings.remove(recipient);
                    CMSEnvelopedData data = null;
                    try {
                        data = new CMSEnvelopedData(recipient.getBytes());
                        final RecipientInformation recipientInfo2 = data.getRecipientInfos().get(this.externalDecryptionProcess.getCmsRecipientId());
                        if (recipientInfo2 != null) {
                            envelopedData = recipientInfo2.getContent(this.externalDecryptionProcess.getCmsRecipient());
                            foundRecipient = true;
                        }
                    }
                    catch (Exception f2) {
                        throw new ExceptionConverter(f2);
                    }
                }
            }
            if (!foundRecipient || envelopedData == null) {
                throw new UnsupportedPdfException(MessageLocalization.getComposedMessage("bad.certificate.and.key", new Object[0]));
            }
            MessageDigest md = null;
            try {
                if ((cryptoMode & 0x7) == 0x3) {
                    md = MessageDigest.getInstance("SHA-256");
                }
                else {
                    md = MessageDigest.getInstance("SHA-1");
                }
                md.update(envelopedData, 0, 20);
                for (int j = 0; j < recipients.size(); ++j) {
                    final byte[] encodedRecipient = recipients.getPdfObject(j).getBytes();
                    md.update(encodedRecipient);
                }
                if ((cryptoMode & 0x8) != 0x0) {
                    md.update(new byte[] { -1, -1, -1, -1 });
                }
                encryptionKey = md.digest();
            }
            catch (Exception f3) {
                throw new ExceptionConverter(f3);
            }
        }
        (this.decrypt = new PdfEncryption()).setCryptoMode(cryptoMode, lengthValue);
        if (filter.equals(PdfName.STANDARD)) {
            if (this.rValue == 5) {
                this.ownerPasswordUsed = this.decrypt.readKey(enc, this.password);
                this.decrypt.documentID = documentID;
                this.pValue = this.decrypt.getPermissions();
            }
            else {
                this.decrypt.setupByOwnerPassword(documentID, this.password, uValue, oValue, this.pValue);
                if (!this.equalsArray(uValue, this.decrypt.userKey, (this.rValue == 3 || this.rValue == 4) ? 16 : 32)) {
                    this.decrypt.setupByUserPassword(documentID, this.password, oValue, this.pValue);
                    if (!this.equalsArray(uValue, this.decrypt.userKey, (this.rValue == 3 || this.rValue == 4) ? 16 : 32)) {
                        throw new BadPasswordException(MessageLocalization.getComposedMessage("bad.user.password", new Object[0]));
                    }
                }
                else {
                    this.ownerPasswordUsed = true;
                }
            }
        }
        else if (filter.equals(PdfName.PUBSEC)) {
            this.decrypt.documentID = documentID;
            if ((cryptoMode & 0x7) == 0x3) {
                this.decrypt.setKey(encryptionKey);
            }
            else {
                this.decrypt.setupByEncryptionKey(encryptionKey, lengthValue);
            }
            this.ownerPasswordUsed = true;
        }
        for (int k = 0; k < this.strings.size(); ++k) {
            final PdfString str = this.strings.get(k);
            str.decrypt(this);
        }
        if (encDic.isIndirect()) {
            this.cryptoRef = (PRIndirectReference)encDic;
            this.xrefObj.set(this.cryptoRef.getNumber(), null);
        }
        this.encryptionError = false;
    }
    
    public static PdfObject getPdfObjectRelease(final PdfObject obj) {
        final PdfObject obj2 = getPdfObject(obj);
        releaseLastXrefPartial(obj);
        return obj2;
    }
    
    public static PdfObject getPdfObject(PdfObject obj) {
        if (obj == null) {
            return null;
        }
        if (!obj.isIndirect()) {
            return obj;
        }
        try {
            final PRIndirectReference ref = (PRIndirectReference)obj;
            final int idx = ref.getNumber();
            final boolean appendable = ref.getReader().appendable;
            obj = ref.getReader().getPdfObject(idx);
            if (obj == null) {
                return null;
            }
            if (appendable) {
                switch (obj.type()) {
                    case 8: {
                        obj = new PdfNull();
                        break;
                    }
                    case 1: {
                        obj = new PdfBoolean(((PdfBoolean)obj).booleanValue());
                        break;
                    }
                    case 4: {
                        obj = new PdfName(obj.getBytes());
                        break;
                    }
                }
                obj.setIndRef(ref);
            }
            return obj;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public static PdfObject getPdfObjectRelease(final PdfObject obj, final PdfObject parent) {
        final PdfObject obj2 = getPdfObject(obj, parent);
        releaseLastXrefPartial(obj);
        return obj2;
    }
    
    public static PdfObject getPdfObject(PdfObject obj, final PdfObject parent) {
        if (obj == null) {
            return null;
        }
        if (!obj.isIndirect()) {
            PRIndirectReference ref = null;
            if (parent != null && (ref = parent.getIndRef()) != null && ref.getReader().isAppendable()) {
                switch (obj.type()) {
                    case 8: {
                        obj = new PdfNull();
                        break;
                    }
                    case 1: {
                        obj = new PdfBoolean(((PdfBoolean)obj).booleanValue());
                        break;
                    }
                    case 4: {
                        obj = new PdfName(obj.getBytes());
                        break;
                    }
                }
                obj.setIndRef(ref);
            }
            return obj;
        }
        return getPdfObject(obj);
    }
    
    public PdfObject getPdfObjectRelease(final int idx) {
        final PdfObject obj = this.getPdfObject(idx);
        this.releaseLastXrefPartial();
        return obj;
    }
    
    public PdfObject getPdfObject(final int idx) {
        try {
            this.lastXrefPartial = -1;
            if (idx < 0 || idx >= this.xrefObj.size()) {
                return null;
            }
            PdfObject obj = this.xrefObj.get(idx);
            if (!this.partial || obj != null) {
                return obj;
            }
            if (idx * 2 >= this.xref.length) {
                return null;
            }
            obj = this.readSingleObject(idx);
            this.lastXrefPartial = -1;
            if (obj != null) {
                this.lastXrefPartial = idx;
            }
            return obj;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public void resetLastXrefPartial() {
        this.lastXrefPartial = -1;
    }
    
    public void releaseLastXrefPartial() {
        if (this.partial && this.lastXrefPartial != -1) {
            this.xrefObj.set(this.lastXrefPartial, null);
            this.lastXrefPartial = -1;
        }
    }
    
    public static void releaseLastXrefPartial(final PdfObject obj) {
        if (obj == null) {
            return;
        }
        if (!obj.isIndirect()) {
            return;
        }
        if (!(obj instanceof PRIndirectReference)) {
            return;
        }
        final PRIndirectReference ref = (PRIndirectReference)obj;
        final PdfReader reader = ref.getReader();
        if (reader.partial && reader.lastXrefPartial != -1 && reader.lastXrefPartial == ref.getNumber()) {
            reader.xrefObj.set(reader.lastXrefPartial, null);
        }
        reader.lastXrefPartial = -1;
    }
    
    private void setXrefPartialObject(final int idx, final PdfObject obj) {
        if (!this.partial || idx < 0) {
            return;
        }
        this.xrefObj.set(idx, obj);
    }
    
    public PRIndirectReference addPdfObject(final PdfObject obj) {
        this.xrefObj.add(obj);
        return new PRIndirectReference(this, this.xrefObj.size() - 1);
    }
    
    protected void readPages() throws IOException {
        this.catalog = this.trailer.getAsDict(PdfName.ROOT);
        if (this.catalog == null) {
            throw new InvalidPdfException(MessageLocalization.getComposedMessage("the.document.has.no.catalog.object", new Object[0]));
        }
        this.rootPages = this.catalog.getAsDict(PdfName.PAGES);
        if (this.rootPages == null || (!PdfName.PAGES.equals(this.rootPages.get(PdfName.TYPE)) && !PdfName.PAGES.equals(this.rootPages.get(new PdfName("Types"))))) {
            if (!PdfReader.debugmode) {
                throw new InvalidPdfException(MessageLocalization.getComposedMessage("the.document.has.no.page.root", new Object[0]));
            }
            if (PdfReader.LOGGER.isLogging(Level.ERROR)) {
                PdfReader.LOGGER.error(MessageLocalization.getComposedMessage("the.document.has.no.page.root", new Object[0]));
            }
        }
        this.pageRefs = new PageRefs(this);
    }
    
    protected void readDocObjPartial() throws IOException {
        (this.xrefObj = new ArrayList<PdfObject>(this.xref.length / 2)).addAll((Collection<? extends PdfObject>)Collections.nCopies(this.xref.length / 2, (Object)null));
        this.readDecryptedDocObj();
        if (this.objStmToOffset != null) {
            final long[] keys = this.objStmToOffset.getKeys();
            for (int k = 0; k < keys.length; ++k) {
                final long n = keys[k];
                this.objStmToOffset.put(n, this.xref[(int)(n * 2L)]);
                this.xref[(int)(n * 2L)] = -1L;
            }
        }
    }
    
    protected PdfObject readSingleObject(final int k) throws IOException {
        this.strings.clear();
        final int k2 = k * 2;
        long pos = this.xref[k2];
        if (pos < 0L) {
            return null;
        }
        if (this.xref[k2 + 1] > 0L) {
            pos = this.objStmToOffset.get(this.xref[k2 + 1]);
        }
        if (pos == 0L) {
            return null;
        }
        this.tokens.seek(pos);
        this.tokens.nextValidToken();
        if (this.tokens.getTokenType() != PRTokeniser.TokenType.NUMBER) {
            this.tokens.throwError(MessageLocalization.getComposedMessage("invalid.object.number", new Object[0]));
        }
        this.objNum = this.tokens.intValue();
        this.tokens.nextValidToken();
        if (this.tokens.getTokenType() != PRTokeniser.TokenType.NUMBER) {
            this.tokens.throwError(MessageLocalization.getComposedMessage("invalid.generation.number", new Object[0]));
        }
        this.objGen = this.tokens.intValue();
        this.tokens.nextValidToken();
        if (!this.tokens.getStringValue().equals("obj")) {
            this.tokens.throwError(MessageLocalization.getComposedMessage("token.obj.expected", new Object[0]));
        }
        PdfObject obj;
        try {
            obj = this.readPRObject();
            for (int j = 0; j < this.strings.size(); ++j) {
                final PdfString str = this.strings.get(j);
                str.decrypt(this);
            }
            if (obj.isStream()) {
                this.checkPRStreamLength((PRStream)obj);
            }
        }
        catch (IOException e) {
            if (!PdfReader.debugmode) {
                throw e;
            }
            if (PdfReader.LOGGER.isLogging(Level.ERROR)) {
                PdfReader.LOGGER.error(e.getMessage(), e);
            }
            obj = null;
        }
        if (this.xref[k2 + 1] > 0L) {
            obj = this.readOneObjStm((PRStream)obj, (int)this.xref[k2]);
        }
        this.xrefObj.set(k, obj);
        return obj;
    }
    
    protected PdfObject readOneObjStm(final PRStream stream, int idx) throws IOException {
        final int first = stream.getAsNumber(PdfName.FIRST).intValue();
        final byte[] b = getStreamBytes(stream, this.tokens.getFile());
        final PRTokeniser saveTokens = this.tokens;
        this.tokens = new PRTokeniser(new RandomAccessFileOrArray(new RandomAccessSourceFactory().createSource(b)));
        try {
            int address = 0;
            boolean ok = true;
            ++idx;
            for (int k = 0; k < idx; ++k) {
                ok = this.tokens.nextToken();
                if (!ok) {
                    break;
                }
                if (this.tokens.getTokenType() != PRTokeniser.TokenType.NUMBER) {
                    ok = false;
                    break;
                }
                ok = this.tokens.nextToken();
                if (!ok) {
                    break;
                }
                if (this.tokens.getTokenType() != PRTokeniser.TokenType.NUMBER) {
                    ok = false;
                    break;
                }
                address = this.tokens.intValue() + first;
            }
            if (!ok) {
                throw new InvalidPdfException(MessageLocalization.getComposedMessage("error.reading.objstm", new Object[0]));
            }
            this.tokens.seek(address);
            this.tokens.nextToken();
            PdfObject obj;
            if (this.tokens.getTokenType() == PRTokeniser.TokenType.NUMBER) {
                obj = new PdfNumber(this.tokens.getStringValue());
            }
            else {
                this.tokens.seek(address);
                obj = this.readPRObject();
            }
            return obj;
        }
        finally {
            this.tokens = saveTokens;
        }
    }
    
    public double dumpPerc() {
        int total = 0;
        for (int k = 0; k < this.xrefObj.size(); ++k) {
            if (this.xrefObj.get(k) != null) {
                ++total;
            }
        }
        return total * 100.0 / this.xrefObj.size();
    }
    
    protected void readDocObj() throws IOException {
        final ArrayList<PRStream> streams = new ArrayList<PRStream>();
        (this.xrefObj = new ArrayList<PdfObject>(this.xref.length / 2)).addAll((Collection<? extends PdfObject>)Collections.nCopies(this.xref.length / 2, (Object)null));
        for (int k = 2; k < this.xref.length; k += 2) {
            final long pos = this.xref[k];
            if (pos > 0L) {
                if (this.xref[k + 1] <= 0L) {
                    this.tokens.seek(pos);
                    this.tokens.nextValidToken();
                    if (this.tokens.getTokenType() != PRTokeniser.TokenType.NUMBER) {
                        this.tokens.throwError(MessageLocalization.getComposedMessage("invalid.object.number", new Object[0]));
                    }
                    this.objNum = this.tokens.intValue();
                    this.tokens.nextValidToken();
                    if (this.tokens.getTokenType() != PRTokeniser.TokenType.NUMBER) {
                        this.tokens.throwError(MessageLocalization.getComposedMessage("invalid.generation.number", new Object[0]));
                    }
                    this.objGen = this.tokens.intValue();
                    this.tokens.nextValidToken();
                    if (!this.tokens.getStringValue().equals("obj")) {
                        this.tokens.throwError(MessageLocalization.getComposedMessage("token.obj.expected", new Object[0]));
                    }
                    PdfObject obj;
                    try {
                        obj = this.readPRObject();
                        if (obj.isStream()) {
                            streams.add((PRStream)obj);
                        }
                    }
                    catch (IOException e) {
                        if (!PdfReader.debugmode) {
                            throw e;
                        }
                        if (PdfReader.LOGGER.isLogging(Level.ERROR)) {
                            PdfReader.LOGGER.error(e.getMessage(), e);
                        }
                        obj = null;
                    }
                    this.xrefObj.set(k / 2, obj);
                }
            }
        }
        for (int k = 0; k < streams.size(); ++k) {
            this.checkPRStreamLength(streams.get(k));
        }
        this.readDecryptedDocObj();
        if (this.objStmMark != null) {
            for (final Map.Entry<Integer, IntHashtable> entry : this.objStmMark.entrySet()) {
                final int n = entry.getKey();
                final IntHashtable h = entry.getValue();
                this.readObjStm(this.xrefObj.get(n), h);
                this.xrefObj.set(n, null);
            }
            this.objStmMark = null;
        }
        this.xref = null;
    }
    
    private void checkPRStreamLength(final PRStream stream) throws IOException {
        final long fileLength = this.tokens.length();
        final long start = stream.getOffset();
        boolean calc = false;
        long streamLength = 0L;
        final PdfObject obj = getPdfObjectRelease(stream.get(PdfName.LENGTH));
        if (obj != null && obj.type() == 2) {
            streamLength = ((PdfNumber)obj).intValue();
            if (streamLength + start > fileLength - 20L) {
                calc = true;
            }
            else {
                this.tokens.seek(start + streamLength);
                final String line = this.tokens.readString(20);
                if (!line.startsWith("\nendstream") && !line.startsWith("\r\nendstream") && !line.startsWith("\rendstream") && !line.startsWith("endstream")) {
                    calc = true;
                }
            }
        }
        else {
            calc = true;
        }
        Label_0361: {
            if (calc) {
                final byte[] tline = new byte[16];
                this.tokens.seek(start);
                while (true) {
                    long pos;
                    do {
                        pos = this.tokens.getFilePointer();
                        if (this.tokens.readLineSegment(tline, false)) {
                            if (!equalsn(tline, PdfReader.endstream)) {
                                continue;
                            }
                            streamLength = pos - start;
                        }
                        this.tokens.seek(pos - 2L);
                        if (this.tokens.read() == 13) {
                            --streamLength;
                        }
                        this.tokens.seek(pos - 1L);
                        if (this.tokens.read() == 10) {
                            --streamLength;
                        }
                        if (streamLength < 0L) {
                            streamLength = 0L;
                        }
                        break Label_0361;
                    } while (!equalsn(tline, PdfReader.endobj));
                    this.tokens.seek(pos - 16L);
                    final String s = this.tokens.readString(16);
                    final int index = s.indexOf("endstream");
                    if (index >= 0) {
                        pos = pos - 16L + index;
                    }
                    streamLength = pos - start;
                    continue;
                }
            }
        }
        stream.setLength((int)streamLength);
    }
    
    protected void readObjStm(final PRStream stream, final IntHashtable map) throws IOException {
        if (stream == null) {
            return;
        }
        final int first = stream.getAsNumber(PdfName.FIRST).intValue();
        final int n = stream.getAsNumber(PdfName.N).intValue();
        final byte[] b = getStreamBytes(stream, this.tokens.getFile());
        final PRTokeniser saveTokens = this.tokens;
        this.tokens = new PRTokeniser(new RandomAccessFileOrArray(new RandomAccessSourceFactory().createSource(b)));
        try {
            final int[] address = new int[n];
            final int[] objNumber = new int[n];
            boolean ok = true;
            for (int k = 0; k < n; ++k) {
                ok = this.tokens.nextToken();
                if (!ok) {
                    break;
                }
                if (this.tokens.getTokenType() != PRTokeniser.TokenType.NUMBER) {
                    ok = false;
                    break;
                }
                objNumber[k] = this.tokens.intValue();
                ok = this.tokens.nextToken();
                if (!ok) {
                    break;
                }
                if (this.tokens.getTokenType() != PRTokeniser.TokenType.NUMBER) {
                    ok = false;
                    break;
                }
                address[k] = this.tokens.intValue() + first;
            }
            if (!ok) {
                throw new InvalidPdfException(MessageLocalization.getComposedMessage("error.reading.objstm", new Object[0]));
            }
            for (int k = 0; k < n; ++k) {
                if (map.containsKey(k)) {
                    this.tokens.seek(address[k]);
                    this.tokens.nextToken();
                    PdfObject obj;
                    if (this.tokens.getTokenType() == PRTokeniser.TokenType.NUMBER) {
                        obj = new PdfNumber(this.tokens.getStringValue());
                    }
                    else {
                        this.tokens.seek(address[k]);
                        obj = this.readPRObject();
                    }
                    this.xrefObj.set(objNumber[k], obj);
                }
            }
        }
        finally {
            this.tokens = saveTokens;
        }
    }
    
    public static PdfObject killIndirect(final PdfObject obj) {
        if (obj == null || obj.isNull()) {
            return null;
        }
        final PdfObject ret = getPdfObjectRelease(obj);
        if (obj.isIndirect()) {
            final PRIndirectReference ref = (PRIndirectReference)obj;
            final PdfReader reader = ref.getReader();
            final int n = ref.getNumber();
            reader.xrefObj.set(n, null);
            if (reader.partial) {
                reader.xref[n * 2] = -1L;
            }
        }
        return ret;
    }
    
    private void ensureXrefSize(final int size) {
        if (size == 0) {
            return;
        }
        if (this.xref == null) {
            this.xref = new long[size];
        }
        else if (this.xref.length < size) {
            final long[] xref2 = new long[size];
            System.arraycopy(this.xref, 0, xref2, 0, this.xref.length);
            this.xref = xref2;
        }
    }
    
    protected void readXref() throws IOException {
        this.hybridXref = false;
        this.newXrefType = false;
        this.tokens.seek(this.tokens.getStartxref());
        this.tokens.nextToken();
        if (!this.tokens.getStringValue().equals("startxref")) {
            throw new InvalidPdfException(MessageLocalization.getComposedMessage("startxref.not.found", new Object[0]));
        }
        this.tokens.nextToken();
        if (this.tokens.getTokenType() != PRTokeniser.TokenType.NUMBER) {
            throw new InvalidPdfException(MessageLocalization.getComposedMessage("startxref.is.not.followed.by.a.number", new Object[0]));
        }
        long startxref = this.tokens.longValue();
        this.lastXref = startxref;
        this.eofPos = this.tokens.getFilePointer();
        try {
            if (this.readXRefStream(startxref)) {
                this.newXrefType = true;
                return;
            }
        }
        catch (Exception ex) {}
        this.xref = null;
        this.tokens.seek(startxref);
        this.trailer = this.readXrefSection();
        PdfDictionary trailer2 = this.trailer;
        while (true) {
            final PdfNumber prev = (PdfNumber)trailer2.get(PdfName.PREV);
            if (prev == null) {
                return;
            }
            if (prev.longValue() == startxref) {
                throw new InvalidPdfException(MessageLocalization.getComposedMessage("trailer.prev.entry.points.to.its.own.cross.reference.section", new Object[0]));
            }
            startxref = prev.longValue();
            this.tokens.seek(startxref);
            trailer2 = this.readXrefSection();
        }
    }
    
    protected PdfDictionary readXrefSection() throws IOException {
        this.tokens.nextValidToken();
        if (!this.tokens.getStringValue().equals("xref")) {
            this.tokens.throwError(MessageLocalization.getComposedMessage("xref.subsection.not.found", new Object[0]));
        }
        int start = 0;
        int end = 0;
        long pos = 0L;
        int gen = 0;
        while (true) {
            this.tokens.nextValidToken();
            if (this.tokens.getStringValue().equals("trailer")) {
                break;
            }
            if (this.tokens.getTokenType() != PRTokeniser.TokenType.NUMBER) {
                this.tokens.throwError(MessageLocalization.getComposedMessage("object.number.of.the.first.object.in.this.xref.subsection.not.found", new Object[0]));
            }
            start = this.tokens.intValue();
            this.tokens.nextValidToken();
            if (this.tokens.getTokenType() != PRTokeniser.TokenType.NUMBER) {
                this.tokens.throwError(MessageLocalization.getComposedMessage("number.of.entries.in.this.xref.subsection.not.found", new Object[0]));
            }
            end = this.tokens.intValue() + start;
            if (start == 1) {
                final long back = this.tokens.getFilePointer();
                this.tokens.nextValidToken();
                pos = this.tokens.longValue();
                this.tokens.nextValidToken();
                gen = this.tokens.intValue();
                if (pos == 0L && gen == 65535) {
                    --start;
                    --end;
                }
                this.tokens.seek(back);
            }
            this.ensureXrefSize(end * 2);
            for (int k = start; k < end; ++k) {
                this.tokens.nextValidToken();
                pos = this.tokens.longValue();
                this.tokens.nextValidToken();
                gen = this.tokens.intValue();
                this.tokens.nextValidToken();
                final int p = k * 2;
                if (this.tokens.getStringValue().equals("n")) {
                    if (this.xref[p] == 0L && this.xref[p + 1] == 0L) {
                        this.xref[p] = pos;
                    }
                }
                else if (this.tokens.getStringValue().equals("f")) {
                    if (this.xref[p] == 0L && this.xref[p + 1] == 0L) {
                        this.xref[p] = -1L;
                    }
                }
                else {
                    this.tokens.throwError(MessageLocalization.getComposedMessage("invalid.cross.reference.entry.in.this.xref.subsection", new Object[0]));
                }
            }
        }
        final PdfDictionary trailer = (PdfDictionary)this.readPRObject();
        final PdfNumber xrefSize = (PdfNumber)trailer.get(PdfName.SIZE);
        this.ensureXrefSize(xrefSize.intValue() * 2);
        final PdfObject xrs = trailer.get(PdfName.XREFSTM);
        if (xrs != null && xrs.isNumber()) {
            final int loc = ((PdfNumber)xrs).intValue();
            try {
                this.readXRefStream(loc);
                this.newXrefType = true;
                this.hybridXref = true;
            }
            catch (IOException e) {
                this.xref = null;
                throw e;
            }
        }
        return trailer;
    }
    
    protected boolean readXRefStream(final long ptr) throws IOException {
        this.tokens.seek(ptr);
        int thisStream = 0;
        if (!this.tokens.nextToken()) {
            return false;
        }
        if (this.tokens.getTokenType() != PRTokeniser.TokenType.NUMBER) {
            return false;
        }
        thisStream = this.tokens.intValue();
        if (!this.tokens.nextToken() || this.tokens.getTokenType() != PRTokeniser.TokenType.NUMBER) {
            return false;
        }
        if (!this.tokens.nextToken() || !this.tokens.getStringValue().equals("obj")) {
            return false;
        }
        final PdfObject object = this.readPRObject();
        PRStream stm = null;
        if (!object.isStream()) {
            return false;
        }
        stm = (PRStream)object;
        if (!PdfName.XREF.equals(stm.get(PdfName.TYPE))) {
            return false;
        }
        if (this.trailer == null) {
            (this.trailer = new PdfDictionary()).putAll(stm);
        }
        stm.setLength(((PdfNumber)stm.get(PdfName.LENGTH)).intValue());
        final int size = ((PdfNumber)stm.get(PdfName.SIZE)).intValue();
        PdfObject obj = stm.get(PdfName.INDEX);
        PdfArray index;
        if (obj == null) {
            index = new PdfArray();
            index.add(new int[] { 0, size });
        }
        else {
            index = (PdfArray)obj;
        }
        final PdfArray w = (PdfArray)stm.get(PdfName.W);
        long prev = -1L;
        obj = stm.get(PdfName.PREV);
        if (obj != null) {
            prev = ((PdfNumber)obj).longValue();
        }
        this.ensureXrefSize(size * 2);
        if (this.objStmMark == null && !this.partial) {
            this.objStmMark = new HashMap<Integer, IntHashtable>();
        }
        if (this.objStmToOffset == null && this.partial) {
            this.objStmToOffset = new LongHashtable();
        }
        final byte[] b = getStreamBytes(stm, this.tokens.getFile());
        int bptr = 0;
        final int[] wc = new int[3];
        for (int k = 0; k < 3; ++k) {
            wc[k] = w.getAsNumber(k).intValue();
        }
        for (int idx = 0; idx < index.size(); idx += 2) {
            int start = index.getAsNumber(idx).intValue();
            int length = index.getAsNumber(idx + 1).intValue();
            this.ensureXrefSize((start + length) * 2);
            while (length-- > 0) {
                int type = 1;
                if (wc[0] > 0) {
                    type = 0;
                    for (int i = 0; i < wc[0]; ++i) {
                        type = (type << 8) + (b[bptr++] & 0xFF);
                    }
                }
                long field2 = 0L;
                for (int j = 0; j < wc[1]; ++j) {
                    field2 = (field2 << 8) + (b[bptr++] & 0xFF);
                }
                int field3 = 0;
                for (int l = 0; l < wc[2]; ++l) {
                    field3 = (field3 << 8) + (b[bptr++] & 0xFF);
                }
                final int base = start * 2;
                if (this.xref[base] == 0L && this.xref[base + 1] == 0L) {
                    switch (type) {
                        case 0: {
                            this.xref[base] = -1L;
                            break;
                        }
                        case 1: {
                            this.xref[base] = field2;
                            break;
                        }
                        case 2: {
                            this.xref[base] = field3;
                            this.xref[base + 1] = field2;
                            if (this.partial) {
                                this.objStmToOffset.put(field2, 0L);
                                break;
                            }
                            final Integer on = (int)field2;
                            IntHashtable seq = this.objStmMark.get(on);
                            if (seq == null) {
                                seq = new IntHashtable();
                                seq.put(field3, 1);
                                this.objStmMark.put(on, seq);
                                break;
                            }
                            seq.put(field3, 1);
                            break;
                        }
                    }
                }
                ++start;
            }
        }
        thisStream *= 2;
        if (thisStream + 1 < this.xref.length && this.xref[thisStream] == 0L && this.xref[thisStream + 1] == 0L) {
            this.xref[thisStream] = -1L;
        }
        return prev == -1L || this.readXRefStream(prev);
    }
    
    protected void rebuildXref() throws IOException {
        this.hybridXref = false;
        this.newXrefType = false;
        this.tokens.seek(0L);
        long[][] xr = new long[1024][];
        long top = 0L;
        this.trailer = null;
        final byte[] line = new byte[64];
        while (true) {
            long pos = this.tokens.getFilePointer();
            if (!this.tokens.readLineSegment(line, true)) {
                break;
            }
            if (line[0] == 116) {
                if (!PdfEncodings.convertToString(line, null).startsWith("trailer")) {
                    continue;
                }
                this.tokens.seek(pos);
                this.tokens.nextToken();
                pos = this.tokens.getFilePointer();
                try {
                    final PdfDictionary dic = (PdfDictionary)this.readPRObject();
                    if (dic.get(PdfName.ROOT) != null) {
                        this.trailer = dic;
                    }
                    else {
                        this.tokens.seek(pos);
                    }
                }
                catch (Exception e) {
                    this.tokens.seek(pos);
                }
            }
            else {
                if (line[0] < 48 || line[0] > 57) {
                    continue;
                }
                final long[] obj = PRTokeniser.checkObjectStart(line);
                if (obj == null) {
                    continue;
                }
                final long num = obj[0];
                final long gen = obj[1];
                if (num >= xr.length) {
                    final long newLength = num * 2L;
                    final long[][] xr2 = new long[(int)newLength][];
                    System.arraycopy(xr, 0, xr2, 0, (int)top);
                    xr = xr2;
                }
                if (num >= top) {
                    top = num + 1L;
                }
                if (xr[(int)num] != null && gen < xr[(int)num][1]) {
                    continue;
                }
                obj[0] = pos;
                xr[(int)num] = obj;
            }
        }
        if (this.trailer == null) {
            throw new InvalidPdfException(MessageLocalization.getComposedMessage("trailer.not.found", new Object[0]));
        }
        this.xref = new long[(int)(top * 2L)];
        for (int k = 0; k < top; ++k) {
            final long[] obj2 = xr[k];
            if (obj2 != null) {
                this.xref[k * 2] = obj2[0];
            }
        }
    }
    
    protected PdfDictionary readDictionary() throws IOException {
        final PdfDictionary dic = new PdfDictionary();
        while (true) {
            this.tokens.nextValidToken();
            if (this.tokens.getTokenType() == PRTokeniser.TokenType.END_DIC) {
                break;
            }
            if (this.tokens.getTokenType() != PRTokeniser.TokenType.NAME) {
                this.tokens.throwError(MessageLocalization.getComposedMessage("dictionary.key.1.is.not.a.name", this.tokens.getStringValue()));
            }
            final PdfName name = new PdfName(this.tokens.getStringValue(), false);
            final PdfObject obj = this.readPRObject();
            final int type = obj.type();
            if (-type == PRTokeniser.TokenType.END_DIC.ordinal()) {
                this.tokens.throwError(MessageLocalization.getComposedMessage("unexpected.gt.gt", new Object[0]));
            }
            if (-type == PRTokeniser.TokenType.END_ARRAY.ordinal()) {
                this.tokens.throwError(MessageLocalization.getComposedMessage("unexpected.close.bracket", new Object[0]));
            }
            dic.put(name, obj);
        }
        return dic;
    }
    
    protected PdfArray readArray() throws IOException {
        final PdfArray array = new PdfArray();
        while (true) {
            final PdfObject obj = this.readPRObject();
            final int type = obj.type();
            if (-type == PRTokeniser.TokenType.END_ARRAY.ordinal()) {
                break;
            }
            if (-type == PRTokeniser.TokenType.END_DIC.ordinal()) {
                this.tokens.throwError(MessageLocalization.getComposedMessage("unexpected.gt.gt", new Object[0]));
            }
            array.add(obj);
        }
        return array;
    }
    
    protected PdfObject readPRObject() throws IOException {
        this.tokens.nextValidToken();
        final PRTokeniser.TokenType type = this.tokens.getTokenType();
        switch (type) {
            case START_DIC: {
                ++this.readDepth;
                final PdfDictionary dic = this.readDictionary();
                --this.readDepth;
                final long pos = this.tokens.getFilePointer();
                boolean hasNext;
                do {
                    hasNext = this.tokens.nextToken();
                } while (hasNext && this.tokens.getTokenType() == PRTokeniser.TokenType.COMMENT);
                if (hasNext && this.tokens.getStringValue().equals("stream")) {
                    int ch;
                    do {
                        ch = this.tokens.read();
                    } while (ch == 32 || ch == 9 || ch == 0 || ch == 12);
                    if (ch != 10) {
                        ch = this.tokens.read();
                    }
                    if (ch != 10) {
                        this.tokens.backOnePosition(ch);
                    }
                    final PRStream stream = new PRStream(this, this.tokens.getFilePointer());
                    stream.putAll(dic);
                    stream.setObjNum(this.objNum, this.objGen);
                    return stream;
                }
                this.tokens.seek(pos);
                return dic;
            }
            case START_ARRAY: {
                ++this.readDepth;
                final PdfArray arr = this.readArray();
                --this.readDepth;
                return arr;
            }
            case NUMBER: {
                return new PdfNumber(this.tokens.getStringValue());
            }
            case STRING: {
                final PdfString str = new PdfString(this.tokens.getStringValue(), null).setHexWriting(this.tokens.isHexString());
                str.setObjNum(this.objNum, this.objGen);
                if (this.strings != null) {
                    this.strings.add(str);
                }
                return str;
            }
            case NAME: {
                final PdfName cachedName = PdfName.staticNames.get(this.tokens.getStringValue());
                if (this.readDepth > 0 && cachedName != null) {
                    return cachedName;
                }
                return new PdfName(this.tokens.getStringValue(), false);
            }
            case REF: {
                final int num = this.tokens.getReference();
                final PRIndirectReference ref = new PRIndirectReference(this, num, this.tokens.getGeneration());
                return ref;
            }
            case ENDOFFILE: {
                throw new IOException(MessageLocalization.getComposedMessage("unexpected.end.of.file", new Object[0]));
            }
            default: {
                final String sv = this.tokens.getStringValue();
                if ("null".equals(sv)) {
                    if (this.readDepth == 0) {
                        return new PdfNull();
                    }
                    return PdfNull.PDFNULL;
                }
                else if ("true".equals(sv)) {
                    if (this.readDepth == 0) {
                        return new PdfBoolean(true);
                    }
                    return PdfBoolean.PDFTRUE;
                }
                else {
                    if (!"false".equals(sv)) {
                        return new PdfLiteral(-type.ordinal(), this.tokens.getStringValue());
                    }
                    if (this.readDepth == 0) {
                        return new PdfBoolean(false);
                    }
                    return PdfBoolean.PDFFALSE;
                }
                break;
            }
        }
    }
    
    public static byte[] FlateDecode(final byte[] in) {
        final byte[] b = FlateDecode(in, true);
        if (b == null) {
            return FlateDecode(in, false);
        }
        return b;
    }
    
    public static byte[] decodePredictor(final byte[] in, final PdfObject dicPar) {
        if (dicPar == null || !dicPar.isDictionary()) {
            return in;
        }
        final PdfDictionary dic = (PdfDictionary)dicPar;
        PdfObject obj = getPdfObject(dic.get(PdfName.PREDICTOR));
        if (obj == null || !obj.isNumber()) {
            return in;
        }
        final int predictor = ((PdfNumber)obj).intValue();
        if (predictor < 10 && predictor != 2) {
            return in;
        }
        int width = 1;
        obj = getPdfObject(dic.get(PdfName.COLUMNS));
        if (obj != null && obj.isNumber()) {
            width = ((PdfNumber)obj).intValue();
        }
        int colors = 1;
        obj = getPdfObject(dic.get(PdfName.COLORS));
        if (obj != null && obj.isNumber()) {
            colors = ((PdfNumber)obj).intValue();
        }
        int bpc = 8;
        obj = getPdfObject(dic.get(PdfName.BITSPERCOMPONENT));
        if (obj != null && obj.isNumber()) {
            bpc = ((PdfNumber)obj).intValue();
        }
        final DataInputStream dataStream = new DataInputStream(new ByteArrayInputStream(in));
        final ByteArrayOutputStream fout = new ByteArrayOutputStream(in.length);
        final int bytesPerPixel = colors * bpc / 8;
        final int bytesPerRow = (colors * width * bpc + 7) / 8;
        byte[] curr = new byte[bytesPerRow];
        byte[] prior = new byte[bytesPerRow];
        if (predictor == 2) {
            if (bpc == 8) {
                for (int numRows = in.length / bytesPerRow, row = 0; row < numRows; ++row) {
                    final int rowStart = row * bytesPerRow;
                    for (int col = 0 + bytesPerPixel; col < bytesPerRow; ++col) {
                        in[rowStart + col] += in[rowStart + col - bytesPerPixel];
                    }
                }
            }
            return in;
        }
        while (true) {
            int filter = 0;
            try {
                filter = dataStream.read();
                if (filter < 0) {
                    return fout.toByteArray();
                }
                dataStream.readFully(curr, 0, bytesPerRow);
            }
            catch (Exception e) {
                return fout.toByteArray();
            }
            switch (filter) {
                case 0: {
                    break;
                }
                case 1: {
                    for (int i = bytesPerPixel; i < bytesPerRow; ++i) {
                        final byte[] array = curr;
                        final int n = i;
                        array[n] += curr[i - bytesPerPixel];
                    }
                    break;
                }
                case 2: {
                    for (int i = 0; i < bytesPerRow; ++i) {
                        final byte[] array2 = curr;
                        final int n2 = i;
                        array2[n2] += prior[i];
                    }
                    break;
                }
                case 3: {
                    for (int i = 0; i < bytesPerPixel; ++i) {
                        final byte[] array3 = curr;
                        final int n3 = i;
                        array3[n3] += (byte)(prior[i] / 2);
                    }
                    for (int i = bytesPerPixel; i < bytesPerRow; ++i) {
                        final byte[] array4 = curr;
                        final int n4 = i;
                        array4[n4] += (byte)(((curr[i - bytesPerPixel] & 0xFF) + (prior[i] & 0xFF)) / 2);
                    }
                    break;
                }
                case 4: {
                    for (int i = 0; i < bytesPerPixel; ++i) {
                        final byte[] array5 = curr;
                        final int n5 = i;
                        array5[n5] += prior[i];
                    }
                    for (int i = bytesPerPixel; i < bytesPerRow; ++i) {
                        final int a = curr[i - bytesPerPixel] & 0xFF;
                        final int b = prior[i] & 0xFF;
                        final int c = prior[i - bytesPerPixel] & 0xFF;
                        final int p = a + b - c;
                        final int pa = Math.abs(p - a);
                        final int pb = Math.abs(p - b);
                        final int pc = Math.abs(p - c);
                        int ret;
                        if (pa <= pb && pa <= pc) {
                            ret = a;
                        }
                        else if (pb <= pc) {
                            ret = b;
                        }
                        else {
                            ret = c;
                        }
                        final byte[] array6 = curr;
                        final int n6 = i;
                        array6[n6] += (byte)ret;
                    }
                    break;
                }
                default: {
                    throw new RuntimeException(MessageLocalization.getComposedMessage("png.filter.unknown", new Object[0]));
                }
            }
            try {
                fout.write(curr);
            }
            catch (IOException ex) {}
            final byte[] tmp = prior;
            prior = curr;
            curr = tmp;
        }
    }
    
    public static byte[] FlateDecode(final byte[] in, final boolean strict) {
        final ByteArrayInputStream stream = new ByteArrayInputStream(in);
        final InflaterInputStream zip = new InflaterInputStream(stream);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final byte[] b = new byte[strict ? 4092 : 1];
        try {
            int n;
            while ((n = zip.read(b)) >= 0) {
                out.write(b, 0, n);
            }
            zip.close();
            out.close();
            return out.toByteArray();
        }
        catch (Exception e) {
            if (strict) {
                return null;
            }
            return out.toByteArray();
        }
        finally {
            try {
                zip.close();
            }
            catch (IOException ex) {}
            try {
                out.close();
            }
            catch (IOException ex2) {}
        }
    }
    
    public static byte[] ASCIIHexDecode(final byte[] in) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        boolean first = true;
        int n1 = 0;
        for (int k = 0; k < in.length; ++k) {
            final int ch = in[k] & 0xFF;
            if (ch == 62) {
                break;
            }
            if (!PRTokeniser.isWhitespace(ch)) {
                final int n2 = PRTokeniser.getHex(ch);
                if (n2 == -1) {
                    throw new RuntimeException(MessageLocalization.getComposedMessage("illegal.character.in.asciihexdecode", new Object[0]));
                }
                if (first) {
                    n1 = n2;
                }
                else {
                    out.write((byte)((n1 << 4) + n2));
                }
                first = !first;
            }
        }
        if (!first) {
            out.write((byte)(n1 << 4));
        }
        return out.toByteArray();
    }
    
    public static byte[] ASCII85Decode(final byte[] in) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        int state = 0;
        final int[] chn = new int[5];
        for (int k = 0; k < in.length; ++k) {
            final int ch = in[k] & 0xFF;
            if (ch == 126) {
                break;
            }
            if (!PRTokeniser.isWhitespace(ch)) {
                if (ch == 122 && state == 0) {
                    out.write(0);
                    out.write(0);
                    out.write(0);
                    out.write(0);
                }
                else {
                    if (ch < 33 || ch > 117) {
                        throw new RuntimeException(MessageLocalization.getComposedMessage("illegal.character.in.ascii85decode", new Object[0]));
                    }
                    chn[state] = ch - 33;
                    if (++state == 5) {
                        state = 0;
                        int r = 0;
                        for (int j = 0; j < 5; ++j) {
                            r = r * 85 + chn[j];
                        }
                        out.write((byte)(r >> 24));
                        out.write((byte)(r >> 16));
                        out.write((byte)(r >> 8));
                        out.write((byte)r);
                    }
                }
            }
        }
        int r2 = 0;
        if (state == 2) {
            r2 = chn[0] * 85 * 85 * 85 * 85 + chn[1] * 85 * 85 * 85 + 614125 + 7225 + 85;
            out.write((byte)(r2 >> 24));
        }
        else if (state == 3) {
            r2 = chn[0] * 85 * 85 * 85 * 85 + chn[1] * 85 * 85 * 85 + chn[2] * 85 * 85 + 7225 + 85;
            out.write((byte)(r2 >> 24));
            out.write((byte)(r2 >> 16));
        }
        else if (state == 4) {
            r2 = chn[0] * 85 * 85 * 85 * 85 + chn[1] * 85 * 85 * 85 + chn[2] * 85 * 85 + chn[3] * 85 + 85;
            out.write((byte)(r2 >> 24));
            out.write((byte)(r2 >> 16));
            out.write((byte)(r2 >> 8));
        }
        return out.toByteArray();
    }
    
    public static byte[] LZWDecode(final byte[] in) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final LZWDecoder lzw = new LZWDecoder();
        lzw.decode(in, out);
        return out.toByteArray();
    }
    
    public boolean isRebuilt() {
        return this.rebuilt;
    }
    
    public PdfDictionary getPageN(final int pageNum) {
        final PdfDictionary dic = this.pageRefs.getPageN(pageNum);
        if (dic == null) {
            return null;
        }
        if (this.appendable) {
            dic.setIndRef(this.pageRefs.getPageOrigRef(pageNum));
        }
        return dic;
    }
    
    public PdfDictionary getPageNRelease(final int pageNum) {
        final PdfDictionary dic = this.getPageN(pageNum);
        this.pageRefs.releasePage(pageNum);
        return dic;
    }
    
    public void releasePage(final int pageNum) {
        this.pageRefs.releasePage(pageNum);
    }
    
    public void resetReleasePage() {
        this.pageRefs.resetReleasePage();
    }
    
    public PRIndirectReference getPageOrigRef(final int pageNum) {
        return this.pageRefs.getPageOrigRef(pageNum);
    }
    
    public byte[] getPageContent(final int pageNum, final RandomAccessFileOrArray file) throws IOException {
        final PdfDictionary page = this.getPageNRelease(pageNum);
        if (page == null) {
            return null;
        }
        final PdfObject contents = getPdfObjectRelease(page.get(PdfName.CONTENTS));
        if (contents == null) {
            return new byte[0];
        }
        ByteArrayOutputStream bout = null;
        if (contents.isStream()) {
            return getStreamBytes((PRStream)contents, file);
        }
        if (contents.isArray()) {
            final PdfArray array = (PdfArray)contents;
            bout = new ByteArrayOutputStream();
            for (int k = 0; k < array.size(); ++k) {
                final PdfObject item = getPdfObjectRelease(array.getPdfObject(k));
                if (item != null) {
                    if (item.isStream()) {
                        final byte[] b = getStreamBytes((PRStream)item, file);
                        bout.write(b);
                        if (k != array.size() - 1) {
                            bout.write(10);
                        }
                    }
                }
            }
            return bout.toByteArray();
        }
        return new byte[0];
    }
    
    public static byte[] getPageContent(final PdfDictionary page) throws IOException {
        if (page == null) {
            return null;
        }
        RandomAccessFileOrArray rf = null;
        try {
            final PdfObject contents = getPdfObjectRelease(page.get(PdfName.CONTENTS));
            if (contents == null) {
                return new byte[0];
            }
            if (contents.isStream()) {
                if (rf == null) {
                    rf = ((PRStream)contents).getReader().getSafeFile();
                    rf.reOpen();
                }
                return getStreamBytes((PRStream)contents, rf);
            }
            if (contents.isArray()) {
                final PdfArray array = (PdfArray)contents;
                final ByteArrayOutputStream bout = new ByteArrayOutputStream();
                for (int k = 0; k < array.size(); ++k) {
                    final PdfObject item = getPdfObjectRelease(array.getPdfObject(k));
                    if (item != null) {
                        if (item.isStream()) {
                            if (rf == null) {
                                rf = ((PRStream)item).getReader().getSafeFile();
                                rf.reOpen();
                            }
                            final byte[] b = getStreamBytes((PRStream)item, rf);
                            bout.write(b);
                            if (k != array.size() - 1) {
                                bout.write(10);
                            }
                        }
                    }
                }
                return bout.toByteArray();
            }
            return new byte[0];
        }
        finally {
            try {
                if (rf != null) {
                    rf.close();
                }
            }
            catch (Exception ex) {}
        }
    }
    
    public PdfDictionary getPageResources(final int pageNum) {
        return this.getPageResources(this.getPageN(pageNum));
    }
    
    public PdfDictionary getPageResources(final PdfDictionary pageDict) {
        return pageDict.getAsDict(PdfName.RESOURCES);
    }
    
    public byte[] getPageContent(final int pageNum) throws IOException {
        final RandomAccessFileOrArray rf = this.getSafeFile();
        try {
            rf.reOpen();
            return this.getPageContent(pageNum, rf);
        }
        finally {
            try {
                rf.close();
            }
            catch (Exception ex) {}
        }
    }
    
    protected void killXref(PdfObject obj) {
        if (obj == null) {
            return;
        }
        if (obj instanceof PdfIndirectReference && !obj.isIndirect()) {
            return;
        }
        switch (obj.type()) {
            case 10: {
                final int xr = ((PRIndirectReference)obj).getNumber();
                obj = this.xrefObj.get(xr);
                this.xrefObj.set(xr, null);
                this.freeXref = xr;
                this.killXref(obj);
                break;
            }
            case 5: {
                final PdfArray t = (PdfArray)obj;
                for (int i = 0; i < t.size(); ++i) {
                    this.killXref(t.getPdfObject(i));
                }
                break;
            }
            case 6:
            case 7: {
                final PdfDictionary dic = (PdfDictionary)obj;
                for (final Object element : dic.getKeys()) {
                    this.killXref(dic.get((PdfName)element));
                }
                break;
            }
        }
    }
    
    public void setPageContent(final int pageNum, final byte[] content) {
        this.setPageContent(pageNum, content, -1);
    }
    
    public void setPageContent(final int pageNum, final byte[] content, final int compressionLevel) {
        this.setPageContent(pageNum, content, compressionLevel, false);
    }
    
    public void setPageContent(final int pageNum, final byte[] content, final int compressionLevel, final boolean killOldXRefRecursively) {
        final PdfDictionary page = this.getPageN(pageNum);
        if (page == null) {
            return;
        }
        final PdfObject contents = page.get(PdfName.CONTENTS);
        this.freeXref = -1;
        if (killOldXRefRecursively) {
            this.killXref(contents);
        }
        if (this.freeXref == -1) {
            this.xrefObj.add(null);
            this.freeXref = this.xrefObj.size() - 1;
        }
        page.put(PdfName.CONTENTS, new PRIndirectReference(this, this.freeXref));
        this.xrefObj.set(this.freeXref, new PRStream(this, content, compressionLevel));
    }
    
    public static byte[] decodeBytes(final byte[] b, final PdfDictionary streamDictionary) throws IOException {
        return decodeBytes(b, streamDictionary, FilterHandlers.getDefaultFilterHandlers());
    }
    
    public static byte[] decodeBytes(byte[] b, final PdfDictionary streamDictionary, final Map<PdfName, FilterHandlers.FilterHandler> filterHandlers) throws IOException {
        final PdfObject filter = getPdfObjectRelease(streamDictionary.get(PdfName.FILTER));
        ArrayList<PdfObject> filters = new ArrayList<PdfObject>();
        if (filter != null) {
            if (filter.isName()) {
                filters.add(filter);
            }
            else if (filter.isArray()) {
                filters = ((PdfArray)filter).getArrayList();
            }
        }
        ArrayList<PdfObject> dp = new ArrayList<PdfObject>();
        PdfObject dpo = getPdfObjectRelease(streamDictionary.get(PdfName.DECODEPARMS));
        if (dpo == null || (!dpo.isDictionary() && !dpo.isArray())) {
            dpo = getPdfObjectRelease(streamDictionary.get(PdfName.DP));
        }
        if (dpo != null) {
            if (dpo.isDictionary()) {
                dp.add(dpo);
            }
            else if (dpo.isArray()) {
                dp = ((PdfArray)dpo).getArrayList();
            }
        }
        for (int j = 0; j < filters.size(); ++j) {
            final PdfName filterName = filters.get(j);
            final FilterHandlers.FilterHandler filterHandler = filterHandlers.get(filterName);
            if (filterHandler == null) {
                throw new UnsupportedPdfException(MessageLocalization.getComposedMessage("the.filter.1.is.not.supported", filterName));
            }
            PdfDictionary decodeParams;
            if (j < dp.size()) {
                final PdfObject dpEntry = getPdfObject(dp.get(j));
                if (dpEntry instanceof PdfDictionary) {
                    decodeParams = (PdfDictionary)dpEntry;
                }
                else {
                    if (dpEntry != null && !(dpEntry instanceof PdfNull) && (!(dpEntry instanceof PdfLiteral) || !Arrays.equals("null".getBytes(), ((PdfLiteral)dpEntry).getBytes()))) {
                        throw new UnsupportedPdfException(MessageLocalization.getComposedMessage("the.decode.parameter.type.1.is.not.supported", dpEntry.getClass().toString()));
                    }
                    decodeParams = null;
                }
            }
            else {
                decodeParams = null;
            }
            b = filterHandler.decode(b, filterName, decodeParams, streamDictionary);
        }
        return b;
    }
    
    public static byte[] getStreamBytes(final PRStream stream, final RandomAccessFileOrArray file) throws IOException {
        final byte[] b = getStreamBytesRaw(stream, file);
        return decodeBytes(b, stream);
    }
    
    public static byte[] getStreamBytes(final PRStream stream) throws IOException {
        final RandomAccessFileOrArray rf = stream.getReader().getSafeFile();
        try {
            rf.reOpen();
            return getStreamBytes(stream, rf);
        }
        finally {
            try {
                rf.close();
            }
            catch (Exception ex) {}
        }
    }
    
    public static byte[] getStreamBytesRaw(final PRStream stream, final RandomAccessFileOrArray file) throws IOException {
        final PdfReader reader = stream.getReader();
        byte[] b;
        if (stream.getOffset() < 0L) {
            b = stream.getBytes();
        }
        else {
            b = new byte[stream.getLength()];
            file.seek(stream.getOffset());
            file.readFully(b);
            final PdfEncryption decrypt = reader.getDecrypt();
            if (decrypt != null) {
                final PdfObject filter = getPdfObjectRelease(stream.get(PdfName.FILTER));
                ArrayList<PdfObject> filters = new ArrayList<PdfObject>();
                if (filter != null) {
                    if (filter.isName()) {
                        filters.add(filter);
                    }
                    else if (filter.isArray()) {
                        filters = ((PdfArray)filter).getArrayList();
                    }
                }
                boolean skip = false;
                for (int k = 0; k < filters.size(); ++k) {
                    final PdfObject obj = getPdfObjectRelease(filters.get(k));
                    if (obj != null && obj.toString().equals("/Crypt")) {
                        skip = true;
                        break;
                    }
                }
                if (!skip) {
                    decrypt.setHashKey(stream.getObjNum(), stream.getObjGen());
                    b = decrypt.decryptByteArray(b);
                }
            }
        }
        return b;
    }
    
    public static byte[] getStreamBytesRaw(final PRStream stream) throws IOException {
        final RandomAccessFileOrArray rf = stream.getReader().getSafeFile();
        try {
            rf.reOpen();
            return getStreamBytesRaw(stream, rf);
        }
        finally {
            try {
                rf.close();
            }
            catch (Exception ex) {}
        }
    }
    
    public void eliminateSharedStreams() {
        if (!this.sharedStreams) {
            return;
        }
        this.sharedStreams = false;
        if (this.pageRefs.size() == 1) {
            return;
        }
        final ArrayList<PRIndirectReference> newRefs = new ArrayList<PRIndirectReference>();
        final ArrayList<PRStream> newStreams = new ArrayList<PRStream>();
        final IntHashtable visited = new IntHashtable();
        for (int k = 1; k <= this.pageRefs.size(); ++k) {
            final PdfDictionary page = this.pageRefs.getPageN(k);
            if (page != null) {
                final PdfObject contents = getPdfObject(page.get(PdfName.CONTENTS));
                if (contents != null) {
                    if (contents.isStream()) {
                        final PRIndirectReference ref = (PRIndirectReference)page.get(PdfName.CONTENTS);
                        if (visited.containsKey(ref.getNumber())) {
                            newRefs.add(ref);
                            newStreams.add(new PRStream((PRStream)contents, null));
                        }
                        else {
                            visited.put(ref.getNumber(), 1);
                        }
                    }
                    else if (contents.isArray()) {
                        final PdfArray array = (PdfArray)contents;
                        for (int j = 0; j < array.size(); ++j) {
                            final PRIndirectReference ref2 = (PRIndirectReference)array.getPdfObject(j);
                            if (visited.containsKey(ref2.getNumber())) {
                                newRefs.add(ref2);
                                newStreams.add(new PRStream((PRStream)getPdfObject(ref2), null));
                            }
                            else {
                                visited.put(ref2.getNumber(), 1);
                            }
                        }
                    }
                }
            }
        }
        if (newStreams.isEmpty()) {
            return;
        }
        for (int k = 0; k < newStreams.size(); ++k) {
            this.xrefObj.add(newStreams.get(k));
            final PRIndirectReference ref3 = newRefs.get(k);
            ref3.setNumber(this.xrefObj.size() - 1, 0);
        }
    }
    
    public boolean isTampered() {
        return this.tampered;
    }
    
    public void setTampered(final boolean tampered) {
        this.tampered = tampered;
        this.pageRefs.keepPages();
    }
    
    public byte[] getMetadata() throws IOException {
        final PdfObject obj = getPdfObject(this.catalog.get(PdfName.METADATA));
        if (!(obj instanceof PRStream)) {
            return null;
        }
        final RandomAccessFileOrArray rf = this.getSafeFile();
        byte[] b = null;
        try {
            rf.reOpen();
            b = getStreamBytes((PRStream)obj, rf);
        }
        finally {
            try {
                rf.close();
            }
            catch (Exception ex) {}
        }
        return b;
    }
    
    public long getLastXref() {
        return this.lastXref;
    }
    
    public int getXrefSize() {
        return this.xrefObj.size();
    }
    
    public long getEofPos() {
        return this.eofPos;
    }
    
    public char getPdfVersion() {
        return this.pdfVersion;
    }
    
    public boolean isEncrypted() {
        return this.encrypted;
    }
    
    public long getPermissions() {
        return this.pValue;
    }
    
    public boolean is128Key() {
        return this.rValue == 3;
    }
    
    public PdfDictionary getTrailer() {
        return this.trailer;
    }
    
    PdfEncryption getDecrypt() {
        return this.decrypt;
    }
    
    static boolean equalsn(final byte[] a1, final byte[] a2) {
        for (int length = a2.length, k = 0; k < length; ++k) {
            if (a1[k] != a2[k]) {
                return false;
            }
        }
        return true;
    }
    
    static boolean existsName(final PdfDictionary dic, final PdfName key, final PdfName value) {
        final PdfObject type = getPdfObjectRelease(dic.get(key));
        if (type == null || !type.isName()) {
            return false;
        }
        final PdfName name = (PdfName)type;
        return name.equals(value);
    }
    
    static String getFontName(final PdfDictionary dic) {
        if (dic == null) {
            return null;
        }
        final PdfObject type = getPdfObjectRelease(dic.get(PdfName.BASEFONT));
        if (type == null || !type.isName()) {
            return null;
        }
        return PdfName.decodeName(type.toString());
    }
    
    static String getSubsetPrefix(final PdfDictionary dic) {
        if (dic == null) {
            return null;
        }
        final String s = getFontName(dic);
        if (s == null) {
            return null;
        }
        if (s.length() < 8 || s.charAt(6) != '+') {
            return null;
        }
        for (int k = 0; k < 6; ++k) {
            final char c = s.charAt(k);
            if (c < 'A' || c > 'Z') {
                return null;
            }
        }
        return s;
    }
    
    public int shuffleSubsetNames() {
        int total = 0;
        for (int k = 1; k < this.xrefObj.size(); ++k) {
            final PdfObject obj = this.getPdfObjectRelease(k);
            if (obj != null) {
                if (obj.isDictionary()) {
                    final PdfDictionary dic = (PdfDictionary)obj;
                    if (existsName(dic, PdfName.TYPE, PdfName.FONT)) {
                        if (existsName(dic, PdfName.SUBTYPE, PdfName.TYPE1) || existsName(dic, PdfName.SUBTYPE, PdfName.MMTYPE1) || existsName(dic, PdfName.SUBTYPE, PdfName.TRUETYPE)) {
                            final String s = getSubsetPrefix(dic);
                            if (s != null) {
                                final String ns = BaseFont.createSubsetPrefix() + s.substring(7);
                                final PdfName newName = new PdfName(ns);
                                dic.put(PdfName.BASEFONT, newName);
                                this.setXrefPartialObject(k, dic);
                                ++total;
                                final PdfDictionary fd = dic.getAsDict(PdfName.FONTDESCRIPTOR);
                                if (fd != null) {
                                    fd.put(PdfName.FONTNAME, newName);
                                }
                            }
                        }
                        else if (existsName(dic, PdfName.SUBTYPE, PdfName.TYPE0)) {
                            final String s = getSubsetPrefix(dic);
                            final PdfArray arr = dic.getAsArray(PdfName.DESCENDANTFONTS);
                            if (arr != null) {
                                if (!arr.isEmpty()) {
                                    final PdfDictionary desc = arr.getAsDict(0);
                                    final String sde = getSubsetPrefix(desc);
                                    if (sde != null) {
                                        final String ns2 = BaseFont.createSubsetPrefix();
                                        if (s != null) {
                                            dic.put(PdfName.BASEFONT, new PdfName(ns2 + s.substring(7)));
                                        }
                                        this.setXrefPartialObject(k, dic);
                                        final PdfName newName2 = new PdfName(ns2 + sde.substring(7));
                                        desc.put(PdfName.BASEFONT, newName2);
                                        ++total;
                                        final PdfDictionary fd2 = desc.getAsDict(PdfName.FONTDESCRIPTOR);
                                        if (fd2 != null) {
                                            fd2.put(PdfName.FONTNAME, newName2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return total;
    }
    
    public int createFakeFontSubsets() {
        int total = 0;
        for (int k = 1; k < this.xrefObj.size(); ++k) {
            final PdfObject obj = this.getPdfObjectRelease(k);
            if (obj != null) {
                if (obj.isDictionary()) {
                    final PdfDictionary dic = (PdfDictionary)obj;
                    if (existsName(dic, PdfName.TYPE, PdfName.FONT)) {
                        if (existsName(dic, PdfName.SUBTYPE, PdfName.TYPE1) || existsName(dic, PdfName.SUBTYPE, PdfName.MMTYPE1) || existsName(dic, PdfName.SUBTYPE, PdfName.TRUETYPE)) {
                            String s = getSubsetPrefix(dic);
                            if (s == null) {
                                s = getFontName(dic);
                                if (s != null) {
                                    final String ns = BaseFont.createSubsetPrefix() + s;
                                    PdfDictionary fd = (PdfDictionary)getPdfObjectRelease(dic.get(PdfName.FONTDESCRIPTOR));
                                    if (fd != null) {
                                        if (fd.get(PdfName.FONTFILE) != null || fd.get(PdfName.FONTFILE2) != null || fd.get(PdfName.FONTFILE3) != null) {
                                            fd = dic.getAsDict(PdfName.FONTDESCRIPTOR);
                                            final PdfName newName = new PdfName(ns);
                                            dic.put(PdfName.BASEFONT, newName);
                                            fd.put(PdfName.FONTNAME, newName);
                                            this.setXrefPartialObject(k, dic);
                                            ++total;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return total;
    }
    
    private static PdfArray getNameArray(PdfObject obj) {
        if (obj == null) {
            return null;
        }
        obj = getPdfObjectRelease(obj);
        if (obj == null) {
            return null;
        }
        if (obj.isArray()) {
            return (PdfArray)obj;
        }
        if (obj.isDictionary()) {
            final PdfObject arr2 = getPdfObjectRelease(((PdfDictionary)obj).get(PdfName.D));
            if (arr2 != null && arr2.isArray()) {
                return (PdfArray)arr2;
            }
        }
        return null;
    }
    
    public HashMap<Object, PdfObject> getNamedDestination() {
        return this.getNamedDestination(false);
    }
    
    public HashMap<Object, PdfObject> getNamedDestination(final boolean keepNames) {
        final HashMap<Object, PdfObject> names = this.getNamedDestinationFromNames(keepNames);
        names.putAll(this.getNamedDestinationFromStrings());
        return names;
    }
    
    public HashMap<String, PdfObject> getNamedDestinationFromNames() {
        return new HashMap<String, PdfObject>((Map<? extends String, ? extends PdfObject>)this.getNamedDestinationFromNames(false));
    }
    
    public HashMap<Object, PdfObject> getNamedDestinationFromNames(final boolean keepNames) {
        final HashMap<Object, PdfObject> names = new HashMap<Object, PdfObject>();
        if (this.catalog.get(PdfName.DESTS) != null) {
            final PdfDictionary dic = (PdfDictionary)getPdfObjectRelease(this.catalog.get(PdfName.DESTS));
            if (dic == null) {
                return names;
            }
            final Set<PdfName> keys = dic.getKeys();
            for (final PdfName key : keys) {
                final PdfArray arr = getNameArray(dic.get(key));
                if (arr == null) {
                    continue;
                }
                if (keepNames) {
                    names.put(key, arr);
                }
                else {
                    final String name = PdfName.decodeName(key.toString());
                    names.put(name, arr);
                }
            }
        }
        return names;
    }
    
    public HashMap<String, PdfObject> getNamedDestinationFromStrings() {
        if (this.catalog.get(PdfName.NAMES) != null) {
            PdfDictionary dic = (PdfDictionary)getPdfObjectRelease(this.catalog.get(PdfName.NAMES));
            if (dic != null) {
                dic = (PdfDictionary)getPdfObjectRelease(dic.get(PdfName.DESTS));
                if (dic != null) {
                    final HashMap<String, PdfObject> names = PdfNameTree.readTree(dic);
                    final Iterator<Map.Entry<String, PdfObject>> it = names.entrySet().iterator();
                    while (it.hasNext()) {
                        final Map.Entry<String, PdfObject> entry = it.next();
                        final PdfArray arr = getNameArray(entry.getValue());
                        if (arr != null) {
                            entry.setValue(arr);
                        }
                        else {
                            it.remove();
                        }
                    }
                    return names;
                }
            }
        }
        return new HashMap<String, PdfObject>();
    }
    
    public void removeFields() {
        this.pageRefs.resetReleasePage();
        for (int k = 1; k <= this.pageRefs.size(); ++k) {
            final PdfDictionary page = this.pageRefs.getPageN(k);
            final PdfArray annots = page.getAsArray(PdfName.ANNOTS);
            if (annots == null) {
                this.pageRefs.releasePage(k);
            }
            else {
                for (int j = 0; j < annots.size(); ++j) {
                    final PdfObject obj = getPdfObjectRelease(annots.getPdfObject(j));
                    if (obj != null) {
                        if (obj.isDictionary()) {
                            final PdfDictionary annot = (PdfDictionary)obj;
                            if (PdfName.WIDGET.equals(annot.get(PdfName.SUBTYPE))) {
                                annots.remove(j--);
                            }
                        }
                    }
                }
                if (annots.isEmpty()) {
                    page.remove(PdfName.ANNOTS);
                }
                else {
                    this.pageRefs.releasePage(k);
                }
            }
        }
        this.catalog.remove(PdfName.ACROFORM);
        this.pageRefs.resetReleasePage();
    }
    
    public void removeAnnotations() {
        this.pageRefs.resetReleasePage();
        for (int k = 1; k <= this.pageRefs.size(); ++k) {
            final PdfDictionary page = this.pageRefs.getPageN(k);
            if (page.get(PdfName.ANNOTS) == null) {
                this.pageRefs.releasePage(k);
            }
            else {
                page.remove(PdfName.ANNOTS);
            }
        }
        this.catalog.remove(PdfName.ACROFORM);
        this.pageRefs.resetReleasePage();
    }
    
    public ArrayList<PdfAnnotation.PdfImportedLink> getLinks(final int page) {
        this.pageRefs.resetReleasePage();
        final ArrayList<PdfAnnotation.PdfImportedLink> result = new ArrayList<PdfAnnotation.PdfImportedLink>();
        final PdfDictionary pageDic = this.pageRefs.getPageN(page);
        if (pageDic.get(PdfName.ANNOTS) != null) {
            final PdfArray annots = pageDic.getAsArray(PdfName.ANNOTS);
            for (int j = 0; j < annots.size(); ++j) {
                final PdfDictionary annot = (PdfDictionary)getPdfObjectRelease(annots.getPdfObject(j));
                if (PdfName.LINK.equals(annot.get(PdfName.SUBTYPE))) {
                    result.add(new PdfAnnotation.PdfImportedLink(annot));
                }
            }
        }
        this.pageRefs.releasePage(page);
        this.pageRefs.resetReleasePage();
        return result;
    }
    
    private void iterateBookmarks(PdfObject outlineRef, final HashMap<Object, PdfObject> names) {
        while (outlineRef != null) {
            this.replaceNamedDestination(outlineRef, names);
            final PdfDictionary outline = (PdfDictionary)getPdfObjectRelease(outlineRef);
            final PdfObject first = outline.get(PdfName.FIRST);
            if (first != null) {
                this.iterateBookmarks(first, names);
            }
            outlineRef = outline.get(PdfName.NEXT);
        }
    }
    
    public void makeRemoteNamedDestinationsLocal() {
        if (this.remoteToLocalNamedDestinations) {
            return;
        }
        this.remoteToLocalNamedDestinations = true;
        final HashMap<Object, PdfObject> names = this.getNamedDestination(true);
        if (names.isEmpty()) {
            return;
        }
        for (int k = 1; k <= this.pageRefs.size(); ++k) {
            final PdfDictionary page = this.pageRefs.getPageN(k);
            final PdfObject annotsRef;
            final PdfArray annots = (PdfArray)getPdfObject(annotsRef = page.get(PdfName.ANNOTS));
            final int annotIdx = this.lastXrefPartial;
            this.releaseLastXrefPartial();
            if (annots == null) {
                this.pageRefs.releasePage(k);
            }
            else {
                boolean commitAnnots = false;
                for (int an = 0; an < annots.size(); ++an) {
                    final PdfObject objRef = annots.getPdfObject(an);
                    if (this.convertNamedDestination(objRef, names) && !objRef.isIndirect()) {
                        commitAnnots = true;
                    }
                }
                if (commitAnnots) {
                    this.setXrefPartialObject(annotIdx, annots);
                }
                if (!commitAnnots || annotsRef.isIndirect()) {
                    this.pageRefs.releasePage(k);
                }
            }
        }
    }
    
    private boolean convertNamedDestination(PdfObject obj, final HashMap<Object, PdfObject> names) {
        obj = getPdfObject(obj);
        final int objIdx = this.lastXrefPartial;
        this.releaseLastXrefPartial();
        if (obj != null && obj.isDictionary()) {
            final PdfObject ob2 = getPdfObject(((PdfDictionary)obj).get(PdfName.A));
            if (ob2 != null) {
                final int obj2Idx = this.lastXrefPartial;
                this.releaseLastXrefPartial();
                final PdfDictionary dic = (PdfDictionary)ob2;
                final PdfName type = (PdfName)getPdfObjectRelease(dic.get(PdfName.S));
                if (PdfName.GOTOR.equals(type)) {
                    final PdfObject ob3 = getPdfObjectRelease(dic.get(PdfName.D));
                    Object name = null;
                    if (ob3 != null) {
                        if (ob3.isName()) {
                            name = ob3;
                        }
                        else if (ob3.isString()) {
                            name = ob3.toString();
                        }
                        final PdfArray dest = names.get(name);
                        if (dest != null) {
                            dic.remove(PdfName.F);
                            dic.remove(PdfName.NEWWINDOW);
                            dic.put(PdfName.S, PdfName.GOTO);
                            this.setXrefPartialObject(obj2Idx, ob2);
                            this.setXrefPartialObject(objIdx, obj);
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    
    public void consolidateNamedDestinations() {
        if (this.consolidateNamedDestinations) {
            return;
        }
        this.consolidateNamedDestinations = true;
        final HashMap<Object, PdfObject> names = this.getNamedDestination(true);
        if (names.isEmpty()) {
            return;
        }
        for (int k = 1; k <= this.pageRefs.size(); ++k) {
            final PdfDictionary page = this.pageRefs.getPageN(k);
            final PdfObject annotsRef;
            final PdfArray annots = (PdfArray)getPdfObject(annotsRef = page.get(PdfName.ANNOTS));
            final int annotIdx = this.lastXrefPartial;
            this.releaseLastXrefPartial();
            if (annots == null) {
                this.pageRefs.releasePage(k);
            }
            else {
                boolean commitAnnots = false;
                for (int an = 0; an < annots.size(); ++an) {
                    final PdfObject objRef = annots.getPdfObject(an);
                    if (this.replaceNamedDestination(objRef, names) && !objRef.isIndirect()) {
                        commitAnnots = true;
                    }
                }
                if (commitAnnots) {
                    this.setXrefPartialObject(annotIdx, annots);
                }
                if (!commitAnnots || annotsRef.isIndirect()) {
                    this.pageRefs.releasePage(k);
                }
            }
        }
        final PdfDictionary outlines = (PdfDictionary)getPdfObjectRelease(this.catalog.get(PdfName.OUTLINES));
        if (outlines == null) {
            return;
        }
        this.iterateBookmarks(outlines.get(PdfName.FIRST), names);
    }
    
    private boolean replaceNamedDestination(PdfObject obj, final HashMap<Object, PdfObject> names) {
        obj = getPdfObject(obj);
        final int objIdx = this.lastXrefPartial;
        this.releaseLastXrefPartial();
        if (obj != null && obj.isDictionary()) {
            PdfObject ob2 = getPdfObjectRelease(((PdfDictionary)obj).get(PdfName.DEST));
            Object name = null;
            if (ob2 != null) {
                if (ob2.isName()) {
                    name = ob2;
                }
                else if (ob2.isString()) {
                    name = ob2.toString();
                }
                final PdfArray dest = names.get(name);
                if (dest != null) {
                    ((PdfDictionary)obj).put(PdfName.DEST, dest);
                    this.setXrefPartialObject(objIdx, obj);
                    return true;
                }
            }
            else if ((ob2 = getPdfObject(((PdfDictionary)obj).get(PdfName.A))) != null) {
                final int obj2Idx = this.lastXrefPartial;
                this.releaseLastXrefPartial();
                final PdfDictionary dic = (PdfDictionary)ob2;
                final PdfName type = (PdfName)getPdfObjectRelease(dic.get(PdfName.S));
                if (PdfName.GOTO.equals(type)) {
                    final PdfObject ob3 = getPdfObjectRelease(dic.get(PdfName.D));
                    if (ob3 != null) {
                        if (ob3.isName()) {
                            name = ob3;
                        }
                        else if (ob3.isString()) {
                            name = ob3.toString();
                        }
                    }
                    final PdfArray dest2 = names.get(name);
                    if (dest2 != null) {
                        dic.put(PdfName.D, dest2);
                        this.setXrefPartialObject(obj2Idx, ob2);
                        this.setXrefPartialObject(objIdx, obj);
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    protected static PdfDictionary duplicatePdfDictionary(final PdfDictionary original, PdfDictionary copy, final PdfReader newReader) {
        if (copy == null) {
            copy = new PdfDictionary(original.size());
        }
        for (final Object element : original.getKeys()) {
            final PdfName key = (PdfName)element;
            copy.put(key, duplicatePdfObject(original.get(key), newReader));
        }
        return copy;
    }
    
    protected static PdfObject duplicatePdfObject(final PdfObject original, final PdfReader newReader) {
        if (original == null) {
            return null;
        }
        switch (original.type()) {
            case 6: {
                return duplicatePdfDictionary((PdfDictionary)original, null, newReader);
            }
            case 7: {
                final PRStream org = (PRStream)original;
                final PRStream stream = new PRStream(org, null, newReader);
                duplicatePdfDictionary(org, stream, newReader);
                return stream;
            }
            case 5: {
                final PdfArray originalArray = (PdfArray)original;
                final PdfArray arr = new PdfArray(originalArray.size());
                final Iterator<PdfObject> it = originalArray.listIterator();
                while (it.hasNext()) {
                    arr.add(duplicatePdfObject(it.next(), newReader));
                }
                return arr;
            }
            case 10: {
                final PRIndirectReference org2 = (PRIndirectReference)original;
                return new PRIndirectReference(newReader, org2.getNumber(), org2.getGeneration());
            }
            default: {
                return original;
            }
        }
    }
    
    public void close() {
        try {
            this.tokens.close();
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }
    
    protected void removeUnusedNode(PdfObject obj, final boolean[] hits) {
        final Stack<Object> state = new Stack<Object>();
        state.push(obj);
        while (!state.empty()) {
            final Object current = state.pop();
            if (current == null) {
                continue;
            }
            ArrayList<PdfObject> ar = null;
            PdfDictionary dic = null;
            PdfName[] keys = null;
            Object[] objs = null;
            int idx = 0;
            if (current instanceof PdfObject) {
                obj = (PdfObject)current;
                switch (obj.type()) {
                    case 6:
                    case 7: {
                        dic = (PdfDictionary)obj;
                        keys = new PdfName[dic.size()];
                        dic.getKeys().toArray(keys);
                        break;
                    }
                    case 5: {
                        ar = ((PdfArray)obj).getArrayList();
                        break;
                    }
                    case 10: {
                        final PRIndirectReference ref = (PRIndirectReference)obj;
                        final int num = ref.getNumber();
                        if (!hits[num]) {
                            hits[num] = true;
                            state.push(getPdfObjectRelease(ref));
                            continue;
                        }
                        continue;
                    }
                    default: {
                        continue;
                    }
                }
            }
            else {
                objs = (Object[])current;
                if (objs[0] instanceof ArrayList) {
                    ar = (ArrayList<PdfObject>)objs[0];
                    idx = (int)objs[1];
                }
                else {
                    keys = (PdfName[])objs[0];
                    dic = (PdfDictionary)objs[1];
                    idx = (int)objs[2];
                }
            }
            if (ar != null) {
                int k = idx;
                while (k < ar.size()) {
                    final PdfObject v = ar.get(k);
                    if (v.isIndirect()) {
                        final int num2 = ((PRIndirectReference)v).getNumber();
                        if (num2 >= this.xrefObj.size() || (!this.partial && this.xrefObj.get(num2) == null)) {
                            ar.set(k, PdfNull.PDFNULL);
                            ++k;
                            continue;
                        }
                    }
                    if (objs == null) {
                        state.push(new Object[] { ar, k + 1 });
                    }
                    else {
                        objs[1] = k + 1;
                        state.push(objs);
                    }
                    state.push(v);
                    break;
                }
            }
            else {
                int k = idx;
                while (k < keys.length) {
                    final PdfName key = keys[k];
                    final PdfObject v2 = dic.get(key);
                    if (v2.isIndirect()) {
                        final int num3 = ((PRIndirectReference)v2).getNumber();
                        if (num3 < 0 || num3 >= this.xrefObj.size() || (!this.partial && this.xrefObj.get(num3) == null)) {
                            dic.put(key, PdfNull.PDFNULL);
                            ++k;
                            continue;
                        }
                    }
                    if (objs == null) {
                        state.push(new Object[] { keys, dic, k + 1 });
                    }
                    else {
                        objs[2] = k + 1;
                        state.push(objs);
                    }
                    state.push(v2);
                    break;
                }
            }
        }
    }
    
    public int removeUnusedObjects() {
        final boolean[] hits = new boolean[this.xrefObj.size()];
        this.removeUnusedNode(this.trailer, hits);
        int total = 0;
        if (this.partial) {
            for (int k = 1; k < hits.length; ++k) {
                if (!hits[k]) {
                    this.xref[k * 2] = -1L;
                    this.xref[k * 2 + 1] = 0L;
                    this.xrefObj.set(k, null);
                    ++total;
                }
            }
        }
        else {
            for (int k = 1; k < hits.length; ++k) {
                if (!hits[k]) {
                    this.xrefObj.set(k, null);
                    ++total;
                }
            }
        }
        return total;
    }
    
    public AcroFields getAcroFields() {
        return new AcroFields(this, null);
    }
    
    public String getJavaScript(final RandomAccessFileOrArray file) throws IOException {
        final PdfDictionary names = (PdfDictionary)getPdfObjectRelease(this.catalog.get(PdfName.NAMES));
        if (names == null) {
            return null;
        }
        final PdfDictionary js = (PdfDictionary)getPdfObjectRelease(names.get(PdfName.JAVASCRIPT));
        if (js == null) {
            return null;
        }
        final HashMap<String, PdfObject> jscript = PdfNameTree.readTree(js);
        String[] sortedNames = new String[jscript.size()];
        sortedNames = jscript.keySet().toArray(sortedNames);
        Arrays.sort(sortedNames);
        final StringBuffer buf = new StringBuffer();
        for (int k = 0; k < sortedNames.length; ++k) {
            final PdfDictionary j = (PdfDictionary)getPdfObjectRelease(jscript.get(sortedNames[k]));
            if (j != null) {
                final PdfObject obj = getPdfObjectRelease(j.get(PdfName.JS));
                if (obj != null) {
                    if (obj.isString()) {
                        buf.append(((PdfString)obj).toUnicodeString()).append('\n');
                    }
                    else if (obj.isStream()) {
                        final byte[] bytes = getStreamBytes((PRStream)obj, file);
                        if (bytes.length >= 2 && bytes[0] == -2 && bytes[1] == -1) {
                            buf.append(PdfEncodings.convertToString(bytes, "UnicodeBig"));
                        }
                        else {
                            buf.append(PdfEncodings.convertToString(bytes, "PDF"));
                        }
                        buf.append('\n');
                    }
                }
            }
        }
        return buf.toString();
    }
    
    public String getJavaScript() throws IOException {
        final RandomAccessFileOrArray rf = this.getSafeFile();
        try {
            rf.reOpen();
            return this.getJavaScript(rf);
        }
        finally {
            try {
                rf.close();
            }
            catch (Exception ex) {}
        }
    }
    
    public void selectPages(final String ranges) {
        this.selectPages(SequenceList.expand(ranges, this.getNumberOfPages()));
    }
    
    public void selectPages(final List<Integer> pagesToKeep) {
        this.selectPages(pagesToKeep, true);
    }
    
    protected void selectPages(final List<Integer> pagesToKeep, final boolean removeUnused) {
        this.pageRefs.selectPages(pagesToKeep);
        if (removeUnused) {
            this.removeUnusedObjects();
        }
    }
    
    @Override
    public void setViewerPreferences(final int preferences) {
        this.viewerPreferences.setViewerPreferences(preferences);
        this.setViewerPreferences(this.viewerPreferences);
    }
    
    @Override
    public void addViewerPreference(final PdfName key, final PdfObject value) {
        this.viewerPreferences.addViewerPreference(key, value);
        this.setViewerPreferences(this.viewerPreferences);
    }
    
    public void setViewerPreferences(final PdfViewerPreferencesImp vp) {
        vp.addToCatalog(this.catalog);
    }
    
    public int getSimpleViewerPreferences() {
        return PdfViewerPreferencesImp.getViewerPreferences(this.catalog).getPageLayoutAndMode();
    }
    
    public boolean isAppendable() {
        return this.appendable;
    }
    
    public void setAppendable(final boolean appendable) {
        this.appendable = appendable;
        if (appendable) {
            getPdfObject(this.trailer.get(PdfName.ROOT));
        }
    }
    
    public boolean isNewXrefType() {
        return this.newXrefType;
    }
    
    public long getFileLength() {
        return this.fileLength;
    }
    
    public boolean isHybridXref() {
        return this.hybridXref;
    }
    
    PdfIndirectReference getCryptoRef() {
        if (this.cryptoRef == null) {
            return null;
        }
        return new PdfIndirectReference(0, this.cryptoRef.getNumber(), this.cryptoRef.getGeneration());
    }
    
    public boolean hasUsageRights() {
        final PdfDictionary perms = this.catalog.getAsDict(PdfName.PERMS);
        return perms != null && (perms.contains(PdfName.UR) || perms.contains(PdfName.UR3));
    }
    
    public void removeUsageRights() {
        final PdfDictionary perms = this.catalog.getAsDict(PdfName.PERMS);
        if (perms == null) {
            return;
        }
        perms.remove(PdfName.UR);
        perms.remove(PdfName.UR3);
        if (perms.size() == 0) {
            this.catalog.remove(PdfName.PERMS);
        }
    }
    
    public int getCertificationLevel() {
        PdfDictionary dic = this.catalog.getAsDict(PdfName.PERMS);
        if (dic == null) {
            return 0;
        }
        dic = dic.getAsDict(PdfName.DOCMDP);
        if (dic == null) {
            return 0;
        }
        final PdfArray arr = dic.getAsArray(PdfName.REFERENCE);
        if (arr == null || arr.size() == 0) {
            return 0;
        }
        dic = arr.getAsDict(0);
        if (dic == null) {
            return 0;
        }
        dic = dic.getAsDict(PdfName.TRANSFORMPARAMS);
        if (dic == null) {
            return 0;
        }
        final PdfNumber p = dic.getAsNumber(PdfName.P);
        if (p == null) {
            return 0;
        }
        return p.intValue();
    }
    
    public final boolean isOpenedWithFullPermissions() {
        return !this.encrypted || this.ownerPasswordUsed || PdfReader.unethicalreading;
    }
    
    public int getCryptoMode() {
        if (this.decrypt == null) {
            return -1;
        }
        return this.decrypt.getCryptoMode();
    }
    
    public boolean isMetadataEncrypted() {
        return this.decrypt != null && this.decrypt.isMetadataEncrypted();
    }
    
    public byte[] computeUserPassword() {
        if (!this.encrypted || !this.ownerPasswordUsed) {
            return null;
        }
        return this.decrypt.computeUserPassword(this.password);
    }
    
    static {
        PdfReader.unethicalreading = false;
        PdfReader.debugmode = false;
        LOGGER = LoggerFactory.getLogger(PdfReader.class);
        pageInhCandidates = new PdfName[] { PdfName.MEDIABOX, PdfName.ROTATE, PdfName.RESOURCES, PdfName.CROPBOX };
        endstream = PdfEncodings.convertToBytes("endstream", null);
        endobj = PdfEncodings.convertToBytes("endobj", null);
        PdfReader.COUNTER = CounterFactory.getCounter(PdfReader.class);
    }
    
    static class PageRefs
    {
        private final PdfReader reader;
        private ArrayList<PRIndirectReference> refsn;
        private int sizep;
        private IntHashtable refsp;
        private int lastPageRead;
        private ArrayList<PdfDictionary> pageInh;
        private boolean keepPages;
        private Set<PdfObject> pagesNodes;
        
        private PageRefs(final PdfReader reader) throws IOException {
            this.lastPageRead = -1;
            this.pagesNodes = new HashSet<PdfObject>();
            this.reader = reader;
            if (reader.partial) {
                this.refsp = new IntHashtable();
                final PdfNumber npages = (PdfNumber)PdfReader.getPdfObjectRelease(reader.rootPages.get(PdfName.COUNT));
                this.sizep = npages.intValue();
            }
            else {
                this.readPages();
            }
        }
        
        PageRefs(final PageRefs other, final PdfReader reader) {
            this.lastPageRead = -1;
            this.pagesNodes = new HashSet<PdfObject>();
            this.reader = reader;
            this.sizep = other.sizep;
            if (other.refsn != null) {
                this.refsn = new ArrayList<PRIndirectReference>(other.refsn);
                for (int k = 0; k < this.refsn.size(); ++k) {
                    this.refsn.set(k, (PRIndirectReference)PdfReader.duplicatePdfObject(this.refsn.get(k), reader));
                }
            }
            else {
                this.refsp = (IntHashtable)other.refsp.clone();
            }
        }
        
        int size() {
            if (this.refsn != null) {
                return this.refsn.size();
            }
            return this.sizep;
        }
        
        void readPages() throws IOException {
            if (this.refsn != null) {
                return;
            }
            this.refsp = null;
            this.refsn = new ArrayList<PRIndirectReference>();
            this.pageInh = new ArrayList<PdfDictionary>();
            this.iteratePages((PRIndirectReference)this.reader.catalog.get(PdfName.PAGES));
            this.pageInh = null;
            this.reader.rootPages.put(PdfName.COUNT, new PdfNumber(this.refsn.size()));
        }
        
        void reReadPages() throws IOException {
            this.refsn = null;
            this.readPages();
        }
        
        public PdfDictionary getPageN(final int pageNum) {
            final PRIndirectReference ref = this.getPageOrigRef(pageNum);
            return (PdfDictionary)PdfReader.getPdfObject(ref);
        }
        
        public PdfDictionary getPageNRelease(final int pageNum) {
            final PdfDictionary page = this.getPageN(pageNum);
            this.releasePage(pageNum);
            return page;
        }
        
        public PRIndirectReference getPageOrigRefRelease(final int pageNum) {
            final PRIndirectReference ref = this.getPageOrigRef(pageNum);
            this.releasePage(pageNum);
            return ref;
        }
        
        public PRIndirectReference getPageOrigRef(int pageNum) {
            try {
                if (--pageNum < 0 || pageNum >= this.size()) {
                    return null;
                }
                if (this.refsn != null) {
                    return this.refsn.get(pageNum);
                }
                final int n = this.refsp.get(pageNum);
                if (n == 0) {
                    final PRIndirectReference ref = this.getSinglePage(pageNum);
                    if (this.reader.lastXrefPartial == -1) {
                        this.lastPageRead = -1;
                    }
                    else {
                        this.lastPageRead = pageNum;
                    }
                    this.reader.lastXrefPartial = -1;
                    this.refsp.put(pageNum, ref.getNumber());
                    if (this.keepPages) {
                        this.lastPageRead = -1;
                    }
                    return ref;
                }
                if (this.lastPageRead != pageNum) {
                    this.lastPageRead = -1;
                }
                if (this.keepPages) {
                    this.lastPageRead = -1;
                }
                return new PRIndirectReference(this.reader, n);
            }
            catch (Exception e) {
                throw new ExceptionConverter(e);
            }
        }
        
        void keepPages() {
            if (this.refsp == null || this.keepPages) {
                return;
            }
            this.keepPages = true;
            this.refsp.clear();
        }
        
        public void releasePage(int pageNum) {
            if (this.refsp == null) {
                return;
            }
            if (--pageNum < 0 || pageNum >= this.size()) {
                return;
            }
            if (pageNum != this.lastPageRead) {
                return;
            }
            this.lastPageRead = -1;
            this.reader.lastXrefPartial = this.refsp.get(pageNum);
            this.reader.releaseLastXrefPartial();
            this.refsp.remove(pageNum);
        }
        
        public void resetReleasePage() {
            if (this.refsp == null) {
                return;
            }
            this.lastPageRead = -1;
        }
        
        void insertPage(int pageNum, final PRIndirectReference ref) {
            --pageNum;
            if (this.refsn != null) {
                if (pageNum >= this.refsn.size()) {
                    this.refsn.add(ref);
                }
                else {
                    this.refsn.add(pageNum, ref);
                }
            }
            else {
                ++this.sizep;
                this.lastPageRead = -1;
                if (pageNum >= this.size()) {
                    this.refsp.put(this.size(), ref.getNumber());
                }
                else {
                    final IntHashtable refs2 = new IntHashtable((this.refsp.size() + 1) * 2);
                    final Iterator<IntHashtable.Entry> it = this.refsp.getEntryIterator();
                    while (it.hasNext()) {
                        final IntHashtable.Entry entry = it.next();
                        final int p = entry.getKey();
                        refs2.put((p >= pageNum) ? (p + 1) : p, entry.getValue());
                    }
                    refs2.put(pageNum, ref.getNumber());
                    this.refsp = refs2;
                }
            }
        }
        
        private void pushPageAttributes(final PdfDictionary nodePages) {
            final PdfDictionary dic = new PdfDictionary();
            if (!this.pageInh.isEmpty()) {
                dic.putAll(this.pageInh.get(this.pageInh.size() - 1));
            }
            for (int k = 0; k < PdfReader.pageInhCandidates.length; ++k) {
                final PdfObject obj = nodePages.get(PdfReader.pageInhCandidates[k]);
                if (obj != null) {
                    dic.put(PdfReader.pageInhCandidates[k], obj);
                }
            }
            this.pageInh.add(dic);
        }
        
        private void popPageAttributes() {
            this.pageInh.remove(this.pageInh.size() - 1);
        }
        
        private void iteratePages(final PRIndirectReference rpage) throws IOException {
            final PdfDictionary page = (PdfDictionary)PdfReader.getPdfObject(rpage);
            if (page == null) {
                return;
            }
            if (!this.pagesNodes.add(PdfReader.getPdfObject(rpage))) {
                throw new InvalidPdfException(MessageLocalization.getComposedMessage("illegal.pages.tree", new Object[0]));
            }
            final PdfArray kidsPR = page.getAsArray(PdfName.KIDS);
            if (kidsPR == null) {
                page.put(PdfName.TYPE, PdfName.PAGE);
                final PdfDictionary dic = this.pageInh.get(this.pageInh.size() - 1);
                for (final Object element : dic.getKeys()) {
                    final PdfName key = (PdfName)element;
                    if (page.get(key) == null) {
                        page.put(key, dic.get(key));
                    }
                }
                if (page.get(PdfName.MEDIABOX) == null) {
                    final PdfArray arr = new PdfArray(new float[] { 0.0f, 0.0f, PageSize.LETTER.getRight(), PageSize.LETTER.getTop() });
                    page.put(PdfName.MEDIABOX, arr);
                }
                this.refsn.add(rpage);
            }
            else {
                page.put(PdfName.TYPE, PdfName.PAGES);
                this.pushPageAttributes(page);
                for (int k = 0; k < kidsPR.size(); ++k) {
                    final PdfObject obj = kidsPR.getPdfObject(k);
                    if (!obj.isIndirect()) {
                        while (k < kidsPR.size()) {
                            kidsPR.remove(k);
                        }
                        break;
                    }
                    this.iteratePages((PRIndirectReference)obj);
                }
                this.popPageAttributes();
            }
        }
        
        protected PRIndirectReference getSinglePage(final int n) {
            final PdfDictionary acc = new PdfDictionary();
            PdfDictionary top = this.reader.rootPages;
            int base = 0;
            PRIndirectReference ref = null;
            PdfDictionary dic = null;
        Block_7:
            while (true) {
                for (int k = 0; k < PdfReader.pageInhCandidates.length; ++k) {
                    final PdfObject obj = top.get(PdfReader.pageInhCandidates[k]);
                    if (obj != null) {
                        acc.put(PdfReader.pageInhCandidates[k], obj);
                    }
                }
                final PdfArray kids = (PdfArray)PdfReader.getPdfObjectRelease(top.get(PdfName.KIDS));
                final Iterator<PdfObject> it = kids.listIterator();
                while (it.hasNext()) {
                    ref = it.next();
                    dic = (PdfDictionary)PdfReader.getPdfObject(ref);
                    final int last = this.reader.lastXrefPartial;
                    final PdfObject count = PdfReader.getPdfObjectRelease(dic.get(PdfName.COUNT));
                    this.reader.lastXrefPartial = last;
                    int acn = 1;
                    if (count != null && count.type() == 2) {
                        acn = ((PdfNumber)count).intValue();
                    }
                    if (n < base + acn) {
                        if (count == null) {
                            break Block_7;
                        }
                        this.reader.releaseLastXrefPartial();
                        top = dic;
                        break;
                    }
                    else {
                        this.reader.releaseLastXrefPartial();
                        base += acn;
                    }
                }
            }
            dic.mergeDifferent(acc);
            return ref;
        }
        
        private void selectPages(final List<Integer> pagesToKeep) {
            final IntHashtable pg = new IntHashtable();
            final ArrayList<Integer> finalPages = new ArrayList<Integer>();
            final int psize = this.size();
            for (final Integer pi : pagesToKeep) {
                final int p = pi;
                if (p >= 1 && p <= psize && pg.put(p, 1) == 0) {
                    finalPages.add(pi);
                }
            }
            if (this.reader.partial) {
                for (int k = 1; k <= psize; ++k) {
                    this.getPageOrigRef(k);
                    this.resetReleasePage();
                }
            }
            final PRIndirectReference parent = (PRIndirectReference)this.reader.catalog.get(PdfName.PAGES);
            final PdfDictionary topPages = (PdfDictionary)PdfReader.getPdfObject(parent);
            final ArrayList<PRIndirectReference> newPageRefs = new ArrayList<PRIndirectReference>(finalPages.size());
            final PdfArray kids = new PdfArray();
            for (int i = 0; i < finalPages.size(); ++i) {
                final int p2 = finalPages.get(i);
                final PRIndirectReference pref = this.getPageOrigRef(p2);
                this.resetReleasePage();
                kids.add(pref);
                newPageRefs.add(pref);
                this.getPageN(p2).put(PdfName.PARENT, parent);
            }
            final AcroFields af = this.reader.getAcroFields();
            final boolean removeFields = af.getFields().size() > 0;
            for (int j = 1; j <= psize; ++j) {
                if (!pg.containsKey(j)) {
                    if (removeFields) {
                        af.removeFieldsFromPage(j);
                    }
                    final PRIndirectReference pref2 = this.getPageOrigRef(j);
                    final int nref = pref2.getNumber();
                    this.reader.xrefObj.set(nref, null);
                    if (this.reader.partial) {
                        this.reader.xref[nref * 2] = -1L;
                        this.reader.xref[nref * 2 + 1] = 0L;
                    }
                }
            }
            topPages.put(PdfName.COUNT, new PdfNumber(finalPages.size()));
            topPages.put(PdfName.KIDS, kids);
            this.refsp = null;
            this.refsn = newPageRefs;
        }
    }
}
