// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.error_messages.MessageLocalization;
import java.io.IOException;
import java.util.ArrayList;

public class PdfContentParser
{
    public static final int COMMAND_TYPE = 200;
    private PRTokeniser tokeniser;
    
    public PdfContentParser(final PRTokeniser tokeniser) {
        this.tokeniser = tokeniser;
    }
    
    public ArrayList<PdfObject> parse(ArrayList<PdfObject> ls) throws IOException {
        if (ls == null) {
            ls = new ArrayList<PdfObject>();
        }
        else {
            ls.clear();
        }
        PdfObject ob = null;
        while ((ob = this.readPRObject()) != null) {
            ls.add(ob);
            if (ob.type() == 200) {
                break;
            }
        }
        return ls;
    }
    
    public PRTokeniser getTokeniser() {
        return this.tokeniser;
    }
    
    public void setTokeniser(final PRTokeniser tokeniser) {
        this.tokeniser = tokeniser;
    }
    
    public PdfDictionary readDictionary() throws IOException {
        final PdfDictionary dic = new PdfDictionary();
        while (this.nextValidToken()) {
            if (this.tokeniser.getTokenType() == PRTokeniser.TokenType.END_DIC) {
                return dic;
            }
            if (this.tokeniser.getTokenType() == PRTokeniser.TokenType.OTHER && "def".equals(this.tokeniser.getStringValue())) {
                continue;
            }
            if (this.tokeniser.getTokenType() != PRTokeniser.TokenType.NAME) {
                throw new IOException(MessageLocalization.getComposedMessage("dictionary.key.1.is.not.a.name", this.tokeniser.getStringValue()));
            }
            final PdfName name = new PdfName(this.tokeniser.getStringValue(), false);
            final PdfObject obj = this.readPRObject();
            final int type = obj.type();
            if (-type == PRTokeniser.TokenType.END_DIC.ordinal()) {
                throw new IOException(MessageLocalization.getComposedMessage("unexpected.gt.gt", new Object[0]));
            }
            if (-type == PRTokeniser.TokenType.END_ARRAY.ordinal()) {
                throw new IOException(MessageLocalization.getComposedMessage("unexpected.close.bracket", new Object[0]));
            }
            dic.put(name, obj);
        }
        throw new IOException(MessageLocalization.getComposedMessage("unexpected.end.of.file", new Object[0]));
    }
    
    public PdfArray readArray() throws IOException {
        final PdfArray array = new PdfArray();
        while (true) {
            final PdfObject obj = this.readPRObject();
            final int type = obj.type();
            if (-type == PRTokeniser.TokenType.END_ARRAY.ordinal()) {
                return array;
            }
            if (-type == PRTokeniser.TokenType.END_DIC.ordinal()) {
                throw new IOException(MessageLocalization.getComposedMessage("unexpected.gt.gt", new Object[0]));
            }
            array.add(obj);
        }
    }
    
    public PdfObject readPRObject() throws IOException {
        if (!this.nextValidToken()) {
            return null;
        }
        final PRTokeniser.TokenType type = this.tokeniser.getTokenType();
        switch (type) {
            case START_DIC: {
                final PdfDictionary dic = this.readDictionary();
                return dic;
            }
            case START_ARRAY: {
                return this.readArray();
            }
            case STRING: {
                final PdfString str = new PdfString(this.tokeniser.getStringValue(), null).setHexWriting(this.tokeniser.isHexString());
                return str;
            }
            case NAME: {
                return new PdfName(this.tokeniser.getStringValue(), false);
            }
            case NUMBER: {
                return new PdfNumber(this.tokeniser.getStringValue());
            }
            case OTHER: {
                return new PdfLiteral(200, this.tokeniser.getStringValue());
            }
            default: {
                return new PdfLiteral(-type.ordinal(), this.tokeniser.getStringValue());
            }
        }
    }
    
    public boolean nextValidToken() throws IOException {
        while (this.tokeniser.nextToken()) {
            if (this.tokeniser.getTokenType() == PRTokeniser.TokenType.COMMENT) {
                continue;
            }
            return true;
        }
        return false;
    }
}
