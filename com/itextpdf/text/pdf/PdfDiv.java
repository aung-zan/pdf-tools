// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.Collection;
import com.itextpdf.text.Rectangle;
import com.itextpdf.awt.geom.AffineTransform;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.ElementListener;
import com.itextpdf.text.Chunk;
import java.util.List;
import com.itextpdf.text.Image;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.AccessibleElementId;
import java.util.HashMap;
import java.util.ArrayList;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;
import com.itextpdf.text.api.Spaceable;
import com.itextpdf.text.Element;

public class PdfDiv implements Element, Spaceable, IAccessibleElement
{
    private ArrayList<Element> content;
    private Float left;
    private Float top;
    private Float right;
    private Float bottom;
    private Float width;
    private Float height;
    private Float percentageHeight;
    private Float percentageWidth;
    private float contentWidth;
    private float contentHeight;
    private int textAlignment;
    private float paddingLeft;
    private float paddingRight;
    private float paddingTop;
    private float paddingBottom;
    private FloatType floatType;
    private PositionType position;
    private DisplayType display;
    private FloatLayout floatLayout;
    private BorderTopStyle borderTopStyle;
    private float yLine;
    protected int runDirection;
    private boolean keepTogether;
    protected PdfName role;
    protected HashMap<PdfName, PdfObject> accessibleAttributes;
    protected AccessibleElementId id;
    private BaseColor backgroundColor;
    private Image backgroundImage;
    private Float backgroundImageWidth;
    private Float backgroundImageHeight;
    protected float spacingBefore;
    protected float spacingAfter;
    
    public float getContentWidth() {
        return this.contentWidth;
    }
    
    public void setContentWidth(final float contentWidth) {
        this.contentWidth = contentWidth;
    }
    
    public float getContentHeight() {
        return this.contentHeight;
    }
    
    public void setContentHeight(final float contentHeight) {
        this.contentHeight = contentHeight;
    }
    
    public float getActualHeight() {
        return (this.height != null && this.height >= this.contentHeight) ? this.height : this.contentHeight;
    }
    
    public float getActualWidth() {
        return (this.width != null && this.width >= this.contentWidth) ? this.width : this.contentWidth;
    }
    
    public Float getPercentageHeight() {
        return this.percentageHeight;
    }
    
    public void setPercentageHeight(final Float percentageHeight) {
        this.percentageHeight = percentageHeight;
    }
    
    public Float getPercentageWidth() {
        return this.percentageWidth;
    }
    
    public void setPercentageWidth(final Float percentageWidth) {
        this.percentageWidth = percentageWidth;
    }
    
    public DisplayType getDisplay() {
        return this.display;
    }
    
    public void setDisplay(final DisplayType display) {
        this.display = display;
    }
    
    public BaseColor getBackgroundColor() {
        return this.backgroundColor;
    }
    
    public void setBackgroundColor(final BaseColor backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
    
    public void setBackgroundImage(final Image image) {
        this.backgroundImage = image;
    }
    
    public void setBackgroundImage(final Image image, final float width, final float height) {
        this.backgroundImage = image;
        this.backgroundImageWidth = width;
        this.backgroundImageHeight = height;
    }
    
    public float getYLine() {
        return this.yLine;
    }
    
    public int getRunDirection() {
        return this.runDirection;
    }
    
    public void setRunDirection(final int runDirection) {
        this.runDirection = runDirection;
    }
    
    public boolean getKeepTogether() {
        return this.keepTogether;
    }
    
    public void setKeepTogether(final boolean keepTogether) {
        this.keepTogether = keepTogether;
    }
    
    public PdfDiv() {
        this.left = null;
        this.top = null;
        this.right = null;
        this.bottom = null;
        this.width = null;
        this.height = null;
        this.percentageHeight = null;
        this.percentageWidth = null;
        this.contentWidth = 0.0f;
        this.contentHeight = 0.0f;
        this.textAlignment = -1;
        this.paddingLeft = 0.0f;
        this.paddingRight = 0.0f;
        this.paddingTop = 0.0f;
        this.paddingBottom = 0.0f;
        this.floatType = FloatType.NONE;
        this.position = PositionType.STATIC;
        this.floatLayout = null;
        this.runDirection = 1;
        this.role = PdfName.DIV;
        this.accessibleAttributes = null;
        this.id = new AccessibleElementId();
        this.backgroundColor = null;
        this.content = new ArrayList<Element>();
        this.keepTogether = false;
    }
    
    @Override
    public List<Chunk> getChunks() {
        return new ArrayList<Chunk>();
    }
    
    @Override
    public int type() {
        return 37;
    }
    
    @Override
    public boolean isContent() {
        return true;
    }
    
    @Override
    public boolean isNestable() {
        return true;
    }
    
    @Override
    public boolean process(final ElementListener listener) {
        try {
            return listener.add(this);
        }
        catch (DocumentException de) {
            return false;
        }
    }
    
    @Override
    public void setSpacingBefore(final float spacing) {
        this.spacingBefore = spacing;
    }
    
    @Override
    public void setSpacingAfter(final float spacing) {
        this.spacingAfter = spacing;
    }
    
    @Override
    public float getSpacingBefore() {
        return this.spacingBefore;
    }
    
    @Override
    public float getSpacingAfter() {
        return this.spacingAfter;
    }
    
    public int getTextAlignment() {
        return this.textAlignment;
    }
    
    public void setTextAlignment(final int textAlignment) {
        this.textAlignment = textAlignment;
    }
    
    public void addElement(final Element element) {
        this.content.add(element);
    }
    
    public Float getLeft() {
        return this.left;
    }
    
    public void setLeft(final Float left) {
        this.left = left;
    }
    
    public Float getRight() {
        return this.right;
    }
    
    public void setRight(final Float right) {
        this.right = right;
    }
    
    public Float getTop() {
        return this.top;
    }
    
    public void setTop(final Float top) {
        this.top = top;
    }
    
    public Float getBottom() {
        return this.bottom;
    }
    
    public void setBottom(final Float bottom) {
        this.bottom = bottom;
    }
    
    public Float getWidth() {
        return this.width;
    }
    
    public void setWidth(final Float width) {
        this.width = width;
    }
    
    public Float getHeight() {
        return this.height;
    }
    
    public void setHeight(final Float height) {
        this.height = height;
    }
    
    public float getPaddingLeft() {
        return this.paddingLeft;
    }
    
    public void setPaddingLeft(final float paddingLeft) {
        this.paddingLeft = paddingLeft;
    }
    
    public float getPaddingRight() {
        return this.paddingRight;
    }
    
    public void setPaddingRight(final float paddingRight) {
        this.paddingRight = paddingRight;
    }
    
    @Override
    public float getPaddingTop() {
        return this.paddingTop;
    }
    
    @Override
    public void setPaddingTop(final float paddingTop) {
        this.paddingTop = paddingTop;
    }
    
    public float getPaddingBottom() {
        return this.paddingBottom;
    }
    
    public void setPaddingBottom(final float paddingBottom) {
        this.paddingBottom = paddingBottom;
    }
    
    public FloatType getFloatType() {
        return this.floatType;
    }
    
    public void setFloatType(final FloatType floatType) {
        this.floatType = floatType;
    }
    
    public PositionType getPosition() {
        return this.position;
    }
    
    public void setPosition(final PositionType position) {
        this.position = position;
    }
    
    public ArrayList<Element> getContent() {
        return this.content;
    }
    
    public void setContent(final ArrayList<Element> content) {
        this.content = content;
    }
    
    public BorderTopStyle getBorderTopStyle() {
        return this.borderTopStyle;
    }
    
    public void setBorderTopStyle(final BorderTopStyle borderTopStyle) {
        this.borderTopStyle = borderTopStyle;
    }
    
    public int layout(final PdfContentByte canvas, final boolean useAscender, final boolean simulate, final float llx, final float lly, final float urx, final float ury) throws DocumentException {
        float leftX = Math.min(llx, urx);
        final float maxY = Math.max(lly, ury);
        float minY = Math.min(lly, ury);
        float rightX = Math.max(llx, urx);
        this.yLine = maxY;
        boolean contentCutByFixedHeight = false;
        if (this.width != null && this.width > 0.0f) {
            if (this.width < rightX - leftX) {
                rightX = leftX + this.width;
            }
            else if (this.width > rightX - leftX) {
                return 2;
            }
        }
        else if (this.percentageWidth != null) {
            this.contentWidth = (rightX - leftX) * this.percentageWidth;
            rightX = leftX + this.contentWidth;
        }
        else if (this.percentageWidth == null && this.floatType == FloatType.NONE && (this.display == null || this.display == DisplayType.BLOCK || this.display == DisplayType.LIST_ITEM || this.display == DisplayType.RUN_IN)) {
            this.contentWidth = rightX - leftX;
        }
        if (this.height != null && this.height > 0.0f) {
            if (this.height < maxY - minY) {
                minY = maxY - this.height;
                contentCutByFixedHeight = true;
            }
            else if (this.height > maxY - minY) {
                return 2;
            }
        }
        else if (this.percentageHeight != null) {
            if (this.percentageHeight < 1.0) {
                contentCutByFixedHeight = true;
            }
            this.contentHeight = (maxY - minY) * this.percentageHeight;
            minY = maxY - this.contentHeight;
        }
        if (!simulate && this.position == PositionType.RELATIVE) {
            Float translationX = null;
            if (this.left != null) {
                translationX = this.left;
            }
            else if (this.right != null) {
                translationX = -this.right;
            }
            else {
                translationX = 0.0f;
            }
            Float translationY = null;
            if (this.top != null) {
                translationY = -this.top;
            }
            else if (this.bottom != null) {
                translationY = this.bottom;
            }
            else {
                translationY = 0.0f;
            }
            canvas.saveState();
            canvas.transform(new AffineTransform(1.0f, 0.0f, 0.0f, 1.0f, translationX, translationY));
        }
        if (!simulate && (this.backgroundColor != null || this.backgroundImage != null) && this.getActualWidth() > 0.0f && this.getActualHeight() > 0.0f) {
            float backgroundWidth = this.getActualWidth();
            float backgroundHeight = this.getActualHeight();
            if (this.width != null) {
                backgroundWidth = ((this.width > 0.0f) ? this.width : 0.0f);
            }
            if (this.height != null) {
                backgroundHeight = ((this.height > 0.0f) ? this.height : 0.0f);
            }
            if (backgroundWidth > 0.0f && backgroundHeight > 0.0f) {
                final Rectangle background = new Rectangle(leftX, maxY - backgroundHeight, backgroundWidth + leftX, maxY);
                if (this.backgroundColor != null) {
                    background.setBackgroundColor(this.backgroundColor);
                    final PdfArtifact artifact = new PdfArtifact();
                    canvas.openMCBlock(artifact);
                    canvas.rectangle(background);
                    canvas.closeMCBlock(artifact);
                }
                if (this.backgroundImage != null) {
                    if (this.backgroundImageWidth == null) {
                        this.backgroundImage.scaleToFit(background);
                    }
                    else {
                        this.backgroundImage.scaleAbsolute(this.backgroundImageWidth, this.backgroundImageHeight);
                    }
                    this.backgroundImage.setAbsolutePosition(background.getLeft(), background.getBottom());
                    canvas.openMCBlock(this.backgroundImage);
                    canvas.addImage(this.backgroundImage);
                    canvas.closeMCBlock(this.backgroundImage);
                }
            }
        }
        if (this.percentageWidth == null) {
            this.contentWidth = 0.0f;
        }
        if (this.percentageHeight == null) {
            this.contentHeight = 0.0f;
        }
        minY += this.paddingBottom;
        leftX += this.paddingLeft;
        rightX -= this.paddingRight;
        this.yLine -= this.paddingTop;
        int status = 1;
        if (!this.content.isEmpty()) {
            if (this.floatLayout == null) {
                final ArrayList<Element> floatingElements = new ArrayList<Element>(this.content);
                (this.floatLayout = new FloatLayout(floatingElements, useAscender)).setRunDirection(this.runDirection);
            }
            this.floatLayout.setSimpleColumn(leftX, minY, rightX, this.yLine);
            if (this.getBorderTopStyle() != null) {
                this.floatLayout.compositeColumn.setIgnoreSpacingBefore(false);
            }
            status = this.floatLayout.layout(canvas, simulate);
            this.yLine = this.floatLayout.getYLine();
            if (this.percentageWidth == null && this.contentWidth < this.floatLayout.getFilledWidth()) {
                this.contentWidth = this.floatLayout.getFilledWidth();
            }
        }
        if (!simulate && this.position == PositionType.RELATIVE) {
            canvas.restoreState();
        }
        this.yLine -= this.paddingBottom;
        if (this.percentageHeight == null) {
            this.contentHeight = maxY - this.yLine;
        }
        if (this.percentageWidth == null) {
            this.contentWidth += this.paddingLeft + this.paddingRight;
        }
        return contentCutByFixedHeight ? 1 : status;
    }
    
    @Override
    public PdfObject getAccessibleAttribute(final PdfName key) {
        if (this.accessibleAttributes != null) {
            return this.accessibleAttributes.get(key);
        }
        return null;
    }
    
    @Override
    public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
        if (this.accessibleAttributes == null) {
            this.accessibleAttributes = new HashMap<PdfName, PdfObject>();
        }
        this.accessibleAttributes.put(key, value);
    }
    
    @Override
    public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
        return this.accessibleAttributes;
    }
    
    @Override
    public PdfName getRole() {
        return this.role;
    }
    
    @Override
    public void setRole(final PdfName role) {
        this.role = role;
    }
    
    @Override
    public AccessibleElementId getId() {
        return this.id;
    }
    
    @Override
    public void setId(final AccessibleElementId id) {
        this.id = id;
    }
    
    @Override
    public boolean isInline() {
        return false;
    }
    
    public enum FloatType
    {
        NONE, 
        LEFT, 
        RIGHT;
    }
    
    public enum PositionType
    {
        STATIC, 
        ABSOLUTE, 
        FIXED, 
        RELATIVE;
    }
    
    public enum DisplayType
    {
        NONE, 
        BLOCK, 
        INLINE, 
        INLINE_BLOCK, 
        INLINE_TABLE, 
        LIST_ITEM, 
        RUN_IN, 
        TABLE, 
        TABLE_CAPTION, 
        TABLE_CELL, 
        TABLE_COLUMN_GROUP, 
        TABLE_COLUMN, 
        TABLE_FOOTER_GROUP, 
        TABLE_HEADER_GROUP, 
        TABLE_ROW, 
        TABLE_ROW_GROUP;
    }
    
    public enum BorderTopStyle
    {
        DOTTED, 
        DASHED, 
        SOLID, 
        DOUBLE, 
        GROOVE, 
        RIDGE, 
        INSET, 
        OUTSET;
    }
}
