// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.DocWriter;

public class StringUtils
{
    private static final byte[] r;
    private static final byte[] n;
    private static final byte[] t;
    private static final byte[] b;
    private static final byte[] f;
    
    private StringUtils() {
    }
    
    public static byte[] escapeString(final byte[] bytes) {
        final ByteBuffer content = new ByteBuffer();
        escapeString(bytes, content);
        return content.toByteArray();
    }
    
    public static void escapeString(final byte[] bytes, final ByteBuffer content) {
        content.append_i(40);
        for (int k = 0; k < bytes.length; ++k) {
            final byte c = bytes[k];
            switch (c) {
                case 13: {
                    content.append(StringUtils.r);
                    break;
                }
                case 10: {
                    content.append(StringUtils.n);
                    break;
                }
                case 9: {
                    content.append(StringUtils.t);
                    break;
                }
                case 8: {
                    content.append(StringUtils.b);
                    break;
                }
                case 12: {
                    content.append(StringUtils.f);
                    break;
                }
                case 40:
                case 41:
                case 92: {
                    content.append_i(92).append_i(c);
                    break;
                }
                default: {
                    content.append_i(c);
                    break;
                }
            }
        }
        content.append_i(41);
    }
    
    public static byte[] convertCharsToBytes(final char[] chars) {
        final byte[] result = new byte[chars.length * 2];
        for (int i = 0; i < chars.length; ++i) {
            result[2 * i] = (byte)(chars[i] / '\u0100');
            result[2 * i + 1] = (byte)(chars[i] % '\u0100');
        }
        return result;
    }
    
    static {
        r = DocWriter.getISOBytes("\\r");
        n = DocWriter.getISOBytes("\\n");
        t = DocWriter.getISOBytes("\\t");
        b = DocWriter.getISOBytes("\\b");
        f = DocWriter.getISOBytes("\\f");
    }
}
