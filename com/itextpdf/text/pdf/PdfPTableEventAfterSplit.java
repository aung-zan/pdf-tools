// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public interface PdfPTableEventAfterSplit extends PdfPTableEventSplit
{
    void afterSplitTable(final PdfPTable p0, final PdfPRow p1, final int p2);
}
