// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.Set;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.io.OutputStream;
import java.util.LinkedHashMap;

public class PdfDictionary extends PdfObject
{
    public static final PdfName FONT;
    public static final PdfName OUTLINES;
    public static final PdfName PAGE;
    public static final PdfName PAGES;
    public static final PdfName CATALOG;
    private PdfName dictionaryType;
    protected LinkedHashMap<PdfName, PdfObject> hashMap;
    
    public PdfDictionary() {
        super(6);
        this.dictionaryType = null;
        this.hashMap = new LinkedHashMap<PdfName, PdfObject>();
    }
    
    public PdfDictionary(final int capacity) {
        super(6);
        this.dictionaryType = null;
        this.hashMap = new LinkedHashMap<PdfName, PdfObject>(capacity);
    }
    
    public PdfDictionary(final PdfName type) {
        this();
        this.dictionaryType = type;
        this.put(PdfName.TYPE, this.dictionaryType);
    }
    
    @Override
    public void toPdf(final PdfWriter writer, final OutputStream os) throws IOException {
        PdfWriter.checkPdfIsoConformance(writer, 11, this);
        os.write(60);
        os.write(60);
        int type = 0;
        for (final Map.Entry<PdfName, PdfObject> e : this.hashMap.entrySet()) {
            e.getKey().toPdf(writer, os);
            final PdfObject value = e.getValue();
            type = value.type();
            if (type != 5 && type != 6 && type != 4 && type != 3) {
                os.write(32);
            }
            value.toPdf(writer, os);
        }
        os.write(62);
        os.write(62);
    }
    
    @Override
    public String toString() {
        if (this.get(PdfName.TYPE) == null) {
            return "Dictionary";
        }
        return "Dictionary of type: " + this.get(PdfName.TYPE);
    }
    
    public void put(final PdfName key, final PdfObject object) {
        if (key == null) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("key.is.null", new Object[0]));
        }
        if (object == null || object.isNull()) {
            this.hashMap.remove(key);
        }
        else {
            this.hashMap.put(key, object);
        }
    }
    
    public void putEx(final PdfName key, final PdfObject value) {
        if (key == null) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("key.is.null", new Object[0]));
        }
        if (value == null) {
            return;
        }
        this.put(key, value);
    }
    
    public void putAll(final PdfDictionary dic) {
        this.hashMap.putAll((Map<?, ?>)dic.hashMap);
    }
    
    public void remove(final PdfName key) {
        this.hashMap.remove(key);
    }
    
    public void clear() {
        this.hashMap.clear();
    }
    
    public PdfObject get(final PdfName key) {
        return this.hashMap.get(key);
    }
    
    public PdfObject getDirectObject(final PdfName key) {
        return PdfReader.getPdfObject(this.get(key));
    }
    
    public Set<PdfName> getKeys() {
        return this.hashMap.keySet();
    }
    
    public int size() {
        return this.hashMap.size();
    }
    
    public boolean contains(final PdfName key) {
        return this.hashMap.containsKey(key);
    }
    
    public boolean isFont() {
        return this.checkType(PdfDictionary.FONT);
    }
    
    public boolean isPage() {
        return this.checkType(PdfDictionary.PAGE);
    }
    
    public boolean isPages() {
        return this.checkType(PdfDictionary.PAGES);
    }
    
    public boolean isCatalog() {
        return this.checkType(PdfDictionary.CATALOG);
    }
    
    public boolean isOutlineTree() {
        return this.checkType(PdfDictionary.OUTLINES);
    }
    
    public boolean checkType(final PdfName type) {
        if (type == null) {
            return false;
        }
        if (this.dictionaryType == null) {
            this.dictionaryType = this.getAsName(PdfName.TYPE);
        }
        return type.equals(this.dictionaryType);
    }
    
    public void merge(final PdfDictionary other) {
        this.hashMap.putAll((Map<?, ?>)other.hashMap);
    }
    
    public void mergeDifferent(final PdfDictionary other) {
        for (final PdfName key : other.hashMap.keySet()) {
            if (!this.hashMap.containsKey(key)) {
                this.hashMap.put(key, other.hashMap.get(key));
            }
        }
    }
    
    public PdfDictionary getAsDict(final PdfName key) {
        PdfDictionary dict = null;
        final PdfObject orig = this.getDirectObject(key);
        if (orig != null && orig.isDictionary()) {
            dict = (PdfDictionary)orig;
        }
        return dict;
    }
    
    public PdfArray getAsArray(final PdfName key) {
        PdfArray array = null;
        final PdfObject orig = this.getDirectObject(key);
        if (orig != null && orig.isArray()) {
            array = (PdfArray)orig;
        }
        return array;
    }
    
    public PdfStream getAsStream(final PdfName key) {
        PdfStream stream = null;
        final PdfObject orig = this.getDirectObject(key);
        if (orig != null && orig.isStream()) {
            stream = (PdfStream)orig;
        }
        return stream;
    }
    
    public PdfString getAsString(final PdfName key) {
        PdfString string = null;
        final PdfObject orig = this.getDirectObject(key);
        if (orig != null && orig.isString()) {
            string = (PdfString)orig;
        }
        return string;
    }
    
    public PdfNumber getAsNumber(final PdfName key) {
        PdfNumber number = null;
        final PdfObject orig = this.getDirectObject(key);
        if (orig != null && orig.isNumber()) {
            number = (PdfNumber)orig;
        }
        return number;
    }
    
    public PdfName getAsName(final PdfName key) {
        PdfName name = null;
        final PdfObject orig = this.getDirectObject(key);
        if (orig != null && orig.isName()) {
            name = (PdfName)orig;
        }
        return name;
    }
    
    public PdfBoolean getAsBoolean(final PdfName key) {
        PdfBoolean bool = null;
        final PdfObject orig = this.getDirectObject(key);
        if (orig != null && orig.isBoolean()) {
            bool = (PdfBoolean)orig;
        }
        return bool;
    }
    
    public PdfIndirectReference getAsIndirectObject(final PdfName key) {
        PdfIndirectReference ref = null;
        final PdfObject orig = this.get(key);
        if (orig != null && orig.isIndirect()) {
            ref = (PdfIndirectReference)orig;
        }
        return ref;
    }
    
    static {
        FONT = PdfName.FONT;
        OUTLINES = PdfName.OUTLINES;
        PAGE = PdfName.PAGE;
        PAGES = PdfName.PAGES;
        CATALOG = PdfName.CATALOG;
    }
}
