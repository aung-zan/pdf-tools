// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public interface ExtraEncoding
{
    byte[] charToByte(final String p0, final String p1);
    
    byte[] charToByte(final char p0, final String p1);
    
    String byteToChar(final byte[] p0, final String p1);
}
