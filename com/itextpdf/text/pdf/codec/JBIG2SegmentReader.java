// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.codec;

import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.io.IOException;
import java.util.TreeSet;
import java.util.TreeMap;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import java.util.SortedSet;
import java.util.SortedMap;

public class JBIG2SegmentReader
{
    public static final int SYMBOL_DICTIONARY = 0;
    public static final int INTERMEDIATE_TEXT_REGION = 4;
    public static final int IMMEDIATE_TEXT_REGION = 6;
    public static final int IMMEDIATE_LOSSLESS_TEXT_REGION = 7;
    public static final int PATTERN_DICTIONARY = 16;
    public static final int INTERMEDIATE_HALFTONE_REGION = 20;
    public static final int IMMEDIATE_HALFTONE_REGION = 22;
    public static final int IMMEDIATE_LOSSLESS_HALFTONE_REGION = 23;
    public static final int INTERMEDIATE_GENERIC_REGION = 36;
    public static final int IMMEDIATE_GENERIC_REGION = 38;
    public static final int IMMEDIATE_LOSSLESS_GENERIC_REGION = 39;
    public static final int INTERMEDIATE_GENERIC_REFINEMENT_REGION = 40;
    public static final int IMMEDIATE_GENERIC_REFINEMENT_REGION = 42;
    public static final int IMMEDIATE_LOSSLESS_GENERIC_REFINEMENT_REGION = 43;
    public static final int PAGE_INFORMATION = 48;
    public static final int END_OF_PAGE = 49;
    public static final int END_OF_STRIPE = 50;
    public static final int END_OF_FILE = 51;
    public static final int PROFILES = 52;
    public static final int TABLES = 53;
    public static final int EXTENSION = 62;
    private final SortedMap<Integer, JBIG2Segment> segments;
    private final SortedMap<Integer, JBIG2Page> pages;
    private final SortedSet<JBIG2Segment> globals;
    private RandomAccessFileOrArray ra;
    private boolean sequential;
    private boolean number_of_pages_known;
    private int number_of_pages;
    private boolean read;
    
    public JBIG2SegmentReader(final RandomAccessFileOrArray ra) throws IOException {
        this.segments = new TreeMap<Integer, JBIG2Segment>();
        this.pages = new TreeMap<Integer, JBIG2Page>();
        this.globals = new TreeSet<JBIG2Segment>();
        this.number_of_pages = -1;
        this.read = false;
        this.ra = ra;
    }
    
    public static byte[] copyByteArray(final byte[] b) {
        final byte[] bc = new byte[b.length];
        System.arraycopy(b, 0, bc, 0, b.length);
        return bc;
    }
    
    public void read() throws IOException {
        if (this.read) {
            throw new IllegalStateException(MessageLocalization.getComposedMessage("already.attempted.a.read.on.this.jbig2.file", new Object[0]));
        }
        this.read = true;
        this.readFileHeader();
        if (this.sequential) {
            do {
                final JBIG2Segment tmp = this.readHeader();
                this.readSegment(tmp);
                this.segments.put(tmp.segmentNumber, tmp);
            } while (this.ra.getFilePointer() < this.ra.length());
        }
        else {
            JBIG2Segment tmp;
            do {
                tmp = this.readHeader();
                this.segments.put(tmp.segmentNumber, tmp);
            } while (tmp.type != 51);
            final Iterator<Integer> segs = this.segments.keySet().iterator();
            while (segs.hasNext()) {
                this.readSegment(this.segments.get(segs.next()));
            }
        }
    }
    
    void readSegment(final JBIG2Segment s) throws IOException {
        final int ptr = (int)this.ra.getFilePointer();
        if (s.dataLength == 4294967295L) {
            return;
        }
        final byte[] data = new byte[(int)s.dataLength];
        this.ra.read(data);
        s.data = data;
        if (s.type == 48) {
            final int last = (int)this.ra.getFilePointer();
            this.ra.seek(ptr);
            final int page_bitmap_width = this.ra.readInt();
            final int page_bitmap_height = this.ra.readInt();
            this.ra.seek(last);
            final JBIG2Page p = this.pages.get(s.page);
            if (p == null) {
                throw new IllegalStateException(MessageLocalization.getComposedMessage("referring.to.widht.height.of.page.we.havent.seen.yet.1", s.page));
            }
            p.pageBitmapWidth = page_bitmap_width;
            p.pageBitmapHeight = page_bitmap_height;
        }
    }
    
    JBIG2Segment readHeader() throws IOException {
        final int ptr = (int)this.ra.getFilePointer();
        final int segment_number = this.ra.readInt();
        final JBIG2Segment s = new JBIG2Segment(segment_number);
        final int segment_header_flags = this.ra.read();
        final boolean deferred_non_retain = (segment_header_flags & 0x80) == 0x80;
        s.deferredNonRetain = deferred_non_retain;
        final boolean page_association_size = (segment_header_flags & 0x40) == 0x40;
        final int segment_type = segment_header_flags & 0x3F;
        s.type = segment_type;
        int referred_to_byte0 = this.ra.read();
        int count_of_referred_to_segments = (referred_to_byte0 & 0xE0) >> 5;
        int[] referred_to_segment_numbers = null;
        boolean[] segment_retention_flags = null;
        if (count_of_referred_to_segments == 7) {
            this.ra.seek(this.ra.getFilePointer() - 1L);
            count_of_referred_to_segments = (this.ra.readInt() & 0x1FFFFFFF);
            segment_retention_flags = new boolean[count_of_referred_to_segments + 1];
            int i = 0;
            int referred_to_current_byte = 0;
            do {
                final int j = i % 8;
                if (j == 0) {
                    referred_to_current_byte = this.ra.read();
                }
                segment_retention_flags[i] = ((1 << j & referred_to_current_byte) >> j == 1);
            } while (++i <= count_of_referred_to_segments);
        }
        else if (count_of_referred_to_segments <= 4) {
            segment_retention_flags = new boolean[count_of_referred_to_segments + 1];
            referred_to_byte0 &= 0x1F;
            for (int i = 0; i <= count_of_referred_to_segments; ++i) {
                segment_retention_flags[i] = ((1 << i & referred_to_byte0) >> i == 1);
            }
        }
        else if (count_of_referred_to_segments == 5 || count_of_referred_to_segments == 6) {
            throw new IllegalStateException(MessageLocalization.getComposedMessage("count.of.referred.to.segments.had.bad.value.in.header.for.segment.1.starting.at.2", String.valueOf(segment_number), String.valueOf(ptr)));
        }
        s.segmentRetentionFlags = segment_retention_flags;
        s.countOfReferredToSegments = count_of_referred_to_segments;
        referred_to_segment_numbers = new int[count_of_referred_to_segments + 1];
        for (int i = 1; i <= count_of_referred_to_segments; ++i) {
            if (segment_number <= 256) {
                referred_to_segment_numbers[i] = this.ra.read();
            }
            else if (segment_number <= 65536) {
                referred_to_segment_numbers[i] = this.ra.readUnsignedShort();
            }
            else {
                referred_to_segment_numbers[i] = (int)this.ra.readUnsignedInt();
            }
        }
        s.referredToSegmentNumbers = referred_to_segment_numbers;
        final int page_association_offset = (int)this.ra.getFilePointer() - ptr;
        int segment_page_association;
        if (page_association_size) {
            segment_page_association = this.ra.readInt();
        }
        else {
            segment_page_association = this.ra.read();
        }
        if (segment_page_association < 0) {
            throw new IllegalStateException(MessageLocalization.getComposedMessage("page.1.invalid.for.segment.2.starting.at.3", String.valueOf(segment_page_association), String.valueOf(segment_number), String.valueOf(ptr)));
        }
        s.page = segment_page_association;
        s.page_association_size = page_association_size;
        s.page_association_offset = page_association_offset;
        if (segment_page_association > 0 && !this.pages.containsKey(segment_page_association)) {
            this.pages.put(segment_page_association, new JBIG2Page(segment_page_association, this));
        }
        if (segment_page_association > 0) {
            this.pages.get(segment_page_association).addSegment(s);
        }
        else {
            this.globals.add(s);
        }
        final long segment_data_length = this.ra.readUnsignedInt();
        s.dataLength = segment_data_length;
        final int end_ptr = (int)this.ra.getFilePointer();
        this.ra.seek(ptr);
        final byte[] header_data = new byte[end_ptr - ptr];
        this.ra.read(header_data);
        s.headerData = header_data;
        return s;
    }
    
    void readFileHeader() throws IOException {
        this.ra.seek(0L);
        final byte[] idstring = new byte[8];
        this.ra.read(idstring);
        final byte[] refidstring = { -105, 74, 66, 50, 13, 10, 26, 10 };
        for (int i = 0; i < idstring.length; ++i) {
            if (idstring[i] != refidstring[i]) {
                throw new IllegalStateException(MessageLocalization.getComposedMessage("file.header.idstring.not.good.at.byte.1", i));
            }
        }
        final int fileheaderflags = this.ra.read();
        this.sequential = ((fileheaderflags & 0x1) == 0x1);
        this.number_of_pages_known = ((fileheaderflags & 0x2) == 0x0);
        if ((fileheaderflags & 0xFC) != 0x0) {
            throw new IllegalStateException(MessageLocalization.getComposedMessage("file.header.flags.bits.2.7.not.0", new Object[0]));
        }
        if (this.number_of_pages_known) {
            this.number_of_pages = this.ra.readInt();
        }
    }
    
    public int numberOfPages() {
        return this.pages.size();
    }
    
    public int getPageHeight(final int i) {
        return this.pages.get(i).pageBitmapHeight;
    }
    
    public int getPageWidth(final int i) {
        return this.pages.get(i).pageBitmapWidth;
    }
    
    public JBIG2Page getPage(final int page) {
        return this.pages.get(page);
    }
    
    public byte[] getGlobal(final boolean for_embedding) {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            for (final Object element : this.globals) {
                final JBIG2Segment s = (JBIG2Segment)element;
                if (for_embedding) {
                    if (s.type == 51) {
                        continue;
                    }
                    if (s.type == 49) {
                        continue;
                    }
                }
                os.write(s.headerData);
                os.write(s.data);
            }
            os.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        if (os.size() <= 0) {
            return null;
        }
        return os.toByteArray();
    }
    
    @Override
    public String toString() {
        if (this.read) {
            return "Jbig2SegmentReader: number of pages: " + this.numberOfPages();
        }
        return "Jbig2SegmentReader in indeterminate state.";
    }
    
    public static class JBIG2Segment implements Comparable<JBIG2Segment>
    {
        public final int segmentNumber;
        public long dataLength;
        public int page;
        public int[] referredToSegmentNumbers;
        public boolean[] segmentRetentionFlags;
        public int type;
        public boolean deferredNonRetain;
        public int countOfReferredToSegments;
        public byte[] data;
        public byte[] headerData;
        public boolean page_association_size;
        public int page_association_offset;
        
        public JBIG2Segment(final int segment_number) {
            this.dataLength = -1L;
            this.page = -1;
            this.referredToSegmentNumbers = null;
            this.segmentRetentionFlags = null;
            this.type = -1;
            this.deferredNonRetain = false;
            this.countOfReferredToSegments = -1;
            this.data = null;
            this.headerData = null;
            this.page_association_size = false;
            this.page_association_offset = -1;
            this.segmentNumber = segment_number;
        }
        
        @Override
        public int compareTo(final JBIG2Segment s) {
            return this.segmentNumber - s.segmentNumber;
        }
    }
    
    public static class JBIG2Page
    {
        public final int page;
        private final JBIG2SegmentReader sr;
        private final SortedMap<Integer, JBIG2Segment> segs;
        public int pageBitmapWidth;
        public int pageBitmapHeight;
        
        public JBIG2Page(final int page, final JBIG2SegmentReader sr) {
            this.segs = new TreeMap<Integer, JBIG2Segment>();
            this.pageBitmapWidth = -1;
            this.pageBitmapHeight = -1;
            this.page = page;
            this.sr = sr;
        }
        
        public byte[] getData(final boolean for_embedding) throws IOException {
            final ByteArrayOutputStream os = new ByteArrayOutputStream();
            for (final Integer sn : this.segs.keySet()) {
                final JBIG2Segment s = this.segs.get(sn);
                if (for_embedding) {
                    if (s.type == 51) {
                        continue;
                    }
                    if (s.type == 49) {
                        continue;
                    }
                }
                if (for_embedding) {
                    final byte[] headerData_emb = JBIG2SegmentReader.copyByteArray(s.headerData);
                    if (s.page_association_size) {
                        headerData_emb[s.page_association_offset] = 0;
                        headerData_emb[s.page_association_offset + 1] = 0;
                        headerData_emb[s.page_association_offset + 2] = 0;
                        headerData_emb[s.page_association_offset + 3] = 1;
                    }
                    else {
                        headerData_emb[s.page_association_offset] = 1;
                    }
                    os.write(headerData_emb);
                }
                else {
                    os.write(s.headerData);
                }
                os.write(s.data);
            }
            os.close();
            return os.toByteArray();
        }
        
        public void addSegment(final JBIG2Segment s) {
            this.segs.put(s.segmentNumber, s);
        }
    }
}
