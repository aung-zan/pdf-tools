// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.codec;

import java.io.PrintStream;

public class LZWStringTable
{
    private static final int RES_CODES = 2;
    private static final short HASH_FREE = -1;
    private static final short NEXT_FIRST = -1;
    private static final int MAXBITS = 12;
    private static final int MAXSTR = 4096;
    private static final short HASHSIZE = 9973;
    private static final short HASHSTEP = 2039;
    byte[] strChr_;
    short[] strNxt_;
    short[] strHsh_;
    short numStrings_;
    int[] strLen_;
    
    public LZWStringTable() {
        this.strChr_ = new byte[4096];
        this.strNxt_ = new short[4096];
        this.strLen_ = new int[4096];
        this.strHsh_ = new short[9973];
    }
    
    public int AddCharString(final short index, final byte b) {
        if (this.numStrings_ >= 4096) {
            return 65535;
        }
        int hshidx;
        for (hshidx = Hash(index, b); this.strHsh_[hshidx] != -1; hshidx = (hshidx + 2039) % 9973) {}
        this.strHsh_[hshidx] = this.numStrings_;
        this.strChr_[this.numStrings_] = b;
        if (index == -1) {
            this.strNxt_[this.numStrings_] = -1;
            this.strLen_[this.numStrings_] = 1;
        }
        else {
            this.strNxt_[this.numStrings_] = index;
            this.strLen_[this.numStrings_] = this.strLen_[index] + 1;
        }
        final short numStrings_ = this.numStrings_;
        this.numStrings_ = (short)(numStrings_ + 1);
        return numStrings_;
    }
    
    public short FindCharString(final short index, final byte b) {
        if (index == -1) {
            return (short)(b & 0xFF);
        }
        int nxtidx;
        for (int hshidx = Hash(index, b); (nxtidx = this.strHsh_[hshidx]) != -1; hshidx = (hshidx + 2039) % 9973) {
            if (this.strNxt_[nxtidx] == index && this.strChr_[nxtidx] == b) {
                return (short)nxtidx;
            }
        }
        return -1;
    }
    
    public void ClearTable(final int codesize) {
        this.numStrings_ = 0;
        for (int q = 0; q < 9973; ++q) {
            this.strHsh_[q] = -1;
        }
        for (int w = (1 << codesize) + 2, q2 = 0; q2 < w; ++q2) {
            this.AddCharString((short)(-1), (byte)q2);
        }
    }
    
    public static int Hash(final short index, final byte lastbyte) {
        return (((short)(lastbyte << 8) ^ index) & 0xFFFF) % 9973;
    }
    
    public int expandCode(final byte[] buf, final int offset, short code, int skipHead) {
        if (offset == -2 && skipHead == 1) {
            skipHead = 0;
        }
        if (code == -1 || skipHead == this.strLen_[code]) {
            return 0;
        }
        final int codeLen = this.strLen_[code] - skipHead;
        final int bufSpace = buf.length - offset;
        int expandLen;
        if (bufSpace > codeLen) {
            expandLen = codeLen;
        }
        else {
            expandLen = bufSpace;
        }
        int skipTail = codeLen - expandLen;
        for (int idx = offset + expandLen; idx > offset && code != -1; code = this.strNxt_[code]) {
            if (--skipTail < 0) {
                buf[--idx] = this.strChr_[code];
            }
        }
        if (codeLen > expandLen) {
            return -expandLen;
        }
        return expandLen;
    }
    
    public void dump(final PrintStream out) {
        for (int i = 258; i < this.numStrings_; ++i) {
            out.println(" strNxt_[" + i + "] = " + this.strNxt_[i] + " strChr_ " + Integer.toHexString(this.strChr_[i] & 0xFF) + " strLen_ " + Integer.toHexString(this.strLen_[i]));
        }
    }
}
