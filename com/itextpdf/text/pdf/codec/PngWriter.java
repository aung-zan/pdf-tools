// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.codec;

import com.itextpdf.text.DocWriter;
import java.util.zip.DeflaterOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class PngWriter
{
    private static final byte[] PNG_SIGNTURE;
    private static final byte[] IHDR;
    private static final byte[] PLTE;
    private static final byte[] IDAT;
    private static final byte[] IEND;
    private static final byte[] iCCP;
    private static int[] crc_table;
    private OutputStream outp;
    
    public PngWriter(final OutputStream outp) throws IOException {
        (this.outp = outp).write(PngWriter.PNG_SIGNTURE);
    }
    
    public void writeHeader(final int width, final int height, final int bitDepth, final int colorType) throws IOException {
        final ByteArrayOutputStream ms = new ByteArrayOutputStream();
        outputInt(width, ms);
        outputInt(height, ms);
        ms.write(bitDepth);
        ms.write(colorType);
        ms.write(0);
        ms.write(0);
        ms.write(0);
        this.writeChunk(PngWriter.IHDR, ms.toByteArray());
    }
    
    public void writeEnd() throws IOException {
        this.writeChunk(PngWriter.IEND, new byte[0]);
    }
    
    public void writeData(final byte[] data, final int stride) throws IOException {
        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
        final DeflaterOutputStream zip = new DeflaterOutputStream(stream);
        int k;
        for (k = 0; k < data.length - stride; k += stride) {
            zip.write(0);
            zip.write(data, k, stride);
        }
        final int remaining = data.length - k;
        if (remaining > 0) {
            zip.write(0);
            zip.write(data, k, remaining);
        }
        zip.close();
        this.writeChunk(PngWriter.IDAT, stream.toByteArray());
    }
    
    public void writePalette(final byte[] data) throws IOException {
        this.writeChunk(PngWriter.PLTE, data);
    }
    
    public void writeIccProfile(final byte[] data) throws IOException {
        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
        stream.write(73);
        stream.write(67);
        stream.write(67);
        stream.write(0);
        stream.write(0);
        final DeflaterOutputStream zip = new DeflaterOutputStream(stream);
        zip.write(data);
        zip.close();
        this.writeChunk(PngWriter.iCCP, stream.toByteArray());
    }
    
    private static void make_crc_table() {
        if (PngWriter.crc_table != null) {
            return;
        }
        final int[] crc2 = new int[256];
        for (int n = 0; n < 256; ++n) {
            int c = n;
            for (int k = 0; k < 8; ++k) {
                if ((c & 0x1) != 0x0) {
                    c = (0xEDB88320 ^ c >>> 1);
                }
                else {
                    c >>>= 1;
                }
            }
            crc2[n] = c;
        }
        PngWriter.crc_table = crc2;
    }
    
    private static int update_crc(final int crc, final byte[] buf, final int offset, final int len) {
        int c = crc;
        if (PngWriter.crc_table == null) {
            make_crc_table();
        }
        for (int n = 0; n < len; ++n) {
            c = (PngWriter.crc_table[(c ^ buf[n + offset]) & 0xFF] ^ c >>> 8);
        }
        return c;
    }
    
    private static int crc(final byte[] buf, final int offset, final int len) {
        return ~update_crc(-1, buf, offset, len);
    }
    
    private static int crc(final byte[] buf) {
        return ~update_crc(-1, buf, 0, buf.length);
    }
    
    public void outputInt(final int n) throws IOException {
        outputInt(n, this.outp);
    }
    
    public static void outputInt(final int n, final OutputStream s) throws IOException {
        s.write((byte)(n >> 24));
        s.write((byte)(n >> 16));
        s.write((byte)(n >> 8));
        s.write((byte)n);
    }
    
    public void writeChunk(final byte[] chunkType, final byte[] data) throws IOException {
        this.outputInt(data.length);
        this.outp.write(chunkType, 0, 4);
        this.outp.write(data);
        int c = update_crc(-1, chunkType, 0, chunkType.length);
        c = ~update_crc(c, data, 0, data.length);
        this.outputInt(c);
    }
    
    static {
        PNG_SIGNTURE = new byte[] { -119, 80, 78, 71, 13, 10, 26, 10 };
        IHDR = DocWriter.getISOBytes("IHDR");
        PLTE = DocWriter.getISOBytes("PLTE");
        IDAT = DocWriter.getISOBytes("IDAT");
        IEND = DocWriter.getISOBytes("IEND");
        iCCP = DocWriter.getISOBytes("iCCP");
    }
}
