// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.codec;

import java.io.EOFException;
import java.util.Enumeration;
import java.util.ArrayList;
import java.io.IOException;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import java.util.Hashtable;
import java.io.Serializable;

public class TIFFDirectory implements Serializable
{
    private static final long serialVersionUID = -168636766193675380L;
    boolean isBigEndian;
    int numEntries;
    TIFFField[] fields;
    Hashtable<Integer, Integer> fieldIndex;
    long IFDOffset;
    long nextIFDOffset;
    private static final int[] sizeOfType;
    
    TIFFDirectory() {
        this.fieldIndex = new Hashtable<Integer, Integer>();
        this.IFDOffset = 8L;
        this.nextIFDOffset = 0L;
    }
    
    private static boolean isValidEndianTag(final int endian) {
        return endian == 18761 || endian == 19789;
    }
    
    public TIFFDirectory(final RandomAccessFileOrArray stream, final int directory) throws IOException {
        this.fieldIndex = new Hashtable<Integer, Integer>();
        this.IFDOffset = 8L;
        this.nextIFDOffset = 0L;
        final long global_save_offset = stream.getFilePointer();
        stream.seek(0L);
        final int endian = stream.readUnsignedShort();
        if (!isValidEndianTag(endian)) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("bad.endianness.tag.not.0x4949.or.0x4d4d", new Object[0]));
        }
        this.isBigEndian = (endian == 19789);
        final int magic = this.readUnsignedShort(stream);
        if (magic != 42) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("bad.magic.number.should.be.42", new Object[0]));
        }
        long ifd_offset = this.readUnsignedInt(stream);
        for (int i = 0; i < directory; ++i) {
            if (ifd_offset == 0L) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("directory.number.too.large", new Object[0]));
            }
            stream.seek(ifd_offset);
            final int entries = this.readUnsignedShort(stream);
            stream.skip(12 * entries);
            ifd_offset = this.readUnsignedInt(stream);
        }
        stream.seek(ifd_offset);
        this.initialize(stream);
        stream.seek(global_save_offset);
    }
    
    public TIFFDirectory(final RandomAccessFileOrArray stream, long ifd_offset, final int directory) throws IOException {
        this.fieldIndex = new Hashtable<Integer, Integer>();
        this.IFDOffset = 8L;
        this.nextIFDOffset = 0L;
        final long global_save_offset = stream.getFilePointer();
        stream.seek(0L);
        final int endian = stream.readUnsignedShort();
        if (!isValidEndianTag(endian)) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("bad.endianness.tag.not.0x4949.or.0x4d4d", new Object[0]));
        }
        this.isBigEndian = (endian == 19789);
        stream.seek(ifd_offset);
        for (int dirNum = 0; dirNum < directory; ++dirNum) {
            final int numEntries = this.readUnsignedShort(stream);
            stream.seek(ifd_offset + 12 * numEntries);
            ifd_offset = this.readUnsignedInt(stream);
            stream.seek(ifd_offset);
        }
        this.initialize(stream);
        stream.seek(global_save_offset);
    }
    
    private void initialize(final RandomAccessFileOrArray stream) throws IOException {
        long nextTagOffset = 0L;
        final long maxOffset = stream.length();
        this.IFDOffset = stream.getFilePointer();
        this.numEntries = this.readUnsignedShort(stream);
        this.fields = new TIFFField[this.numEntries];
        for (int i = 0; i < this.numEntries && nextTagOffset < maxOffset; ++i) {
            final int tag = this.readUnsignedShort(stream);
            final int type = this.readUnsignedShort(stream);
            int count = (int)this.readUnsignedInt(stream);
            boolean processTag = true;
            nextTagOffset = stream.getFilePointer() + 4L;
            try {
                if (count * TIFFDirectory.sizeOfType[type] > 4) {
                    final long valueOffset = this.readUnsignedInt(stream);
                    if (valueOffset < maxOffset) {
                        stream.seek(valueOffset);
                    }
                    else {
                        processTag = false;
                    }
                }
            }
            catch (ArrayIndexOutOfBoundsException ae) {
                processTag = false;
            }
            if (processTag) {
                this.fieldIndex.put(tag, i);
                Object obj = null;
                switch (type) {
                    case 1:
                    case 2:
                    case 6:
                    case 7: {
                        final byte[] bvalues = new byte[count];
                        stream.readFully(bvalues, 0, count);
                        if (type == 2) {
                            int index = 0;
                            int prevIndex = 0;
                            final ArrayList<String> v = new ArrayList<String>();
                            while (index < count) {
                                while (index < count && bvalues[index++] != 0) {}
                                v.add(new String(bvalues, prevIndex, index - prevIndex));
                                prevIndex = index;
                            }
                            count = v.size();
                            final String[] strings = new String[count];
                            for (int c = 0; c < count; ++c) {
                                strings[c] = v.get(c);
                            }
                            obj = strings;
                            break;
                        }
                        obj = bvalues;
                        break;
                    }
                    case 3: {
                        final char[] cvalues = new char[count];
                        for (int j = 0; j < count; ++j) {
                            cvalues[j] = (char)this.readUnsignedShort(stream);
                        }
                        obj = cvalues;
                        break;
                    }
                    case 4: {
                        final long[] lvalues = new long[count];
                        for (int j = 0; j < count; ++j) {
                            lvalues[j] = this.readUnsignedInt(stream);
                        }
                        obj = lvalues;
                        break;
                    }
                    case 5: {
                        final long[][] llvalues = new long[count][2];
                        for (int j = 0; j < count; ++j) {
                            llvalues[j][0] = this.readUnsignedInt(stream);
                            llvalues[j][1] = this.readUnsignedInt(stream);
                        }
                        obj = llvalues;
                        break;
                    }
                    case 8: {
                        final short[] svalues = new short[count];
                        for (int j = 0; j < count; ++j) {
                            svalues[j] = this.readShort(stream);
                        }
                        obj = svalues;
                        break;
                    }
                    case 9: {
                        final int[] ivalues = new int[count];
                        for (int j = 0; j < count; ++j) {
                            ivalues[j] = this.readInt(stream);
                        }
                        obj = ivalues;
                        break;
                    }
                    case 10: {
                        final int[][] iivalues = new int[count][2];
                        for (int j = 0; j < count; ++j) {
                            iivalues[j][0] = this.readInt(stream);
                            iivalues[j][1] = this.readInt(stream);
                        }
                        obj = iivalues;
                        break;
                    }
                    case 11: {
                        final float[] fvalues = new float[count];
                        for (int j = 0; j < count; ++j) {
                            fvalues[j] = this.readFloat(stream);
                        }
                        obj = fvalues;
                        break;
                    }
                    case 12: {
                        final double[] dvalues = new double[count];
                        for (int j = 0; j < count; ++j) {
                            dvalues[j] = this.readDouble(stream);
                        }
                        obj = dvalues;
                        break;
                    }
                }
                this.fields[i] = new TIFFField(tag, type, count, obj);
            }
            stream.seek(nextTagOffset);
        }
        try {
            this.nextIFDOffset = this.readUnsignedInt(stream);
        }
        catch (Exception e) {
            this.nextIFDOffset = 0L;
        }
    }
    
    public int getNumEntries() {
        return this.numEntries;
    }
    
    public TIFFField getField(final int tag) {
        final Integer i = this.fieldIndex.get(tag);
        if (i == null) {
            return null;
        }
        return this.fields[i];
    }
    
    public boolean isTagPresent(final int tag) {
        return this.fieldIndex.containsKey(tag);
    }
    
    public int[] getTags() {
        final int[] tags = new int[this.fieldIndex.size()];
        final Enumeration<Integer> e = this.fieldIndex.keys();
        int i = 0;
        while (e.hasMoreElements()) {
            tags[i++] = e.nextElement();
        }
        return tags;
    }
    
    public TIFFField[] getFields() {
        return this.fields;
    }
    
    public byte getFieldAsByte(final int tag, final int index) {
        final Integer i = this.fieldIndex.get(tag);
        final byte[] b = this.fields[i].getAsBytes();
        return b[index];
    }
    
    public byte getFieldAsByte(final int tag) {
        return this.getFieldAsByte(tag, 0);
    }
    
    public long getFieldAsLong(final int tag, final int index) {
        final Integer i = this.fieldIndex.get(tag);
        return this.fields[i].getAsLong(index);
    }
    
    public long getFieldAsLong(final int tag) {
        return this.getFieldAsLong(tag, 0);
    }
    
    public float getFieldAsFloat(final int tag, final int index) {
        final Integer i = this.fieldIndex.get(tag);
        return this.fields[i].getAsFloat(index);
    }
    
    public float getFieldAsFloat(final int tag) {
        return this.getFieldAsFloat(tag, 0);
    }
    
    public double getFieldAsDouble(final int tag, final int index) {
        final Integer i = this.fieldIndex.get(tag);
        return this.fields[i].getAsDouble(index);
    }
    
    public double getFieldAsDouble(final int tag) {
        return this.getFieldAsDouble(tag, 0);
    }
    
    private short readShort(final RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readShort();
        }
        return stream.readShortLE();
    }
    
    private int readUnsignedShort(final RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readUnsignedShort();
        }
        return stream.readUnsignedShortLE();
    }
    
    private int readInt(final RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readInt();
        }
        return stream.readIntLE();
    }
    
    private long readUnsignedInt(final RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readUnsignedInt();
        }
        return stream.readUnsignedIntLE();
    }
    
    private long readLong(final RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readLong();
        }
        return stream.readLongLE();
    }
    
    private float readFloat(final RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readFloat();
        }
        return stream.readFloatLE();
    }
    
    private double readDouble(final RandomAccessFileOrArray stream) throws IOException {
        if (this.isBigEndian) {
            return stream.readDouble();
        }
        return stream.readDoubleLE();
    }
    
    private static int readUnsignedShort(final RandomAccessFileOrArray stream, final boolean isBigEndian) throws IOException {
        if (isBigEndian) {
            return stream.readUnsignedShort();
        }
        return stream.readUnsignedShortLE();
    }
    
    private static long readUnsignedInt(final RandomAccessFileOrArray stream, final boolean isBigEndian) throws IOException {
        if (isBigEndian) {
            return stream.readUnsignedInt();
        }
        return stream.readUnsignedIntLE();
    }
    
    public static int getNumDirectories(final RandomAccessFileOrArray stream) throws IOException {
        final long pointer = stream.getFilePointer();
        stream.seek(0L);
        final int endian = stream.readUnsignedShort();
        if (!isValidEndianTag(endian)) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("bad.endianness.tag.not.0x4949.or.0x4d4d", new Object[0]));
        }
        final boolean isBigEndian = endian == 19789;
        final int magic = readUnsignedShort(stream, isBigEndian);
        if (magic != 42) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("bad.magic.number.should.be.42", new Object[0]));
        }
        stream.seek(4L);
        long offset = readUnsignedInt(stream, isBigEndian);
        int numDirectories = 0;
        while (offset != 0L) {
            ++numDirectories;
            try {
                stream.seek(offset);
                final int entries = readUnsignedShort(stream, isBigEndian);
                stream.skip(12 * entries);
                offset = readUnsignedInt(stream, isBigEndian);
                continue;
            }
            catch (EOFException eof) {
                --numDirectories;
            }
            break;
        }
        stream.seek(pointer);
        return numDirectories;
    }
    
    public boolean isBigEndian() {
        return this.isBigEndian;
    }
    
    public long getIFDOffset() {
        return this.IFDOffset;
    }
    
    public long getNextIFDOffset() {
        return this.nextIFDOffset;
    }
    
    static {
        sizeOfType = new int[] { 0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8 };
    }
}
