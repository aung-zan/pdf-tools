// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.codec;

import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.ImgRaw;
import com.itextpdf.text.Jpeg;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.DeflaterOutputStream;
import java.io.ByteArrayOutputStream;
import com.itextpdf.text.pdf.ICC_Profile;
import com.itextpdf.text.exceptions.InvalidImageException;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.Image;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;

public class TiffImage
{
    public static int getNumberOfPages(final RandomAccessFileOrArray s) {
        try {
            return TIFFDirectory.getNumDirectories(s);
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    static int getDpi(final TIFFField fd, final int resolutionUnit) {
        if (fd == null) {
            return 0;
        }
        final long[] res = fd.getAsRational(0);
        final float frac = res[0] / (float)res[1];
        int dpi = 0;
        switch (resolutionUnit) {
            case 1:
            case 2: {
                dpi = (int)(frac + 0.5);
                break;
            }
            case 3: {
                dpi = (int)(frac * 2.54 + 0.5);
                break;
            }
        }
        return dpi;
    }
    
    public static Image getTiffImage(final RandomAccessFileOrArray s, final boolean recoverFromImageError, final int page, final boolean direct) {
        if (page < 1) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.page.number.must.be.gt.eq.1", new Object[0]));
        }
        try {
            final TIFFDirectory dir = new TIFFDirectory(s, page - 1);
            if (dir.isTagPresent(322)) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("tiles.are.not.supported", new Object[0]));
            }
            int compression = 1;
            if (dir.isTagPresent(259)) {
                compression = (int)dir.getFieldAsLong(259);
            }
            switch (compression) {
                case 2:
                case 3:
                case 4:
                case 32771: {
                    float rotation = 0.0f;
                    if (dir.isTagPresent(274)) {
                        final int rot = (int)dir.getFieldAsLong(274);
                        if (rot == 3 || rot == 4) {
                            rotation = 3.1415927f;
                        }
                        else if (rot == 5 || rot == 8) {
                            rotation = 1.5707964f;
                        }
                        else if (rot == 6 || rot == 7) {
                            rotation = -1.5707964f;
                        }
                    }
                    Image img = null;
                    long tiffT4Options = 0L;
                    long tiffT6Options = 0L;
                    long fillOrder = 1L;
                    final int h = (int)dir.getFieldAsLong(257);
                    final int w = (int)dir.getFieldAsLong(256);
                    int dpiX = 0;
                    int dpiY = 0;
                    float XYRatio = 0.0f;
                    int resolutionUnit = 2;
                    if (dir.isTagPresent(296)) {
                        resolutionUnit = (int)dir.getFieldAsLong(296);
                    }
                    dpiX = getDpi(dir.getField(282), resolutionUnit);
                    dpiY = getDpi(dir.getField(283), resolutionUnit);
                    if (resolutionUnit == 1) {
                        if (dpiY != 0) {
                            XYRatio = dpiX / (float)dpiY;
                        }
                        dpiX = 0;
                        dpiY = 0;
                    }
                    int rowsStrip = h;
                    if (dir.isTagPresent(278)) {
                        rowsStrip = (int)dir.getFieldAsLong(278);
                    }
                    if (rowsStrip <= 0 || rowsStrip > h) {
                        rowsStrip = h;
                    }
                    final long[] offset = getArrayLongShort(dir, 273);
                    long[] size = getArrayLongShort(dir, 279);
                    if ((size == null || (size.length == 1 && (size[0] == 0L || size[0] + offset[0] > s.length()))) && h == rowsStrip) {
                        size = new long[] { s.length() - (int)offset[0] };
                    }
                    boolean reverse = false;
                    final TIFFField fillOrderField = dir.getField(266);
                    if (fillOrderField != null) {
                        fillOrder = fillOrderField.getAsLong(0);
                    }
                    reverse = (fillOrder == 2L);
                    int params = 0;
                    if (dir.isTagPresent(262)) {
                        final long photo = dir.getFieldAsLong(262);
                        if (photo == 1L) {
                            params |= 0x1;
                        }
                    }
                    int imagecomp = 0;
                    switch (compression) {
                        case 2:
                        case 32771: {
                            imagecomp = 257;
                            params |= 0xA;
                            break;
                        }
                        case 3: {
                            imagecomp = 257;
                            params |= 0xC;
                            final TIFFField t4OptionsField = dir.getField(292);
                            if (t4OptionsField == null) {
                                break;
                            }
                            tiffT4Options = t4OptionsField.getAsLong(0);
                            if ((tiffT4Options & 0x1L) != 0x0L) {
                                imagecomp = 258;
                            }
                            if ((tiffT4Options & 0x4L) != 0x0L) {
                                params |= 0x2;
                                break;
                            }
                            break;
                        }
                        case 4: {
                            imagecomp = 256;
                            final TIFFField t6OptionsField = dir.getField(293);
                            if (t6OptionsField != null) {
                                tiffT6Options = t6OptionsField.getAsLong(0);
                                break;
                            }
                            break;
                        }
                    }
                    if (direct && rowsStrip == h) {
                        final byte[] im = new byte[(int)size[0]];
                        s.seek(offset[0]);
                        s.readFully(im);
                        img = Image.getInstance(w, h, false, imagecomp, params, im);
                        img.setInverted(true);
                    }
                    else {
                        int rowsLeft = h;
                        final CCITTG4Encoder g4 = new CCITTG4Encoder(w);
                        for (int k = 0; k < offset.length; ++k) {
                            byte[] im2 = new byte[(int)size[k]];
                            s.seek(offset[k]);
                            s.readFully(im2);
                            final int height = Math.min(rowsStrip, rowsLeft);
                            final TIFFFaxDecoder decoder = new TIFFFaxDecoder(fillOrder, w, height);
                            decoder.setRecoverFromImageError(recoverFromImageError);
                            final byte[] outBuf = new byte[(w + 7) / 8 * height];
                            switch (compression) {
                                case 2:
                                case 32771: {
                                    decoder.decode1D(outBuf, im2, 0, height);
                                    g4.fax4Encode(outBuf, height);
                                    break;
                                }
                                case 3: {
                                    try {
                                        decoder.decode2D(outBuf, im2, 0, height, tiffT4Options);
                                    }
                                    catch (RuntimeException e) {
                                        tiffT4Options ^= 0x4L;
                                        try {
                                            decoder.decode2D(outBuf, im2, 0, height, tiffT4Options);
                                        }
                                        catch (RuntimeException e4) {
                                            if (!recoverFromImageError) {
                                                throw e;
                                            }
                                            if (rowsStrip == 1) {
                                                throw e;
                                            }
                                            im2 = new byte[(int)size[0]];
                                            s.seek(offset[0]);
                                            s.readFully(im2);
                                            img = Image.getInstance(w, h, false, imagecomp, params, im2);
                                            img.setInverted(true);
                                            img.setDpi(dpiX, dpiY);
                                            img.setXYRatio(XYRatio);
                                            img.setOriginalType(5);
                                            if (rotation != 0.0f) {
                                                img.setInitialRotation(rotation);
                                            }
                                            return img;
                                        }
                                    }
                                    g4.fax4Encode(outBuf, height);
                                    break;
                                }
                                case 4: {
                                    try {
                                        decoder.decodeT6(outBuf, im2, 0, height, tiffT6Options);
                                    }
                                    catch (InvalidImageException e2) {
                                        if (!recoverFromImageError) {
                                            throw e2;
                                        }
                                    }
                                    g4.fax4Encode(outBuf, height);
                                    break;
                                }
                            }
                            rowsLeft -= rowsStrip;
                        }
                        final byte[] g4pic = g4.close();
                        img = Image.getInstance(w, h, false, 256, params & 0x1, g4pic);
                    }
                    img.setDpi(dpiX, dpiY);
                    img.setXYRatio(XYRatio);
                    if (dir.isTagPresent(34675)) {
                        try {
                            final TIFFField fd = dir.getField(34675);
                            final ICC_Profile icc_prof = ICC_Profile.getInstance(fd.getAsBytes());
                            if (icc_prof.getNumComponents() == 1) {
                                img.tagICC(icc_prof);
                            }
                        }
                        catch (RuntimeException ex) {}
                    }
                    img.setOriginalType(5);
                    if (rotation != 0.0f) {
                        img.setInitialRotation(rotation);
                    }
                    return img;
                }
                default: {
                    return getTiffImageColor(dir, s);
                }
            }
        }
        catch (Exception e3) {
            throw new ExceptionConverter(e3);
        }
    }
    
    public static Image getTiffImage(final RandomAccessFileOrArray s, final boolean recoverFromImageError, final int page) {
        return getTiffImage(s, recoverFromImageError, page, false);
    }
    
    public static Image getTiffImage(final RandomAccessFileOrArray s, final int page) {
        return getTiffImage(s, page, false);
    }
    
    public static Image getTiffImage(final RandomAccessFileOrArray s, final int page, final boolean direct) {
        return getTiffImage(s, false, page, direct);
    }
    
    protected static Image getTiffImageColor(final TIFFDirectory dir, final RandomAccessFileOrArray s) {
        try {
            int compression = 1;
            if (dir.isTagPresent(259)) {
                compression = (int)dir.getFieldAsLong(259);
            }
            int predictor = 1;
            TIFFLZWDecoder lzwDecoder = null;
            switch (compression) {
                case 1:
                case 5:
                case 6:
                case 7:
                case 8:
                case 32773:
                case 32946: {
                    final int photometric = (int)dir.getFieldAsLong(262);
                    switch (photometric) {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 5: {
                            break;
                        }
                        default: {
                            if (compression != 6 && compression != 7) {
                                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.photometric.1.is.not.supported", photometric));
                            }
                            break;
                        }
                    }
                    float rotation = 0.0f;
                    if (dir.isTagPresent(274)) {
                        final int rot = (int)dir.getFieldAsLong(274);
                        if (rot == 3 || rot == 4) {
                            rotation = 3.1415927f;
                        }
                        else if (rot == 5 || rot == 8) {
                            rotation = 1.5707964f;
                        }
                        else if (rot == 6 || rot == 7) {
                            rotation = -1.5707964f;
                        }
                    }
                    if (dir.isTagPresent(284) && dir.getFieldAsLong(284) == 2L) {
                        throw new IllegalArgumentException(MessageLocalization.getComposedMessage("planar.images.are.not.supported", new Object[0]));
                    }
                    int extraSamples = 0;
                    if (dir.isTagPresent(338)) {
                        extraSamples = 1;
                    }
                    int samplePerPixel = 1;
                    if (dir.isTagPresent(277)) {
                        samplePerPixel = (int)dir.getFieldAsLong(277);
                    }
                    int bitsPerSample = 1;
                    if (dir.isTagPresent(258)) {
                        bitsPerSample = (int)dir.getFieldAsLong(258);
                    }
                    switch (bitsPerSample) {
                        case 1:
                        case 2:
                        case 4:
                        case 8: {
                            Image img = null;
                            final int h = (int)dir.getFieldAsLong(257);
                            final int w = (int)dir.getFieldAsLong(256);
                            int dpiX = 0;
                            int dpiY = 0;
                            int resolutionUnit = 2;
                            if (dir.isTagPresent(296)) {
                                resolutionUnit = (int)dir.getFieldAsLong(296);
                            }
                            dpiX = getDpi(dir.getField(282), resolutionUnit);
                            dpiY = getDpi(dir.getField(283), resolutionUnit);
                            int fillOrder = 1;
                            boolean reverse = false;
                            final TIFFField fillOrderField = dir.getField(266);
                            if (fillOrderField != null) {
                                fillOrder = fillOrderField.getAsInt(0);
                            }
                            reverse = (fillOrder == 2);
                            int rowsStrip = h;
                            if (dir.isTagPresent(278)) {
                                rowsStrip = (int)dir.getFieldAsLong(278);
                            }
                            if (rowsStrip <= 0 || rowsStrip > h) {
                                rowsStrip = h;
                            }
                            final long[] offset = getArrayLongShort(dir, 273);
                            long[] size = getArrayLongShort(dir, 279);
                            if ((size == null || (size.length == 1 && (size[0] == 0L || size[0] + offset[0] > s.length()))) && h == rowsStrip) {
                                size = new long[] { s.length() - (int)offset[0] };
                            }
                            if (compression == 5 || compression == 32946 || compression == 8) {
                                final TIFFField predictorField = dir.getField(317);
                                if (predictorField != null) {
                                    predictor = predictorField.getAsInt(0);
                                    if (predictor != 1 && predictor != 2) {
                                        throw new RuntimeException(MessageLocalization.getComposedMessage("illegal.value.for.predictor.in.tiff.file", new Object[0]));
                                    }
                                    if (predictor == 2 && bitsPerSample != 8) {
                                        throw new RuntimeException(MessageLocalization.getComposedMessage("1.bit.samples.are.not.supported.for.horizontal.differencing.predictor", bitsPerSample));
                                    }
                                }
                            }
                            if (compression == 5) {
                                lzwDecoder = new TIFFLZWDecoder(w, predictor, samplePerPixel);
                            }
                            int rowsLeft = h;
                            ByteArrayOutputStream stream = null;
                            ByteArrayOutputStream mstream = null;
                            DeflaterOutputStream zip = null;
                            DeflaterOutputStream mzip = null;
                            if (extraSamples > 0) {
                                mstream = new ByteArrayOutputStream();
                                mzip = new DeflaterOutputStream(mstream);
                            }
                            CCITTG4Encoder g4 = null;
                            if (bitsPerSample == 1 && samplePerPixel == 1 && photometric != 3) {
                                g4 = new CCITTG4Encoder(w);
                            }
                            else {
                                stream = new ByteArrayOutputStream();
                                if (compression != 6 && compression != 7) {
                                    zip = new DeflaterOutputStream(stream);
                                }
                            }
                            if (compression == 6) {
                                if (!dir.isTagPresent(513)) {
                                    throw new IOException(MessageLocalization.getComposedMessage("missing.tag.s.for.ojpeg.compression", new Object[0]));
                                }
                                final int jpegOffset = (int)dir.getFieldAsLong(513);
                                int jpegLength = (int)s.length() - jpegOffset;
                                if (dir.isTagPresent(514)) {
                                    jpegLength = (int)dir.getFieldAsLong(514) + (int)size[0];
                                }
                                final byte[] jpeg = new byte[Math.min(jpegLength, (int)s.length() - jpegOffset)];
                                s.seek(jpegOffset);
                                s.readFully(jpeg);
                                img = new Jpeg(jpeg);
                            }
                            else if (compression == 7) {
                                if (size.length > 1) {
                                    throw new IOException(MessageLocalization.getComposedMessage("compression.jpeg.is.only.supported.with.a.single.strip.this.image.has.1.strips", size.length));
                                }
                                byte[] jpeg2 = new byte[(int)size[0]];
                                s.seek(offset[0]);
                                s.readFully(jpeg2);
                                final TIFFField jpegtables = dir.getField(347);
                                if (jpegtables != null) {
                                    final byte[] temp = jpegtables.getAsBytes();
                                    int tableoffset = 0;
                                    int tablelength = temp.length;
                                    if (temp[0] == -1 && temp[1] == -40) {
                                        tableoffset = 2;
                                        tablelength -= 2;
                                    }
                                    if (temp[temp.length - 2] == -1 && temp[temp.length - 1] == -39) {
                                        tablelength -= 2;
                                    }
                                    final byte[] tables = new byte[tablelength];
                                    System.arraycopy(temp, tableoffset, tables, 0, tablelength);
                                    final byte[] jpegwithtables = new byte[jpeg2.length + tables.length];
                                    System.arraycopy(jpeg2, 0, jpegwithtables, 0, 2);
                                    System.arraycopy(tables, 0, jpegwithtables, 2, tables.length);
                                    System.arraycopy(jpeg2, 2, jpegwithtables, tables.length + 2, jpeg2.length - 2);
                                    jpeg2 = jpegwithtables;
                                }
                                img = new Jpeg(jpeg2);
                                if (photometric == 2) {
                                    img.setColorTransform(0);
                                }
                            }
                            else {
                                for (int k = 0; k < offset.length; ++k) {
                                    final byte[] im = new byte[(int)size[k]];
                                    s.seek(offset[k]);
                                    s.readFully(im);
                                    final int height = Math.min(rowsStrip, rowsLeft);
                                    byte[] outBuf = null;
                                    if (compression != 1) {
                                        outBuf = new byte[(w * bitsPerSample * samplePerPixel + 7) / 8 * height];
                                    }
                                    if (reverse) {
                                        TIFFFaxDecoder.reverseBits(im);
                                    }
                                    switch (compression) {
                                        case 8:
                                        case 32946: {
                                            inflate(im, outBuf);
                                            applyPredictor(outBuf, predictor, w, height, samplePerPixel);
                                            break;
                                        }
                                        case 1: {
                                            outBuf = im;
                                            break;
                                        }
                                        case 32773: {
                                            decodePackbits(im, outBuf);
                                            break;
                                        }
                                        case 5: {
                                            lzwDecoder.decode(im, outBuf, height);
                                            break;
                                        }
                                    }
                                    if (bitsPerSample == 1 && samplePerPixel == 1 && photometric != 3) {
                                        g4.fax4Encode(outBuf, height);
                                    }
                                    else if (extraSamples > 0) {
                                        ProcessExtraSamples(zip, mzip, outBuf, samplePerPixel, bitsPerSample, w, height);
                                    }
                                    else {
                                        zip.write(outBuf);
                                    }
                                    rowsLeft -= rowsStrip;
                                }
                                if (bitsPerSample == 1 && samplePerPixel == 1 && photometric != 3) {
                                    img = Image.getInstance(w, h, false, 256, (photometric == 1) ? 1 : 0, g4.close());
                                }
                                else {
                                    zip.close();
                                    img = new ImgRaw(w, h, samplePerPixel - extraSamples, bitsPerSample, stream.toByteArray());
                                    img.setDeflated(true);
                                }
                            }
                            img.setDpi(dpiX, dpiY);
                            if (compression != 6 && compression != 7) {
                                if (dir.isTagPresent(34675)) {
                                    try {
                                        final TIFFField fd = dir.getField(34675);
                                        final ICC_Profile icc_prof = ICC_Profile.getInstance(fd.getAsBytes());
                                        if (samplePerPixel - extraSamples == icc_prof.getNumComponents()) {
                                            img.tagICC(icc_prof);
                                        }
                                    }
                                    catch (RuntimeException ex) {}
                                }
                                if (dir.isTagPresent(320)) {
                                    final TIFFField fd = dir.getField(320);
                                    final char[] rgb = fd.getAsChars();
                                    final byte[] palette = new byte[rgb.length];
                                    final int gColor = rgb.length / 3;
                                    final int bColor = gColor * 2;
                                    for (int i = 0; i < gColor; ++i) {
                                        palette[i * 3] = (byte)(rgb[i] >>> 8);
                                        palette[i * 3 + 1] = (byte)(rgb[i + gColor] >>> 8);
                                        palette[i * 3 + 2] = (byte)(rgb[i + bColor] >>> 8);
                                    }
                                    boolean colormapBroken = true;
                                    for (int j = 0; j < palette.length; ++j) {
                                        if (palette[j] != 0) {
                                            colormapBroken = false;
                                            break;
                                        }
                                    }
                                    if (colormapBroken) {
                                        for (int j = 0; j < gColor; ++j) {
                                            palette[j * 3] = (byte)rgb[j];
                                            palette[j * 3 + 1] = (byte)rgb[j + gColor];
                                            palette[j * 3 + 2] = (byte)rgb[j + bColor];
                                        }
                                    }
                                    final PdfArray indexed = new PdfArray();
                                    indexed.add(PdfName.INDEXED);
                                    indexed.add(PdfName.DEVICERGB);
                                    indexed.add(new PdfNumber(gColor - 1));
                                    indexed.add(new PdfString(palette));
                                    final PdfDictionary additional = new PdfDictionary();
                                    additional.put(PdfName.COLORSPACE, indexed);
                                    img.setAdditional(additional);
                                }
                                img.setOriginalType(5);
                            }
                            if (photometric == 0) {
                                img.setInverted(true);
                            }
                            if (rotation != 0.0f) {
                                img.setInitialRotation(rotation);
                            }
                            if (extraSamples > 0) {
                                mzip.close();
                                final Image mimg = Image.getInstance(w, h, 1, bitsPerSample, mstream.toByteArray());
                                mimg.makeMask();
                                mimg.setDeflated(true);
                                img.setImageMask(mimg);
                            }
                            return img;
                        }
                        default: {
                            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("bits.per.sample.1.is.not.supported", bitsPerSample));
                        }
                    }
                    break;
                }
                default: {
                    throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.compression.1.is.not.supported", compression));
                }
            }
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    static Image ProcessExtraSamples(final DeflaterOutputStream zip, final DeflaterOutputStream mzip, final byte[] outBuf, final int samplePerPixel, final int bitsPerSample, final int width, final int height) throws IOException {
        if (bitsPerSample == 8) {
            final byte[] mask = new byte[width * height];
            int mptr = 0;
            int optr = 0;
            for (int total = width * height * samplePerPixel, k = 0; k < total; k += samplePerPixel) {
                for (int s = 0; s < samplePerPixel - 1; ++s) {
                    outBuf[optr++] = outBuf[k + s];
                }
                mask[mptr++] = outBuf[k + samplePerPixel - 1];
            }
            zip.write(outBuf, 0, optr);
            mzip.write(mask, 0, mptr);
            return null;
        }
        throw new IllegalArgumentException(MessageLocalization.getComposedMessage("extra.samples.are.not.supported", new Object[0]));
    }
    
    static long[] getArrayLongShort(final TIFFDirectory dir, final int tag) {
        final TIFFField field = dir.getField(tag);
        if (field == null) {
            return null;
        }
        long[] offset;
        if (field.getType() == 4) {
            offset = field.getAsLongs();
        }
        else {
            final char[] temp = field.getAsChars();
            offset = new long[temp.length];
            for (int k = 0; k < temp.length; ++k) {
                offset[k] = temp[k];
            }
        }
        return offset;
    }
    
    public static void decodePackbits(final byte[] data, final byte[] dst) {
        int srcCount = 0;
        int dstCount = 0;
        try {
            while (dstCount < dst.length) {
                final byte b = data[srcCount++];
                if (b >= 0 && b <= 127) {
                    for (int i = 0; i < b + 1; ++i) {
                        dst[dstCount++] = data[srcCount++];
                    }
                }
                else if (b <= -1 && b >= -127) {
                    final byte repeat = data[srcCount++];
                    for (int i = 0; i < -b + 1; ++i) {
                        dst[dstCount++] = repeat;
                    }
                }
                else {
                    ++srcCount;
                }
            }
        }
        catch (Exception ex) {}
    }
    
    public static void inflate(final byte[] deflated, final byte[] inflated) {
        final Inflater inflater = new Inflater();
        inflater.setInput(deflated);
        try {
            inflater.inflate(inflated);
        }
        catch (DataFormatException dfe) {
            throw new ExceptionConverter(dfe);
        }
    }
    
    public static void applyPredictor(final byte[] uncompData, final int predictor, final int w, final int h, final int samplesPerPixel) {
        if (predictor != 2) {
            return;
        }
        for (int j = 0; j < h; ++j) {
            int count = samplesPerPixel * (j * w + 1);
            for (int i = samplesPerPixel; i < w * samplesPerPixel; ++i) {
                final int n = count;
                uncompData[n] += uncompData[count - samplesPerPixel];
                ++count;
            }
        }
    }
}
