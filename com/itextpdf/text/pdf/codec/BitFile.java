// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.codec;

import java.io.IOException;
import java.io.OutputStream;

public class BitFile
{
    OutputStream output_;
    byte[] buffer_;
    int index_;
    int bitsLeft_;
    boolean blocks_;
    
    public BitFile(final OutputStream output, final boolean blocks) {
        this.blocks_ = false;
        this.output_ = output;
        this.blocks_ = blocks;
        this.buffer_ = new byte[256];
        this.index_ = 0;
        this.bitsLeft_ = 8;
    }
    
    public void flush() throws IOException {
        final int numBytes = this.index_ + ((this.bitsLeft_ != 8) ? 1 : 0);
        if (numBytes > 0) {
            if (this.blocks_) {
                this.output_.write(numBytes);
            }
            this.output_.write(this.buffer_, 0, numBytes);
            this.buffer_[0] = 0;
            this.index_ = 0;
            this.bitsLeft_ = 8;
        }
    }
    
    public void writeBits(int bits, int numbits) throws IOException {
        int bitsWritten = 0;
        final int numBytes = 255;
        do {
            if ((this.index_ == 254 && this.bitsLeft_ == 0) || this.index_ > 254) {
                if (this.blocks_) {
                    this.output_.write(numBytes);
                }
                this.output_.write(this.buffer_, 0, numBytes);
                this.buffer_[0] = 0;
                this.index_ = 0;
                this.bitsLeft_ = 8;
            }
            if (numbits <= this.bitsLeft_) {
                if (this.blocks_) {
                    final byte[] buffer_ = this.buffer_;
                    final int index_ = this.index_;
                    buffer_[index_] |= (byte)((bits & (1 << numbits) - 1) << 8 - this.bitsLeft_);
                    bitsWritten += numbits;
                    this.bitsLeft_ -= numbits;
                    numbits = 0;
                }
                else {
                    final byte[] buffer_2 = this.buffer_;
                    final int index_2 = this.index_;
                    buffer_2[index_2] |= (byte)((bits & (1 << numbits) - 1) << this.bitsLeft_ - numbits);
                    bitsWritten += numbits;
                    this.bitsLeft_ -= numbits;
                    numbits = 0;
                }
            }
            else if (this.blocks_) {
                final byte[] buffer_3 = this.buffer_;
                final int index_3 = this.index_;
                buffer_3[index_3] |= (byte)((bits & (1 << this.bitsLeft_) - 1) << 8 - this.bitsLeft_);
                bitsWritten += this.bitsLeft_;
                bits >>= this.bitsLeft_;
                numbits -= this.bitsLeft_;
                this.buffer_[++this.index_] = 0;
                this.bitsLeft_ = 8;
            }
            else {
                final int topbits = bits >>> numbits - this.bitsLeft_ & (1 << this.bitsLeft_) - 1;
                final byte[] buffer_4 = this.buffer_;
                final int index_4 = this.index_;
                buffer_4[index_4] |= (byte)topbits;
                numbits -= this.bitsLeft_;
                bitsWritten += this.bitsLeft_;
                this.buffer_[++this.index_] = 0;
                this.bitsLeft_ = 8;
            }
        } while (numbits != 0);
    }
}
