// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.codec.wmf;

public class Point
{
    public int x;
    public int y;
    
    public Point() {
    }
    
    public Point(final int x, final int y) {
        this.x = x;
        this.y = y;
    }
}
