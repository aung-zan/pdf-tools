// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.codec;

import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.ImgRaw;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import com.itextpdf.text.Utilities;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Image;
import java.net.URL;
import java.io.IOException;
import java.util.HashMap;
import java.io.InputStream;

public class BmpImage
{
    private InputStream inputStream;
    private long bitmapFileSize;
    private long bitmapOffset;
    private long compression;
    private long imageSize;
    private byte[] palette;
    private int imageType;
    private int numBands;
    private boolean isBottomUp;
    private int bitsPerPixel;
    private int redMask;
    private int greenMask;
    private int blueMask;
    private int alphaMask;
    public HashMap<String, Object> properties;
    private long xPelsPerMeter;
    private long yPelsPerMeter;
    private static final int VERSION_2_1_BIT = 0;
    private static final int VERSION_2_4_BIT = 1;
    private static final int VERSION_2_8_BIT = 2;
    private static final int VERSION_2_24_BIT = 3;
    private static final int VERSION_3_1_BIT = 4;
    private static final int VERSION_3_4_BIT = 5;
    private static final int VERSION_3_8_BIT = 6;
    private static final int VERSION_3_24_BIT = 7;
    private static final int VERSION_3_NT_16_BIT = 8;
    private static final int VERSION_3_NT_32_BIT = 9;
    private static final int VERSION_4_1_BIT = 10;
    private static final int VERSION_4_4_BIT = 11;
    private static final int VERSION_4_8_BIT = 12;
    private static final int VERSION_4_16_BIT = 13;
    private static final int VERSION_4_24_BIT = 14;
    private static final int VERSION_4_32_BIT = 15;
    private static final int LCS_CALIBRATED_RGB = 0;
    private static final int LCS_sRGB = 1;
    private static final int LCS_CMYK = 2;
    private static final int BI_RGB = 0;
    private static final int BI_RLE8 = 1;
    private static final int BI_RLE4 = 2;
    private static final int BI_BITFIELDS = 3;
    int width;
    int height;
    
    BmpImage(final InputStream is, final boolean noHeader, final int size) throws IOException {
        this.properties = new HashMap<String, Object>();
        this.bitmapFileSize = size;
        this.bitmapOffset = 0L;
        this.process(is, noHeader);
    }
    
    public static Image getImage(final URL url) throws IOException {
        InputStream is = null;
        try {
            is = url.openStream();
            final Image img = getImage(is);
            img.setUrl(url);
            return img;
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
    }
    
    public static Image getImage(final InputStream is) throws IOException {
        return getImage(is, false, 0);
    }
    
    public static Image getImage(final InputStream is, final boolean noHeader, final int size) throws IOException {
        final BmpImage bmp = new BmpImage(is, noHeader, size);
        try {
            final Image img = bmp.getImage();
            img.setDpi((int)(bmp.xPelsPerMeter * 0.0254 + 0.5), (int)(bmp.yPelsPerMeter * 0.0254 + 0.5));
            img.setOriginalType(4);
            return img;
        }
        catch (BadElementException be) {
            throw new ExceptionConverter(be);
        }
    }
    
    public static Image getImage(final String file) throws IOException {
        return getImage(Utilities.toURL(file));
    }
    
    public static Image getImage(final byte[] data) throws IOException {
        final ByteArrayInputStream is = new ByteArrayInputStream(data);
        final Image img = getImage(is);
        img.setOriginalData(data);
        return img;
    }
    
    protected void process(final InputStream stream, final boolean noHeader) throws IOException {
        if (noHeader || stream instanceof BufferedInputStream) {
            this.inputStream = stream;
        }
        else {
            this.inputStream = new BufferedInputStream(stream);
        }
        if (!noHeader) {
            if (this.readUnsignedByte(this.inputStream) != 66 || this.readUnsignedByte(this.inputStream) != 77) {
                throw new RuntimeException(MessageLocalization.getComposedMessage("invalid.magic.value.for.bmp.file", new Object[0]));
            }
            this.bitmapFileSize = this.readDWord(this.inputStream);
            this.readWord(this.inputStream);
            this.readWord(this.inputStream);
            this.bitmapOffset = this.readDWord(this.inputStream);
        }
        final long size = this.readDWord(this.inputStream);
        if (size == 12L) {
            this.width = this.readWord(this.inputStream);
            this.height = this.readWord(this.inputStream);
        }
        else {
            this.width = this.readLong(this.inputStream);
            this.height = this.readLong(this.inputStream);
        }
        final int planes = this.readWord(this.inputStream);
        this.bitsPerPixel = this.readWord(this.inputStream);
        this.properties.put("color_planes", planes);
        this.properties.put("bits_per_pixel", this.bitsPerPixel);
        this.numBands = 3;
        if (this.bitmapOffset == 0L) {
            this.bitmapOffset = size;
        }
        if (size == 12L) {
            this.properties.put("bmp_version", "BMP v. 2.x");
            if (this.bitsPerPixel == 1) {
                this.imageType = 0;
            }
            else if (this.bitsPerPixel == 4) {
                this.imageType = 1;
            }
            else if (this.bitsPerPixel == 8) {
                this.imageType = 2;
            }
            else if (this.bitsPerPixel == 24) {
                this.imageType = 3;
            }
            final int numberOfEntries = (int)((this.bitmapOffset - 14L - size) / 3L);
            int sizeOfPalette = numberOfEntries * 3;
            if (this.bitmapOffset == size) {
                switch (this.imageType) {
                    case 0: {
                        sizeOfPalette = 6;
                        break;
                    }
                    case 1: {
                        sizeOfPalette = 48;
                        break;
                    }
                    case 2: {
                        sizeOfPalette = 768;
                        break;
                    }
                    case 3: {
                        sizeOfPalette = 0;
                        break;
                    }
                }
                this.bitmapOffset = size + sizeOfPalette;
            }
            this.readPalette(sizeOfPalette);
        }
        else {
            this.compression = this.readDWord(this.inputStream);
            this.imageSize = this.readDWord(this.inputStream);
            this.xPelsPerMeter = this.readLong(this.inputStream);
            this.yPelsPerMeter = this.readLong(this.inputStream);
            final long colorsUsed = this.readDWord(this.inputStream);
            final long colorsImportant = this.readDWord(this.inputStream);
            switch ((int)this.compression) {
                case 0: {
                    this.properties.put("compression", "BI_RGB");
                    break;
                }
                case 1: {
                    this.properties.put("compression", "BI_RLE8");
                    break;
                }
                case 2: {
                    this.properties.put("compression", "BI_RLE4");
                    break;
                }
                case 3: {
                    this.properties.put("compression", "BI_BITFIELDS");
                    break;
                }
            }
            this.properties.put("x_pixels_per_meter", this.xPelsPerMeter);
            this.properties.put("y_pixels_per_meter", this.yPelsPerMeter);
            this.properties.put("colors_used", colorsUsed);
            this.properties.put("colors_important", colorsImportant);
            if (size == 40L || size == 52L || size == 56L) {
                switch ((int)this.compression) {
                    case 0:
                    case 1:
                    case 2: {
                        if (this.bitsPerPixel == 1) {
                            this.imageType = 4;
                        }
                        else if (this.bitsPerPixel == 4) {
                            this.imageType = 5;
                        }
                        else if (this.bitsPerPixel == 8) {
                            this.imageType = 6;
                        }
                        else if (this.bitsPerPixel == 24) {
                            this.imageType = 7;
                        }
                        else if (this.bitsPerPixel == 16) {
                            this.imageType = 8;
                            this.redMask = 31744;
                            this.greenMask = 992;
                            this.blueMask = 31;
                            this.properties.put("red_mask", this.redMask);
                            this.properties.put("green_mask", this.greenMask);
                            this.properties.put("blue_mask", this.blueMask);
                        }
                        else if (this.bitsPerPixel == 32) {
                            this.imageType = 9;
                            this.redMask = 16711680;
                            this.greenMask = 65280;
                            this.blueMask = 255;
                            this.properties.put("red_mask", this.redMask);
                            this.properties.put("green_mask", this.greenMask);
                            this.properties.put("blue_mask", this.blueMask);
                        }
                        if (size >= 52L) {
                            this.redMask = (int)this.readDWord(this.inputStream);
                            this.greenMask = (int)this.readDWord(this.inputStream);
                            this.blueMask = (int)this.readDWord(this.inputStream);
                            this.properties.put("red_mask", this.redMask);
                            this.properties.put("green_mask", this.greenMask);
                            this.properties.put("blue_mask", this.blueMask);
                        }
                        if (size == 56L) {
                            this.alphaMask = (int)this.readDWord(this.inputStream);
                            this.properties.put("alpha_mask", this.alphaMask);
                        }
                        final int numberOfEntries2 = (int)((this.bitmapOffset - 14L - size) / 4L);
                        int sizeOfPalette2 = numberOfEntries2 * 4;
                        if (this.bitmapOffset == size) {
                            switch (this.imageType) {
                                case 4: {
                                    sizeOfPalette2 = (int)((colorsUsed == 0L) ? 2L : colorsUsed) * 4;
                                    break;
                                }
                                case 5: {
                                    sizeOfPalette2 = (int)((colorsUsed == 0L) ? 16L : colorsUsed) * 4;
                                    break;
                                }
                                case 6: {
                                    sizeOfPalette2 = (int)((colorsUsed == 0L) ? 256L : colorsUsed) * 4;
                                    break;
                                }
                                default: {
                                    sizeOfPalette2 = 0;
                                    break;
                                }
                            }
                            this.bitmapOffset = size + sizeOfPalette2;
                        }
                        this.readPalette(sizeOfPalette2);
                        this.properties.put("bmp_version", "BMP v. 3.x");
                        break;
                    }
                    case 3: {
                        if (this.bitsPerPixel == 16) {
                            this.imageType = 8;
                        }
                        else if (this.bitsPerPixel == 32) {
                            this.imageType = 9;
                        }
                        this.redMask = (int)this.readDWord(this.inputStream);
                        this.greenMask = (int)this.readDWord(this.inputStream);
                        this.blueMask = (int)this.readDWord(this.inputStream);
                        if (size == 56L) {
                            this.alphaMask = (int)this.readDWord(this.inputStream);
                            this.properties.put("alpha_mask", this.alphaMask);
                        }
                        this.properties.put("red_mask", this.redMask);
                        this.properties.put("green_mask", this.greenMask);
                        this.properties.put("blue_mask", this.blueMask);
                        if (colorsUsed != 0L) {
                            final int sizeOfPalette2 = (int)colorsUsed * 4;
                            this.readPalette(sizeOfPalette2);
                        }
                        this.properties.put("bmp_version", "BMP v. 3.x NT");
                        break;
                    }
                    default: {
                        throw new RuntimeException("Invalid compression specified in BMP file.");
                    }
                }
            }
            else {
                if (size != 108L) {
                    this.properties.put("bmp_version", "BMP v. 5.x");
                    throw new RuntimeException("BMP version 5 not implemented yet.");
                }
                this.properties.put("bmp_version", "BMP v. 4.x");
                this.redMask = (int)this.readDWord(this.inputStream);
                this.greenMask = (int)this.readDWord(this.inputStream);
                this.blueMask = (int)this.readDWord(this.inputStream);
                this.alphaMask = (int)this.readDWord(this.inputStream);
                final long csType = this.readDWord(this.inputStream);
                final int redX = this.readLong(this.inputStream);
                final int redY = this.readLong(this.inputStream);
                final int redZ = this.readLong(this.inputStream);
                final int greenX = this.readLong(this.inputStream);
                final int greenY = this.readLong(this.inputStream);
                final int greenZ = this.readLong(this.inputStream);
                final int blueX = this.readLong(this.inputStream);
                final int blueY = this.readLong(this.inputStream);
                final int blueZ = this.readLong(this.inputStream);
                final long gammaRed = this.readDWord(this.inputStream);
                final long gammaGreen = this.readDWord(this.inputStream);
                final long gammaBlue = this.readDWord(this.inputStream);
                if (this.bitsPerPixel == 1) {
                    this.imageType = 10;
                }
                else if (this.bitsPerPixel == 4) {
                    this.imageType = 11;
                }
                else if (this.bitsPerPixel == 8) {
                    this.imageType = 12;
                }
                else if (this.bitsPerPixel == 16) {
                    this.imageType = 13;
                    if ((int)this.compression == 0) {
                        this.redMask = 31744;
                        this.greenMask = 992;
                        this.blueMask = 31;
                    }
                }
                else if (this.bitsPerPixel == 24) {
                    this.imageType = 14;
                }
                else if (this.bitsPerPixel == 32) {
                    this.imageType = 15;
                    if ((int)this.compression == 0) {
                        this.redMask = 16711680;
                        this.greenMask = 65280;
                        this.blueMask = 255;
                    }
                }
                this.properties.put("red_mask", this.redMask);
                this.properties.put("green_mask", this.greenMask);
                this.properties.put("blue_mask", this.blueMask);
                this.properties.put("alpha_mask", this.alphaMask);
                final int numberOfEntries3 = (int)((this.bitmapOffset - 14L - size) / 4L);
                int sizeOfPalette3 = numberOfEntries3 * 4;
                if (this.bitmapOffset == size) {
                    switch (this.imageType) {
                        case 10: {
                            sizeOfPalette3 = (int)((colorsUsed == 0L) ? 2L : colorsUsed) * 4;
                            break;
                        }
                        case 11: {
                            sizeOfPalette3 = (int)((colorsUsed == 0L) ? 16L : colorsUsed) * 4;
                            break;
                        }
                        case 12: {
                            sizeOfPalette3 = (int)((colorsUsed == 0L) ? 256L : colorsUsed) * 4;
                            break;
                        }
                        default: {
                            sizeOfPalette3 = 0;
                            break;
                        }
                    }
                    this.bitmapOffset = size + sizeOfPalette3;
                }
                this.readPalette(sizeOfPalette3);
                switch ((int)csType) {
                    case 0: {
                        this.properties.put("color_space", "LCS_CALIBRATED_RGB");
                        this.properties.put("redX", redX);
                        this.properties.put("redY", redY);
                        this.properties.put("redZ", redZ);
                        this.properties.put("greenX", greenX);
                        this.properties.put("greenY", greenY);
                        this.properties.put("greenZ", greenZ);
                        this.properties.put("blueX", blueX);
                        this.properties.put("blueY", blueY);
                        this.properties.put("blueZ", blueZ);
                        this.properties.put("gamma_red", gammaRed);
                        this.properties.put("gamma_green", gammaGreen);
                        this.properties.put("gamma_blue", gammaBlue);
                        throw new RuntimeException("Not implemented yet.");
                    }
                    case 1: {
                        this.properties.put("color_space", "LCS_sRGB");
                        break;
                    }
                    case 2: {
                        this.properties.put("color_space", "LCS_CMYK");
                        throw new RuntimeException("Not implemented yet.");
                    }
                }
            }
        }
        if (this.height > 0) {
            this.isBottomUp = true;
        }
        else {
            this.isBottomUp = false;
            this.height = Math.abs(this.height);
        }
        if (this.bitsPerPixel == 1 || this.bitsPerPixel == 4 || this.bitsPerPixel == 8) {
            this.numBands = 1;
            if (this.imageType == 0 || this.imageType == 1 || this.imageType == 2) {
                int sizep = this.palette.length / 3;
                if (sizep > 256) {
                    sizep = 256;
                }
                final byte[] r = new byte[sizep];
                final byte[] g = new byte[sizep];
                final byte[] b = new byte[sizep];
                for (int i = 0; i < sizep; ++i) {
                    final int off = 3 * i;
                    b[i] = this.palette[off];
                    g[i] = this.palette[off + 1];
                    r[i] = this.palette[off + 2];
                }
            }
            else {
                int sizep = this.palette.length / 4;
                if (sizep > 256) {
                    sizep = 256;
                }
                final byte[] r = new byte[sizep];
                final byte[] g = new byte[sizep];
                final byte[] b = new byte[sizep];
                for (int i = 0; i < sizep; ++i) {
                    final int off = 4 * i;
                    b[i] = this.palette[off];
                    g[i] = this.palette[off + 1];
                    r[i] = this.palette[off + 2];
                }
            }
        }
        else if (this.bitsPerPixel == 16) {
            this.numBands = 3;
        }
        else if (this.bitsPerPixel == 32) {
            this.numBands = ((this.alphaMask == 0) ? 3 : 4);
        }
        else {
            this.numBands = 3;
        }
    }
    
    private byte[] getPalette(final int group) {
        if (this.palette == null) {
            return null;
        }
        final byte[] np = new byte[this.palette.length / group * 3];
        for (int e = this.palette.length / group, k = 0; k < e; ++k) {
            int src = k * group;
            final int dest = k * 3;
            np[dest + 2] = this.palette[src++];
            np[dest + 1] = this.palette[src++];
            np[dest] = this.palette[src];
        }
        return np;
    }
    
    private Image getImage() throws IOException, BadElementException {
        byte[] bdata = null;
        switch (this.imageType) {
            case 0: {
                return this.read1Bit(3);
            }
            case 1: {
                return this.read4Bit(3);
            }
            case 2: {
                return this.read8Bit(3);
            }
            case 3: {
                bdata = new byte[this.width * this.height * 3];
                this.read24Bit(bdata);
                return new ImgRaw(this.width, this.height, 3, 8, bdata);
            }
            case 4: {
                return this.read1Bit(4);
            }
            case 5: {
                switch ((int)this.compression) {
                    case 0: {
                        return this.read4Bit(4);
                    }
                    case 2: {
                        return this.readRLE4();
                    }
                    default: {
                        throw new RuntimeException("Invalid compression specified for BMP file.");
                    }
                }
                break;
            }
            case 6: {
                switch ((int)this.compression) {
                    case 0: {
                        return this.read8Bit(4);
                    }
                    case 1: {
                        return this.readRLE8();
                    }
                    default: {
                        throw new RuntimeException("Invalid compression specified for BMP file.");
                    }
                }
                break;
            }
            case 7: {
                bdata = new byte[this.width * this.height * 3];
                this.read24Bit(bdata);
                return new ImgRaw(this.width, this.height, 3, 8, bdata);
            }
            case 8: {
                return this.read1632Bit(false);
            }
            case 9: {
                return this.read1632Bit(true);
            }
            case 10: {
                return this.read1Bit(4);
            }
            case 11: {
                switch ((int)this.compression) {
                    case 0: {
                        return this.read4Bit(4);
                    }
                    case 2: {
                        return this.readRLE4();
                    }
                    default: {
                        throw new RuntimeException("Invalid compression specified for BMP file.");
                    }
                }
                break;
            }
            case 12: {
                switch ((int)this.compression) {
                    case 0: {
                        return this.read8Bit(4);
                    }
                    case 1: {
                        return this.readRLE8();
                    }
                    default: {
                        throw new RuntimeException("Invalid compression specified for BMP file.");
                    }
                }
                break;
            }
            case 13: {
                return this.read1632Bit(false);
            }
            case 14: {
                bdata = new byte[this.width * this.height * 3];
                this.read24Bit(bdata);
                return new ImgRaw(this.width, this.height, 3, 8, bdata);
            }
            case 15: {
                return this.read1632Bit(true);
            }
            default: {
                return null;
            }
        }
    }
    
    private Image indexedModel(final byte[] bdata, final int bpc, final int paletteEntries) throws BadElementException {
        final Image img = new ImgRaw(this.width, this.height, 1, bpc, bdata);
        final PdfArray colorspace = new PdfArray();
        colorspace.add(PdfName.INDEXED);
        colorspace.add(PdfName.DEVICERGB);
        final byte[] np = this.getPalette(paletteEntries);
        final int len = np.length;
        colorspace.add(new PdfNumber(len / 3 - 1));
        colorspace.add(new PdfString(np));
        final PdfDictionary ad = new PdfDictionary();
        ad.put(PdfName.COLORSPACE, colorspace);
        img.setAdditional(ad);
        return img;
    }
    
    private void readPalette(final int sizeOfPalette) throws IOException {
        if (sizeOfPalette == 0) {
            return;
        }
        this.palette = new byte[sizeOfPalette];
        int r;
        for (int bytesRead = 0; bytesRead < sizeOfPalette; bytesRead += r) {
            r = this.inputStream.read(this.palette, bytesRead, sizeOfPalette - bytesRead);
            if (r < 0) {
                throw new RuntimeException(MessageLocalization.getComposedMessage("incomplete.palette", new Object[0]));
            }
        }
        this.properties.put("palette", this.palette);
    }
    
    private Image read1Bit(final int paletteEntries) throws IOException, BadElementException {
        final byte[] bdata = new byte[(this.width + 7) / 8 * this.height];
        int padding = 0;
        final int bytesPerScanline = (int)Math.ceil(this.width / 8.0);
        final int remainder = bytesPerScanline % 4;
        if (remainder != 0) {
            padding = 4 - remainder;
        }
        final int imSize = (bytesPerScanline + padding) * this.height;
        final byte[] values = new byte[imSize];
        for (int bytesRead = 0; bytesRead < imSize; bytesRead += this.inputStream.read(values, bytesRead, imSize - bytesRead)) {}
        if (this.isBottomUp) {
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(values, imSize - (i + 1) * (bytesPerScanline + padding), bdata, i * bytesPerScanline, bytesPerScanline);
            }
        }
        else {
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(values, i * (bytesPerScanline + padding), bdata, i * bytesPerScanline, bytesPerScanline);
            }
        }
        return this.indexedModel(bdata, 1, paletteEntries);
    }
    
    private Image read4Bit(final int paletteEntries) throws IOException, BadElementException {
        final byte[] bdata = new byte[(this.width + 1) / 2 * this.height];
        int padding = 0;
        final int bytesPerScanline = (int)Math.ceil(this.width / 2.0);
        final int remainder = bytesPerScanline % 4;
        if (remainder != 0) {
            padding = 4 - remainder;
        }
        final int imSize = (bytesPerScanline + padding) * this.height;
        final byte[] values = new byte[imSize];
        for (int bytesRead = 0; bytesRead < imSize; bytesRead += this.inputStream.read(values, bytesRead, imSize - bytesRead)) {}
        if (this.isBottomUp) {
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(values, imSize - (i + 1) * (bytesPerScanline + padding), bdata, i * bytesPerScanline, bytesPerScanline);
            }
        }
        else {
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(values, i * (bytesPerScanline + padding), bdata, i * bytesPerScanline, bytesPerScanline);
            }
        }
        return this.indexedModel(bdata, 4, paletteEntries);
    }
    
    private Image read8Bit(final int paletteEntries) throws IOException, BadElementException {
        final byte[] bdata = new byte[this.width * this.height];
        int padding = 0;
        final int bitsPerScanline = this.width * 8;
        if (bitsPerScanline % 32 != 0) {
            padding = (bitsPerScanline / 32 + 1) * 32 - bitsPerScanline;
            padding = (int)Math.ceil(padding / 8.0);
        }
        final int imSize = (this.width + padding) * this.height;
        final byte[] values = new byte[imSize];
        for (int bytesRead = 0; bytesRead < imSize; bytesRead += this.inputStream.read(values, bytesRead, imSize - bytesRead)) {}
        if (this.isBottomUp) {
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(values, imSize - (i + 1) * (this.width + padding), bdata, i * this.width, this.width);
            }
        }
        else {
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(values, i * (this.width + padding), bdata, i * this.width, this.width);
            }
        }
        return this.indexedModel(bdata, 8, paletteEntries);
    }
    
    private void read24Bit(final byte[] bdata) {
        int padding = 0;
        final int bitsPerScanline = this.width * 24;
        if (bitsPerScanline % 32 != 0) {
            padding = (bitsPerScanline / 32 + 1) * 32 - bitsPerScanline;
            padding = (int)Math.ceil(padding / 8.0);
        }
        final int imSize = (this.width * 3 + 3) / 4 * 4 * this.height;
        final byte[] values = new byte[imSize];
        try {
            int r;
            for (int bytesRead = 0; bytesRead < imSize; bytesRead += r) {
                r = this.inputStream.read(values, bytesRead, imSize - bytesRead);
                if (r < 0) {
                    break;
                }
            }
        }
        catch (IOException ioe) {
            throw new ExceptionConverter(ioe);
        }
        int l = 0;
        if (this.isBottomUp) {
            final int max = this.width * this.height * 3 - 1;
            int count = -padding;
            for (int i = 0; i < this.height; ++i) {
                l = max - (i + 1) * this.width * 3 + 1;
                count += padding;
                for (int j = 0; j < this.width; ++j) {
                    bdata[l + 2] = values[count++];
                    bdata[l + 1] = values[count++];
                    bdata[l] = values[count++];
                    l += 3;
                }
            }
        }
        else {
            int count = -padding;
            for (int k = 0; k < this.height; ++k) {
                count += padding;
                for (int m = 0; m < this.width; ++m) {
                    bdata[l + 2] = values[count++];
                    bdata[l + 1] = values[count++];
                    bdata[l] = values[count++];
                    l += 3;
                }
            }
        }
    }
    
    private int findMask(int mask) {
        for (int k = 0; k < 32 && (mask & 0x1) != 0x1; mask >>>= 1, ++k) {}
        return mask;
    }
    
    private int findShift(int mask) {
        int k;
        for (k = 0; k < 32 && (mask & 0x1) != 0x1; mask >>>= 1, ++k) {}
        return k;
    }
    
    private Image read1632Bit(final boolean is32) throws IOException, BadElementException {
        final int red_mask = this.findMask(this.redMask);
        final int red_shift = this.findShift(this.redMask);
        final int red_factor = red_mask + 1;
        final int green_mask = this.findMask(this.greenMask);
        final int green_shift = this.findShift(this.greenMask);
        final int green_factor = green_mask + 1;
        final int blue_mask = this.findMask(this.blueMask);
        final int blue_shift = this.findShift(this.blueMask);
        final int blue_factor = blue_mask + 1;
        final byte[] bdata = new byte[this.width * this.height * 3];
        int padding = 0;
        if (!is32) {
            final int bitsPerScanline = this.width * 16;
            if (bitsPerScanline % 32 != 0) {
                padding = (bitsPerScanline / 32 + 1) * 32 - bitsPerScanline;
                padding = (int)Math.ceil(padding / 8.0);
            }
        }
        int imSize = (int)this.imageSize;
        if (imSize == 0) {
            imSize = (int)(this.bitmapFileSize - this.bitmapOffset);
        }
        int l = 0;
        if (this.isBottomUp) {
            for (int i = this.height - 1; i >= 0; --i) {
                l = this.width * 3 * i;
                for (int j = 0; j < this.width; ++j) {
                    int v;
                    if (is32) {
                        v = (int)this.readDWord(this.inputStream);
                    }
                    else {
                        v = this.readWord(this.inputStream);
                    }
                    bdata[l++] = (byte)((v >>> red_shift & red_mask) * 256 / red_factor);
                    bdata[l++] = (byte)((v >>> green_shift & green_mask) * 256 / green_factor);
                    bdata[l++] = (byte)((v >>> blue_shift & blue_mask) * 256 / blue_factor);
                }
                for (int m = 0; m < padding; ++m) {
                    this.inputStream.read();
                }
            }
        }
        else {
            for (int i = 0; i < this.height; ++i) {
                for (int j = 0; j < this.width; ++j) {
                    int v;
                    if (is32) {
                        v = (int)this.readDWord(this.inputStream);
                    }
                    else {
                        v = this.readWord(this.inputStream);
                    }
                    bdata[l++] = (byte)((v >>> red_shift & red_mask) * 256 / red_factor);
                    bdata[l++] = (byte)((v >>> green_shift & green_mask) * 256 / green_factor);
                    bdata[l++] = (byte)((v >>> blue_shift & blue_mask) * 256 / blue_factor);
                }
                for (int m = 0; m < padding; ++m) {
                    this.inputStream.read();
                }
            }
        }
        return new ImgRaw(this.width, this.height, 3, 8, bdata);
    }
    
    private Image readRLE8() throws IOException, BadElementException {
        int imSize = (int)this.imageSize;
        if (imSize == 0) {
            imSize = (int)(this.bitmapFileSize - this.bitmapOffset);
        }
        final byte[] values = new byte[imSize];
        for (int bytesRead = 0; bytesRead < imSize; bytesRead += this.inputStream.read(values, bytesRead, imSize - bytesRead)) {}
        byte[] val = this.decodeRLE(true, values);
        imSize = this.width * this.height;
        if (this.isBottomUp) {
            final byte[] temp = new byte[val.length];
            final int bytesPerScanline = this.width;
            for (int i = 0; i < this.height; ++i) {
                System.arraycopy(val, imSize - (i + 1) * bytesPerScanline, temp, i * bytesPerScanline, bytesPerScanline);
            }
            val = temp;
        }
        return this.indexedModel(val, 8, 4);
    }
    
    private Image readRLE4() throws IOException, BadElementException {
        int imSize = (int)this.imageSize;
        if (imSize == 0) {
            imSize = (int)(this.bitmapFileSize - this.bitmapOffset);
        }
        final byte[] values = new byte[imSize];
        for (int bytesRead = 0; bytesRead < imSize; bytesRead += this.inputStream.read(values, bytesRead, imSize - bytesRead)) {}
        byte[] val = this.decodeRLE(false, values);
        if (this.isBottomUp) {
            final byte[] inverted = val;
            val = new byte[this.width * this.height];
            int l = 0;
            for (int i = this.height - 1; i >= 0; --i) {
                for (int index = i * this.width, lineEnd = l + this.width; l != lineEnd; val[l++] = inverted[index++]) {}
            }
        }
        final int stride = (this.width + 1) / 2;
        final byte[] bdata = new byte[stride * this.height];
        int ptr = 0;
        int sh = 0;
        for (int h = 0; h < this.height; ++h) {
            for (int w = 0; w < this.width; ++w) {
                if ((w & 0x1) == 0x0) {
                    bdata[sh + w / 2] = (byte)(val[ptr++] << 4);
                }
                else {
                    final byte[] array = bdata;
                    final int n = sh + w / 2;
                    array[n] |= (byte)(val[ptr++] & 0xF);
                }
            }
            sh += stride;
        }
        return this.indexedModel(bdata, 4, 4);
    }
    
    private byte[] decodeRLE(final boolean is8, final byte[] values) {
        final byte[] val = new byte[this.width * this.height];
        try {
            int ptr = 0;
            int x = 0;
            int q = 0;
            int y = 0;
            while (y < this.height && ptr < values.length) {
                int count = values[ptr++] & 0xFF;
                if (count != 0) {
                    final int bt = values[ptr++] & 0xFF;
                    if (is8) {
                        for (int i = count; i != 0; --i) {
                            val[q++] = (byte)bt;
                        }
                    }
                    else {
                        for (int i = 0; i < count; ++i) {
                            val[q++] = (byte)(((i & 0x1) == 0x1) ? (bt & 0xF) : (bt >>> 4 & 0xF));
                        }
                    }
                    x += count;
                }
                else {
                    count = (values[ptr++] & 0xFF);
                    if (count == 1) {
                        break;
                    }
                    switch (count) {
                        case 0: {
                            x = 0;
                            q = ++y * this.width;
                            continue;
                        }
                        case 2: {
                            x += (values[ptr++] & 0xFF);
                            y += (values[ptr++] & 0xFF);
                            q = y * this.width + x;
                            continue;
                        }
                        default: {
                            if (is8) {
                                for (int j = count; j != 0; --j) {
                                    val[q++] = (byte)(values[ptr++] & 0xFF);
                                }
                            }
                            else {
                                int bt = 0;
                                for (int i = 0; i < count; ++i) {
                                    if ((i & 0x1) == 0x0) {
                                        bt = (values[ptr++] & 0xFF);
                                    }
                                    val[q++] = (byte)(((i & 0x1) == 0x1) ? (bt & 0xF) : (bt >>> 4 & 0xF));
                                }
                            }
                            x += count;
                            if (is8) {
                                if ((count & 0x1) == 0x1) {
                                    ++ptr;
                                    continue;
                                }
                                continue;
                            }
                            else {
                                if ((count & 0x3) == 0x1 || (count & 0x3) == 0x2) {
                                    ++ptr;
                                    continue;
                                }
                                continue;
                            }
                            break;
                        }
                    }
                }
            }
        }
        catch (RuntimeException ex) {}
        return val;
    }
    
    private int readUnsignedByte(final InputStream stream) throws IOException {
        return stream.read() & 0xFF;
    }
    
    private int readUnsignedShort(final InputStream stream) throws IOException {
        final int b1 = this.readUnsignedByte(stream);
        final int b2 = this.readUnsignedByte(stream);
        return (b2 << 8 | b1) & 0xFFFF;
    }
    
    private int readShort(final InputStream stream) throws IOException {
        final int b1 = this.readUnsignedByte(stream);
        final int b2 = this.readUnsignedByte(stream);
        return b2 << 8 | b1;
    }
    
    private int readWord(final InputStream stream) throws IOException {
        return this.readUnsignedShort(stream);
    }
    
    private long readUnsignedInt(final InputStream stream) throws IOException {
        final int b1 = this.readUnsignedByte(stream);
        final int b2 = this.readUnsignedByte(stream);
        final int b3 = this.readUnsignedByte(stream);
        final int b4 = this.readUnsignedByte(stream);
        final long l = b4 << 24 | b3 << 16 | b2 << 8 | b1;
        return l & -1L;
    }
    
    private int readInt(final InputStream stream) throws IOException {
        final int b1 = this.readUnsignedByte(stream);
        final int b2 = this.readUnsignedByte(stream);
        final int b3 = this.readUnsignedByte(stream);
        final int b4 = this.readUnsignedByte(stream);
        return b4 << 24 | b3 << 16 | b2 << 8 | b1;
    }
    
    private long readDWord(final InputStream stream) throws IOException {
        return this.readUnsignedInt(stream);
    }
    
    private int readLong(final InputStream stream) throws IOException {
        return this.readInt(stream);
    }
}
