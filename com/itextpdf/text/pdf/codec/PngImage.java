// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.codec;

import java.io.ByteArrayOutputStream;
import com.itextpdf.text.log.Logger;
import com.itextpdf.text.log.LoggerFactory;
import java.util.zip.InflaterInputStream;
import java.util.zip.Inflater;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.ImgRaw;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.ByteBuffer;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfLiteral;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.io.ByteArrayInputStream;
import com.itextpdf.text.Utilities;
import java.io.IOException;
import com.itextpdf.text.Image;
import java.net.URL;
import com.itextpdf.text.pdf.ICC_Profile;
import com.itextpdf.text.pdf.PdfDictionary;
import java.io.DataInputStream;
import java.io.InputStream;
import com.itextpdf.text.pdf.PdfName;

public class PngImage
{
    public static final int[] PNGID;
    public static final String IHDR = "IHDR";
    public static final String PLTE = "PLTE";
    public static final String IDAT = "IDAT";
    public static final String IEND = "IEND";
    public static final String tRNS = "tRNS";
    public static final String pHYs = "pHYs";
    public static final String gAMA = "gAMA";
    public static final String cHRM = "cHRM";
    public static final String sRGB = "sRGB";
    public static final String iCCP = "iCCP";
    private static final int TRANSFERSIZE = 4096;
    private static final int PNG_FILTER_NONE = 0;
    private static final int PNG_FILTER_SUB = 1;
    private static final int PNG_FILTER_UP = 2;
    private static final int PNG_FILTER_AVERAGE = 3;
    private static final int PNG_FILTER_PAETH = 4;
    private static final PdfName[] intents;
    InputStream is;
    DataInputStream dataStream;
    int width;
    int height;
    int bitDepth;
    int colorType;
    int compressionMethod;
    int filterMethod;
    int interlaceMethod;
    PdfDictionary additional;
    byte[] image;
    byte[] smask;
    byte[] trans;
    NewByteArrayOutputStream idat;
    int dpiX;
    int dpiY;
    float XYRatio;
    boolean genBWMask;
    boolean palShades;
    int transRedGray;
    int transGreen;
    int transBlue;
    int inputBands;
    int bytesPerPixel;
    byte[] colorTable;
    float gamma;
    boolean hasCHRM;
    float xW;
    float yW;
    float xR;
    float yR;
    float xG;
    float yG;
    float xB;
    float yB;
    PdfName intent;
    ICC_Profile icc_profile;
    
    PngImage(final InputStream is) {
        this.additional = new PdfDictionary();
        this.idat = new NewByteArrayOutputStream();
        this.transRedGray = -1;
        this.transGreen = -1;
        this.transBlue = -1;
        this.gamma = 1.0f;
        this.hasCHRM = false;
        this.is = is;
    }
    
    public static Image getImage(final URL url) throws IOException {
        InputStream is = null;
        try {
            is = url.openStream();
            final Image img = getImage(is);
            img.setUrl(url);
            return img;
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
    }
    
    public static Image getImage(final InputStream is) throws IOException {
        final PngImage png = new PngImage(is);
        return png.getImage();
    }
    
    public static Image getImage(final String file) throws IOException {
        return getImage(Utilities.toURL(file));
    }
    
    public static Image getImage(final byte[] data) throws IOException {
        final ByteArrayInputStream is = new ByteArrayInputStream(data);
        final Image img = getImage(is);
        img.setOriginalData(data);
        return img;
    }
    
    boolean checkMarker(final String s) {
        if (s.length() != 4) {
            return false;
        }
        for (int k = 0; k < 4; ++k) {
            final char c = s.charAt(k);
            if ((c < 'a' || c > 'z') && (c < 'A' || c > 'Z')) {
                return false;
            }
        }
        return true;
    }
    
    void readPng() throws IOException {
        for (int i = 0; i < PngImage.PNGID.length; ++i) {
            if (PngImage.PNGID[i] != this.is.read()) {
                throw new IOException(MessageLocalization.getComposedMessage("file.is.not.a.valid.png", new Object[0]));
            }
        }
        final byte[] buffer = new byte[4096];
        while (true) {
            int len = getInt(this.is);
            final String marker = getString(this.is);
            if (len < 0 || !this.checkMarker(marker)) {
                throw new IOException(MessageLocalization.getComposedMessage("corrupted.png.file", new Object[0]));
            }
            if ("IDAT".equals(marker)) {
                while (len != 0) {
                    final int size = this.is.read(buffer, 0, Math.min(len, 4096));
                    if (size < 0) {
                        return;
                    }
                    this.idat.write(buffer, 0, size);
                    len -= size;
                }
            }
            else if ("tRNS".equals(marker)) {
                switch (this.colorType) {
                    case 0: {
                        if (len >= 2) {
                            len -= 2;
                            final int gray = getWord(this.is);
                            if (this.bitDepth == 16) {
                                this.transRedGray = gray;
                            }
                            else {
                                this.additional.put(PdfName.MASK, new PdfLiteral("[" + gray + " " + gray + "]"));
                            }
                            break;
                        }
                        break;
                    }
                    case 2: {
                        if (len >= 6) {
                            len -= 6;
                            final int red = getWord(this.is);
                            final int green = getWord(this.is);
                            final int blue = getWord(this.is);
                            if (this.bitDepth == 16) {
                                this.transRedGray = red;
                                this.transGreen = green;
                                this.transBlue = blue;
                            }
                            else {
                                this.additional.put(PdfName.MASK, new PdfLiteral("[" + red + " " + red + " " + green + " " + green + " " + blue + " " + blue + "]"));
                            }
                            break;
                        }
                        break;
                    }
                    case 3: {
                        if (len > 0) {
                            this.trans = new byte[len];
                            for (int k = 0; k < len; ++k) {
                                this.trans[k] = (byte)this.is.read();
                            }
                            len = 0;
                            break;
                        }
                        break;
                    }
                }
                Utilities.skip(this.is, len);
            }
            else if ("IHDR".equals(marker)) {
                this.width = getInt(this.is);
                this.height = getInt(this.is);
                this.bitDepth = this.is.read();
                this.colorType = this.is.read();
                this.compressionMethod = this.is.read();
                this.filterMethod = this.is.read();
                this.interlaceMethod = this.is.read();
            }
            else if ("PLTE".equals(marker)) {
                if (this.colorType == 3) {
                    final PdfArray colorspace = new PdfArray();
                    colorspace.add(PdfName.INDEXED);
                    colorspace.add(this.getColorspace());
                    colorspace.add(new PdfNumber(len / 3 - 1));
                    final ByteBuffer colortable = new ByteBuffer();
                    while (len-- > 0) {
                        colortable.append_i(this.is.read());
                    }
                    colorspace.add(new PdfString(this.colorTable = colortable.toByteArray()));
                    this.additional.put(PdfName.COLORSPACE, colorspace);
                }
                else {
                    Utilities.skip(this.is, len);
                }
            }
            else if ("pHYs".equals(marker)) {
                final int dx = getInt(this.is);
                final int dy = getInt(this.is);
                final int unit = this.is.read();
                if (unit == 1) {
                    this.dpiX = (int)(dx * 0.0254f + 0.5f);
                    this.dpiY = (int)(dy * 0.0254f + 0.5f);
                }
                else if (dy != 0) {
                    this.XYRatio = dx / (float)dy;
                }
            }
            else if ("cHRM".equals(marker)) {
                this.xW = getInt(this.is) / 100000.0f;
                this.yW = getInt(this.is) / 100000.0f;
                this.xR = getInt(this.is) / 100000.0f;
                this.yR = getInt(this.is) / 100000.0f;
                this.xG = getInt(this.is) / 100000.0f;
                this.yG = getInt(this.is) / 100000.0f;
                this.xB = getInt(this.is) / 100000.0f;
                this.yB = getInt(this.is) / 100000.0f;
                this.hasCHRM = (Math.abs(this.xW) >= 1.0E-4f && Math.abs(this.yW) >= 1.0E-4f && Math.abs(this.xR) >= 1.0E-4f && Math.abs(this.yR) >= 1.0E-4f && Math.abs(this.xG) >= 1.0E-4f && Math.abs(this.yG) >= 1.0E-4f && Math.abs(this.xB) >= 1.0E-4f && Math.abs(this.yB) >= 1.0E-4f);
            }
            else if ("sRGB".equals(marker)) {
                final int ri = this.is.read();
                this.intent = PngImage.intents[ri];
                this.gamma = 2.2f;
                this.xW = 0.3127f;
                this.yW = 0.329f;
                this.xR = 0.64f;
                this.yR = 0.33f;
                this.xG = 0.3f;
                this.yG = 0.6f;
                this.xB = 0.15f;
                this.yB = 0.06f;
                this.hasCHRM = true;
            }
            else if ("gAMA".equals(marker)) {
                final int gm = getInt(this.is);
                if (gm != 0) {
                    this.gamma = 100000.0f / gm;
                    if (!this.hasCHRM) {
                        this.xW = 0.3127f;
                        this.yW = 0.329f;
                        this.xR = 0.64f;
                        this.yR = 0.33f;
                        this.xG = 0.3f;
                        this.yG = 0.6f;
                        this.xB = 0.15f;
                        this.yB = 0.06f;
                        this.hasCHRM = true;
                    }
                }
            }
            else if ("iCCP".equals(marker)) {
                do {
                    --len;
                } while (this.is.read() != 0);
                this.is.read();
                byte[] icccom = new byte[--len];
                int p = 0;
                while (len > 0) {
                    final int r = this.is.read(icccom, p, len);
                    if (r < 0) {
                        throw new IOException(MessageLocalization.getComposedMessage("premature.end.of.file", new Object[0]));
                    }
                    p += r;
                    len -= r;
                }
                final byte[] iccp = PdfReader.FlateDecode(icccom, true);
                icccom = null;
                try {
                    this.icc_profile = ICC_Profile.getInstance(iccp);
                }
                catch (RuntimeException e) {
                    this.icc_profile = null;
                }
            }
            else {
                if ("IEND".equals(marker)) {
                    return;
                }
                Utilities.skip(this.is, len);
            }
            Utilities.skip(this.is, 4);
        }
    }
    
    PdfObject getColorspace() {
        if (this.icc_profile != null) {
            if ((this.colorType & 0x2) == 0x0) {
                return PdfName.DEVICEGRAY;
            }
            return PdfName.DEVICERGB;
        }
        else {
            if (this.gamma != 1.0f || this.hasCHRM) {
                final PdfArray array = new PdfArray();
                final PdfDictionary dic = new PdfDictionary();
                if ((this.colorType & 0x2) == 0x0) {
                    if (this.gamma == 1.0f) {
                        return PdfName.DEVICEGRAY;
                    }
                    array.add(PdfName.CALGRAY);
                    dic.put(PdfName.GAMMA, new PdfNumber(this.gamma));
                    dic.put(PdfName.WHITEPOINT, new PdfLiteral("[1 1 1]"));
                    array.add(dic);
                }
                else {
                    PdfObject wp = new PdfLiteral("[1 1 1]");
                    array.add(PdfName.CALRGB);
                    if (this.gamma != 1.0f) {
                        final PdfArray gm = new PdfArray();
                        final PdfNumber n = new PdfNumber(this.gamma);
                        gm.add(n);
                        gm.add(n);
                        gm.add(n);
                        dic.put(PdfName.GAMMA, gm);
                    }
                    if (this.hasCHRM) {
                        final float z = this.yW * ((this.xG - this.xB) * this.yR - (this.xR - this.xB) * this.yG + (this.xR - this.xG) * this.yB);
                        final float YA = this.yR * ((this.xG - this.xB) * this.yW - (this.xW - this.xB) * this.yG + (this.xW - this.xG) * this.yB) / z;
                        final float XA = YA * this.xR / this.yR;
                        final float ZA = YA * ((1.0f - this.xR) / this.yR - 1.0f);
                        final float YB = -this.yG * ((this.xR - this.xB) * this.yW - (this.xW - this.xB) * this.yR + (this.xW - this.xR) * this.yB) / z;
                        final float XB = YB * this.xG / this.yG;
                        final float ZB = YB * ((1.0f - this.xG) / this.yG - 1.0f);
                        final float YC = this.yB * ((this.xR - this.xG) * this.yW - (this.xW - this.xG) * this.yW + (this.xW - this.xR) * this.yG) / z;
                        final float XC = YC * this.xB / this.yB;
                        final float ZC = YC * ((1.0f - this.xB) / this.yB - 1.0f);
                        final float XW = XA + XB + XC;
                        final float YW = 1.0f;
                        final float ZW = ZA + ZB + ZC;
                        final PdfArray wpa = new PdfArray();
                        wpa.add(new PdfNumber(XW));
                        wpa.add(new PdfNumber(YW));
                        wpa.add(new PdfNumber(ZW));
                        wp = wpa;
                        final PdfArray matrix = new PdfArray();
                        matrix.add(new PdfNumber(XA));
                        matrix.add(new PdfNumber(YA));
                        matrix.add(new PdfNumber(ZA));
                        matrix.add(new PdfNumber(XB));
                        matrix.add(new PdfNumber(YB));
                        matrix.add(new PdfNumber(ZB));
                        matrix.add(new PdfNumber(XC));
                        matrix.add(new PdfNumber(YC));
                        matrix.add(new PdfNumber(ZC));
                        dic.put(PdfName.MATRIX, matrix);
                    }
                    dic.put(PdfName.WHITEPOINT, wp);
                    array.add(dic);
                }
                return array;
            }
            if ((this.colorType & 0x2) == 0x0) {
                return PdfName.DEVICEGRAY;
            }
            return PdfName.DEVICERGB;
        }
    }
    
    Image getImage() throws IOException {
        this.readPng();
        this.checkIccProfile();
        try {
            int pal0 = 0;
            int palIdx = 0;
            this.palShades = false;
            if (this.trans != null) {
                for (int k = 0; k < this.trans.length; ++k) {
                    final int n = this.trans[k] & 0xFF;
                    if (n == 0) {
                        ++pal0;
                        palIdx = k;
                    }
                    if (n != 0 && n != 255) {
                        this.palShades = true;
                        break;
                    }
                }
            }
            if ((this.colorType & 0x4) != 0x0) {
                this.palShades = true;
            }
            this.genBWMask = (!this.palShades && (pal0 > 1 || this.transRedGray >= 0));
            if (!this.palShades && !this.genBWMask && pal0 == 1) {
                this.additional.put(PdfName.MASK, new PdfLiteral("[" + palIdx + " " + palIdx + "]"));
            }
            final boolean needDecode = this.interlaceMethod == 1 || this.bitDepth == 16 || (this.colorType & 0x4) != 0x0 || this.palShades || this.genBWMask;
            switch (this.colorType) {
                case 0: {
                    this.inputBands = 1;
                    break;
                }
                case 2: {
                    this.inputBands = 3;
                    break;
                }
                case 3: {
                    this.inputBands = 1;
                    break;
                }
                case 4: {
                    this.inputBands = 2;
                    break;
                }
                case 6: {
                    this.inputBands = 4;
                    break;
                }
            }
            if (needDecode) {
                this.decodeIdat();
            }
            int components = this.inputBands;
            if ((this.colorType & 0x4) != 0x0) {
                --components;
            }
            int bpc = this.bitDepth;
            if (bpc == 16) {
                bpc = 8;
            }
            Image img;
            if (this.image != null) {
                if (this.colorType == 3) {
                    img = new ImgRaw(this.width, this.height, components, bpc, this.image);
                }
                else {
                    img = Image.getInstance(this.width, this.height, components, bpc, this.image);
                }
            }
            else {
                img = new ImgRaw(this.width, this.height, components, bpc, this.idat.toByteArray());
                img.setDeflated(true);
                final PdfDictionary decodeparms = new PdfDictionary();
                decodeparms.put(PdfName.BITSPERCOMPONENT, new PdfNumber(this.bitDepth));
                decodeparms.put(PdfName.PREDICTOR, new PdfNumber(15));
                decodeparms.put(PdfName.COLUMNS, new PdfNumber(this.width));
                decodeparms.put(PdfName.COLORS, new PdfNumber((this.colorType == 3 || (this.colorType & 0x2) == 0x0) ? 1 : 3));
                this.additional.put(PdfName.DECODEPARMS, decodeparms);
            }
            if (this.additional.get(PdfName.COLORSPACE) == null) {
                this.additional.put(PdfName.COLORSPACE, this.getColorspace());
            }
            if (this.intent != null) {
                this.additional.put(PdfName.INTENT, this.intent);
            }
            if (this.additional.size() > 0) {
                img.setAdditional(this.additional);
            }
            if (this.icc_profile != null) {
                img.tagICC(this.icc_profile);
            }
            if (this.palShades) {
                final Image im2 = Image.getInstance(this.width, this.height, 1, 8, this.smask);
                im2.makeMask();
                img.setImageMask(im2);
            }
            if (this.genBWMask) {
                final Image im2 = Image.getInstance(this.width, this.height, 1, 1, this.smask);
                im2.makeMask();
                img.setImageMask(im2);
            }
            img.setDpi(this.dpiX, this.dpiY);
            img.setXYRatio(this.XYRatio);
            img.setOriginalType(2);
            return img;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    void decodeIdat() {
        int nbitDepth = this.bitDepth;
        if (nbitDepth == 16) {
            nbitDepth = 8;
        }
        int size = -1;
        this.bytesPerPixel = ((this.bitDepth == 16) ? 2 : 1);
        switch (this.colorType) {
            case 0: {
                size = (nbitDepth * this.width + 7) / 8 * this.height;
                break;
            }
            case 2: {
                size = this.width * 3 * this.height;
                this.bytesPerPixel *= 3;
                break;
            }
            case 3: {
                if (this.interlaceMethod == 1) {
                    size = (nbitDepth * this.width + 7) / 8 * this.height;
                }
                this.bytesPerPixel = 1;
                break;
            }
            case 4: {
                size = this.width * this.height;
                this.bytesPerPixel *= 2;
                break;
            }
            case 6: {
                size = this.width * 3 * this.height;
                this.bytesPerPixel *= 4;
                break;
            }
        }
        if (size >= 0) {
            this.image = new byte[size];
        }
        if (this.palShades) {
            this.smask = new byte[this.width * this.height];
        }
        else if (this.genBWMask) {
            this.smask = new byte[(this.width + 7) / 8 * this.height];
        }
        final ByteArrayInputStream bai = new ByteArrayInputStream(this.idat.getBuf(), 0, this.idat.size());
        final InputStream infStream = new InflaterInputStream(bai, new Inflater());
        this.dataStream = new DataInputStream(infStream);
        if (this.interlaceMethod != 1) {
            this.decodePass(0, 0, 1, 1, this.width, this.height);
        }
        else {
            this.decodePass(0, 0, 8, 8, (this.width + 7) / 8, (this.height + 7) / 8);
            this.decodePass(4, 0, 8, 8, (this.width + 3) / 8, (this.height + 7) / 8);
            this.decodePass(0, 4, 4, 8, (this.width + 3) / 4, (this.height + 3) / 8);
            this.decodePass(2, 0, 4, 4, (this.width + 1) / 4, (this.height + 3) / 4);
            this.decodePass(0, 2, 2, 4, (this.width + 1) / 2, (this.height + 1) / 4);
            this.decodePass(1, 0, 2, 2, this.width / 2, (this.height + 1) / 2);
            this.decodePass(0, 1, 1, 2, this.width, this.height / 2);
        }
        try {
            this.dataStream.close();
        }
        catch (IOException e) {
            final Logger logger = LoggerFactory.getLogger(this.getClass());
            logger.warn("Datastream of PngImage#decodeIdat didn't close properly.");
        }
    }
    
    void decodePass(final int xOffset, final int yOffset, final int xStep, final int yStep, final int passWidth, final int passHeight) {
        if (passWidth == 0 || passHeight == 0) {
            return;
        }
        final int bytesPerRow = (this.inputBands * passWidth * this.bitDepth + 7) / 8;
        byte[] curr = new byte[bytesPerRow];
        byte[] prior = new byte[bytesPerRow];
        for (int srcY = 0, dstY = yOffset; srcY < passHeight; ++srcY, dstY += yStep) {
            int filter = 0;
            try {
                filter = this.dataStream.read();
                this.dataStream.readFully(curr, 0, bytesPerRow);
            }
            catch (Exception ex) {}
            switch (filter) {
                case 0: {
                    break;
                }
                case 1: {
                    decodeSubFilter(curr, bytesPerRow, this.bytesPerPixel);
                    break;
                }
                case 2: {
                    decodeUpFilter(curr, prior, bytesPerRow);
                    break;
                }
                case 3: {
                    decodeAverageFilter(curr, prior, bytesPerRow, this.bytesPerPixel);
                    break;
                }
                case 4: {
                    decodePaethFilter(curr, prior, bytesPerRow, this.bytesPerPixel);
                    break;
                }
                default: {
                    throw new RuntimeException(MessageLocalization.getComposedMessage("png.filter.unknown", new Object[0]));
                }
            }
            this.processPixels(curr, xOffset, xStep, dstY, passWidth);
            final byte[] tmp = prior;
            prior = curr;
            curr = tmp;
        }
    }
    
    void processPixels(final byte[] curr, final int xOffset, final int step, final int y, final int width) {
        final int[] out = this.getPixel(curr);
        int sizes = 0;
        switch (this.colorType) {
            case 0:
            case 3:
            case 4: {
                sizes = 1;
                break;
            }
            case 2:
            case 6: {
                sizes = 3;
                break;
            }
        }
        if (this.image != null) {
            int dstX = xOffset;
            final int yStride = (sizes * this.width * ((this.bitDepth == 16) ? 8 : this.bitDepth) + 7) / 8;
            for (int srcX = 0; srcX < width; ++srcX) {
                setPixel(this.image, out, this.inputBands * srcX, sizes, dstX, y, this.bitDepth, yStride);
                dstX += step;
            }
        }
        if (this.palShades) {
            if ((this.colorType & 0x4) != 0x0) {
                if (this.bitDepth == 16) {
                    for (int k = 0; k < width; ++k) {
                        final int[] array = out;
                        final int n = k * this.inputBands + sizes;
                        array[n] >>>= 8;
                    }
                }
                final int yStride = this.width;
                int dstX = xOffset;
                for (int srcX = 0; srcX < width; ++srcX) {
                    setPixel(this.smask, out, this.inputBands * srcX + sizes, 1, dstX, y, 8, yStride);
                    dstX += step;
                }
            }
            else {
                final int yStride = this.width;
                final int[] v = { 0 };
                int dstX = xOffset;
                for (final int idx : out) {
                    if (idx < this.trans.length) {
                        v[0] = this.trans[idx];
                    }
                    else {
                        v[0] = 255;
                    }
                    setPixel(this.smask, v, 0, 1, dstX, y, 8, yStride);
                    dstX += step;
                }
            }
        }
        else if (this.genBWMask) {
            switch (this.colorType) {
                case 3: {
                    final int yStride = (this.width + 7) / 8;
                    final int[] v = { 0 };
                    int dstX = xOffset;
                    for (final int idx : out) {
                        v[0] = ((idx < this.trans.length && this.trans[idx] == 0) ? 1 : 0);
                        setPixel(this.smask, v, 0, 1, dstX, y, 1, yStride);
                        dstX += step;
                    }
                    break;
                }
                case 0: {
                    final int yStride = (this.width + 7) / 8;
                    final int[] v = { 0 };
                    int dstX = xOffset;
                    for (final int g : out) {
                        v[0] = ((g == this.transRedGray) ? 1 : 0);
                        setPixel(this.smask, v, 0, 1, dstX, y, 1, yStride);
                        dstX += step;
                    }
                    break;
                }
                case 2: {
                    final int yStride = (this.width + 7) / 8;
                    final int[] v = { 0 };
                    int dstX = xOffset;
                    for (int srcX = 0; srcX < width; ++srcX) {
                        final int markRed = this.inputBands * srcX;
                        v[0] = ((out[markRed] == this.transRedGray && out[markRed + 1] == this.transGreen && out[markRed + 2] == this.transBlue) ? 1 : 0);
                        setPixel(this.smask, v, 0, 1, dstX, y, 1, yStride);
                        dstX += step;
                    }
                    break;
                }
            }
        }
    }
    
    static int getPixel(final byte[] image, final int x, final int y, final int bitDepth, final int bytesPerRow) {
        if (bitDepth == 8) {
            final int pos = bytesPerRow * y + x;
            return image[pos] & 0xFF;
        }
        final int pos = bytesPerRow * y + x / (8 / bitDepth);
        final int v = image[pos] >> 8 - bitDepth * (x % (8 / bitDepth)) - bitDepth;
        return v & (1 << bitDepth) - 1;
    }
    
    static void setPixel(final byte[] image, final int[] data, final int offset, final int size, final int x, final int y, final int bitDepth, final int bytesPerRow) {
        if (bitDepth == 8) {
            final int pos = bytesPerRow * y + size * x;
            for (int k = 0; k < size; ++k) {
                image[pos + k] = (byte)data[k + offset];
            }
        }
        else if (bitDepth == 16) {
            final int pos = bytesPerRow * y + size * x;
            for (int k = 0; k < size; ++k) {
                image[pos + k] = (byte)(data[k + offset] >>> 8);
            }
        }
        else {
            final int pos = bytesPerRow * y + x / (8 / bitDepth);
            final int v = data[offset] << 8 - bitDepth * (x % (8 / bitDepth)) - bitDepth;
            final int n = pos;
            image[n] |= (byte)v;
        }
    }
    
    int[] getPixel(final byte[] curr) {
        switch (this.bitDepth) {
            case 8: {
                final int[] out = new int[curr.length];
                for (int k = 0; k < out.length; ++k) {
                    out[k] = (curr[k] & 0xFF);
                }
                return out;
            }
            case 16: {
                final int[] out = new int[curr.length / 2];
                for (int k = 0; k < out.length; ++k) {
                    out[k] = ((curr[k * 2] & 0xFF) << 8) + (curr[k * 2 + 1] & 0xFF);
                }
                return out;
            }
            default: {
                final int[] out = new int[curr.length * 8 / this.bitDepth];
                int idx = 0;
                final int passes = 8 / this.bitDepth;
                final int mask = (1 << this.bitDepth) - 1;
                for (int i = 0; i < curr.length; ++i) {
                    for (int j = passes - 1; j >= 0; --j) {
                        out[idx++] = (curr[i] >>> this.bitDepth * j & mask);
                    }
                }
                return out;
            }
        }
    }
    
    private int getExpectedIccNumberOfComponents() {
        if (this.colorType == 0 || this.colorType == 4) {
            return 1;
        }
        return 3;
    }
    
    private void checkIccProfile() {
        if (this.icc_profile != null && this.icc_profile.getNumComponents() != this.getExpectedIccNumberOfComponents()) {
            LoggerFactory.getLogger(this.getClass()).warn(MessageLocalization.getComposedMessage("unexpected.color.space.in.embedded.icc.profile", new Object[0]));
            this.icc_profile = null;
        }
    }
    
    private static void decodeSubFilter(final byte[] curr, final int count, final int bpp) {
        for (int i = bpp; i < count; ++i) {
            int val = curr[i] & 0xFF;
            val += (curr[i - bpp] & 0xFF);
            curr[i] = (byte)val;
        }
    }
    
    private static void decodeUpFilter(final byte[] curr, final byte[] prev, final int count) {
        for (int i = 0; i < count; ++i) {
            final int raw = curr[i] & 0xFF;
            final int prior = prev[i] & 0xFF;
            curr[i] = (byte)(raw + prior);
        }
    }
    
    private static void decodeAverageFilter(final byte[] curr, final byte[] prev, final int count, final int bpp) {
        for (int i = 0; i < bpp; ++i) {
            final int raw = curr[i] & 0xFF;
            final int priorRow = prev[i] & 0xFF;
            curr[i] = (byte)(raw + priorRow / 2);
        }
        for (int i = bpp; i < count; ++i) {
            final int raw = curr[i] & 0xFF;
            final int priorPixel = curr[i - bpp] & 0xFF;
            final int priorRow = prev[i] & 0xFF;
            curr[i] = (byte)(raw + (priorPixel + priorRow) / 2);
        }
    }
    
    private static int paethPredictor(final int a, final int b, final int c) {
        final int p = a + b - c;
        final int pa = Math.abs(p - a);
        final int pb = Math.abs(p - b);
        final int pc = Math.abs(p - c);
        if (pa <= pb && pa <= pc) {
            return a;
        }
        if (pb <= pc) {
            return b;
        }
        return c;
    }
    
    private static void decodePaethFilter(final byte[] curr, final byte[] prev, final int count, final int bpp) {
        for (int i = 0; i < bpp; ++i) {
            final int raw = curr[i] & 0xFF;
            final int priorRow = prev[i] & 0xFF;
            curr[i] = (byte)(raw + priorRow);
        }
        for (int i = bpp; i < count; ++i) {
            final int raw = curr[i] & 0xFF;
            final int priorPixel = curr[i - bpp] & 0xFF;
            final int priorRow = prev[i] & 0xFF;
            final int priorRowPixel = prev[i - bpp] & 0xFF;
            curr[i] = (byte)(raw + paethPredictor(priorPixel, priorRow, priorRowPixel));
        }
    }
    
    public static final int getInt(final InputStream is) throws IOException {
        return (is.read() << 24) + (is.read() << 16) + (is.read() << 8) + is.read();
    }
    
    public static final int getWord(final InputStream is) throws IOException {
        return (is.read() << 8) + is.read();
    }
    
    public static final String getString(final InputStream is) throws IOException {
        final StringBuffer buf = new StringBuffer();
        for (int i = 0; i < 4; ++i) {
            buf.append((char)is.read());
        }
        return buf.toString();
    }
    
    static {
        PNGID = new int[] { 137, 80, 78, 71, 13, 10, 26, 10 };
        intents = new PdfName[] { PdfName.PERCEPTUAL, PdfName.RELATIVECOLORIMETRIC, PdfName.SATURATION, PdfName.ABSOLUTECOLORIMETRIC };
    }
    
    static class NewByteArrayOutputStream extends ByteArrayOutputStream
    {
        public byte[] getBuf() {
            return this.buf;
        }
    }
}
