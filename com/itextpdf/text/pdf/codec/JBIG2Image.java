// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.codec;

import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.ImgJBIG2;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;

public class JBIG2Image
{
    public static byte[] getGlobalSegment(final RandomAccessFileOrArray ra) {
        try {
            final JBIG2SegmentReader sr = new JBIG2SegmentReader(ra);
            sr.read();
            return sr.getGlobal(true);
        }
        catch (Exception e) {
            return null;
        }
    }
    
    public static Image getJbig2Image(final RandomAccessFileOrArray ra, final int page) {
        if (page < 1) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.page.number.must.be.gt.eq.1", new Object[0]));
        }
        try {
            final JBIG2SegmentReader sr = new JBIG2SegmentReader(ra);
            sr.read();
            final JBIG2SegmentReader.JBIG2Page p = sr.getPage(page);
            final Image img = new ImgJBIG2(p.pageBitmapWidth, p.pageBitmapHeight, p.getData(true), sr.getGlobal(true));
            return img;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public static int getNumberOfPages(final RandomAccessFileOrArray ra) {
        try {
            final JBIG2SegmentReader sr = new JBIG2SegmentReader(ra);
            sr.read();
            return sr.numberOfPages();
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
}
