// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.codec;

import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.ImgRaw;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.io.BufferedInputStream;
import com.itextpdf.text.Image;
import com.itextpdf.text.Utilities;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.net.URL;
import java.io.DataInputStream;

public class GifImage
{
    protected DataInputStream in;
    protected int width;
    protected int height;
    protected boolean gctFlag;
    protected int bgIndex;
    protected int bgColor;
    protected int pixelAspect;
    protected boolean lctFlag;
    protected boolean interlace;
    protected int lctSize;
    protected int ix;
    protected int iy;
    protected int iw;
    protected int ih;
    protected byte[] block;
    protected int blockSize;
    protected int dispose;
    protected boolean transparency;
    protected int delay;
    protected int transIndex;
    protected static final int MaxStackSize = 4096;
    protected short[] prefix;
    protected byte[] suffix;
    protected byte[] pixelStack;
    protected byte[] pixels;
    protected byte[] m_out;
    protected int m_bpc;
    protected int m_gbpc;
    protected byte[] m_global_table;
    protected byte[] m_local_table;
    protected byte[] m_curr_table;
    protected int m_line_stride;
    protected byte[] fromData;
    protected URL fromUrl;
    protected ArrayList<GifFrame> frames;
    
    public GifImage(final URL url) throws IOException {
        this.block = new byte[256];
        this.blockSize = 0;
        this.dispose = 0;
        this.transparency = false;
        this.delay = 0;
        this.frames = new ArrayList<GifFrame>();
        this.fromUrl = url;
        InputStream is = null;
        try {
            is = url.openStream();
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int read = 0;
            final byte[] bytes = new byte[1024];
            while ((read = is.read(bytes)) != -1) {
                baos.write(bytes, 0, read);
            }
            is.close();
            is = new ByteArrayInputStream(baos.toByteArray());
            baos.flush();
            baos.close();
            this.process(is);
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
    }
    
    public GifImage(final String file) throws IOException {
        this(Utilities.toURL(file));
    }
    
    public GifImage(final byte[] data) throws IOException {
        this.block = new byte[256];
        this.blockSize = 0;
        this.dispose = 0;
        this.transparency = false;
        this.delay = 0;
        this.frames = new ArrayList<GifFrame>();
        this.fromData = data;
        InputStream is = null;
        try {
            is = new ByteArrayInputStream(data);
            this.process(is);
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
    }
    
    public GifImage(final InputStream is) throws IOException {
        this.block = new byte[256];
        this.blockSize = 0;
        this.dispose = 0;
        this.transparency = false;
        this.delay = 0;
        this.frames = new ArrayList<GifFrame>();
        this.process(is);
    }
    
    public int getFrameCount() {
        return this.frames.size();
    }
    
    public Image getImage(final int frame) {
        final GifFrame gf = this.frames.get(frame - 1);
        return gf.image;
    }
    
    public int[] getFramePosition(final int frame) {
        final GifFrame gf = this.frames.get(frame - 1);
        return new int[] { gf.ix, gf.iy };
    }
    
    public int[] getLogicalScreen() {
        return new int[] { this.width, this.height };
    }
    
    void process(final InputStream is) throws IOException {
        this.in = new DataInputStream(new BufferedInputStream(is));
        this.readHeader();
        this.readContents();
        if (this.frames.isEmpty()) {
            throw new IOException(MessageLocalization.getComposedMessage("the.file.does.not.contain.any.valid.image", new Object[0]));
        }
    }
    
    protected void readHeader() throws IOException {
        final StringBuilder id = new StringBuilder("");
        for (int i = 0; i < 6; ++i) {
            id.append((char)this.in.read());
        }
        if (!id.toString().startsWith("GIF8")) {
            throw new IOException(MessageLocalization.getComposedMessage("gif.signature.nor.found", new Object[0]));
        }
        this.readLSD();
        if (this.gctFlag) {
            this.m_global_table = this.readColorTable(this.m_gbpc);
        }
    }
    
    protected void readLSD() throws IOException {
        this.width = this.readShort();
        this.height = this.readShort();
        final int packed = this.in.read();
        this.gctFlag = ((packed & 0x80) != 0x0);
        this.m_gbpc = (packed & 0x7) + 1;
        this.bgIndex = this.in.read();
        this.pixelAspect = this.in.read();
    }
    
    protected int readShort() throws IOException {
        return this.in.read() | this.in.read() << 8;
    }
    
    protected int readBlock() throws IOException {
        this.blockSize = this.in.read();
        if (this.blockSize <= 0) {
            return this.blockSize = 0;
        }
        return this.blockSize = this.in.read(this.block, 0, this.blockSize);
    }
    
    protected byte[] readColorTable(int bpc) throws IOException {
        final int ncolors = 1 << bpc;
        final int nbytes = 3 * ncolors;
        bpc = newBpc(bpc);
        final byte[] table = new byte[(1 << bpc) * 3];
        this.in.readFully(table, 0, nbytes);
        return table;
    }
    
    protected static int newBpc(final int bpc) {
        switch (bpc) {
            case 1:
            case 2:
            case 4: {
                return bpc;
            }
            case 3: {
                return 4;
            }
            default: {
                return 8;
            }
        }
    }
    
    protected void readContents() throws IOException {
        boolean done = false;
        while (!done) {
            int code = this.in.read();
            switch (code) {
                case 44: {
                    this.readImage();
                    continue;
                }
                case 33: {
                    code = this.in.read();
                    switch (code) {
                        case 249: {
                            this.readGraphicControlExt();
                            continue;
                        }
                        case 255: {
                            this.readBlock();
                            this.skip();
                            continue;
                        }
                        default: {
                            this.skip();
                            continue;
                        }
                    }
                    break;
                }
                default: {
                    done = true;
                    continue;
                }
            }
        }
    }
    
    protected void readImage() throws IOException {
        this.ix = this.readShort();
        this.iy = this.readShort();
        this.iw = this.readShort();
        this.ih = this.readShort();
        final int packed = this.in.read();
        this.lctFlag = ((packed & 0x80) != 0x0);
        this.interlace = ((packed & 0x40) != 0x0);
        this.lctSize = 2 << (packed & 0x7);
        this.m_bpc = newBpc(this.m_gbpc);
        if (this.lctFlag) {
            this.m_curr_table = this.readColorTable((packed & 0x7) + 1);
            this.m_bpc = newBpc((packed & 0x7) + 1);
        }
        else {
            this.m_curr_table = this.m_global_table;
        }
        if (this.transparency && this.transIndex >= this.m_curr_table.length / 3) {
            this.transparency = false;
        }
        if (this.transparency && this.m_bpc == 1) {
            final byte[] tp = new byte[12];
            System.arraycopy(this.m_curr_table, 0, tp, 0, 6);
            this.m_curr_table = tp;
            this.m_bpc = 2;
        }
        final boolean skipZero = this.decodeImageData();
        if (!skipZero) {
            this.skip();
        }
        Image img = null;
        try {
            img = new ImgRaw(this.iw, this.ih, 1, this.m_bpc, this.m_out);
            final PdfArray colorspace = new PdfArray();
            colorspace.add(PdfName.INDEXED);
            colorspace.add(PdfName.DEVICERGB);
            final int len = this.m_curr_table.length;
            colorspace.add(new PdfNumber(len / 3 - 1));
            colorspace.add(new PdfString(this.m_curr_table));
            final PdfDictionary ad = new PdfDictionary();
            ad.put(PdfName.COLORSPACE, colorspace);
            img.setAdditional(ad);
            if (this.transparency) {
                img.setTransparency(new int[] { this.transIndex, this.transIndex });
            }
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
        img.setOriginalType(3);
        img.setOriginalData(this.fromData);
        img.setUrl(this.fromUrl);
        final GifFrame gf = new GifFrame();
        gf.image = img;
        gf.ix = this.ix;
        gf.iy = this.iy;
        this.frames.add(gf);
    }
    
    protected boolean decodeImageData() throws IOException {
        final int NullCode = -1;
        final int npix = this.iw * this.ih;
        boolean skipZero = false;
        if (this.prefix == null) {
            this.prefix = new short[4096];
        }
        if (this.suffix == null) {
            this.suffix = new byte[4096];
        }
        if (this.pixelStack == null) {
            this.pixelStack = new byte[4097];
        }
        this.m_line_stride = (this.iw * this.m_bpc + 7) / 8;
        this.m_out = new byte[this.m_line_stride * this.ih];
        int pass = 1;
        int inc = this.interlace ? 8 : 1;
        int line = 0;
        int xpos = 0;
        final int data_size = this.in.read();
        final int clear = 1 << data_size;
        final int end_of_information = clear + 1;
        int available = clear + 2;
        int old_code = NullCode;
        int code_size = data_size + 1;
        int code_mask = (1 << code_size) - 1;
        for (int code = 0; code < clear; ++code) {
            this.prefix[code] = 0;
            this.suffix[code] = (byte)code;
        }
        int bi;
        int top;
        int first;
        int count;
        int datum;
        int bits = datum = (count = (first = (top = (bi = 0))));
        int i = 0;
        while (i < npix) {
            if (top == 0) {
                if (bits < code_size) {
                    if (count == 0) {
                        count = this.readBlock();
                        if (count <= 0) {
                            skipZero = true;
                            break;
                        }
                        bi = 0;
                    }
                    datum += (this.block[bi] & 0xFF) << bits;
                    bits += 8;
                    ++bi;
                    --count;
                    continue;
                }
                int code = datum & code_mask;
                datum >>= code_size;
                bits -= code_size;
                if (code > available) {
                    break;
                }
                if (code == end_of_information) {
                    break;
                }
                if (code == clear) {
                    code_size = data_size + 1;
                    code_mask = (1 << code_size) - 1;
                    available = clear + 2;
                    old_code = NullCode;
                    continue;
                }
                if (old_code == NullCode) {
                    this.pixelStack[top++] = this.suffix[code];
                    old_code = code;
                    first = code;
                    continue;
                }
                final int in_code;
                if ((in_code = code) == available) {
                    this.pixelStack[top++] = (byte)first;
                    code = old_code;
                }
                while (code > clear) {
                    this.pixelStack[top++] = this.suffix[code];
                    code = this.prefix[code];
                }
                first = (this.suffix[code] & 0xFF);
                if (available >= 4096) {
                    break;
                }
                this.pixelStack[top++] = (byte)first;
                this.prefix[available] = (short)old_code;
                this.suffix[available] = (byte)first;
                if ((++available & code_mask) == 0x0 && available < 4096) {
                    ++code_size;
                    code_mask += available;
                }
                old_code = in_code;
            }
            --top;
            ++i;
            this.setPixel(xpos, line, this.pixelStack[top]);
            if (++xpos >= this.iw) {
                xpos = 0;
                line += inc;
                if (line < this.ih) {
                    continue;
                }
                if (this.interlace) {
                    do {
                        switch (++pass) {
                            case 2: {
                                line = 4;
                                continue;
                            }
                            case 3: {
                                line = 2;
                                inc = 4;
                                continue;
                            }
                            case 4: {
                                line = 1;
                                inc = 2;
                                continue;
                            }
                            default: {
                                line = this.ih - 1;
                                inc = 0;
                                continue;
                            }
                        }
                    } while (line >= this.ih);
                }
                else {
                    line = this.ih - 1;
                    inc = 0;
                }
            }
        }
        return skipZero;
    }
    
    protected void setPixel(final int x, final int y, final int v) {
        if (this.m_bpc == 8) {
            final int pos = x + this.iw * y;
            this.m_out[pos] = (byte)v;
        }
        else {
            final int pos = this.m_line_stride * y + x / (8 / this.m_bpc);
            final int vout = v << 8 - this.m_bpc * (x % (8 / this.m_bpc)) - this.m_bpc;
            final byte[] out = this.m_out;
            final int n = pos;
            out[n] |= (byte)vout;
        }
    }
    
    protected void resetFrame() {
    }
    
    protected void readGraphicControlExt() throws IOException {
        this.in.read();
        final int packed = this.in.read();
        this.dispose = (packed & 0x1C) >> 2;
        if (this.dispose == 0) {
            this.dispose = 1;
        }
        this.transparency = ((packed & 0x1) != 0x0);
        this.delay = this.readShort() * 10;
        this.transIndex = this.in.read();
        this.in.read();
    }
    
    protected void skip() throws IOException {
        do {
            this.readBlock();
        } while (this.blockSize > 0);
    }
    
    static class GifFrame
    {
        Image image;
        int ix;
        int iy;
    }
}
