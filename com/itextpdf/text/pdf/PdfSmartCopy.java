// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.ExceptionConverter;
import java.util.Arrays;
import java.security.MessageDigest;
import com.itextpdf.text.log.LoggerFactory;
import java.io.IOException;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.log.CounterFactory;
import java.io.OutputStream;
import com.itextpdf.text.Document;
import com.itextpdf.text.log.Counter;
import java.util.HashMap;
import com.itextpdf.text.log.Logger;

public class PdfSmartCopy extends PdfCopy
{
    private static final Logger LOGGER;
    private HashMap<ByteStore, PdfIndirectReference> streamMap;
    private final HashMap<RefKey, Integer> serialized;
    protected Counter COUNTER;
    
    @Override
    protected Counter getCounter() {
        return this.COUNTER;
    }
    
    public PdfSmartCopy(final Document document, final OutputStream os) throws DocumentException {
        super(document, os);
        this.streamMap = null;
        this.serialized = new HashMap<RefKey, Integer>();
        this.COUNTER = CounterFactory.getCounter(PdfSmartCopy.class);
        this.streamMap = new HashMap<ByteStore, PdfIndirectReference>();
    }
    
    @Override
    protected PdfIndirectReference copyIndirect(final PRIndirectReference in) throws IOException, BadPdfFormatException {
        final PdfObject srcObj = PdfReader.getPdfObjectRelease(in);
        ByteStore streamKey = null;
        boolean validStream = false;
        if (srcObj.isStream()) {
            streamKey = new ByteStore((PRStream)srcObj, this.serialized);
            validStream = true;
            final PdfIndirectReference streamRef = this.streamMap.get(streamKey);
            if (streamRef != null) {
                return streamRef;
            }
        }
        else if (srcObj.isDictionary()) {
            streamKey = new ByteStore((PdfDictionary)srcObj, this.serialized);
            validStream = true;
            final PdfIndirectReference streamRef = this.streamMap.get(streamKey);
            if (streamRef != null) {
                return streamRef;
            }
        }
        final RefKey key = new RefKey(in);
        IndirectReferences iRef = this.indirects.get(key);
        PdfIndirectReference theRef;
        if (iRef != null) {
            theRef = iRef.getRef();
            if (iRef.getCopied()) {
                return theRef;
            }
        }
        else {
            theRef = this.body.getPdfIndirectReference();
            iRef = new IndirectReferences(theRef);
            this.indirects.put(key, iRef);
        }
        if (srcObj.isDictionary()) {
            final PdfObject type = PdfReader.getPdfObjectRelease(((PdfDictionary)srcObj).get(PdfName.TYPE));
            if (type != null) {
                if (PdfName.PAGE.equals(type)) {
                    return theRef;
                }
                if (PdfName.CATALOG.equals(type)) {
                    PdfSmartCopy.LOGGER.warn(MessageLocalization.getComposedMessage("make.copy.of.catalog.dictionary.is.forbidden", new Object[0]));
                    return null;
                }
            }
        }
        iRef.setCopied();
        if (validStream) {
            this.streamMap.put(streamKey, theRef);
        }
        final PdfObject obj = this.copyObject(srcObj);
        this.addToBody(obj, theRef);
        return theRef;
    }
    
    @Override
    public void freeReader(final PdfReader reader) throws IOException {
        this.serialized.clear();
        super.freeReader(reader);
    }
    
    @Override
    public void addPage(final PdfImportedPage iPage) throws IOException, BadPdfFormatException {
        if (this.currentPdfReaderInstance.getReader() != this.reader) {
            this.serialized.clear();
        }
        super.addPage(iPage);
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(PdfSmartCopy.class);
    }
    
    static class ByteStore
    {
        private final byte[] b;
        private final int hash;
        private MessageDigest md5;
        
        private void serObject(PdfObject obj, final int level, ByteBuffer bb, final HashMap<RefKey, Integer> serialized) throws IOException {
            if (level <= 0) {
                return;
            }
            if (obj == null) {
                bb.append("$Lnull");
                return;
            }
            PdfIndirectReference ref = null;
            ByteBuffer savedBb = null;
            if (obj.isIndirect()) {
                ref = (PdfIndirectReference)obj;
                final RefKey key = new RefKey(ref);
                if (serialized.containsKey(key)) {
                    bb.append(serialized.get(key));
                    return;
                }
                savedBb = bb;
                bb = new ByteBuffer();
            }
            obj = PdfReader.getPdfObject(obj);
            if (obj.isStream()) {
                bb.append("$B");
                this.serDic((PdfDictionary)obj, level - 1, bb, serialized);
                if (level > 0) {
                    this.md5.reset();
                    bb.append(this.md5.digest(PdfReader.getStreamBytesRaw((PRStream)obj)));
                }
            }
            else if (obj.isDictionary()) {
                this.serDic((PdfDictionary)obj, level - 1, bb, serialized);
            }
            else if (obj.isArray()) {
                this.serArray((PdfArray)obj, level - 1, bb, serialized);
            }
            else if (obj.isString()) {
                bb.append("$S").append(obj.toString());
            }
            else if (obj.isName()) {
                bb.append("$N").append(obj.toString());
            }
            else {
                bb.append("$L").append(obj.toString());
            }
            if (savedBb != null) {
                final RefKey key = new RefKey(ref);
                if (!serialized.containsKey(key)) {
                    serialized.put(key, calculateHash(bb.getBuffer()));
                }
                savedBb.append(bb);
            }
        }
        
        private void serDic(final PdfDictionary dic, final int level, final ByteBuffer bb, final HashMap<RefKey, Integer> serialized) throws IOException {
            bb.append("$D");
            if (level <= 0) {
                return;
            }
            final Object[] keys = dic.getKeys().toArray();
            Arrays.sort(keys);
            for (int k = 0; k < keys.length; ++k) {
                if (keys[k].equals(PdfName.P)) {
                    if (dic.get((PdfName)keys[k]).isIndirect()) {
                        continue;
                    }
                    if (dic.get((PdfName)keys[k]).isDictionary()) {
                        continue;
                    }
                }
                this.serObject((PdfObject)keys[k], level, bb, serialized);
                this.serObject(dic.get((PdfName)keys[k]), level, bb, serialized);
            }
        }
        
        private void serArray(final PdfArray array, final int level, final ByteBuffer bb, final HashMap<RefKey, Integer> serialized) throws IOException {
            bb.append("$A");
            if (level <= 0) {
                return;
            }
            for (int k = 0; k < array.size(); ++k) {
                this.serObject(array.getPdfObject(k), level, bb, serialized);
            }
        }
        
        ByteStore(final PRStream str, final HashMap<RefKey, Integer> serialized) throws IOException {
            try {
                this.md5 = MessageDigest.getInstance("MD5");
            }
            catch (Exception e) {
                throw new ExceptionConverter(e);
            }
            final ByteBuffer bb = new ByteBuffer();
            final int level = 100;
            this.serObject(str, level, bb, serialized);
            this.b = bb.toByteArray();
            this.hash = calculateHash(this.b);
            this.md5 = null;
        }
        
        ByteStore(final PdfDictionary dict, final HashMap<RefKey, Integer> serialized) throws IOException {
            try {
                this.md5 = MessageDigest.getInstance("MD5");
            }
            catch (Exception e) {
                throw new ExceptionConverter(e);
            }
            final ByteBuffer bb = new ByteBuffer();
            final int level = 100;
            this.serObject(dict, level, bb, serialized);
            this.b = bb.toByteArray();
            this.hash = calculateHash(this.b);
            this.md5 = null;
        }
        
        private static int calculateHash(final byte[] b) {
            int hash = 0;
            for (int len = b.length, k = 0; k < len; ++k) {
                hash = hash * 31 + (b[k] & 0xFF);
            }
            return hash;
        }
        
        @Override
        public boolean equals(final Object obj) {
            return obj instanceof ByteStore && this.hashCode() == obj.hashCode() && Arrays.equals(this.b, ((ByteStore)obj).b);
        }
        
        @Override
        public int hashCode() {
            return this.hash;
        }
    }
}
