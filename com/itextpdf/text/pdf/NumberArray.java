// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.Iterator;
import java.util.List;

public class NumberArray extends PdfArray
{
    public NumberArray(final float... numbers) {
        for (final float f : numbers) {
            this.add(new PdfNumber(f));
        }
    }
    
    public NumberArray(final List<PdfNumber> numbers) {
        for (final PdfNumber n : numbers) {
            this.add(n);
        }
    }
}
