// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.Map;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.DocumentException;
import java.io.OutputStream;

class PdfCopyFormsImp extends PdfCopyFieldsImp
{
    PdfCopyFormsImp(final OutputStream os) throws DocumentException {
        super(os);
    }
    
    public void copyDocumentFields(PdfReader reader) throws DocumentException {
        if (!reader.isOpenedWithFullPermissions()) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("pdfreader.not.opened.with.owner.password", new Object[0]));
        }
        if (this.readers2intrefs.containsKey(reader)) {
            reader = new PdfReader(reader);
        }
        else {
            if (reader.isTampered()) {
                throw new DocumentException(MessageLocalization.getComposedMessage("the.document.was.reused", new Object[0]));
            }
            reader.consolidateNamedDestinations();
            reader.setTampered(true);
        }
        reader.shuffleSubsetNames();
        this.readers2intrefs.put(reader, new IntHashtable());
        this.visited.put(reader, new IntHashtable());
        this.fields.add(reader.getAcroFields());
        this.updateCalculationOrder(reader);
    }
    
    @Override
    void mergeFields() {
        for (int k = 0; k < this.fields.size(); ++k) {
            final Map<String, AcroFields.Item> fd = this.fields.get(k).getFields();
            this.mergeWithMaster(fd);
        }
    }
}
