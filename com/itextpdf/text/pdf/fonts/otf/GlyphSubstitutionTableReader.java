// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.otf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Collections;
import com.itextpdf.text.pdf.Glyph;
import java.util.LinkedHashMap;
import java.io.IOException;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import java.util.List;
import java.util.Map;

public class GlyphSubstitutionTableReader extends OpenTypeFontTableReader
{
    private final int[] glyphWidthsByIndex;
    private final Map<Integer, Character> glyphToCharacterMap;
    private Map<Integer, List<Integer>> rawLigatureSubstitutionMap;
    
    public GlyphSubstitutionTableReader(final RandomAccessFileOrArray rf, final int gsubTableLocation, final Map<Integer, Character> glyphToCharacterMap, final int[] glyphWidthsByIndex) throws IOException {
        super(rf, gsubTableLocation);
        this.glyphWidthsByIndex = glyphWidthsByIndex;
        this.glyphToCharacterMap = glyphToCharacterMap;
    }
    
    public void read() throws FontReadingException {
        this.rawLigatureSubstitutionMap = new LinkedHashMap<Integer, List<Integer>>();
        this.startReadingTable();
    }
    
    public Map<String, Glyph> getGlyphSubstitutionMap() throws FontReadingException {
        final Map<String, Glyph> glyphSubstitutionMap = new LinkedHashMap<String, Glyph>();
        for (final Integer glyphIdToReplace : this.rawLigatureSubstitutionMap.keySet()) {
            final List<Integer> constituentGlyphs = this.rawLigatureSubstitutionMap.get(glyphIdToReplace);
            final StringBuilder chars = new StringBuilder(constituentGlyphs.size());
            for (final Integer constituentGlyphId : constituentGlyphs) {
                chars.append(this.getTextFromGlyph(constituentGlyphId, this.glyphToCharacterMap));
            }
            final Glyph glyph = new Glyph(glyphIdToReplace, this.glyphWidthsByIndex[glyphIdToReplace], chars.toString());
            glyphSubstitutionMap.put(glyph.chars, glyph);
        }
        return Collections.unmodifiableMap((Map<? extends String, ? extends Glyph>)glyphSubstitutionMap);
    }
    
    private String getTextFromGlyph(final int glyphId, final Map<Integer, Character> glyphToCharacterMap) throws FontReadingException {
        final StringBuilder chars = new StringBuilder(1);
        final Character c = glyphToCharacterMap.get(glyphId);
        if (c == null) {
            final List<Integer> constituentGlyphs = this.rawLigatureSubstitutionMap.get(glyphId);
            if (constituentGlyphs == null || constituentGlyphs.isEmpty()) {
                throw new FontReadingException("No corresponding character or simple glyphs found for GlyphID=" + glyphId);
            }
            for (final int constituentGlyphId : constituentGlyphs) {
                chars.append(this.getTextFromGlyph(constituentGlyphId, glyphToCharacterMap));
            }
        }
        else {
            chars.append((char)c);
        }
        return chars.toString();
    }
    
    @Override
    protected void readSubTable(final int lookupType, final int subTableLocation) throws IOException {
        if (lookupType == 1) {
            this.readSingleSubstitutionSubtable(subTableLocation);
        }
        else if (lookupType == 4) {
            this.readLigatureSubstitutionSubtable(subTableLocation);
        }
        else {
            System.err.println("LookupType " + lookupType + " is not yet handled for " + GlyphSubstitutionTableReader.class.getSimpleName());
        }
    }
    
    private void readSingleSubstitutionSubtable(final int subTableLocation) throws IOException {
        this.rf.seek(subTableLocation);
        final int substFormat = this.rf.readShort();
        GlyphSubstitutionTableReader.LOG.debug("substFormat=" + substFormat);
        if (substFormat == 1) {
            final int coverage = this.rf.readShort();
            GlyphSubstitutionTableReader.LOG.debug("coverage=" + coverage);
            final int deltaGlyphID = this.rf.readShort();
            GlyphSubstitutionTableReader.LOG.debug("deltaGlyphID=" + deltaGlyphID);
            final List<Integer> coverageGlyphIds = this.readCoverageFormat(subTableLocation + coverage);
            for (final int coverageGlyphId : coverageGlyphIds) {
                final int substituteGlyphId = coverageGlyphId + deltaGlyphID;
                this.rawLigatureSubstitutionMap.put(substituteGlyphId, Arrays.asList(coverageGlyphId));
            }
        }
        else {
            if (substFormat != 2) {
                throw new IllegalArgumentException("Bad substFormat: " + substFormat);
            }
            final int coverage = this.rf.readShort();
            GlyphSubstitutionTableReader.LOG.debug("coverage=" + coverage);
            final int glyphCount = this.rf.readUnsignedShort();
            final int[] substitute = new int[glyphCount];
            for (int k = 0; k < glyphCount; ++k) {
                substitute[k] = this.rf.readUnsignedShort();
            }
            final List<Integer> coverageGlyphIds2 = this.readCoverageFormat(subTableLocation + coverage);
            for (int i = 0; i < glyphCount; ++i) {
                this.rawLigatureSubstitutionMap.put(substitute[i], Arrays.asList(coverageGlyphIds2.get(i)));
            }
        }
    }
    
    private void readLigatureSubstitutionSubtable(final int ligatureSubstitutionSubtableLocation) throws IOException {
        this.rf.seek(ligatureSubstitutionSubtableLocation);
        final int substFormat = this.rf.readShort();
        GlyphSubstitutionTableReader.LOG.debug("substFormat=" + substFormat);
        if (substFormat != 1) {
            throw new IllegalArgumentException("The expected SubstFormat is 1");
        }
        final int coverage = this.rf.readShort();
        GlyphSubstitutionTableReader.LOG.debug("coverage=" + coverage);
        final int ligSetCount = this.rf.readShort();
        final List<Integer> ligatureOffsets = new ArrayList<Integer>(ligSetCount);
        for (int i = 0; i < ligSetCount; ++i) {
            final int ligatureOffset = this.rf.readShort();
            ligatureOffsets.add(ligatureOffset);
        }
        final List<Integer> coverageGlyphIds = this.readCoverageFormat(ligatureSubstitutionSubtableLocation + coverage);
        if (ligSetCount != coverageGlyphIds.size()) {
            throw new IllegalArgumentException("According to the OpenTypeFont specifications, the coverage count should be equal to the no. of LigatureSetTables");
        }
        for (int j = 0; j < ligSetCount; ++j) {
            final int coverageGlyphId = coverageGlyphIds.get(j);
            final int ligatureOffset2 = ligatureOffsets.get(j);
            GlyphSubstitutionTableReader.LOG.debug("ligatureOffset=" + ligatureOffset2);
            this.readLigatureSetTable(ligatureSubstitutionSubtableLocation + ligatureOffset2, coverageGlyphId);
        }
    }
    
    private void readLigatureSetTable(final int ligatureSetTableLocation, final int coverageGlyphId) throws IOException {
        this.rf.seek(ligatureSetTableLocation);
        final int ligatureCount = this.rf.readShort();
        GlyphSubstitutionTableReader.LOG.debug("ligatureCount=" + ligatureCount);
        final List<Integer> ligatureOffsets = new ArrayList<Integer>(ligatureCount);
        for (int i = 0; i < ligatureCount; ++i) {
            final int ligatureOffset = this.rf.readShort();
            ligatureOffsets.add(ligatureOffset);
        }
        final Iterator i$ = ligatureOffsets.iterator();
        while (i$.hasNext()) {
            final int ligatureOffset = i$.next();
            this.readLigatureTable(ligatureSetTableLocation + ligatureOffset, coverageGlyphId);
        }
    }
    
    private void readLigatureTable(final int ligatureTableLocation, final int coverageGlyphId) throws IOException {
        this.rf.seek(ligatureTableLocation);
        final int ligGlyph = this.rf.readShort();
        GlyphSubstitutionTableReader.LOG.debug("ligGlyph=" + ligGlyph);
        final int compCount = this.rf.readShort();
        final List<Integer> glyphIdList = new ArrayList<Integer>();
        glyphIdList.add(coverageGlyphId);
        for (int i = 0; i < compCount - 1; ++i) {
            final int glyphId = this.rf.readShort();
            glyphIdList.add(glyphId);
        }
        GlyphSubstitutionTableReader.LOG.debug("glyphIdList=" + glyphIdList);
        final List<Integer> previousValue = this.rawLigatureSubstitutionMap.put(ligGlyph, glyphIdList);
        if (previousValue != null) {
            GlyphSubstitutionTableReader.LOG.warn("!!!!!!!!!!glyphId=" + ligGlyph + ",\npreviousValue=" + previousValue + ",\ncurrentVal=" + glyphIdList);
        }
    }
}
