// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.otf;

public class TableHeader
{
    public int version;
    public int scriptListOffset;
    public int featureListOffset;
    public int lookupListOffset;
    
    public TableHeader(final int version, final int scriptListOffset, final int featureListOffset, final int lookupListOffset) {
        this.version = version;
        this.scriptListOffset = scriptListOffset;
        this.featureListOffset = featureListOffset;
        this.lookupListOffset = lookupListOffset;
    }
}
