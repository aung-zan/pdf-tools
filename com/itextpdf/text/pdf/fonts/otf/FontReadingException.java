// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.otf;

public class FontReadingException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public FontReadingException(final String message) {
        super(message);
    }
    
    public FontReadingException(final String message, final Exception e) {
        super(message, e);
    }
}
