// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.otf;

import java.util.Arrays;
import java.util.List;

public enum Language
{
    BENGALI(new String[] { "beng", "bng2" });
    
    private final List<String> codes;
    
    private Language(final String[] codes) {
        this.codes = Arrays.asList(codes);
    }
    
    public boolean isSupported(final String languageCode) {
        return this.codes.contains(languageCode);
    }
}
