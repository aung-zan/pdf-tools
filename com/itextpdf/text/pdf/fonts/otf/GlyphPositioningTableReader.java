// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.otf;

import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;

public class GlyphPositioningTableReader extends OpenTypeFontTableReader
{
    public GlyphPositioningTableReader(final RandomAccessFileOrArray rf, final int gposTableLocation) throws IOException {
        super(rf, gposTableLocation);
    }
    
    public void read() throws FontReadingException {
        this.startReadingTable();
    }
    
    @Override
    protected void readSubTable(final int lookupType, final int subTableLocation) throws IOException {
        if (lookupType == 1) {
            this.readLookUpType_1(subTableLocation);
        }
        else if (lookupType == 4) {
            this.readLookUpType_4(subTableLocation);
        }
        else if (lookupType == 8) {
            this.readLookUpType_8(subTableLocation);
        }
        else {
            System.err.println("The lookupType " + lookupType + " is not yet supported by " + GlyphPositioningTableReader.class.getSimpleName());
        }
    }
    
    private void readLookUpType_1(final int lookupTableLocation) throws IOException {
        this.rf.seek(lookupTableLocation);
        final int posFormat = this.rf.readShort();
        if (posFormat == 1) {
            GlyphPositioningTableReader.LOG.debug("Reading `Look Up Type 1, Format 1` ....");
            final int coverageOffset = this.rf.readShort();
            final int valueFormat = this.rf.readShort();
            if ((valueFormat & 0x1) == 0x1) {
                final int xPlacement = this.rf.readShort();
                GlyphPositioningTableReader.LOG.debug("xPlacement=" + xPlacement);
            }
            if ((valueFormat & 0x2) == 0x2) {
                final int yPlacement = this.rf.readShort();
                GlyphPositioningTableReader.LOG.debug("yPlacement=" + yPlacement);
            }
            final List<Integer> glyphCodes = this.readCoverageFormat(lookupTableLocation + coverageOffset);
            GlyphPositioningTableReader.LOG.debug("glyphCodes=" + glyphCodes);
        }
        else {
            System.err.println("The PosFormat " + posFormat + " for `LookupType 1` is not yet supported by " + GlyphPositioningTableReader.class.getSimpleName());
        }
    }
    
    private void readLookUpType_4(final int lookupTableLocation) throws IOException {
        this.rf.seek(lookupTableLocation);
        final int posFormat = this.rf.readShort();
        if (posFormat == 1) {
            GlyphPositioningTableReader.LOG.debug("Reading `Look Up Type 4, Format 1` ....");
            final int markCoverageOffset = this.rf.readShort();
            final int baseCoverageOffset = this.rf.readShort();
            final int classCount = this.rf.readShort();
            final int markArrayOffset = this.rf.readShort();
            final int baseArrayOffset = this.rf.readShort();
            final List<Integer> markCoverages = this.readCoverageFormat(lookupTableLocation + markCoverageOffset);
            GlyphPositioningTableReader.LOG.debug("markCoverages=" + markCoverages);
            final List<Integer> baseCoverages = this.readCoverageFormat(lookupTableLocation + baseCoverageOffset);
            GlyphPositioningTableReader.LOG.debug("baseCoverages=" + baseCoverages);
            this.readMarkArrayTable(lookupTableLocation + markArrayOffset);
            this.readBaseArrayTable(lookupTableLocation + baseArrayOffset, classCount);
        }
        else {
            System.err.println("The posFormat " + posFormat + " is not supported by " + GlyphPositioningTableReader.class.getSimpleName());
        }
    }
    
    private void readLookUpType_8(final int lookupTableLocation) throws IOException {
        this.rf.seek(lookupTableLocation);
        final int posFormat = this.rf.readShort();
        if (posFormat == 3) {
            GlyphPositioningTableReader.LOG.debug("Reading `Look Up Type 8, Format 3` ....");
            this.readChainingContextPositioningFormat_3(lookupTableLocation);
        }
        else {
            System.err.println("The posFormat " + posFormat + " for `Look Up Type 8` is not supported by " + GlyphPositioningTableReader.class.getSimpleName());
        }
    }
    
    private void readChainingContextPositioningFormat_3(final int lookupTableLocation) throws IOException {
        final int backtrackGlyphCount = this.rf.readShort();
        GlyphPositioningTableReader.LOG.debug("backtrackGlyphCount=" + backtrackGlyphCount);
        final List<Integer> backtrackGlyphOffsets = new ArrayList<Integer>(backtrackGlyphCount);
        for (int i = 0; i < backtrackGlyphCount; ++i) {
            final int backtrackGlyphOffset = this.rf.readShort();
            backtrackGlyphOffsets.add(backtrackGlyphOffset);
        }
        final int inputGlyphCount = this.rf.readShort();
        GlyphPositioningTableReader.LOG.debug("inputGlyphCount=" + inputGlyphCount);
        final List<Integer> inputGlyphOffsets = new ArrayList<Integer>(inputGlyphCount);
        for (int j = 0; j < inputGlyphCount; ++j) {
            final int inputGlyphOffset = this.rf.readShort();
            inputGlyphOffsets.add(inputGlyphOffset);
        }
        final int lookaheadGlyphCount = this.rf.readShort();
        GlyphPositioningTableReader.LOG.debug("lookaheadGlyphCount=" + lookaheadGlyphCount);
        final List<Integer> lookaheadGlyphOffsets = new ArrayList<Integer>(lookaheadGlyphCount);
        for (int k = 0; k < lookaheadGlyphCount; ++k) {
            final int lookaheadGlyphOffset = this.rf.readShort();
            lookaheadGlyphOffsets.add(lookaheadGlyphOffset);
        }
        final int posCount = this.rf.readShort();
        GlyphPositioningTableReader.LOG.debug("posCount=" + posCount);
        final List<PosLookupRecord> posLookupRecords = new ArrayList<PosLookupRecord>(posCount);
        for (int l = 0; l < posCount; ++l) {
            final int sequenceIndex = this.rf.readShort();
            final int lookupListIndex = this.rf.readShort();
            GlyphPositioningTableReader.LOG.debug("sequenceIndex=" + sequenceIndex + ", lookupListIndex=" + lookupListIndex);
            posLookupRecords.add(new PosLookupRecord(sequenceIndex, lookupListIndex));
        }
        for (final int backtrackGlyphOffset2 : backtrackGlyphOffsets) {
            final List<Integer> backtrackGlyphs = this.readCoverageFormat(lookupTableLocation + backtrackGlyphOffset2);
            GlyphPositioningTableReader.LOG.debug("backtrackGlyphs=" + backtrackGlyphs);
        }
        for (final int inputGlyphOffset2 : inputGlyphOffsets) {
            final List<Integer> inputGlyphs = this.readCoverageFormat(lookupTableLocation + inputGlyphOffset2);
            GlyphPositioningTableReader.LOG.debug("inputGlyphs=" + inputGlyphs);
        }
        for (final int lookaheadGlyphOffset2 : lookaheadGlyphOffsets) {
            final List<Integer> lookaheadGlyphs = this.readCoverageFormat(lookupTableLocation + lookaheadGlyphOffset2);
            GlyphPositioningTableReader.LOG.debug("lookaheadGlyphs=" + lookaheadGlyphs);
        }
    }
    
    private void readMarkArrayTable(final int markArrayLocation) throws IOException {
        this.rf.seek(markArrayLocation);
        final int markCount = this.rf.readShort();
        final List<MarkRecord> markRecords = new ArrayList<MarkRecord>();
        for (int i = 0; i < markCount; ++i) {
            markRecords.add(this.readMarkRecord());
        }
        for (final MarkRecord markRecord : markRecords) {
            this.readAnchorTable(markArrayLocation + markRecord.markAnchorOffset);
        }
    }
    
    private MarkRecord readMarkRecord() throws IOException {
        final int markClass = this.rf.readShort();
        final int markAnchorOffset = this.rf.readShort();
        return new MarkRecord(markClass, markAnchorOffset);
    }
    
    private void readAnchorTable(final int anchorTableLocation) throws IOException {
        this.rf.seek(anchorTableLocation);
        final int anchorFormat = this.rf.readShort();
        if (anchorFormat != 1) {
            System.err.println("The extra features of the AnchorFormat " + anchorFormat + " will not be used");
        }
        final int x = this.rf.readShort();
        final int y = this.rf.readShort();
    }
    
    private void readBaseArrayTable(final int baseArrayTableLocation, final int classCount) throws IOException {
        this.rf.seek(baseArrayTableLocation);
        final int baseCount = this.rf.readShort();
        final Set<Integer> baseAnchors = new HashSet<Integer>();
        for (int i = 0; i < baseCount; ++i) {
            for (int k = 0; k < classCount; ++k) {
                final int baseAnchor = this.rf.readShort();
                baseAnchors.add(baseAnchor);
            }
        }
        for (final int baseAnchor2 : baseAnchors) {
            this.readAnchorTable(baseArrayTableLocation + baseAnchor2);
        }
    }
    
    static class MarkRecord
    {
        final int markClass;
        final int markAnchorOffset;
        
        public MarkRecord(final int markClass, final int markAnchorOffset) {
            this.markClass = markClass;
            this.markAnchorOffset = markAnchorOffset;
        }
    }
    
    static class PosLookupRecord
    {
        final int sequenceIndex;
        final int lookupListIndex;
        
        public PosLookupRecord(final int sequenceIndex, final int lookupListIndex) {
            this.sequenceIndex = sequenceIndex;
            this.lookupListIndex = lookupListIndex;
        }
    }
}
