// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.otf;

import com.itextpdf.text.log.LoggerFactory;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.IOException;
import java.util.List;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.log.Logger;

public abstract class OpenTypeFontTableReader
{
    protected static final Logger LOG;
    protected final RandomAccessFileOrArray rf;
    protected final int tableLocation;
    private List<String> supportedLanguages;
    
    public OpenTypeFontTableReader(final RandomAccessFileOrArray rf, final int tableLocation) throws IOException {
        this.rf = rf;
        this.tableLocation = tableLocation;
    }
    
    public Language getSupportedLanguage() throws FontReadingException {
        final Language[] allLangs = Language.values();
        for (final String supportedLang : this.supportedLanguages) {
            for (final Language lang : allLangs) {
                if (lang.isSupported(supportedLang)) {
                    return lang;
                }
            }
        }
        throw new FontReadingException("Unsupported languages " + this.supportedLanguages);
    }
    
    protected final void startReadingTable() throws FontReadingException {
        try {
            final TableHeader header = this.readHeader();
            this.readScriptListTable(this.tableLocation + header.scriptListOffset);
            this.readFeatureListTable(this.tableLocation + header.featureListOffset);
            this.readLookupListTable(this.tableLocation + header.lookupListOffset);
        }
        catch (IOException e) {
            throw new FontReadingException("Error reading font file", e);
        }
    }
    
    protected abstract void readSubTable(final int p0, final int p1) throws IOException;
    
    private void readLookupListTable(final int lookupListTableLocation) throws IOException {
        this.rf.seek(lookupListTableLocation);
        final int lookupCount = this.rf.readShort();
        final List<Integer> lookupTableOffsets = new ArrayList<Integer>();
        for (int i = 0; i < lookupCount; ++i) {
            final int lookupTableOffset = this.rf.readShort();
            lookupTableOffsets.add(lookupTableOffset);
        }
        for (int i = 0; i < lookupCount; ++i) {
            final int lookupTableOffset = lookupTableOffsets.get(i);
            this.readLookupTable(lookupListTableLocation + lookupTableOffset);
        }
    }
    
    private void readLookupTable(final int lookupTableLocation) throws IOException {
        this.rf.seek(lookupTableLocation);
        final int lookupType = this.rf.readShort();
        this.rf.skipBytes(2);
        final int subTableCount = this.rf.readShort();
        final List<Integer> subTableOffsets = new ArrayList<Integer>();
        for (int i = 0; i < subTableCount; ++i) {
            final int subTableOffset = this.rf.readShort();
            subTableOffsets.add(subTableOffset);
        }
        final Iterator i$ = subTableOffsets.iterator();
        while (i$.hasNext()) {
            final int subTableOffset = i$.next();
            this.readSubTable(lookupType, lookupTableLocation + subTableOffset);
        }
    }
    
    protected final List<Integer> readCoverageFormat(final int coverageLocation) throws IOException {
        this.rf.seek(coverageLocation);
        final int coverageFormat = this.rf.readShort();
        List<Integer> glyphIds;
        if (coverageFormat == 1) {
            final int glyphCount = this.rf.readShort();
            glyphIds = new ArrayList<Integer>(glyphCount);
            for (int i = 0; i < glyphCount; ++i) {
                final int coverageGlyphId = this.rf.readShort();
                glyphIds.add(coverageGlyphId);
            }
        }
        else {
            if (coverageFormat != 2) {
                throw new UnsupportedOperationException("Invalid coverage format: " + coverageFormat);
            }
            final int rangeCount = this.rf.readShort();
            glyphIds = new ArrayList<Integer>();
            for (int i = 0; i < rangeCount; ++i) {
                this.readRangeRecord(glyphIds);
            }
        }
        return Collections.unmodifiableList((List<? extends Integer>)glyphIds);
    }
    
    private void readRangeRecord(final List<Integer> glyphIds) throws IOException {
        final int startGlyphId = this.rf.readShort();
        final int endGlyphId = this.rf.readShort();
        final int startCoverageIndex = this.rf.readShort();
        for (int glyphId = startGlyphId; glyphId <= endGlyphId; ++glyphId) {
            glyphIds.add(glyphId);
        }
    }
    
    private void readScriptListTable(final int scriptListTableLocationOffset) throws IOException {
        this.rf.seek(scriptListTableLocationOffset);
        final int scriptCount = this.rf.readShort();
        final Map<String, Integer> scriptRecords = new HashMap<String, Integer>(scriptCount);
        for (int i = 0; i < scriptCount; ++i) {
            this.readScriptRecord(scriptListTableLocationOffset, scriptRecords);
        }
        final List<String> supportedLanguages = new ArrayList<String>(scriptCount);
        for (final String scriptName : scriptRecords.keySet()) {
            this.readScriptTable(scriptRecords.get(scriptName));
            supportedLanguages.add(scriptName);
        }
        this.supportedLanguages = Collections.unmodifiableList((List<? extends String>)supportedLanguages);
    }
    
    private void readScriptRecord(final int scriptListTableLocationOffset, final Map<String, Integer> scriptRecords) throws IOException {
        final String scriptTag = this.rf.readString(4, "utf-8");
        final int scriptOffset = this.rf.readShort();
        scriptRecords.put(scriptTag, scriptListTableLocationOffset + scriptOffset);
    }
    
    private void readScriptTable(final int scriptTableLocationOffset) throws IOException {
        this.rf.seek(scriptTableLocationOffset);
        final int defaultLangSys = this.rf.readShort();
        final int langSysCount = this.rf.readShort();
        if (langSysCount > 0) {
            final Map<String, Integer> langSysRecords = new LinkedHashMap<String, Integer>(langSysCount);
            for (int i = 0; i < langSysCount; ++i) {
                this.readLangSysRecord(langSysRecords);
            }
            for (final String langSysTag : langSysRecords.keySet()) {
                this.readLangSysTable(scriptTableLocationOffset + langSysRecords.get(langSysTag));
            }
        }
        this.readLangSysTable(scriptTableLocationOffset + defaultLangSys);
    }
    
    private void readLangSysRecord(final Map<String, Integer> langSysRecords) throws IOException {
        final String langSysTag = this.rf.readString(4, "utf-8");
        final int langSys = this.rf.readShort();
        langSysRecords.put(langSysTag, langSys);
    }
    
    private void readLangSysTable(final int langSysTableLocationOffset) throws IOException {
        this.rf.seek(langSysTableLocationOffset);
        final int lookupOrderOffset = this.rf.readShort();
        OpenTypeFontTableReader.LOG.debug("lookupOrderOffset=" + lookupOrderOffset);
        final int reqFeatureIndex = this.rf.readShort();
        OpenTypeFontTableReader.LOG.debug("reqFeatureIndex=" + reqFeatureIndex);
        final int featureCount = this.rf.readShort();
        final List<Short> featureListIndices = new ArrayList<Short>(featureCount);
        for (int i = 0; i < featureCount; ++i) {
            featureListIndices.add(this.rf.readShort());
        }
        OpenTypeFontTableReader.LOG.debug("featureListIndices=" + featureListIndices);
    }
    
    private void readFeatureListTable(final int featureListTableLocationOffset) throws IOException {
        this.rf.seek(featureListTableLocationOffset);
        final int featureCount = this.rf.readShort();
        OpenTypeFontTableReader.LOG.debug("featureCount=" + featureCount);
        final Map<String, Short> featureRecords = new LinkedHashMap<String, Short>(featureCount);
        for (int i = 0; i < featureCount; ++i) {
            featureRecords.put(this.rf.readString(4, "utf-8"), this.rf.readShort());
        }
        for (final String featureName : featureRecords.keySet()) {
            OpenTypeFontTableReader.LOG.debug("*************featureName=" + featureName);
            this.readFeatureTable(featureListTableLocationOffset + featureRecords.get(featureName));
        }
    }
    
    private void readFeatureTable(final int featureTableLocationOffset) throws IOException {
        this.rf.seek(featureTableLocationOffset);
        final int featureParamsOffset = this.rf.readShort();
        OpenTypeFontTableReader.LOG.debug("featureParamsOffset=" + featureParamsOffset);
        final int lookupCount = this.rf.readShort();
        OpenTypeFontTableReader.LOG.debug("lookupCount=" + lookupCount);
        final List<Short> lookupListIndices = new ArrayList<Short>(lookupCount);
        for (int i = 0; i < lookupCount; ++i) {
            lookupListIndices.add(this.rf.readShort());
        }
    }
    
    private TableHeader readHeader() throws IOException {
        this.rf.seek(this.tableLocation);
        final int version = this.rf.readInt();
        final int scriptListOffset = this.rf.readUnsignedShort();
        final int featureListOffset = this.rf.readUnsignedShort();
        final int lookupListOffset = this.rf.readUnsignedShort();
        final TableHeader header = new TableHeader(version, scriptListOffset, featureListOffset, lookupListOffset);
        return header;
    }
    
    static {
        LOG = LoggerFactory.getLogger(OpenTypeFontTableReader.class);
    }
}
