// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.cmaps;

import java.io.IOException;
import com.itextpdf.text.pdf.PRTokeniser;

public interface CidLocation
{
    PRTokeniser getLocation(final String p0) throws IOException;
}
