// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.cmaps;

import com.itextpdf.text.Utilities;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.IntHashtable;

public class CMapUniCid extends AbstractCMap
{
    private IntHashtable map;
    
    public CMapUniCid() {
        this.map = new IntHashtable(65537);
    }
    
    @Override
    void addChar(final PdfString mark, final PdfObject code) {
        if (!(code instanceof PdfNumber)) {
            return;
        }
        final String s = this.decodeStringToUnicode(mark);
        int codepoint;
        if (Utilities.isSurrogatePair(s, 0)) {
            codepoint = Utilities.convertToUtf32(s, 0);
        }
        else {
            codepoint = s.charAt(0);
        }
        this.map.put(codepoint, ((PdfNumber)code).intValue());
    }
    
    public int lookup(final int character) {
        return this.map.get(character);
    }
    
    public CMapToUnicode exportToUnicode() {
        final CMapToUnicode uni = new CMapToUnicode();
        final int[] arr$;
        final int[] keys = arr$ = this.map.toOrderedKeys();
        for (final int key : arr$) {
            uni.addChar(this.map.get(key), Utilities.convertFromUtf32(key));
        }
        return uni;
    }
}
