// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.cmaps;

import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.pdf.PRTokeniser;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfContentParser;
import com.itextpdf.text.pdf.PdfObject;
import java.util.ArrayList;
import java.io.IOException;
import com.itextpdf.text.pdf.PdfName;

public class CMapParserEx
{
    private static final PdfName CMAPNAME;
    private static final String DEF = "def";
    private static final String ENDCIDRANGE = "endcidrange";
    private static final String ENDCIDCHAR = "endcidchar";
    private static final String ENDBFRANGE = "endbfrange";
    private static final String ENDBFCHAR = "endbfchar";
    private static final String USECMAP = "usecmap";
    private static final int MAXLEVEL = 10;
    
    public static void parseCid(final String cmapName, final AbstractCMap cmap, final CidLocation location) throws IOException {
        parseCid(cmapName, cmap, location, 0);
    }
    
    private static void parseCid(final String cmapName, final AbstractCMap cmap, final CidLocation location, final int level) throws IOException {
        if (level >= 10) {
            return;
        }
        final PRTokeniser inp = location.getLocation(cmapName);
        try {
            final ArrayList<PdfObject> list = new ArrayList<PdfObject>();
            final PdfContentParser cp = new PdfContentParser(inp);
            int maxExc = 50;
            while (true) {
                try {
                    cp.parse(list);
                }
                catch (Exception ex) {
                    if (--maxExc < 0) {
                        break;
                    }
                    continue;
                }
                if (list.isEmpty()) {
                    break;
                }
                final String last = list.get(list.size() - 1).toString();
                if (level == 0 && list.size() == 3 && last.equals("def")) {
                    final PdfObject key = list.get(0);
                    if (PdfName.REGISTRY.equals(key)) {
                        cmap.setRegistry(list.get(1).toString());
                    }
                    else if (PdfName.ORDERING.equals(key)) {
                        cmap.setOrdering(list.get(1).toString());
                    }
                    else if (CMapParserEx.CMAPNAME.equals(key)) {
                        cmap.setName(list.get(1).toString());
                    }
                    else {
                        if (!PdfName.SUPPLEMENT.equals(key)) {
                            continue;
                        }
                        try {
                            cmap.setSupplement(list.get(1).intValue());
                        }
                        catch (Exception ex2) {}
                    }
                }
                else if ((last.equals("endcidchar") || last.equals("endbfchar")) && list.size() >= 3) {
                    for (int lmax = list.size() - 2, k = 0; k < lmax; k += 2) {
                        if (list.get(k) instanceof PdfString) {
                            cmap.addChar(list.get(k), list.get(k + 1));
                        }
                    }
                }
                else if ((last.equals("endcidrange") || last.equals("endbfrange")) && list.size() >= 4) {
                    for (int lmax = list.size() - 3, k = 0; k < lmax; k += 3) {
                        if (list.get(k) instanceof PdfString && list.get(k + 1) instanceof PdfString) {
                            cmap.addRange(list.get(k), list.get(k + 1), list.get(k + 2));
                        }
                    }
                }
                else {
                    if (!last.equals("usecmap") || list.size() != 2 || !(list.get(0) instanceof PdfName)) {
                        continue;
                    }
                    parseCid(PdfName.decodeName(list.get(0).toString()), cmap, location, level + 1);
                }
            }
        }
        finally {
            inp.close();
        }
    }
    
    private static void encodeSequence(int size, final byte[] seqs, final char cid, final ArrayList<char[]> planes) {
        --size;
        int nextPlane = 0;
        for (int idx = 0; idx < size; ++idx) {
            final char[] plane = planes.get(nextPlane);
            final int one = seqs[idx] & 0xFF;
            char c = plane[one];
            if (c != '\0' && (c & '\u8000') == 0x0) {
                throw new RuntimeException(MessageLocalization.getComposedMessage("inconsistent.mapping", new Object[0]));
            }
            if (c == '\0') {
                planes.add(new char[256]);
                c = (char)(planes.size() - 1 | 0x8000);
                plane[one] = c;
            }
            nextPlane = (c & '\u7fff');
        }
        final char[] plane2 = planes.get(nextPlane);
        final int one2 = seqs[size] & 0xFF;
        final char c2 = plane2[one2];
        if ((c2 & '\u8000') != 0x0) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("inconsistent.mapping", new Object[0]));
        }
        plane2[one2] = cid;
    }
    
    static {
        CMAPNAME = new PdfName("CMapName");
    }
}
