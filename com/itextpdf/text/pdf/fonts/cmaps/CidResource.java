// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.cmaps;

import java.io.InputStream;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import java.io.IOException;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.io.StreamUtil;
import com.itextpdf.text.pdf.PRTokeniser;

public class CidResource implements CidLocation
{
    @Override
    public PRTokeniser getLocation(final String location) throws IOException {
        final String fullName = "com/itextpdf/text/pdf/fonts/cmaps/" + location;
        final InputStream inp = StreamUtil.getResourceStream(fullName);
        if (inp == null) {
            throw new IOException(MessageLocalization.getComposedMessage("the.cmap.1.was.not.found", fullName));
        }
        return new PRTokeniser(new RandomAccessFileOrArray(new RandomAccessSourceFactory().createSource(inp)));
    }
}
