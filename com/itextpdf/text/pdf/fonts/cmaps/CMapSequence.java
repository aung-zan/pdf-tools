// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.cmaps;

public class CMapSequence
{
    public byte[] seq;
    public int off;
    public int len;
    
    public CMapSequence() {
    }
    
    public CMapSequence(final byte[] seq, final int off, final int len) {
        this.seq = seq;
        this.off = off;
        this.len = len;
    }
}
