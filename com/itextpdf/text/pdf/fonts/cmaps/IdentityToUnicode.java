// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.cmaps;

import java.io.IOException;

public class IdentityToUnicode
{
    private static CMapToUnicode identityCNS;
    private static CMapToUnicode identityJapan;
    private static CMapToUnicode identityKorea;
    private static CMapToUnicode identityGB;
    private static CMapToUnicode identityH;
    
    public static CMapToUnicode GetMapFromOrdering(final String ordering) throws IOException {
        if (ordering.equals("CNS1")) {
            if (IdentityToUnicode.identityCNS == null) {
                final CMapUniCid uni = CMapCache.getCachedCMapUniCid("UniCNS-UTF16-H");
                if (uni == null) {
                    return null;
                }
                IdentityToUnicode.identityCNS = uni.exportToUnicode();
            }
            return IdentityToUnicode.identityCNS;
        }
        if (ordering.equals("Japan1")) {
            if (IdentityToUnicode.identityJapan == null) {
                final CMapUniCid uni = CMapCache.getCachedCMapUniCid("UniJIS-UTF16-H");
                if (uni == null) {
                    return null;
                }
                IdentityToUnicode.identityJapan = uni.exportToUnicode();
            }
            return IdentityToUnicode.identityJapan;
        }
        if (ordering.equals("Korea1")) {
            if (IdentityToUnicode.identityKorea == null) {
                final CMapUniCid uni = CMapCache.getCachedCMapUniCid("UniKS-UTF16-H");
                if (uni == null) {
                    return null;
                }
                IdentityToUnicode.identityKorea = uni.exportToUnicode();
            }
            return IdentityToUnicode.identityKorea;
        }
        if (ordering.equals("GB1")) {
            if (IdentityToUnicode.identityGB == null) {
                final CMapUniCid uni = CMapCache.getCachedCMapUniCid("UniGB-UTF16-H");
                if (uni == null) {
                    return null;
                }
                IdentityToUnicode.identityGB = uni.exportToUnicode();
            }
            return IdentityToUnicode.identityGB;
        }
        if (ordering.equals("Identity")) {
            if (IdentityToUnicode.identityH == null) {
                IdentityToUnicode.identityH = CMapToUnicode.getIdentity();
            }
            return IdentityToUnicode.identityH;
        }
        return null;
    }
}
