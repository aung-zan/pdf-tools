// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.cmaps;

import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfString;
import java.util.HashMap;

public class CMapCidByte extends AbstractCMap
{
    private HashMap<Integer, byte[]> map;
    private final byte[] EMPTY;
    
    public CMapCidByte() {
        this.map = new HashMap<Integer, byte[]>();
        this.EMPTY = new byte[0];
    }
    
    @Override
    void addChar(final PdfString mark, final PdfObject code) {
        if (!(code instanceof PdfNumber)) {
            return;
        }
        final byte[] ser = AbstractCMap.decodeStringToByte(mark);
        this.map.put(((PdfNumber)code).intValue(), ser);
    }
    
    public byte[] lookup(final int cid) {
        final byte[] ser = this.map.get(cid);
        if (ser == null) {
            return this.EMPTY;
        }
        return ser;
    }
}
