// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.cmaps;

import java.io.IOException;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import com.itextpdf.text.pdf.PRTokeniser;

public class CidLocationFromByte implements CidLocation
{
    private byte[] data;
    
    public CidLocationFromByte(final byte[] data) {
        this.data = data;
    }
    
    @Override
    public PRTokeniser getLocation(final String location) throws IOException {
        return new PRTokeniser(new RandomAccessFileOrArray(new RandomAccessSourceFactory().createSource(this.data)));
    }
}
