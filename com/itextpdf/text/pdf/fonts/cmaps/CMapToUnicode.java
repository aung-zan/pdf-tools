// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.cmaps;

import com.itextpdf.text.Utilities;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfString;
import java.io.IOException;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;

public class CMapToUnicode extends AbstractCMap
{
    private Map<Integer, String> singleByteMappings;
    private Map<Integer, String> doubleByteMappings;
    
    public CMapToUnicode() {
        this.singleByteMappings = new HashMap<Integer, String>();
        this.doubleByteMappings = new HashMap<Integer, String>();
    }
    
    public boolean hasOneByteMappings() {
        return !this.singleByteMappings.isEmpty();
    }
    
    public boolean hasTwoByteMappings() {
        return !this.doubleByteMappings.isEmpty();
    }
    
    public String lookup(final byte[] code, final int offset, final int length) {
        String result = null;
        Integer key = null;
        if (length == 1) {
            key = (code[offset] & 0xFF);
            result = this.singleByteMappings.get(key);
        }
        else if (length == 2) {
            int intKey = code[offset] & 0xFF;
            intKey <<= 8;
            intKey += (code[offset + 1] & 0xFF);
            key = intKey;
            result = this.doubleByteMappings.get(key);
        }
        return result;
    }
    
    public Map<Integer, Integer> createReverseMapping() throws IOException {
        final Map<Integer, Integer> result = new HashMap<Integer, Integer>();
        for (final Map.Entry<Integer, String> entry : this.singleByteMappings.entrySet()) {
            result.put(this.convertToInt(entry.getValue()), entry.getKey());
        }
        for (final Map.Entry<Integer, String> entry : this.doubleByteMappings.entrySet()) {
            result.put(this.convertToInt(entry.getValue()), entry.getKey());
        }
        return result;
    }
    
    public Map<Integer, Integer> createDirectMapping() throws IOException {
        final Map<Integer, Integer> result = new HashMap<Integer, Integer>();
        for (final Map.Entry<Integer, String> entry : this.singleByteMappings.entrySet()) {
            result.put(entry.getKey(), this.convertToInt(entry.getValue()));
        }
        for (final Map.Entry<Integer, String> entry : this.doubleByteMappings.entrySet()) {
            result.put(entry.getKey(), this.convertToInt(entry.getValue()));
        }
        return result;
    }
    
    private int convertToInt(final String s) throws IOException {
        final byte[] b = s.getBytes("UTF-16BE");
        int value = 0;
        for (int i = 0; i < b.length - 1; ++i) {
            value += (b[i] & 0xFF);
            value <<= 8;
        }
        value += (b[b.length - 1] & 0xFF);
        return value;
    }
    
    void addChar(final int cid, final String uni) {
        this.doubleByteMappings.put(cid, uni);
    }
    
    @Override
    void addChar(final PdfString mark, final PdfObject code) {
        try {
            final byte[] src = mark.getBytes();
            final String dest = this.createStringFromBytes(code.getBytes());
            if (src.length == 1) {
                this.singleByteMappings.put(src[0] & 0xFF, dest);
            }
            else {
                if (src.length != 2) {
                    throw new IOException(MessageLocalization.getComposedMessage("mapping.code.should.be.1.or.two.bytes.and.not.1", src.length));
                }
                int intSrc = src[0] & 0xFF;
                intSrc <<= 8;
                intSrc |= (src[1] & 0xFF);
                this.doubleByteMappings.put(intSrc, dest);
            }
        }
        catch (Exception ex) {
            throw new ExceptionConverter(ex);
        }
    }
    
    private String createStringFromBytes(final byte[] bytes) throws IOException {
        String retval = null;
        if (bytes.length == 1) {
            retval = new String(bytes);
        }
        else {
            retval = new String(bytes, "UTF-16BE");
        }
        return retval;
    }
    
    public static CMapToUnicode getIdentity() {
        final CMapToUnicode uni = new CMapToUnicode();
        for (int i = 0; i < 65537; ++i) {
            uni.addChar(i, Utilities.convertFromUtf32(i));
        }
        return uni;
    }
}
