// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.cmaps;

import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfString;
import java.util.ArrayList;

public class CMapByteCid extends AbstractCMap
{
    private ArrayList<char[]> planes;
    
    public CMapByteCid() {
        (this.planes = new ArrayList<char[]>()).add(new char[256]);
    }
    
    @Override
    void addChar(final PdfString mark, final PdfObject code) {
        if (!(code instanceof PdfNumber)) {
            return;
        }
        this.encodeSequence(AbstractCMap.decodeStringToByte(mark), (char)((PdfNumber)code).intValue());
    }
    
    private void encodeSequence(final byte[] seqs, final char cid) {
        final int size = seqs.length - 1;
        int nextPlane = 0;
        for (int idx = 0; idx < size; ++idx) {
            final char[] plane = this.planes.get(nextPlane);
            final int one = seqs[idx] & 0xFF;
            char c = plane[one];
            if (c != '\0' && (c & '\u8000') == 0x0) {
                throw new RuntimeException(MessageLocalization.getComposedMessage("inconsistent.mapping", new Object[0]));
            }
            if (c == '\0') {
                this.planes.add(new char[256]);
                c = (char)(this.planes.size() - 1 | 0x8000);
                plane[one] = c;
            }
            nextPlane = (c & '\u7fff');
        }
        final char[] plane2 = this.planes.get(nextPlane);
        final int one2 = seqs[size] & 0xFF;
        final char c2 = plane2[one2];
        if ((c2 & '\u8000') != 0x0) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("inconsistent.mapping", new Object[0]));
        }
        plane2[one2] = cid;
    }
    
    public int decodeSingle(final CMapSequence seq) {
        final int end = seq.off + seq.len;
        int currentPlane = 0;
        while (seq.off < end) {
            final int one = seq.seq[seq.off++] & 0xFF;
            --seq.len;
            final char[] plane = this.planes.get(currentPlane);
            final int cid = plane[one];
            if ((cid & 0x8000) == 0x0) {
                return cid;
            }
            currentPlane = (cid & 0x7FFF);
        }
        return -1;
    }
    
    public String decodeSequence(final CMapSequence seq) {
        final StringBuilder sb = new StringBuilder();
        int cid = 0;
        while ((cid = this.decodeSingle(seq)) >= 0) {
            sb.append((char)cid);
        }
        return sb.toString();
    }
}
