// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.cmaps;

import com.itextpdf.text.pdf.PdfEncodings;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfString;

public abstract class AbstractCMap
{
    private String cmapName;
    private String registry;
    private String ordering;
    private int supplement;
    
    public String getName() {
        return this.cmapName;
    }
    
    void setName(final String cmapName) {
        this.cmapName = cmapName;
    }
    
    public String getOrdering() {
        return this.ordering;
    }
    
    void setOrdering(final String ordering) {
        this.ordering = ordering;
    }
    
    public String getRegistry() {
        return this.registry;
    }
    
    void setRegistry(final String registry) {
        this.registry = registry;
    }
    
    public int getSupplement() {
        return this.supplement;
    }
    
    void setSupplement(final int supplement) {
        this.supplement = supplement;
    }
    
    abstract void addChar(final PdfString p0, final PdfObject p1);
    
    void addRange(final PdfString from, final PdfString to, final PdfObject code) {
        final byte[] a1 = decodeStringToByte(from);
        final byte[] a2 = decodeStringToByte(to);
        if (a1.length != a2.length || a1.length == 0) {
            throw new IllegalArgumentException("Invalid map.");
        }
        byte[] sout = null;
        if (code instanceof PdfString) {
            sout = decodeStringToByte((PdfString)code);
        }
        final int start = byteArrayToInt(a1);
        for (int end = byteArrayToInt(a2), k = start; k <= end; ++k) {
            intToByteArray(k, a1);
            final PdfString s = new PdfString(a1);
            s.setHexWriting(true);
            if (code instanceof PdfArray) {
                this.addChar(s, ((PdfArray)code).getPdfObject(k - start));
            }
            else if (code instanceof PdfNumber) {
                final int nn = ((PdfNumber)code).intValue() + k - start;
                this.addChar(s, new PdfNumber(nn));
            }
            else if (code instanceof PdfString) {
                final PdfString s2 = new PdfString(sout);
                s2.setHexWriting(true);
                final byte[] array = sout;
                final int n = sout.length - 1;
                ++array[n];
                this.addChar(s, s2);
            }
        }
    }
    
    private static void intToByteArray(int v, final byte[] b) {
        for (int k = b.length - 1; k >= 0; --k) {
            b[k] = (byte)v;
            v >>>= 8;
        }
    }
    
    private static int byteArrayToInt(final byte[] b) {
        int v = 0;
        for (int k = 0; k < b.length; ++k) {
            v <<= 8;
            v |= (b[k] & 0xFF);
        }
        return v;
    }
    
    public static byte[] decodeStringToByte(final PdfString s) {
        final byte[] b = s.getBytes();
        final byte[] br = new byte[b.length];
        System.arraycopy(b, 0, br, 0, b.length);
        return br;
    }
    
    public String decodeStringToUnicode(final PdfString ps) {
        if (ps.isHexWriting()) {
            return PdfEncodings.convertToString(ps.getBytes(), "UnicodeBigUnmarked");
        }
        return ps.toUnicodeString();
    }
}
