// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.fonts.cmaps;

import java.io.IOException;
import java.util.HashMap;

public class CMapCache
{
    private static final HashMap<String, CMapUniCid> cacheUniCid;
    private static final HashMap<String, CMapCidUni> cacheCidUni;
    private static final HashMap<String, CMapCidByte> cacheCidByte;
    private static final HashMap<String, CMapByteCid> cacheByteCid;
    
    public static CMapUniCid getCachedCMapUniCid(final String name) throws IOException {
        CMapUniCid cmap = null;
        synchronized (CMapCache.cacheUniCid) {
            cmap = CMapCache.cacheUniCid.get(name);
        }
        if (cmap == null) {
            cmap = new CMapUniCid();
            CMapParserEx.parseCid(name, cmap, new CidResource());
            synchronized (CMapCache.cacheUniCid) {
                CMapCache.cacheUniCid.put(name, cmap);
            }
        }
        return cmap;
    }
    
    public static CMapCidUni getCachedCMapCidUni(final String name) throws IOException {
        CMapCidUni cmap = null;
        synchronized (CMapCache.cacheCidUni) {
            cmap = CMapCache.cacheCidUni.get(name);
        }
        if (cmap == null) {
            cmap = new CMapCidUni();
            CMapParserEx.parseCid(name, cmap, new CidResource());
            synchronized (CMapCache.cacheCidUni) {
                CMapCache.cacheCidUni.put(name, cmap);
            }
        }
        return cmap;
    }
    
    public static CMapCidByte getCachedCMapCidByte(final String name) throws IOException {
        CMapCidByte cmap = null;
        synchronized (CMapCache.cacheCidByte) {
            cmap = CMapCache.cacheCidByte.get(name);
        }
        if (cmap == null) {
            cmap = new CMapCidByte();
            CMapParserEx.parseCid(name, cmap, new CidResource());
            synchronized (CMapCache.cacheCidByte) {
                CMapCache.cacheCidByte.put(name, cmap);
            }
        }
        return cmap;
    }
    
    public static CMapByteCid getCachedCMapByteCid(final String name) throws IOException {
        CMapByteCid cmap = null;
        synchronized (CMapCache.cacheByteCid) {
            cmap = CMapCache.cacheByteCid.get(name);
        }
        if (cmap == null) {
            cmap = new CMapByteCid();
            CMapParserEx.parseCid(name, cmap, new CidResource());
            synchronized (CMapCache.cacheByteCid) {
                CMapCache.cacheByteCid.put(name, cmap);
            }
        }
        return cmap;
    }
    
    static {
        cacheUniCid = new HashMap<String, CMapUniCid>();
        cacheCidUni = new HashMap<String, CMapCidUni>();
        cacheCidByte = new HashMap<String, CMapCidByte>();
        cacheByteCid = new HashMap<String, CMapByteCid>();
    }
}
