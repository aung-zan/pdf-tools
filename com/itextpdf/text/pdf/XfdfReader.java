// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.ArrayList;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.util.Map;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import com.itextpdf.text.xml.simpleparser.SimpleXMLParser;
import java.io.FileInputStream;
import java.util.List;
import java.util.HashMap;
import java.util.Stack;
import com.itextpdf.text.xml.simpleparser.SimpleXMLDocHandler;

public class XfdfReader implements SimpleXMLDocHandler
{
    private boolean foundRoot;
    private final Stack<String> fieldNames;
    private final Stack<String> fieldValues;
    HashMap<String, String> fields;
    protected HashMap<String, List<String>> listFields;
    String fileSpec;
    
    public XfdfReader(final String filename) throws IOException {
        this.foundRoot = false;
        this.fieldNames = new Stack<String>();
        this.fieldValues = new Stack<String>();
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(filename);
            SimpleXMLParser.parse(this, fin);
        }
        finally {
            try {
                if (fin != null) {
                    fin.close();
                }
            }
            catch (Exception ex) {}
        }
    }
    
    public XfdfReader(final byte[] xfdfIn) throws IOException {
        this(new ByteArrayInputStream(xfdfIn));
    }
    
    public XfdfReader(final InputStream is) throws IOException {
        this.foundRoot = false;
        this.fieldNames = new Stack<String>();
        this.fieldValues = new Stack<String>();
        SimpleXMLParser.parse(this, is);
    }
    
    public HashMap<String, String> getFields() {
        return this.fields;
    }
    
    public String getField(final String name) {
        return this.fields.get(name);
    }
    
    public String getFieldValue(final String name) {
        final String field = this.fields.get(name);
        if (field == null) {
            return null;
        }
        return field;
    }
    
    public List<String> getListValues(final String name) {
        return this.listFields.get(name);
    }
    
    public String getFileSpec() {
        return this.fileSpec;
    }
    
    @Override
    public void startElement(final String tag, final Map<String, String> h) {
        if (!this.foundRoot) {
            if (!tag.equals("xfdf")) {
                throw new RuntimeException(MessageLocalization.getComposedMessage("root.element.is.not.xfdf.1", tag));
            }
            this.foundRoot = true;
        }
        if (!tag.equals("xfdf")) {
            if (tag.equals("f")) {
                this.fileSpec = h.get("href");
            }
            else if (tag.equals("fields")) {
                this.fields = new HashMap<String, String>();
                this.listFields = new HashMap<String, List<String>>();
            }
            else if (tag.equals("field")) {
                final String fName = h.get("name");
                this.fieldNames.push(fName);
            }
            else if (tag.equals("value")) {
                this.fieldValues.push("");
            }
        }
    }
    
    @Override
    public void endElement(final String tag) {
        if (tag.equals("value")) {
            String fName = "";
            for (int k = 0; k < this.fieldNames.size(); ++k) {
                fName = fName + "." + this.fieldNames.elementAt(k);
            }
            if (fName.startsWith(".")) {
                fName = fName.substring(1);
            }
            final String fVal = this.fieldValues.pop();
            final String old = this.fields.put(fName, fVal);
            if (old != null) {
                List<String> l = this.listFields.get(fName);
                if (l == null) {
                    l = new ArrayList<String>();
                    l.add(old);
                }
                l.add(fVal);
                this.listFields.put(fName, l);
            }
        }
        else if (tag.equals("field") && !this.fieldNames.isEmpty()) {
            this.fieldNames.pop();
        }
    }
    
    @Override
    public void startDocument() {
        this.fileSpec = "";
    }
    
    @Override
    public void endDocument() {
    }
    
    @Override
    public void text(final String str) {
        if (this.fieldNames.isEmpty() || this.fieldValues.isEmpty()) {
            return;
        }
        String val = this.fieldValues.pop();
        val += str;
        this.fieldValues.push(val);
    }
}
