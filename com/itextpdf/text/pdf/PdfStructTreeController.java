// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.Map;
import java.util.Iterator;
import java.io.IOException;
import com.itextpdf.text.error_messages.MessageLocalization;

public class PdfStructTreeController
{
    private PdfDictionary structTreeRoot;
    private PdfCopy writer;
    private PdfStructureTreeRoot structureTreeRoot;
    private PdfDictionary parentTree;
    protected PdfReader reader;
    private PdfDictionary roleMap;
    private PdfDictionary sourceRoleMap;
    private PdfDictionary sourceClassMap;
    private PdfIndirectReference nullReference;
    
    protected PdfStructTreeController(final PdfReader reader, final PdfCopy writer) throws BadPdfFormatException {
        this.roleMap = null;
        this.sourceRoleMap = null;
        this.sourceClassMap = null;
        this.nullReference = null;
        if (!writer.isTagged()) {
            throw new BadPdfFormatException(MessageLocalization.getComposedMessage("no.structtreeroot.found", new Object[0]));
        }
        this.writer = writer;
        (this.structureTreeRoot = writer.getStructureTreeRoot()).put(PdfName.PARENTTREE, new PdfDictionary(PdfName.STRUCTELEM));
        this.setReader(reader);
    }
    
    protected void setReader(final PdfReader reader) throws BadPdfFormatException {
        this.reader = reader;
        PdfObject obj = reader.getCatalog().get(PdfName.STRUCTTREEROOT);
        obj = getDirectObject(obj);
        if (obj == null || !obj.isDictionary()) {
            throw new BadPdfFormatException(MessageLocalization.getComposedMessage("no.structtreeroot.found", new Object[0]));
        }
        this.structTreeRoot = (PdfDictionary)obj;
        obj = getDirectObject(this.structTreeRoot.get(PdfName.PARENTTREE));
        if (obj == null || !obj.isDictionary()) {
            throw new BadPdfFormatException(MessageLocalization.getComposedMessage("the.document.does.not.contain.parenttree", new Object[0]));
        }
        this.parentTree = (PdfDictionary)obj;
        this.sourceRoleMap = null;
        this.sourceClassMap = null;
        this.nullReference = null;
    }
    
    public static boolean checkTagged(final PdfReader reader) {
        PdfObject obj = reader.getCatalog().get(PdfName.STRUCTTREEROOT);
        obj = getDirectObject(obj);
        if (obj == null || !obj.isDictionary()) {
            return false;
        }
        final PdfDictionary structTreeRoot = (PdfDictionary)obj;
        obj = getDirectObject(structTreeRoot.get(PdfName.PARENTTREE));
        return obj != null && obj.isDictionary();
    }
    
    public static PdfObject getDirectObject(PdfObject object) {
        if (object == null) {
            return null;
        }
        while (object.isIndirect()) {
            object = PdfReader.getPdfObjectRelease(object);
        }
        return object;
    }
    
    public void copyStructTreeForPage(final PdfNumber sourceArrayNumber, final int newArrayNumber) throws BadPdfFormatException, IOException {
        if (this.copyPageMarks(this.parentTree, sourceArrayNumber, newArrayNumber) == returnType.NOTFOUND) {
            throw new BadPdfFormatException(MessageLocalization.getComposedMessage("invalid.structparent", new Object[0]));
        }
    }
    
    private returnType copyPageMarks(final PdfDictionary parentTree, final PdfNumber arrayNumber, final int newArrayNumber) throws BadPdfFormatException, IOException {
        final PdfArray pages = (PdfArray)getDirectObject(parentTree.get(PdfName.NUMS));
        if (pages == null) {
            final PdfArray kids = (PdfArray)getDirectObject(parentTree.get(PdfName.KIDS));
            if (kids == null) {
                return returnType.NOTFOUND;
            }
            int cur = kids.size() / 2;
            int begin = 0;
            while (true) {
                final PdfDictionary kidTree = (PdfDictionary)getDirectObject(kids.getPdfObject(cur + begin));
                switch (this.copyPageMarks(kidTree, arrayNumber, newArrayNumber)) {
                    case FOUND: {
                        return returnType.FOUND;
                    }
                    case ABOVE: {
                        begin += cur;
                        cur /= 2;
                        if (cur == 0) {
                            cur = 1;
                        }
                        if (cur + begin == kids.size()) {
                            return returnType.ABOVE;
                        }
                        continue;
                    }
                    case BELOW: {
                        if (cur + begin == 0) {
                            return returnType.BELOW;
                        }
                        if (cur == 0) {
                            return returnType.NOTFOUND;
                        }
                        cur /= 2;
                        continue;
                    }
                    default: {
                        return returnType.NOTFOUND;
                    }
                }
            }
        }
        else {
            if (pages.size() == 0) {
                return returnType.NOTFOUND;
            }
            return this.findAndCopyMarks(pages, arrayNumber.intValue(), newArrayNumber);
        }
    }
    
    private returnType findAndCopyMarks(final PdfArray pages, final int arrayNumber, final int newArrayNumber) throws BadPdfFormatException, IOException {
        if (pages.getAsNumber(0).intValue() > arrayNumber) {
            return returnType.BELOW;
        }
        if (pages.getAsNumber(pages.size() - 2).intValue() < arrayNumber) {
            return returnType.ABOVE;
        }
        int cur = pages.size() / 4;
        int begin = 0;
        while (true) {
            final int curNumber = pages.getAsNumber((begin + cur) * 2).intValue();
            if (curNumber == arrayNumber) {
                PdfObject obj2;
                PdfObject obj;
                for (obj = (obj2 = pages.getPdfObject((begin + cur) * 2 + 1)); obj.isIndirect(); obj = PdfReader.getPdfObjectRelease(obj)) {}
                if (obj.isArray()) {
                    PdfObject firstNotNullKid = null;
                    for (final PdfObject numObj : (PdfArray)obj) {
                        if (numObj.isNull()) {
                            if (this.nullReference == null) {
                                this.nullReference = this.writer.addToBody(new PdfNull()).getIndirectReference();
                            }
                            this.structureTreeRoot.setPageMark(newArrayNumber, this.nullReference);
                        }
                        else {
                            final PdfObject res = this.writer.copyObject(numObj, true, false);
                            if (firstNotNullKid == null) {
                                firstNotNullKid = res;
                            }
                            this.structureTreeRoot.setPageMark(newArrayNumber, (PdfIndirectReference)res);
                        }
                    }
                    this.attachStructTreeRootKids(firstNotNullKid);
                }
                else {
                    if (!obj.isDictionary()) {
                        return returnType.NOTFOUND;
                    }
                    final PdfDictionary k = getKDict((PdfDictionary)obj);
                    if (k == null) {
                        return returnType.NOTFOUND;
                    }
                    final PdfObject res2 = this.writer.copyObject(obj2, true, false);
                    this.structureTreeRoot.setAnnotationMark(newArrayNumber, (PdfIndirectReference)res2);
                }
                return returnType.FOUND;
            }
            if (curNumber < arrayNumber) {
                if (cur == 0) {
                    return returnType.NOTFOUND;
                }
                begin += cur;
                if (cur != 1) {
                    cur /= 2;
                }
                if (cur + begin == pages.size()) {
                    return returnType.NOTFOUND;
                }
                continue;
            }
            else {
                if (cur + begin == 0) {
                    return returnType.BELOW;
                }
                if (cur == 0) {
                    return returnType.NOTFOUND;
                }
                cur /= 2;
            }
        }
    }
    
    protected void attachStructTreeRootKids(final PdfObject firstNotNullKid) throws IOException, BadPdfFormatException {
        final PdfObject structKids = this.structTreeRoot.get(PdfName.K);
        if (structKids == null || (!structKids.isArray() && !structKids.isIndirect())) {
            this.addKid(this.structureTreeRoot, firstNotNullKid);
        }
        else if (structKids.isIndirect()) {
            this.addKid(structKids);
        }
        else {
            for (final PdfObject kid : (PdfArray)structKids) {
                this.addKid(kid);
            }
        }
    }
    
    static PdfDictionary getKDict(final PdfDictionary obj) {
        PdfDictionary k = obj.getAsDict(PdfName.K);
        if (k != null) {
            if (PdfName.OBJR.equals(k.getAsName(PdfName.TYPE))) {
                return k;
            }
        }
        else {
            final PdfArray k2 = obj.getAsArray(PdfName.K);
            if (k2 == null) {
                return null;
            }
            for (int i = 0; i < k2.size(); ++i) {
                k = k2.getAsDict(i);
                if (k != null && PdfName.OBJR.equals(k.getAsName(PdfName.TYPE))) {
                    return k;
                }
            }
        }
        return null;
    }
    
    private void addKid(final PdfObject obj) throws IOException, BadPdfFormatException {
        if (!obj.isIndirect()) {
            return;
        }
        final PRIndirectReference currRef = (PRIndirectReference)obj;
        final RefKey key = new RefKey(currRef);
        if (!this.writer.indirects.containsKey(key)) {
            this.writer.copyIndirect(currRef, true, false);
        }
        final PdfIndirectReference newKid = this.writer.indirects.get(key).getRef();
        if (this.writer.updateRootKids) {
            this.addKid(this.structureTreeRoot, newKid);
            this.writer.structureTreeRootKidsForReaderImported(this.reader);
        }
    }
    
    private static PdfArray getDirectArray(final PdfArray in) {
        final PdfArray out = new PdfArray();
        for (int i = 0; i < in.size(); ++i) {
            final PdfObject value = getDirectObject(in.getPdfObject(i));
            if (value != null) {
                if (value.isArray()) {
                    out.add(getDirectArray((PdfArray)value));
                }
                else if (value.isDictionary()) {
                    out.add(getDirectDict((PdfDictionary)value));
                }
                else {
                    out.add(value);
                }
            }
        }
        return out;
    }
    
    private static PdfDictionary getDirectDict(final PdfDictionary in) {
        final PdfDictionary out = new PdfDictionary();
        for (final Map.Entry<PdfName, PdfObject> entry : in.hashMap.entrySet()) {
            final PdfObject value = getDirectObject(entry.getValue());
            if (value == null) {
                continue;
            }
            if (value.isArray()) {
                out.put(entry.getKey(), getDirectArray((PdfArray)value));
            }
            else if (value.isDictionary()) {
                out.put(entry.getKey(), getDirectDict((PdfDictionary)value));
            }
            else {
                out.put(entry.getKey(), value);
            }
        }
        return out;
    }
    
    public static boolean compareObjects(final PdfObject value1, PdfObject value2) {
        value2 = getDirectObject(value2);
        if (value2 == null) {
            return false;
        }
        if (value1.type() != value2.type()) {
            return false;
        }
        if (value1.isBoolean()) {
            return value1 == value2 || (value2 instanceof PdfBoolean && ((PdfBoolean)value1).booleanValue() == ((PdfBoolean)value2).booleanValue());
        }
        if (value1.isName()) {
            return value1.equals(value2);
        }
        if (value1.isNumber()) {
            return value1 == value2 || (value2 instanceof PdfNumber && ((PdfNumber)value1).doubleValue() == ((PdfNumber)value2).doubleValue());
        }
        if (value1.isNull()) {
            return value1 == value2 || value2 instanceof PdfNull;
        }
        if (value1.isString()) {
            return value1 == value2 || (value2 instanceof PdfString && ((((PdfString)value2).value == null && ((PdfString)value1).value == null) || (((PdfString)value1).value != null && ((PdfString)value1).value.equals(((PdfString)value2).value))));
        }
        if (value1.isArray()) {
            final PdfArray array1 = (PdfArray)value1;
            final PdfArray array2 = (PdfArray)value2;
            if (array1.size() != array2.size()) {
                return false;
            }
            for (int i = 0; i < array1.size(); ++i) {
                if (!compareObjects(array1.getPdfObject(i), array2.getPdfObject(i))) {
                    return false;
                }
            }
            return true;
        }
        else {
            if (!value1.isDictionary()) {
                return false;
            }
            final PdfDictionary first = (PdfDictionary)value1;
            final PdfDictionary second = (PdfDictionary)value2;
            if (first.size() != second.size()) {
                return false;
            }
            for (final PdfName name : first.hashMap.keySet()) {
                if (!compareObjects(first.get(name), second.get(name))) {
                    return false;
                }
            }
            return true;
        }
    }
    
    protected void addClass(PdfObject object) throws BadPdfFormatException {
        object = getDirectObject(object);
        if (object.isDictionary()) {
            final PdfObject curClass = ((PdfDictionary)object).get(PdfName.C);
            if (curClass == null) {
                return;
            }
            if (curClass.isArray()) {
                final PdfArray array = (PdfArray)curClass;
                for (int i = 0; i < array.size(); ++i) {
                    this.addClass(array.getPdfObject(i));
                }
            }
            else if (curClass.isName()) {
                this.addClass(curClass);
            }
        }
        else if (object.isName()) {
            final PdfName name = (PdfName)object;
            if (this.sourceClassMap == null) {
                object = getDirectObject(this.structTreeRoot.get(PdfName.CLASSMAP));
                if (object == null || !object.isDictionary()) {
                    return;
                }
                this.sourceClassMap = (PdfDictionary)object;
            }
            object = getDirectObject(this.sourceClassMap.get(name));
            if (object == null) {
                return;
            }
            final PdfObject put = this.structureTreeRoot.getMappedClass(name);
            if (put != null) {
                if (!compareObjects(put, object)) {
                    throw new BadPdfFormatException(MessageLocalization.getComposedMessage("conflict.in.classmap", name));
                }
            }
            else if (object.isDictionary()) {
                this.structureTreeRoot.mapClass(name, getDirectDict((PdfDictionary)object));
            }
            else if (object.isArray()) {
                this.structureTreeRoot.mapClass(name, getDirectArray((PdfArray)object));
            }
        }
    }
    
    protected void addRole(final PdfName structType) throws BadPdfFormatException {
        if (structType == null) {
            return;
        }
        for (final PdfName name : this.writer.getStandardStructElems()) {
            if (name.equals(structType)) {
                return;
            }
        }
        if (this.sourceRoleMap == null) {
            final PdfObject object = getDirectObject(this.structTreeRoot.get(PdfName.ROLEMAP));
            if (object == null || !object.isDictionary()) {
                return;
            }
            this.sourceRoleMap = (PdfDictionary)object;
        }
        final PdfObject object = this.sourceRoleMap.get(structType);
        if (object == null || !object.isName()) {
            return;
        }
        if (this.roleMap == null) {
            this.roleMap = new PdfDictionary();
            this.structureTreeRoot.put(PdfName.ROLEMAP, this.roleMap);
            this.roleMap.put(structType, object);
        }
        else {
            final PdfObject currentRole;
            if ((currentRole = this.roleMap.get(structType)) != null) {
                if (!currentRole.equals(object)) {
                    throw new BadPdfFormatException(MessageLocalization.getComposedMessage("conflict.in.rolemap", structType));
                }
            }
            else {
                this.roleMap.put(structType, object);
            }
        }
    }
    
    protected void addKid(final PdfDictionary parent, final PdfObject kid) {
        final PdfObject kidObj = parent.get(PdfName.K);
        PdfArray kids;
        if (kidObj instanceof PdfArray) {
            kids = (PdfArray)kidObj;
        }
        else {
            kids = new PdfArray();
            if (kidObj != null) {
                kids.add(kidObj);
            }
        }
        kids.add(kid);
        parent.put(PdfName.K, kids);
    }
    
    public enum returnType
    {
        BELOW, 
        FOUND, 
        ABOVE, 
        NOTFOUND;
    }
}
