// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.util.Collection;
import java.util.Arrays;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.AccessibleElementId;
import java.util.HashMap;
import java.util.HashSet;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;

public class PdfArtifact implements IAccessibleElement
{
    private static final HashSet<String> allowedArtifactTypes;
    protected PdfName role;
    protected HashMap<PdfName, PdfObject> accessibleAttributes;
    protected AccessibleElementId id;
    
    public PdfArtifact() {
        this.role = PdfName.ARTIFACT;
        this.accessibleAttributes = null;
        this.id = new AccessibleElementId();
    }
    
    @Override
    public PdfObject getAccessibleAttribute(final PdfName key) {
        if (this.accessibleAttributes != null) {
            return this.accessibleAttributes.get(key);
        }
        return null;
    }
    
    @Override
    public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
        if (this.accessibleAttributes == null) {
            this.accessibleAttributes = new HashMap<PdfName, PdfObject>();
        }
        this.accessibleAttributes.put(key, value);
    }
    
    @Override
    public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
        return this.accessibleAttributes;
    }
    
    @Override
    public PdfName getRole() {
        return this.role;
    }
    
    @Override
    public void setRole(final PdfName role) {
    }
    
    @Override
    public AccessibleElementId getId() {
        return this.id;
    }
    
    @Override
    public void setId(final AccessibleElementId id) {
        this.id = id;
    }
    
    @Override
    public boolean isInline() {
        return true;
    }
    
    public PdfString getType() {
        return (this.accessibleAttributes == null) ? null : this.accessibleAttributes.get(PdfName.TYPE);
    }
    
    public void setType(final PdfString type) {
        if (!PdfArtifact.allowedArtifactTypes.contains(type.toString())) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("the.artifact.type.1.is.invalid", type));
        }
        this.setAccessibleAttribute(PdfName.TYPE, type);
    }
    
    public void setType(final ArtifactType type) {
        PdfString artifactType = null;
        switch (type) {
            case BACKGROUND: {
                artifactType = new PdfString("Background");
                break;
            }
            case LAYOUT: {
                artifactType = new PdfString("Layout");
                break;
            }
            case PAGE: {
                artifactType = new PdfString("Page");
                break;
            }
            case PAGINATION: {
                artifactType = new PdfString("Pagination");
                break;
            }
        }
        this.setAccessibleAttribute(PdfName.TYPE, artifactType);
    }
    
    public PdfArray getBBox() {
        return (this.accessibleAttributes == null) ? null : this.accessibleAttributes.get(PdfName.BBOX);
    }
    
    public void setBBox(final PdfArray bbox) {
        this.setAccessibleAttribute(PdfName.BBOX, bbox);
    }
    
    public PdfArray getAttached() {
        return (this.accessibleAttributes == null) ? null : this.accessibleAttributes.get(PdfName.ATTACHED);
    }
    
    public void setAttached(final PdfArray attached) {
        this.setAccessibleAttribute(PdfName.ATTACHED, attached);
    }
    
    static {
        allowedArtifactTypes = new HashSet<String>(Arrays.asList("Pagination", "Layout", "Page", "Background"));
    }
    
    public enum ArtifactType
    {
        PAGINATION, 
        LAYOUT, 
        PAGE, 
        BACKGROUND;
    }
}
