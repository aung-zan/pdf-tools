// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.log.LoggerFactory;
import java.text.MessageFormat;
import com.itextpdf.text.Utilities;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Element;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Font;
import java.util.ArrayList;
import com.itextpdf.text.log.Logger;

public class FontSelector
{
    private static final Logger LOGGER;
    protected ArrayList<Font> fonts;
    protected ArrayList<Font> unsupportedFonts;
    protected Font currentFont;
    
    public FontSelector() {
        this.fonts = new ArrayList<Font>();
        this.unsupportedFonts = new ArrayList<Font>();
        this.currentFont = null;
    }
    
    public void addFont(final Font font) {
        if (!this.isSupported(font)) {
            this.unsupportedFonts.add(font);
            return;
        }
        if (font.getBaseFont() != null) {
            this.fonts.add(font);
            return;
        }
        final BaseFont bf = font.getCalculatedBaseFont(true);
        final Font f2 = new Font(bf, font.getSize(), font.getCalculatedStyle(), font.getColor());
        this.fonts.add(f2);
    }
    
    public Phrase process(final String text) {
        if (this.getSize() == 0) {
            throw new IndexOutOfBoundsException(MessageLocalization.getComposedMessage("no.font.is.defined", new Object[0]));
        }
        final char[] cc = text.toCharArray();
        final int len = cc.length;
        final StringBuffer sb = new StringBuffer();
        final Phrase ret = new Phrase();
        this.currentFont = null;
        for (int k = 0; k < len; ++k) {
            final Chunk newChunk = this.processChar(cc, k, sb);
            if (newChunk != null) {
                ret.add(newChunk);
            }
        }
        if (sb.length() > 0) {
            final Chunk ck = new Chunk(sb.toString(), (this.currentFont != null) ? this.currentFont : this.getFont(0));
            ret.add(ck);
        }
        return ret;
    }
    
    protected Chunk processChar(final char[] cc, int k, final StringBuffer sb) {
        Chunk newChunk = null;
        final char c = cc[k];
        if (c == '\n' || c == '\r') {
            sb.append(c);
        }
        else {
            Font font = null;
            if (Utilities.isSurrogatePair(cc, k)) {
                final int u = Utilities.convertToUtf32(cc, k);
                for (int f = 0; f < this.getSize(); ++f) {
                    font = this.getFont(f);
                    if (font.getBaseFont().charExists(u) || Character.getType(u) == 16) {
                        if (this.currentFont != font) {
                            if (sb.length() > 0 && this.currentFont != null) {
                                newChunk = new Chunk(sb.toString(), this.currentFont);
                                sb.setLength(0);
                            }
                            this.currentFont = font;
                        }
                        sb.append(c);
                        sb.append(cc[++k]);
                        break;
                    }
                }
            }
            else {
                for (int f2 = 0; f2 < this.getSize(); ++f2) {
                    font = this.getFont(f2);
                    if (font.getBaseFont().charExists(c) || Character.getType(c) == 16) {
                        if (this.currentFont != font) {
                            if (sb.length() > 0 && this.currentFont != null) {
                                newChunk = new Chunk(sb.toString(), this.currentFont);
                                sb.setLength(0);
                            }
                            this.currentFont = font;
                        }
                        sb.append(c);
                        break;
                    }
                }
            }
        }
        return newChunk;
    }
    
    protected int getSize() {
        return this.fonts.size() + this.unsupportedFonts.size();
    }
    
    protected Font getFont(final int i) {
        return (i < this.fonts.size()) ? this.fonts.get(i) : this.unsupportedFonts.get(i);
    }
    
    private boolean isSupported(final Font font) {
        final BaseFont bf = font.getBaseFont();
        if (bf instanceof TrueTypeFont && "Cp1252".equals(bf.getEncoding()) && !((TrueTypeFont)bf).isWinAnsiSupported()) {
            FontSelector.LOGGER.warn(MessageFormat.format("cmap(1, 0) not found for TrueType Font {0}, it is required for WinAnsi encoding.", font));
            return false;
        }
        return true;
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(PdfSmartCopy.class);
    }
}
