// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import com.itextpdf.text.DocWriter;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Image;
import com.itextpdf.text.ExceptionConverter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.OutputStream;
import com.itextpdf.text.log.CounterFactory;
import com.itextpdf.text.log.Counter;
import java.util.HashMap;

public class FdfWriter
{
    private static final byte[] HEADER_FDF;
    HashMap<String, Object> fields;
    Wrt wrt;
    private String file;
    private String statusMessage;
    protected Counter COUNTER;
    
    public FdfWriter() {
        this.fields = new HashMap<String, Object>();
        this.wrt = null;
        this.COUNTER = CounterFactory.getCounter(FdfWriter.class);
    }
    
    public FdfWriter(final OutputStream os) throws IOException {
        this.fields = new HashMap<String, Object>();
        this.wrt = null;
        this.COUNTER = CounterFactory.getCounter(FdfWriter.class);
        this.wrt = new Wrt(os, this);
    }
    
    public void writeTo(final OutputStream os) throws IOException {
        if (this.wrt == null) {
            this.wrt = new Wrt(os, this);
        }
        this.wrt.write();
    }
    
    public void write() throws IOException {
        this.wrt.write();
    }
    
    public String getStatusMessage() {
        return this.statusMessage;
    }
    
    public void setStatusMessage(final String statusMessage) {
        this.statusMessage = statusMessage;
    }
    
    boolean setField(final String field, final PdfObject value) {
        HashMap<String, Object> map = this.fields;
        final StringTokenizer tk = new StringTokenizer(field, ".");
        if (!tk.hasMoreTokens()) {
            return false;
        }
        while (true) {
            final String s = tk.nextToken();
            Object obj = map.get(s);
            if (tk.hasMoreTokens()) {
                if (obj == null) {
                    obj = new HashMap();
                    map.put(s, obj);
                    map = (HashMap<String, Object>)obj;
                }
                else {
                    if (!(obj instanceof HashMap)) {
                        return false;
                    }
                    map = (HashMap<String, Object>)obj;
                }
            }
            else {
                if (!(obj instanceof HashMap)) {
                    map.put(s, value);
                    return true;
                }
                return false;
            }
        }
    }
    
    void iterateFields(final HashMap<String, Object> values, final HashMap<String, Object> map, final String name) {
        for (final Map.Entry<String, Object> entry : map.entrySet()) {
            final String s = entry.getKey();
            final Object obj = entry.getValue();
            if (obj instanceof HashMap) {
                this.iterateFields(values, (HashMap<String, Object>)obj, name + "." + s);
            }
            else {
                values.put((name + "." + s).substring(1), obj);
            }
        }
    }
    
    public boolean removeField(final String field) {
        HashMap<String, Object> map = this.fields;
        final StringTokenizer tk = new StringTokenizer(field, ".");
        if (!tk.hasMoreTokens()) {
            return false;
        }
        final ArrayList<Object> hist = new ArrayList<Object>();
        while (true) {
            final String s = tk.nextToken();
            final Object obj = map.get(s);
            if (obj == null) {
                return false;
            }
            hist.add(map);
            hist.add(s);
            if (tk.hasMoreTokens()) {
                if (!(obj instanceof HashMap)) {
                    return false;
                }
                map = (HashMap<String, Object>)obj;
            }
            else {
                if (obj instanceof HashMap) {
                    return false;
                }
                for (int k = hist.size() - 2; k >= 0; k -= 2) {
                    map = hist.get(k);
                    final String s2 = hist.get(k + 1);
                    map.remove(s2);
                    if (!map.isEmpty()) {
                        break;
                    }
                }
                return true;
            }
        }
    }
    
    public HashMap<String, Object> getFields() {
        final HashMap<String, Object> values = new HashMap<String, Object>();
        this.iterateFields(values, this.fields, "");
        return values;
    }
    
    public String getField(final String field) {
        HashMap<String, Object> map = this.fields;
        final StringTokenizer tk = new StringTokenizer(field, ".");
        if (!tk.hasMoreTokens()) {
            return null;
        }
        while (true) {
            final String s = tk.nextToken();
            final Object obj = map.get(s);
            if (obj == null) {
                return null;
            }
            if (tk.hasMoreTokens()) {
                if (!(obj instanceof HashMap)) {
                    return null;
                }
                map = (HashMap<String, Object>)obj;
            }
            else {
                if (obj instanceof HashMap) {
                    return null;
                }
                if (((PdfObject)obj).isString()) {
                    return ((PdfString)obj).toUnicodeString();
                }
                return PdfName.decodeName(obj.toString());
            }
        }
    }
    
    public boolean setFieldAsName(final String field, final String value) {
        return this.setField(field, new PdfName(value));
    }
    
    public boolean setFieldAsString(final String field, final String value) {
        return this.setField(field, new PdfString(value, "UnicodeBig"));
    }
    
    public boolean setFieldAsAction(final String field, final PdfAction action) {
        return this.setField(field, action);
    }
    
    public boolean setFieldAsTemplate(final String field, final PdfTemplate template) {
        try {
            final PdfDictionary d = new PdfDictionary();
            if (template instanceof PdfImportedPage) {
                d.put(PdfName.N, template.getIndirectReference());
            }
            else {
                final PdfStream str = template.getFormXObject(0);
                final PdfIndirectReference ref = this.wrt.addToBody(str).getIndirectReference();
                d.put(PdfName.N, ref);
            }
            return this.setField(field, d);
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public boolean setFieldAsImage(final String field, final Image image) {
        try {
            if (Float.isNaN(image.getAbsoluteX())) {
                image.setAbsolutePosition(0.0f, image.getAbsoluteY());
            }
            if (Float.isNaN(image.getAbsoluteY())) {
                image.setAbsolutePosition(image.getAbsoluteY(), 0.0f);
            }
            final PdfTemplate tmpl = PdfTemplate.createTemplate(this.wrt, image.getWidth(), image.getHeight());
            tmpl.addImage(image);
            final PdfStream str = tmpl.getFormXObject(0);
            final PdfIndirectReference ref = this.wrt.addToBody(str).getIndirectReference();
            final PdfDictionary d = new PdfDictionary();
            d.put(PdfName.N, ref);
            return this.setField(field, d);
        }
        catch (Exception de) {
            throw new ExceptionConverter(de);
        }
    }
    
    public boolean setFieldAsJavascript(final String field, final PdfName jsTrigName, final String js) {
        final PdfAnnotation dict = this.wrt.createAnnotation(null, null);
        final PdfAction javascript = PdfAction.javaScript(js, this.wrt);
        dict.put(jsTrigName, javascript);
        return this.setField(field, dict);
    }
    
    public PdfImportedPage getImportedPage(final PdfReader reader, final int pageNumber) {
        return this.wrt.getImportedPage(reader, pageNumber);
    }
    
    public PdfTemplate createTemplate(final float width, final float height) {
        return PdfTemplate.createTemplate(this.wrt, width, height);
    }
    
    public void setFields(final FdfReader fdf) {
        final HashMap<String, PdfDictionary> map = fdf.getFields();
        for (final Map.Entry<String, PdfDictionary> entry : map.entrySet()) {
            final String key = entry.getKey();
            final PdfDictionary dic = entry.getValue();
            PdfObject v = dic.get(PdfName.V);
            if (v != null) {
                this.setField(key, v);
            }
            v = dic.get(PdfName.A);
            if (v != null) {
                this.setField(key, v);
            }
        }
    }
    
    public void setFields(final PdfReader pdf) {
        this.setFields(pdf.getAcroFields());
    }
    
    public void setFields(final AcroFields af) {
        for (final Map.Entry<String, AcroFields.Item> entry : af.getFields().entrySet()) {
            final String fn = entry.getKey();
            final AcroFields.Item item = entry.getValue();
            final PdfDictionary dic = item.getMerged(0);
            final PdfObject v = PdfReader.getPdfObjectRelease(dic.get(PdfName.V));
            if (v == null) {
                continue;
            }
            final PdfObject ft = PdfReader.getPdfObjectRelease(dic.get(PdfName.FT));
            if (ft == null) {
                continue;
            }
            if (PdfName.SIG.equals(ft)) {
                continue;
            }
            this.setField(fn, v);
        }
    }
    
    public String getFile() {
        return this.file;
    }
    
    public void setFile(final String file) {
        this.file = file;
    }
    
    protected Counter getCounter() {
        return this.COUNTER;
    }
    
    static {
        HEADER_FDF = DocWriter.getISOBytes("%FDF-1.4\n%\u00e2\u00e3\u00cf\u00d3\n");
    }
    
    static class Wrt extends PdfWriter
    {
        private FdfWriter fdf;
        
        Wrt(final OutputStream os, final FdfWriter fdf) throws IOException {
            super(new PdfDocument(), os);
            this.fdf = fdf;
            this.os.write(FdfWriter.HEADER_FDF);
            this.body = new PdfBody(this);
        }
        
        void write() throws IOException {
            for (final PdfReaderInstance element : this.readerInstances.values()) {
                (this.currentPdfReaderInstance = element).writeAllPages();
            }
            final PdfDictionary dic = new PdfDictionary();
            dic.put(PdfName.FIELDS, this.calculate(this.fdf.fields));
            if (this.fdf.file != null) {
                dic.put(PdfName.F, new PdfString(this.fdf.file, "UnicodeBig"));
            }
            if (this.fdf.statusMessage != null && this.fdf.statusMessage.trim().length() != 0) {
                dic.put(PdfName.STATUS, new PdfString(this.fdf.statusMessage));
            }
            final PdfDictionary fd = new PdfDictionary();
            fd.put(PdfName.FDF, dic);
            final PdfIndirectReference ref = this.addToBody(fd).getIndirectReference();
            this.os.write(DocWriter.getISOBytes("trailer\n"));
            final PdfDictionary trailer = new PdfDictionary();
            trailer.put(PdfName.ROOT, ref);
            trailer.toPdf(null, this.os);
            this.os.write(DocWriter.getISOBytes("\n%%EOF\n"));
            this.os.close();
        }
        
        PdfArray calculate(final HashMap<String, Object> map) throws IOException {
            final PdfArray ar = new PdfArray();
            for (final Map.Entry<String, Object> entry : map.entrySet()) {
                final String key = entry.getKey();
                final Object v = entry.getValue();
                final PdfDictionary dic = new PdfDictionary();
                dic.put(PdfName.T, new PdfString(key, "UnicodeBig"));
                if (v instanceof HashMap) {
                    dic.put(PdfName.KIDS, this.calculate((HashMap<String, Object>)v));
                }
                else if (v instanceof PdfAction) {
                    dic.put(PdfName.A, (PdfObject)v);
                }
                else if (v instanceof PdfAnnotation) {
                    dic.put(PdfName.AA, (PdfObject)v);
                }
                else if (v instanceof PdfDictionary && ((PdfDictionary)v).size() == 1 && ((PdfDictionary)v).contains(PdfName.N)) {
                    dic.put(PdfName.AP, (PdfObject)v);
                }
                else {
                    dic.put(PdfName.V, (PdfObject)v);
                }
                ar.add(dic);
            }
            return ar;
        }
    }
}
