// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.collection;

import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfDictionary;

public class PdfCollectionSchema extends PdfDictionary
{
    public PdfCollectionSchema() {
        super(PdfName.COLLECTIONSCHEMA);
    }
    
    public void addField(final String name, final PdfCollectionField field) {
        this.put(new PdfName(name), field);
    }
}
