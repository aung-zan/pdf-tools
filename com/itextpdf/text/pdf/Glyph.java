// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public class Glyph
{
    public final int code;
    public final int width;
    public final String chars;
    
    public Glyph(final int code, final int width, final String chars) {
        this.code = code;
        this.width = width;
        this.chars = chars;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + ((this.chars == null) ? 0 : this.chars.hashCode());
        result = 31 * result + this.code;
        result = 31 * result + this.width;
        return result;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Glyph other = (Glyph)obj;
        if (this.chars == null) {
            if (other.chars != null) {
                return false;
            }
        }
        else if (!this.chars.equals(other.chars)) {
            return false;
        }
        return this.code == other.code && this.width == other.width;
    }
    
    @Override
    public String toString() {
        return Glyph.class.getSimpleName() + " [id=" + this.code + ", width=" + this.width + ", chars=" + this.chars + "]";
    }
}
