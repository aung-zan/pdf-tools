// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.draw;

import com.itextpdf.text.pdf.PdfContentByte;

public interface DrawInterface
{
    void draw(final PdfContentByte p0, final float p1, final float p2, final float p3, final float p4, final float p5);
}
