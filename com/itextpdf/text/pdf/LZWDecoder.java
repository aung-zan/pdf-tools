// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.io.IOException;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.io.OutputStream;

public class LZWDecoder
{
    byte[][] stringTable;
    byte[] data;
    OutputStream uncompData;
    int tableIndex;
    int bitsToGet;
    int bytePointer;
    int bitPointer;
    int nextData;
    int nextBits;
    int[] andTable;
    
    public LZWDecoder() {
        this.data = null;
        this.bitsToGet = 9;
        this.nextData = 0;
        this.nextBits = 0;
        this.andTable = new int[] { 511, 1023, 2047, 4095 };
    }
    
    public void decode(final byte[] data, final OutputStream uncompData) {
        if (data[0] == 0 && data[1] == 1) {
            throw new RuntimeException(MessageLocalization.getComposedMessage("lzw.flavour.not.supported", new Object[0]));
        }
        this.initializeStringTable();
        this.data = data;
        this.uncompData = uncompData;
        this.bytePointer = 0;
        this.bitPointer = 0;
        this.nextData = 0;
        this.nextBits = 0;
        int oldCode = 0;
        int code;
        while ((code = this.getNextCode()) != 257) {
            if (code == 256) {
                this.initializeStringTable();
                code = this.getNextCode();
                if (code == 257) {
                    break;
                }
                this.writeString(this.stringTable[code]);
                oldCode = code;
            }
            else if (code < this.tableIndex) {
                final byte[] string = this.stringTable[code];
                this.writeString(string);
                this.addStringToTable(this.stringTable[oldCode], string[0]);
                oldCode = code;
            }
            else {
                byte[] string = this.stringTable[oldCode];
                string = this.composeString(string, string[0]);
                this.writeString(string);
                this.addStringToTable(string);
                oldCode = code;
            }
        }
    }
    
    public void initializeStringTable() {
        this.stringTable = new byte[8192][];
        for (int i = 0; i < 256; ++i) {
            (this.stringTable[i] = new byte[1])[0] = (byte)i;
        }
        this.tableIndex = 258;
        this.bitsToGet = 9;
    }
    
    public void writeString(final byte[] string) {
        try {
            this.uncompData.write(string);
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public void addStringToTable(final byte[] oldString, final byte newString) {
        final int length = oldString.length;
        final byte[] string = new byte[length + 1];
        System.arraycopy(oldString, 0, string, 0, length);
        string[length] = newString;
        this.stringTable[this.tableIndex++] = string;
        if (this.tableIndex == 511) {
            this.bitsToGet = 10;
        }
        else if (this.tableIndex == 1023) {
            this.bitsToGet = 11;
        }
        else if (this.tableIndex == 2047) {
            this.bitsToGet = 12;
        }
    }
    
    public void addStringToTable(final byte[] string) {
        this.stringTable[this.tableIndex++] = string;
        if (this.tableIndex == 511) {
            this.bitsToGet = 10;
        }
        else if (this.tableIndex == 1023) {
            this.bitsToGet = 11;
        }
        else if (this.tableIndex == 2047) {
            this.bitsToGet = 12;
        }
    }
    
    public byte[] composeString(final byte[] oldString, final byte newString) {
        final int length = oldString.length;
        final byte[] string = new byte[length + 1];
        System.arraycopy(oldString, 0, string, 0, length);
        string[length] = newString;
        return string;
    }
    
    public int getNextCode() {
        try {
            this.nextData = (this.nextData << 8 | (this.data[this.bytePointer++] & 0xFF));
            this.nextBits += 8;
            if (this.nextBits < this.bitsToGet) {
                this.nextData = (this.nextData << 8 | (this.data[this.bytePointer++] & 0xFF));
                this.nextBits += 8;
            }
            final int code = this.nextData >> this.nextBits - this.bitsToGet & this.andTable[this.bitsToGet - 9];
            this.nextBits -= this.bitsToGet;
            return code;
        }
        catch (ArrayIndexOutOfBoundsException e) {
            return 257;
        }
    }
}
