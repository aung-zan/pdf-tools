// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.security.cert.Certificate;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.IOException;
import com.itextpdf.text.exceptions.BadPasswordException;
import com.itextpdf.text.DocWriter;
import com.itextpdf.text.pdf.crypto.AESCipherCBCnoPad;
import com.itextpdf.text.pdf.crypto.IVGenerator;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.pdf.crypto.ARCFOUREncryption;
import java.security.MessageDigest;

public class PdfEncryption
{
    public static final int STANDARD_ENCRYPTION_40 = 2;
    public static final int STANDARD_ENCRYPTION_128 = 3;
    public static final int AES_128 = 4;
    public static final int AES_256 = 5;
    private static final byte[] pad;
    private static final byte[] salt;
    private static final byte[] metadataPad;
    byte[] key;
    int keySize;
    byte[] mkey;
    byte[] ownerKey;
    byte[] userKey;
    byte[] oeKey;
    byte[] ueKey;
    byte[] perms;
    long permissions;
    byte[] documentID;
    private int revision;
    private int keyLength;
    protected PdfPublicKeySecurityHandler publicKeyHandler;
    byte[] extra;
    MessageDigest md5;
    private ARCFOUREncryption arcfour;
    private boolean encryptMetadata;
    static long seq;
    private boolean embeddedFilesOnly;
    private int cryptoMode;
    private static final int VALIDATION_SALT_OFFSET = 32;
    private static final int KEY_SALT_OFFSET = 40;
    private static final int SALT_LENGHT = 8;
    private static final int OU_LENGHT = 48;
    
    public PdfEncryption() {
        this.mkey = new byte[0];
        this.ownerKey = new byte[32];
        this.userKey = new byte[32];
        this.publicKeyHandler = null;
        this.extra = new byte[5];
        this.arcfour = new ARCFOUREncryption();
        try {
            this.md5 = MessageDigest.getInstance("MD5");
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
        this.publicKeyHandler = new PdfPublicKeySecurityHandler();
    }
    
    public PdfEncryption(final PdfEncryption enc) {
        this();
        if (enc.key != null) {
            this.key = enc.key.clone();
        }
        this.keySize = enc.keySize;
        this.mkey = enc.mkey.clone();
        this.ownerKey = enc.ownerKey.clone();
        this.userKey = enc.userKey.clone();
        this.permissions = enc.permissions;
        if (enc.documentID != null) {
            this.documentID = enc.documentID.clone();
        }
        this.revision = enc.revision;
        this.keyLength = enc.keyLength;
        this.encryptMetadata = enc.encryptMetadata;
        this.embeddedFilesOnly = enc.embeddedFilesOnly;
        this.publicKeyHandler = enc.publicKeyHandler;
        if (enc.ueKey != null) {
            this.ueKey = enc.ueKey.clone();
        }
        if (enc.oeKey != null) {
            this.oeKey = enc.oeKey.clone();
        }
        if (enc.perms != null) {
            this.perms = enc.perms.clone();
        }
    }
    
    public void setCryptoMode(int mode, final int kl) {
        this.cryptoMode = mode;
        this.encryptMetadata = ((mode & 0x8) != 0x8);
        this.embeddedFilesOnly = ((mode & 0x18) == 0x18);
        mode &= 0x7;
        switch (mode) {
            case 0: {
                this.encryptMetadata = true;
                this.embeddedFilesOnly = false;
                this.keyLength = 40;
                this.revision = 2;
                break;
            }
            case 1: {
                this.embeddedFilesOnly = false;
                if (kl > 0) {
                    this.keyLength = kl;
                }
                else {
                    this.keyLength = 128;
                }
                this.revision = 3;
                break;
            }
            case 2: {
                this.keyLength = 128;
                this.revision = 4;
                break;
            }
            case 3: {
                this.keyLength = 256;
                this.keySize = 32;
                this.revision = 5;
                break;
            }
            default: {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("no.valid.encryption.mode", new Object[0]));
            }
        }
    }
    
    public int getCryptoMode() {
        return this.cryptoMode;
    }
    
    public boolean isMetadataEncrypted() {
        return this.encryptMetadata;
    }
    
    public long getPermissions() {
        return this.permissions;
    }
    
    public boolean isEmbeddedFilesOnly() {
        return this.embeddedFilesOnly;
    }
    
    private byte[] padPassword(final byte[] userPassword) {
        final byte[] userPad = new byte[32];
        if (userPassword == null) {
            System.arraycopy(PdfEncryption.pad, 0, userPad, 0, 32);
        }
        else {
            System.arraycopy(userPassword, 0, userPad, 0, Math.min(userPassword.length, 32));
            if (userPassword.length < 32) {
                System.arraycopy(PdfEncryption.pad, 0, userPad, userPassword.length, 32 - userPassword.length);
            }
        }
        return userPad;
    }
    
    private byte[] computeOwnerKey(final byte[] userPad, final byte[] ownerPad) {
        final byte[] ownerKey = new byte[32];
        final byte[] digest = this.md5.digest(ownerPad);
        if (this.revision == 3 || this.revision == 4) {
            final byte[] mkey = new byte[this.keyLength / 8];
            for (int k = 0; k < 50; ++k) {
                this.md5.update(digest, 0, mkey.length);
                System.arraycopy(this.md5.digest(), 0, digest, 0, mkey.length);
            }
            System.arraycopy(userPad, 0, ownerKey, 0, 32);
            for (int i = 0; i < 20; ++i) {
                for (int j = 0; j < mkey.length; ++j) {
                    mkey[j] = (byte)(digest[j] ^ i);
                }
                this.arcfour.prepareARCFOURKey(mkey);
                this.arcfour.encryptARCFOUR(ownerKey);
            }
        }
        else {
            this.arcfour.prepareARCFOURKey(digest, 0, 5);
            this.arcfour.encryptARCFOUR(userPad, ownerKey);
        }
        return ownerKey;
    }
    
    private void setupGlobalEncryptionKey(final byte[] documentID, final byte[] userPad, final byte[] ownerKey, final long permissions) {
        this.documentID = documentID;
        this.ownerKey = ownerKey;
        this.permissions = permissions;
        this.mkey = new byte[this.keyLength / 8];
        this.md5.reset();
        this.md5.update(userPad);
        this.md5.update(ownerKey);
        final byte[] ext = { (byte)permissions, (byte)(permissions >> 8), (byte)(permissions >> 16), (byte)(permissions >> 24) };
        this.md5.update(ext, 0, 4);
        if (documentID != null) {
            this.md5.update(documentID);
        }
        if (!this.encryptMetadata) {
            this.md5.update(PdfEncryption.metadataPad);
        }
        final byte[] digest = new byte[this.mkey.length];
        System.arraycopy(this.md5.digest(), 0, digest, 0, this.mkey.length);
        if (this.revision == 3 || this.revision == 4) {
            for (int k = 0; k < 50; ++k) {
                System.arraycopy(this.md5.digest(digest), 0, digest, 0, this.mkey.length);
            }
        }
        System.arraycopy(digest, 0, this.mkey, 0, this.mkey.length);
    }
    
    private void setupUserKey() {
        if (this.revision == 3 || this.revision == 4) {
            this.md5.update(PdfEncryption.pad);
            final byte[] digest = this.md5.digest(this.documentID);
            System.arraycopy(digest, 0, this.userKey, 0, 16);
            for (int k = 16; k < 32; ++k) {
                this.userKey[k] = 0;
            }
            for (int i = 0; i < 20; ++i) {
                for (int j = 0; j < this.mkey.length; ++j) {
                    digest[j] = (byte)(this.mkey[j] ^ i);
                }
                this.arcfour.prepareARCFOURKey(digest, 0, this.mkey.length);
                this.arcfour.encryptARCFOUR(this.userKey, 0, 16);
            }
        }
        else {
            this.arcfour.prepareARCFOURKey(this.mkey);
            this.arcfour.encryptARCFOUR(PdfEncryption.pad, this.userKey);
        }
    }
    
    public void setupAllKeys(byte[] userPassword, byte[] ownerPassword, int permissions) {
        if (ownerPassword == null || ownerPassword.length == 0) {
            ownerPassword = this.md5.digest(createDocumentId());
        }
        permissions |= ((this.revision == 3 || this.revision == 4 || this.revision == 5) ? -3904 : -64);
        permissions &= 0xFFFFFFFC;
        this.permissions = permissions;
        if (this.revision == 5) {
            try {
                if (userPassword == null) {
                    userPassword = new byte[0];
                }
                this.documentID = createDocumentId();
                final byte[] uvs = IVGenerator.getIV(8);
                final byte[] uks = IVGenerator.getIV(8);
                this.key = IVGenerator.getIV(32);
                final MessageDigest md = MessageDigest.getInstance("SHA-256");
                md.update(userPassword, 0, Math.min(userPassword.length, 127));
                md.update(uvs);
                md.digest(this.userKey = new byte[48], 0, 32);
                System.arraycopy(uvs, 0, this.userKey, 32, 8);
                System.arraycopy(uks, 0, this.userKey, 40, 8);
                md.update(userPassword, 0, Math.min(userPassword.length, 127));
                md.update(uks);
                AESCipherCBCnoPad ac = new AESCipherCBCnoPad(true, md.digest());
                this.ueKey = ac.processBlock(this.key, 0, this.key.length);
                final byte[] ovs = IVGenerator.getIV(8);
                final byte[] oks = IVGenerator.getIV(8);
                md.update(ownerPassword, 0, Math.min(ownerPassword.length, 127));
                md.update(ovs);
                md.update(this.userKey);
                md.digest(this.ownerKey = new byte[48], 0, 32);
                System.arraycopy(ovs, 0, this.ownerKey, 32, 8);
                System.arraycopy(oks, 0, this.ownerKey, 40, 8);
                md.update(ownerPassword, 0, Math.min(ownerPassword.length, 127));
                md.update(oks);
                md.update(this.userKey);
                ac = new AESCipherCBCnoPad(true, md.digest());
                this.oeKey = ac.processBlock(this.key, 0, this.key.length);
                final byte[] permsp = IVGenerator.getIV(16);
                permsp[0] = (byte)permissions;
                permsp[1] = (byte)(permissions >> 8);
                permsp[2] = (byte)(permissions >> 16);
                permsp[3] = (byte)(permissions >> 24);
                permsp[5] = (permsp[4] = -1);
                permsp[7] = (permsp[6] = -1);
                permsp[8] = (byte)(this.encryptMetadata ? 84 : 70);
                permsp[9] = 97;
                permsp[10] = 100;
                permsp[11] = 98;
                ac = new AESCipherCBCnoPad(true, this.key);
                this.perms = ac.processBlock(permsp, 0, permsp.length);
                return;
            }
            catch (Exception ex) {
                throw new ExceptionConverter(ex);
            }
        }
        final byte[] userPad = this.padPassword(userPassword);
        final byte[] ownerPad = this.padPassword(ownerPassword);
        this.ownerKey = this.computeOwnerKey(userPad, ownerPad);
        this.setupByUserPad(this.documentID = createDocumentId(), userPad, this.ownerKey, permissions);
    }
    
    public boolean readKey(final PdfDictionary enc, byte[] password) throws BadPasswordException {
        try {
            if (password == null) {
                password = new byte[0];
            }
            final byte[] oValue = DocWriter.getISOBytes(enc.get(PdfName.O).toString());
            final byte[] uValue = DocWriter.getISOBytes(enc.get(PdfName.U).toString());
            final byte[] oeValue = DocWriter.getISOBytes(enc.get(PdfName.OE).toString());
            final byte[] ueValue = DocWriter.getISOBytes(enc.get(PdfName.UE).toString());
            final byte[] perms = DocWriter.getISOBytes(enc.get(PdfName.PERMS).toString());
            final PdfNumber pValue = (PdfNumber)enc.get(PdfName.P);
            this.oeKey = oeValue;
            this.ueKey = ueValue;
            this.perms = perms;
            this.ownerKey = oValue;
            this.userKey = uValue;
            this.permissions = pValue.longValue();
            boolean isUserPass = false;
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password, 0, Math.min(password.length, 127));
            md.update(oValue, 32, 8);
            md.update(uValue, 0, 48);
            byte[] hash = md.digest();
            final boolean isOwnerPass = compareArray(hash, oValue, 32);
            if (isOwnerPass) {
                md.update(password, 0, Math.min(password.length, 127));
                md.update(oValue, 40, 8);
                md.update(uValue, 0, 48);
                hash = md.digest();
                final AESCipherCBCnoPad ac = new AESCipherCBCnoPad(false, hash);
                this.key = ac.processBlock(oeValue, 0, oeValue.length);
            }
            else {
                md.update(password, 0, Math.min(password.length, 127));
                md.update(uValue, 32, 8);
                hash = md.digest();
                isUserPass = compareArray(hash, uValue, 32);
                if (!isUserPass) {
                    throw new BadPasswordException(MessageLocalization.getComposedMessage("bad.user.password", new Object[0]));
                }
                md.update(password, 0, Math.min(password.length, 127));
                md.update(uValue, 40, 8);
                hash = md.digest();
                final AESCipherCBCnoPad ac = new AESCipherCBCnoPad(false, hash);
                this.key = ac.processBlock(ueValue, 0, ueValue.length);
            }
            final AESCipherCBCnoPad ac = new AESCipherCBCnoPad(false, this.key);
            final byte[] decPerms = ac.processBlock(perms, 0, perms.length);
            if (decPerms[9] != 97 || decPerms[10] != 100 || decPerms[11] != 98) {
                throw new BadPasswordException(MessageLocalization.getComposedMessage("bad.user.password", new Object[0]));
            }
            this.permissions = ((decPerms[0] & 0xFF) | (decPerms[1] & 0xFF) << 8 | (decPerms[2] & 0xFF) << 16 | (decPerms[2] & 0xFF) << 24);
            this.encryptMetadata = (decPerms[8] == 84);
            return isOwnerPass;
        }
        catch (BadPasswordException ex) {
            throw ex;
        }
        catch (Exception ex2) {
            throw new ExceptionConverter(ex2);
        }
    }
    
    private static boolean compareArray(final byte[] a, final byte[] b, final int len) {
        for (int k = 0; k < len; ++k) {
            if (a[k] != b[k]) {
                return false;
            }
        }
        return true;
    }
    
    public static byte[] createDocumentId() {
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
        final long time = System.currentTimeMillis();
        final long mem = Runtime.getRuntime().freeMemory();
        final String s = time + "+" + mem + "+" + PdfEncryption.seq++;
        return md5.digest(s.getBytes());
    }
    
    public void setupByUserPassword(final byte[] documentID, final byte[] userPassword, final byte[] ownerKey, final long permissions) {
        this.setupByUserPad(documentID, this.padPassword(userPassword), ownerKey, permissions);
    }
    
    private void setupByUserPad(final byte[] documentID, final byte[] userPad, final byte[] ownerKey, final long permissions) {
        this.setupGlobalEncryptionKey(documentID, userPad, ownerKey, permissions);
        this.setupUserKey();
    }
    
    public void setupByOwnerPassword(final byte[] documentID, final byte[] ownerPassword, final byte[] userKey, final byte[] ownerKey, final long permissions) {
        this.setupByOwnerPad(documentID, this.padPassword(ownerPassword), userKey, ownerKey, permissions);
    }
    
    private void setupByOwnerPad(final byte[] documentID, final byte[] ownerPad, final byte[] userKey, final byte[] ownerKey, final long permissions) {
        final byte[] userPad = this.computeOwnerKey(ownerKey, ownerPad);
        this.setupGlobalEncryptionKey(documentID, userPad, ownerKey, permissions);
        this.setupUserKey();
    }
    
    public void setKey(final byte[] key) {
        this.key = key;
    }
    
    public void setupByEncryptionKey(final byte[] key, final int keylength) {
        System.arraycopy(key, 0, this.mkey = new byte[keylength / 8], 0, this.mkey.length);
    }
    
    public void setHashKey(final int number, final int generation) {
        if (this.revision == 5) {
            return;
        }
        this.md5.reset();
        this.extra[0] = (byte)number;
        this.extra[1] = (byte)(number >> 8);
        this.extra[2] = (byte)(number >> 16);
        this.extra[3] = (byte)generation;
        this.extra[4] = (byte)(generation >> 8);
        this.md5.update(this.mkey);
        this.md5.update(this.extra);
        if (this.revision == 4) {
            this.md5.update(PdfEncryption.salt);
        }
        this.key = this.md5.digest();
        this.keySize = this.mkey.length + 5;
        if (this.keySize > 16) {
            this.keySize = 16;
        }
    }
    
    public static PdfObject createInfoId(byte[] id, final boolean modified) throws IOException {
        final ByteBuffer buf = new ByteBuffer(90);
        if (id.length == 0) {
            id = createDocumentId();
        }
        buf.append('[').append('<');
        for (int k = 0; k < id.length; ++k) {
            buf.appendHex(id[k]);
        }
        buf.append('>').append('<');
        if (modified) {
            id = createDocumentId();
        }
        for (int k = 0; k < id.length; ++k) {
            buf.appendHex(id[k]);
        }
        buf.append('>').append(']');
        buf.close();
        return new PdfLiteral(buf.toByteArray());
    }
    
    public PdfDictionary getEncryptionDictionary() {
        final PdfDictionary dic = new PdfDictionary();
        if (this.publicKeyHandler.getRecipientsSize() > 0) {
            PdfArray recipients = null;
            dic.put(PdfName.FILTER, PdfName.PUBSEC);
            dic.put(PdfName.R, new PdfNumber(this.revision));
            try {
                recipients = this.publicKeyHandler.getEncodedRecipients();
            }
            catch (Exception f) {
                throw new ExceptionConverter(f);
            }
            if (this.revision == 2) {
                dic.put(PdfName.V, new PdfNumber(1));
                dic.put(PdfName.SUBFILTER, PdfName.ADBE_PKCS7_S4);
                dic.put(PdfName.RECIPIENTS, recipients);
            }
            else if (this.revision == 3 && this.encryptMetadata) {
                dic.put(PdfName.V, new PdfNumber(2));
                dic.put(PdfName.LENGTH, new PdfNumber(128));
                dic.put(PdfName.SUBFILTER, PdfName.ADBE_PKCS7_S4);
                dic.put(PdfName.RECIPIENTS, recipients);
            }
            else {
                if (this.revision == 5) {
                    dic.put(PdfName.R, new PdfNumber(5));
                    dic.put(PdfName.V, new PdfNumber(5));
                }
                else {
                    dic.put(PdfName.R, new PdfNumber(4));
                    dic.put(PdfName.V, new PdfNumber(4));
                }
                dic.put(PdfName.SUBFILTER, PdfName.ADBE_PKCS7_S5);
                final PdfDictionary stdcf = new PdfDictionary();
                stdcf.put(PdfName.RECIPIENTS, recipients);
                if (!this.encryptMetadata) {
                    stdcf.put(PdfName.ENCRYPTMETADATA, PdfBoolean.PDFFALSE);
                }
                if (this.revision == 4) {
                    stdcf.put(PdfName.CFM, PdfName.AESV2);
                    stdcf.put(PdfName.LENGTH, new PdfNumber(128));
                }
                else if (this.revision == 5) {
                    stdcf.put(PdfName.CFM, PdfName.AESV3);
                    stdcf.put(PdfName.LENGTH, new PdfNumber(256));
                }
                else {
                    stdcf.put(PdfName.CFM, PdfName.V2);
                }
                final PdfDictionary cf = new PdfDictionary();
                cf.put(PdfName.DEFAULTCRYPTFILTER, stdcf);
                dic.put(PdfName.CF, cf);
                if (this.embeddedFilesOnly) {
                    dic.put(PdfName.EFF, PdfName.DEFAULTCRYPTFILTER);
                    dic.put(PdfName.STRF, PdfName.IDENTITY);
                    dic.put(PdfName.STMF, PdfName.IDENTITY);
                }
                else {
                    dic.put(PdfName.STRF, PdfName.DEFAULTCRYPTFILTER);
                    dic.put(PdfName.STMF, PdfName.DEFAULTCRYPTFILTER);
                }
            }
            MessageDigest md = null;
            byte[] encodedRecipient = null;
            try {
                if (this.revision == 5) {
                    md = MessageDigest.getInstance("SHA-256");
                }
                else {
                    md = MessageDigest.getInstance("SHA-1");
                }
                md.update(this.publicKeyHandler.getSeed());
                for (int i = 0; i < this.publicKeyHandler.getRecipientsSize(); ++i) {
                    encodedRecipient = this.publicKeyHandler.getEncodedRecipient(i);
                    md.update(encodedRecipient);
                }
                if (!this.encryptMetadata) {
                    md.update(new byte[] { -1, -1, -1, -1 });
                }
            }
            catch (Exception f2) {
                throw new ExceptionConverter(f2);
            }
            final byte[] mdResult = md.digest();
            if (this.revision == 5) {
                this.key = mdResult;
            }
            else {
                this.setupByEncryptionKey(mdResult, this.keyLength);
            }
        }
        else {
            dic.put(PdfName.FILTER, PdfName.STANDARD);
            dic.put(PdfName.O, new PdfLiteral(StringUtils.escapeString(this.ownerKey)));
            dic.put(PdfName.U, new PdfLiteral(StringUtils.escapeString(this.userKey)));
            dic.put(PdfName.P, new PdfNumber(this.permissions));
            dic.put(PdfName.R, new PdfNumber(this.revision));
            if (this.revision == 2) {
                dic.put(PdfName.V, new PdfNumber(1));
            }
            else if (this.revision == 3 && this.encryptMetadata) {
                dic.put(PdfName.V, new PdfNumber(2));
                dic.put(PdfName.LENGTH, new PdfNumber(128));
            }
            else if (this.revision == 5) {
                if (!this.encryptMetadata) {
                    dic.put(PdfName.ENCRYPTMETADATA, PdfBoolean.PDFFALSE);
                }
                dic.put(PdfName.OE, new PdfLiteral(StringUtils.escapeString(this.oeKey)));
                dic.put(PdfName.UE, new PdfLiteral(StringUtils.escapeString(this.ueKey)));
                dic.put(PdfName.PERMS, new PdfLiteral(StringUtils.escapeString(this.perms)));
                dic.put(PdfName.V, new PdfNumber(this.revision));
                dic.put(PdfName.LENGTH, new PdfNumber(256));
                final PdfDictionary stdcf2 = new PdfDictionary();
                stdcf2.put(PdfName.LENGTH, new PdfNumber(32));
                if (this.embeddedFilesOnly) {
                    stdcf2.put(PdfName.AUTHEVENT, PdfName.EFOPEN);
                    dic.put(PdfName.EFF, PdfName.STDCF);
                    dic.put(PdfName.STRF, PdfName.IDENTITY);
                    dic.put(PdfName.STMF, PdfName.IDENTITY);
                }
                else {
                    stdcf2.put(PdfName.AUTHEVENT, PdfName.DOCOPEN);
                    dic.put(PdfName.STRF, PdfName.STDCF);
                    dic.put(PdfName.STMF, PdfName.STDCF);
                }
                stdcf2.put(PdfName.CFM, PdfName.AESV3);
                final PdfDictionary cf2 = new PdfDictionary();
                cf2.put(PdfName.STDCF, stdcf2);
                dic.put(PdfName.CF, cf2);
            }
            else {
                if (!this.encryptMetadata) {
                    dic.put(PdfName.ENCRYPTMETADATA, PdfBoolean.PDFFALSE);
                }
                dic.put(PdfName.R, new PdfNumber(4));
                dic.put(PdfName.V, new PdfNumber(4));
                dic.put(PdfName.LENGTH, new PdfNumber(128));
                final PdfDictionary stdcf2 = new PdfDictionary();
                stdcf2.put(PdfName.LENGTH, new PdfNumber(16));
                if (this.embeddedFilesOnly) {
                    stdcf2.put(PdfName.AUTHEVENT, PdfName.EFOPEN);
                    dic.put(PdfName.EFF, PdfName.STDCF);
                    dic.put(PdfName.STRF, PdfName.IDENTITY);
                    dic.put(PdfName.STMF, PdfName.IDENTITY);
                }
                else {
                    stdcf2.put(PdfName.AUTHEVENT, PdfName.DOCOPEN);
                    dic.put(PdfName.STRF, PdfName.STDCF);
                    dic.put(PdfName.STMF, PdfName.STDCF);
                }
                if (this.revision == 4) {
                    stdcf2.put(PdfName.CFM, PdfName.AESV2);
                }
                else {
                    stdcf2.put(PdfName.CFM, PdfName.V2);
                }
                final PdfDictionary cf2 = new PdfDictionary();
                cf2.put(PdfName.STDCF, stdcf2);
                dic.put(PdfName.CF, cf2);
            }
        }
        return dic;
    }
    
    public PdfObject getFileID(final boolean modified) throws IOException {
        return createInfoId(this.documentID, modified);
    }
    
    public OutputStreamEncryption getEncryptionStream(final OutputStream os) {
        return new OutputStreamEncryption(os, this.key, 0, this.keySize, this.revision);
    }
    
    public int calculateStreamSize(final int n) {
        if (this.revision == 4 || this.revision == 5) {
            return (n & 0x7FFFFFF0) + 32;
        }
        return n;
    }
    
    public byte[] encryptByteArray(final byte[] b) {
        try {
            final ByteArrayOutputStream ba = new ByteArrayOutputStream();
            final OutputStreamEncryption os2 = this.getEncryptionStream(ba);
            os2.write(b);
            os2.finish();
            return ba.toByteArray();
        }
        catch (IOException ex) {
            throw new ExceptionConverter(ex);
        }
    }
    
    public StandardDecryption getDecryptor() {
        return new StandardDecryption(this.key, 0, this.keySize, this.revision);
    }
    
    public byte[] decryptByteArray(final byte[] b) {
        try {
            final ByteArrayOutputStream ba = new ByteArrayOutputStream();
            final StandardDecryption dec = this.getDecryptor();
            byte[] b2 = dec.update(b, 0, b.length);
            if (b2 != null) {
                ba.write(b2);
            }
            b2 = dec.finish();
            if (b2 != null) {
                ba.write(b2);
            }
            return ba.toByteArray();
        }
        catch (IOException ex) {
            throw new ExceptionConverter(ex);
        }
    }
    
    public void addRecipient(final Certificate cert, final int permission) {
        this.documentID = createDocumentId();
        this.publicKeyHandler.addRecipient(new PdfPublicKeyRecipient(cert, permission));
    }
    
    public byte[] computeUserPassword(final byte[] ownerPassword) {
        byte[] userPad = null;
        if (this.publicKeyHandler.getRecipientsSize() == 0 && 2 <= this.revision && this.revision <= 4) {
            userPad = this.computeOwnerKey(this.ownerKey, this.padPassword(ownerPassword));
            for (int i = 0; i < userPad.length; ++i) {
                boolean match = true;
                for (int j = 0; j < userPad.length - i; ++j) {
                    if (userPad[i + j] != PdfEncryption.pad[j]) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    final byte[] userPassword = new byte[i];
                    System.arraycopy(userPad, 0, userPassword, 0, i);
                    return userPassword;
                }
            }
        }
        return userPad;
    }
    
    static {
        pad = new byte[] { 40, -65, 78, 94, 78, 117, -118, 65, 100, 0, 78, 86, -1, -6, 1, 8, 46, 46, 0, -74, -48, 104, 62, -128, 47, 12, -87, -2, 100, 83, 105, 122 };
        salt = new byte[] { 115, 65, 108, 84 };
        metadataPad = new byte[] { -1, -1, -1, -1 };
        PdfEncryption.seq = System.currentTimeMillis();
    }
}
