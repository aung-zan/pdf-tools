// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.log.LoggerFactory;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.net.HttpURLConnection;
import java.util.Collection;
import java.net.MalformedURLException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.security.cert.Certificate;
import java.util.Iterator;
import java.util.ArrayList;
import java.net.URL;
import java.util.List;
import com.itextpdf.text.log.Logger;

public class CrlClientOnline implements CrlClient
{
    private static final Logger LOGGER;
    protected List<URL> urls;
    
    public CrlClientOnline() {
        this.urls = new ArrayList<URL>();
    }
    
    public CrlClientOnline(final String... crls) {
        this.urls = new ArrayList<URL>();
        for (final String url : crls) {
            this.addUrl(url);
        }
    }
    
    public CrlClientOnline(final URL... crls) {
        this.urls = new ArrayList<URL>();
        for (final URL url : this.urls) {
            this.addUrl(url);
        }
    }
    
    public CrlClientOnline(final Certificate[] chain) {
        this.urls = new ArrayList<URL>();
        for (int i = 0; i < chain.length; ++i) {
            final X509Certificate cert = (X509Certificate)chain[i];
            CrlClientOnline.LOGGER.info("Checking certificate: " + cert.getSubjectDN());
            try {
                this.addUrl(CertificateUtil.getCRLURL(cert));
            }
            catch (CertificateParsingException e) {
                CrlClientOnline.LOGGER.info("Skipped CRL url (certificate could not be parsed)");
            }
        }
    }
    
    protected void addUrl(final String url) {
        try {
            this.addUrl(new URL(url));
        }
        catch (MalformedURLException e) {
            CrlClientOnline.LOGGER.info("Skipped CRL url (malformed): " + url);
        }
    }
    
    protected void addUrl(final URL url) {
        if (this.urls.contains(url)) {
            CrlClientOnline.LOGGER.info("Skipped CRL url (duplicate): " + url);
            return;
        }
        this.urls.add(url);
        CrlClientOnline.LOGGER.info("Added CRL url: " + url);
    }
    
    @Override
    public Collection<byte[]> getEncoded(final X509Certificate checkCert, String url) {
        if (checkCert == null) {
            return null;
        }
        final List<URL> urllist = new ArrayList<URL>(this.urls);
        if (urllist.size() == 0) {
            CrlClientOnline.LOGGER.info("Looking for CRL for certificate " + checkCert.getSubjectDN());
            try {
                if (url == null) {
                    url = CertificateUtil.getCRLURL(checkCert);
                }
                if (url == null) {
                    throw new NullPointerException();
                }
                urllist.add(new URL(url));
                CrlClientOnline.LOGGER.info("Found CRL url: " + url);
            }
            catch (Exception e) {
                CrlClientOnline.LOGGER.info("Skipped CRL url: " + e.getMessage());
            }
        }
        final ArrayList<byte[]> ar = new ArrayList<byte[]>();
        for (final URL urlt : urllist) {
            try {
                CrlClientOnline.LOGGER.info("Checking CRL: " + urlt);
                final HttpURLConnection con = (HttpURLConnection)urlt.openConnection();
                if (con.getResponseCode() / 100 != 2) {
                    throw new IOException(MessageLocalization.getComposedMessage("invalid.http.response.1", con.getResponseCode()));
                }
                final InputStream inp = (InputStream)con.getContent();
                final byte[] buf = new byte[1024];
                final ByteArrayOutputStream bout = new ByteArrayOutputStream();
                while (true) {
                    final int n = inp.read(buf, 0, buf.length);
                    if (n <= 0) {
                        break;
                    }
                    bout.write(buf, 0, n);
                }
                inp.close();
                ar.add(bout.toByteArray());
                CrlClientOnline.LOGGER.info("Added CRL found at: " + urlt);
            }
            catch (Exception e2) {
                CrlClientOnline.LOGGER.info("Skipped CRL: " + e2.getMessage() + " for " + urlt);
            }
        }
        return ar;
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(CrlClientOnline.class);
    }
}
