// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.DocumentException;
import java.security.MessageDigest;
import java.io.InputStream;
import com.itextpdf.text.pdf.PdfString;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfSignature;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfDeveloperExtension;
import com.itextpdf.text.pdf.PdfSignatureAppearance;

public class LtvTimestamp
{
    public static void timestamp(final PdfSignatureAppearance sap, final TSAClient tsa, final String signatureName) throws IOException, DocumentException, GeneralSecurityException {
        final int contentEstimated = tsa.getTokenSizeEstimate();
        sap.addDeveloperExtension(PdfDeveloperExtension.ESIC_1_7_EXTENSIONLEVEL5);
        sap.setVisibleSignature(new Rectangle(0.0f, 0.0f, 0.0f, 0.0f), 1, signatureName);
        final PdfSignature dic = new PdfSignature(PdfName.ADOBE_PPKLITE, PdfName.ETSI_RFC3161);
        dic.put(PdfName.TYPE, PdfName.DOCTIMESTAMP);
        sap.setCryptoDictionary(dic);
        final HashMap<PdfName, Integer> exc = new HashMap<PdfName, Integer>();
        exc.put(PdfName.CONTENTS, new Integer(contentEstimated * 2 + 2));
        sap.preClose(exc);
        final InputStream data = sap.getRangeStream();
        final MessageDigest messageDigest = tsa.getMessageDigest();
        final byte[] buf = new byte[4096];
        int n;
        while ((n = data.read(buf)) > 0) {
            messageDigest.update(buf, 0, n);
        }
        final byte[] tsImprint = messageDigest.digest();
        byte[] tsToken;
        try {
            tsToken = tsa.getTimeStampToken(tsImprint);
        }
        catch (Exception e) {
            throw new GeneralSecurityException(e);
        }
        if (contentEstimated + 2 < tsToken.length) {
            throw new IOException("Not enough space");
        }
        final byte[] paddedSig = new byte[contentEstimated];
        System.arraycopy(tsToken, 0, paddedSig, 0, tsToken.length);
        final PdfDictionary dic2 = new PdfDictionary();
        dic2.put(PdfName.CONTENTS, new PdfString(paddedSig).setHexWriting(true));
        sap.close(dic2);
    }
}
