// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.security.cert.X509Certificate;
import java.security.cert.Certificate;
import java.security.GeneralSecurityException;

public class VerificationException extends GeneralSecurityException
{
    private static final long serialVersionUID = 2978604513926438256L;
    
    public VerificationException(final Certificate cert, final String message) {
        super(String.format("Certificate %s failed: %s", (cert == null) ? "Unknown" : ((X509Certificate)cert).getSubjectDN().getName(), message));
    }
}
