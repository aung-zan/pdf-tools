// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.util.ArrayList;
import java.util.List;
import com.itextpdf.text.pdf.PdfIndirectReference;
import com.itextpdf.text.pdf.PdfStream;
import com.itextpdf.text.pdf.PdfDeveloperExtension;
import com.itextpdf.text.pdf.PRIndirectReference;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfObject;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.asn1.ASN1Primitive;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.Utilities;
import java.io.InputStream;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Enumerated;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.DEROctetString;
import java.security.GeneralSecurityException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Collection;
import java.security.cert.Certificate;
import java.util.Arrays;
import java.security.cert.X509Certificate;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.util.HashMap;
import com.itextpdf.text.log.LoggerFactory;
import com.itextpdf.text.pdf.PdfName;
import java.util.Map;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.log.Logger;

public class LtvVerification
{
    private Logger LOGGER;
    private PdfStamper stp;
    private PdfWriter writer;
    private PdfReader reader;
    private AcroFields acroFields;
    private Map<PdfName, ValidationData> validated;
    private boolean used;
    
    public LtvVerification(final PdfStamper stp) {
        this.LOGGER = LoggerFactory.getLogger(LtvVerification.class);
        this.validated = new HashMap<PdfName, ValidationData>();
        this.used = false;
        this.stp = stp;
        this.writer = stp.getWriter();
        this.reader = stp.getReader();
        this.acroFields = stp.getAcroFields();
    }
    
    public boolean addVerification(final String signatureName, final OcspClient ocsp, final CrlClient crl, final CertificateOption certOption, final Level level, final CertificateInclusion certInclude) throws IOException, GeneralSecurityException {
        if (this.used) {
            throw new IllegalStateException(MessageLocalization.getComposedMessage("verification.already.output", new Object[0]));
        }
        final PdfPKCS7 pk = this.acroFields.verifySignature(signatureName);
        this.LOGGER.info("Adding verification for " + signatureName);
        final Certificate[] xc = pk.getCertificates();
        final X509Certificate signingCert = pk.getSigningCertificate();
        final ValidationData vd = new ValidationData();
        for (int k = 0; k < xc.length; ++k) {
            final X509Certificate cert = (X509Certificate)xc[k];
            this.LOGGER.info("Certificate: " + cert.getSubjectDN());
            if (certOption != CertificateOption.SIGNING_CERTIFICATE || cert.equals(signingCert)) {
                byte[] ocspEnc = null;
                if (ocsp != null && level != Level.CRL) {
                    ocspEnc = ocsp.getEncoded(cert, this.getParent(cert, xc), null);
                    if (ocspEnc != null) {
                        vd.ocsps.add(buildOCSPResponse(ocspEnc));
                        this.LOGGER.info("OCSP added");
                    }
                }
                if (crl != null && (level == Level.CRL || level == Level.OCSP_CRL || (level == Level.OCSP_OPTIONAL_CRL && ocspEnc == null))) {
                    final Collection<byte[]> cims = crl.getEncoded(cert, null);
                    if (cims != null) {
                        for (final byte[] cim : cims) {
                            boolean dup = false;
                            for (final byte[] b : vd.crls) {
                                if (Arrays.equals(b, cim)) {
                                    dup = true;
                                    break;
                                }
                            }
                            if (!dup) {
                                vd.crls.add(cim);
                                this.LOGGER.info("CRL added");
                            }
                        }
                    }
                }
                if (certInclude == CertificateInclusion.YES) {
                    vd.certs.add(cert.getEncoded());
                }
            }
        }
        if (vd.crls.isEmpty() && vd.ocsps.isEmpty()) {
            return false;
        }
        this.validated.put(this.getSignatureHashKey(signatureName), vd);
        return true;
    }
    
    private X509Certificate getParent(final X509Certificate cert, final Certificate[] certs) {
        for (int i = 0; i < certs.length; ++i) {
            final X509Certificate parent = (X509Certificate)certs[i];
            if (cert.getIssuerDN().equals(parent.getSubjectDN())) {
                try {
                    cert.verify(parent.getPublicKey());
                    return parent;
                }
                catch (Exception ex) {}
            }
        }
        return null;
    }
    
    public boolean addVerification(final String signatureName, final Collection<byte[]> ocsps, final Collection<byte[]> crls, final Collection<byte[]> certs) throws IOException, GeneralSecurityException {
        if (this.used) {
            throw new IllegalStateException(MessageLocalization.getComposedMessage("verification.already.output", new Object[0]));
        }
        final ValidationData vd = new ValidationData();
        if (ocsps != null) {
            for (final byte[] ocsp : ocsps) {
                vd.ocsps.add(buildOCSPResponse(ocsp));
            }
        }
        if (crls != null) {
            for (final byte[] crl : crls) {
                vd.crls.add(crl);
            }
        }
        if (certs != null) {
            for (final byte[] cert : certs) {
                vd.certs.add(cert);
            }
        }
        this.validated.put(this.getSignatureHashKey(signatureName), vd);
        return true;
    }
    
    private static byte[] buildOCSPResponse(final byte[] BasicOCSPResponse) throws IOException {
        final DEROctetString doctet = new DEROctetString(BasicOCSPResponse);
        final ASN1EncodableVector v2 = new ASN1EncodableVector();
        v2.add(OCSPObjectIdentifiers.id_pkix_ocsp_basic);
        v2.add(doctet);
        final ASN1Enumerated den = new ASN1Enumerated(0);
        final ASN1EncodableVector v3 = new ASN1EncodableVector();
        v3.add(den);
        v3.add(new DERTaggedObject(true, 0, new DERSequence(v2)));
        final DERSequence seq = new DERSequence(v3);
        return seq.getEncoded();
    }
    
    private PdfName getSignatureHashKey(final String signatureName) throws NoSuchAlgorithmException, IOException {
        final PdfDictionary dic = this.acroFields.getSignatureDictionary(signatureName);
        final PdfString contents = dic.getAsString(PdfName.CONTENTS);
        byte[] bc = null;
        if (!this.reader.isEncrypted()) {
            bc = contents.getOriginalBytes();
        }
        else {
            bc = contents.getBytes();
        }
        byte[] bt = null;
        if (PdfName.ETSI_RFC3161.equals(PdfReader.getPdfObject(dic.get(PdfName.SUBFILTER)))) {
            final ASN1InputStream din = new ASN1InputStream(new ByteArrayInputStream(bc));
            final ASN1Primitive pkcs = din.readObject();
            bc = pkcs.getEncoded();
        }
        bt = hashBytesSha1(bc);
        return new PdfName(Utilities.convertToHex(bt));
    }
    
    private static byte[] hashBytesSha1(final byte[] b) throws NoSuchAlgorithmException {
        final MessageDigest sh = MessageDigest.getInstance("SHA1");
        return sh.digest(b);
    }
    
    public void merge() throws IOException {
        if (this.used || this.validated.isEmpty()) {
            return;
        }
        this.used = true;
        final PdfDictionary catalog = this.reader.getCatalog();
        final PdfObject dss = catalog.get(PdfName.DSS);
        if (dss == null) {
            this.createDss();
        }
        else {
            this.updateDss();
        }
    }
    
    private void updateDss() throws IOException {
        final PdfDictionary catalog = this.reader.getCatalog();
        this.stp.markUsed(catalog);
        final PdfDictionary dss = catalog.getAsDict(PdfName.DSS);
        PdfArray ocsps = dss.getAsArray(PdfName.OCSPS);
        PdfArray crls = dss.getAsArray(PdfName.CRLS);
        PdfArray certs = dss.getAsArray(PdfName.CERTS);
        dss.remove(PdfName.OCSPS);
        dss.remove(PdfName.CRLS);
        dss.remove(PdfName.CERTS);
        final PdfDictionary vrim = dss.getAsDict(PdfName.VRI);
        if (vrim != null) {
            for (final PdfName n : vrim.getKeys()) {
                if (this.validated.containsKey(n)) {
                    final PdfDictionary vri = vrim.getAsDict(n);
                    if (vri == null) {
                        continue;
                    }
                    deleteOldReferences(ocsps, vri.getAsArray(PdfName.OCSP));
                    deleteOldReferences(crls, vri.getAsArray(PdfName.CRL));
                    deleteOldReferences(certs, vri.getAsArray(PdfName.CERT));
                }
            }
        }
        if (ocsps == null) {
            ocsps = new PdfArray();
        }
        if (crls == null) {
            crls = new PdfArray();
        }
        if (certs == null) {
            certs = new PdfArray();
        }
        this.outputDss(dss, vrim, ocsps, crls, certs);
    }
    
    private static void deleteOldReferences(final PdfArray all, final PdfArray toDelete) {
        if (all == null || toDelete == null) {
            return;
        }
        for (final PdfObject pi : toDelete) {
            if (!pi.isIndirect()) {
                continue;
            }
            final PRIndirectReference pir = (PRIndirectReference)pi;
            for (int k = 0; k < all.size(); ++k) {
                final PdfObject po = all.getPdfObject(k);
                if (po.isIndirect()) {
                    final PRIndirectReference pod = (PRIndirectReference)po;
                    if (pir.getNumber() == pod.getNumber()) {
                        all.remove(k);
                        --k;
                    }
                }
            }
        }
    }
    
    private void createDss() throws IOException {
        this.outputDss(new PdfDictionary(), new PdfDictionary(), new PdfArray(), new PdfArray(), new PdfArray());
    }
    
    private void outputDss(final PdfDictionary dss, final PdfDictionary vrim, final PdfArray ocsps, final PdfArray crls, final PdfArray certs) throws IOException {
        this.writer.addDeveloperExtension(PdfDeveloperExtension.ESIC_1_7_EXTENSIONLEVEL5);
        final PdfDictionary catalog = this.reader.getCatalog();
        this.stp.markUsed(catalog);
        for (final PdfName vkey : this.validated.keySet()) {
            final PdfArray ocsp = new PdfArray();
            final PdfArray crl = new PdfArray();
            final PdfArray cert = new PdfArray();
            final PdfDictionary vri = new PdfDictionary();
            for (final byte[] b : this.validated.get(vkey).crls) {
                final PdfStream ps = new PdfStream(b);
                ps.flateCompress();
                final PdfIndirectReference iref = this.writer.addToBody(ps, false).getIndirectReference();
                crl.add(iref);
                crls.add(iref);
            }
            for (final byte[] b : this.validated.get(vkey).ocsps) {
                final PdfStream ps = new PdfStream(b);
                ps.flateCompress();
                final PdfIndirectReference iref = this.writer.addToBody(ps, false).getIndirectReference();
                ocsp.add(iref);
                ocsps.add(iref);
            }
            for (final byte[] b : this.validated.get(vkey).certs) {
                final PdfStream ps = new PdfStream(b);
                ps.flateCompress();
                final PdfIndirectReference iref = this.writer.addToBody(ps, false).getIndirectReference();
                cert.add(iref);
                certs.add(iref);
            }
            if (ocsp.size() > 0) {
                vri.put(PdfName.OCSP, this.writer.addToBody(ocsp, false).getIndirectReference());
            }
            if (crl.size() > 0) {
                vri.put(PdfName.CRL, this.writer.addToBody(crl, false).getIndirectReference());
            }
            if (cert.size() > 0) {
                vri.put(PdfName.CERT, this.writer.addToBody(cert, false).getIndirectReference());
            }
            vrim.put(vkey, this.writer.addToBody(vri, false).getIndirectReference());
        }
        dss.put(PdfName.VRI, this.writer.addToBody(vrim, false).getIndirectReference());
        if (ocsps.size() > 0) {
            dss.put(PdfName.OCSPS, this.writer.addToBody(ocsps, false).getIndirectReference());
        }
        if (crls.size() > 0) {
            dss.put(PdfName.CRLS, this.writer.addToBody(crls, false).getIndirectReference());
        }
        if (certs.size() > 0) {
            dss.put(PdfName.CERTS, this.writer.addToBody(certs, false).getIndirectReference());
        }
        catalog.put(PdfName.DSS, this.writer.addToBody(dss, false).getIndirectReference());
    }
    
    public enum Level
    {
        OCSP, 
        CRL, 
        OCSP_CRL, 
        OCSP_OPTIONAL_CRL;
    }
    
    public enum CertificateOption
    {
        SIGNING_CERTIFICATE, 
        WHOLE_CHAIN;
    }
    
    public enum CertificateInclusion
    {
        YES, 
        NO;
    }
    
    private static class ValidationData
    {
        public List<byte[]> crls;
        public List<byte[]> ocsps;
        public List<byte[]> certs;
        
        private ValidationData() {
            this.crls = new ArrayList<byte[]>();
            this.ocsps = new ArrayList<byte[]>();
            this.certs = new ArrayList<byte[]>();
        }
    }
}
