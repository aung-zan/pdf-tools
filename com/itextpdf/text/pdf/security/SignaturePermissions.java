// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfName;
import java.util.Collection;
import java.util.ArrayList;
import com.itextpdf.text.pdf.PdfDictionary;
import java.util.List;

public class SignaturePermissions
{
    boolean certification;
    boolean fillInAllowed;
    boolean annotationsAllowed;
    List<FieldLock> fieldLocks;
    
    public SignaturePermissions(final PdfDictionary sigDict, final SignaturePermissions previous) {
        this.certification = false;
        this.fillInAllowed = true;
        this.annotationsAllowed = true;
        this.fieldLocks = new ArrayList<FieldLock>();
        if (previous != null) {
            this.annotationsAllowed &= previous.isAnnotationsAllowed();
            this.fillInAllowed &= previous.isFillInAllowed();
            this.fieldLocks.addAll(previous.getFieldLocks());
        }
        final PdfArray ref = sigDict.getAsArray(PdfName.REFERENCE);
        if (ref != null) {
            for (int i = 0; i < ref.size(); ++i) {
                final PdfDictionary dict = ref.getAsDict(i);
                final PdfDictionary params = dict.getAsDict(PdfName.TRANSFORMPARAMS);
                if (PdfName.DOCMDP.equals(dict.getAsName(PdfName.TRANSFORMMETHOD))) {
                    this.certification = true;
                }
                final PdfName action = params.getAsName(PdfName.ACTION);
                if (action != null) {
                    this.fieldLocks.add(new FieldLock(action, params.getAsArray(PdfName.FIELDS)));
                }
                final PdfNumber p = params.getAsNumber(PdfName.P);
                if (p != null) {
                    switch (p.intValue()) {
                        case 1: {
                            this.fillInAllowed &= false;
                        }
                        case 2: {
                            this.annotationsAllowed &= false;
                            break;
                        }
                    }
                }
            }
        }
    }
    
    public boolean isCertification() {
        return this.certification;
    }
    
    public boolean isFillInAllowed() {
        return this.fillInAllowed;
    }
    
    public boolean isAnnotationsAllowed() {
        return this.annotationsAllowed;
    }
    
    public List<FieldLock> getFieldLocks() {
        return this.fieldLocks;
    }
    
    public class FieldLock
    {
        PdfName action;
        PdfArray fields;
        
        public FieldLock(final PdfName action, final PdfArray fields) {
            this.action = action;
            this.fields = fields;
        }
        
        public PdfName getAction() {
            return this.action;
        }
        
        public PdfArray getFields() {
            return this.fields;
        }
        
        @Override
        public String toString() {
            return this.action.toString() + ((this.fields == null) ? "" : this.fields.toString());
        }
    }
}
