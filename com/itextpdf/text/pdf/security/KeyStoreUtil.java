// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.ExceptionConverter;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import java.security.KeyStore;

public class KeyStoreUtil
{
    public static KeyStore loadCacertsKeyStore(final String provider) {
        File file = new File(System.getProperty("java.home"), "lib");
        file = new File(file, "security");
        file = new File(file, "cacerts");
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(file);
            KeyStore k;
            if (provider == null) {
                k = KeyStore.getInstance("JKS");
            }
            else {
                k = KeyStore.getInstance("JKS", provider);
            }
            k.load(fin, null);
            return k;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
        finally {
            try {
                if (fin != null) {
                    fin.close();
                }
            }
            catch (Exception ex) {}
        }
    }
    
    public static KeyStore loadCacertsKeyStore() {
        return loadCacertsKeyStore(null);
    }
}
