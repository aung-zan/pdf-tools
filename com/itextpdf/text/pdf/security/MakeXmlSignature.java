// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import javax.xml.transform.Transformer;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.TransformerFactory;
import java.io.Writer;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import javax.xml.crypto.dsig.XMLSignContext;
import org.apache.jcp.xml.dsig.internal.dom.DOMReference;
import javax.xml.crypto.dom.DOMCryptoContext;
import javax.xml.crypto.XMLCryptoContext;
import org.apache.jcp.xml.dsig.internal.dom.DOMUtils;
import java.io.ByteArrayOutputStream;
import javax.xml.crypto.dsig.SignedInfo;
import org.apache.jcp.xml.dsig.internal.dom.DOMXMLSignature;
import java.security.Key;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.spec.XPathFilter2ParameterSpec;
import javax.xml.crypto.dsig.spec.XPathType;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.crypto.dsig.Transform;
import java.util.ArrayList;
import javax.xml.crypto.dsig.spec.DigestMethodParameterSpec;
import org.w3c.dom.Document;
import javax.xml.crypto.dom.DOMStructure;
import java.security.cert.X509Certificate;
import org.apache.xml.security.utils.Base64;
import java.text.SimpleDateFormat;
import java.security.MessageDigest;
import java.security.Provider;
import org.apache.jcp.xml.dsig.internal.dom.XMLDSigRI;
import java.util.UUID;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.XMLStructure;
import org.apache.jcp.xml.dsig.internal.dom.DOMKeyInfoFactory;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.util.Arrays;
import java.security.PublicKey;
import java.security.cert.Certificate;
import com.itextpdf.text.DocumentException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.XMLObject;
import javax.xml.crypto.dsig.Reference;
import java.util.List;
import java.util.Collections;
import javax.xml.crypto.dsig.spec.SignatureMethodParameterSpec;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import org.apache.jcp.xml.dsig.internal.dom.DOMSignedInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import com.itextpdf.text.pdf.XmlSignatureAppearance;

public class MakeXmlSignature
{
    public static void signXmlDSig(final XmlSignatureAppearance sap, final ExternalSignature externalSignature, final KeyInfo keyInfo) throws GeneralSecurityException, IOException, DocumentException {
        verifyArguments(sap, externalSignature);
        final XMLSignatureFactory fac = createSignatureFactory();
        final Reference reference = generateContentReference(fac, sap, null);
        String signatureMethod = null;
        if (externalSignature.getEncryptionAlgorithm().equals("RSA")) {
            signatureMethod = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
        }
        else if (externalSignature.getEncryptionAlgorithm().equals("DSA")) {
            signatureMethod = "http://www.w3.org/2000/09/xmldsig#dsa-sha1";
        }
        final DOMSignedInfo signedInfo = (DOMSignedInfo)fac.newSignedInfo(fac.newCanonicalizationMethod("http://www.w3.org/TR/2001/REC-xml-c14n-20010315", (C14NMethodParameterSpec)null), fac.newSignatureMethod(signatureMethod, null), Collections.singletonList(reference));
        sign(fac, externalSignature, sap.getXmlLocator(), signedInfo, null, keyInfo, null);
        sap.close();
    }
    
    public static void signXmlDSig(final XmlSignatureAppearance sap, final ExternalSignature externalSignature, final Certificate[] chain) throws DocumentException, GeneralSecurityException, IOException {
        signXmlDSig(sap, externalSignature, generateKeyInfo(chain, sap));
    }
    
    public static void signXmlDSig(final XmlSignatureAppearance sap, final ExternalSignature externalSignature, final PublicKey publicKey) throws GeneralSecurityException, DocumentException, IOException {
        signXmlDSig(sap, externalSignature, generateKeyInfo(publicKey));
    }
    
    public static void signXades(final XmlSignatureAppearance sap, final ExternalSignature externalSignature, final Certificate[] chain, final boolean includeSignaturePolicy) throws GeneralSecurityException, DocumentException, IOException {
        verifyArguments(sap, externalSignature);
        String signatureMethod = null;
        if (externalSignature.getEncryptionAlgorithm().equals("RSA")) {
            signatureMethod = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
        }
        else if (externalSignature.getEncryptionAlgorithm().equals("DSA")) {
            signatureMethod = "http://www.w3.org/2000/09/xmldsig#dsa-sha1";
        }
        final String contentReferenceId = "Reference-" + getRandomId();
        final String signedPropertiesId = "SignedProperties-" + getRandomId();
        final String signatureId = "Signature-" + getRandomId();
        final XMLSignatureFactory fac = createSignatureFactory();
        final KeyInfo keyInfo = generateKeyInfo(chain, sap);
        String[] signaturePolicy = null;
        if (includeSignaturePolicy) {
            signaturePolicy = new String[2];
            if (signatureMethod.equals("http://www.w3.org/2000/09/xmldsig#rsa-sha1")) {
                signaturePolicy[0] = "urn:oid:1.2.840.113549.1.1.5";
                signaturePolicy[1] = "RSA (PKCS #1 v1.5) with SHA-1 signature";
            }
            else {
                signaturePolicy[0] = "urn:oid:1.2.840.10040.4.3";
                signaturePolicy[1] = "ANSI X9.57 DSA signature generated with SHA-1 hash (DSA x9.30)";
            }
        }
        final XMLObject xmlObject = generateXadesObject(fac, sap, signatureId, contentReferenceId, signedPropertiesId, signaturePolicy);
        final Reference contentReference = generateContentReference(fac, sap, contentReferenceId);
        final Reference signedPropertiesReference = generateCustomReference(fac, "#" + signedPropertiesId, "http://uri.etsi.org/01903#SignedProperties", null);
        final List<Reference> references = Arrays.asList(signedPropertiesReference, contentReference);
        final DOMSignedInfo signedInfo = (DOMSignedInfo)fac.newSignedInfo(fac.newCanonicalizationMethod("http://www.w3.org/TR/2001/REC-xml-c14n-20010315", (C14NMethodParameterSpec)null), fac.newSignatureMethod(signatureMethod, null), references, null);
        sign(fac, externalSignature, sap.getXmlLocator(), signedInfo, xmlObject, keyInfo, signatureId);
        sap.close();
    }
    
    public static void signXadesBes(final XmlSignatureAppearance sap, final ExternalSignature externalSignature, final Certificate[] chain) throws GeneralSecurityException, DocumentException, IOException {
        signXades(sap, externalSignature, chain, false);
    }
    
    public static void signXadesEpes(final XmlSignatureAppearance sap, final ExternalSignature externalSignature, final Certificate[] chain) throws GeneralSecurityException, DocumentException, IOException {
        signXades(sap, externalSignature, chain, true);
    }
    
    private static void verifyArguments(final XmlSignatureAppearance sap, final ExternalSignature externalSignature) throws DocumentException {
        if (sap.getXmlLocator() == null) {
            throw new DocumentException(MessageLocalization.getComposedMessage("xmllocator.cannot.be.null", new Object[0]));
        }
        if (!externalSignature.getHashAlgorithm().equals("SHA1")) {
            throw new UnsupportedOperationException(MessageLocalization.getComposedMessage("support.only.sha1.hash.algorithm", new Object[0]));
        }
        if (!externalSignature.getEncryptionAlgorithm().equals("RSA") && !externalSignature.getEncryptionAlgorithm().equals("DSA")) {
            throw new UnsupportedOperationException(MessageLocalization.getComposedMessage("support.only.rsa.and.dsa.algorithms", new Object[0]));
        }
    }
    
    private static Element findElement(final NodeList nodes, final String localName) {
        for (int i = nodes.getLength() - 1; i >= 0; --i) {
            final Node currNode = nodes.item(i);
            if (currNode.getNodeType() == 1 && currNode.getLocalName().equals(localName)) {
                return (Element)currNode;
            }
        }
        return null;
    }
    
    private static KeyInfo generateKeyInfo(final Certificate[] chain, final XmlSignatureAppearance sap) {
        final Certificate certificate = chain[0];
        sap.setCertificate(certificate);
        final KeyInfoFactory kif = (KeyInfoFactory)new DOMKeyInfoFactory();
        final X509Data x509d = kif.newX509Data(Collections.singletonList(certificate));
        return kif.newKeyInfo(Collections.singletonList(x509d));
    }
    
    private static KeyInfo generateKeyInfo(final PublicKey publicKey) throws GeneralSecurityException {
        final KeyInfoFactory kif = (KeyInfoFactory)new DOMKeyInfoFactory();
        final KeyValue kv = kif.newKeyValue(publicKey);
        return kif.newKeyInfo(Collections.singletonList(kv));
    }
    
    private static String getRandomId() {
        return UUID.randomUUID().toString().substring(24);
    }
    
    private static XMLSignatureFactory createSignatureFactory() {
        return XMLSignatureFactory.getInstance("DOM", (Provider)new XMLDSigRI());
    }
    
    private static XMLObject generateXadesObject(final XMLSignatureFactory fac, final XmlSignatureAppearance sap, final String signatureId, final String contentReferenceId, final String signedPropertiesId, final String[] signaturePolicy) throws GeneralSecurityException {
        final MessageDigest md = MessageDigest.getInstance("SHA1");
        final Certificate cert = sap.getCertificate();
        final Document doc = sap.getXmlLocator().getDocument();
        final Element QualifyingProperties = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:QualifyingProperties");
        QualifyingProperties.setAttribute("Target", "#" + signatureId);
        final Element SignedProperties = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:SignedProperties");
        SignedProperties.setAttribute("Id", signedPropertiesId);
        SignedProperties.setIdAttribute("Id", true);
        final Element SignedSignatureProperties = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:SignedSignatureProperties");
        final Element SigningTime = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:SigningTime");
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        String result = sdf.format(sap.getSignDate().getTime());
        result = result.substring(0, result.length() - 2).concat(":").concat(result.substring(result.length() - 2));
        SigningTime.appendChild(doc.createTextNode(result));
        SignedSignatureProperties.appendChild(SigningTime);
        final Element SigningCertificate = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:SigningCertificate");
        final Element Cert = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:Cert");
        final Element CertDigest = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:CertDigest");
        Element DigestMethod = doc.createElementNS("http://www.w3.org/2000/09/xmldsig#", "DigestMethod");
        DigestMethod.setAttribute("Algorithm", "http://www.w3.org/2000/09/xmldsig#sha1");
        CertDigest.appendChild(DigestMethod);
        Element DigestValue = doc.createElementNS("http://www.w3.org/2000/09/xmldsig#", "DigestValue");
        DigestValue.appendChild(doc.createTextNode(Base64.encode(md.digest(cert.getEncoded()))));
        CertDigest.appendChild(DigestValue);
        Cert.appendChild(CertDigest);
        if (cert instanceof X509Certificate) {
            final Element IssueSerial = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:IssuerSerial");
            final Element X509IssuerName = doc.createElementNS("http://www.w3.org/2000/09/xmldsig#", "X509IssuerName");
            X509IssuerName.appendChild(doc.createTextNode(getX509IssuerName((X509Certificate)cert)));
            IssueSerial.appendChild(X509IssuerName);
            final Element X509SerialNumber = doc.createElementNS("http://www.w3.org/2000/09/xmldsig#", "X509SerialNumber");
            X509SerialNumber.appendChild(doc.createTextNode(getX509SerialNumber((X509Certificate)cert)));
            IssueSerial.appendChild(X509SerialNumber);
            Cert.appendChild(IssueSerial);
        }
        SigningCertificate.appendChild(Cert);
        SignedSignatureProperties.appendChild(SigningCertificate);
        if (signaturePolicy != null) {
            final Element SignaturePolicyIdentifier = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:SignaturePolicyIdentifier");
            final Element SignaturePolicyId = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:SignaturePolicyId");
            final Element SigPolicyId = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:SigPolicyId");
            final Element Identifier = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:Identifier");
            Identifier.appendChild(doc.createTextNode(signaturePolicy[0]));
            Identifier.setAttribute("Qualifier", "OIDAsURN");
            SigPolicyId.appendChild(Identifier);
            final Element Description = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:Description");
            Description.appendChild(doc.createTextNode(signaturePolicy[1]));
            SigPolicyId.appendChild(Description);
            SignaturePolicyId.appendChild(SigPolicyId);
            final Element SigPolicyHash = doc.createElementNS("http://uri.etsi.org/01903/v1.3.2#", "xades:SigPolicyHash");
            DigestMethod = doc.createElementNS("http://www.w3.org/2000/09/xmldsig#", "DigestMethod");
            DigestMethod.setAttribute("Algorithm", "http://www.w3.org/2000/09/xmldsig#sha1");
            SigPolicyHash.appendChild(DigestMethod);
            DigestValue = doc.createElementNS("http://www.w3.org/2000/09/xmldsig#", "DigestValue");
            final byte[] policyIdContent = getByteArrayOfNode(SigPolicyId);
            DigestValue.appendChild(doc.createTextNode(Base64.encode(md.digest(policyIdContent))));
            SigPolicyHash.appendChild(DigestValue);
            SignaturePolicyId.appendChild(SigPolicyHash);
            SignaturePolicyIdentifier.appendChild(SignaturePolicyId);
            SignedSignatureProperties.appendChild(SignaturePolicyIdentifier);
        }
        SignedProperties.appendChild(SignedSignatureProperties);
        final Element SignedDataObjectProperties = doc.createElement("xades:SignedDataObjectProperties");
        final Element DataObjectFormat = doc.createElement("xades:DataObjectFormat");
        DataObjectFormat.setAttribute("ObjectReference", "#" + contentReferenceId);
        final String descr = sap.getDescription();
        if (descr != null) {
            final Element Description2 = doc.createElement("xades:Description");
            Description2.appendChild(doc.createTextNode(descr));
            DataObjectFormat.appendChild(Description2);
        }
        final Element MimeType = doc.createElement("xades:MimeType");
        MimeType.appendChild(doc.createTextNode(sap.getMimeType()));
        DataObjectFormat.appendChild(MimeType);
        final String enc = sap.getXmlLocator().getEncoding();
        if (enc != null) {
            final Element Encoding = doc.createElement("xades:Encoding");
            Encoding.appendChild(doc.createTextNode(enc));
            DataObjectFormat.appendChild(Encoding);
        }
        SignedDataObjectProperties.appendChild(DataObjectFormat);
        SignedProperties.appendChild(SignedDataObjectProperties);
        QualifyingProperties.appendChild(SignedProperties);
        final XMLStructure content = new DOMStructure(QualifyingProperties);
        return fac.newXMLObject(Collections.singletonList(content), null, null, null);
    }
    
    private static String getX509IssuerName(final X509Certificate cert) {
        return cert.getIssuerX500Principal().toString();
    }
    
    private static String getX509SerialNumber(final X509Certificate cert) {
        return cert.getSerialNumber().toString();
    }
    
    private static Reference generateContentReference(final XMLSignatureFactory fac, final XmlSignatureAppearance sap, final String referenceId) throws GeneralSecurityException {
        final DigestMethod digestMethodSHA1 = fac.newDigestMethod("http://www.w3.org/2000/09/xmldsig#sha1", null);
        final List<Transform> transforms = new ArrayList<Transform>();
        transforms.add(fac.newTransform("http://www.w3.org/2000/09/xmldsig#enveloped-signature", (TransformParameterSpec)null));
        final XpathConstructor xpathConstructor = sap.getXpathConstructor();
        if (xpathConstructor != null && xpathConstructor.getXpathExpression().length() > 0) {
            final XPathFilter2ParameterSpec xpath2Spec = new XPathFilter2ParameterSpec(Collections.singletonList(new XPathType(xpathConstructor.getXpathExpression(), XPathType.Filter.INTERSECT)));
            transforms.add(fac.newTransform("http://www.w3.org/2002/06/xmldsig-filter2", xpath2Spec));
        }
        return fac.newReference("", digestMethodSHA1, transforms, null, referenceId);
    }
    
    private static Reference generateCustomReference(final XMLSignatureFactory fac, final String uri, final String type, final String id) throws GeneralSecurityException {
        final DigestMethod dsDigestMethod = fac.newDigestMethod("http://www.w3.org/2000/09/xmldsig#sha1", null);
        return fac.newReference(uri, dsDigestMethod, null, type, id);
    }
    
    private static void sign(final XMLSignatureFactory fac, final ExternalSignature externalSignature, final XmlLocator locator, final DOMSignedInfo si, final XMLObject xo, final KeyInfo ki, final String signatureId) throws DocumentException {
        final Document doc = locator.getDocument();
        final DOMSignContext domSignContext = new DOMSignContext(EmptyKey.getInstance(), doc.getDocumentElement());
        List objects = null;
        if (xo != null) {
            objects = Collections.singletonList(xo);
        }
        final DOMXMLSignature signature = (DOMXMLSignature)fac.newXMLSignature((SignedInfo)si, ki, objects, signatureId, null);
        final ByteArrayOutputStream byteRange = new ByteArrayOutputStream();
        try {
            signature.marshal(domSignContext.getParent(), domSignContext.getNextSibling(), DOMUtils.getSignaturePrefix((XMLCryptoContext)domSignContext), (DOMCryptoContext)domSignContext);
            final Element signElement = findElement(doc.getDocumentElement().getChildNodes(), "Signature");
            if (signatureId != null) {
                signElement.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xades", "http://uri.etsi.org/01903/v1.3.2#");
            }
            final List references = si.getReferences();
            for (int i = 0; i < references.size(); ++i) {
                references.get(i).digest((XMLSignContext)domSignContext);
            }
            si.canonicalize((XMLCryptoContext)domSignContext, byteRange);
            final Element signValue = findElement(signElement.getChildNodes(), "SignatureValue");
            final String valueBase64 = Base64.encode(externalSignature.sign(byteRange.toByteArray()));
            signValue.appendChild(doc.createTextNode(valueBase64));
            locator.setDocument(doc);
        }
        catch (Exception e) {
            throw new DocumentException(e);
        }
    }
    
    private static byte[] getByteArrayOfNode(final Node node) {
        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            final StreamResult xmlOutput = new StreamResult(new StringWriter());
            final Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty("omit-xml-declaration", "yes");
            transformer.transform(new DOMSource(node), xmlOutput);
            return xmlOutput.getWriter().toString().getBytes();
        }
        catch (Exception ex) {
            return stream.toByteArray();
        }
    }
    
    private static class EmptyKey implements Key
    {
        private static EmptyKey instance;
        
        public static EmptyKey getInstance() {
            return EmptyKey.instance;
        }
        
        @Override
        public String getAlgorithm() {
            return null;
        }
        
        @Override
        public String getFormat() {
            return null;
        }
        
        @Override
        public byte[] getEncoded() {
            return new byte[0];
        }
        
        static {
            EmptyKey.instance = new EmptyKey();
        }
    }
}
