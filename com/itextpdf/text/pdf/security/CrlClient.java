// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.util.Collection;
import java.security.cert.X509Certificate;

public interface CrlClient
{
    Collection<byte[]> getEncoded(final X509Certificate p0, final String p1);
}
