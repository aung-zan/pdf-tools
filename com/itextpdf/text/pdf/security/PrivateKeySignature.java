// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.security.GeneralSecurityException;
import java.security.Signature;
import java.security.PrivateKey;

public class PrivateKeySignature implements ExternalSignature
{
    private PrivateKey pk;
    private String hashAlgorithm;
    private String encryptionAlgorithm;
    private String provider;
    
    public PrivateKeySignature(final PrivateKey pk, final String hashAlgorithm, final String provider) {
        this.pk = pk;
        this.provider = provider;
        this.hashAlgorithm = DigestAlgorithms.getDigest(DigestAlgorithms.getAllowedDigests(hashAlgorithm));
        this.encryptionAlgorithm = pk.getAlgorithm();
        if (this.encryptionAlgorithm.startsWith("EC")) {
            this.encryptionAlgorithm = "ECDSA";
        }
    }
    
    @Override
    public String getHashAlgorithm() {
        return this.hashAlgorithm;
    }
    
    @Override
    public String getEncryptionAlgorithm() {
        return this.encryptionAlgorithm;
    }
    
    @Override
    public byte[] sign(final byte[] b) throws GeneralSecurityException {
        final String signMode = this.hashAlgorithm + "with" + this.encryptionAlgorithm;
        Signature sig;
        if (this.provider == null) {
            sig = Signature.getInstance(signMode);
        }
        else {
            sig = Signature.getInstance(signMode, this.provider);
        }
        sig.initSign(this.pk);
        sig.update(b);
        return sig.sign();
    }
}
