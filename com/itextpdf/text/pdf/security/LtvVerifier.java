// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.log.LoggerFactory;
import org.bouncycastle.cert.ocsp.OCSPException;
import org.bouncycastle.cert.ocsp.OCSPResp;
import org.bouncycastle.cert.ocsp.BasicOCSPResp;
import com.itextpdf.text.pdf.PdfArray;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import com.itextpdf.text.pdf.PRStream;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.util.Calendar;
import com.itextpdf.text.pdf.PdfName;
import java.security.cert.X509Certificate;
import java.io.IOException;
import java.util.Collection;
import java.util.ArrayList;
import java.security.cert.Certificate;
import java.security.GeneralSecurityException;
import java.util.List;
import com.itextpdf.text.log.Level;
import com.itextpdf.text.pdf.PdfDictionary;
import java.util.Date;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.log.Logger;

public class LtvVerifier extends RootStoreVerifier
{
    protected static final Logger LOGGER;
    protected LtvVerification.CertificateOption option;
    protected boolean verifyRootCertificate;
    protected PdfReader reader;
    protected AcroFields fields;
    protected Date signDate;
    protected String signatureName;
    protected PdfPKCS7 pkcs7;
    protected boolean latestRevision;
    protected PdfDictionary dss;
    
    public LtvVerifier(final PdfReader reader) throws GeneralSecurityException {
        super(null);
        this.option = LtvVerification.CertificateOption.SIGNING_CERTIFICATE;
        this.verifyRootCertificate = true;
        this.latestRevision = true;
        this.reader = reader;
        this.fields = reader.getAcroFields();
        final List<String> names = this.fields.getSignatureNames();
        this.signatureName = names.get(names.size() - 1);
        this.signDate = new Date();
        this.pkcs7 = this.coversWholeDocument();
        if (LtvVerifier.LOGGER.isLogging(Level.INFO)) {
            LtvVerifier.LOGGER.info(String.format("Checking %ssignature %s", this.pkcs7.isTsp() ? "document-level timestamp " : "", this.signatureName));
        }
    }
    
    public void setVerifier(final CertificateVerifier verifier) {
        this.verifier = verifier;
    }
    
    public void setCertificateOption(final LtvVerification.CertificateOption option) {
        this.option = option;
    }
    
    public void setVerifyRootCertificate(final boolean verifyRootCertificate) {
        this.verifyRootCertificate = verifyRootCertificate;
    }
    
    protected PdfPKCS7 coversWholeDocument() throws GeneralSecurityException {
        final PdfPKCS7 pkcs7 = this.fields.verifySignature(this.signatureName);
        if (!this.fields.signatureCoversWholeDocument(this.signatureName)) {
            throw new VerificationException(null, "Signature doesn't cover whole document.");
        }
        LtvVerifier.LOGGER.info("The timestamp covers whole document.");
        if (pkcs7.verify()) {
            LtvVerifier.LOGGER.info("The signed document has not been modified.");
            return pkcs7;
        }
        throw new VerificationException(null, "The document was altered after the final signature was applied.");
    }
    
    public List<VerificationOK> verify(List<VerificationOK> result) throws IOException, GeneralSecurityException {
        if (result == null) {
            result = new ArrayList<VerificationOK>();
        }
        while (this.pkcs7 != null) {
            result.addAll(this.verifySignature());
        }
        return result;
    }
    
    public List<VerificationOK> verifySignature() throws GeneralSecurityException, IOException {
        LtvVerifier.LOGGER.info("Verifying signature.");
        final List<VerificationOK> result = new ArrayList<VerificationOK>();
        final Certificate[] chain = this.pkcs7.getSignCertificateChain();
        this.verifyChain(chain);
        int total = 1;
        if (LtvVerification.CertificateOption.WHOLE_CHAIN.equals(this.option)) {
            total = chain.length;
        }
        int i = 0;
        while (i < total) {
            final X509Certificate signCert = (X509Certificate)chain[i++];
            X509Certificate issuerCert = null;
            if (i < chain.length) {
                issuerCert = (X509Certificate)chain[i];
            }
            LtvVerifier.LOGGER.info(signCert.getSubjectDN().getName());
            final List<VerificationOK> list = this.verify(signCert, issuerCert, this.signDate);
            if (list.size() == 0) {
                try {
                    signCert.verify(signCert.getPublicKey());
                    if (this.latestRevision && chain.length > 1) {
                        list.add(new VerificationOK(signCert, this.getClass(), "Root certificate in final revision"));
                    }
                    if (list.size() == 0 && this.verifyRootCertificate) {
                        throw new GeneralSecurityException();
                    }
                    if (chain.length > 1) {
                        list.add(new VerificationOK(signCert, this.getClass(), "Root certificate passed without checking"));
                    }
                }
                catch (GeneralSecurityException e) {
                    throw new VerificationException(signCert, "Couldn't verify with CRL or OCSP or trusted anchor");
                }
            }
            result.addAll(list);
        }
        this.switchToPreviousRevision();
        return result;
    }
    
    public void verifyChain(final Certificate[] chain) throws GeneralSecurityException {
        for (int i = 0; i < chain.length; ++i) {
            final X509Certificate cert = (X509Certificate)chain[i];
            cert.checkValidity(this.signDate);
            if (i > 0) {
                chain[i - 1].verify(chain[i].getPublicKey());
            }
        }
        LtvVerifier.LOGGER.info("All certificates are valid on " + this.signDate.toString());
    }
    
    @Override
    public List<VerificationOK> verify(final X509Certificate signCert, final X509Certificate issuerCert, final Date signDate) throws GeneralSecurityException, IOException {
        final RootStoreVerifier rootStoreVerifier = new RootStoreVerifier(this.verifier);
        rootStoreVerifier.setRootStore(this.rootStore);
        final CRLVerifier crlVerifier = new CRLVerifier(rootStoreVerifier, this.getCRLsFromDSS());
        crlVerifier.setRootStore(this.rootStore);
        crlVerifier.setOnlineCheckingAllowed(this.latestRevision || this.onlineCheckingAllowed);
        final OCSPVerifier ocspVerifier = new OCSPVerifier(crlVerifier, this.getOCSPResponsesFromDSS());
        ocspVerifier.setRootStore(this.rootStore);
        ocspVerifier.setOnlineCheckingAllowed(this.latestRevision || this.onlineCheckingAllowed);
        return ocspVerifier.verify(signCert, issuerCert, signDate);
    }
    
    public void switchToPreviousRevision() throws IOException, GeneralSecurityException {
        LtvVerifier.LOGGER.info("Switching to previous revision.");
        this.latestRevision = false;
        this.dss = this.reader.getCatalog().getAsDict(PdfName.DSS);
        Calendar cal = this.pkcs7.getTimeStampDate();
        if (cal == null) {
            cal = this.pkcs7.getSignDate();
        }
        this.signDate = cal.getTime();
        List<String> names = this.fields.getSignatureNames();
        if (names.size() > 1) {
            this.signatureName = names.get(names.size() - 2);
            this.reader = new PdfReader(this.fields.extractRevision(this.signatureName));
            this.fields = this.reader.getAcroFields();
            names = this.fields.getSignatureNames();
            this.signatureName = names.get(names.size() - 1);
            this.pkcs7 = this.coversWholeDocument();
            if (LtvVerifier.LOGGER.isLogging(Level.INFO)) {
                LtvVerifier.LOGGER.info(String.format("Checking %ssignature %s", this.pkcs7.isTsp() ? "document-level timestamp " : "", this.signatureName));
            }
        }
        else {
            LtvVerifier.LOGGER.info("No signatures in revision");
            this.pkcs7 = null;
        }
    }
    
    public List<X509CRL> getCRLsFromDSS() throws GeneralSecurityException, IOException {
        final List<X509CRL> crls = new ArrayList<X509CRL>();
        if (this.dss == null) {
            return crls;
        }
        final PdfArray crlarray = this.dss.getAsArray(PdfName.CRLS);
        if (crlarray == null) {
            return crls;
        }
        final CertificateFactory cf = CertificateFactory.getInstance("X.509");
        for (int i = 0; i < crlarray.size(); ++i) {
            final PRStream stream = (PRStream)crlarray.getAsStream(i);
            final X509CRL crl = (X509CRL)cf.generateCRL(new ByteArrayInputStream(PdfReader.getStreamBytes(stream)));
            crls.add(crl);
        }
        return crls;
    }
    
    public List<BasicOCSPResp> getOCSPResponsesFromDSS() throws IOException, GeneralSecurityException {
        final List<BasicOCSPResp> ocsps = new ArrayList<BasicOCSPResp>();
        if (this.dss == null) {
            return ocsps;
        }
        final PdfArray ocsparray = this.dss.getAsArray(PdfName.OCSPS);
        if (ocsparray == null) {
            return ocsps;
        }
        for (int i = 0; i < ocsparray.size(); ++i) {
            final PRStream stream = (PRStream)ocsparray.getAsStream(i);
            final OCSPResp ocspResponse = new OCSPResp(PdfReader.getStreamBytes(stream));
            if (ocspResponse.getStatus() == 0) {
                try {
                    ocsps.add((BasicOCSPResp)ocspResponse.getResponseObject());
                }
                catch (OCSPException e) {
                    throw new GeneralSecurityException(e);
                }
            }
        }
        return ocsps;
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(LtvVerifier.class);
    }
}
