// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.pdf.PdfDictionary;
import java.security.GeneralSecurityException;
import java.io.InputStream;

public interface ExternalSignatureContainer
{
    byte[] sign(final InputStream p0) throws GeneralSecurityException;
    
    void modifySigningDictionary(final PdfDictionary p0);
}
