// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.bouncycastle.cert.ocsp.BasicOCSPResp;
import java.util.Enumeration;
import java.util.ArrayList;
import java.util.List;
import java.security.KeyStore;
import java.util.Iterator;
import java.security.cert.Certificate;
import java.security.cert.CertificateParsingException;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.security.cert.CRL;
import java.util.Collection;
import java.security.cert.X509Certificate;

public class CertificateVerification
{
    public static String verifyCertificate(final X509Certificate cert, final Collection<CRL> crls, Calendar calendar) {
        if (calendar == null) {
            calendar = new GregorianCalendar();
        }
        if (cert.hasUnsupportedCriticalExtension()) {
            for (final String oid : cert.getCriticalExtensionOIDs()) {
                if ("2.5.29.15".equals(oid) && cert.getKeyUsage()[0]) {
                    continue;
                }
                try {
                    if ("2.5.29.37".equals(oid) && cert.getExtendedKeyUsage().contains("1.3.6.1.5.5.7.3.8")) {
                        continue;
                    }
                }
                catch (CertificateParsingException ex) {}
                return "Has unsupported critical extension";
            }
        }
        try {
            cert.checkValidity(calendar.getTime());
        }
        catch (Exception e) {
            return e.getMessage();
        }
        if (crls != null) {
            for (final CRL crl : crls) {
                if (crl.isRevoked(cert)) {
                    return "Certificate revoked";
                }
            }
        }
        return null;
    }
    
    public static List<VerificationException> verifyCertificates(final Certificate[] certs, final KeyStore keystore, final Collection<CRL> crls, Calendar calendar) {
        final List<VerificationException> result = new ArrayList<VerificationException>();
        if (calendar == null) {
            calendar = new GregorianCalendar();
        }
        for (int k = 0; k < certs.length; ++k) {
            final X509Certificate cert = (X509Certificate)certs[k];
            final String err = verifyCertificate(cert, crls, calendar);
            if (err != null) {
                result.add(new VerificationException(cert, err));
            }
            try {
                final Enumeration<String> aliases = keystore.aliases();
                while (aliases.hasMoreElements()) {
                    try {
                        final String alias = aliases.nextElement();
                        if (!keystore.isCertificateEntry(alias)) {
                            continue;
                        }
                        final X509Certificate certStoreX509 = (X509Certificate)keystore.getCertificate(alias);
                        if (verifyCertificate(certStoreX509, crls, calendar) != null) {
                            continue;
                        }
                        try {
                            cert.verify(certStoreX509.getPublicKey());
                            return result;
                        }
                        catch (Exception e) {}
                    }
                    catch (Exception ex) {
                        continue;
                    }
                    break;
                }
            }
            catch (Exception ex2) {}
            int j;
            for (j = 0; j < certs.length; ++j) {
                if (j != k) {
                    final X509Certificate certNext = (X509Certificate)certs[j];
                    try {
                        cert.verify(certNext.getPublicKey());
                        break;
                    }
                    catch (Exception ex3) {}
                }
            }
            if (j == certs.length) {
                result.add(new VerificationException(cert, "Cannot be verified against the KeyStore or the certificate chain"));
            }
        }
        if (result.size() == 0) {
            result.add(new VerificationException(null, "Invalid state. Possible circular certificate chain"));
        }
        return result;
    }
    
    public static List<VerificationException> verifyCertificates(final Certificate[] certs, final KeyStore keystore, final Calendar calendar) {
        return verifyCertificates(certs, keystore, null, calendar);
    }
    
    public static boolean verifyOcspCertificates(final BasicOCSPResp ocsp, final KeyStore keystore, String provider) {
        if (provider == null) {
            provider = "BC";
        }
        try {
            final Enumeration<String> aliases = keystore.aliases();
            while (aliases.hasMoreElements()) {
                try {
                    final String alias = aliases.nextElement();
                    if (!keystore.isCertificateEntry(alias)) {
                        continue;
                    }
                    final X509Certificate certStoreX509 = (X509Certificate)keystore.getCertificate(alias);
                    if (ocsp.isSignatureValid(new JcaContentVerifierProviderBuilder().setProvider(provider).build(certStoreX509.getPublicKey()))) {
                        return true;
                    }
                    continue;
                }
                catch (Exception ex) {}
            }
        }
        catch (Exception ex2) {}
        return false;
    }
    
    public static boolean verifyTimestampCertificates(final TimeStampToken ts, final KeyStore keystore, String provider) {
        if (provider == null) {
            provider = "BC";
        }
        try {
            final Enumeration<String> aliases = keystore.aliases();
            while (aliases.hasMoreElements()) {
                try {
                    final String alias = aliases.nextElement();
                    if (!keystore.isCertificateEntry(alias)) {
                        continue;
                    }
                    final X509Certificate certStoreX509 = (X509Certificate)keystore.getCertificate(alias);
                    ts.isSignatureValid(new JcaSimpleSignerInfoVerifierBuilder().setProvider(provider).build(certStoreX509));
                    return true;
                }
                catch (Exception ex) {
                    continue;
                }
                break;
            }
        }
        catch (Exception ex2) {}
        return false;
    }
}
