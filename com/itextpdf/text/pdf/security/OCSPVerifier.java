// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.log.LoggerFactory;
import org.bouncycastle.operator.ContentVerifierProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import java.security.cert.CRL;
import java.util.Enumeration;
import java.security.cert.X509CRL;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import java.security.KeyStoreException;
import java.security.cert.CertificateParsingException;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import java.security.cert.Certificate;
import org.bouncycastle.cert.ocsp.SingleResp;
import org.bouncycastle.cert.ocsp.CertificateStatus;
import com.itextpdf.text.log.Level;
import org.bouncycastle.cert.ocsp.OCSPException;
import org.bouncycastle.operator.DigestCalculatorProvider;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
import org.bouncycastle.cert.X509CertificateHolder;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Date;
import java.security.cert.X509Certificate;
import org.bouncycastle.cert.ocsp.BasicOCSPResp;
import java.util.List;
import com.itextpdf.text.log.Logger;

public class OCSPVerifier extends RootStoreVerifier
{
    protected static final Logger LOGGER;
    protected static final String id_kp_OCSPSigning = "1.3.6.1.5.5.7.3.9";
    protected List<BasicOCSPResp> ocsps;
    
    public OCSPVerifier(final CertificateVerifier verifier, final List<BasicOCSPResp> ocsps) {
        super(verifier);
        this.ocsps = ocsps;
    }
    
    @Override
    public List<VerificationOK> verify(final X509Certificate signCert, final X509Certificate issuerCert, final Date signDate) throws GeneralSecurityException, IOException {
        final List<VerificationOK> result = new ArrayList<VerificationOK>();
        int validOCSPsFound = 0;
        if (this.ocsps != null) {
            for (final BasicOCSPResp ocspResp : this.ocsps) {
                if (this.verify(ocspResp, signCert, issuerCert, signDate)) {
                    ++validOCSPsFound;
                }
            }
        }
        boolean online = false;
        if (this.onlineCheckingAllowed && validOCSPsFound == 0 && this.verify(this.getOcspResponse(signCert, issuerCert), signCert, issuerCert, signDate)) {
            ++validOCSPsFound;
            online = true;
        }
        OCSPVerifier.LOGGER.info("Valid OCSPs found: " + validOCSPsFound);
        if (validOCSPsFound > 0) {
            result.add(new VerificationOK(signCert, this.getClass(), "Valid OCSPs Found: " + validOCSPsFound + (online ? " (online)" : "")));
        }
        if (this.verifier != null) {
            result.addAll(this.verifier.verify(signCert, issuerCert, signDate));
        }
        return result;
    }
    
    public boolean verify(final BasicOCSPResp ocspResp, final X509Certificate signCert, X509Certificate issuerCert, final Date signDate) throws GeneralSecurityException, IOException {
        if (ocspResp == null) {
            return false;
        }
        final SingleResp[] resp = ocspResp.getResponses();
        for (int i = 0; i < resp.length; ++i) {
            if (signCert.getSerialNumber().equals(resp[i].getCertID().getSerialNumber())) {
                try {
                    if (issuerCert == null) {
                        issuerCert = signCert;
                    }
                    if (!resp[i].getCertID().matchesIssuer(new X509CertificateHolder(issuerCert.getEncoded()), new BcDigestCalculatorProvider())) {
                        OCSPVerifier.LOGGER.info("OCSP: Issuers doesn't match.");
                        continue;
                    }
                }
                catch (OCSPException e) {
                    continue;
                }
                Date nextUpdate = resp[i].getNextUpdate();
                if (nextUpdate == null) {
                    nextUpdate = new Date(resp[i].getThisUpdate().getTime() + 180000L);
                    if (OCSPVerifier.LOGGER.isLogging(Level.INFO)) {
                        OCSPVerifier.LOGGER.info(String.format("No 'next update' for OCSP Response; assuming %s", nextUpdate));
                    }
                }
                if (signDate.after(nextUpdate)) {
                    if (OCSPVerifier.LOGGER.isLogging(Level.INFO)) {
                        OCSPVerifier.LOGGER.info(String.format("OCSP no longer valid: %s after %s", signDate, nextUpdate));
                    }
                }
                else {
                    final Object status = resp[i].getCertStatus();
                    if (status == CertificateStatus.GOOD) {
                        this.isValidResponse(ocspResp, issuerCert);
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public void isValidResponse(final BasicOCSPResp ocspResp, final X509Certificate issuerCert) throws GeneralSecurityException, IOException {
        X509Certificate responderCert = null;
        if (this.isSignatureValid(ocspResp, issuerCert)) {
            responderCert = issuerCert;
        }
        if (responderCert == null) {
            if (ocspResp.getCerts() != null) {
                final X509CertificateHolder[] arr$;
                final X509CertificateHolder[] certs = arr$ = ocspResp.getCerts();
                for (final X509CertificateHolder cert : arr$) {
                    Label_0126: {
                        X509Certificate tempCert;
                        try {
                            tempCert = new JcaX509CertificateConverter().getCertificate(cert);
                        }
                        catch (Exception ex) {
                            break Label_0126;
                        }
                        List<String> keyPurposes = null;
                        try {
                            keyPurposes = tempCert.getExtendedKeyUsage();
                            if (keyPurposes != null && keyPurposes.contains("1.3.6.1.5.5.7.3.9") && this.isSignatureValid(ocspResp, tempCert)) {
                                responderCert = tempCert;
                                break;
                            }
                        }
                        catch (CertificateParsingException ex2) {}
                    }
                }
                if (responderCert == null) {
                    throw new VerificationException(issuerCert, "OCSP response could not be verified");
                }
            }
            else {
                if (this.rootStore != null) {
                    try {
                        final Enumeration<String> aliases = this.rootStore.aliases();
                        while (aliases.hasMoreElements()) {
                            final String alias = aliases.nextElement();
                            try {
                                if (!this.rootStore.isCertificateEntry(alias)) {
                                    continue;
                                }
                                final X509Certificate anchor = (X509Certificate)this.rootStore.getCertificate(alias);
                                if (this.isSignatureValid(ocspResp, anchor)) {
                                    responderCert = anchor;
                                    break;
                                }
                                continue;
                            }
                            catch (GeneralSecurityException ex3) {}
                        }
                    }
                    catch (KeyStoreException e) {
                        responderCert = null;
                    }
                }
                if (responderCert == null) {
                    throw new VerificationException(issuerCert, "OCSP response could not be verified");
                }
            }
        }
        responderCert.verify(issuerCert.getPublicKey());
        if (responderCert.getExtensionValue(OCSPObjectIdentifiers.id_pkix_ocsp_nocheck.getId()) == null) {
            CRL crl;
            try {
                crl = CertificateUtil.getCRL(responderCert);
            }
            catch (Exception ignored) {
                crl = null;
            }
            if (crl != null && crl instanceof X509CRL) {
                final CRLVerifier crlVerifier = new CRLVerifier(null, null);
                crlVerifier.setRootStore(this.rootStore);
                crlVerifier.setOnlineCheckingAllowed(this.onlineCheckingAllowed);
                crlVerifier.verify((X509CRL)crl, responderCert, issuerCert, new Date());
                return;
            }
        }
        responderCert.checkValidity();
    }
    
    @Deprecated
    public boolean verifyResponse(final BasicOCSPResp ocspResp, final X509Certificate issuerCert) {
        try {
            this.isValidResponse(ocspResp, issuerCert);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
    
    public boolean isSignatureValid(final BasicOCSPResp ocspResp, final Certificate responderCert) {
        try {
            final ContentVerifierProvider verifierProvider = new JcaContentVerifierProviderBuilder().setProvider("BC").build(responderCert.getPublicKey());
            return ocspResp.isSignatureValid(verifierProvider);
        }
        catch (OperatorCreationException e) {
            return false;
        }
        catch (OCSPException e2) {
            return false;
        }
    }
    
    public BasicOCSPResp getOcspResponse(final X509Certificate signCert, final X509Certificate issuerCert) {
        if (signCert == null && issuerCert == null) {
            return null;
        }
        final OcspClientBouncyCastle ocsp = new OcspClientBouncyCastle();
        final BasicOCSPResp ocspResp = ocsp.getBasicOCSPResp(signCert, issuerCert, null);
        if (ocspResp == null) {
            return null;
        }
        final SingleResp[] resp = ocspResp.getResponses();
        for (int i = 0; i < resp.length; ++i) {
            final Object status = resp[i].getCertStatus();
            if (status == CertificateStatus.GOOD) {
                return ocspResp;
            }
        }
        return null;
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(OCSPVerifier.class);
    }
}
