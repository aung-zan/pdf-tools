// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.security.GeneralSecurityException;

public interface ExternalSignature
{
    String getHashAlgorithm();
    
    String getEncryptionAlgorithm();
    
    byte[] sign(final byte[] p0) throws GeneralSecurityException;
}
