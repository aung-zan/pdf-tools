// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.security.cert.X509Certificate;

public class CertificateVerifier
{
    protected CertificateVerifier verifier;
    protected boolean onlineCheckingAllowed;
    
    public CertificateVerifier(final CertificateVerifier verifier) {
        this.onlineCheckingAllowed = true;
        this.verifier = verifier;
    }
    
    public void setOnlineCheckingAllowed(final boolean onlineCheckingAllowed) {
        this.onlineCheckingAllowed = onlineCheckingAllowed;
    }
    
    public List<VerificationOK> verify(final X509Certificate signCert, final X509Certificate issuerCert, final Date signDate) throws GeneralSecurityException, IOException {
        if (signDate != null) {
            signCert.checkValidity(signDate);
        }
        if (issuerCert != null) {
            signCert.verify(issuerCert.getPublicKey());
        }
        else {
            signCert.verify(signCert.getPublicKey());
        }
        final List<VerificationOK> result = new ArrayList<VerificationOK>();
        if (this.verifier != null) {
            result.addAll(this.verifier.verify(signCert, issuerCert, signDate));
        }
        return result;
    }
}
