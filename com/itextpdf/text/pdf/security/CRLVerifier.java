// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.log.LoggerFactory;
import java.util.Enumeration;
import java.net.URL;
import java.security.cert.CertificateFactory;
import java.security.cert.Certificate;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Date;
import java.security.cert.X509Certificate;
import java.security.cert.X509CRL;
import java.util.List;
import com.itextpdf.text.log.Logger;

public class CRLVerifier extends RootStoreVerifier
{
    protected static final Logger LOGGER;
    List<X509CRL> crls;
    
    public CRLVerifier(final CertificateVerifier verifier, final List<X509CRL> crls) {
        super(verifier);
        this.crls = crls;
    }
    
    @Override
    public List<VerificationOK> verify(final X509Certificate signCert, final X509Certificate issuerCert, final Date signDate) throws GeneralSecurityException, IOException {
        final List<VerificationOK> result = new ArrayList<VerificationOK>();
        int validCrlsFound = 0;
        if (this.crls != null) {
            for (final X509CRL crl : this.crls) {
                if (this.verify(crl, signCert, issuerCert, signDate)) {
                    ++validCrlsFound;
                }
            }
        }
        boolean online = false;
        if (this.onlineCheckingAllowed && validCrlsFound == 0 && this.verify(this.getCRL(signCert, issuerCert), signCert, issuerCert, signDate)) {
            ++validCrlsFound;
            online = true;
        }
        CRLVerifier.LOGGER.info("Valid CRLs found: " + validCrlsFound);
        if (validCrlsFound > 0) {
            result.add(new VerificationOK(signCert, this.getClass(), "Valid CRLs found: " + validCrlsFound + (online ? " (online)" : "")));
        }
        if (this.verifier != null) {
            result.addAll(this.verifier.verify(signCert, issuerCert, signDate));
        }
        return result;
    }
    
    public boolean verify(final X509CRL crl, final X509Certificate signCert, final X509Certificate issuerCert, final Date signDate) throws GeneralSecurityException {
        if (crl == null || signDate == null) {
            return false;
        }
        if (!crl.getIssuerX500Principal().equals(signCert.getIssuerX500Principal()) || !signDate.after(crl.getThisUpdate()) || !signDate.before(crl.getNextUpdate())) {
            return false;
        }
        if (this.isSignatureValid(crl, issuerCert) && crl.isRevoked(signCert)) {
            throw new VerificationException(signCert, "The certificate has been revoked.");
        }
        return true;
    }
    
    public X509CRL getCRL(final X509Certificate signCert, X509Certificate issuerCert) {
        if (issuerCert == null) {
            issuerCert = signCert;
        }
        try {
            final String crlurl = CertificateUtil.getCRLURL(signCert);
            if (crlurl == null) {
                return null;
            }
            CRLVerifier.LOGGER.info("Getting CRL from " + crlurl);
            final CertificateFactory cf = CertificateFactory.getInstance("X.509");
            return (X509CRL)cf.generateCRL(new URL(crlurl).openStream());
        }
        catch (IOException e) {
            return null;
        }
        catch (GeneralSecurityException e2) {
            return null;
        }
    }
    
    public boolean isSignatureValid(final X509CRL crl, final X509Certificate crlIssuer) {
        if (crlIssuer != null) {
            try {
                crl.verify(crlIssuer.getPublicKey());
                return true;
            }
            catch (GeneralSecurityException e) {
                CRLVerifier.LOGGER.warn("CRL not issued by the same authority as the certificate that is being checked");
            }
        }
        if (this.rootStore == null) {
            return false;
        }
        try {
            final Enumeration<String> aliases = this.rootStore.aliases();
            while (aliases.hasMoreElements()) {
                final String alias = aliases.nextElement();
                try {
                    if (!this.rootStore.isCertificateEntry(alias)) {
                        continue;
                    }
                    final X509Certificate anchor = (X509Certificate)this.rootStore.getCertificate(alias);
                    crl.verify(anchor.getPublicKey());
                    return true;
                }
                catch (GeneralSecurityException e2) {
                    continue;
                }
                break;
            }
        }
        catch (GeneralSecurityException e) {
            return false;
        }
        return false;
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(CRLVerifier.class);
    }
}
