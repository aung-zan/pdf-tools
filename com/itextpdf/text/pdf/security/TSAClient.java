// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.security.GeneralSecurityException;
import java.security.MessageDigest;

public interface TSAClient
{
    int getTokenSizeEstimate();
    
    MessageDigest getMessageDigest() throws GeneralSecurityException;
    
    byte[] getTimeStampToken(final byte[] p0) throws Exception;
}
