// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.security.GeneralSecurityException;
import java.security.MessageDigest;

public interface ExternalDigest
{
    MessageDigest getMessageDigest(final String p0) throws GeneralSecurityException;
}
