// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.log.LoggerFactory;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.io.ByteArrayOutputStream;
import com.itextpdf.text.pdf.codec.Base64;
import java.net.URL;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampTokenInfo;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.asn1.cmp.PKIFailureInfo;
import org.bouncycastle.tsp.TimeStampRequest;
import java.io.IOException;
import com.itextpdf.text.error_messages.MessageLocalization;
import org.bouncycastle.tsp.TimeStampResponse;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.tsp.TimeStampRequestGenerator;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import com.itextpdf.text.log.Logger;

public class TSAClientBouncyCastle implements TSAClient
{
    private static final Logger LOGGER;
    protected String tsaURL;
    protected String tsaUsername;
    protected String tsaPassword;
    protected TSAInfoBouncyCastle tsaInfo;
    public static final int DEFAULTTOKENSIZE = 4096;
    protected int tokenSizeEstimate;
    public static final String DEFAULTHASHALGORITHM = "SHA-256";
    protected String digestAlgorithm;
    private String tsaReqPolicy;
    
    public TSAClientBouncyCastle(final String url) {
        this(url, null, null, 4096, "SHA-256");
    }
    
    public TSAClientBouncyCastle(final String url, final String username, final String password) {
        this(url, username, password, 4096, "SHA-256");
    }
    
    public TSAClientBouncyCastle(final String url, final String username, final String password, final int tokSzEstimate, final String digestAlgorithm) {
        this.tsaReqPolicy = null;
        this.tsaURL = url;
        this.tsaUsername = username;
        this.tsaPassword = password;
        this.tokenSizeEstimate = tokSzEstimate;
        this.digestAlgorithm = digestAlgorithm;
    }
    
    public void setTSAInfo(final TSAInfoBouncyCastle tsaInfo) {
        this.tsaInfo = tsaInfo;
    }
    
    @Override
    public int getTokenSizeEstimate() {
        return this.tokenSizeEstimate;
    }
    
    public String getTSAReqPolicy() {
        return this.tsaReqPolicy;
    }
    
    public void setTSAReqPolicy(final String tsaReqPolicy) {
        this.tsaReqPolicy = tsaReqPolicy;
    }
    
    @Override
    public MessageDigest getMessageDigest() throws GeneralSecurityException {
        return new BouncyCastleDigest().getMessageDigest(this.digestAlgorithm);
    }
    
    @Override
    public byte[] getTimeStampToken(final byte[] imprint) throws IOException, TSPException {
        byte[] respBytes = null;
        final TimeStampRequestGenerator tsqGenerator = new TimeStampRequestGenerator();
        tsqGenerator.setCertReq(true);
        if (this.tsaReqPolicy != null && this.tsaReqPolicy.length() > 0) {
            tsqGenerator.setReqPolicy(new ASN1ObjectIdentifier(this.tsaReqPolicy));
        }
        final BigInteger nonce = BigInteger.valueOf(System.currentTimeMillis());
        final TimeStampRequest request = tsqGenerator.generate(new ASN1ObjectIdentifier(DigestAlgorithms.getAllowedDigests(this.digestAlgorithm)), imprint, nonce);
        final byte[] requestBytes = request.getEncoded();
        respBytes = this.getTSAResponse(requestBytes);
        final TimeStampResponse response = new TimeStampResponse(respBytes);
        response.validate(request);
        final PKIFailureInfo failure = response.getFailInfo();
        final int value = (failure == null) ? 0 : failure.intValue();
        if (value != 0) {
            throw new IOException(MessageLocalization.getComposedMessage("invalid.tsa.1.response.code.2", this.tsaURL, String.valueOf(value)));
        }
        final TimeStampToken tsToken = response.getTimeStampToken();
        if (tsToken == null) {
            throw new IOException(MessageLocalization.getComposedMessage("tsa.1.failed.to.return.time.stamp.token.2", this.tsaURL, response.getStatusString()));
        }
        final TimeStampTokenInfo tsTokenInfo = tsToken.getTimeStampInfo();
        final byte[] encoded = tsToken.getEncoded();
        TSAClientBouncyCastle.LOGGER.info("Timestamp generated: " + tsTokenInfo.getGenTime());
        if (this.tsaInfo != null) {
            this.tsaInfo.inspectTimeStampTokenInfo(tsTokenInfo);
        }
        this.tokenSizeEstimate = encoded.length + 32;
        return encoded;
    }
    
    protected byte[] getTSAResponse(final byte[] requestBytes) throws IOException {
        final URL url = new URL(this.tsaURL);
        URLConnection tsaConnection;
        try {
            tsaConnection = url.openConnection();
        }
        catch (IOException ioe) {
            throw new IOException(MessageLocalization.getComposedMessage("failed.to.get.tsa.response.from.1", this.tsaURL));
        }
        tsaConnection.setDoInput(true);
        tsaConnection.setDoOutput(true);
        tsaConnection.setUseCaches(false);
        tsaConnection.setRequestProperty("Content-Type", "application/timestamp-query");
        tsaConnection.setRequestProperty("Content-Transfer-Encoding", "binary");
        if (this.tsaUsername != null && !this.tsaUsername.equals("")) {
            final String userPassword = this.tsaUsername + ":" + this.tsaPassword;
            tsaConnection.setRequestProperty("Authorization", "Basic " + Base64.encodeBytes(userPassword.getBytes(), 8));
        }
        final OutputStream out = tsaConnection.getOutputStream();
        out.write(requestBytes);
        out.close();
        final InputStream inp = tsaConnection.getInputStream();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final byte[] buffer = new byte[1024];
        int bytesRead = 0;
        while ((bytesRead = inp.read(buffer, 0, buffer.length)) >= 0) {
            baos.write(buffer, 0, bytesRead);
        }
        byte[] respBytes = baos.toByteArray();
        final String encoding = tsaConnection.getContentEncoding();
        if (encoding != null && encoding.equalsIgnoreCase("base64")) {
            respBytes = Base64.decode(new String(respBytes));
        }
        return respBytes;
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(TSAClientBouncyCastle.class);
    }
}
