// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import java.io.InputStream;
import java.security.cert.CertificateFactory;
import java.net.URL;
import java.security.cert.CertificateParsingException;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.Extension;
import java.io.IOException;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CRL;
import java.security.cert.X509Certificate;

public class CertificateUtil
{
    public static CRL getCRL(final X509Certificate certificate) throws CertificateException, CRLException, IOException {
        return getCRL(getCRLURL(certificate));
    }
    
    public static String getCRLURL(final X509Certificate certificate) throws CertificateParsingException {
        ASN1Primitive obj;
        try {
            obj = getExtensionValue(certificate, Extension.cRLDistributionPoints.getId());
        }
        catch (IOException e) {
            obj = null;
        }
        if (obj == null) {
            return null;
        }
        final CRLDistPoint dist = CRLDistPoint.getInstance(obj);
        final DistributionPoint[] arr$;
        final DistributionPoint[] dists = arr$ = dist.getDistributionPoints();
        for (final DistributionPoint p : arr$) {
            final DistributionPointName distributionPointName = p.getDistributionPoint();
            if (0 == distributionPointName.getType()) {
                final GeneralNames generalNames = (GeneralNames)distributionPointName.getName();
                final GeneralName[] arr$2;
                final GeneralName[] names = arr$2 = generalNames.getNames();
                for (final GeneralName name : arr$2) {
                    if (name.getTagNo() == 6) {
                        final DERIA5String derStr = DERIA5String.getInstance((ASN1TaggedObject)name.toASN1Primitive(), false);
                        return derStr.getString();
                    }
                }
            }
        }
        return null;
    }
    
    public static CRL getCRL(final String url) throws IOException, CertificateException, CRLException {
        if (url == null) {
            return null;
        }
        final InputStream is = new URL(url).openStream();
        final CertificateFactory cf = CertificateFactory.getInstance("X.509");
        return cf.generateCRL(is);
    }
    
    public static String getOCSPURL(final X509Certificate certificate) {
        try {
            final ASN1Primitive obj = getExtensionValue(certificate, Extension.authorityInfoAccess.getId());
            if (obj == null) {
                return null;
            }
            final ASN1Sequence AccessDescriptions = (ASN1Sequence)obj;
            for (int i = 0; i < AccessDescriptions.size(); ++i) {
                final ASN1Sequence AccessDescription = (ASN1Sequence)AccessDescriptions.getObjectAt(i);
                if (AccessDescription.size() == 2) {
                    if (AccessDescription.getObjectAt(0) instanceof ASN1ObjectIdentifier) {
                        final ASN1ObjectIdentifier id = (ASN1ObjectIdentifier)AccessDescription.getObjectAt(0);
                        if ("1.3.6.1.5.5.7.48.1".equals(id.getId())) {
                            final ASN1Primitive description = (ASN1Primitive)AccessDescription.getObjectAt(1);
                            final String AccessLocation = getStringFromGeneralName(description);
                            if (AccessLocation == null) {
                                return "";
                            }
                            return AccessLocation;
                        }
                    }
                }
            }
        }
        catch (IOException e) {
            return null;
        }
        return null;
    }
    
    public static String getTSAURL(final X509Certificate certificate) {
        final byte[] der = certificate.getExtensionValue("1.2.840.113583.1.1.9.1");
        if (der == null) {
            return null;
        }
        try {
            ASN1Primitive asn1obj = ASN1Primitive.fromByteArray(der);
            final DEROctetString octets = (DEROctetString)asn1obj;
            asn1obj = ASN1Primitive.fromByteArray(octets.getOctets());
            final ASN1Sequence asn1seq = ASN1Sequence.getInstance(asn1obj);
            return getStringFromGeneralName(asn1seq.getObjectAt(1).toASN1Primitive());
        }
        catch (IOException e) {
            return null;
        }
    }
    
    private static ASN1Primitive getExtensionValue(final X509Certificate certificate, final String oid) throws IOException {
        final byte[] bytes = certificate.getExtensionValue(oid);
        if (bytes == null) {
            return null;
        }
        ASN1InputStream aIn = new ASN1InputStream(new ByteArrayInputStream(bytes));
        final ASN1OctetString octs = (ASN1OctetString)aIn.readObject();
        aIn = new ASN1InputStream(new ByteArrayInputStream(octs.getOctets()));
        return aIn.readObject();
    }
    
    private static String getStringFromGeneralName(final ASN1Primitive names) throws IOException {
        final ASN1TaggedObject taggedObject = (ASN1TaggedObject)names;
        return new String(ASN1OctetString.getInstance(taggedObject, false).getOctets(), "ISO-8859-1");
    }
}
