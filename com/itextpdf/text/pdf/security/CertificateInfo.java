// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.util.List;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1String;
import org.bouncycastle.asn1.ASN1Set;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1TaggedObject;
import java.io.InputStream;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.asn1.ASN1Primitive;
import com.itextpdf.text.ExceptionConverter;
import org.bouncycastle.asn1.ASN1Sequence;
import java.security.cert.X509Certificate;

public class CertificateInfo
{
    public static X500Name getIssuerFields(final X509Certificate cert) {
        try {
            return new X500Name((ASN1Sequence)getIssuer(cert.getTBSCertificate()));
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public static ASN1Primitive getIssuer(final byte[] enc) {
        try {
            final ASN1InputStream in = new ASN1InputStream(new ByteArrayInputStream(enc));
            final ASN1Sequence seq = (ASN1Sequence)in.readObject();
            return (ASN1Primitive)seq.getObjectAt((seq.getObjectAt(0) instanceof ASN1TaggedObject) ? 3 : 2);
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public static X500Name getSubjectFields(final X509Certificate cert) {
        try {
            if (cert != null) {
                return new X500Name((ASN1Sequence)getSubject(cert.getTBSCertificate()));
            }
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
        return null;
    }
    
    public static ASN1Primitive getSubject(final byte[] enc) {
        try {
            final ASN1InputStream in = new ASN1InputStream(new ByteArrayInputStream(enc));
            final ASN1Sequence seq = (ASN1Sequence)in.readObject();
            return (ASN1Primitive)seq.getObjectAt((seq.getObjectAt(0) instanceof ASN1TaggedObject) ? 5 : 4);
        }
        catch (IOException e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public static class X500Name
    {
        public static final ASN1ObjectIdentifier C;
        public static final ASN1ObjectIdentifier O;
        public static final ASN1ObjectIdentifier OU;
        public static final ASN1ObjectIdentifier T;
        public static final ASN1ObjectIdentifier CN;
        public static final ASN1ObjectIdentifier SN;
        public static final ASN1ObjectIdentifier L;
        public static final ASN1ObjectIdentifier ST;
        public static final ASN1ObjectIdentifier SURNAME;
        public static final ASN1ObjectIdentifier GIVENNAME;
        public static final ASN1ObjectIdentifier INITIALS;
        public static final ASN1ObjectIdentifier GENERATION;
        public static final ASN1ObjectIdentifier UNIQUE_IDENTIFIER;
        public static final ASN1ObjectIdentifier EmailAddress;
        public static final ASN1ObjectIdentifier E;
        public static final ASN1ObjectIdentifier DC;
        public static final ASN1ObjectIdentifier UID;
        public static final Map<ASN1ObjectIdentifier, String> DefaultSymbols;
        public Map<String, ArrayList<String>> values;
        
        public X500Name(final ASN1Sequence seq) {
            this.values = new HashMap<String, ArrayList<String>>();
            final Enumeration<ASN1Set> e = (Enumeration<ASN1Set>)seq.getObjects();
            while (e.hasMoreElements()) {
                final ASN1Set set = e.nextElement();
                for (int i = 0; i < set.size(); ++i) {
                    final ASN1Sequence s = (ASN1Sequence)set.getObjectAt(i);
                    final String id = X500Name.DefaultSymbols.get(s.getObjectAt(0));
                    if (id != null) {
                        ArrayList<String> vs = this.values.get(id);
                        if (vs == null) {
                            vs = new ArrayList<String>();
                            this.values.put(id, vs);
                        }
                        vs.add(((ASN1String)s.getObjectAt(1)).getString());
                    }
                }
            }
        }
        
        public X500Name(final String dirName) {
            this.values = new HashMap<String, ArrayList<String>>();
            final X509NameTokenizer nTok = new X509NameTokenizer(dirName);
            while (nTok.hasMoreTokens()) {
                final String token = nTok.nextToken();
                final int index = token.indexOf(61);
                if (index == -1) {
                    throw new IllegalArgumentException(MessageLocalization.getComposedMessage("badly.formated.directory.string", new Object[0]));
                }
                final String id = token.substring(0, index).toUpperCase();
                final String value = token.substring(index + 1);
                ArrayList<String> vs = this.values.get(id);
                if (vs == null) {
                    vs = new ArrayList<String>();
                    this.values.put(id, vs);
                }
                vs.add(value);
            }
        }
        
        public String getField(final String name) {
            final List<String> vs = this.values.get(name);
            return (vs == null) ? null : vs.get(0);
        }
        
        public List<String> getFieldArray(final String name) {
            return this.values.get(name);
        }
        
        public Map<String, ArrayList<String>> getFields() {
            return this.values;
        }
        
        @Override
        public String toString() {
            return this.values.toString();
        }
        
        static {
            C = new ASN1ObjectIdentifier("2.5.4.6");
            O = new ASN1ObjectIdentifier("2.5.4.10");
            OU = new ASN1ObjectIdentifier("2.5.4.11");
            T = new ASN1ObjectIdentifier("2.5.4.12");
            CN = new ASN1ObjectIdentifier("2.5.4.3");
            SN = new ASN1ObjectIdentifier("2.5.4.5");
            L = new ASN1ObjectIdentifier("2.5.4.7");
            ST = new ASN1ObjectIdentifier("2.5.4.8");
            SURNAME = new ASN1ObjectIdentifier("2.5.4.4");
            GIVENNAME = new ASN1ObjectIdentifier("2.5.4.42");
            INITIALS = new ASN1ObjectIdentifier("2.5.4.43");
            GENERATION = new ASN1ObjectIdentifier("2.5.4.44");
            UNIQUE_IDENTIFIER = new ASN1ObjectIdentifier("2.5.4.45");
            EmailAddress = new ASN1ObjectIdentifier("1.2.840.113549.1.9.1");
            E = X500Name.EmailAddress;
            DC = new ASN1ObjectIdentifier("0.9.2342.19200300.100.1.25");
            UID = new ASN1ObjectIdentifier("0.9.2342.19200300.100.1.1");
            (DefaultSymbols = new HashMap<ASN1ObjectIdentifier, String>()).put(X500Name.C, "C");
            X500Name.DefaultSymbols.put(X500Name.O, "O");
            X500Name.DefaultSymbols.put(X500Name.T, "T");
            X500Name.DefaultSymbols.put(X500Name.OU, "OU");
            X500Name.DefaultSymbols.put(X500Name.CN, "CN");
            X500Name.DefaultSymbols.put(X500Name.L, "L");
            X500Name.DefaultSymbols.put(X500Name.ST, "ST");
            X500Name.DefaultSymbols.put(X500Name.SN, "SN");
            X500Name.DefaultSymbols.put(X500Name.EmailAddress, "E");
            X500Name.DefaultSymbols.put(X500Name.DC, "DC");
            X500Name.DefaultSymbols.put(X500Name.UID, "UID");
            X500Name.DefaultSymbols.put(X500Name.SURNAME, "SURNAME");
            X500Name.DefaultSymbols.put(X500Name.GIVENNAME, "GIVENNAME");
            X500Name.DefaultSymbols.put(X500Name.INITIALS, "INITIALS");
            X500Name.DefaultSymbols.put(X500Name.GENERATION, "GENERATION");
        }
    }
    
    public static class X509NameTokenizer
    {
        private String oid;
        private int index;
        private StringBuffer buf;
        
        public X509NameTokenizer(final String oid) {
            this.buf = new StringBuffer();
            this.oid = oid;
            this.index = -1;
        }
        
        public boolean hasMoreTokens() {
            return this.index != this.oid.length();
        }
        
        public String nextToken() {
            if (this.index == this.oid.length()) {
                return null;
            }
            int end = this.index + 1;
            boolean quoted = false;
            boolean escaped = false;
            this.buf.setLength(0);
            while (end != this.oid.length()) {
                final char c = this.oid.charAt(end);
                if (c == '\"') {
                    if (!escaped) {
                        quoted = !quoted;
                    }
                    else {
                        this.buf.append(c);
                    }
                    escaped = false;
                }
                else if (escaped || quoted) {
                    this.buf.append(c);
                    escaped = false;
                }
                else if (c == '\\') {
                    escaped = true;
                }
                else {
                    if (c == ',') {
                        break;
                    }
                    this.buf.append(c);
                }
                ++end;
            }
            this.index = end;
            return this.buf.toString().trim();
        }
    }
}
