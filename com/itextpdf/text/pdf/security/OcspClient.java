// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.security.cert.X509Certificate;

public interface OcspClient
{
    byte[] getEncoded(final X509Certificate p0, final X509Certificate p1, final String p2);
}
