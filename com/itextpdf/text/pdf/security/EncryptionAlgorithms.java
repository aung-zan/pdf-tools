// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.util.HashMap;

public class EncryptionAlgorithms
{
    static final HashMap<String, String> algorithmNames;
    
    public static String getAlgorithm(final String oid) {
        final String ret = EncryptionAlgorithms.algorithmNames.get(oid);
        if (ret == null) {
            return oid;
        }
        return ret;
    }
    
    static {
        (algorithmNames = new HashMap<String, String>()).put("1.2.840.113549.1.1.1", "RSA");
        EncryptionAlgorithms.algorithmNames.put("1.2.840.10040.4.1", "DSA");
        EncryptionAlgorithms.algorithmNames.put("1.2.840.113549.1.1.2", "RSA");
        EncryptionAlgorithms.algorithmNames.put("1.2.840.113549.1.1.4", "RSA");
        EncryptionAlgorithms.algorithmNames.put("1.2.840.113549.1.1.5", "RSA");
        EncryptionAlgorithms.algorithmNames.put("1.2.840.113549.1.1.14", "RSA");
        EncryptionAlgorithms.algorithmNames.put("1.2.840.113549.1.1.11", "RSA");
        EncryptionAlgorithms.algorithmNames.put("1.2.840.113549.1.1.12", "RSA");
        EncryptionAlgorithms.algorithmNames.put("1.2.840.113549.1.1.13", "RSA");
        EncryptionAlgorithms.algorithmNames.put("1.2.840.10040.4.3", "DSA");
        EncryptionAlgorithms.algorithmNames.put("2.16.840.1.101.3.4.3.1", "DSA");
        EncryptionAlgorithms.algorithmNames.put("2.16.840.1.101.3.4.3.2", "DSA");
        EncryptionAlgorithms.algorithmNames.put("1.3.14.3.2.29", "RSA");
        EncryptionAlgorithms.algorithmNames.put("1.3.36.3.3.1.2", "RSA");
        EncryptionAlgorithms.algorithmNames.put("1.3.36.3.3.1.3", "RSA");
        EncryptionAlgorithms.algorithmNames.put("1.3.36.3.3.1.4", "RSA");
        EncryptionAlgorithms.algorithmNames.put("1.2.643.2.2.19", "ECGOST3410");
    }
}
