// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import org.bouncycastle.tsp.TimeStampTokenInfo;

public interface TSAInfoBouncyCastle
{
    void inspectTimeStampTokenInfo(final TimeStampTokenInfo p0);
}
