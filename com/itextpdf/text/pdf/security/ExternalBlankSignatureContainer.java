// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.security.GeneralSecurityException;
import java.io.InputStream;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfDictionary;

public class ExternalBlankSignatureContainer implements ExternalSignatureContainer
{
    private PdfDictionary sigDic;
    
    public ExternalBlankSignatureContainer(final PdfDictionary sigDic) {
        this.sigDic = sigDic;
    }
    
    public ExternalBlankSignatureContainer(final PdfName filter, final PdfName subFilter) {
        (this.sigDic = new PdfDictionary()).put(PdfName.FILTER, filter);
        this.sigDic.put(PdfName.SUBFILTER, subFilter);
    }
    
    @Override
    public byte[] sign(final InputStream data) throws GeneralSecurityException {
        return new byte[0];
    }
    
    @Override
    public void modifySigningDictionary(final PdfDictionary signDic) {
        signDic.putAll(this.sigDic);
    }
}
