// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.log.LoggerFactory;
import java.io.IOException;
import java.util.Enumeration;
import java.security.GeneralSecurityException;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.security.cert.X509Certificate;
import java.security.KeyStore;
import com.itextpdf.text.log.Logger;

public class RootStoreVerifier extends CertificateVerifier
{
    protected static final Logger LOGGER;
    protected KeyStore rootStore;
    
    public RootStoreVerifier(final CertificateVerifier verifier) {
        super(verifier);
        this.rootStore = null;
    }
    
    public void setRootStore(final KeyStore keyStore) {
        this.rootStore = keyStore;
    }
    
    @Override
    public List<VerificationOK> verify(final X509Certificate signCert, final X509Certificate issuerCert, final Date signDate) throws GeneralSecurityException, IOException {
        RootStoreVerifier.LOGGER.info("Root store verification: " + signCert.getSubjectDN().getName());
        if (this.rootStore == null) {
            return super.verify(signCert, issuerCert, signDate);
        }
        try {
            final List<VerificationOK> result = new ArrayList<VerificationOK>();
            final Enumeration<String> aliases = this.rootStore.aliases();
            while (aliases.hasMoreElements()) {
                final String alias = aliases.nextElement();
                try {
                    if (!this.rootStore.isCertificateEntry(alias)) {
                        continue;
                    }
                    final X509Certificate anchor = (X509Certificate)this.rootStore.getCertificate(alias);
                    signCert.verify(anchor.getPublicKey());
                    RootStoreVerifier.LOGGER.info("Certificate verified against root store");
                    result.add(new VerificationOK(signCert, this.getClass(), "Certificate verified against root store."));
                    result.addAll(super.verify(signCert, issuerCert, signDate));
                    return result;
                }
                catch (GeneralSecurityException e) {
                    continue;
                }
                break;
            }
            result.addAll(super.verify(signCert, issuerCert, signDate));
            return result;
        }
        catch (GeneralSecurityException e2) {
            return super.verify(signCert, issuerCert, signDate);
        }
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(RootStoreVerifier.class);
    }
}
