// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import org.bouncycastle.asn1.esf.SignaturePolicyId;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.esf.OtherHashAlgAndValue;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.esf.SigPolicyQualifiers;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.esf.SigPolicyQualifierInfo;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.esf.SignaturePolicyIdentifier;
import com.itextpdf.text.pdf.codec.Base64;

public class SignaturePolicyInfo
{
    private String policyIdentifier;
    private byte[] policyHash;
    private String policyDigestAlgorithm;
    private String policyUri;
    
    public SignaturePolicyInfo(final String policyIdentifier, final byte[] policyHash, final String policyDigestAlgorithm, final String policyUri) {
        if (policyIdentifier == null || policyIdentifier.length() == 0) {
            throw new IllegalArgumentException("Policy identifier cannot be null");
        }
        if (policyHash == null) {
            throw new IllegalArgumentException("Policy hash cannot be null");
        }
        if (policyDigestAlgorithm == null || policyDigestAlgorithm.length() == 0) {
            throw new IllegalArgumentException("Policy digest algorithm cannot be null");
        }
        this.policyIdentifier = policyIdentifier;
        this.policyHash = policyHash;
        this.policyDigestAlgorithm = policyDigestAlgorithm;
        this.policyUri = policyUri;
    }
    
    public SignaturePolicyInfo(final String policyIdentifier, final String policyHashBase64, final String policyDigestAlgorithm, final String policyUri) {
        this(policyIdentifier, (byte[])((policyHashBase64 != null) ? Base64.decode(policyHashBase64) : null), policyDigestAlgorithm, policyUri);
    }
    
    public String getPolicyIdentifier() {
        return this.policyIdentifier;
    }
    
    public byte[] getPolicyHash() {
        return this.policyHash;
    }
    
    public String getPolicyDigestAlgorithm() {
        return this.policyDigestAlgorithm;
    }
    
    public String getPolicyUri() {
        return this.policyUri;
    }
    
    SignaturePolicyIdentifier toSignaturePolicyIdentifier() {
        final String algId = DigestAlgorithms.getAllowedDigests(this.policyDigestAlgorithm);
        if (algId == null || algId.length() == 0) {
            throw new IllegalArgumentException("Invalid policy hash algorithm");
        }
        SignaturePolicyIdentifier signaturePolicyIdentifier = null;
        SigPolicyQualifierInfo spqi = null;
        if (this.policyUri != null && this.policyUri.length() > 0) {
            spqi = new SigPolicyQualifierInfo(PKCSObjectIdentifiers.id_spq_ets_uri, new DERIA5String(this.policyUri));
        }
        final SigPolicyQualifiers qualifiers = new SigPolicyQualifiers(new SigPolicyQualifierInfo[] { spqi });
        signaturePolicyIdentifier = new SignaturePolicyIdentifier(new SignaturePolicyId(DERObjectIdentifier.getInstance(new DERObjectIdentifier(this.policyIdentifier.replace("urn:oid:", ""))), new OtherHashAlgAndValue(new AlgorithmIdentifier(algId), new DEROctetString(this.policyHash)), qualifiers));
        return signaturePolicyIdentifier;
    }
}
