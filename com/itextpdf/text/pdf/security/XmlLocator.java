// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.DocumentException;
import java.io.IOException;
import org.w3c.dom.Document;

public interface XmlLocator
{
    Document getDocument();
    
    void setDocument(final Document p0) throws IOException, DocumentException;
    
    String getEncoding();
}
