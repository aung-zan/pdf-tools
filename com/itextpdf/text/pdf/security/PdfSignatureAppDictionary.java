// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfDictionary;

public class PdfSignatureAppDictionary extends PdfDictionary
{
    public void setSignatureCreator(final String name) {
        this.put(PdfName.NAME, new PdfString(name, "UnicodeBig"));
    }
}
