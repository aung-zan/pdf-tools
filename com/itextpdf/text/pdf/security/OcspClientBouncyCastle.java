// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.log.LoggerFactory;
import java.security.GeneralSecurityException;
import com.itextpdf.text.io.StreamUtil;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.BufferedOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateEncodingException;
import org.bouncycastle.operator.OperatorException;
import org.bouncycastle.cert.ocsp.OCSPException;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.DEROctetString;
import com.itextpdf.text.pdf.PdfEncryption;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.cert.ocsp.OCSPReqBuilder;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cert.ocsp.CertificateID;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import java.security.Provider;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.cert.ocsp.OCSPReq;
import java.math.BigInteger;
import org.bouncycastle.cert.ocsp.SingleResp;
import java.io.IOException;
import com.itextpdf.text.error_messages.MessageLocalization;
import org.bouncycastle.ocsp.RevokedStatus;
import org.bouncycastle.cert.ocsp.CertificateStatus;
import org.bouncycastle.cert.ocsp.OCSPResp;
import com.itextpdf.text.log.Level;
import org.bouncycastle.cert.ocsp.BasicOCSPResp;
import java.security.cert.X509Certificate;
import com.itextpdf.text.log.Logger;

public class OcspClientBouncyCastle implements OcspClient
{
    private static final Logger LOGGER;
    private final OCSPVerifier verifier;
    
    @Deprecated
    public OcspClientBouncyCastle() {
        this.verifier = null;
    }
    
    public OcspClientBouncyCastle(final OCSPVerifier verifier) {
        this.verifier = verifier;
    }
    
    public BasicOCSPResp getBasicOCSPResp(final X509Certificate checkCert, final X509Certificate rootCert, final String url) {
        try {
            final OCSPResp ocspResponse = this.getOcspResponse(checkCert, rootCert, url);
            if (ocspResponse == null) {
                return null;
            }
            if (ocspResponse.getStatus() != 0) {
                return null;
            }
            final BasicOCSPResp basicResponse = (BasicOCSPResp)ocspResponse.getResponseObject();
            if (this.verifier != null) {
                this.verifier.isValidResponse(basicResponse, rootCert);
            }
            return basicResponse;
        }
        catch (Exception ex) {
            if (OcspClientBouncyCastle.LOGGER.isLogging(Level.ERROR)) {
                OcspClientBouncyCastle.LOGGER.error(ex.getMessage());
            }
            return null;
        }
    }
    
    @Override
    public byte[] getEncoded(final X509Certificate checkCert, final X509Certificate rootCert, final String url) {
        try {
            final BasicOCSPResp basicResponse = this.getBasicOCSPResp(checkCert, rootCert, url);
            if (basicResponse != null) {
                final SingleResp[] responses = basicResponse.getResponses();
                if (responses.length == 1) {
                    final SingleResp resp = responses[0];
                    final Object status = resp.getCertStatus();
                    if (status == CertificateStatus.GOOD) {
                        return basicResponse.getEncoded();
                    }
                    if (status instanceof RevokedStatus) {
                        throw new IOException(MessageLocalization.getComposedMessage("ocsp.status.is.revoked", new Object[0]));
                    }
                    throw new IOException(MessageLocalization.getComposedMessage("ocsp.status.is.unknown", new Object[0]));
                }
            }
        }
        catch (Exception ex) {
            if (OcspClientBouncyCastle.LOGGER.isLogging(Level.ERROR)) {
                OcspClientBouncyCastle.LOGGER.error(ex.getMessage());
            }
        }
        return null;
    }
    
    private static OCSPReq generateOCSPRequest(final X509Certificate issuerCert, final BigInteger serialNumber) throws OCSPException, IOException, OperatorException, CertificateEncodingException {
        Security.addProvider(new BouncyCastleProvider());
        final CertificateID id = new CertificateID(new JcaDigestCalculatorProviderBuilder().build().get(CertificateID.HASH_SHA1), new JcaX509CertificateHolder(issuerCert), serialNumber);
        final OCSPReqBuilder gen = new OCSPReqBuilder();
        gen.addRequest(id);
        final Extension ext = new Extension(OCSPObjectIdentifiers.id_pkix_ocsp_nonce, false, new DEROctetString(new DEROctetString(PdfEncryption.createDocumentId()).getEncoded()));
        gen.setRequestExtensions(new Extensions(new Extension[] { ext }));
        return gen.build();
    }
    
    private OCSPResp getOcspResponse(final X509Certificate checkCert, final X509Certificate rootCert, String url) throws GeneralSecurityException, OCSPException, IOException, OperatorException {
        if (checkCert == null || rootCert == null) {
            return null;
        }
        if (url == null) {
            url = CertificateUtil.getOCSPURL(checkCert);
        }
        if (url == null) {
            return null;
        }
        OcspClientBouncyCastle.LOGGER.info("Getting OCSP from " + url);
        final OCSPReq request = generateOCSPRequest(rootCert, checkCert.getSerialNumber());
        final byte[] array = request.getEncoded();
        final URL urlt = new URL(url);
        final HttpURLConnection con = (HttpURLConnection)urlt.openConnection();
        con.setRequestProperty("Content-Type", "application/ocsp-request");
        con.setRequestProperty("Accept", "application/ocsp-response");
        con.setDoOutput(true);
        final OutputStream out = con.getOutputStream();
        final DataOutputStream dataOut = new DataOutputStream(new BufferedOutputStream(out));
        dataOut.write(array);
        dataOut.flush();
        dataOut.close();
        if (con.getResponseCode() / 100 != 2) {
            throw new IOException(MessageLocalization.getComposedMessage("invalid.http.response.1", con.getResponseCode()));
        }
        final InputStream in = (InputStream)con.getContent();
        return new OCSPResp(StreamUtil.inputStreamToArray(in));
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(OcspClientBouncyCastle.class);
    }
}
