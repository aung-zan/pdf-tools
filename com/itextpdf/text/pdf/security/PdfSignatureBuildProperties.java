// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfDictionary;

public class PdfSignatureBuildProperties extends PdfDictionary
{
    public void setSignatureCreator(final String name) {
        this.getPdfSignatureAppProperty().setSignatureCreator(name);
    }
    
    private PdfSignatureAppDictionary getPdfSignatureAppProperty() {
        PdfSignatureAppDictionary appPropDic = (PdfSignatureAppDictionary)this.getAsDict(PdfName.APP);
        if (appPropDic == null) {
            appPropDic = new PdfSignatureAppDictionary();
            this.put(PdfName.APP, appPropDic);
        }
        return appPropDic;
    }
}
