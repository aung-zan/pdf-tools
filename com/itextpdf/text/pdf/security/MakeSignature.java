// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import com.itextpdf.text.log.LoggerFactory;
import com.itextpdf.text.io.RandomAccessSource;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.ByteBuffer;
import com.itextpdf.text.io.StreamUtil;
import com.itextpdf.text.io.RASInputStream;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import java.io.OutputStream;
import com.itextpdf.text.pdf.PdfReader;
import java.util.ArrayList;
import java.io.InputStream;
import java.util.Iterator;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfString;
import java.security.cert.X509Certificate;
import java.security.PrivateKey;
import java.util.HashMap;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfDate;
import com.itextpdf.text.pdf.PdfSignature;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfDeveloperExtension;
import java.security.GeneralSecurityException;
import com.itextpdf.text.DocumentException;
import java.io.IOException;
import org.bouncycastle.asn1.esf.SignaturePolicyIdentifier;
import java.util.Collection;
import java.security.cert.Certificate;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.log.Logger;

public class MakeSignature
{
    private static final Logger LOGGER;
    
    public static void signDetached(final PdfSignatureAppearance sap, final ExternalDigest externalDigest, final ExternalSignature externalSignature, final Certificate[] chain, final Collection<CrlClient> crlList, final OcspClient ocspClient, final TSAClient tsaClient, final int estimatedSize, final CryptoStandard sigtype) throws IOException, DocumentException, GeneralSecurityException {
        signDetached(sap, externalDigest, externalSignature, chain, crlList, ocspClient, tsaClient, estimatedSize, sigtype, (SignaturePolicyIdentifier)null);
    }
    
    public static void signDetached(final PdfSignatureAppearance sap, final ExternalDigest externalDigest, final ExternalSignature externalSignature, final Certificate[] chain, final Collection<CrlClient> crlList, final OcspClient ocspClient, final TSAClient tsaClient, final int estimatedSize, final CryptoStandard sigtype, final SignaturePolicyInfo signaturePolicy) throws IOException, DocumentException, GeneralSecurityException {
        signDetached(sap, externalDigest, externalSignature, chain, crlList, ocspClient, tsaClient, estimatedSize, sigtype, signaturePolicy.toSignaturePolicyIdentifier());
    }
    
    public static void signDetached(final PdfSignatureAppearance sap, final ExternalDigest externalDigest, final ExternalSignature externalSignature, final Certificate[] chain, final Collection<CrlClient> crlList, final OcspClient ocspClient, final TSAClient tsaClient, int estimatedSize, final CryptoStandard sigtype, final SignaturePolicyIdentifier signaturePolicy) throws IOException, DocumentException, GeneralSecurityException {
        Collection<byte[]> crlBytes = null;
        for (int i = 0; crlBytes == null && i < chain.length; crlBytes = processCrl(chain[i++], crlList)) {}
        if (estimatedSize == 0) {
            estimatedSize = 8192;
            if (crlBytes != null) {
                for (final byte[] element : crlBytes) {
                    estimatedSize += element.length + 10;
                }
            }
            if (ocspClient != null) {
                estimatedSize += 4192;
            }
            if (tsaClient != null) {
                estimatedSize += 4192;
            }
        }
        sap.setCertificate(chain[0]);
        if (sigtype == CryptoStandard.CADES) {
            sap.addDeveloperExtension(PdfDeveloperExtension.ESIC_1_7_EXTENSIONLEVEL2);
        }
        final PdfSignature dic = new PdfSignature(PdfName.ADOBE_PPKLITE, (sigtype == CryptoStandard.CADES) ? PdfName.ETSI_CADES_DETACHED : PdfName.ADBE_PKCS7_DETACHED);
        dic.setReason(sap.getReason());
        dic.setLocation(sap.getLocation());
        dic.setSignatureCreator(sap.getSignatureCreator());
        dic.setContact(sap.getContact());
        dic.setDate(new PdfDate(sap.getSignDate()));
        sap.setCryptoDictionary(dic);
        final HashMap<PdfName, Integer> exc = new HashMap<PdfName, Integer>();
        exc.put(PdfName.CONTENTS, new Integer(estimatedSize * 2 + 2));
        sap.preClose(exc);
        final String hashAlgorithm = externalSignature.getHashAlgorithm();
        final PdfPKCS7 sgn = new PdfPKCS7(null, chain, hashAlgorithm, null, externalDigest, false);
        if (signaturePolicy != null) {
            sgn.setSignaturePolicy(signaturePolicy);
        }
        final InputStream data = sap.getRangeStream();
        final byte[] hash = DigestAlgorithms.digest(data, externalDigest.getMessageDigest(hashAlgorithm));
        byte[] ocsp = null;
        if (chain.length >= 2 && ocspClient != null) {
            ocsp = ocspClient.getEncoded((X509Certificate)chain[0], (X509Certificate)chain[1], null);
        }
        final byte[] sh = sgn.getAuthenticatedAttributeBytes(hash, ocsp, crlBytes, sigtype);
        final byte[] extSignature = externalSignature.sign(sh);
        sgn.setExternalDigest(extSignature, null, externalSignature.getEncryptionAlgorithm());
        final byte[] encodedSig = sgn.getEncodedPKCS7(hash, tsaClient, ocsp, crlBytes, sigtype);
        if (estimatedSize < encodedSig.length) {
            throw new IOException("Not enough space");
        }
        final byte[] paddedSig = new byte[estimatedSize];
        System.arraycopy(encodedSig, 0, paddedSig, 0, encodedSig.length);
        final PdfDictionary dic2 = new PdfDictionary();
        dic2.put(PdfName.CONTENTS, new PdfString(paddedSig).setHexWriting(true));
        sap.close(dic2);
    }
    
    public static Collection<byte[]> processCrl(final Certificate cert, final Collection<CrlClient> crlList) {
        if (crlList == null) {
            return null;
        }
        final ArrayList<byte[]> crlBytes = new ArrayList<byte[]>();
        for (final CrlClient cc : crlList) {
            if (cc == null) {
                continue;
            }
            MakeSignature.LOGGER.info("Processing " + cc.getClass().getName());
            final Collection<byte[]> b = cc.getEncoded((X509Certificate)cert, null);
            if (b == null) {
                continue;
            }
            crlBytes.addAll(b);
        }
        if (crlBytes.isEmpty()) {
            return null;
        }
        return crlBytes;
    }
    
    public static void signExternalContainer(final PdfSignatureAppearance sap, final ExternalSignatureContainer externalSignatureContainer, final int estimatedSize) throws GeneralSecurityException, IOException, DocumentException {
        final PdfSignature dic = new PdfSignature(null, null);
        dic.setReason(sap.getReason());
        dic.setLocation(sap.getLocation());
        dic.setSignatureCreator(sap.getSignatureCreator());
        dic.setContact(sap.getContact());
        dic.setDate(new PdfDate(sap.getSignDate()));
        externalSignatureContainer.modifySigningDictionary(dic);
        sap.setCryptoDictionary(dic);
        final HashMap<PdfName, Integer> exc = new HashMap<PdfName, Integer>();
        exc.put(PdfName.CONTENTS, new Integer(estimatedSize * 2 + 2));
        sap.preClose(exc);
        final InputStream data = sap.getRangeStream();
        final byte[] encodedSig = externalSignatureContainer.sign(data);
        if (estimatedSize < encodedSig.length) {
            throw new IOException("Not enough space");
        }
        final byte[] paddedSig = new byte[estimatedSize];
        System.arraycopy(encodedSig, 0, paddedSig, 0, encodedSig.length);
        final PdfDictionary dic2 = new PdfDictionary();
        dic2.put(PdfName.CONTENTS, new PdfString(paddedSig).setHexWriting(true));
        sap.close(dic2);
    }
    
    public static void signDeferred(final PdfReader reader, final String fieldName, final OutputStream outs, final ExternalSignatureContainer externalSignatureContainer) throws DocumentException, IOException, GeneralSecurityException {
        final AcroFields af = reader.getAcroFields();
        final PdfDictionary v = af.getSignatureDictionary(fieldName);
        if (v == null) {
            throw new DocumentException("No field");
        }
        if (!af.signatureCoversWholeDocument(fieldName)) {
            throw new DocumentException("Not the last signature");
        }
        final PdfArray b = v.getAsArray(PdfName.BYTERANGE);
        final long[] gaps = b.asLongArray();
        if (b.size() != 4 || gaps[0] != 0L) {
            throw new DocumentException("Single exclusion space supported");
        }
        final RandomAccessSource readerSource = reader.getSafeFile().createSourceView();
        final InputStream rg = new RASInputStream(new RandomAccessSourceFactory().createRanged(readerSource, gaps));
        final byte[] signedContent = externalSignatureContainer.sign(rg);
        int spaceAvailable = (int)(gaps[2] - gaps[1]) - 2;
        if ((spaceAvailable & 0x1) != 0x0) {
            throw new DocumentException("Gap is not a multiple of 2");
        }
        spaceAvailable /= 2;
        if (spaceAvailable < signedContent.length) {
            throw new DocumentException("Not enough space");
        }
        StreamUtil.CopyBytes(readerSource, 0L, gaps[1] + 1L, outs);
        final ByteBuffer bb = new ByteBuffer(spaceAvailable * 2);
        for (final byte bi : signedContent) {
            bb.appendHex(bi);
        }
        for (int remain = (spaceAvailable - signedContent.length) * 2, k = 0; k < remain; ++k) {
            bb.append((byte)48);
        }
        bb.writeTo(outs);
        StreamUtil.CopyBytes(readerSource, gaps[2] - 1L, gaps[3] + 1L, outs);
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(MakeSignature.class);
    }
    
    public enum CryptoStandard
    {
        CMS, 
        CADES;
    }
}
