// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.util.Collection;
import java.security.cert.X509Certificate;
import com.itextpdf.text.ExceptionConverter;
import java.security.cert.X509CRL;
import java.security.cert.CRL;
import java.util.ArrayList;

public class CrlClientOffline implements CrlClient
{
    private ArrayList<byte[]> crls;
    
    public CrlClientOffline(final byte[] crlEncoded) {
        (this.crls = new ArrayList<byte[]>()).add(crlEncoded);
    }
    
    public CrlClientOffline(final CRL crl) {
        this.crls = new ArrayList<byte[]>();
        try {
            this.crls.add(((X509CRL)crl).getEncoded());
        }
        catch (Exception ex) {
            throw new ExceptionConverter(ex);
        }
    }
    
    @Override
    public Collection<byte[]> getEncoded(final X509Certificate checkCert, final String url) {
        return this.crls;
    }
}
