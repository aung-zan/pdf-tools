// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.security.GeneralSecurityException;
import java.security.MessageDigest;

public class ProviderDigest implements ExternalDigest
{
    private String provider;
    
    public ProviderDigest(final String provider) {
        this.provider = provider;
    }
    
    @Override
    public MessageDigest getMessageDigest(final String hashAlgorithm) throws GeneralSecurityException {
        return DigestAlgorithms.getMessageDigest(hashAlgorithm, this.provider);
    }
}
