// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.util.Date;
import java.util.GregorianCalendar;
import org.bouncycastle.asn1.ocsp.BasicOCSPResponse;
import org.bouncycastle.operator.DigestCalculator;
import org.bouncycastle.cert.ocsp.SingleResp;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.ocsp.CertificateID;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import java.security.cert.X509CRL;
import java.security.cert.CertificateFactory;
import java.security.GeneralSecurityException;
import org.bouncycastle.asn1.tsp.MessageImprint;
import org.bouncycastle.asn1.ASN1Enumerated;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DEROctetString;
import java.io.OutputStream;
import org.bouncycastle.asn1.ASN1OutputStream;
import java.io.ByteArrayOutputStream;
import java.security.SignatureException;
import java.security.PublicKey;
import org.bouncycastle.tsp.TimeStampTokenInfo;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ess.ESSCertIDv2;
import org.bouncycastle.asn1.ess.ESSCertID;
import java.util.Iterator;
import java.math.BigInteger;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.ess.SigningCertificateV2;
import java.util.Arrays;
import org.bouncycastle.asn1.ess.SigningCertificate;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import java.io.IOException;
import com.itextpdf.text.ExceptionConverter;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.jce.provider.X509CertParser;
import java.security.NoSuchProviderException;
import java.security.InvalidKeyException;
import java.util.HashSet;
import java.util.ArrayList;
import java.security.NoSuchAlgorithmException;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.security.PrivateKey;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.cert.ocsp.BasicOCSPResp;
import java.security.cert.CRL;
import java.security.cert.X509Certificate;
import java.security.cert.Certificate;
import java.util.Collection;
import java.security.Signature;
import com.itextpdf.text.pdf.PdfName;
import java.util.Set;
import java.security.MessageDigest;
import java.util.Calendar;
import org.bouncycastle.asn1.esf.SignaturePolicyIdentifier;

public class PdfPKCS7
{
    private String provider;
    private SignaturePolicyIdentifier signaturePolicyIdentifier;
    private String signName;
    private String reason;
    private String location;
    private Calendar signDate;
    private int version;
    private int signerversion;
    private String digestAlgorithmOid;
    private MessageDigest messageDigest;
    private Set<String> digestalgos;
    private byte[] digestAttr;
    private PdfName filterSubtype;
    private String digestEncryptionAlgorithmOid;
    private ExternalDigest interfaceDigest;
    private byte[] externalDigest;
    private byte[] externalRSAdata;
    private Signature sig;
    private byte[] digest;
    private byte[] RSAdata;
    private byte[] sigAttr;
    private byte[] sigAttrDer;
    private MessageDigest encContDigest;
    private boolean verified;
    private boolean verifyResult;
    private Collection<Certificate> certs;
    private Collection<Certificate> signCerts;
    private X509Certificate signCert;
    private Collection<CRL> crls;
    private BasicOCSPResp basicResp;
    private boolean isTsp;
    private boolean isCades;
    private TimeStampToken timeStampToken;
    
    public PdfPKCS7(final PrivateKey privKey, final Certificate[] certChain, final String hashAlgorithm, final String provider, final ExternalDigest interfaceDigest, final boolean hasRSAdata) throws InvalidKeyException, NoSuchProviderException, NoSuchAlgorithmException {
        this.version = 1;
        this.signerversion = 1;
        this.provider = provider;
        this.interfaceDigest = interfaceDigest;
        this.digestAlgorithmOid = DigestAlgorithms.getAllowedDigests(hashAlgorithm);
        if (this.digestAlgorithmOid == null) {
            throw new NoSuchAlgorithmException(MessageLocalization.getComposedMessage("unknown.hash.algorithm.1", hashAlgorithm));
        }
        this.signCert = (X509Certificate)certChain[0];
        this.certs = new ArrayList<Certificate>();
        for (final Certificate element : certChain) {
            this.certs.add(element);
        }
        (this.digestalgos = new HashSet<String>()).add(this.digestAlgorithmOid);
        if (privKey != null) {
            this.digestEncryptionAlgorithmOid = privKey.getAlgorithm();
            if (this.digestEncryptionAlgorithmOid.equals("RSA")) {
                this.digestEncryptionAlgorithmOid = "1.2.840.113549.1.1.1";
            }
            else {
                if (!this.digestEncryptionAlgorithmOid.equals("DSA")) {
                    throw new NoSuchAlgorithmException(MessageLocalization.getComposedMessage("unknown.key.algorithm.1", this.digestEncryptionAlgorithmOid));
                }
                this.digestEncryptionAlgorithmOid = "1.2.840.10040.4.1";
            }
        }
        if (hasRSAdata) {
            this.RSAdata = new byte[0];
            this.messageDigest = DigestAlgorithms.getMessageDigest(this.getHashAlgorithm(), provider);
        }
        if (privKey != null) {
            this.sig = this.initSignature(privKey);
        }
    }
    
    public PdfPKCS7(final byte[] contentsKey, final byte[] certsKey, final String provider) {
        this.version = 1;
        this.signerversion = 1;
        try {
            this.provider = provider;
            final X509CertParser cr = new X509CertParser();
            cr.engineInit(new ByteArrayInputStream(certsKey));
            this.certs = (Collection<Certificate>)cr.engineReadAll();
            this.signCerts = this.certs;
            this.signCert = this.certs.iterator().next();
            this.crls = new ArrayList<CRL>();
            final ASN1InputStream in = new ASN1InputStream(new ByteArrayInputStream(contentsKey));
            this.digest = ((ASN1OctetString)in.readObject()).getOctets();
            if (provider == null) {
                this.sig = Signature.getInstance("SHA1withRSA");
            }
            else {
                this.sig = Signature.getInstance("SHA1withRSA", provider);
            }
            this.sig.initVerify(this.signCert.getPublicKey());
            this.digestAlgorithmOid = "1.2.840.10040.4.3";
            this.digestEncryptionAlgorithmOid = "1.3.36.3.3.1.2";
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public PdfPKCS7(final byte[] contentsKey, final PdfName filterSubtype, final String provider) {
        this.version = 1;
        this.signerversion = 1;
        this.filterSubtype = filterSubtype;
        this.isTsp = PdfName.ETSI_RFC3161.equals(filterSubtype);
        this.isCades = PdfName.ETSI_CADES_DETACHED.equals(filterSubtype);
        try {
            this.provider = provider;
            final ASN1InputStream din = new ASN1InputStream(new ByteArrayInputStream(contentsKey));
            ASN1Primitive pkcs;
            try {
                pkcs = din.readObject();
            }
            catch (IOException e3) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("can.t.decode.pkcs7signeddata.object", new Object[0]));
            }
            if (!(pkcs instanceof ASN1Sequence)) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("not.a.valid.pkcs.7.object.not.a.sequence", new Object[0]));
            }
            final ASN1Sequence signedData = (ASN1Sequence)pkcs;
            final ASN1ObjectIdentifier objId = (ASN1ObjectIdentifier)signedData.getObjectAt(0);
            if (!objId.getId().equals("1.2.840.113549.1.7.2")) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("not.a.valid.pkcs.7.object.not.signed.data", new Object[0]));
            }
            final ASN1Sequence content = (ASN1Sequence)((ASN1TaggedObject)signedData.getObjectAt(1)).getObject();
            this.version = ((ASN1Integer)content.getObjectAt(0)).getValue().intValue();
            this.digestalgos = new HashSet<String>();
            final Enumeration<ASN1Sequence> e = (Enumeration<ASN1Sequence>)((ASN1Set)content.getObjectAt(1)).getObjects();
            while (e.hasMoreElements()) {
                final ASN1Sequence s = e.nextElement();
                final ASN1ObjectIdentifier o = (ASN1ObjectIdentifier)s.getObjectAt(0);
                this.digestalgos.add(o.getId());
            }
            final ASN1Sequence rsaData = (ASN1Sequence)content.getObjectAt(2);
            if (rsaData.size() > 1) {
                final ASN1OctetString rsaDataContent = (ASN1OctetString)((ASN1TaggedObject)rsaData.getObjectAt(1)).getObject();
                this.RSAdata = rsaDataContent.getOctets();
            }
            int next;
            for (next = 3; content.getObjectAt(next) instanceof ASN1TaggedObject; ++next) {}
            final X509CertParser cr = new X509CertParser();
            cr.engineInit(new ByteArrayInputStream(contentsKey));
            this.certs = (Collection<Certificate>)cr.engineReadAll();
            final ASN1Set signerInfos = (ASN1Set)content.getObjectAt(next);
            if (signerInfos.size() != 1) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("this.pkcs.7.object.has.multiple.signerinfos.only.one.is.supported.at.this.time", new Object[0]));
            }
            final ASN1Sequence signerInfo = (ASN1Sequence)signerInfos.getObjectAt(0);
            this.signerversion = ((ASN1Integer)signerInfo.getObjectAt(0)).getValue().intValue();
            final ASN1Sequence issuerAndSerialNumber = (ASN1Sequence)signerInfo.getObjectAt(1);
            final X509Principal issuer = new X509Principal(issuerAndSerialNumber.getObjectAt(0).toASN1Primitive().getEncoded());
            final BigInteger serialNumber = ((ASN1Integer)issuerAndSerialNumber.getObjectAt(1)).getValue();
            for (final Object element : this.certs) {
                final X509Certificate cert = (X509Certificate)element;
                if (cert.getIssuerDN().equals(issuer) && serialNumber.equals(cert.getSerialNumber())) {
                    this.signCert = cert;
                    break;
                }
            }
            if (this.signCert == null) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("can.t.find.signing.certificate.with.serial.1", issuer.getName() + " / " + serialNumber.toString(16)));
            }
            this.signCertificateChain();
            this.digestAlgorithmOid = ((ASN1ObjectIdentifier)((ASN1Sequence)signerInfo.getObjectAt(2)).getObjectAt(0)).getId();
            next = 3;
            boolean foundCades = false;
            if (signerInfo.getObjectAt(next) instanceof ASN1TaggedObject) {
                final ASN1TaggedObject tagsig = (ASN1TaggedObject)signerInfo.getObjectAt(next);
                final ASN1Set sseq = ASN1Set.getInstance(tagsig, false);
                this.sigAttr = sseq.getEncoded();
                this.sigAttrDer = sseq.getEncoded("DER");
                for (int k = 0; k < sseq.size(); ++k) {
                    final ASN1Sequence seq2 = (ASN1Sequence)sseq.getObjectAt(k);
                    final String idSeq2 = ((ASN1ObjectIdentifier)seq2.getObjectAt(0)).getId();
                    if (idSeq2.equals("1.2.840.113549.1.9.4")) {
                        final ASN1Set set = (ASN1Set)seq2.getObjectAt(1);
                        this.digestAttr = ((ASN1OctetString)set.getObjectAt(0)).getOctets();
                    }
                    else if (idSeq2.equals("1.2.840.113583.1.1.8")) {
                        final ASN1Set setout = (ASN1Set)seq2.getObjectAt(1);
                        final ASN1Sequence seqout = (ASN1Sequence)setout.getObjectAt(0);
                        for (int j = 0; j < seqout.size(); ++j) {
                            final ASN1TaggedObject tg = (ASN1TaggedObject)seqout.getObjectAt(j);
                            if (tg.getTagNo() == 0) {
                                final ASN1Sequence seqin = (ASN1Sequence)tg.getObject();
                                this.findCRL(seqin);
                            }
                            if (tg.getTagNo() == 1) {
                                final ASN1Sequence seqin = (ASN1Sequence)tg.getObject();
                                this.findOcsp(seqin);
                            }
                        }
                    }
                    else if (this.isCades && idSeq2.equals("1.2.840.113549.1.9.16.2.12")) {
                        final ASN1Set setout = (ASN1Set)seq2.getObjectAt(1);
                        final ASN1Sequence seqout = (ASN1Sequence)setout.getObjectAt(0);
                        final SigningCertificate sv2 = SigningCertificate.getInstance(seqout);
                        final ESSCertID[] cerv2m = sv2.getCerts();
                        final ESSCertID cerv2 = cerv2m[0];
                        final byte[] enc2 = this.signCert.getEncoded();
                        final MessageDigest m2 = new BouncyCastleDigest().getMessageDigest("SHA-1");
                        final byte[] signCertHash = m2.digest(enc2);
                        final byte[] hs2 = cerv2.getCertHash();
                        if (!Arrays.equals(signCertHash, hs2)) {
                            throw new IllegalArgumentException("Signing certificate doesn't match the ESS information.");
                        }
                        foundCades = true;
                    }
                    else if (this.isCades && idSeq2.equals("1.2.840.113549.1.9.16.2.47")) {
                        final ASN1Set setout = (ASN1Set)seq2.getObjectAt(1);
                        final ASN1Sequence seqout = (ASN1Sequence)setout.getObjectAt(0);
                        final SigningCertificateV2 sv3 = SigningCertificateV2.getInstance(seqout);
                        final ESSCertIDv2[] cerv2m2 = sv3.getCerts();
                        final ESSCertIDv2 cerv3 = cerv2m2[0];
                        final AlgorithmIdentifier ai2 = cerv3.getHashAlgorithm();
                        final byte[] enc3 = this.signCert.getEncoded();
                        final MessageDigest m3 = new BouncyCastleDigest().getMessageDigest(DigestAlgorithms.getDigest(ai2.getAlgorithm().getId()));
                        final byte[] signCertHash2 = m3.digest(enc3);
                        final byte[] hs3 = cerv3.getCertHash();
                        if (!Arrays.equals(signCertHash2, hs3)) {
                            throw new IllegalArgumentException("Signing certificate doesn't match the ESS information.");
                        }
                        foundCades = true;
                    }
                }
                if (this.digestAttr == null) {
                    throw new IllegalArgumentException(MessageLocalization.getComposedMessage("authenticated.attribute.is.missing.the.digest", new Object[0]));
                }
                ++next;
            }
            if (this.isCades && !foundCades) {
                throw new IllegalArgumentException("CAdES ESS information missing.");
            }
            this.digestEncryptionAlgorithmOid = ((ASN1ObjectIdentifier)((ASN1Sequence)signerInfo.getObjectAt(next++)).getObjectAt(0)).getId();
            this.digest = ((ASN1OctetString)signerInfo.getObjectAt(next++)).getOctets();
            if (next < signerInfo.size() && signerInfo.getObjectAt(next) instanceof ASN1TaggedObject) {
                final ASN1TaggedObject taggedObject = (ASN1TaggedObject)signerInfo.getObjectAt(next);
                final ASN1Set unat = ASN1Set.getInstance(taggedObject, false);
                final AttributeTable attble = new AttributeTable(unat);
                final Attribute ts = attble.get(PKCSObjectIdentifiers.id_aa_signatureTimeStampToken);
                if (ts != null && ts.getAttrValues().size() > 0) {
                    final ASN1Set attributeValues = ts.getAttrValues();
                    final ASN1Sequence tokenSequence = ASN1Sequence.getInstance(attributeValues.getObjectAt(0));
                    final ContentInfo contentInfo = new ContentInfo(tokenSequence);
                    this.timeStampToken = new TimeStampToken(contentInfo);
                }
            }
            if (this.isTsp) {
                final ContentInfo contentInfoTsp = new ContentInfo(signedData);
                this.timeStampToken = new TimeStampToken(contentInfoTsp);
                final TimeStampTokenInfo info = this.timeStampToken.getTimeStampInfo();
                final String algOID = info.getMessageImprintAlgOID().getId();
                this.messageDigest = DigestAlgorithms.getMessageDigestFromOid(algOID, null);
            }
            else {
                if (this.RSAdata != null || this.digestAttr != null) {
                    if (PdfName.ADBE_PKCS7_SHA1.equals(this.getFilterSubtype())) {
                        this.messageDigest = DigestAlgorithms.getMessageDigest("SHA1", provider);
                    }
                    else {
                        this.messageDigest = DigestAlgorithms.getMessageDigest(this.getHashAlgorithm(), provider);
                    }
                    this.encContDigest = DigestAlgorithms.getMessageDigest(this.getHashAlgorithm(), provider);
                }
                this.sig = this.initSignature(this.signCert.getPublicKey());
            }
        }
        catch (Exception e2) {
            throw new ExceptionConverter(e2);
        }
    }
    
    public void setSignaturePolicy(final SignaturePolicyInfo signaturePolicy) {
        this.signaturePolicyIdentifier = signaturePolicy.toSignaturePolicyIdentifier();
    }
    
    public void setSignaturePolicy(final SignaturePolicyIdentifier signaturePolicy) {
        this.signaturePolicyIdentifier = signaturePolicy;
    }
    
    public String getSignName() {
        return this.signName;
    }
    
    public void setSignName(final String signName) {
        this.signName = signName;
    }
    
    public String getReason() {
        return this.reason;
    }
    
    public void setReason(final String reason) {
        this.reason = reason;
    }
    
    public String getLocation() {
        return this.location;
    }
    
    public void setLocation(final String location) {
        this.location = location;
    }
    
    public Calendar getSignDate() {
        final Calendar dt = this.getTimeStampDate();
        if (dt == null) {
            return this.signDate;
        }
        return dt;
    }
    
    public void setSignDate(final Calendar signDate) {
        this.signDate = signDate;
    }
    
    public int getVersion() {
        return this.version;
    }
    
    public int getSigningInfoVersion() {
        return this.signerversion;
    }
    
    public String getDigestAlgorithmOid() {
        return this.digestAlgorithmOid;
    }
    
    public String getHashAlgorithm() {
        return DigestAlgorithms.getDigest(this.digestAlgorithmOid);
    }
    
    public String getDigestEncryptionAlgorithmOid() {
        return this.digestEncryptionAlgorithmOid;
    }
    
    public String getDigestAlgorithm() {
        return this.getHashAlgorithm() + "with" + this.getEncryptionAlgorithm();
    }
    
    public void setExternalDigest(final byte[] digest, final byte[] RSAdata, final String digestEncryptionAlgorithm) {
        this.externalDigest = digest;
        this.externalRSAdata = RSAdata;
        if (digestEncryptionAlgorithm != null) {
            if (digestEncryptionAlgorithm.equals("RSA")) {
                this.digestEncryptionAlgorithmOid = "1.2.840.113549.1.1.1";
            }
            else if (digestEncryptionAlgorithm.equals("DSA")) {
                this.digestEncryptionAlgorithmOid = "1.2.840.10040.4.1";
            }
            else {
                if (!digestEncryptionAlgorithm.equals("ECDSA")) {
                    throw new ExceptionConverter(new NoSuchAlgorithmException(MessageLocalization.getComposedMessage("unknown.key.algorithm.1", digestEncryptionAlgorithm)));
                }
                this.digestEncryptionAlgorithmOid = "1.2.840.10045.2.1";
            }
        }
    }
    
    private Signature initSignature(final PrivateKey key) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        Signature signature;
        if (this.provider == null) {
            signature = Signature.getInstance(this.getDigestAlgorithm());
        }
        else {
            signature = Signature.getInstance(this.getDigestAlgorithm(), this.provider);
        }
        signature.initSign(key);
        return signature;
    }
    
    private Signature initSignature(final PublicKey key) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        String digestAlgorithm = this.getDigestAlgorithm();
        if (PdfName.ADBE_X509_RSA_SHA1.equals(this.getFilterSubtype())) {
            digestAlgorithm = "SHA1withRSA";
        }
        Signature signature;
        if (this.provider == null) {
            signature = Signature.getInstance(digestAlgorithm);
        }
        else {
            signature = Signature.getInstance(digestAlgorithm, this.provider);
        }
        signature.initVerify(key);
        return signature;
    }
    
    public void update(final byte[] buf, final int off, final int len) throws SignatureException {
        if (this.RSAdata != null || this.digestAttr != null || this.isTsp) {
            this.messageDigest.update(buf, off, len);
        }
        else {
            this.sig.update(buf, off, len);
        }
    }
    
    public byte[] getEncodedPKCS1() {
        try {
            if (this.externalDigest != null) {
                this.digest = this.externalDigest;
            }
            else {
                this.digest = this.sig.sign();
            }
            final ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            final ASN1OutputStream dout = new ASN1OutputStream(bOut);
            dout.writeObject(new DEROctetString(this.digest));
            dout.close();
            return bOut.toByteArray();
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public byte[] getEncodedPKCS7() {
        return this.getEncodedPKCS7(null, null, null, null, MakeSignature.CryptoStandard.CMS);
    }
    
    public byte[] getEncodedPKCS7(final byte[] secondDigest) {
        return this.getEncodedPKCS7(secondDigest, null, null, null, MakeSignature.CryptoStandard.CMS);
    }
    
    public byte[] getEncodedPKCS7(final byte[] secondDigest, final TSAClient tsaClient, final byte[] ocsp, final Collection<byte[]> crlBytes, final MakeSignature.CryptoStandard sigtype) {
        try {
            if (this.externalDigest != null) {
                this.digest = this.externalDigest;
                if (this.RSAdata != null) {
                    this.RSAdata = this.externalRSAdata;
                }
            }
            else if (this.externalRSAdata != null && this.RSAdata != null) {
                this.RSAdata = this.externalRSAdata;
                this.sig.update(this.RSAdata);
                this.digest = this.sig.sign();
            }
            else {
                if (this.RSAdata != null) {
                    this.RSAdata = this.messageDigest.digest();
                    this.sig.update(this.RSAdata);
                }
                this.digest = this.sig.sign();
            }
            final ASN1EncodableVector digestAlgorithms = new ASN1EncodableVector();
            for (final Object element : this.digestalgos) {
                final ASN1EncodableVector algos = new ASN1EncodableVector();
                algos.add(new ASN1ObjectIdentifier((String)element));
                algos.add(DERNull.INSTANCE);
                digestAlgorithms.add(new DERSequence(algos));
            }
            ASN1EncodableVector v = new ASN1EncodableVector();
            v.add(new ASN1ObjectIdentifier("1.2.840.113549.1.7.1"));
            if (this.RSAdata != null) {
                v.add(new DERTaggedObject(0, new DEROctetString(this.RSAdata)));
            }
            final DERSequence contentinfo = new DERSequence(v);
            v = new ASN1EncodableVector();
            for (final Object element2 : this.certs) {
                final ASN1InputStream tempstream = new ASN1InputStream(new ByteArrayInputStream(((X509Certificate)element2).getEncoded()));
                v.add(tempstream.readObject());
            }
            final DERSet dercertificates = new DERSet(v);
            final ASN1EncodableVector signerinfo = new ASN1EncodableVector();
            signerinfo.add(new ASN1Integer(this.signerversion));
            v = new ASN1EncodableVector();
            v.add(CertificateInfo.getIssuer(this.signCert.getTBSCertificate()));
            v.add(new ASN1Integer(this.signCert.getSerialNumber()));
            signerinfo.add(new DERSequence(v));
            v = new ASN1EncodableVector();
            v.add(new ASN1ObjectIdentifier(this.digestAlgorithmOid));
            v.add(new DERNull());
            signerinfo.add(new DERSequence(v));
            if (secondDigest != null) {
                signerinfo.add(new DERTaggedObject(false, 0, this.getAuthenticatedAttributeSet(secondDigest, ocsp, crlBytes, sigtype)));
            }
            v = new ASN1EncodableVector();
            v.add(new ASN1ObjectIdentifier(this.digestEncryptionAlgorithmOid));
            v.add(new DERNull());
            signerinfo.add(new DERSequence(v));
            signerinfo.add(new DEROctetString(this.digest));
            if (tsaClient != null) {
                final byte[] tsImprint = tsaClient.getMessageDigest().digest(this.digest);
                final byte[] tsToken = tsaClient.getTimeStampToken(tsImprint);
                if (tsToken != null) {
                    final ASN1EncodableVector unauthAttributes = this.buildUnauthenticatedAttributes(tsToken);
                    if (unauthAttributes != null) {
                        signerinfo.add(new DERTaggedObject(false, 1, new DERSet(unauthAttributes)));
                    }
                }
            }
            final ASN1EncodableVector body = new ASN1EncodableVector();
            body.add(new ASN1Integer(this.version));
            body.add(new DERSet(digestAlgorithms));
            body.add(contentinfo);
            body.add(new DERTaggedObject(false, 0, dercertificates));
            body.add(new DERSet(new DERSequence(signerinfo)));
            final ASN1EncodableVector whole = new ASN1EncodableVector();
            whole.add(new ASN1ObjectIdentifier("1.2.840.113549.1.7.2"));
            whole.add(new DERTaggedObject(0, new DERSequence(body)));
            final ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            final ASN1OutputStream dout = new ASN1OutputStream(bOut);
            dout.writeObject(new DERSequence(whole));
            dout.close();
            return bOut.toByteArray();
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    private ASN1EncodableVector buildUnauthenticatedAttributes(final byte[] timeStampToken) throws IOException {
        if (timeStampToken == null) {
            return null;
        }
        final String ID_TIME_STAMP_TOKEN = "1.2.840.113549.1.9.16.2.14";
        final ASN1InputStream tempstream = new ASN1InputStream(new ByteArrayInputStream(timeStampToken));
        final ASN1EncodableVector unauthAttributes = new ASN1EncodableVector();
        final ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(new ASN1ObjectIdentifier(ID_TIME_STAMP_TOKEN));
        final ASN1Sequence seq = (ASN1Sequence)tempstream.readObject();
        v.add(new DERSet(seq));
        unauthAttributes.add(new DERSequence(v));
        return unauthAttributes;
    }
    
    public byte[] getAuthenticatedAttributeBytes(final byte[] secondDigest, final byte[] ocsp, final Collection<byte[]> crlBytes, final MakeSignature.CryptoStandard sigtype) {
        try {
            return this.getAuthenticatedAttributeSet(secondDigest, ocsp, crlBytes, sigtype).getEncoded("DER");
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    private DERSet getAuthenticatedAttributeSet(final byte[] secondDigest, final byte[] ocsp, final Collection<byte[]> crlBytes, final MakeSignature.CryptoStandard sigtype) {
        try {
            final ASN1EncodableVector attribute = new ASN1EncodableVector();
            ASN1EncodableVector v = new ASN1EncodableVector();
            v.add(new ASN1ObjectIdentifier("1.2.840.113549.1.9.3"));
            v.add(new DERSet(new ASN1ObjectIdentifier("1.2.840.113549.1.7.1")));
            attribute.add(new DERSequence(v));
            v = new ASN1EncodableVector();
            v.add(new ASN1ObjectIdentifier("1.2.840.113549.1.9.4"));
            v.add(new DERSet(new DEROctetString(secondDigest)));
            attribute.add(new DERSequence(v));
            boolean haveCrl = false;
            if (crlBytes != null) {
                for (final byte[] bCrl : crlBytes) {
                    if (bCrl != null) {
                        haveCrl = true;
                        break;
                    }
                }
            }
            if (ocsp != null || haveCrl) {
                v = new ASN1EncodableVector();
                v.add(new ASN1ObjectIdentifier("1.2.840.113583.1.1.8"));
                final ASN1EncodableVector revocationV = new ASN1EncodableVector();
                if (haveCrl) {
                    final ASN1EncodableVector v2 = new ASN1EncodableVector();
                    for (final byte[] bCrl2 : crlBytes) {
                        if (bCrl2 == null) {
                            continue;
                        }
                        final ASN1InputStream t = new ASN1InputStream(new ByteArrayInputStream(bCrl2));
                        v2.add(t.readObject());
                    }
                    revocationV.add(new DERTaggedObject(true, 0, new DERSequence(v2)));
                }
                if (ocsp != null) {
                    final DEROctetString doctet = new DEROctetString(ocsp);
                    final ASN1EncodableVector vo1 = new ASN1EncodableVector();
                    final ASN1EncodableVector v3 = new ASN1EncodableVector();
                    v3.add(OCSPObjectIdentifiers.id_pkix_ocsp_basic);
                    v3.add(doctet);
                    final ASN1Enumerated den = new ASN1Enumerated(0);
                    final ASN1EncodableVector v4 = new ASN1EncodableVector();
                    v4.add(den);
                    v4.add(new DERTaggedObject(true, 0, new DERSequence(v3)));
                    vo1.add(new DERSequence(v4));
                    revocationV.add(new DERTaggedObject(true, 1, new DERSequence(vo1)));
                }
                v.add(new DERSet(new DERSequence(revocationV)));
                attribute.add(new DERSequence(v));
            }
            if (sigtype == MakeSignature.CryptoStandard.CADES) {
                v = new ASN1EncodableVector();
                v.add(new ASN1ObjectIdentifier("1.2.840.113549.1.9.16.2.47"));
                final ASN1EncodableVector aaV2 = new ASN1EncodableVector();
                final String sha256Oid = DigestAlgorithms.getAllowedDigests("SHA-256");
                if (!sha256Oid.equals(this.digestAlgorithmOid)) {
                    final AlgorithmIdentifier algoId = new AlgorithmIdentifier(new ASN1ObjectIdentifier(this.digestAlgorithmOid));
                    aaV2.add(algoId);
                }
                final MessageDigest md = this.interfaceDigest.getMessageDigest(this.getHashAlgorithm());
                final byte[] dig = md.digest(this.signCert.getEncoded());
                aaV2.add(new DEROctetString(dig));
                v.add(new DERSet(new DERSequence(new DERSequence(new DERSequence(aaV2)))));
                attribute.add(new DERSequence(v));
            }
            if (this.signaturePolicyIdentifier != null) {
                attribute.add(new Attribute(PKCSObjectIdentifiers.id_aa_ets_sigPolicyId, new DERSet(this.signaturePolicyIdentifier)));
            }
            return new DERSet(attribute);
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public boolean verify() throws GeneralSecurityException {
        if (this.verified) {
            return this.verifyResult;
        }
        if (this.isTsp) {
            final TimeStampTokenInfo info = this.timeStampToken.getTimeStampInfo();
            final MessageImprint imprint = info.toASN1Structure().getMessageImprint();
            final byte[] md = this.messageDigest.digest();
            final byte[] imphashed = imprint.getHashedMessage();
            this.verifyResult = Arrays.equals(md, imphashed);
        }
        else if (this.sigAttr != null || this.sigAttrDer != null) {
            final byte[] msgDigestBytes = this.messageDigest.digest();
            boolean verifyRSAdata = true;
            boolean encContDigestCompare = false;
            if (this.RSAdata != null) {
                verifyRSAdata = Arrays.equals(msgDigestBytes, this.RSAdata);
                this.encContDigest.update(this.RSAdata);
                encContDigestCompare = Arrays.equals(this.encContDigest.digest(), this.digestAttr);
            }
            final boolean absentEncContDigestCompare = Arrays.equals(msgDigestBytes, this.digestAttr);
            final boolean concludingDigestCompare = absentEncContDigestCompare || encContDigestCompare;
            final boolean sigVerify = this.verifySigAttributes(this.sigAttr) || this.verifySigAttributes(this.sigAttrDer);
            this.verifyResult = (concludingDigestCompare && sigVerify && verifyRSAdata);
        }
        else {
            if (this.RSAdata != null) {
                this.sig.update(this.messageDigest.digest());
            }
            this.verifyResult = this.sig.verify(this.digest);
        }
        this.verified = true;
        return this.verifyResult;
    }
    
    private boolean verifySigAttributes(final byte[] attr) throws GeneralSecurityException {
        final Signature signature = this.initSignature(this.signCert.getPublicKey());
        signature.update(attr);
        return signature.verify(this.digest);
    }
    
    public boolean verifyTimestampImprint() throws GeneralSecurityException {
        if (this.timeStampToken == null) {
            return false;
        }
        final TimeStampTokenInfo info = this.timeStampToken.getTimeStampInfo();
        final MessageImprint imprint = info.toASN1Structure().getMessageImprint();
        final String algOID = info.getMessageImprintAlgOID().getId();
        final byte[] md = new BouncyCastleDigest().getMessageDigest(DigestAlgorithms.getDigest(algOID)).digest(this.digest);
        final byte[] imphashed = imprint.getHashedMessage();
        final boolean res = Arrays.equals(md, imphashed);
        return res;
    }
    
    public Certificate[] getCertificates() {
        return this.certs.toArray(new X509Certificate[this.certs.size()]);
    }
    
    public Certificate[] getSignCertificateChain() {
        return this.signCerts.toArray(new X509Certificate[this.signCerts.size()]);
    }
    
    public X509Certificate getSigningCertificate() {
        return this.signCert;
    }
    
    private void signCertificateChain() {
        final ArrayList<Certificate> cc = new ArrayList<Certificate>();
        cc.add(this.signCert);
        final ArrayList<Certificate> oc = new ArrayList<Certificate>(this.certs);
        for (int k = 0; k < oc.size(); ++k) {
            if (this.signCert.equals(oc.get(k))) {
                oc.remove(k);
                --k;
            }
        }
        boolean found = true;
        while (found) {
            final X509Certificate v = cc.get(cc.size() - 1);
            found = false;
            int i = 0;
            while (i < oc.size()) {
                final X509Certificate issuer = oc.get(i);
                try {
                    if (this.provider == null) {
                        v.verify(issuer.getPublicKey());
                    }
                    else {
                        v.verify(issuer.getPublicKey(), this.provider);
                    }
                    found = true;
                    cc.add(oc.get(i));
                    oc.remove(i);
                }
                catch (Exception ex) {
                    ++i;
                    continue;
                }
                break;
            }
        }
        this.signCerts = cc;
    }
    
    public Collection<CRL> getCRLs() {
        return this.crls;
    }
    
    private void findCRL(final ASN1Sequence seq) {
        try {
            this.crls = new ArrayList<CRL>();
            for (int k = 0; k < seq.size(); ++k) {
                final ByteArrayInputStream ar = new ByteArrayInputStream(seq.getObjectAt(k).toASN1Primitive().getEncoded("DER"));
                final CertificateFactory cf = CertificateFactory.getInstance("X.509");
                final X509CRL crl = (X509CRL)cf.generateCRL(ar);
                this.crls.add(crl);
            }
        }
        catch (Exception ex) {}
    }
    
    public BasicOCSPResp getOcsp() {
        return this.basicResp;
    }
    
    public boolean isRevocationValid() {
        if (this.basicResp == null) {
            return false;
        }
        if (this.signCerts.size() < 2) {
            return false;
        }
        try {
            final X509Certificate[] cs = (X509Certificate[])this.getSignCertificateChain();
            final SingleResp sr = this.basicResp.getResponses()[0];
            final CertificateID cid = sr.getCertID();
            final DigestCalculator digestalg = new JcaDigestCalculatorProviderBuilder().build().get(new AlgorithmIdentifier(cid.getHashAlgOID(), DERNull.INSTANCE));
            final X509Certificate sigcer = this.getSigningCertificate();
            final X509Certificate isscer = cs[1];
            final CertificateID tis = new CertificateID(digestalg, new JcaX509CertificateHolder(isscer), sigcer.getSerialNumber());
            return tis.equals(cid);
        }
        catch (Exception ex) {
            return false;
        }
    }
    
    private void findOcsp(ASN1Sequence seq) throws IOException {
        this.basicResp = null;
        boolean ret = false;
        while (!(seq.getObjectAt(0) instanceof ASN1ObjectIdentifier) || !((ASN1ObjectIdentifier)seq.getObjectAt(0)).getId().equals(OCSPObjectIdentifiers.id_pkix_ocsp_basic.getId())) {
            ret = true;
            int k = 0;
            while (k < seq.size()) {
                if (seq.getObjectAt(k) instanceof ASN1Sequence) {
                    seq = (ASN1Sequence)seq.getObjectAt(0);
                    ret = false;
                    break;
                }
                if (seq.getObjectAt(k) instanceof ASN1TaggedObject) {
                    final ASN1TaggedObject tag = (ASN1TaggedObject)seq.getObjectAt(k);
                    if (tag.getObject() instanceof ASN1Sequence) {
                        seq = (ASN1Sequence)tag.getObject();
                        ret = false;
                        break;
                    }
                    return;
                }
                else {
                    ++k;
                }
            }
            if (ret) {
                return;
            }
        }
        final ASN1OctetString os = (ASN1OctetString)seq.getObjectAt(1);
        final ASN1InputStream inp = new ASN1InputStream(os.getOctets());
        final BasicOCSPResponse resp = BasicOCSPResponse.getInstance(inp.readObject());
        this.basicResp = new BasicOCSPResp(resp);
    }
    
    public boolean isTsp() {
        return this.isTsp;
    }
    
    public TimeStampToken getTimeStampToken() {
        return this.timeStampToken;
    }
    
    public Calendar getTimeStampDate() {
        if (this.timeStampToken == null) {
            return null;
        }
        final Calendar cal = new GregorianCalendar();
        final Date date = this.timeStampToken.getTimeStampInfo().getGenTime();
        cal.setTime(date);
        return cal;
    }
    
    public PdfName getFilterSubtype() {
        return this.filterSubtype;
    }
    
    public String getEncryptionAlgorithm() {
        String encryptAlgo = EncryptionAlgorithms.getAlgorithm(this.digestEncryptionAlgorithmOid);
        if (encryptAlgo == null) {
            encryptAlgo = this.digestEncryptionAlgorithmOid;
        }
        return encryptAlgo;
    }
}
