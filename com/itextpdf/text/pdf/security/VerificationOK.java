// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.security;

import java.security.cert.X509Certificate;

public class VerificationOK
{
    protected X509Certificate certificate;
    protected Class<? extends CertificateVerifier> verifierClass;
    protected String message;
    
    public VerificationOK(final X509Certificate certificate, final Class<? extends CertificateVerifier> verifierClass, final String message) {
        this.certificate = certificate;
        this.verifierClass = verifierClass;
        this.message = message;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        if (this.certificate != null) {
            sb.append(this.certificate.getSubjectDN().getName());
            sb.append(" verified with ");
        }
        sb.append(this.verifierClass.getName());
        sb.append(": ");
        sb.append(this.message);
        return sb.toString();
    }
}
