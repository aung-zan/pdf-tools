// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

public class PdfPTableHeader extends PdfPTableBody
{
    protected PdfName role;
    
    public PdfPTableHeader() {
        this.role = PdfName.THEAD;
    }
    
    @Override
    public PdfName getRole() {
        return this.role;
    }
    
    @Override
    public void setRole(final PdfName role) {
        this.role = role;
    }
}
