// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.interfaces;

import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfName;

public interface PdfViewerPreferences
{
    void setViewerPreferences(final int p0);
    
    void addViewerPreference(final PdfName p0, final PdfObject p1);
}
