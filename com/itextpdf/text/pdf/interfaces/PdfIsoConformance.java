// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.interfaces;

public interface PdfIsoConformance
{
    boolean isPdfIso();
    
    void checkPdfIsoConformance(final int p0, final Object p1);
}
