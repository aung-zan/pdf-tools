// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.interfaces;

public interface PdfXConformance extends PdfIsoConformance
{
    void setPDFXConformance(final int p0);
    
    int getPDFXConformance();
    
    boolean isPdfX();
}
