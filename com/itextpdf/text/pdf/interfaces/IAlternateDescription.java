// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.interfaces;

public interface IAlternateDescription
{
    String getAlt();
    
    void setAlt(final String p0);
}
