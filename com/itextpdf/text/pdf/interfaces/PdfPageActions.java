// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.interfaces;

import com.itextpdf.text.pdf.PdfTransition;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfName;

public interface PdfPageActions
{
    void setPageAction(final PdfName p0, final PdfAction p1) throws DocumentException;
    
    void setDuration(final int p0);
    
    void setTransition(final PdfTransition p0);
}
