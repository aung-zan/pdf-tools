// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.interfaces;

public interface PdfRunDirection
{
    void setRunDirection(final int p0);
    
    int getRunDirection();
}
