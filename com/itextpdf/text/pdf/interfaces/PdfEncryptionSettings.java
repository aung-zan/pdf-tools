// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.interfaces;

import java.security.cert.Certificate;
import com.itextpdf.text.DocumentException;

public interface PdfEncryptionSettings
{
    void setEncryption(final byte[] p0, final byte[] p1, final int p2, final int p3) throws DocumentException;
    
    void setEncryption(final Certificate[] p0, final int[] p1, final int p2) throws DocumentException;
}
