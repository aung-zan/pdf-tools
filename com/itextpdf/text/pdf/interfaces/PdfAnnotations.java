// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.interfaces;

import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfAnnotation;
import com.itextpdf.text.pdf.PdfAcroForm;

public interface PdfAnnotations
{
    PdfAcroForm getAcroForm();
    
    void addAnnotation(final PdfAnnotation p0);
    
    void addCalculationOrder(final PdfFormField p0);
    
    void setSigFlags(final int p0);
}
