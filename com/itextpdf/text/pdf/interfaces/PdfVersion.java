// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.interfaces;

import com.itextpdf.text.pdf.PdfDeveloperExtension;
import com.itextpdf.text.pdf.PdfName;

public interface PdfVersion
{
    void setPdfVersion(final char p0);
    
    void setAtLeastPdfVersion(final char p0);
    
    void setPdfVersion(final PdfName p0);
    
    void addDeveloperExtension(final PdfDeveloperExtension p0);
}
