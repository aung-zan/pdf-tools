// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.interfaces;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfAction;

public interface PdfDocumentActions
{
    void setOpenAction(final String p0);
    
    void setOpenAction(final PdfAction p0);
    
    void setAdditionalAction(final PdfName p0, final PdfAction p1) throws DocumentException;
}
