// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf.interfaces;

import com.itextpdf.text.AccessibleElementId;
import java.util.HashMap;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfName;

public interface IAccessibleElement
{
    PdfObject getAccessibleAttribute(final PdfName p0);
    
    void setAccessibleAttribute(final PdfName p0, final PdfObject p1);
    
    HashMap<PdfName, PdfObject> getAccessibleAttributes();
    
    PdfName getRole();
    
    void setRole(final PdfName p0);
    
    AccessibleElementId getId();
    
    void setId(final AccessibleElementId p0);
    
    boolean isInline();
}
