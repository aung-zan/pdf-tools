// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.io.IOException;
import com.itextpdf.text.AccessibleElementId;
import java.util.HashMap;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;

public class PdfTemplate extends PdfContentByte implements IAccessibleElement
{
    public static final int TYPE_TEMPLATE = 1;
    public static final int TYPE_IMPORTED = 2;
    public static final int TYPE_PATTERN = 3;
    protected int type;
    protected PdfIndirectReference thisReference;
    protected PageResources pageResources;
    protected Rectangle bBox;
    protected PdfArray matrix;
    protected PdfTransparencyGroup group;
    protected PdfOCG layer;
    protected PdfIndirectReference pageReference;
    protected boolean contentTagged;
    private PdfDictionary additional;
    protected PdfName role;
    protected HashMap<PdfName, PdfObject> accessibleAttributes;
    private AccessibleElementId id;
    
    protected PdfTemplate() {
        super(null);
        this.bBox = new Rectangle(0.0f, 0.0f);
        this.contentTagged = false;
        this.additional = null;
        this.role = PdfName.FIGURE;
        this.accessibleAttributes = null;
        this.id = null;
        this.type = 1;
    }
    
    PdfTemplate(final PdfWriter wr) {
        super(wr);
        this.bBox = new Rectangle(0.0f, 0.0f);
        this.contentTagged = false;
        this.additional = null;
        this.role = PdfName.FIGURE;
        this.accessibleAttributes = null;
        this.id = null;
        this.type = 1;
        (this.pageResources = new PageResources()).addDefaultColor(wr.getDefaultColorspace());
        this.thisReference = this.writer.getPdfIndirectReference();
    }
    
    public static PdfTemplate createTemplate(final PdfWriter writer, final float width, final float height) {
        return createTemplate(writer, width, height, null);
    }
    
    static PdfTemplate createTemplate(final PdfWriter writer, final float width, final float height, final PdfName forcedName) {
        final PdfTemplate template = new PdfTemplate(writer);
        template.setWidth(width);
        template.setHeight(height);
        writer.addDirectTemplateSimple(template, forcedName);
        return template;
    }
    
    @Override
    public boolean isTagged() {
        return super.isTagged() && this.contentTagged;
    }
    
    public void setWidth(final float width) {
        this.bBox.setLeft(0.0f);
        this.bBox.setRight(width);
    }
    
    public void setHeight(final float height) {
        this.bBox.setBottom(0.0f);
        this.bBox.setTop(height);
    }
    
    public float getWidth() {
        return this.bBox.getWidth();
    }
    
    public float getHeight() {
        return this.bBox.getHeight();
    }
    
    public Rectangle getBoundingBox() {
        return this.bBox;
    }
    
    public void setBoundingBox(final Rectangle bBox) {
        this.bBox = bBox;
    }
    
    public void setLayer(final PdfOCG layer) {
        this.layer = layer;
    }
    
    public PdfOCG getLayer() {
        return this.layer;
    }
    
    public void setMatrix(final float a, final float b, final float c, final float d, final float e, final float f) {
        (this.matrix = new PdfArray()).add(new PdfNumber(a));
        this.matrix.add(new PdfNumber(b));
        this.matrix.add(new PdfNumber(c));
        this.matrix.add(new PdfNumber(d));
        this.matrix.add(new PdfNumber(e));
        this.matrix.add(new PdfNumber(f));
    }
    
    PdfArray getMatrix() {
        return this.matrix;
    }
    
    public PdfIndirectReference getIndirectReference() {
        if (this.thisReference == null) {
            this.thisReference = this.writer.getPdfIndirectReference();
        }
        return this.thisReference;
    }
    
    public void beginVariableText() {
        this.content.append("/Tx BMC ");
    }
    
    public void endVariableText() {
        this.content.append("EMC ");
    }
    
    PdfObject getResources() {
        return this.getPageResources().getResources();
    }
    
    public PdfStream getFormXObject(final int compressionLevel) throws IOException {
        return new PdfFormXObject(this, compressionLevel);
    }
    
    @Override
    public PdfContentByte getDuplicate() {
        final PdfTemplate tpl = new PdfTemplate();
        tpl.writer = this.writer;
        tpl.pdf = this.pdf;
        tpl.thisReference = this.thisReference;
        tpl.pageResources = this.pageResources;
        tpl.bBox = new Rectangle(this.bBox);
        tpl.group = this.group;
        tpl.layer = this.layer;
        if (this.matrix != null) {
            tpl.matrix = new PdfArray(this.matrix);
        }
        tpl.separator = this.separator;
        tpl.additional = this.additional;
        tpl.contentTagged = this.contentTagged;
        tpl.duplicatedFrom = this;
        return tpl;
    }
    
    public int getType() {
        return this.type;
    }
    
    @Override
    PageResources getPageResources() {
        return this.pageResources;
    }
    
    public PdfTransparencyGroup getGroup() {
        return this.group;
    }
    
    public void setGroup(final PdfTransparencyGroup group) {
        this.group = group;
    }
    
    public PdfDictionary getAdditional() {
        return this.additional;
    }
    
    public void setAdditional(final PdfDictionary additional) {
        this.additional = additional;
    }
    
    public PdfIndirectReference getCurrentPage() {
        return (this.pageReference == null) ? this.writer.getCurrentPage() : this.pageReference;
    }
    
    public PdfIndirectReference getPageReference() {
        return this.pageReference;
    }
    
    public void setPageReference(final PdfIndirectReference pageReference) {
        this.pageReference = pageReference;
    }
    
    public boolean isContentTagged() {
        return this.contentTagged;
    }
    
    public void setContentTagged(final boolean contentTagged) {
        this.contentTagged = contentTagged;
    }
    
    @Override
    public PdfObject getAccessibleAttribute(final PdfName key) {
        if (this.accessibleAttributes != null) {
            return this.accessibleAttributes.get(key);
        }
        return null;
    }
    
    @Override
    public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
        if (this.accessibleAttributes == null) {
            this.accessibleAttributes = new HashMap<PdfName, PdfObject>();
        }
        this.accessibleAttributes.put(key, value);
    }
    
    @Override
    public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
        return this.accessibleAttributes;
    }
    
    @Override
    public PdfName getRole() {
        return this.role;
    }
    
    @Override
    public void setRole(final PdfName role) {
        this.role = role;
    }
    
    @Override
    public AccessibleElementId getId() {
        if (this.id == null) {
            this.id = new AccessibleElementId();
        }
        return this.id;
    }
    
    @Override
    public void setId(final AccessibleElementId id) {
        this.id = id;
    }
    
    @Override
    public boolean isInline() {
        return true;
    }
}
