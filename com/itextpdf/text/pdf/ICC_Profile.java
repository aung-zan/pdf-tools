// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.pdf;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.util.HashMap;

public class ICC_Profile
{
    protected byte[] data;
    protected int numComponents;
    private static HashMap<String, Integer> cstags;
    
    protected ICC_Profile() {
    }
    
    public static ICC_Profile getInstance(final byte[] data, final int numComponents) {
        if (data.length < 128 || data[36] != 97 || data[37] != 99 || data[38] != 115 || data[39] != 112) {
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("invalid.icc.profile", new Object[0]));
        }
        try {
            final ICC_Profile icc = new ICC_Profile();
            icc.data = data;
            final Integer cs = ICC_Profile.cstags.get(new String(data, 16, 4, "US-ASCII"));
            final int nc = (cs == null) ? 0 : cs;
            icc.numComponents = nc;
            if (nc != numComponents) {
                throw new IllegalArgumentException("ICC profile contains " + nc + " component(s), the image data contains " + numComponents + " component(s)");
            }
            return icc;
        }
        catch (UnsupportedEncodingException e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public static ICC_Profile getInstance(final byte[] data) {
        try {
            final Integer cs = ICC_Profile.cstags.get(new String(data, 16, 4, "US-ASCII"));
            final int numComponents = (cs == null) ? 0 : cs;
            return getInstance(data, numComponents);
        }
        catch (UnsupportedEncodingException e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public static ICC_Profile getInstance(final InputStream file) {
        try {
            final byte[] head = new byte[128];
            int n;
            for (int remain = head.length, ptr = 0; remain > 0; remain -= n, ptr += n) {
                n = file.read(head, ptr, remain);
                if (n < 0) {
                    throw new IllegalArgumentException(MessageLocalization.getComposedMessage("invalid.icc.profile", new Object[0]));
                }
            }
            if (head[36] != 97 || head[37] != 99 || head[38] != 115 || head[39] != 112) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("invalid.icc.profile", new Object[0]));
            }
            int remain = (head[0] & 0xFF) << 24 | (head[1] & 0xFF) << 16 | (head[2] & 0xFF) << 8 | (head[3] & 0xFF);
            final byte[] icc = new byte[remain];
            System.arraycopy(head, 0, icc, 0, head.length);
            remain -= head.length;
            int n2;
            for (int ptr = head.length; remain > 0; remain -= n2, ptr += n2) {
                n2 = file.read(icc, ptr, remain);
                if (n2 < 0) {
                    throw new IllegalArgumentException(MessageLocalization.getComposedMessage("invalid.icc.profile", new Object[0]));
                }
            }
            return getInstance(icc);
        }
        catch (Exception ex) {
            throw new ExceptionConverter(ex);
        }
    }
    
    public static ICC_Profile GetInstance(final String fname) {
        FileInputStream fs = null;
        try {
            fs = new FileInputStream(fname);
            final ICC_Profile icc = getInstance(fs);
            return icc;
        }
        catch (Exception ex) {
            throw new ExceptionConverter(ex);
        }
        finally {
            try {
                fs.close();
            }
            catch (Exception ex2) {}
        }
    }
    
    public byte[] getData() {
        return this.data;
    }
    
    public int getNumComponents() {
        return this.numComponents;
    }
    
    static {
        (ICC_Profile.cstags = new HashMap<String, Integer>()).put("XYZ ", 3);
        ICC_Profile.cstags.put("Lab ", 3);
        ICC_Profile.cstags.put("Luv ", 3);
        ICC_Profile.cstags.put("YCbr", 3);
        ICC_Profile.cstags.put("Yxy ", 3);
        ICC_Profile.cstags.put("RGB ", 3);
        ICC_Profile.cstags.put("GRAY", 1);
        ICC_Profile.cstags.put("HSV ", 3);
        ICC_Profile.cstags.put("HLS ", 3);
        ICC_Profile.cstags.put("CMYK", 4);
        ICC_Profile.cstags.put("CMY ", 3);
        ICC_Profile.cstags.put("2CLR", 2);
        ICC_Profile.cstags.put("3CLR", 3);
        ICC_Profile.cstags.put("4CLR", 4);
        ICC_Profile.cstags.put("5CLR", 5);
        ICC_Profile.cstags.put("6CLR", 6);
        ICC_Profile.cstags.put("7CLR", 7);
        ICC_Profile.cstags.put("8CLR", 8);
        ICC_Profile.cstags.put("9CLR", 9);
        ICC_Profile.cstags.put("ACLR", 10);
        ICC_Profile.cstags.put("BCLR", 11);
        ICC_Profile.cstags.put("CCLR", 12);
        ICC_Profile.cstags.put("DCLR", 13);
        ICC_Profile.cstags.put("ECLR", 14);
        ICC_Profile.cstags.put("FCLR", 15);
    }
}
