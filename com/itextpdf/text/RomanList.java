// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import com.itextpdf.text.factories.RomanNumberFactory;

public class RomanList extends List
{
    public RomanList() {
        super(true);
    }
    
    public RomanList(final int symbolIndent) {
        super(true, (float)symbolIndent);
    }
    
    public RomanList(final boolean lowercase, final int symbolIndent) {
        super(true, (float)symbolIndent);
        this.lowercase = lowercase;
    }
    
    @Override
    public boolean add(final Element o) {
        if (o instanceof ListItem) {
            final ListItem item = (ListItem)o;
            final Chunk chunk = new Chunk(this.preSymbol, this.symbol.getFont());
            chunk.setAttributes(this.symbol.getAttributes());
            chunk.append(RomanNumberFactory.getString(this.first + this.list.size(), this.lowercase));
            chunk.append(this.postSymbol);
            item.setListSymbol(chunk);
            item.setIndentationLeft(this.symbolIndent, this.autoindent);
            item.setIndentationRight(0.0f);
            this.list.add(item);
        }
        else if (o instanceof List) {
            final List nested = (List)o;
            nested.setIndentationLeft(nested.getIndentationLeft() + this.symbolIndent);
            --this.first;
            return this.list.add(nested);
        }
        return false;
    }
    
    @Override
    public List cloneShallow() {
        final RomanList clone = new RomanList();
        this.populateProperties(clone);
        return clone;
    }
}
