// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

public class DocumentException extends Exception
{
    private static final long serialVersionUID = -2191131489390840739L;
    
    public DocumentException(final Exception ex) {
        super(ex);
    }
    
    public DocumentException() {
    }
    
    public DocumentException(final String message) {
        super(message);
    }
    
    public DocumentException(final String message, final Exception ex) {
        super(message, ex);
    }
}
