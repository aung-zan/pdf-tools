// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.xml.simpleparser;

import java.util.Map;

public interface SimpleXMLDocHandler
{
    void startElement(final String p0, final Map<String, String> p1);
    
    void endElement(final String p0);
    
    void startDocument();
    
    void endDocument();
    
    void text(final String p0);
}
