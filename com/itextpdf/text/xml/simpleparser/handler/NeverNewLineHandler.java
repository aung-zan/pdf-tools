// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.xml.simpleparser.handler;

import com.itextpdf.text.xml.simpleparser.NewLineHandler;

public class NeverNewLineHandler implements NewLineHandler
{
    @Override
    public boolean isNewLineTag(final String tag) {
        return false;
    }
}
