// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.xml.simpleparser.handler;

import java.util.HashSet;
import java.util.Set;
import com.itextpdf.text.xml.simpleparser.NewLineHandler;

public class HTMLNewLineHandler implements NewLineHandler
{
    private final Set<String> newLineTags;
    
    public HTMLNewLineHandler() {
        (this.newLineTags = new HashSet<String>()).add("p");
        this.newLineTags.add("blockquote");
        this.newLineTags.add("br");
    }
    
    @Override
    public boolean isNewLineTag(final String tag) {
        return this.newLineTags.contains(tag);
    }
}
