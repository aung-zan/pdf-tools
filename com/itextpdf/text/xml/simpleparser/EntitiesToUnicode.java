// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.xml.simpleparser;

import java.util.HashMap;
import java.util.Map;

public class EntitiesToUnicode
{
    private static final Map<String, Character> MAP;
    
    public static char decodeEntity(final String name) {
        if (name.startsWith("#x")) {
            try {
                return (char)Integer.parseInt(name.substring(2), 16);
            }
            catch (NumberFormatException nfe) {
                return '\0';
            }
        }
        if (name.startsWith("#")) {
            try {
                return (char)Integer.parseInt(name.substring(1));
            }
            catch (NumberFormatException nfe) {
                return '\0';
            }
        }
        final Character c = EntitiesToUnicode.MAP.get(name);
        if (c == null) {
            return '\0';
        }
        return c;
    }
    
    public static String decodeString(final String s) {
        int pos_amp = s.indexOf(38);
        if (pos_amp == -1) {
            return s;
        }
        final StringBuffer buf = new StringBuffer(s.substring(0, pos_amp));
        while (true) {
            final int pos_sc = s.indexOf(59, pos_amp);
            if (pos_sc == -1) {
                buf.append(s.substring(pos_amp));
                return buf.toString();
            }
            for (int pos_a = s.indexOf(38, pos_amp + 1); pos_a != -1 && pos_a < pos_sc; pos_a = s.indexOf(38, pos_amp + 1)) {
                buf.append(s.substring(pos_amp, pos_a));
                pos_amp = pos_a;
            }
            final char replace = decodeEntity(s.substring(pos_amp + 1, pos_sc));
            if (s.length() < pos_sc + 1) {
                return buf.toString();
            }
            if (replace == '\0') {
                buf.append(s.substring(pos_amp, pos_sc + 1));
            }
            else {
                buf.append(replace);
            }
            pos_amp = s.indexOf(38, pos_sc);
            if (pos_amp == -1) {
                buf.append(s.substring(pos_sc + 1));
                return buf.toString();
            }
            buf.append(s.substring(pos_sc + 1, pos_amp));
        }
    }
    
    static {
        (MAP = new HashMap<String, Character>()).put("nbsp", ' ');
        EntitiesToUnicode.MAP.put("iexcl", '¡');
        EntitiesToUnicode.MAP.put("cent", '¢');
        EntitiesToUnicode.MAP.put("pound", '£');
        EntitiesToUnicode.MAP.put("curren", '¤');
        EntitiesToUnicode.MAP.put("yen", '¥');
        EntitiesToUnicode.MAP.put("brvbar", '¦');
        EntitiesToUnicode.MAP.put("sect", '§');
        EntitiesToUnicode.MAP.put("uml", '¨');
        EntitiesToUnicode.MAP.put("copy", '©');
        EntitiesToUnicode.MAP.put("ordf", 'ª');
        EntitiesToUnicode.MAP.put("laquo", '«');
        EntitiesToUnicode.MAP.put("not", '¬');
        EntitiesToUnicode.MAP.put("shy", '\u00ad');
        EntitiesToUnicode.MAP.put("reg", '®');
        EntitiesToUnicode.MAP.put("macr", '¯');
        EntitiesToUnicode.MAP.put("deg", '°');
        EntitiesToUnicode.MAP.put("plusmn", '±');
        EntitiesToUnicode.MAP.put("sup2", '²');
        EntitiesToUnicode.MAP.put("sup3", '³');
        EntitiesToUnicode.MAP.put("acute", '´');
        EntitiesToUnicode.MAP.put("micro", 'µ');
        EntitiesToUnicode.MAP.put("para", '¶');
        EntitiesToUnicode.MAP.put("middot", '·');
        EntitiesToUnicode.MAP.put("cedil", '¸');
        EntitiesToUnicode.MAP.put("sup1", '¹');
        EntitiesToUnicode.MAP.put("ordm", 'º');
        EntitiesToUnicode.MAP.put("raquo", '»');
        EntitiesToUnicode.MAP.put("frac14", '¼');
        EntitiesToUnicode.MAP.put("frac12", '½');
        EntitiesToUnicode.MAP.put("frac34", '¾');
        EntitiesToUnicode.MAP.put("iquest", '¿');
        EntitiesToUnicode.MAP.put("Agrave", '\u00c0');
        EntitiesToUnicode.MAP.put("Aacute", '\u00c1');
        EntitiesToUnicode.MAP.put("Acirc", '\u00c2');
        EntitiesToUnicode.MAP.put("Atilde", '\u00c3');
        EntitiesToUnicode.MAP.put("Auml", '\u00c4');
        EntitiesToUnicode.MAP.put("Aring", '\u00c5');
        EntitiesToUnicode.MAP.put("AElig", '\u00c6');
        EntitiesToUnicode.MAP.put("Ccedil", '\u00c7');
        EntitiesToUnicode.MAP.put("Egrave", '\u00c8');
        EntitiesToUnicode.MAP.put("Eacute", '\u00c9');
        EntitiesToUnicode.MAP.put("Ecirc", '\u00ca');
        EntitiesToUnicode.MAP.put("Euml", '\u00cb');
        EntitiesToUnicode.MAP.put("Igrave", '\u00cc');
        EntitiesToUnicode.MAP.put("Iacute", '\u00cd');
        EntitiesToUnicode.MAP.put("Icirc", '\u00ce');
        EntitiesToUnicode.MAP.put("Iuml", '\u00cf');
        EntitiesToUnicode.MAP.put("ETH", '\u00d0');
        EntitiesToUnicode.MAP.put("Ntilde", '\u00d1');
        EntitiesToUnicode.MAP.put("Ograve", '\u00d2');
        EntitiesToUnicode.MAP.put("Oacute", '\u00d3');
        EntitiesToUnicode.MAP.put("Ocirc", '\u00d4');
        EntitiesToUnicode.MAP.put("Otilde", '\u00d5');
        EntitiesToUnicode.MAP.put("Ouml", '\u00d6');
        EntitiesToUnicode.MAP.put("times", '\u00d7');
        EntitiesToUnicode.MAP.put("Oslash", '\u00d8');
        EntitiesToUnicode.MAP.put("Ugrave", '\u00d9');
        EntitiesToUnicode.MAP.put("Uacute", '\u00da');
        EntitiesToUnicode.MAP.put("Ucirc", '\u00db');
        EntitiesToUnicode.MAP.put("Uuml", '\u00dc');
        EntitiesToUnicode.MAP.put("Yacute", '\u00dd');
        EntitiesToUnicode.MAP.put("THORN", '\u00de');
        EntitiesToUnicode.MAP.put("szlig", '\u00df');
        EntitiesToUnicode.MAP.put("agrave", '\u00e0');
        EntitiesToUnicode.MAP.put("aacute", '\u00e1');
        EntitiesToUnicode.MAP.put("acirc", '\u00e2');
        EntitiesToUnicode.MAP.put("atilde", '\u00e3');
        EntitiesToUnicode.MAP.put("auml", '\u00e4');
        EntitiesToUnicode.MAP.put("aring", '\u00e5');
        EntitiesToUnicode.MAP.put("aelig", '\u00e6');
        EntitiesToUnicode.MAP.put("ccedil", '\u00e7');
        EntitiesToUnicode.MAP.put("egrave", '\u00e8');
        EntitiesToUnicode.MAP.put("eacute", '\u00e9');
        EntitiesToUnicode.MAP.put("ecirc", '\u00ea');
        EntitiesToUnicode.MAP.put("euml", '\u00eb');
        EntitiesToUnicode.MAP.put("igrave", '\u00ec');
        EntitiesToUnicode.MAP.put("iacute", '\u00ed');
        EntitiesToUnicode.MAP.put("icirc", '\u00ee');
        EntitiesToUnicode.MAP.put("iuml", '\u00ef');
        EntitiesToUnicode.MAP.put("eth", '\u00f0');
        EntitiesToUnicode.MAP.put("ntilde", '\u00f1');
        EntitiesToUnicode.MAP.put("ograve", '\u00f2');
        EntitiesToUnicode.MAP.put("oacute", '\u00f3');
        EntitiesToUnicode.MAP.put("ocirc", '\u00f4');
        EntitiesToUnicode.MAP.put("otilde", '\u00f5');
        EntitiesToUnicode.MAP.put("ouml", '\u00f6');
        EntitiesToUnicode.MAP.put("divide", '\u00f7');
        EntitiesToUnicode.MAP.put("oslash", '\u00f8');
        EntitiesToUnicode.MAP.put("ugrave", '\u00f9');
        EntitiesToUnicode.MAP.put("uacute", '\u00fa');
        EntitiesToUnicode.MAP.put("ucirc", '\u00fb');
        EntitiesToUnicode.MAP.put("uuml", '\u00fc');
        EntitiesToUnicode.MAP.put("yacute", '\u00fd');
        EntitiesToUnicode.MAP.put("thorn", '\u00fe');
        EntitiesToUnicode.MAP.put("yuml", '\u00ff');
        EntitiesToUnicode.MAP.put("fnof", '\u0192');
        EntitiesToUnicode.MAP.put("Alpha", '\u0391');
        EntitiesToUnicode.MAP.put("Beta", '\u0392');
        EntitiesToUnicode.MAP.put("Gamma", '\u0393');
        EntitiesToUnicode.MAP.put("Delta", '\u0394');
        EntitiesToUnicode.MAP.put("Epsilon", '\u0395');
        EntitiesToUnicode.MAP.put("Zeta", '\u0396');
        EntitiesToUnicode.MAP.put("Eta", '\u0397');
        EntitiesToUnicode.MAP.put("Theta", '\u0398');
        EntitiesToUnicode.MAP.put("Iota", '\u0399');
        EntitiesToUnicode.MAP.put("Kappa", '\u039a');
        EntitiesToUnicode.MAP.put("Lambda", '\u039b');
        EntitiesToUnicode.MAP.put("Mu", '\u039c');
        EntitiesToUnicode.MAP.put("Nu", '\u039d');
        EntitiesToUnicode.MAP.put("Xi", '\u039e');
        EntitiesToUnicode.MAP.put("Omicron", '\u039f');
        EntitiesToUnicode.MAP.put("Pi", '\u03a0');
        EntitiesToUnicode.MAP.put("Rho", '\u03a1');
        EntitiesToUnicode.MAP.put("Sigma", '\u03a3');
        EntitiesToUnicode.MAP.put("Tau", '\u03a4');
        EntitiesToUnicode.MAP.put("Upsilon", '\u03a5');
        EntitiesToUnicode.MAP.put("Phi", '\u03a6');
        EntitiesToUnicode.MAP.put("Chi", '\u03a7');
        EntitiesToUnicode.MAP.put("Psi", '\u03a8');
        EntitiesToUnicode.MAP.put("Omega", '\u03a9');
        EntitiesToUnicode.MAP.put("alpha", '\u03b1');
        EntitiesToUnicode.MAP.put("beta", '\u03b2');
        EntitiesToUnicode.MAP.put("gamma", '\u03b3');
        EntitiesToUnicode.MAP.put("delta", '\u03b4');
        EntitiesToUnicode.MAP.put("epsilon", '\u03b5');
        EntitiesToUnicode.MAP.put("zeta", '\u03b6');
        EntitiesToUnicode.MAP.put("eta", '\u03b7');
        EntitiesToUnicode.MAP.put("theta", '\u03b8');
        EntitiesToUnicode.MAP.put("iota", '\u03b9');
        EntitiesToUnicode.MAP.put("kappa", '\u03ba');
        EntitiesToUnicode.MAP.put("lambda", '\u03bb');
        EntitiesToUnicode.MAP.put("mu", '\u03bc');
        EntitiesToUnicode.MAP.put("nu", '\u03bd');
        EntitiesToUnicode.MAP.put("xi", '\u03be');
        EntitiesToUnicode.MAP.put("omicron", '\u03bf');
        EntitiesToUnicode.MAP.put("pi", '\u03c0');
        EntitiesToUnicode.MAP.put("rho", '\u03c1');
        EntitiesToUnicode.MAP.put("sigmaf", '\u03c2');
        EntitiesToUnicode.MAP.put("sigma", '\u03c3');
        EntitiesToUnicode.MAP.put("tau", '\u03c4');
        EntitiesToUnicode.MAP.put("upsilon", '\u03c5');
        EntitiesToUnicode.MAP.put("phi", '\u03c6');
        EntitiesToUnicode.MAP.put("chi", '\u03c7');
        EntitiesToUnicode.MAP.put("psi", '\u03c8');
        EntitiesToUnicode.MAP.put("omega", '\u03c9');
        EntitiesToUnicode.MAP.put("thetasym", '\u03d1');
        EntitiesToUnicode.MAP.put("upsih", '\u03d2');
        EntitiesToUnicode.MAP.put("piv", '\u03d6');
        EntitiesToUnicode.MAP.put("bull", '\u2022');
        EntitiesToUnicode.MAP.put("hellip", '\u2026');
        EntitiesToUnicode.MAP.put("prime", '\u2032');
        EntitiesToUnicode.MAP.put("Prime", '\u2033');
        EntitiesToUnicode.MAP.put("oline", '\u203e');
        EntitiesToUnicode.MAP.put("frasl", '\u2044');
        EntitiesToUnicode.MAP.put("weierp", '\u2118');
        EntitiesToUnicode.MAP.put("image", '\u2111');
        EntitiesToUnicode.MAP.put("real", '\u211c');
        EntitiesToUnicode.MAP.put("trade", '\u2122');
        EntitiesToUnicode.MAP.put("alefsym", '\u2135');
        EntitiesToUnicode.MAP.put("larr", '\u2190');
        EntitiesToUnicode.MAP.put("uarr", '\u2191');
        EntitiesToUnicode.MAP.put("rarr", '\u2192');
        EntitiesToUnicode.MAP.put("darr", '\u2193');
        EntitiesToUnicode.MAP.put("harr", '\u2194');
        EntitiesToUnicode.MAP.put("crarr", '\u21b5');
        EntitiesToUnicode.MAP.put("lArr", '\u21d0');
        EntitiesToUnicode.MAP.put("uArr", '\u21d1');
        EntitiesToUnicode.MAP.put("rArr", '\u21d2');
        EntitiesToUnicode.MAP.put("dArr", '\u21d3');
        EntitiesToUnicode.MAP.put("hArr", '\u21d4');
        EntitiesToUnicode.MAP.put("forall", '\u2200');
        EntitiesToUnicode.MAP.put("part", '\u2202');
        EntitiesToUnicode.MAP.put("exist", '\u2203');
        EntitiesToUnicode.MAP.put("empty", '\u2205');
        EntitiesToUnicode.MAP.put("nabla", '\u2207');
        EntitiesToUnicode.MAP.put("isin", '\u2208');
        EntitiesToUnicode.MAP.put("notin", '\u2209');
        EntitiesToUnicode.MAP.put("ni", '\u220b');
        EntitiesToUnicode.MAP.put("prod", '\u220f');
        EntitiesToUnicode.MAP.put("sum", '\u2211');
        EntitiesToUnicode.MAP.put("minus", '\u2212');
        EntitiesToUnicode.MAP.put("lowast", '\u2217');
        EntitiesToUnicode.MAP.put("radic", '\u221a');
        EntitiesToUnicode.MAP.put("prop", '\u221d');
        EntitiesToUnicode.MAP.put("infin", '\u221e');
        EntitiesToUnicode.MAP.put("ang", '\u2220');
        EntitiesToUnicode.MAP.put("and", '\u2227');
        EntitiesToUnicode.MAP.put("or", '\u2228');
        EntitiesToUnicode.MAP.put("cap", '\u2229');
        EntitiesToUnicode.MAP.put("cup", '\u222a');
        EntitiesToUnicode.MAP.put("int", '\u222b');
        EntitiesToUnicode.MAP.put("there4", '\u2234');
        EntitiesToUnicode.MAP.put("sim", '\u223c');
        EntitiesToUnicode.MAP.put("cong", '\u2245');
        EntitiesToUnicode.MAP.put("asymp", '\u2248');
        EntitiesToUnicode.MAP.put("ne", '\u2260');
        EntitiesToUnicode.MAP.put("equiv", '\u2261');
        EntitiesToUnicode.MAP.put("le", '\u2264');
        EntitiesToUnicode.MAP.put("ge", '\u2265');
        EntitiesToUnicode.MAP.put("sub", '\u2282');
        EntitiesToUnicode.MAP.put("sup", '\u2283');
        EntitiesToUnicode.MAP.put("nsub", '\u2284');
        EntitiesToUnicode.MAP.put("sube", '\u2286');
        EntitiesToUnicode.MAP.put("supe", '\u2287');
        EntitiesToUnicode.MAP.put("oplus", '\u2295');
        EntitiesToUnicode.MAP.put("otimes", '\u2297');
        EntitiesToUnicode.MAP.put("perp", '\u22a5');
        EntitiesToUnicode.MAP.put("sdot", '\u22c5');
        EntitiesToUnicode.MAP.put("lceil", '\u2308');
        EntitiesToUnicode.MAP.put("rceil", '\u2309');
        EntitiesToUnicode.MAP.put("lfloor", '\u230a');
        EntitiesToUnicode.MAP.put("rfloor", '\u230b');
        EntitiesToUnicode.MAP.put("lang", '\u2329');
        EntitiesToUnicode.MAP.put("rang", '\u232a');
        EntitiesToUnicode.MAP.put("loz", '\u25ca');
        EntitiesToUnicode.MAP.put("spades", '\u2660');
        EntitiesToUnicode.MAP.put("clubs", '\u2663');
        EntitiesToUnicode.MAP.put("hearts", '\u2665');
        EntitiesToUnicode.MAP.put("diams", '\u2666');
        EntitiesToUnicode.MAP.put("quot", '\"');
        EntitiesToUnicode.MAP.put("amp", '&');
        EntitiesToUnicode.MAP.put("apos", '\'');
        EntitiesToUnicode.MAP.put("lt", '<');
        EntitiesToUnicode.MAP.put("gt", '>');
        EntitiesToUnicode.MAP.put("OElig", '\u0152');
        EntitiesToUnicode.MAP.put("oelig", '\u0153');
        EntitiesToUnicode.MAP.put("Scaron", '\u0160');
        EntitiesToUnicode.MAP.put("scaron", '\u0161');
        EntitiesToUnicode.MAP.put("Yuml", '\u0178');
        EntitiesToUnicode.MAP.put("circ", '\u02c6');
        EntitiesToUnicode.MAP.put("tilde", '\u02dc');
        EntitiesToUnicode.MAP.put("ensp", '\u2002');
        EntitiesToUnicode.MAP.put("emsp", '\u2003');
        EntitiesToUnicode.MAP.put("thinsp", '\u2009');
        EntitiesToUnicode.MAP.put("zwnj", '\u200c');
        EntitiesToUnicode.MAP.put("zwj", '\u200d');
        EntitiesToUnicode.MAP.put("lrm", '\u200e');
        EntitiesToUnicode.MAP.put("rlm", '\u200f');
        EntitiesToUnicode.MAP.put("ndash", '\u2013');
        EntitiesToUnicode.MAP.put("mdash", '\u2014');
        EntitiesToUnicode.MAP.put("lsquo", '\u2018');
        EntitiesToUnicode.MAP.put("rsquo", '\u2019');
        EntitiesToUnicode.MAP.put("sbquo", '\u201a');
        EntitiesToUnicode.MAP.put("ldquo", '\u201c');
        EntitiesToUnicode.MAP.put("rdquo", '\u201d');
        EntitiesToUnicode.MAP.put("bdquo", '\u201e');
        EntitiesToUnicode.MAP.put("dagger", '\u2020');
        EntitiesToUnicode.MAP.put("Dagger", '\u2021');
        EntitiesToUnicode.MAP.put("permil", '\u2030');
        EntitiesToUnicode.MAP.put("lsaquo", '\u2039');
        EntitiesToUnicode.MAP.put("rsaquo", '\u203a');
        EntitiesToUnicode.MAP.put("euro", '\u20ac');
    }
}
