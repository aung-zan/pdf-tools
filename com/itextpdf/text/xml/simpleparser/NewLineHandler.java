// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.xml.simpleparser;

public interface NewLineHandler
{
    boolean isNewLineTag(final String p0);
}
