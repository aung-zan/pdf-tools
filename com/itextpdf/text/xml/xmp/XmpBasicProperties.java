// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.xml.xmp;

import com.itextpdf.xmp.options.PropertyOptions;
import com.itextpdf.xmp.XMPUtils;
import com.itextpdf.xmp.XMPException;
import com.itextpdf.xmp.XMPMeta;

public class XmpBasicProperties
{
    public static final String ADVISORY = "Advisory";
    public static final String BASEURL = "BaseURL";
    public static final String CREATEDATE = "CreateDate";
    public static final String CREATORTOOL = "CreatorTool";
    public static final String IDENTIFIER = "Identifier";
    public static final String METADATADATE = "MetadataDate";
    public static final String MODIFYDATE = "ModifyDate";
    public static final String NICKNAME = "Nickname";
    public static final String THUMBNAILS = "Thumbnails";
    
    public static void setCreatorTool(final XMPMeta xmpMeta, final String creator) throws XMPException {
        xmpMeta.setProperty("http://ns.adobe.com/xap/1.0/", "CreatorTool", creator);
    }
    
    public static void setCreateDate(final XMPMeta xmpMeta, final String date) throws XMPException {
        xmpMeta.setProperty("http://ns.adobe.com/xap/1.0/", "CreateDate", date);
    }
    
    public static void setModDate(final XMPMeta xmpMeta, final String date) throws XMPException {
        xmpMeta.setProperty("http://ns.adobe.com/xap/1.0/", "ModifyDate", date);
    }
    
    public static void setMetaDataDate(final XMPMeta xmpMeta, final String date) throws XMPException {
        xmpMeta.setProperty("http://ns.adobe.com/xap/1.0/", "MetadataDate", date);
    }
    
    public static void setIdentifiers(final XMPMeta xmpMeta, final String[] id) throws XMPException {
        XMPUtils.removeProperties(xmpMeta, "http://purl.org/dc/elements/1.1/", "Identifier", true, true);
        for (int i = 0; i < id.length; ++i) {
            xmpMeta.appendArrayItem("http://purl.org/dc/elements/1.1/", "Identifier", new PropertyOptions(512), id[i], null);
        }
    }
    
    public static void setNickname(final XMPMeta xmpMeta, final String name) throws XMPException {
        xmpMeta.setProperty("http://ns.adobe.com/xap/1.0/", "Nickname", name);
    }
}
