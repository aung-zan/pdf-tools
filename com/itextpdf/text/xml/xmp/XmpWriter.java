// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.xml.xmp;

import com.itextpdf.text.pdf.PdfDate;
import com.itextpdf.xmp.options.PropertyOptions;
import com.itextpdf.xmp.XMPUtils;
import java.util.Map;
import com.itextpdf.text.pdf.PdfObject;
import java.util.Iterator;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfDictionary;
import java.io.IOException;
import com.itextpdf.xmp.XMPException;
import com.itextpdf.text.Version;
import com.itextpdf.xmp.XMPMetaFactory;
import com.itextpdf.xmp.options.SerializeOptions;
import java.io.OutputStream;
import com.itextpdf.xmp.XMPMeta;

public class XmpWriter
{
    public static final String UTF8 = "UTF-8";
    public static final String UTF16 = "UTF-16";
    public static final String UTF16BE = "UTF-16BE";
    public static final String UTF16LE = "UTF-16LE";
    protected XMPMeta xmpMeta;
    protected OutputStream outputStream;
    protected SerializeOptions serializeOptions;
    
    public XmpWriter(final OutputStream os, final String utfEncoding, final int extraSpace) throws IOException {
        this.outputStream = os;
        this.serializeOptions = new SerializeOptions();
        if ("UTF-16BE".equals(utfEncoding) || "UTF-16".equals(utfEncoding)) {
            this.serializeOptions.setEncodeUTF16BE(true);
        }
        else if ("UTF-16LE".equals(utfEncoding)) {
            this.serializeOptions.setEncodeUTF16LE(true);
        }
        this.serializeOptions.setPadding(extraSpace);
        (this.xmpMeta = XMPMetaFactory.create()).setObjectName("xmpmeta");
        this.xmpMeta.setObjectName("");
        try {
            this.xmpMeta.setProperty("http://purl.org/dc/elements/1.1/", "format", "application/pdf");
            this.xmpMeta.setProperty("http://ns.adobe.com/pdf/1.3/", "Producer", Version.getInstance().getVersion());
        }
        catch (XMPException ex) {}
    }
    
    public XmpWriter(final OutputStream os) throws IOException {
        this(os, "UTF-8", 2000);
    }
    
    public XmpWriter(final OutputStream os, final PdfDictionary info) throws IOException {
        this(os);
        if (info != null) {
            for (final PdfName key : info.getKeys()) {
                final PdfName pdfName = key;
                final PdfObject obj = info.get(key);
                if (obj == null) {
                    continue;
                }
                if (!obj.isString()) {
                    continue;
                }
                final String value = ((PdfString)obj).toUnicodeString();
                try {
                    this.addDocInfoProperty(key, value);
                }
                catch (XMPException xmpExc) {
                    throw new IOException(xmpExc.getMessage());
                }
            }
        }
    }
    
    public XmpWriter(final OutputStream os, final Map<String, String> info) throws IOException {
        this(os);
        if (info != null) {
            for (final Map.Entry<String, String> entry : info.entrySet()) {
                final String key = entry.getKey();
                final String value = entry.getValue();
                if (value == null) {
                    continue;
                }
                try {
                    this.addDocInfoProperty(key, value);
                }
                catch (XMPException xmpExc) {
                    throw new IOException(xmpExc.getMessage());
                }
            }
        }
    }
    
    public XMPMeta getXmpMeta() {
        return this.xmpMeta;
    }
    
    public void setReadOnly() {
        this.serializeOptions.setReadOnlyPacket(true);
    }
    
    public void setAbout(final String about) {
        this.xmpMeta.setObjectName(about);
    }
    
    @Deprecated
    public void addRdfDescription(final String xmlns, final String content) throws IOException {
        try {
            final String str = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"><rdf:Description rdf:about=\"" + this.xmpMeta.getObjectName() + "\" " + xmlns + ">" + content + "</rdf:Description></rdf:RDF>\n";
            final XMPMeta extMeta = XMPMetaFactory.parseFromString(str);
            XMPUtils.appendProperties(extMeta, this.xmpMeta, true, true);
        }
        catch (XMPException xmpExc) {
            throw new IOException(xmpExc.getMessage());
        }
    }
    
    @Deprecated
    public void addRdfDescription(final XmpSchema s) throws IOException {
        try {
            final String str = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"><rdf:Description rdf:about=\"" + this.xmpMeta.getObjectName() + "\" " + s.getXmlns() + ">" + s.toString() + "</rdf:Description></rdf:RDF>\n";
            final XMPMeta extMeta = XMPMetaFactory.parseFromString(str);
            XMPUtils.appendProperties(extMeta, this.xmpMeta, true, true);
        }
        catch (XMPException xmpExc) {
            throw new IOException(xmpExc.getMessage());
        }
    }
    
    public void setProperty(final String schemaNS, final String propName, final Object value) throws XMPException {
        this.xmpMeta.setProperty(schemaNS, propName, value);
    }
    
    public void appendArrayItem(final String schemaNS, final String arrayName, final String value) throws XMPException {
        this.xmpMeta.appendArrayItem(schemaNS, arrayName, new PropertyOptions(512), value, null);
    }
    
    public void appendOrderedArrayItem(final String schemaNS, final String arrayName, final String value) throws XMPException {
        this.xmpMeta.appendArrayItem(schemaNS, arrayName, new PropertyOptions(1024), value, null);
    }
    
    public void appendAlternateArrayItem(final String schemaNS, final String arrayName, final String value) throws XMPException {
        this.xmpMeta.appendArrayItem(schemaNS, arrayName, new PropertyOptions(2048), value, null);
    }
    
    public void serialize(final OutputStream externalOutputStream) throws XMPException {
        XMPMetaFactory.serialize(this.xmpMeta, externalOutputStream, this.serializeOptions);
    }
    
    public void close() throws IOException {
        if (this.outputStream == null) {
            return;
        }
        try {
            XMPMetaFactory.serialize(this.xmpMeta, this.outputStream, this.serializeOptions);
            this.outputStream = null;
        }
        catch (XMPException xmpExc) {
            throw new IOException(xmpExc.getMessage());
        }
    }
    
    public void addDocInfoProperty(Object key, final String value) throws XMPException {
        if (key instanceof String) {
            key = new PdfName((String)key);
        }
        if (PdfName.TITLE.equals(key)) {
            this.xmpMeta.setLocalizedText("http://purl.org/dc/elements/1.1/", "title", "x-default", "x-default", value);
        }
        else if (PdfName.AUTHOR.equals(key)) {
            this.xmpMeta.appendArrayItem("http://purl.org/dc/elements/1.1/", "creator", new PropertyOptions(1024), value, null);
        }
        else if (PdfName.SUBJECT.equals(key)) {
            this.xmpMeta.setLocalizedText("http://purl.org/dc/elements/1.1/", "description", "x-default", "x-default", value);
        }
        else if (PdfName.KEYWORDS.equals(key)) {
            for (final String v : value.split(",|;")) {
                if (v.trim().length() > 0) {
                    this.xmpMeta.appendArrayItem("http://purl.org/dc/elements/1.1/", "subject", new PropertyOptions(512), v.trim(), null);
                }
            }
            this.xmpMeta.setProperty("http://ns.adobe.com/pdf/1.3/", "Keywords", value);
        }
        else if (PdfName.PRODUCER.equals(key)) {
            this.xmpMeta.setProperty("http://ns.adobe.com/pdf/1.3/", "Producer", value);
        }
        else if (PdfName.CREATOR.equals(key)) {
            this.xmpMeta.setProperty("http://ns.adobe.com/xap/1.0/", "CreatorTool", value);
        }
        else if (PdfName.CREATIONDATE.equals(key)) {
            this.xmpMeta.setProperty("http://ns.adobe.com/xap/1.0/", "CreateDate", PdfDate.getW3CDate(value));
        }
        else if (PdfName.MODDATE.equals(key)) {
            this.xmpMeta.setProperty("http://ns.adobe.com/xap/1.0/", "ModifyDate", PdfDate.getW3CDate(value));
        }
    }
}
