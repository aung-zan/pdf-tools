// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.xml.xmp;

import com.itextpdf.xmp.XMPException;
import com.itextpdf.xmp.XMPMeta;

public class PdfProperties
{
    public static final String KEYWORDS = "Keywords";
    public static final String VERSION = "PDFVersion";
    public static final String PRODUCER = "Producer";
    public static final String PART = "part";
    
    public static void setKeywords(final XMPMeta xmpMeta, final String keywords) throws XMPException {
        xmpMeta.setProperty("http://ns.adobe.com/pdf/1.3/", "Keywords", keywords);
    }
    
    public static void setProducer(final XMPMeta xmpMeta, final String producer) throws XMPException {
        xmpMeta.setProperty("http://ns.adobe.com/pdf/1.3/", "Producer", producer);
    }
    
    public static void setVersion(final XMPMeta xmpMeta, final String version) throws XMPException {
        xmpMeta.setProperty("http://ns.adobe.com/pdf/1.3/", "PDFVersion", version);
    }
}
