// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.xml.xmp;

import com.itextpdf.text.xml.XMLUtil;
import java.util.Enumeration;
import java.util.Properties;

@Deprecated
public abstract class XmpSchema extends Properties
{
    private static final long serialVersionUID = -176374295948945272L;
    protected String xmlns;
    
    public XmpSchema(final String xmlns) {
        this.xmlns = xmlns;
    }
    
    @Override
    public String toString() {
        final StringBuffer buf = new StringBuffer();
        final Enumeration<?> e = this.propertyNames();
        while (e.hasMoreElements()) {
            this.process(buf, e.nextElement());
        }
        return buf.toString();
    }
    
    protected void process(final StringBuffer buf, final Object p) {
        buf.append('<');
        buf.append(p);
        buf.append('>');
        buf.append(this.get(p));
        buf.append("</");
        buf.append(p);
        buf.append('>');
    }
    
    public String getXmlns() {
        return this.xmlns;
    }
    
    public Object addProperty(final String key, final String value) {
        return this.setProperty(key, value);
    }
    
    @Override
    public Object setProperty(final String key, final String value) {
        return super.setProperty(key, XMLUtil.escapeXML(value, false));
    }
    
    public Object setProperty(final String key, final XmpArray value) {
        return super.setProperty(key, value.toString());
    }
    
    public Object setProperty(final String key, final LangAlt value) {
        return super.setProperty(key, value.toString());
    }
    
    @Deprecated
    public static String escape(final String content) {
        return XMLUtil.escapeXML(content, false);
    }
}
