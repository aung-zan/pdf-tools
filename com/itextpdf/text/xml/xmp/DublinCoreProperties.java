// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.xml.xmp;

import com.itextpdf.xmp.XMPUtils;
import com.itextpdf.xmp.XMPException;
import com.itextpdf.xmp.options.PropertyOptions;
import com.itextpdf.xmp.XMPMeta;

public class DublinCoreProperties
{
    public static final String CONTRIBUTOR = "contributor";
    public static final String COVERAGE = "coverage";
    public static final String CREATOR = "creator";
    public static final String DATE = "date";
    public static final String DESCRIPTION = "description";
    public static final String FORMAT = "format";
    public static final String IDENTIFIER = "identifier";
    public static final String LANGUAGE = "language";
    public static final String PUBLISHER = "publisher";
    public static final String RELATION = "relation";
    public static final String RIGHTS = "rights";
    public static final String SOURCE = "source";
    public static final String SUBJECT = "subject";
    public static final String TITLE = "title";
    public static final String TYPE = "type";
    
    public static void addTitle(final XMPMeta xmpMeta, final String title) throws XMPException {
        xmpMeta.appendArrayItem("http://purl.org/dc/elements/1.1/", "title", new PropertyOptions(2048), title, null);
    }
    
    public static void setTitle(final XMPMeta xmpMeta, final String title, final String genericLang, final String specificLang) throws XMPException {
        xmpMeta.setLocalizedText("http://purl.org/dc/elements/1.1/", "title", genericLang, specificLang, title);
    }
    
    public static void addDescription(final XMPMeta xmpMeta, final String desc) throws XMPException {
        xmpMeta.appendArrayItem("http://purl.org/dc/elements/1.1/", "description", new PropertyOptions(2048), desc, null);
    }
    
    public static void setDescription(final XMPMeta xmpMeta, final String desc, final String genericLang, final String specificLang) throws XMPException {
        xmpMeta.setLocalizedText("http://purl.org/dc/elements/1.1/", "description", genericLang, specificLang, desc);
    }
    
    public static void addSubject(final XMPMeta xmpMeta, final String subject) throws XMPException {
        xmpMeta.appendArrayItem("http://purl.org/dc/elements/1.1/", "subject", new PropertyOptions(512), subject, null);
    }
    
    public static void setSubject(final XMPMeta xmpMeta, final String[] subject) throws XMPException {
        XMPUtils.removeProperties(xmpMeta, "http://purl.org/dc/elements/1.1/", "subject", true, true);
        for (int i = 0; i < subject.length; ++i) {
            xmpMeta.appendArrayItem("http://purl.org/dc/elements/1.1/", "subject", new PropertyOptions(512), subject[i], null);
        }
    }
    
    public static void addAuthor(final XMPMeta xmpMeta, final String author) throws XMPException {
        xmpMeta.appendArrayItem("http://purl.org/dc/elements/1.1/", "creator", new PropertyOptions(1024), author, null);
    }
    
    public static void setAuthor(final XMPMeta xmpMeta, final String[] author) throws XMPException {
        XMPUtils.removeProperties(xmpMeta, "http://purl.org/dc/elements/1.1/", "creator", true, true);
        for (int i = 0; i < author.length; ++i) {
            xmpMeta.appendArrayItem("http://purl.org/dc/elements/1.1/", "creator", new PropertyOptions(1024), author[i], null);
        }
    }
    
    public static void addPublisher(final XMPMeta xmpMeta, final String publisher) throws XMPException {
        xmpMeta.appendArrayItem("http://purl.org/dc/elements/1.1/", "publisher", new PropertyOptions(1024), publisher, null);
    }
    
    public static void setPublisher(final XMPMeta xmpMeta, final String[] publisher) throws XMPException {
        XMPUtils.removeProperties(xmpMeta, "http://purl.org/dc/elements/1.1/", "publisher", true, true);
        for (int i = 0; i < publisher.length; ++i) {
            xmpMeta.appendArrayItem("http://purl.org/dc/elements/1.1/", "publisher", new PropertyOptions(1024), publisher[i], null);
        }
    }
}
