// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.xml;

public class XMLUtil
{
    public static String escapeXML(final String s, final boolean onlyASCII) {
        final char[] cc = s.toCharArray();
        final int len = cc.length;
        final StringBuffer sb = new StringBuffer();
        for (final int c : cc) {
            switch (c) {
                case 60: {
                    sb.append("&lt;");
                    break;
                }
                case 62: {
                    sb.append("&gt;");
                    break;
                }
                case 38: {
                    sb.append("&amp;");
                    break;
                }
                case 34: {
                    sb.append("&quot;");
                    break;
                }
                case 39: {
                    sb.append("&apos;");
                    break;
                }
                default: {
                    if (!isValidCharacterValue(c)) {
                        break;
                    }
                    if (onlyASCII && c > 127) {
                        sb.append("&#").append(c).append(';');
                        break;
                    }
                    sb.append((char)c);
                    break;
                }
            }
        }
        return sb.toString();
    }
    
    public static String unescapeXML(final String s) {
        final char[] cc = s.toCharArray();
        final int len = cc.length;
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < len; ++i) {
            int c = cc[i];
            if (c == 38) {
                final int pos = findInArray(';', cc, i + 3);
                if (pos > -1) {
                    String esc = new String(cc, i + 1, pos - i - 1);
                    if (esc.startsWith("#")) {
                        esc = esc.substring(1);
                        if (!isValidCharacterValue(esc)) {
                            i = pos;
                            continue;
                        }
                        c = (char)Integer.parseInt(esc);
                        i = pos;
                    }
                    else {
                        final int tmp = unescape(esc);
                        if (tmp > 0) {
                            c = tmp;
                            i = pos;
                        }
                    }
                }
            }
            sb.append((char)c);
        }
        return sb.toString();
    }
    
    public static int unescape(final String s) {
        if ("apos".equals(s)) {
            return 39;
        }
        if ("quot".equals(s)) {
            return 34;
        }
        if ("lt".equals(s)) {
            return 60;
        }
        if ("gt".equals(s)) {
            return 62;
        }
        if ("amp".equals(s)) {
            return 38;
        }
        return -1;
    }
    
    public static boolean isValidCharacterValue(final String s) {
        try {
            final int i = Integer.parseInt(s);
            return isValidCharacterValue(i);
        }
        catch (NumberFormatException nfe) {
            return false;
        }
    }
    
    public static boolean isValidCharacterValue(final int c) {
        return c == 9 || c == 10 || c == 13 || (c >= 32 && c <= 55295) || (c >= 57344 && c <= 65533) || (c >= 65536 && c <= 1114111);
    }
    
    public static int findInArray(final char needle, final char[] haystack, final int start) {
        for (int i = start; i < haystack.length; ++i) {
            if (haystack[i] == ';') {
                return i;
            }
        }
        return -1;
    }
    
    public static String getEncodingName(final byte[] b4) {
        final int b5 = b4[0] & 0xFF;
        final int b6 = b4[1] & 0xFF;
        if (b5 == 254 && b6 == 255) {
            return "UTF-16BE";
        }
        if (b5 == 255 && b6 == 254) {
            return "UTF-16LE";
        }
        final int b7 = b4[2] & 0xFF;
        if (b5 == 239 && b6 == 187 && b7 == 191) {
            return "UTF-8";
        }
        final int b8 = b4[3] & 0xFF;
        if (b5 == 0 && b6 == 0 && b7 == 0 && b8 == 60) {
            return "ISO-10646-UCS-4";
        }
        if (b5 == 60 && b6 == 0 && b7 == 0 && b8 == 0) {
            return "ISO-10646-UCS-4";
        }
        if (b5 == 0 && b6 == 0 && b7 == 60 && b8 == 0) {
            return "ISO-10646-UCS-4";
        }
        if (b5 == 0 && b6 == 60 && b7 == 0 && b8 == 0) {
            return "ISO-10646-UCS-4";
        }
        if (b5 == 0 && b6 == 60 && b7 == 0 && b8 == 63) {
            return "UTF-16BE";
        }
        if (b5 == 60 && b6 == 0 && b7 == 63 && b8 == 0) {
            return "UTF-16LE";
        }
        if (b5 == 76 && b6 == 111 && b7 == 167 && b8 == 148) {
            return "CP037";
        }
        return "UTF-8";
    }
}
