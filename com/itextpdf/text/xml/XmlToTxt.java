// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.xml;

import java.util.Map;
import java.io.IOException;
import java.io.Reader;
import com.itextpdf.text.xml.simpleparser.SimpleXMLDocHandlerComment;
import com.itextpdf.text.xml.simpleparser.SimpleXMLParser;
import java.io.InputStreamReader;
import java.io.InputStream;
import com.itextpdf.text.xml.simpleparser.SimpleXMLDocHandler;

public class XmlToTxt implements SimpleXMLDocHandler
{
    protected StringBuffer buf;
    
    public static String parse(final InputStream is) throws IOException {
        final XmlToTxt handler = new XmlToTxt();
        SimpleXMLParser.parse(handler, null, new InputStreamReader(is), true);
        return handler.toString();
    }
    
    protected XmlToTxt() {
        this.buf = new StringBuffer();
    }
    
    @Override
    public String toString() {
        return this.buf.toString();
    }
    
    @Override
    public void startElement(final String tag, final Map<String, String> h) {
    }
    
    @Override
    public void endElement(final String tag) {
    }
    
    @Override
    public void startDocument() {
    }
    
    @Override
    public void endDocument() {
    }
    
    @Override
    public void text(final String str) {
        this.buf.append(str);
    }
}
