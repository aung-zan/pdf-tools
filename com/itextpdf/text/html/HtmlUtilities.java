// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html;

import java.util.StringTokenizer;
import java.util.Properties;
import com.itextpdf.text.BaseColor;
import java.util.HashMap;

@Deprecated
public class HtmlUtilities
{
    public static final float DEFAULT_FONT_SIZE = 12.0f;
    private static HashMap<String, Float> sizes;
    public static final int[] FONTSIZES;
    
    public static float parseLength(final String string) {
        return parseLength(string, 12.0f);
    }
    
    public static float parseLength(String string, final float actualFontSize) {
        if (string == null) {
            return 0.0f;
        }
        final Float fl = HtmlUtilities.sizes.get(string.toLowerCase());
        if (fl != null) {
            return fl;
        }
        int pos = 0;
        final int length = string.length();
        boolean ok = true;
        while (ok && pos < length) {
            switch (string.charAt(pos)) {
                case '+':
                case '-':
                case '.':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9': {
                    ++pos;
                    continue;
                }
                default: {
                    ok = false;
                    continue;
                }
            }
        }
        if (pos == 0) {
            return 0.0f;
        }
        if (pos == length) {
            return Float.parseFloat(string + "f");
        }
        final float f = Float.parseFloat(string.substring(0, pos) + "f");
        string = string.substring(pos);
        if (string.startsWith("in")) {
            return f * 72.0f;
        }
        if (string.startsWith("cm")) {
            return f / 2.54f * 72.0f;
        }
        if (string.startsWith("mm")) {
            return f / 25.4f * 72.0f;
        }
        if (string.startsWith("pc")) {
            return f * 12.0f;
        }
        if (string.startsWith("em")) {
            return f * actualFontSize;
        }
        if (string.startsWith("ex")) {
            return f * actualFontSize / 2.0f;
        }
        return f;
    }
    
    public static BaseColor decodeColor(String s) {
        if (s == null) {
            return null;
        }
        s = s.toLowerCase().trim();
        try {
            return WebColors.getRGBColor(s);
        }
        catch (IllegalArgumentException iae) {
            return null;
        }
    }
    
    public static Properties parseAttributes(final String string) {
        final Properties result = new Properties();
        if (string == null) {
            return result;
        }
        final StringTokenizer keyValuePairs = new StringTokenizer(string, ";");
        while (keyValuePairs.hasMoreTokens()) {
            final StringTokenizer keyValuePair = new StringTokenizer(keyValuePairs.nextToken(), ":");
            if (keyValuePair.hasMoreTokens()) {
                final String key = keyValuePair.nextToken().trim();
                if (!keyValuePair.hasMoreTokens()) {
                    continue;
                }
                String value = keyValuePair.nextToken().trim();
                if (value.startsWith("\"")) {
                    value = value.substring(1);
                }
                if (value.endsWith("\"")) {
                    value = value.substring(0, value.length() - 1);
                }
                result.setProperty(key.toLowerCase(), value);
            }
        }
        return result;
    }
    
    public static String removeComment(final String string, final String startComment, final String endComment) {
        final StringBuffer result = new StringBuffer();
        int pos = 0;
        final int end = endComment.length();
        for (int start = string.indexOf(startComment, pos); start > -1; start = string.indexOf(startComment, pos)) {
            result.append(string.substring(pos, start));
            pos = string.indexOf(endComment, start) + end;
        }
        result.append(string.substring(pos));
        return result.toString();
    }
    
    public static String eliminateWhiteSpace(final String content) {
        final StringBuffer buf = new StringBuffer();
        final int len = content.length();
        boolean newline = false;
        for (int i = 0; i < len; ++i) {
            final char character;
            switch (character = content.charAt(i)) {
                case ' ': {
                    if (!newline) {
                        buf.append(character);
                        break;
                    }
                    break;
                }
                case '\n': {
                    if (i > 0) {
                        newline = true;
                        buf.append(' ');
                        break;
                    }
                    break;
                }
                case '\r': {
                    break;
                }
                case '\t': {
                    break;
                }
                default: {
                    newline = false;
                    buf.append(character);
                    break;
                }
            }
        }
        return buf.toString();
    }
    
    public static int getIndexedFontSize(final String value, String previous) {
        int sIndex = 0;
        if (value.startsWith("+") || value.startsWith("-")) {
            if (previous == null) {
                previous = "12";
            }
            final int c = (int)Float.parseFloat(previous);
            for (int k = HtmlUtilities.FONTSIZES.length - 1; k >= 0; --k) {
                if (c >= HtmlUtilities.FONTSIZES[k]) {
                    sIndex = k;
                    break;
                }
            }
            final int diff = Integer.parseInt(value.startsWith("+") ? value.substring(1) : value);
            sIndex += diff;
        }
        else {
            try {
                sIndex = Integer.parseInt(value) - 1;
            }
            catch (NumberFormatException nfe) {
                sIndex = 0;
            }
        }
        if (sIndex < 0) {
            sIndex = 0;
        }
        else if (sIndex >= HtmlUtilities.FONTSIZES.length) {
            sIndex = HtmlUtilities.FONTSIZES.length - 1;
        }
        return HtmlUtilities.FONTSIZES[sIndex];
    }
    
    public static int alignmentValue(final String alignment) {
        if (alignment == null) {
            return -1;
        }
        if ("center".equalsIgnoreCase(alignment)) {
            return 1;
        }
        if ("left".equalsIgnoreCase(alignment)) {
            return 0;
        }
        if ("right".equalsIgnoreCase(alignment)) {
            return 2;
        }
        if ("justify".equalsIgnoreCase(alignment)) {
            return 3;
        }
        if ("JustifyAll".equalsIgnoreCase(alignment)) {
            return 8;
        }
        if ("top".equalsIgnoreCase(alignment)) {
            return 4;
        }
        if ("middle".equalsIgnoreCase(alignment)) {
            return 5;
        }
        if ("bottom".equalsIgnoreCase(alignment)) {
            return 6;
        }
        if ("baseline".equalsIgnoreCase(alignment)) {
            return 7;
        }
        return -1;
    }
    
    static {
        (HtmlUtilities.sizes = new HashMap<String, Float>()).put("xx-small", new Float(4.0f));
        HtmlUtilities.sizes.put("x-small", new Float(6.0f));
        HtmlUtilities.sizes.put("small", new Float(8.0f));
        HtmlUtilities.sizes.put("medium", new Float(10.0f));
        HtmlUtilities.sizes.put("large", new Float(13.0f));
        HtmlUtilities.sizes.put("x-large", new Float(18.0f));
        HtmlUtilities.sizes.put("xx-large", new Float(26.0f));
        FONTSIZES = new int[] { 8, 10, 12, 14, 18, 24, 36 };
    }
}
