// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html;

import java.util.HashSet;
import com.itextpdf.text.BaseColor;
import java.util.Set;

@Deprecated
public final class HtmlEncoder
{
    private static final String[] HTML_CODE;
    private static final Set<String> NEWLINETAGS;
    
    private HtmlEncoder() {
    }
    
    public static String encode(final String string) {
        final int n = string.length();
        final StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < n; ++i) {
            final char character = string.charAt(i);
            if (character < '\u0100') {
                buffer.append(HtmlEncoder.HTML_CODE[character]);
            }
            else {
                buffer.append("&#").append((int)character).append(';');
            }
        }
        return buffer.toString();
    }
    
    public static String encode(final BaseColor color) {
        final StringBuffer buffer = new StringBuffer("#");
        if (color.getRed() < 16) {
            buffer.append('0');
        }
        buffer.append(Integer.toString(color.getRed(), 16));
        if (color.getGreen() < 16) {
            buffer.append('0');
        }
        buffer.append(Integer.toString(color.getGreen(), 16));
        if (color.getBlue() < 16) {
            buffer.append('0');
        }
        buffer.append(Integer.toString(color.getBlue(), 16));
        return buffer.toString();
    }
    
    public static String getAlignment(final int alignment) {
        switch (alignment) {
            case 0: {
                return "left";
            }
            case 1: {
                return "center";
            }
            case 2: {
                return "right";
            }
            case 3:
            case 8: {
                return "justify";
            }
            case 4: {
                return "top";
            }
            case 5: {
                return "middle";
            }
            case 6: {
                return "bottom";
            }
            case 7: {
                return "baseline";
            }
            default: {
                return "";
            }
        }
    }
    
    public static boolean isNewLineTag(final String tag) {
        return HtmlEncoder.NEWLINETAGS.contains(tag);
    }
    
    static {
        HTML_CODE = new String[256];
        for (int i = 0; i < 10; ++i) {
            HtmlEncoder.HTML_CODE[i] = "&#00" + i + ";";
        }
        for (int i = 10; i < 32; ++i) {
            HtmlEncoder.HTML_CODE[i] = "&#0" + i + ";";
        }
        for (int i = 32; i < 128; ++i) {
            HtmlEncoder.HTML_CODE[i] = String.valueOf((char)i);
        }
        HtmlEncoder.HTML_CODE[9] = "\t";
        HtmlEncoder.HTML_CODE[10] = "<br />\n";
        HtmlEncoder.HTML_CODE[34] = "&quot;";
        HtmlEncoder.HTML_CODE[38] = "&amp;";
        HtmlEncoder.HTML_CODE[60] = "&lt;";
        HtmlEncoder.HTML_CODE[62] = "&gt;";
        for (int i = 128; i < 256; ++i) {
            HtmlEncoder.HTML_CODE[i] = "&#" + i + ";";
        }
        (NEWLINETAGS = new HashSet<String>()).add("p");
        HtmlEncoder.NEWLINETAGS.add("blockquote");
        HtmlEncoder.NEWLINETAGS.add("br");
    }
}
