// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html;

import java.util.StringTokenizer;
import com.itextpdf.text.error_messages.MessageLocalization;
import com.itextpdf.text.BaseColor;
import java.util.HashMap;

@Deprecated
public class WebColors extends HashMap<String, int[]>
{
    private static final long serialVersionUID = 3542523100813372896L;
    public static final WebColors NAMES;
    
    private static boolean missingHashColorFormat(final String colStr) {
        final int len = colStr.length();
        if (len == 3 || len == 6) {
            final String match = "[0-9a-f]{" + len + "}";
            return colStr.matches(match);
        }
        return false;
    }
    
    public static BaseColor getRGBColor(final String name) {
        int[] color = { 0, 0, 0, 255 };
        String colorName = name.toLowerCase();
        final boolean colorStrWithoutHash = missingHashColorFormat(colorName);
        if (colorName.startsWith("#") || colorStrWithoutHash) {
            if (!colorStrWithoutHash) {
                colorName = colorName.substring(1);
            }
            if (colorName.length() == 3) {
                final String red = colorName.substring(0, 1);
                color[0] = Integer.parseInt(red + red, 16);
                final String green = colorName.substring(1, 2);
                color[1] = Integer.parseInt(green + green, 16);
                final String blue = colorName.substring(2);
                color[2] = Integer.parseInt(blue + blue, 16);
                return new BaseColor(color[0], color[1], color[2], color[3]);
            }
            if (colorName.length() == 6) {
                color[0] = Integer.parseInt(colorName.substring(0, 2), 16);
                color[1] = Integer.parseInt(colorName.substring(2, 4), 16);
                color[2] = Integer.parseInt(colorName.substring(4), 16);
                return new BaseColor(color[0], color[1], color[2], color[3]);
            }
            throw new IllegalArgumentException(MessageLocalization.getComposedMessage("unknown.color.format.must.be.rgb.or.rrggbb", new Object[0]));
        }
        else {
            if (colorName.startsWith("rgb(")) {
                final String delim = "rgb(), \t\r\n\f";
                final StringTokenizer tok = new StringTokenizer(colorName, "rgb(), \t\r\n\f");
                for (int k = 0; k < 3; ++k) {
                    if (tok.hasMoreElements()) {
                        color[k] = getRGBChannelValue(tok.nextToken());
                        color[k] = Math.max(0, color[k]);
                        color[k] = Math.min(255, color[k]);
                    }
                }
                return new BaseColor(color[0], color[1], color[2], color[3]);
            }
            if (colorName.startsWith("rgba(")) {
                final String delim = "rgba(), \t\r\n\f";
                final StringTokenizer tok = new StringTokenizer(colorName, "rgba(), \t\r\n\f");
                for (int k = 0; k < 3; ++k) {
                    if (tok.hasMoreElements()) {
                        color[k] = getRGBChannelValue(tok.nextToken());
                        color[k] = Math.max(0, color[k]);
                        color[k] = Math.min(255, color[k]);
                    }
                }
                if (tok.hasMoreElements()) {
                    color[3] = (int)(255.0f * Float.parseFloat(tok.nextToken()) + 0.5);
                }
                return new BaseColor(color[0], color[1], color[2], color[3]);
            }
            if (!WebColors.NAMES.containsKey(colorName)) {
                throw new IllegalArgumentException(MessageLocalization.getComposedMessage("color.not.found", colorName));
            }
            color = ((HashMap<K, int[]>)WebColors.NAMES).get(colorName);
            return new BaseColor(color[0], color[1], color[2], color[3]);
        }
    }
    
    private static int getRGBChannelValue(final String rgbChannel) {
        if (rgbChannel.endsWith("%")) {
            return Integer.parseInt(rgbChannel.substring(0, rgbChannel.length() - 1)) * 255 / 100;
        }
        return Integer.parseInt(rgbChannel);
    }
    
    static {
        (NAMES = new WebColors()).put("aliceblue", new int[] { 240, 248, 255, 255 });
        WebColors.NAMES.put("antiquewhite", new int[] { 250, 235, 215, 255 });
        WebColors.NAMES.put("aqua", new int[] { 0, 255, 255, 255 });
        WebColors.NAMES.put("aquamarine", new int[] { 127, 255, 212, 255 });
        WebColors.NAMES.put("azure", new int[] { 240, 255, 255, 255 });
        WebColors.NAMES.put("beige", new int[] { 245, 245, 220, 255 });
        WebColors.NAMES.put("bisque", new int[] { 255, 228, 196, 255 });
        WebColors.NAMES.put("black", new int[] { 0, 0, 0, 255 });
        WebColors.NAMES.put("blanchedalmond", new int[] { 255, 235, 205, 255 });
        WebColors.NAMES.put("blue", new int[] { 0, 0, 255, 255 });
        WebColors.NAMES.put("blueviolet", new int[] { 138, 43, 226, 255 });
        WebColors.NAMES.put("brown", new int[] { 165, 42, 42, 255 });
        WebColors.NAMES.put("burlywood", new int[] { 222, 184, 135, 255 });
        WebColors.NAMES.put("cadetblue", new int[] { 95, 158, 160, 255 });
        WebColors.NAMES.put("chartreuse", new int[] { 127, 255, 0, 255 });
        WebColors.NAMES.put("chocolate", new int[] { 210, 105, 30, 255 });
        WebColors.NAMES.put("coral", new int[] { 255, 127, 80, 255 });
        WebColors.NAMES.put("cornflowerblue", new int[] { 100, 149, 237, 255 });
        WebColors.NAMES.put("cornsilk", new int[] { 255, 248, 220, 255 });
        WebColors.NAMES.put("crimson", new int[] { 220, 20, 60, 255 });
        WebColors.NAMES.put("cyan", new int[] { 0, 255, 255, 255 });
        WebColors.NAMES.put("darkblue", new int[] { 0, 0, 139, 255 });
        WebColors.NAMES.put("darkcyan", new int[] { 0, 139, 139, 255 });
        WebColors.NAMES.put("darkgoldenrod", new int[] { 184, 134, 11, 255 });
        WebColors.NAMES.put("darkgray", new int[] { 169, 169, 169, 255 });
        WebColors.NAMES.put("darkgreen", new int[] { 0, 100, 0, 255 });
        WebColors.NAMES.put("darkkhaki", new int[] { 189, 183, 107, 255 });
        WebColors.NAMES.put("darkmagenta", new int[] { 139, 0, 139, 255 });
        WebColors.NAMES.put("darkolivegreen", new int[] { 85, 107, 47, 255 });
        WebColors.NAMES.put("darkorange", new int[] { 255, 140, 0, 255 });
        WebColors.NAMES.put("darkorchid", new int[] { 153, 50, 204, 255 });
        WebColors.NAMES.put("darkred", new int[] { 139, 0, 0, 255 });
        WebColors.NAMES.put("darksalmon", new int[] { 233, 150, 122, 255 });
        WebColors.NAMES.put("darkseagreen", new int[] { 143, 188, 143, 255 });
        WebColors.NAMES.put("darkslateblue", new int[] { 72, 61, 139, 255 });
        WebColors.NAMES.put("darkslategray", new int[] { 47, 79, 79, 255 });
        WebColors.NAMES.put("darkturquoise", new int[] { 0, 206, 209, 255 });
        WebColors.NAMES.put("darkviolet", new int[] { 148, 0, 211, 255 });
        WebColors.NAMES.put("deeppink", new int[] { 255, 20, 147, 255 });
        WebColors.NAMES.put("deepskyblue", new int[] { 0, 191, 255, 255 });
        WebColors.NAMES.put("dimgray", new int[] { 105, 105, 105, 255 });
        WebColors.NAMES.put("dodgerblue", new int[] { 30, 144, 255, 255 });
        WebColors.NAMES.put("firebrick", new int[] { 178, 34, 34, 255 });
        WebColors.NAMES.put("floralwhite", new int[] { 255, 250, 240, 255 });
        WebColors.NAMES.put("forestgreen", new int[] { 34, 139, 34, 255 });
        WebColors.NAMES.put("fuchsia", new int[] { 255, 0, 255, 255 });
        WebColors.NAMES.put("gainsboro", new int[] { 220, 220, 220, 255 });
        WebColors.NAMES.put("ghostwhite", new int[] { 248, 248, 255, 255 });
        WebColors.NAMES.put("gold", new int[] { 255, 215, 0, 255 });
        WebColors.NAMES.put("goldenrod", new int[] { 218, 165, 32, 255 });
        WebColors.NAMES.put("gray", new int[] { 128, 128, 128, 255 });
        WebColors.NAMES.put("green", new int[] { 0, 128, 0, 255 });
        WebColors.NAMES.put("greenyellow", new int[] { 173, 255, 47, 255 });
        WebColors.NAMES.put("honeydew", new int[] { 240, 255, 240, 255 });
        WebColors.NAMES.put("hotpink", new int[] { 255, 105, 180, 255 });
        WebColors.NAMES.put("indianred", new int[] { 205, 92, 92, 255 });
        WebColors.NAMES.put("indigo", new int[] { 75, 0, 130, 255 });
        WebColors.NAMES.put("ivory", new int[] { 255, 255, 240, 255 });
        WebColors.NAMES.put("khaki", new int[] { 240, 230, 140, 255 });
        WebColors.NAMES.put("lavender", new int[] { 230, 230, 250, 255 });
        WebColors.NAMES.put("lavenderblush", new int[] { 255, 240, 245, 255 });
        WebColors.NAMES.put("lawngreen", new int[] { 124, 252, 0, 255 });
        WebColors.NAMES.put("lemonchiffon", new int[] { 255, 250, 205, 255 });
        WebColors.NAMES.put("lightblue", new int[] { 173, 216, 230, 255 });
        WebColors.NAMES.put("lightcoral", new int[] { 240, 128, 128, 255 });
        WebColors.NAMES.put("lightcyan", new int[] { 224, 255, 255, 255 });
        WebColors.NAMES.put("lightgoldenrodyellow", new int[] { 250, 250, 210, 255 });
        WebColors.NAMES.put("lightgreen", new int[] { 144, 238, 144, 255 });
        WebColors.NAMES.put("lightgrey", new int[] { 211, 211, 211, 255 });
        WebColors.NAMES.put("lightpink", new int[] { 255, 182, 193, 255 });
        WebColors.NAMES.put("lightsalmon", new int[] { 255, 160, 122, 255 });
        WebColors.NAMES.put("lightseagreen", new int[] { 32, 178, 170, 255 });
        WebColors.NAMES.put("lightskyblue", new int[] { 135, 206, 250, 255 });
        WebColors.NAMES.put("lightslategray", new int[] { 119, 136, 153, 255 });
        WebColors.NAMES.put("lightsteelblue", new int[] { 176, 196, 222, 255 });
        WebColors.NAMES.put("lightyellow", new int[] { 255, 255, 224, 255 });
        WebColors.NAMES.put("lime", new int[] { 0, 255, 0, 255 });
        WebColors.NAMES.put("limegreen", new int[] { 50, 205, 50, 255 });
        WebColors.NAMES.put("linen", new int[] { 250, 240, 230, 255 });
        WebColors.NAMES.put("magenta", new int[] { 255, 0, 255, 255 });
        WebColors.NAMES.put("maroon", new int[] { 128, 0, 0, 255 });
        WebColors.NAMES.put("mediumaquamarine", new int[] { 102, 205, 170, 255 });
        WebColors.NAMES.put("mediumblue", new int[] { 0, 0, 205, 255 });
        WebColors.NAMES.put("mediumorchid", new int[] { 186, 85, 211, 255 });
        WebColors.NAMES.put("mediumpurple", new int[] { 147, 112, 219, 255 });
        WebColors.NAMES.put("mediumseagreen", new int[] { 60, 179, 113, 255 });
        WebColors.NAMES.put("mediumslateblue", new int[] { 123, 104, 238, 255 });
        WebColors.NAMES.put("mediumspringgreen", new int[] { 0, 250, 154, 255 });
        WebColors.NAMES.put("mediumturquoise", new int[] { 72, 209, 204, 255 });
        WebColors.NAMES.put("mediumvioletred", new int[] { 199, 21, 133, 255 });
        WebColors.NAMES.put("midnightblue", new int[] { 25, 25, 112, 255 });
        WebColors.NAMES.put("mintcream", new int[] { 245, 255, 250, 255 });
        WebColors.NAMES.put("mistyrose", new int[] { 255, 228, 225, 255 });
        WebColors.NAMES.put("moccasin", new int[] { 255, 228, 181, 255 });
        WebColors.NAMES.put("navajowhite", new int[] { 255, 222, 173, 255 });
        WebColors.NAMES.put("navy", new int[] { 0, 0, 128, 255 });
        WebColors.NAMES.put("oldlace", new int[] { 253, 245, 230, 255 });
        WebColors.NAMES.put("olive", new int[] { 128, 128, 0, 255 });
        WebColors.NAMES.put("olivedrab", new int[] { 107, 142, 35, 255 });
        WebColors.NAMES.put("orange", new int[] { 255, 165, 0, 255 });
        WebColors.NAMES.put("orangered", new int[] { 255, 69, 0, 255 });
        WebColors.NAMES.put("orchid", new int[] { 218, 112, 214, 255 });
        WebColors.NAMES.put("palegoldenrod", new int[] { 238, 232, 170, 255 });
        WebColors.NAMES.put("palegreen", new int[] { 152, 251, 152, 255 });
        WebColors.NAMES.put("paleturquoise", new int[] { 175, 238, 238, 255 });
        WebColors.NAMES.put("palevioletred", new int[] { 219, 112, 147, 255 });
        WebColors.NAMES.put("papayawhip", new int[] { 255, 239, 213, 255 });
        WebColors.NAMES.put("peachpuff", new int[] { 255, 218, 185, 255 });
        WebColors.NAMES.put("peru", new int[] { 205, 133, 63, 255 });
        WebColors.NAMES.put("pink", new int[] { 255, 192, 203, 255 });
        WebColors.NAMES.put("plum", new int[] { 221, 160, 221, 255 });
        WebColors.NAMES.put("powderblue", new int[] { 176, 224, 230, 255 });
        WebColors.NAMES.put("purple", new int[] { 128, 0, 128, 255 });
        WebColors.NAMES.put("red", new int[] { 255, 0, 0, 255 });
        WebColors.NAMES.put("rosybrown", new int[] { 188, 143, 143, 255 });
        WebColors.NAMES.put("royalblue", new int[] { 65, 105, 225, 255 });
        WebColors.NAMES.put("saddlebrown", new int[] { 139, 69, 19, 255 });
        WebColors.NAMES.put("salmon", new int[] { 250, 128, 114, 255 });
        WebColors.NAMES.put("sandybrown", new int[] { 244, 164, 96, 255 });
        WebColors.NAMES.put("seagreen", new int[] { 46, 139, 87, 255 });
        WebColors.NAMES.put("seashell", new int[] { 255, 245, 238, 255 });
        WebColors.NAMES.put("sienna", new int[] { 160, 82, 45, 255 });
        WebColors.NAMES.put("silver", new int[] { 192, 192, 192, 255 });
        WebColors.NAMES.put("skyblue", new int[] { 135, 206, 235, 255 });
        WebColors.NAMES.put("slateblue", new int[] { 106, 90, 205, 255 });
        WebColors.NAMES.put("slategray", new int[] { 112, 128, 144, 255 });
        WebColors.NAMES.put("snow", new int[] { 255, 250, 250, 255 });
        WebColors.NAMES.put("springgreen", new int[] { 0, 255, 127, 255 });
        WebColors.NAMES.put("steelblue", new int[] { 70, 130, 180, 255 });
        WebColors.NAMES.put("tan", new int[] { 210, 180, 140, 255 });
        WebColors.NAMES.put("teal", new int[] { 0, 128, 128, 255 });
        WebColors.NAMES.put("thistle", new int[] { 216, 191, 216, 255 });
        WebColors.NAMES.put("tomato", new int[] { 255, 99, 71, 255 });
        WebColors.NAMES.put("transparent", new int[] { 255, 255, 255, 0 });
        WebColors.NAMES.put("turquoise", new int[] { 64, 224, 208, 255 });
        WebColors.NAMES.put("violet", new int[] { 238, 130, 238, 255 });
        WebColors.NAMES.put("wheat", new int[] { 245, 222, 179, 255 });
        WebColors.NAMES.put("white", new int[] { 255, 255, 255, 255 });
        WebColors.NAMES.put("whitesmoke", new int[] { 245, 245, 245, 255 });
        WebColors.NAMES.put("yellow", new int[] { 255, 255, 0, 255 });
        WebColors.NAMES.put("yellowgreen", new int[] { 154, 205, 50, 255 });
    }
}
