// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html.simpleparser;

import com.itextpdf.text.log.LoggerFactory;
import com.itextpdf.text.Rectangle;
import java.util.Collections;
import com.itextpdf.text.pdf.PdfPCell;
import java.util.ArrayList;
import com.itextpdf.text.pdf.PdfPTable;
import java.util.Iterator;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.FontProvider;
import com.itextpdf.text.TextElementArray;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.html.HtmlUtilities;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.ExceptionConverter;
import java.io.IOException;
import com.itextpdf.text.xml.simpleparser.SimpleXMLDocHandlerComment;
import com.itextpdf.text.xml.simpleparser.SimpleXMLParser;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Element;
import java.util.Stack;
import java.util.Map;
import com.itextpdf.text.log.Logger;
import com.itextpdf.text.DocListener;
import com.itextpdf.text.xml.simpleparser.SimpleXMLDocHandler;

@Deprecated
public class HTMLWorker implements SimpleXMLDocHandler, DocListener
{
    private static Logger LOGGER;
    protected DocListener document;
    protected Map<String, HTMLTagProcessor> tags;
    private StyleSheet style;
    protected Stack<Element> stack;
    protected Paragraph currentParagraph;
    private final ChainedProperties chain;
    public static final String IMG_PROVIDER = "img_provider";
    public static final String IMG_PROCESSOR = "img_interface";
    public static final String IMG_STORE = "img_static";
    public static final String IMG_BASEURL = "img_baseurl";
    public static final String FONT_PROVIDER = "font_factory";
    public static final String LINK_PROVIDER = "alink_interface";
    private Map<String, Object> providers;
    private final ElementFactory factory;
    private final Stack<boolean[]> tableState;
    private boolean pendingTR;
    private boolean pendingTD;
    private boolean pendingLI;
    private boolean insidePRE;
    protected boolean skipText;
    protected List<Element> objectList;
    
    public HTMLWorker(final DocListener document) {
        this(document, null, null);
    }
    
    public HTMLWorker(final DocListener document, final Map<String, HTMLTagProcessor> tags, final StyleSheet style) {
        this.style = new StyleSheet();
        this.stack = new Stack<Element>();
        this.chain = new ChainedProperties();
        this.providers = new HashMap<String, Object>();
        this.factory = new ElementFactory();
        this.tableState = new Stack<boolean[]>();
        this.pendingTR = false;
        this.pendingTD = false;
        this.pendingLI = false;
        this.insidePRE = false;
        this.skipText = false;
        this.document = document;
        this.setSupportedTags(tags);
        this.setStyleSheet(style);
    }
    
    public void setSupportedTags(Map<String, HTMLTagProcessor> tags) {
        if (tags == null) {
            tags = new HTMLTagProcessors();
        }
        this.tags = tags;
    }
    
    public void setStyleSheet(StyleSheet style) {
        if (style == null) {
            style = new StyleSheet();
        }
        this.style = style;
    }
    
    public void parse(final Reader reader) throws IOException {
        HTMLWorker.LOGGER.info("Please note, there is a more extended version of the HTMLWorker available in the iText XMLWorker");
        SimpleXMLParser.parse(this, null, reader, true);
    }
    
    @Override
    public void startDocument() {
        final HashMap<String, String> attrs = new HashMap<String, String>();
        this.style.applyStyle("body", attrs);
        this.chain.addToChain("body", attrs);
    }
    
    @Override
    public void startElement(final String tag, final Map<String, String> attrs) {
        final HTMLTagProcessor htmlTag = this.tags.get(tag);
        if (htmlTag == null) {
            return;
        }
        this.style.applyStyle(tag, attrs);
        StyleSheet.resolveStyleAttribute(attrs, this.chain);
        try {
            htmlTag.startElement(this, tag, attrs);
        }
        catch (DocumentException e) {
            throw new ExceptionConverter(e);
        }
        catch (IOException e2) {
            throw new ExceptionConverter(e2);
        }
    }
    
    @Override
    public void text(String content) {
        if (this.skipText) {
            return;
        }
        if (this.currentParagraph == null) {
            this.currentParagraph = this.createParagraph();
        }
        if (!this.insidePRE) {
            if (content.trim().length() == 0 && content.indexOf(32) < 0) {
                return;
            }
            content = HtmlUtilities.eliminateWhiteSpace(content);
        }
        final Chunk chunk = this.createChunk(content);
        this.currentParagraph.add(chunk);
    }
    
    @Override
    public void endElement(final String tag) {
        final HTMLTagProcessor htmlTag = this.tags.get(tag);
        if (htmlTag == null) {
            return;
        }
        try {
            htmlTag.endElement(this, tag);
        }
        catch (DocumentException e) {
            throw new ExceptionConverter(e);
        }
    }
    
    @Override
    public void endDocument() {
        try {
            for (int k = 0; k < this.stack.size(); ++k) {
                this.document.add(this.stack.elementAt(k));
            }
            if (this.currentParagraph != null) {
                this.document.add(this.currentParagraph);
            }
            this.currentParagraph = null;
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public void newLine() {
        if (this.currentParagraph == null) {
            this.currentParagraph = new Paragraph();
        }
        this.currentParagraph.add(this.createChunk("\n"));
    }
    
    public void carriageReturn() throws DocumentException {
        if (this.currentParagraph == null) {
            return;
        }
        if (this.stack.empty()) {
            this.document.add(this.currentParagraph);
        }
        else {
            final Element obj = this.stack.pop();
            if (obj instanceof TextElementArray) {
                final TextElementArray current = (TextElementArray)obj;
                current.add(this.currentParagraph);
            }
            this.stack.push(obj);
        }
        this.currentParagraph = null;
    }
    
    public void flushContent() {
        this.pushToStack(this.currentParagraph);
        this.currentParagraph = new Paragraph();
    }
    
    public void pushToStack(final Element element) {
        if (element != null) {
            this.stack.push(element);
        }
    }
    
    public void updateChain(final String tag, final Map<String, String> attrs) {
        this.chain.addToChain(tag, attrs);
    }
    
    public void updateChain(final String tag) {
        this.chain.removeChain(tag);
    }
    
    public void setProviders(final Map<String, Object> providers) {
        if (providers == null) {
            return;
        }
        this.providers = providers;
        FontProvider ff = null;
        if (providers != null) {
            ff = providers.get("font_factory");
        }
        if (ff != null) {
            this.factory.setFontProvider(ff);
        }
    }
    
    public Chunk createChunk(final String content) {
        return this.factory.createChunk(content, this.chain);
    }
    
    public Paragraph createParagraph() {
        return this.factory.createParagraph(this.chain);
    }
    
    public com.itextpdf.text.List createList(final String tag) {
        return this.factory.createList(tag, this.chain);
    }
    
    public ListItem createListItem() {
        return this.factory.createListItem(this.chain);
    }
    
    public LineSeparator createLineSeparator(final Map<String, String> attrs) {
        return this.factory.createLineSeparator(attrs, this.currentParagraph.getLeading() / 2.0f);
    }
    
    public Image createImage(final Map<String, String> attrs) throws DocumentException, IOException {
        final String src = attrs.get("src");
        if (src == null) {
            return null;
        }
        final Image img = this.factory.createImage(src, attrs, this.chain, this.document, this.providers.get("img_provider"), this.providers.get("img_static"), this.providers.get("img_baseurl"));
        return img;
    }
    
    public CellWrapper createCell(final String tag) {
        return new CellWrapper(tag, this.chain);
    }
    
    public void processLink() {
        if (this.currentParagraph == null) {
            this.currentParagraph = new Paragraph();
        }
        final LinkProcessor i = this.providers.get("alink_interface");
        if (i == null || !i.process(this.currentParagraph, this.chain)) {
            final String href = this.chain.getProperty("href");
            if (href != null) {
                for (final Chunk ck : this.currentParagraph.getChunks()) {
                    ck.setAnchor(href);
                }
            }
        }
        if (this.stack.isEmpty()) {
            final Paragraph tmp = new Paragraph(new Phrase(this.currentParagraph));
            this.currentParagraph = tmp;
        }
        else {
            final Paragraph tmp = this.stack.pop();
            tmp.add(new Phrase(this.currentParagraph));
            this.currentParagraph = tmp;
        }
    }
    
    public void processList() throws DocumentException {
        if (this.stack.empty()) {
            return;
        }
        final Element obj = this.stack.pop();
        if (!(obj instanceof com.itextpdf.text.List)) {
            this.stack.push(obj);
            return;
        }
        if (this.stack.empty()) {
            this.document.add(obj);
        }
        else {
            this.stack.peek().add(obj);
        }
    }
    
    public void processListItem() throws DocumentException {
        if (this.stack.empty()) {
            return;
        }
        final Element obj = this.stack.pop();
        if (!(obj instanceof ListItem)) {
            this.stack.push(obj);
            return;
        }
        if (this.stack.empty()) {
            this.document.add(obj);
            return;
        }
        final ListItem item = (ListItem)obj;
        final Element list = this.stack.pop();
        if (!(list instanceof com.itextpdf.text.List)) {
            this.stack.push(list);
            return;
        }
        ((com.itextpdf.text.List)list).add(item);
        item.adjustListSymbolFont();
        this.stack.push(list);
    }
    
    public void processImage(final Image img, final Map<String, String> attrs) throws DocumentException {
        final ImageProcessor processor = this.providers.get("img_interface");
        if (processor == null || !processor.process(img, attrs, this.chain, this.document)) {
            final String align = attrs.get("align");
            if (align != null) {
                this.carriageReturn();
            }
            if (this.currentParagraph == null) {
                this.currentParagraph = this.createParagraph();
            }
            this.currentParagraph.add(new Chunk(img, 0.0f, 0.0f, true));
            this.currentParagraph.setAlignment(HtmlUtilities.alignmentValue(align));
            if (align != null) {
                this.carriageReturn();
            }
        }
    }
    
    public void processTable() throws DocumentException {
        final TableWrapper table = this.stack.pop();
        final PdfPTable tb = table.createTable();
        tb.setSplitRows(true);
        if (this.stack.empty()) {
            this.document.add(tb);
        }
        else {
            this.stack.peek().add(tb);
        }
    }
    
    public void processRow() {
        final ArrayList<PdfPCell> row = new ArrayList<PdfPCell>();
        final ArrayList<Float> cellWidths = new ArrayList<Float>();
        boolean percentage = false;
        float totalWidth = 0.0f;
        int zeroWidth = 0;
        TableWrapper table = null;
        Element obj;
        do {
            obj = this.stack.pop();
            if (obj instanceof CellWrapper) {
                final CellWrapper cell = (CellWrapper)obj;
                final float width = cell.getWidth();
                cellWidths.add(new Float(width));
                percentage |= cell.isPercentage();
                if (width == 0.0f) {
                    ++zeroWidth;
                }
                else {
                    totalWidth += width;
                }
                row.add(cell.getCell());
            }
        } while (!(obj instanceof TableWrapper));
        table = (TableWrapper)obj;
        table.addRow(row);
        if (cellWidths.size() > 0) {
            totalWidth = 100.0f - totalWidth;
            Collections.reverse(cellWidths);
            final float[] widths = new float[cellWidths.size()];
            boolean hasZero = false;
            for (int i = 0; i < widths.length; ++i) {
                widths[i] = cellWidths.get(i);
                if (widths[i] == 0.0f && percentage && zeroWidth > 0) {
                    widths[i] = totalWidth / zeroWidth;
                }
                if (widths[i] == 0.0f) {
                    hasZero = true;
                    break;
                }
            }
            if (!hasZero) {
                table.setColWidths(widths);
            }
        }
        this.stack.push(table);
    }
    
    public void pushTableState() {
        this.tableState.push(new boolean[] { this.pendingTR, this.pendingTD });
    }
    
    public void popTableState() {
        final boolean[] state = this.tableState.pop();
        this.pendingTR = state[0];
        this.pendingTD = state[1];
    }
    
    public boolean isPendingTR() {
        return this.pendingTR;
    }
    
    public void setPendingTR(final boolean pendingTR) {
        this.pendingTR = pendingTR;
    }
    
    public boolean isPendingTD() {
        return this.pendingTD;
    }
    
    public void setPendingTD(final boolean pendingTD) {
        this.pendingTD = pendingTD;
    }
    
    public boolean isPendingLI() {
        return this.pendingLI;
    }
    
    public void setPendingLI(final boolean pendingLI) {
        this.pendingLI = pendingLI;
    }
    
    public boolean isInsidePRE() {
        return this.insidePRE;
    }
    
    public void setInsidePRE(final boolean insidePRE) {
        this.insidePRE = insidePRE;
    }
    
    public boolean isSkipText() {
        return this.skipText;
    }
    
    public void setSkipText(final boolean skipText) {
        this.skipText = skipText;
    }
    
    public static List<Element> parseToList(final Reader reader, final StyleSheet style) throws IOException {
        return parseToList(reader, style, null);
    }
    
    public static List<Element> parseToList(final Reader reader, final StyleSheet style, final HashMap<String, Object> providers) throws IOException {
        return parseToList(reader, style, null, providers);
    }
    
    public static List<Element> parseToList(final Reader reader, final StyleSheet style, final Map<String, HTMLTagProcessor> tags, final HashMap<String, Object> providers) throws IOException {
        final HTMLWorker worker = new HTMLWorker(null, tags, style);
        ((HTMLWorker)(worker.document = worker)).setProviders(providers);
        worker.objectList = new ArrayList<Element>();
        worker.parse(reader);
        return worker.objectList;
    }
    
    @Override
    public boolean add(final Element element) throws DocumentException {
        this.objectList.add(element);
        return true;
    }
    
    @Override
    public void close() {
    }
    
    @Override
    public boolean newPage() {
        return true;
    }
    
    @Override
    public void open() {
    }
    
    @Override
    public void resetPageCount() {
    }
    
    @Override
    public boolean setMarginMirroring(final boolean marginMirroring) {
        return false;
    }
    
    @Override
    public boolean setMarginMirroringTopBottom(final boolean marginMirroring) {
        return false;
    }
    
    @Override
    public boolean setMargins(final float marginLeft, final float marginRight, final float marginTop, final float marginBottom) {
        return true;
    }
    
    @Override
    public void setPageCount(final int pageN) {
    }
    
    @Override
    public boolean setPageSize(final Rectangle pageSize) {
        return true;
    }
    
    @Deprecated
    public void setInterfaceProps(final HashMap<String, Object> providers) {
        this.setProviders(providers);
    }
    
    @Deprecated
    public Map<String, Object> getInterfaceProps() {
        return this.providers;
    }
    
    static {
        HTMLWorker.LOGGER = LoggerFactory.getLogger(HTMLWorker.class);
    }
}
