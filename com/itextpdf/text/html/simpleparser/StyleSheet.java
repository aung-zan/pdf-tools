// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html.simpleparser;

import com.itextpdf.text.BaseColor;
import java.util.Iterator;
import java.util.Properties;
import com.itextpdf.text.html.HtmlUtilities;
import java.util.HashMap;
import java.util.Map;

@Deprecated
public class StyleSheet
{
    protected Map<String, Map<String, String>> tagMap;
    protected Map<String, Map<String, String>> classMap;
    
    public StyleSheet() {
        this.tagMap = new HashMap<String, Map<String, String>>();
        this.classMap = new HashMap<String, Map<String, String>>();
    }
    
    public void loadTagStyle(final String tag, final Map<String, String> attrs) {
        this.tagMap.put(tag.toLowerCase(), attrs);
    }
    
    public void loadTagStyle(String tag, final String key, final String value) {
        tag = tag.toLowerCase();
        Map<String, String> styles = this.tagMap.get(tag);
        if (styles == null) {
            styles = new HashMap<String, String>();
            this.tagMap.put(tag, styles);
        }
        styles.put(key, value);
    }
    
    public void loadStyle(final String className, final HashMap<String, String> attrs) {
        this.classMap.put(className.toLowerCase(), attrs);
    }
    
    public void loadStyle(String className, final String key, final String value) {
        className = className.toLowerCase();
        Map<String, String> styles = this.classMap.get(className);
        if (styles == null) {
            styles = new HashMap<String, String>();
            this.classMap.put(className, styles);
        }
        styles.put(key, value);
    }
    
    public void applyStyle(final String tag, final Map<String, String> attrs) {
        Map<String, String> map = this.tagMap.get(tag.toLowerCase());
        if (map != null) {
            final Map<String, String> temp = new HashMap<String, String>(map);
            temp.putAll(attrs);
            attrs.putAll(temp);
        }
        final String cm = attrs.get("class");
        if (cm == null) {
            return;
        }
        map = this.classMap.get(cm.toLowerCase());
        if (map == null) {
            return;
        }
        attrs.remove("class");
        final Map<String, String> temp2 = new HashMap<String, String>(map);
        temp2.putAll(attrs);
        attrs.putAll(temp2);
    }
    
    public static void resolveStyleAttribute(final Map<String, String> h, final ChainedProperties chain) {
        final String style = h.get("style");
        if (style == null) {
            return;
        }
        final Properties prop = HtmlUtilities.parseAttributes(style);
        for (final Object element : prop.keySet()) {
            final String key = (String)element;
            if (key.equals("font-family")) {
                h.put("face", prop.getProperty(key));
            }
            else if (key.equals("font-size")) {
                float actualFontSize = HtmlUtilities.parseLength(chain.getProperty("size"), 12.0f);
                if (actualFontSize <= 0.0f) {
                    actualFontSize = 12.0f;
                }
                h.put("size", Float.toString(HtmlUtilities.parseLength(prop.getProperty(key), actualFontSize)) + "pt");
            }
            else if (key.equals("font-style")) {
                final String ss = prop.getProperty(key).trim().toLowerCase();
                if (!ss.equals("italic") && !ss.equals("oblique")) {
                    continue;
                }
                h.put("i", null);
            }
            else if (key.equals("font-weight")) {
                final String ss = prop.getProperty(key).trim().toLowerCase();
                if (!ss.equals("bold") && !ss.equals("700") && !ss.equals("800") && !ss.equals("900")) {
                    continue;
                }
                h.put("b", null);
            }
            else if (key.equals("text-decoration")) {
                final String ss = prop.getProperty(key).trim().toLowerCase();
                if (!ss.equals("underline")) {
                    continue;
                }
                h.put("u", null);
            }
            else if (key.equals("color")) {
                final BaseColor c = HtmlUtilities.decodeColor(prop.getProperty(key));
                if (c == null) {
                    continue;
                }
                final int hh = c.getRGB();
                String hs = Integer.toHexString(hh);
                hs = "000000" + hs;
                hs = "#" + hs.substring(hs.length() - 6);
                h.put("color", hs);
            }
            else if (key.equals("line-height")) {
                final String ss = prop.getProperty(key).trim();
                float actualFontSize2 = HtmlUtilities.parseLength(chain.getProperty("size"), 12.0f);
                if (actualFontSize2 <= 0.0f) {
                    actualFontSize2 = 12.0f;
                }
                final float v = HtmlUtilities.parseLength(prop.getProperty(key), actualFontSize2);
                if (ss.endsWith("%")) {
                    h.put("leading", "0," + v / 100.0f);
                    return;
                }
                if ("normal".equalsIgnoreCase(ss)) {
                    h.put("leading", "0,1.5");
                    return;
                }
                h.put("leading", v + ",0");
            }
            else if (key.equals("text-align")) {
                final String ss = prop.getProperty(key).trim().toLowerCase();
                h.put("align", ss);
            }
            else {
                if (!key.equals("padding-left")) {
                    continue;
                }
                final String ss = prop.getProperty(key).trim().toLowerCase();
                h.put("indent", Float.toString(HtmlUtilities.parseLength(ss)));
            }
        }
    }
}
