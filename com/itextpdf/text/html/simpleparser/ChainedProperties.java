// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html.simpleparser;

import com.itextpdf.text.html.HtmlUtilities;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class ChainedProperties
{
    public List<TagAttributes> chain;
    
    public ChainedProperties() {
        this.chain = new ArrayList<TagAttributes>();
    }
    
    public String getProperty(final String key) {
        for (int k = this.chain.size() - 1; k >= 0; --k) {
            final TagAttributes p = this.chain.get(k);
            final Map<String, String> attrs = p.attrs;
            final String ret = attrs.get(key);
            if (ret != null) {
                return ret;
            }
        }
        return null;
    }
    
    public boolean hasProperty(final String key) {
        for (int k = this.chain.size() - 1; k >= 0; --k) {
            final TagAttributes p = this.chain.get(k);
            final Map<String, String> attrs = p.attrs;
            if (attrs.containsKey(key)) {
                return true;
            }
        }
        return false;
    }
    
    public void addToChain(final String tag, final Map<String, String> props) {
        this.adjustFontSize(props);
        this.chain.add(new TagAttributes(tag, props));
    }
    
    public void removeChain(final String tag) {
        for (int k = this.chain.size() - 1; k >= 0; --k) {
            if (tag.equals(this.chain.get(k).tag)) {
                this.chain.remove(k);
                return;
            }
        }
    }
    
    protected void adjustFontSize(final Map<String, String> attrs) {
        final String value = attrs.get("size");
        if (value == null) {
            return;
        }
        if (value.endsWith("pt")) {
            attrs.put("size", value.substring(0, value.length() - 2));
            return;
        }
        final String old = this.getProperty("size");
        attrs.put("size", Integer.toString(HtmlUtilities.getIndexedFontSize(value, old)));
    }
    
    private static final class TagAttributes
    {
        final String tag;
        final Map<String, String> attrs;
        
        TagAttributes(final String tag, final Map<String, String> attrs) {
            this.tag = tag;
            this.attrs = attrs;
        }
    }
}
