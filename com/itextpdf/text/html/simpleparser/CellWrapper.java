// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html.simpleparser;

import com.itextpdf.text.ElementListener;
import com.itextpdf.text.Chunk;
import java.util.List;
import com.itextpdf.text.Element;
import com.itextpdf.text.html.HtmlUtilities;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.TextElementArray;

@Deprecated
public class CellWrapper implements TextElementArray
{
    private final PdfPCell cell;
    private float width;
    private boolean percentage;
    
    public CellWrapper(final String tag, final ChainedProperties chain) {
        this.cell = this.createPdfPCell(tag, chain);
        String value = chain.getProperty("width");
        if (value != null) {
            value = value.trim();
            if (value.endsWith("%")) {
                this.percentage = true;
                value = value.substring(0, value.length() - 1);
            }
            this.width = Float.parseFloat(value);
        }
    }
    
    public PdfPCell createPdfPCell(final String tag, final ChainedProperties chain) {
        final PdfPCell cell = new PdfPCell((Phrase)null);
        String value = chain.getProperty("colspan");
        if (value != null) {
            cell.setColspan(Integer.parseInt(value));
        }
        value = chain.getProperty("rowspan");
        if (value != null) {
            cell.setRowspan(Integer.parseInt(value));
        }
        if (tag.equals("th")) {
            cell.setHorizontalAlignment(1);
        }
        value = chain.getProperty("align");
        if (value != null) {
            cell.setHorizontalAlignment(HtmlUtilities.alignmentValue(value));
        }
        value = chain.getProperty("valign");
        cell.setVerticalAlignment(5);
        if (value != null) {
            cell.setVerticalAlignment(HtmlUtilities.alignmentValue(value));
        }
        value = chain.getProperty("border");
        float border = 0.0f;
        if (value != null) {
            border = Float.parseFloat(value);
        }
        cell.setBorderWidth(border);
        value = chain.getProperty("cellpadding");
        if (value != null) {
            cell.setPadding(Float.parseFloat(value));
        }
        cell.setUseDescender(true);
        value = chain.getProperty("bgcolor");
        cell.setBackgroundColor(HtmlUtilities.decodeColor(value));
        return cell;
    }
    
    public PdfPCell getCell() {
        return this.cell;
    }
    
    public float getWidth() {
        return this.width;
    }
    
    public boolean isPercentage() {
        return this.percentage;
    }
    
    @Override
    public boolean add(final Element o) {
        this.cell.addElement(o);
        return true;
    }
    
    @Override
    public List<Chunk> getChunks() {
        return null;
    }
    
    @Override
    public boolean isContent() {
        return false;
    }
    
    @Override
    public boolean isNestable() {
        return false;
    }
    
    @Override
    public boolean process(final ElementListener listener) {
        return false;
    }
    
    @Override
    public int type() {
        return 0;
    }
}
