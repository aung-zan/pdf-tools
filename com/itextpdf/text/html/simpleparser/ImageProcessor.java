// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html.simpleparser;

import com.itextpdf.text.DocListener;
import java.util.Map;
import com.itextpdf.text.Image;

@Deprecated
public interface ImageProcessor
{
    boolean process(final Image p0, final Map<String, String> p1, final ChainedProperties p2, final DocListener p3);
}
