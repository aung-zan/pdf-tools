// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html.simpleparser;

import java.io.IOException;
import com.itextpdf.text.DocumentException;
import java.util.Map;

@Deprecated
public interface HTMLTagProcessor
{
    void startElement(final HTMLWorker p0, final String p1, final Map<String, String> p2) throws DocumentException, IOException;
    
    void endElement(final HTMLWorker p0, final String p1) throws DocumentException;
}
