// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html.simpleparser;

import java.io.IOException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import java.util.Map;
import java.util.HashMap;

@Deprecated
public class HTMLTagProcessors extends HashMap<String, HTMLTagProcessor>
{
    public static final HTMLTagProcessor EM_STRONG_STRIKE_SUP_SUP;
    public static final HTMLTagProcessor A;
    public static final HTMLTagProcessor BR;
    public static final HTMLTagProcessor UL_OL;
    public static final HTMLTagProcessor HR;
    public static final HTMLTagProcessor SPAN;
    public static final HTMLTagProcessor H;
    public static final HTMLTagProcessor LI;
    public static final HTMLTagProcessor PRE;
    public static final HTMLTagProcessor DIV;
    public static final HTMLTagProcessor TABLE;
    public static final HTMLTagProcessor TR;
    public static final HTMLTagProcessor TD;
    public static final HTMLTagProcessor IMG;
    private static final long serialVersionUID = -959260811961222824L;
    
    public HTMLTagProcessors() {
        this.put("a", HTMLTagProcessors.A);
        this.put("b", HTMLTagProcessors.EM_STRONG_STRIKE_SUP_SUP);
        this.put("body", HTMLTagProcessors.DIV);
        this.put("br", HTMLTagProcessors.BR);
        this.put("div", HTMLTagProcessors.DIV);
        this.put("em", HTMLTagProcessors.EM_STRONG_STRIKE_SUP_SUP);
        this.put("font", HTMLTagProcessors.SPAN);
        this.put("h1", HTMLTagProcessors.H);
        this.put("h2", HTMLTagProcessors.H);
        this.put("h3", HTMLTagProcessors.H);
        this.put("h4", HTMLTagProcessors.H);
        this.put("h5", HTMLTagProcessors.H);
        this.put("h6", HTMLTagProcessors.H);
        this.put("hr", HTMLTagProcessors.HR);
        this.put("i", HTMLTagProcessors.EM_STRONG_STRIKE_SUP_SUP);
        this.put("img", HTMLTagProcessors.IMG);
        this.put("li", HTMLTagProcessors.LI);
        this.put("ol", HTMLTagProcessors.UL_OL);
        this.put("p", HTMLTagProcessors.DIV);
        this.put("pre", HTMLTagProcessors.PRE);
        this.put("s", HTMLTagProcessors.EM_STRONG_STRIKE_SUP_SUP);
        this.put("span", HTMLTagProcessors.SPAN);
        this.put("strike", HTMLTagProcessors.EM_STRONG_STRIKE_SUP_SUP);
        this.put("strong", HTMLTagProcessors.EM_STRONG_STRIKE_SUP_SUP);
        this.put("sub", HTMLTagProcessors.EM_STRONG_STRIKE_SUP_SUP);
        this.put("sup", HTMLTagProcessors.EM_STRONG_STRIKE_SUP_SUP);
        this.put("table", HTMLTagProcessors.TABLE);
        this.put("td", HTMLTagProcessors.TD);
        this.put("th", HTMLTagProcessors.TD);
        this.put("tr", HTMLTagProcessors.TR);
        this.put("u", HTMLTagProcessors.EM_STRONG_STRIKE_SUP_SUP);
        this.put("ul", HTMLTagProcessors.UL_OL);
    }
    
    static {
        EM_STRONG_STRIKE_SUP_SUP = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, String tag, final Map<String, String> attrs) {
                tag = this.mapTag(tag);
                attrs.put(tag, null);
                worker.updateChain(tag, attrs);
            }
            
            @Override
            public void endElement(final HTMLWorker worker, String tag) {
                tag = this.mapTag(tag);
                worker.updateChain(tag);
            }
            
            private String mapTag(final String tag) {
                if ("em".equalsIgnoreCase(tag)) {
                    return "i";
                }
                if ("strong".equalsIgnoreCase(tag)) {
                    return "b";
                }
                if ("strike".equalsIgnoreCase(tag)) {
                    return "s";
                }
                return tag;
            }
        };
        A = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, final String tag, final Map<String, String> attrs) {
                worker.updateChain(tag, attrs);
                worker.flushContent();
            }
            
            @Override
            public void endElement(final HTMLWorker worker, final String tag) {
                worker.processLink();
                worker.updateChain(tag);
            }
        };
        BR = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, final String tag, final Map<String, String> attrs) {
                worker.newLine();
            }
            
            @Override
            public void endElement(final HTMLWorker worker, final String tag) {
            }
        };
        UL_OL = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, final String tag, final Map<String, String> attrs) throws DocumentException {
                worker.carriageReturn();
                if (worker.isPendingLI()) {
                    worker.endElement("li");
                }
                worker.setSkipText(true);
                worker.updateChain(tag, attrs);
                worker.pushToStack(worker.createList(tag));
            }
            
            @Override
            public void endElement(final HTMLWorker worker, final String tag) throws DocumentException {
                worker.carriageReturn();
                if (worker.isPendingLI()) {
                    worker.endElement("li");
                }
                worker.setSkipText(false);
                worker.updateChain(tag);
                worker.processList();
            }
        };
        HR = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, final String tag, final Map<String, String> attrs) throws DocumentException {
                worker.carriageReturn();
                worker.pushToStack(worker.createLineSeparator(attrs));
            }
            
            @Override
            public void endElement(final HTMLWorker worker, final String tag) {
            }
        };
        SPAN = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, final String tag, final Map<String, String> attrs) {
                worker.updateChain(tag, attrs);
            }
            
            @Override
            public void endElement(final HTMLWorker worker, final String tag) {
                worker.updateChain(tag);
            }
        };
        H = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, final String tag, final Map<String, String> attrs) throws DocumentException {
                worker.carriageReturn();
                if (!attrs.containsKey("size")) {
                    final int v = 7 - Integer.parseInt(tag.substring(1));
                    attrs.put("size", Integer.toString(v));
                }
                worker.updateChain(tag, attrs);
            }
            
            @Override
            public void endElement(final HTMLWorker worker, final String tag) throws DocumentException {
                worker.carriageReturn();
                worker.updateChain(tag);
            }
        };
        LI = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, final String tag, final Map<String, String> attrs) throws DocumentException {
                worker.carriageReturn();
                if (worker.isPendingLI()) {
                    worker.endElement(tag);
                }
                worker.setSkipText(false);
                worker.setPendingLI(true);
                worker.updateChain(tag, attrs);
                worker.pushToStack(worker.createListItem());
            }
            
            @Override
            public void endElement(final HTMLWorker worker, final String tag) throws DocumentException {
                worker.carriageReturn();
                worker.setPendingLI(false);
                worker.setSkipText(true);
                worker.updateChain(tag);
                worker.processListItem();
            }
        };
        PRE = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, final String tag, final Map<String, String> attrs) throws DocumentException {
                worker.carriageReturn();
                if (!attrs.containsKey("face")) {
                    attrs.put("face", "Courier");
                }
                worker.updateChain(tag, attrs);
                worker.setInsidePRE(true);
            }
            
            @Override
            public void endElement(final HTMLWorker worker, final String tag) throws DocumentException {
                worker.carriageReturn();
                worker.updateChain(tag);
                worker.setInsidePRE(false);
            }
        };
        DIV = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, final String tag, final Map<String, String> attrs) throws DocumentException {
                worker.carriageReturn();
                worker.updateChain(tag, attrs);
            }
            
            @Override
            public void endElement(final HTMLWorker worker, final String tag) throws DocumentException {
                worker.carriageReturn();
                worker.updateChain(tag);
            }
        };
        TABLE = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, final String tag, final Map<String, String> attrs) throws DocumentException {
                worker.carriageReturn();
                final TableWrapper table = new TableWrapper(attrs);
                worker.pushToStack(table);
                worker.pushTableState();
                worker.setPendingTD(false);
                worker.setPendingTR(false);
                worker.setSkipText(true);
                attrs.remove("align");
                attrs.put("colspan", "1");
                attrs.put("rowspan", "1");
                worker.updateChain(tag, attrs);
            }
            
            @Override
            public void endElement(final HTMLWorker worker, final String tag) throws DocumentException {
                worker.carriageReturn();
                if (worker.isPendingTR()) {
                    worker.endElement("tr");
                }
                worker.updateChain(tag);
                worker.processTable();
                worker.popTableState();
                worker.setSkipText(false);
            }
        };
        TR = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, final String tag, final Map<String, String> attrs) throws DocumentException {
                worker.carriageReturn();
                if (worker.isPendingTR()) {
                    worker.endElement(tag);
                }
                worker.setSkipText(true);
                worker.setPendingTR(true);
                worker.updateChain(tag, attrs);
            }
            
            @Override
            public void endElement(final HTMLWorker worker, final String tag) throws DocumentException {
                worker.carriageReturn();
                if (worker.isPendingTD()) {
                    worker.endElement("td");
                }
                worker.setPendingTR(false);
                worker.updateChain(tag);
                worker.processRow();
                worker.setSkipText(true);
            }
        };
        TD = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, final String tag, final Map<String, String> attrs) throws DocumentException {
                worker.carriageReturn();
                if (worker.isPendingTD()) {
                    worker.endElement(tag);
                }
                worker.setSkipText(false);
                worker.setPendingTD(true);
                worker.updateChain("td", attrs);
                worker.pushToStack(worker.createCell(tag));
            }
            
            @Override
            public void endElement(final HTMLWorker worker, final String tag) throws DocumentException {
                worker.carriageReturn();
                worker.setPendingTD(false);
                worker.updateChain("td");
                worker.setSkipText(true);
            }
        };
        IMG = new HTMLTagProcessor() {
            @Override
            public void startElement(final HTMLWorker worker, final String tag, final Map<String, String> attrs) throws DocumentException, IOException {
                worker.updateChain(tag, attrs);
                worker.processImage(worker.createImage(attrs), attrs);
                worker.updateChain(tag);
            }
            
            @Override
            public void endElement(final HTMLWorker worker, final String tag) {
            }
        };
    }
}
