// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html.simpleparser;

import com.itextpdf.text.List;
import java.io.IOException;
import com.itextpdf.text.DocumentException;
import java.io.File;
import com.itextpdf.text.Image;
import java.util.HashMap;
import com.itextpdf.text.DocListener;
import com.itextpdf.text.pdf.draw.LineSeparator;
import java.util.Map;
import com.itextpdf.text.pdf.HyphenationAuto;
import com.itextpdf.text.pdf.HyphenationEvent;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.html.HtmlUtilities;
import java.util.StringTokenizer;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.FontProvider;

@Deprecated
public class ElementFactory
{
    private FontProvider provider;
    
    public ElementFactory() {
        this.provider = FontFactory.getFontImp();
    }
    
    public void setFontProvider(final FontProvider provider) {
        this.provider = provider;
    }
    
    public FontProvider getFontProvider() {
        return this.provider;
    }
    
    public Font getFont(final ChainedProperties chain) {
        String face = chain.getProperty("face");
        if (face == null || face.trim().length() == 0) {
            face = chain.getProperty("font-family");
        }
        if (face != null) {
            final StringTokenizer tok = new StringTokenizer(face, ",");
            while (tok.hasMoreTokens()) {
                face = tok.nextToken().trim();
                if (face.startsWith("\"")) {
                    face = face.substring(1);
                }
                if (face.endsWith("\"")) {
                    face = face.substring(0, face.length() - 1);
                }
                if (this.provider.isRegistered(face)) {
                    break;
                }
            }
        }
        String encoding = chain.getProperty("encoding");
        if (encoding == null) {
            encoding = "Cp1252";
        }
        final String value = chain.getProperty("size");
        float size = 12.0f;
        if (value != null) {
            size = Float.parseFloat(value);
        }
        int style = 0;
        final String decoration = chain.getProperty("text-decoration");
        if (decoration != null && decoration.trim().length() != 0) {
            if ("underline".equals(decoration)) {
                style |= 0x4;
            }
            else if ("line-through".equals(decoration)) {
                style |= 0x8;
            }
        }
        if (chain.hasProperty("i")) {
            style |= 0x2;
        }
        if (chain.hasProperty("b")) {
            style |= 0x1;
        }
        if (chain.hasProperty("u")) {
            style |= 0x4;
        }
        if (chain.hasProperty("s")) {
            style |= 0x8;
        }
        final BaseColor color = HtmlUtilities.decodeColor(chain.getProperty("color"));
        return this.provider.getFont(face, encoding, true, size, style, color);
    }
    
    public Chunk createChunk(final String content, final ChainedProperties chain) {
        final Font font = this.getFont(chain);
        final Chunk ck = new Chunk(content, font);
        if (chain.hasProperty("sub")) {
            ck.setTextRise(-font.getSize() / 2.0f);
        }
        else if (chain.hasProperty("sup")) {
            ck.setTextRise(font.getSize() / 2.0f);
        }
        ck.setHyphenation(this.getHyphenation(chain));
        return ck;
    }
    
    public Paragraph createParagraph(final ChainedProperties chain) {
        final Paragraph paragraph = new Paragraph();
        this.updateElement(paragraph, chain);
        return paragraph;
    }
    
    public ListItem createListItem(final ChainedProperties chain) {
        final ListItem item = new ListItem();
        this.updateElement(item, chain);
        return item;
    }
    
    protected void updateElement(final Paragraph paragraph, final ChainedProperties chain) {
        String value = chain.getProperty("align");
        paragraph.setAlignment(HtmlUtilities.alignmentValue(value));
        paragraph.setHyphenation(this.getHyphenation(chain));
        setParagraphLeading(paragraph, chain.getProperty("leading"));
        value = chain.getProperty("after");
        if (value != null) {
            try {
                paragraph.setSpacingBefore(Float.parseFloat(value));
            }
            catch (Exception ex) {}
        }
        value = chain.getProperty("after");
        if (value != null) {
            try {
                paragraph.setSpacingAfter(Float.parseFloat(value));
            }
            catch (Exception ex2) {}
        }
        value = chain.getProperty("extraparaspace");
        if (value != null) {
            try {
                paragraph.setExtraParagraphSpace(Float.parseFloat(value));
            }
            catch (Exception ex3) {}
        }
        value = chain.getProperty("indent");
        if (value != null) {
            try {
                paragraph.setIndentationLeft(Float.parseFloat(value));
            }
            catch (Exception ex4) {}
        }
    }
    
    protected static void setParagraphLeading(final Paragraph paragraph, final String leading) {
        if (leading == null) {
            paragraph.setLeading(0.0f, 1.5f);
            return;
        }
        try {
            final StringTokenizer tk = new StringTokenizer(leading, " ,");
            String v = tk.nextToken();
            final float v2 = Float.parseFloat(v);
            if (!tk.hasMoreTokens()) {
                paragraph.setLeading(v2, 0.0f);
                return;
            }
            v = tk.nextToken();
            final float v3 = Float.parseFloat(v);
            paragraph.setLeading(v2, v3);
        }
        catch (Exception e) {
            paragraph.setLeading(0.0f, 1.5f);
        }
    }
    
    public HyphenationEvent getHyphenation(final ChainedProperties chain) {
        String value = chain.getProperty("hyphenation");
        if (value == null || value.length() == 0) {
            return null;
        }
        int pos = value.indexOf(95);
        if (pos == -1) {
            return new HyphenationAuto(value, null, 2, 2);
        }
        final String lang = value.substring(0, pos);
        String country = value.substring(pos + 1);
        pos = country.indexOf(44);
        if (pos == -1) {
            return new HyphenationAuto(lang, country, 2, 2);
        }
        int rightMin = 2;
        value = country.substring(pos + 1);
        country = country.substring(0, pos);
        pos = value.indexOf(44);
        int leftMin;
        if (pos == -1) {
            leftMin = Integer.parseInt(value);
        }
        else {
            leftMin = Integer.parseInt(value.substring(0, pos));
            rightMin = Integer.parseInt(value.substring(pos + 1));
        }
        return new HyphenationAuto(lang, country, leftMin, rightMin);
    }
    
    public LineSeparator createLineSeparator(final Map<String, String> attrs, final float offset) {
        float lineWidth = 1.0f;
        final String size = attrs.get("size");
        if (size != null) {
            final float tmpSize = HtmlUtilities.parseLength(size, 12.0f);
            if (tmpSize > 0.0f) {
                lineWidth = tmpSize;
            }
        }
        final String width = attrs.get("width");
        float percentage = 100.0f;
        if (width != null) {
            final float tmpWidth = HtmlUtilities.parseLength(width, 12.0f);
            if (tmpWidth > 0.0f) {
                percentage = tmpWidth;
            }
            if (!width.endsWith("%")) {
                percentage = 100.0f;
            }
        }
        final BaseColor lineColor = null;
        final int align = HtmlUtilities.alignmentValue(attrs.get("align"));
        return new LineSeparator(lineWidth, percentage, lineColor, align, offset);
    }
    
    public Image createImage(String src, final Map<String, String> attrs, final ChainedProperties chain, final DocListener document, final ImageProvider img_provider, final HashMap<String, Image> img_store, final String img_baseurl) throws DocumentException, IOException {
        Image img = null;
        if (img_provider != null) {
            img = img_provider.getImage(src, attrs, chain, document);
        }
        if (img == null && img_store != null) {
            final Image tim = img_store.get(src);
            if (tim != null) {
                img = Image.getInstance(tim);
            }
        }
        if (img != null) {
            return img;
        }
        if (!src.startsWith("http") && img_baseurl != null) {
            src = img_baseurl + src;
        }
        else if (img == null && !src.startsWith("http")) {
            String path = chain.getProperty("image_path");
            if (path == null) {
                path = "";
            }
            src = new File(path, src).getPath();
        }
        img = Image.getInstance(src);
        if (img == null) {
            return null;
        }
        float actualFontSize = HtmlUtilities.parseLength(chain.getProperty("size"), 12.0f);
        if (actualFontSize <= 0.0f) {
            actualFontSize = 12.0f;
        }
        final String width = attrs.get("width");
        float widthInPoints = HtmlUtilities.parseLength(width, actualFontSize);
        final String height = attrs.get("height");
        float heightInPoints = HtmlUtilities.parseLength(height, actualFontSize);
        if (widthInPoints > 0.0f && heightInPoints > 0.0f) {
            img.scaleAbsolute(widthInPoints, heightInPoints);
        }
        else if (widthInPoints > 0.0f) {
            heightInPoints = img.getHeight() * widthInPoints / img.getWidth();
            img.scaleAbsolute(widthInPoints, heightInPoints);
        }
        else if (heightInPoints > 0.0f) {
            widthInPoints = img.getWidth() * heightInPoints / img.getHeight();
            img.scaleAbsolute(widthInPoints, heightInPoints);
        }
        final String before = chain.getProperty("before");
        if (before != null) {
            img.setSpacingBefore(Float.parseFloat(before));
        }
        final String after = chain.getProperty("after");
        if (after != null) {
            img.setSpacingAfter(Float.parseFloat(after));
        }
        img.setWidthPercentage(0.0f);
        return img;
    }
    
    public List createList(final String tag, final ChainedProperties chain) {
        List list;
        if ("ul".equalsIgnoreCase(tag)) {
            list = new List(false);
            list.setListSymbol("\u2022 ");
        }
        else {
            list = new List(true);
        }
        try {
            list.setIndentationLeft(new Float(chain.getProperty("indent")));
        }
        catch (Exception e) {
            list.setAutoindent(true);
        }
        return list;
    }
}
