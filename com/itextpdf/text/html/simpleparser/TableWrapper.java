// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html.simpleparser;

import com.itextpdf.text.ElementListener;
import com.itextpdf.text.Chunk;
import java.util.Iterator;
import com.itextpdf.text.html.HtmlUtilities;
import com.itextpdf.text.pdf.PdfPTable;
import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import com.itextpdf.text.pdf.PdfPCell;
import java.util.List;
import java.util.Map;
import com.itextpdf.text.Element;

@Deprecated
public class TableWrapper implements Element
{
    private final Map<String, String> styles;
    private final List<List<PdfPCell>> rows;
    private float[] colWidths;
    
    public TableWrapper(final Map<String, String> attrs) {
        this.styles = new HashMap<String, String>();
        this.rows = new ArrayList<List<PdfPCell>>();
        this.styles.putAll(attrs);
    }
    
    public void addRow(List<PdfPCell> row) {
        if (row != null) {
            Collections.reverse(row);
            this.rows.add(row);
            row = null;
        }
    }
    
    public void setColWidths(final float[] colWidths) {
        this.colWidths = colWidths;
    }
    
    public PdfPTable createTable() {
        if (this.rows.isEmpty()) {
            return new PdfPTable(1);
        }
        int ncol = 0;
        for (final PdfPCell pc : this.rows.get(0)) {
            ncol += pc.getColspan();
        }
        final PdfPTable table = new PdfPTable(ncol);
        final String width = this.styles.get("width");
        if (width == null) {
            table.setWidthPercentage(100.0f);
        }
        else if (width.endsWith("%")) {
            table.setWidthPercentage(Float.parseFloat(width.substring(0, width.length() - 1)));
        }
        else {
            table.setTotalWidth(Float.parseFloat(width));
            table.setLockedWidth(true);
        }
        final String alignment = this.styles.get("align");
        int align = 0;
        if (alignment != null) {
            align = HtmlUtilities.alignmentValue(alignment);
        }
        table.setHorizontalAlignment(align);
        try {
            if (this.colWidths != null) {
                table.setWidths(this.colWidths);
            }
        }
        catch (Exception ex) {}
        for (final List<PdfPCell> col : this.rows) {
            for (final PdfPCell pc2 : col) {
                table.addCell(pc2);
            }
        }
        return table;
    }
    
    @Override
    public List<Chunk> getChunks() {
        return null;
    }
    
    @Override
    public boolean isContent() {
        return false;
    }
    
    @Override
    public boolean isNestable() {
        return false;
    }
    
    @Override
    public boolean process(final ElementListener listener) {
        return false;
    }
    
    @Override
    public int type() {
        return 0;
    }
}
