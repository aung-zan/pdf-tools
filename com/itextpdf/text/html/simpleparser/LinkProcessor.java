// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html.simpleparser;

import com.itextpdf.text.Paragraph;

@Deprecated
public interface LinkProcessor
{
    boolean process(final Paragraph p0, final ChainedProperties p1);
}
