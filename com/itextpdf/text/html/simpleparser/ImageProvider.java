// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.html.simpleparser;

import com.itextpdf.text.Image;
import com.itextpdf.text.DocListener;
import java.util.Map;

@Deprecated
public interface ImageProvider
{
    Image getImage(final String p0, final Map<String, String> p1, final ChainedProperties p2, final DocListener p3);
}
