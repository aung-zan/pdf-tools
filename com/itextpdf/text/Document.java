// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import java.util.Date;
import java.text.SimpleDateFormat;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.util.Iterator;
import com.itextpdf.text.pdf.PdfObject;
import java.util.HashMap;
import com.itextpdf.text.pdf.PdfName;
import java.util.ArrayList;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;

public class Document implements DocListener, IAccessibleElement
{
    public static boolean compress;
    public static boolean plainRandomAccess;
    public static float wmfFontCorrection;
    protected ArrayList<DocListener> listeners;
    protected boolean open;
    protected boolean close;
    protected Rectangle pageSize;
    protected float marginLeft;
    protected float marginRight;
    protected float marginTop;
    protected float marginBottom;
    protected boolean marginMirroring;
    protected boolean marginMirroringTopBottom;
    protected String javaScript_onLoad;
    protected String javaScript_onUnLoad;
    protected String htmlStyleClass;
    protected int pageN;
    protected int chapternumber;
    protected PdfName role;
    protected HashMap<PdfName, PdfObject> accessibleAttributes;
    protected AccessibleElementId id;
    
    public Document() {
        this(PageSize.A4);
    }
    
    public Document(final Rectangle pageSize) {
        this(pageSize, 36.0f, 36.0f, 36.0f, 36.0f);
    }
    
    public Document(final Rectangle pageSize, final float marginLeft, final float marginRight, final float marginTop, final float marginBottom) {
        this.listeners = new ArrayList<DocListener>();
        this.marginLeft = 0.0f;
        this.marginRight = 0.0f;
        this.marginTop = 0.0f;
        this.marginBottom = 0.0f;
        this.marginMirroring = false;
        this.marginMirroringTopBottom = false;
        this.javaScript_onLoad = null;
        this.javaScript_onUnLoad = null;
        this.htmlStyleClass = null;
        this.pageN = 0;
        this.chapternumber = 0;
        this.role = PdfName.DOCUMENT;
        this.accessibleAttributes = null;
        this.id = new AccessibleElementId();
        this.pageSize = pageSize;
        this.marginLeft = marginLeft;
        this.marginRight = marginRight;
        this.marginTop = marginTop;
        this.marginBottom = marginBottom;
    }
    
    public void addDocListener(final DocListener listener) {
        this.listeners.add(listener);
        if (listener instanceof IAccessibleElement) {
            final IAccessibleElement ae = (IAccessibleElement)listener;
            ae.setRole(this.role);
            ae.setId(this.id);
            if (this.accessibleAttributes != null) {
                for (final PdfName key : this.accessibleAttributes.keySet()) {
                    ae.setAccessibleAttribute(key, this.accessibleAttributes.get(key));
                }
            }
        }
    }
    
    public void removeDocListener(final DocListener listener) {
        this.listeners.remove(listener);
    }
    
    @Override
    public boolean add(final Element element) throws DocumentException {
        if (this.close) {
            throw new DocumentException(MessageLocalization.getComposedMessage("the.document.has.been.closed.you.can.t.add.any.elements", new Object[0]));
        }
        if (!this.open && element.isContent()) {
            throw new DocumentException(MessageLocalization.getComposedMessage("the.document.is.not.open.yet.you.can.only.add.meta.information", new Object[0]));
        }
        boolean success = false;
        if (element instanceof ChapterAutoNumber) {
            this.chapternumber = ((ChapterAutoNumber)element).setAutomaticNumber(this.chapternumber);
        }
        for (final DocListener listener : this.listeners) {
            success |= listener.add(element);
        }
        if (element instanceof LargeElement) {
            final LargeElement e = (LargeElement)element;
            if (!e.isComplete()) {
                e.flushContent();
            }
        }
        return success;
    }
    
    @Override
    public void open() {
        if (!this.close) {
            this.open = true;
        }
        for (final DocListener listener : this.listeners) {
            listener.setPageSize(this.pageSize);
            listener.setMargins(this.marginLeft, this.marginRight, this.marginTop, this.marginBottom);
            listener.open();
        }
    }
    
    @Override
    public boolean setPageSize(final Rectangle pageSize) {
        this.pageSize = pageSize;
        for (final DocListener listener : this.listeners) {
            listener.setPageSize(pageSize);
        }
        return true;
    }
    
    @Override
    public boolean setMargins(final float marginLeft, final float marginRight, final float marginTop, final float marginBottom) {
        this.marginLeft = marginLeft;
        this.marginRight = marginRight;
        this.marginTop = marginTop;
        this.marginBottom = marginBottom;
        for (final DocListener listener : this.listeners) {
            listener.setMargins(marginLeft, marginRight, marginTop, marginBottom);
        }
        return true;
    }
    
    @Override
    public boolean newPage() {
        if (!this.open || this.close) {
            return false;
        }
        for (final DocListener listener : this.listeners) {
            listener.newPage();
        }
        return true;
    }
    
    @Override
    public void resetPageCount() {
        this.pageN = 0;
        for (final DocListener listener : this.listeners) {
            listener.resetPageCount();
        }
    }
    
    @Override
    public void setPageCount(final int pageN) {
        this.pageN = pageN;
        for (final DocListener listener : this.listeners) {
            listener.setPageCount(pageN);
        }
    }
    
    public int getPageNumber() {
        return this.pageN;
    }
    
    @Override
    public void close() {
        if (!this.close) {
            this.open = false;
            this.close = true;
        }
        for (final DocListener listener : this.listeners) {
            listener.close();
        }
    }
    
    public boolean addHeader(final String name, final String content) {
        try {
            return this.add(new Header(name, content));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    public boolean addTitle(final String title) {
        try {
            return this.add(new Meta(1, title));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    public boolean addSubject(final String subject) {
        try {
            return this.add(new Meta(2, subject));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    public boolean addKeywords(final String keywords) {
        try {
            return this.add(new Meta(3, keywords));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    public boolean addAuthor(final String author) {
        try {
            return this.add(new Meta(4, author));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    public boolean addCreator(final String creator) {
        try {
            return this.add(new Meta(7, creator));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    public boolean addProducer() {
        try {
            return this.add(new Meta(5, Version.getInstance().getVersion()));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    public boolean addLanguage(final String language) {
        try {
            return this.add(new Meta(8, language));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    public boolean addCreationDate() {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
            return this.add(new Meta(6, sdf.format(new Date())));
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
    
    public float leftMargin() {
        return this.marginLeft;
    }
    
    public float rightMargin() {
        return this.marginRight;
    }
    
    public float topMargin() {
        return this.marginTop;
    }
    
    public float bottomMargin() {
        return this.marginBottom;
    }
    
    public float left() {
        return this.pageSize.getLeft(this.marginLeft);
    }
    
    public float right() {
        return this.pageSize.getRight(this.marginRight);
    }
    
    public float top() {
        return this.pageSize.getTop(this.marginTop);
    }
    
    public float bottom() {
        return this.pageSize.getBottom(this.marginBottom);
    }
    
    public float left(final float margin) {
        return this.pageSize.getLeft(this.marginLeft + margin);
    }
    
    public float right(final float margin) {
        return this.pageSize.getRight(this.marginRight + margin);
    }
    
    public float top(final float margin) {
        return this.pageSize.getTop(this.marginTop + margin);
    }
    
    public float bottom(final float margin) {
        return this.pageSize.getBottom(this.marginBottom + margin);
    }
    
    public Rectangle getPageSize() {
        return this.pageSize;
    }
    
    public boolean isOpen() {
        return this.open;
    }
    
    public void setJavaScript_onLoad(final String code) {
        this.javaScript_onLoad = code;
    }
    
    public String getJavaScript_onLoad() {
        return this.javaScript_onLoad;
    }
    
    public void setJavaScript_onUnLoad(final String code) {
        this.javaScript_onUnLoad = code;
    }
    
    public String getJavaScript_onUnLoad() {
        return this.javaScript_onUnLoad;
    }
    
    public void setHtmlStyleClass(final String htmlStyleClass) {
        this.htmlStyleClass = htmlStyleClass;
    }
    
    public String getHtmlStyleClass() {
        return this.htmlStyleClass;
    }
    
    @Override
    public boolean setMarginMirroring(final boolean marginMirroring) {
        this.marginMirroring = marginMirroring;
        for (final Object element : this.listeners) {
            final DocListener listener = (DocListener)element;
            listener.setMarginMirroring(marginMirroring);
        }
        return true;
    }
    
    @Override
    public boolean setMarginMirroringTopBottom(final boolean marginMirroringTopBottom) {
        this.marginMirroringTopBottom = marginMirroringTopBottom;
        for (final Object element : this.listeners) {
            final DocListener listener = (DocListener)element;
            listener.setMarginMirroringTopBottom(marginMirroringTopBottom);
        }
        return true;
    }
    
    public boolean isMarginMirroring() {
        return this.marginMirroring;
    }
    
    @Override
    public PdfObject getAccessibleAttribute(final PdfName key) {
        if (this.accessibleAttributes != null) {
            return this.accessibleAttributes.get(key);
        }
        return null;
    }
    
    @Override
    public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
        if (this.accessibleAttributes == null) {
            this.accessibleAttributes = new HashMap<PdfName, PdfObject>();
        }
        this.accessibleAttributes.put(key, value);
    }
    
    @Override
    public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
        return this.accessibleAttributes;
    }
    
    @Override
    public PdfName getRole() {
        return this.role;
    }
    
    @Override
    public void setRole(final PdfName role) {
        this.role = role;
    }
    
    @Override
    public AccessibleElementId getId() {
        return this.id;
    }
    
    @Override
    public void setId(final AccessibleElementId id) {
        this.id = id;
    }
    
    @Override
    public boolean isInline() {
        return false;
    }
    
    static {
        Document.compress = true;
        Document.plainRandomAccess = false;
        Document.wmfFontCorrection = 0.86f;
    }
}
