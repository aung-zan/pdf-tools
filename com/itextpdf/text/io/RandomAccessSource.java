// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.io.IOException;

public interface RandomAccessSource
{
    int get(final long p0) throws IOException;
    
    int get(final long p0, final byte[] p1, final int p2, final int p3) throws IOException;
    
    long length();
    
    void close() throws IOException;
}
