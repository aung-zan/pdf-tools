// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.io.IOException;
import java.io.RandomAccessFile;

class RAFRandomAccessSource implements RandomAccessSource
{
    private final RandomAccessFile raf;
    private final long length;
    
    public RAFRandomAccessSource(final RandomAccessFile raf) throws IOException {
        this.raf = raf;
        this.length = raf.length();
    }
    
    @Override
    public int get(final long position) throws IOException {
        if (position > this.raf.length()) {
            return -1;
        }
        this.raf.seek(position);
        return this.raf.read();
    }
    
    @Override
    public int get(final long position, final byte[] bytes, final int off, final int len) throws IOException {
        if (position > this.length) {
            return -1;
        }
        this.raf.seek(position);
        return this.raf.read(bytes, off, len);
    }
    
    @Override
    public long length() {
        return this.length;
    }
    
    @Override
    public void close() throws IOException {
        this.raf.close();
    }
}
