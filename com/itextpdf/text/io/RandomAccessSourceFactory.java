// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import com.itextpdf.text.error_messages.MessageLocalization;
import java.nio.channels.FileChannel;
import java.io.FileInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.io.IOException;
import java.io.RandomAccessFile;

public final class RandomAccessSourceFactory
{
    private boolean forceRead;
    private boolean usePlainRandomAccess;
    private boolean exclusivelyLockFile;
    
    public RandomAccessSourceFactory() {
        this.forceRead = false;
        this.usePlainRandomAccess = false;
        this.exclusivelyLockFile = false;
    }
    
    public RandomAccessSourceFactory setForceRead(final boolean forceRead) {
        this.forceRead = forceRead;
        return this;
    }
    
    public RandomAccessSourceFactory setUsePlainRandomAccess(final boolean usePlainRandomAccess) {
        this.usePlainRandomAccess = usePlainRandomAccess;
        return this;
    }
    
    public RandomAccessSourceFactory setExclusivelyLockFile(final boolean exclusivelyLockFile) {
        this.exclusivelyLockFile = exclusivelyLockFile;
        return this;
    }
    
    public RandomAccessSource createSource(final byte[] data) {
        return new ArrayRandomAccessSource(data);
    }
    
    public RandomAccessSource createSource(final RandomAccessFile raf) throws IOException {
        return new RAFRandomAccessSource(raf);
    }
    
    public RandomAccessSource createSource(final URL url) throws IOException {
        final InputStream is = url.openStream();
        try {
            return this.createSource(is);
        }
        finally {
            try {
                is.close();
            }
            catch (IOException ex) {}
        }
    }
    
    public RandomAccessSource createSource(final InputStream is) throws IOException {
        try {
            return this.createSource(StreamUtil.inputStreamToArray(is));
        }
        finally {
            try {
                is.close();
            }
            catch (IOException ex) {}
        }
    }
    
    public RandomAccessSource createBestSource(final String filename) throws IOException {
        final File file = new File(filename);
        if (!file.canRead()) {
            if (filename.startsWith("file:/") || filename.startsWith("http://") || filename.startsWith("https://") || filename.startsWith("jar:") || filename.startsWith("wsjar:") || filename.startsWith("wsjar:") || filename.startsWith("vfszip:")) {
                return this.createSource(new URL(filename));
            }
            return this.createByReadingToMemory(filename);
        }
        else {
            if (this.forceRead) {
                return this.createByReadingToMemory(new FileInputStream(filename));
            }
            final String openMode = this.exclusivelyLockFile ? "rw" : "r";
            final RandomAccessFile raf = new RandomAccessFile(file, openMode);
            if (this.exclusivelyLockFile) {
                raf.getChannel().lock();
            }
            try {
                return this.createBestSource(raf);
            }
            catch (IOException e) {
                try {
                    raf.close();
                }
                catch (IOException ex) {}
                throw e;
            }
            catch (RuntimeException e2) {
                try {
                    raf.close();
                }
                catch (IOException ex2) {}
                throw e2;
            }
        }
    }
    
    public RandomAccessSource createBestSource(final RandomAccessFile raf) throws IOException {
        if (this.usePlainRandomAccess) {
            return new RAFRandomAccessSource(raf);
        }
        if (raf.length() <= 0L) {
            return new RAFRandomAccessSource(raf);
        }
        try {
            return this.createBestSource(raf.getChannel());
        }
        catch (MapFailedException e) {
            return new RAFRandomAccessSource(raf);
        }
    }
    
    public RandomAccessSource createBestSource(final FileChannel channel) throws IOException {
        if (channel.size() <= 67108864L) {
            return new GetBufferedRandomAccessSource(new FileChannelRandomAccessSource(channel));
        }
        return new GetBufferedRandomAccessSource(new PagedChannelRandomAccessSource(channel));
    }
    
    public RandomAccessSource createRanged(final RandomAccessSource source, final long[] ranges) throws IOException {
        final RandomAccessSource[] sources = new RandomAccessSource[ranges.length / 2];
        for (int i = 0; i < ranges.length; i += 2) {
            sources[i / 2] = new WindowRandomAccessSource(source, ranges[i], ranges[i + 1]);
        }
        return new GroupedRandomAccessSource(sources);
    }
    
    private RandomAccessSource createByReadingToMemory(final String filename) throws IOException {
        final InputStream is = StreamUtil.getResourceStream(filename);
        if (is == null) {
            throw new IOException(MessageLocalization.getComposedMessage("1.not.found.as.file.or.resource", filename));
        }
        return this.createByReadingToMemory(is);
    }
    
    private RandomAccessSource createByReadingToMemory(final InputStream is) throws IOException {
        try {
            return new ArrayRandomAccessSource(StreamUtil.inputStreamToArray(is));
        }
        finally {
            try {
                is.close();
            }
            catch (IOException ex) {}
        }
    }
}
