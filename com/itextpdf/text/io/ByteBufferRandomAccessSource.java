// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.security.AccessController;
import java.lang.reflect.Method;
import java.security.PrivilegedAction;
import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

class ByteBufferRandomAccessSource implements RandomAccessSource
{
    private final ByteBuffer byteBuffer;
    
    public ByteBufferRandomAccessSource(final ByteBuffer byteBuffer) {
        this.byteBuffer = byteBuffer;
    }
    
    @Override
    public int get(final long position) throws IOException {
        if (position > 2147483647L) {
            throw new IllegalArgumentException("Position must be less than Integer.MAX_VALUE");
        }
        try {
            if (position >= this.byteBuffer.limit()) {
                return -1;
            }
            final byte b = this.byteBuffer.get((int)position);
            final int n = b & 0xFF;
            return n;
        }
        catch (BufferUnderflowException e) {
            return -1;
        }
    }
    
    @Override
    public int get(final long position, final byte[] bytes, final int off, final int len) throws IOException {
        if (position > 2147483647L) {
            throw new IllegalArgumentException("Position must be less than Integer.MAX_VALUE");
        }
        if (position >= this.byteBuffer.limit()) {
            return -1;
        }
        this.byteBuffer.position((int)position);
        final int bytesFromThisBuffer = Math.min(len, this.byteBuffer.remaining());
        this.byteBuffer.get(bytes, off, bytesFromThisBuffer);
        return bytesFromThisBuffer;
    }
    
    @Override
    public long length() {
        return this.byteBuffer.limit();
    }
    
    @Override
    public void close() throws IOException {
        clean(this.byteBuffer);
    }
    
    private static boolean clean(final ByteBuffer buffer) {
        if (buffer == null || !buffer.isDirect()) {
            return false;
        }
        final Boolean b = AccessController.doPrivileged((PrivilegedAction<Boolean>)new PrivilegedAction<Boolean>() {
            @Override
            public Boolean run() {
                Boolean success = Boolean.FALSE;
                try {
                    final Method getCleanerMethod = buffer.getClass().getMethod("cleaner", (Class<?>[])null);
                    getCleanerMethod.setAccessible(true);
                    final Object cleaner = getCleanerMethod.invoke(buffer, (Object[])null);
                    final Method clean = cleaner.getClass().getMethod("clean", (Class<?>[])null);
                    clean.invoke(cleaner, (Object[])null);
                    success = Boolean.TRUE;
                }
                catch (Exception ex) {}
                return success;
            }
        });
        return b;
    }
}
