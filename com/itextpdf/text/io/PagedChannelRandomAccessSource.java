// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.util.Iterator;
import java.util.LinkedList;
import java.io.IOException;
import java.nio.channels.FileChannel;

class PagedChannelRandomAccessSource extends GroupedRandomAccessSource implements RandomAccessSource
{
    public static final int DEFAULT_TOTAL_BUFSIZE = 67108864;
    public static final int DEFAULT_MAX_OPEN_BUFFERS = 16;
    private final int bufferSize;
    private final FileChannel channel;
    private final MRU<RandomAccessSource> mru;
    
    public PagedChannelRandomAccessSource(final FileChannel channel) throws IOException {
        this(channel, 67108864, 16);
    }
    
    public PagedChannelRandomAccessSource(final FileChannel channel, final int totalBufferSize, final int maxOpenBuffers) throws IOException {
        super(buildSources(channel, totalBufferSize / maxOpenBuffers));
        this.channel = channel;
        this.bufferSize = totalBufferSize / maxOpenBuffers;
        this.mru = new MRU<RandomAccessSource>(maxOpenBuffers);
    }
    
    private static RandomAccessSource[] buildSources(final FileChannel channel, final int bufferSize) throws IOException {
        final long size = channel.size();
        if (size <= 0L) {
            throw new IOException("File size must be greater than zero");
        }
        final int bufferCount = (int)(size / bufferSize) + ((size % bufferSize != 0L) ? 1 : 0);
        final MappedChannelRandomAccessSource[] sources = new MappedChannelRandomAccessSource[bufferCount];
        for (int i = 0; i < bufferCount; ++i) {
            final long pageOffset = i * (long)bufferSize;
            final long pageLength = Math.min(size - pageOffset, bufferSize);
            sources[i] = new MappedChannelRandomAccessSource(channel, pageOffset, pageLength);
        }
        return sources;
    }
    
    @Override
    protected int getStartingSourceIndex(final long offset) {
        return (int)(offset / this.bufferSize);
    }
    
    @Override
    protected void sourceReleased(final RandomAccessSource source) throws IOException {
        final RandomAccessSource old = this.mru.enqueue(source);
        if (old != null) {
            old.close();
        }
    }
    
    @Override
    protected void sourceInUse(final RandomAccessSource source) throws IOException {
        ((MappedChannelRandomAccessSource)source).open();
    }
    
    @Override
    public void close() throws IOException {
        super.close();
        this.channel.close();
    }
    
    private static class MRU<E>
    {
        private final int limit;
        private LinkedList<E> queue;
        
        public MRU(final int limit) {
            this.queue = new LinkedList<E>();
            this.limit = limit;
        }
        
        public E enqueue(final E newElement) {
            if (this.queue.size() > 0 && this.queue.getFirst() == newElement) {
                return null;
            }
            final Iterator<E> it = this.queue.iterator();
            while (it.hasNext()) {
                final E element = it.next();
                if (newElement == element) {
                    it.remove();
                    this.queue.addFirst(newElement);
                    return null;
                }
            }
            this.queue.addFirst(newElement);
            if (this.queue.size() > this.limit) {
                return this.queue.removeLast();
            }
            return null;
        }
    }
}
