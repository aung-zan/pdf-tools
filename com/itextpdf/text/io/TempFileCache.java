// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.ObjectOutputStream;
import com.itextpdf.text.pdf.PdfObject;
import java.io.IOException;
import java.io.File;
import java.io.ByteArrayOutputStream;
import java.io.RandomAccessFile;

public class TempFileCache
{
    private String filename;
    private RandomAccessFile cache;
    private ByteArrayOutputStream baos;
    private byte[] buf;
    
    public TempFileCache(final String filename) throws IOException {
        this.filename = filename;
        final File f = new File(filename);
        final File parent = f.getParentFile();
        if (parent != null) {
            parent.mkdirs();
        }
        this.cache = new RandomAccessFile(filename, "rw");
        this.baos = new ByteArrayOutputStream();
    }
    
    public ObjectPosition put(final PdfObject obj) throws IOException {
        this.baos.reset();
        final ObjectOutputStream oos = new ObjectOutputStream(this.baos);
        final long offset = this.cache.length();
        oos.writeObject(obj);
        this.cache.seek(offset);
        this.cache.write(this.baos.toByteArray());
        final long size = this.cache.length() - offset;
        return new ObjectPosition(offset, (int)size);
    }
    
    public PdfObject get(final ObjectPosition pos) throws IOException, ClassNotFoundException {
        PdfObject obj = null;
        if (pos != null) {
            this.cache.seek(pos.offset);
            this.cache.read(this.getBuffer(pos.length), 0, pos.length);
            final ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(this.getBuffer(pos.length)));
            try {
                obj = (PdfObject)ois.readObject();
            }
            finally {
                ois.close();
            }
        }
        return obj;
    }
    
    private byte[] getBuffer(final int size) {
        if (this.buf == null || this.buf.length < size) {
            this.buf = new byte[size];
        }
        return this.buf;
    }
    
    public void close() throws IOException {
        this.cache.close();
        this.cache = null;
        new File(this.filename).delete();
    }
    
    public class ObjectPosition
    {
        long offset;
        int length;
        
        ObjectPosition(final long offset, final int length) {
            this.offset = offset;
            this.length = length;
        }
    }
}
