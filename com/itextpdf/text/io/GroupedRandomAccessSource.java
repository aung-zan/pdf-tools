// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.io.IOException;

class GroupedRandomAccessSource implements RandomAccessSource
{
    private final SourceEntry[] sources;
    private SourceEntry currentSourceEntry;
    private final long size;
    
    public GroupedRandomAccessSource(final RandomAccessSource[] sources) throws IOException {
        this.sources = new SourceEntry[sources.length];
        long totalSize = 0L;
        for (int i = 0; i < sources.length; ++i) {
            this.sources[i] = new SourceEntry(i, sources[i], totalSize);
            totalSize += sources[i].length();
        }
        this.size = totalSize;
        this.currentSourceEntry = this.sources[sources.length - 1];
        this.sourceInUse(this.currentSourceEntry.source);
    }
    
    protected int getStartingSourceIndex(final long offset) {
        if (offset >= this.currentSourceEntry.firstByte) {
            return this.currentSourceEntry.index;
        }
        return 0;
    }
    
    private SourceEntry getSourceEntryForOffset(final long offset) throws IOException {
        if (offset >= this.size) {
            return null;
        }
        if (offset >= this.currentSourceEntry.firstByte && offset <= this.currentSourceEntry.lastByte) {
            return this.currentSourceEntry;
        }
        this.sourceReleased(this.currentSourceEntry.source);
        int i;
        for (int startAt = i = this.getStartingSourceIndex(offset); i < this.sources.length; ++i) {
            if (offset >= this.sources[i].firstByte && offset <= this.sources[i].lastByte) {
                this.currentSourceEntry = this.sources[i];
                this.sourceInUse(this.currentSourceEntry.source);
                return this.currentSourceEntry;
            }
        }
        return null;
    }
    
    protected void sourceReleased(final RandomAccessSource source) throws IOException {
    }
    
    protected void sourceInUse(final RandomAccessSource source) throws IOException {
    }
    
    @Override
    public int get(final long position) throws IOException {
        final SourceEntry entry = this.getSourceEntryForOffset(position);
        if (entry == null) {
            return -1;
        }
        return entry.source.get(entry.offsetN(position));
    }
    
    @Override
    public int get(long position, final byte[] bytes, int off, final int len) throws IOException {
        SourceEntry entry = this.getSourceEntryForOffset(position);
        if (entry == null) {
            return -1;
        }
        long offN = entry.offsetN(position);
        int remaining;
        int count;
        for (remaining = len; remaining > 0; remaining -= count, offN = 0L, entry = this.getSourceEntryForOffset(position)) {
            if (entry == null) {
                break;
            }
            if (offN > entry.source.length()) {
                break;
            }
            count = entry.source.get(offN, bytes, off, remaining);
            if (count == -1) {
                break;
            }
            off += count;
            position += count;
        }
        return (remaining == len) ? -1 : (len - remaining);
    }
    
    @Override
    public long length() {
        return this.size;
    }
    
    @Override
    public void close() throws IOException {
        for (final SourceEntry entry : this.sources) {
            entry.source.close();
        }
    }
    
    private static class SourceEntry
    {
        final RandomAccessSource source;
        final long firstByte;
        final long lastByte;
        final int index;
        
        public SourceEntry(final int index, final RandomAccessSource source, final long offset) {
            this.index = index;
            this.source = source;
            this.firstByte = offset;
            this.lastByte = offset + source.length() - 1L;
        }
        
        public long offsetN(final long absoluteOffset) {
            return absoluteOffset - this.firstByte;
        }
    }
}
