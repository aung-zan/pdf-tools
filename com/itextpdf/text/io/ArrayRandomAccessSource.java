// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.io.IOException;

class ArrayRandomAccessSource implements RandomAccessSource
{
    private byte[] array;
    
    public ArrayRandomAccessSource(final byte[] array) {
        if (array == null) {
            throw new NullPointerException();
        }
        this.array = array;
    }
    
    @Override
    public int get(final long offset) {
        if (offset >= this.array.length) {
            return -1;
        }
        return 0xFF & this.array[(int)offset];
    }
    
    @Override
    public int get(final long offset, final byte[] bytes, final int off, int len) {
        if (this.array == null) {
            throw new IllegalStateException("Already closed");
        }
        if (offset >= this.array.length) {
            return -1;
        }
        if (offset + len > this.array.length) {
            len = (int)(this.array.length - offset);
        }
        System.arraycopy(this.array, (int)offset, bytes, off, len);
        return len;
    }
    
    @Override
    public long length() {
        return this.array.length;
    }
    
    @Override
    public void close() throws IOException {
        this.array = null;
    }
}
