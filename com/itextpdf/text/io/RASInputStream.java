// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.io.IOException;
import java.io.InputStream;

public class RASInputStream extends InputStream
{
    private final RandomAccessSource source;
    private long position;
    
    public RASInputStream(final RandomAccessSource source) {
        this.position = 0L;
        this.source = source;
    }
    
    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        final int count = this.source.get(this.position, b, off, len);
        this.position += count;
        return count;
    }
    
    @Override
    public int read() throws IOException {
        return this.source.get(this.position++);
    }
}
