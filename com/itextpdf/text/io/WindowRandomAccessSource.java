// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.io.IOException;

public class WindowRandomAccessSource implements RandomAccessSource
{
    private final RandomAccessSource source;
    private final long offset;
    private final long length;
    
    public WindowRandomAccessSource(final RandomAccessSource source, final long offset) {
        this(source, offset, source.length() - offset);
    }
    
    public WindowRandomAccessSource(final RandomAccessSource source, final long offset, final long length) {
        this.source = source;
        this.offset = offset;
        this.length = length;
    }
    
    @Override
    public int get(final long position) throws IOException {
        if (position >= this.length) {
            return -1;
        }
        return this.source.get(this.offset + position);
    }
    
    @Override
    public int get(final long position, final byte[] bytes, final int off, final int len) throws IOException {
        if (position >= this.length) {
            return -1;
        }
        final long toRead = Math.min(len, this.length - position);
        return this.source.get(this.offset + position, bytes, off, (int)toRead);
    }
    
    @Override
    public long length() {
        return this.length;
    }
    
    @Override
    public void close() throws IOException {
        this.source.close();
    }
}
