// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.io.IOException;

public class MapFailedException extends IOException
{
    public MapFailedException(final IOException e) {
        super(e.getMessage());
        this.initCause(e);
    }
}
