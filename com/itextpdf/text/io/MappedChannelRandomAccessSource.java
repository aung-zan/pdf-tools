// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

class MappedChannelRandomAccessSource implements RandomAccessSource
{
    private final FileChannel channel;
    private final long offset;
    private final long length;
    private ByteBufferRandomAccessSource source;
    
    public MappedChannelRandomAccessSource(final FileChannel channel, final long offset, final long length) {
        if (offset < 0L) {
            throw new IllegalArgumentException(offset + " is negative");
        }
        if (length <= 0L) {
            throw new IllegalArgumentException(length + " is zero or negative");
        }
        this.channel = channel;
        this.offset = offset;
        this.length = length;
        this.source = null;
    }
    
    void open() throws IOException {
        if (this.source != null) {
            return;
        }
        if (!this.channel.isOpen()) {
            throw new IllegalStateException("Channel is closed");
        }
        try {
            this.source = new ByteBufferRandomAccessSource(this.channel.map(FileChannel.MapMode.READ_ONLY, this.offset, this.length));
        }
        catch (IOException e) {
            if (exceptionIsMapFailureException(e)) {
                throw new MapFailedException(e);
            }
            throw e;
        }
    }
    
    private static boolean exceptionIsMapFailureException(final IOException e) {
        return e.getMessage() != null && e.getMessage().indexOf("Map failed") >= 0;
    }
    
    @Override
    public int get(final long position) throws IOException {
        if (this.source == null) {
            throw new IOException("RandomAccessSource not opened");
        }
        return this.source.get(position);
    }
    
    @Override
    public int get(final long position, final byte[] bytes, final int off, final int len) throws IOException {
        if (this.source == null) {
            throw new IOException("RandomAccessSource not opened");
        }
        return this.source.get(position, bytes, off, len);
    }
    
    @Override
    public long length() {
        return this.length;
    }
    
    @Override
    public void close() throws IOException {
        if (this.source == null) {
            return;
        }
        this.source.close();
        this.source = null;
    }
    
    @Override
    public String toString() {
        return this.getClass().getName() + " (" + this.offset + ", " + this.length + ")";
    }
}
