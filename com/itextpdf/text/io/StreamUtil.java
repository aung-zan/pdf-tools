// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.io.EOFException;
import java.io.OutputStream;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public final class StreamUtil
{
    private StreamUtil() {
    }
    
    public static byte[] inputStreamToArray(final InputStream is) throws IOException {
        final byte[] b = new byte[8192];
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        while (true) {
            final int read = is.read(b);
            if (read < 1) {
                break;
            }
            out.write(b, 0, read);
        }
        out.close();
        return out.toByteArray();
    }
    
    public static void CopyBytes(final RandomAccessSource source, final long start, long length, final OutputStream outs) throws IOException {
        if (length <= 0L) {
            return;
        }
        long idx = start;
        final byte[] buf = new byte[8192];
        while (length > 0L) {
            final long n = source.get(idx, buf, 0, (int)Math.min(buf.length, length));
            if (n <= 0L) {
                throw new EOFException();
            }
            outs.write(buf, 0, (int)n);
            idx += n;
            length -= n;
        }
    }
    
    public static InputStream getResourceStream(final String key) {
        return getResourceStream(key, null);
    }
    
    public static InputStream getResourceStream(String key, final ClassLoader loader) {
        if (key.startsWith("/")) {
            key = key.substring(1);
        }
        InputStream is = null;
        if (loader != null) {
            is = loader.getResourceAsStream(key);
            if (is != null) {
                return is;
            }
        }
        try {
            final ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            if (contextClassLoader != null) {
                is = contextClassLoader.getResourceAsStream(key);
            }
        }
        catch (Throwable t) {}
        if (is == null) {
            is = StreamUtil.class.getResourceAsStream("/" + key);
        }
        if (is == null) {
            is = ClassLoader.getSystemResourceAsStream(key);
        }
        return is;
    }
}
