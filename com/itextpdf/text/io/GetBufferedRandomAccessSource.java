// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.io.IOException;

public class GetBufferedRandomAccessSource implements RandomAccessSource
{
    private final RandomAccessSource source;
    private final byte[] getBuffer;
    private long getBufferStart;
    private long getBufferEnd;
    
    public GetBufferedRandomAccessSource(final RandomAccessSource source) {
        this.getBufferStart = -1L;
        this.getBufferEnd = -1L;
        this.source = source;
        this.getBuffer = new byte[(int)Math.min(Math.max(source.length() / 4L, 1L), 4096L)];
        this.getBufferStart = -1L;
        this.getBufferEnd = -1L;
    }
    
    @Override
    public int get(final long position) throws IOException {
        if (position < this.getBufferStart || position > this.getBufferEnd) {
            final int count = this.source.get(position, this.getBuffer, 0, this.getBuffer.length);
            if (count == -1) {
                return -1;
            }
            this.getBufferStart = position;
            this.getBufferEnd = position + count - 1L;
        }
        final int bufPos = (int)(position - this.getBufferStart);
        return 0xFF & this.getBuffer[bufPos];
    }
    
    @Override
    public int get(final long position, final byte[] bytes, final int off, final int len) throws IOException {
        return this.source.get(position, bytes, off, len);
    }
    
    @Override
    public long length() {
        return this.source.length();
    }
    
    @Override
    public void close() throws IOException {
        this.source.close();
        this.getBufferStart = -1L;
        this.getBufferEnd = -1L;
    }
}
