// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.io.IOException;

public class IndependentRandomAccessSource implements RandomAccessSource
{
    private final RandomAccessSource source;
    
    public IndependentRandomAccessSource(final RandomAccessSource source) {
        this.source = source;
    }
    
    @Override
    public int get(final long position) throws IOException {
        return this.source.get(position);
    }
    
    @Override
    public int get(final long position, final byte[] bytes, final int off, final int len) throws IOException {
        return this.source.get(position, bytes, off, len);
    }
    
    @Override
    public long length() {
        return this.source.length();
    }
    
    @Override
    public void close() throws IOException {
    }
}
