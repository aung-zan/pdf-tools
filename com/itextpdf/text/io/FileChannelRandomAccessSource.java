// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.io;

import java.io.IOException;
import java.nio.channels.FileChannel;

public class FileChannelRandomAccessSource implements RandomAccessSource
{
    private final FileChannel channel;
    private final MappedChannelRandomAccessSource source;
    
    public FileChannelRandomAccessSource(final FileChannel channel) throws IOException {
        this.channel = channel;
        if (channel.size() == 0L) {
            throw new IOException("File size is 0 bytes");
        }
        (this.source = new MappedChannelRandomAccessSource(channel, 0L, channel.size())).open();
    }
    
    @Override
    public void close() throws IOException {
        this.source.close();
        this.channel.close();
    }
    
    @Override
    public int get(final long position) throws IOException {
        return this.source.get(position);
    }
    
    @Override
    public int get(final long position, final byte[] bytes, final int off, final int len) throws IOException {
        return this.source.get(position, bytes, off, len);
    }
    
    @Override
    public long length() {
        return this.source.length();
    }
}
