// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import com.itextpdf.text.factories.RomanAlphabetFactory;
import java.util.Collection;
import java.util.Iterator;
import com.itextpdf.text.pdf.PdfObject;
import java.util.HashMap;
import com.itextpdf.text.pdf.PdfName;
import java.util.ArrayList;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;
import com.itextpdf.text.api.Indentable;

public class List implements TextElementArray, Indentable, IAccessibleElement
{
    public static final boolean ORDERED = true;
    public static final boolean UNORDERED = false;
    public static final boolean NUMERICAL = false;
    public static final boolean ALPHABETICAL = true;
    public static final boolean UPPERCASE = false;
    public static final boolean LOWERCASE = true;
    protected ArrayList<Element> list;
    protected boolean numbered;
    protected boolean lettered;
    protected boolean lowercase;
    protected boolean autoindent;
    protected boolean alignindent;
    protected int first;
    protected Chunk symbol;
    protected String preSymbol;
    protected String postSymbol;
    protected float indentationLeft;
    protected float indentationRight;
    protected float symbolIndent;
    protected PdfName role;
    protected HashMap<PdfName, PdfObject> accessibleAttributes;
    private AccessibleElementId id;
    
    public List() {
        this(false, false);
    }
    
    public List(final float symbolIndent) {
        this.list = new ArrayList<Element>();
        this.numbered = false;
        this.lettered = false;
        this.lowercase = false;
        this.autoindent = false;
        this.alignindent = false;
        this.first = 1;
        this.symbol = new Chunk("- ");
        this.preSymbol = "";
        this.postSymbol = ". ";
        this.indentationLeft = 0.0f;
        this.indentationRight = 0.0f;
        this.symbolIndent = 0.0f;
        this.role = PdfName.L;
        this.accessibleAttributes = null;
        this.id = null;
        this.symbolIndent = symbolIndent;
    }
    
    public List(final boolean numbered) {
        this(numbered, false);
    }
    
    public List(final boolean numbered, final boolean lettered) {
        this.list = new ArrayList<Element>();
        this.numbered = false;
        this.lettered = false;
        this.lowercase = false;
        this.autoindent = false;
        this.alignindent = false;
        this.first = 1;
        this.symbol = new Chunk("- ");
        this.preSymbol = "";
        this.postSymbol = ". ";
        this.indentationLeft = 0.0f;
        this.indentationRight = 0.0f;
        this.symbolIndent = 0.0f;
        this.role = PdfName.L;
        this.accessibleAttributes = null;
        this.id = null;
        this.numbered = numbered;
        this.lettered = lettered;
        this.autoindent = true;
        this.alignindent = true;
    }
    
    public List(final boolean numbered, final float symbolIndent) {
        this(numbered, false, symbolIndent);
    }
    
    public List(final boolean numbered, final boolean lettered, final float symbolIndent) {
        this.list = new ArrayList<Element>();
        this.numbered = false;
        this.lettered = false;
        this.lowercase = false;
        this.autoindent = false;
        this.alignindent = false;
        this.first = 1;
        this.symbol = new Chunk("- ");
        this.preSymbol = "";
        this.postSymbol = ". ";
        this.indentationLeft = 0.0f;
        this.indentationRight = 0.0f;
        this.symbolIndent = 0.0f;
        this.role = PdfName.L;
        this.accessibleAttributes = null;
        this.id = null;
        this.numbered = numbered;
        this.lettered = lettered;
        this.symbolIndent = symbolIndent;
    }
    
    @Override
    public boolean process(final ElementListener listener) {
        try {
            for (final Element element : this.list) {
                listener.add(element);
            }
            return true;
        }
        catch (DocumentException de) {
            return false;
        }
    }
    
    @Override
    public int type() {
        return 14;
    }
    
    @Override
    public java.util.List<Chunk> getChunks() {
        final java.util.List<Chunk> tmp = new ArrayList<Chunk>();
        for (final Element element : this.list) {
            tmp.addAll(element.getChunks());
        }
        return tmp;
    }
    
    public boolean add(final String s) {
        return s != null && this.add(new ListItem(s));
    }
    
    @Override
    public boolean add(final Element o) {
        if (o instanceof ListItem) {
            final ListItem item = (ListItem)o;
            if (this.numbered || this.lettered) {
                final Chunk chunk = new Chunk(this.preSymbol, this.symbol.getFont());
                chunk.setAttributes(this.symbol.getAttributes());
                final int index = this.first + this.list.size();
                if (this.lettered) {
                    chunk.append(RomanAlphabetFactory.getString(index, this.lowercase));
                }
                else {
                    chunk.append(String.valueOf(index));
                }
                chunk.append(this.postSymbol);
                item.setListSymbol(chunk);
            }
            else {
                item.setListSymbol(this.symbol);
            }
            item.setIndentationLeft(this.symbolIndent, this.autoindent);
            item.setIndentationRight(0.0f);
            return this.list.add(item);
        }
        if (o instanceof List) {
            final List nested = (List)o;
            nested.setIndentationLeft(nested.getIndentationLeft() + this.symbolIndent);
            --this.first;
            return this.list.add(nested);
        }
        return false;
    }
    
    public List cloneShallow() {
        final List clone = new List();
        this.populateProperties(clone);
        return clone;
    }
    
    protected void populateProperties(final List clone) {
        clone.indentationLeft = this.indentationLeft;
        clone.indentationRight = this.indentationRight;
        clone.autoindent = this.autoindent;
        clone.alignindent = this.alignindent;
        clone.symbolIndent = this.symbolIndent;
        clone.symbol = this.symbol;
    }
    
    public void normalizeIndentation() {
        float max = 0.0f;
        for (final Element o : this.list) {
            if (o instanceof ListItem) {
                max = Math.max(max, ((ListItem)o).getIndentationLeft());
            }
        }
        for (final Element o : this.list) {
            if (o instanceof ListItem) {
                ((ListItem)o).setIndentationLeft(max);
            }
        }
    }
    
    public void setNumbered(final boolean numbered) {
        this.numbered = numbered;
    }
    
    public void setLettered(final boolean lettered) {
        this.lettered = lettered;
    }
    
    public void setLowercase(final boolean uppercase) {
        this.lowercase = uppercase;
    }
    
    public void setAutoindent(final boolean autoindent) {
        this.autoindent = autoindent;
    }
    
    public void setAlignindent(final boolean alignindent) {
        this.alignindent = alignindent;
    }
    
    public void setFirst(final int first) {
        this.first = first;
    }
    
    public void setListSymbol(final Chunk symbol) {
        this.symbol = symbol;
    }
    
    public void setListSymbol(final String symbol) {
        this.symbol = new Chunk(symbol);
    }
    
    @Override
    public void setIndentationLeft(final float indentation) {
        this.indentationLeft = indentation;
    }
    
    @Override
    public void setIndentationRight(final float indentation) {
        this.indentationRight = indentation;
    }
    
    public void setSymbolIndent(final float symbolIndent) {
        this.symbolIndent = symbolIndent;
    }
    
    public ArrayList<Element> getItems() {
        return this.list;
    }
    
    public int size() {
        return this.list.size();
    }
    
    public boolean isEmpty() {
        return this.list.isEmpty();
    }
    
    public float getTotalLeading() {
        if (this.list.size() < 1) {
            return -1.0f;
        }
        final ListItem item = this.list.get(0);
        return item.getTotalLeading();
    }
    
    public boolean isNumbered() {
        return this.numbered;
    }
    
    public boolean isLettered() {
        return this.lettered;
    }
    
    public boolean isLowercase() {
        return this.lowercase;
    }
    
    public boolean isAutoindent() {
        return this.autoindent;
    }
    
    public boolean isAlignindent() {
        return this.alignindent;
    }
    
    public int getFirst() {
        return this.first;
    }
    
    public Chunk getSymbol() {
        return this.symbol;
    }
    
    @Override
    public float getIndentationLeft() {
        return this.indentationLeft;
    }
    
    @Override
    public float getIndentationRight() {
        return this.indentationRight;
    }
    
    public float getSymbolIndent() {
        return this.symbolIndent;
    }
    
    @Override
    public boolean isContent() {
        return true;
    }
    
    @Override
    public boolean isNestable() {
        return true;
    }
    
    public String getPostSymbol() {
        return this.postSymbol;
    }
    
    public void setPostSymbol(final String postSymbol) {
        this.postSymbol = postSymbol;
    }
    
    public String getPreSymbol() {
        return this.preSymbol;
    }
    
    public void setPreSymbol(final String preSymbol) {
        this.preSymbol = preSymbol;
    }
    
    public ListItem getFirstItem() {
        final Element lastElement = (this.list.size() > 0) ? this.list.get(0) : null;
        if (lastElement != null) {
            if (lastElement instanceof ListItem) {
                return (ListItem)lastElement;
            }
            if (lastElement instanceof List) {
                return ((List)lastElement).getFirstItem();
            }
        }
        return null;
    }
    
    public ListItem getLastItem() {
        final Element lastElement = (this.list.size() > 0) ? this.list.get(this.list.size() - 1) : null;
        if (lastElement != null) {
            if (lastElement instanceof ListItem) {
                return (ListItem)lastElement;
            }
            if (lastElement instanceof List) {
                return ((List)lastElement).getLastItem();
            }
        }
        return null;
    }
    
    @Override
    public PdfObject getAccessibleAttribute(final PdfName key) {
        if (this.accessibleAttributes != null) {
            return this.accessibleAttributes.get(key);
        }
        return null;
    }
    
    @Override
    public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
        if (this.accessibleAttributes == null) {
            this.accessibleAttributes = new HashMap<PdfName, PdfObject>();
        }
        this.accessibleAttributes.put(key, value);
    }
    
    @Override
    public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
        return this.accessibleAttributes;
    }
    
    @Override
    public PdfName getRole() {
        return this.role;
    }
    
    @Override
    public void setRole(final PdfName role) {
        this.role = role;
    }
    
    @Override
    public AccessibleElementId getId() {
        if (this.id == null) {
            this.id = new AccessibleElementId();
        }
        return this.id;
    }
    
    @Override
    public void setId(final AccessibleElementId id) {
        this.id = id;
    }
    
    @Override
    public boolean isInline() {
        return false;
    }
}
