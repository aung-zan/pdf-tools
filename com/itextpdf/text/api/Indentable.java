// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.api;

public interface Indentable
{
    void setIndentationLeft(final float p0);
    
    void setIndentationRight(final float p0);
    
    float getIndentationLeft();
    
    float getIndentationRight();
}
