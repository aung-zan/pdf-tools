// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.api;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;

public interface WriterOperation
{
    void write(final PdfWriter p0, final Document p1) throws DocumentException;
}
