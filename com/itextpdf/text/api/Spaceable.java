// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.api;

public interface Spaceable
{
    void setSpacingBefore(final float p0);
    
    void setSpacingAfter(final float p0);
    
    void setPaddingTop(final float p0);
    
    float getSpacingBefore();
    
    float getSpacingAfter();
    
    float getPaddingTop();
}
