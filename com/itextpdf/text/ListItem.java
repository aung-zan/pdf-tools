// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import java.util.List;
import com.itextpdf.text.pdf.PdfName;

public class ListItem extends Paragraph
{
    private static final long serialVersionUID = 1970670787169329006L;
    protected Chunk symbol;
    private ListBody listBody;
    private ListLabel listLabel;
    
    public ListItem() {
        this.listBody = null;
        this.listLabel = null;
        this.setRole(PdfName.LI);
    }
    
    public ListItem(final float leading) {
        super(leading);
        this.listBody = null;
        this.listLabel = null;
        this.setRole(PdfName.LI);
    }
    
    public ListItem(final Chunk chunk) {
        super(chunk);
        this.listBody = null;
        this.listLabel = null;
        this.setRole(PdfName.LI);
    }
    
    public ListItem(final String string) {
        super(string);
        this.listBody = null;
        this.listLabel = null;
        this.setRole(PdfName.LI);
    }
    
    public ListItem(final String string, final Font font) {
        super(string, font);
        this.listBody = null;
        this.listLabel = null;
        this.setRole(PdfName.LI);
    }
    
    public ListItem(final float leading, final Chunk chunk) {
        super(leading, chunk);
        this.listBody = null;
        this.listLabel = null;
        this.setRole(PdfName.LI);
    }
    
    public ListItem(final float leading, final String string) {
        super(leading, string);
        this.listBody = null;
        this.listLabel = null;
        this.setRole(PdfName.LI);
    }
    
    public ListItem(final float leading, final String string, final Font font) {
        super(leading, string, font);
        this.listBody = null;
        this.listLabel = null;
        this.setRole(PdfName.LI);
    }
    
    public ListItem(final Phrase phrase) {
        super(phrase);
        this.listBody = null;
        this.listLabel = null;
        this.setRole(PdfName.LI);
    }
    
    @Override
    public int type() {
        return 15;
    }
    
    @Override
    public Paragraph cloneShallow(final boolean spacingBefore) {
        final ListItem copy = new ListItem();
        this.populateProperties(copy, spacingBefore);
        return copy;
    }
    
    public void setListSymbol(final Chunk symbol) {
        if (this.symbol == null) {
            this.symbol = symbol;
            if (this.symbol.getFont().isStandardFont()) {
                this.symbol.setFont(this.font);
            }
        }
    }
    
    public void setIndentationLeft(final float indentation, final boolean autoindent) {
        if (autoindent) {
            this.setIndentationLeft(this.getListSymbol().getWidthPoint());
        }
        else {
            this.setIndentationLeft(indentation);
        }
    }
    
    public void adjustListSymbolFont() {
        final List<Chunk> cks = this.getChunks();
        if (!cks.isEmpty() && this.symbol != null) {
            this.symbol.setFont(cks.get(0).getFont());
        }
    }
    
    public Chunk getListSymbol() {
        return this.symbol;
    }
    
    public ListBody getListBody() {
        if (this.listBody == null) {
            this.listBody = new ListBody(this);
        }
        return this.listBody;
    }
    
    public ListLabel getListLabel() {
        if (this.listLabel == null) {
            this.listLabel = new ListLabel(this);
        }
        return this.listLabel;
    }
}
