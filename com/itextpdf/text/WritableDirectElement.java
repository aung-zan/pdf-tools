// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import java.util.ArrayList;
import java.util.List;
import com.itextpdf.text.api.WriterOperation;

public abstract class WritableDirectElement implements Element, WriterOperation
{
    public static final int DIRECT_ELEMENT_TYPE_UNKNOWN = 0;
    public static final int DIRECT_ELEMENT_TYPE_HEADER = 1;
    protected int directElementType;
    
    public WritableDirectElement() {
        this.directElementType = 0;
    }
    
    public WritableDirectElement(final int directElementType) {
        this.directElementType = 0;
        this.directElementType = directElementType;
    }
    
    @Override
    public boolean process(final ElementListener listener) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public int type() {
        return 666;
    }
    
    @Override
    public boolean isContent() {
        return false;
    }
    
    @Override
    public boolean isNestable() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public List<Chunk> getChunks() {
        return new ArrayList<Chunk>(0);
    }
    
    public int getDirectElementType() {
        return this.directElementType;
    }
}
