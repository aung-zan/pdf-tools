// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.exceptions;

public class UnsupportedPdfException extends InvalidPdfException
{
    private static final long serialVersionUID = 2180764250839096628L;
    
    public UnsupportedPdfException(final String message) {
        super(message);
    }
}
