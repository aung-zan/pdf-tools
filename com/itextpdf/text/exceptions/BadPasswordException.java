// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.exceptions;

import java.io.IOException;

public class BadPasswordException extends IOException
{
    private static final long serialVersionUID = -4333706268155063964L;
    
    public BadPasswordException(final String message) {
        super(message);
    }
}
