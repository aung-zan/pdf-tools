// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.exceptions;

public class InvalidImageException extends RuntimeException
{
    private static final long serialVersionUID = -1319471492541702697L;
    private final Throwable cause;
    
    public InvalidImageException(final String message) {
        this(message, null);
    }
    
    public InvalidImageException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
