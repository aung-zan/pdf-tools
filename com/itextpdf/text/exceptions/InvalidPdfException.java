// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.exceptions;

import java.io.IOException;

public class InvalidPdfException extends IOException
{
    private static final long serialVersionUID = -2319614911517026938L;
    private final Throwable cause;
    
    public InvalidPdfException(final String message) {
        this(message, null);
    }
    
    public InvalidPdfException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
