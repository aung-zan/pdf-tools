// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import com.itextpdf.text.pdf.PdfName;

public class ListLabel extends ListBody
{
    protected PdfName role;
    protected float indentation;
    
    protected ListLabel(final ListItem parentItem) {
        super(parentItem);
        this.role = PdfName.LBL;
        this.indentation = 0.0f;
    }
    
    @Override
    public PdfName getRole() {
        return this.role;
    }
    
    @Override
    public void setRole(final PdfName role) {
        this.role = role;
    }
    
    public float getIndentation() {
        return this.indentation;
    }
    
    public void setIndentation(final float indentation) {
        this.indentation = indentation;
    }
    
    @Deprecated
    public boolean getTagLabelContent() {
        return false;
    }
    
    @Deprecated
    public void setTagLabelContent(final boolean tagLabelContent) {
    }
    
    @Override
    public boolean isInline() {
        return true;
    }
}
