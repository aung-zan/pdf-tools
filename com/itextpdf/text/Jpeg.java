// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import com.itextpdf.text.pdf.ICC_Profile;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.net.URL;

public class Jpeg extends Image
{
    public static final int NOT_A_MARKER = -1;
    public static final int VALID_MARKER = 0;
    public static final int[] VALID_MARKERS;
    public static final int UNSUPPORTED_MARKER = 1;
    public static final int[] UNSUPPORTED_MARKERS;
    public static final int NOPARAM_MARKER = 2;
    public static final int[] NOPARAM_MARKERS;
    public static final int M_APP0 = 224;
    public static final int M_APP2 = 226;
    public static final int M_APPE = 238;
    public static final int M_APPD = 237;
    public static final byte[] JFIF_ID;
    public static final byte[] PS_8BIM_RESO;
    private byte[][] icc;
    
    Jpeg(final Image image) {
        super(image);
    }
    
    public Jpeg(final URL url) throws BadElementException, IOException {
        super(url);
        this.processParameters();
    }
    
    public Jpeg(final byte[] img) throws BadElementException, IOException {
        super((URL)null);
        this.rawData = img;
        this.originalData = img;
        this.processParameters();
    }
    
    public Jpeg(final byte[] img, final float width, final float height) throws BadElementException, IOException {
        this(img);
        this.scaledWidth = width;
        this.scaledHeight = height;
    }
    
    private static final int getShort(final InputStream is) throws IOException {
        return (is.read() << 8) + is.read();
    }
    
    private static final int marker(final int marker) {
        for (int i = 0; i < Jpeg.VALID_MARKERS.length; ++i) {
            if (marker == Jpeg.VALID_MARKERS[i]) {
                return 0;
            }
        }
        for (int i = 0; i < Jpeg.NOPARAM_MARKERS.length; ++i) {
            if (marker == Jpeg.NOPARAM_MARKERS[i]) {
                return 2;
            }
        }
        for (int i = 0; i < Jpeg.UNSUPPORTED_MARKERS.length; ++i) {
            if (marker == Jpeg.UNSUPPORTED_MARKERS[i]) {
                return 1;
            }
        }
        return -1;
    }
    
    private void processParameters() throws BadElementException, IOException {
        this.type = 32;
        this.originalType = 1;
        InputStream is = null;
        try {
            String errorID;
            if (this.rawData == null) {
                is = this.url.openStream();
                errorID = this.url.toString();
            }
            else {
                is = new ByteArrayInputStream(this.rawData);
                errorID = "Byte array";
            }
            if (is.read() != 255 || is.read() != 216) {
                throw new BadElementException(MessageLocalization.getComposedMessage("1.is.not.a.valid.jpeg.file", errorID));
            }
            boolean firstPass = true;
            while (true) {
                final int v = is.read();
                if (v < 0) {
                    throw new IOException(MessageLocalization.getComposedMessage("premature.eof.while.reading.jpg", new Object[0]));
                }
                if (v != 255) {
                    continue;
                }
                final int marker = is.read();
                if (firstPass && marker == 224) {
                    firstPass = false;
                    final int len = getShort(is);
                    if (len < 16) {
                        Utilities.skip(is, len - 2);
                    }
                    else {
                        final byte[] bcomp = new byte[Jpeg.JFIF_ID.length];
                        final int r = is.read(bcomp);
                        if (r != bcomp.length) {
                            throw new BadElementException(MessageLocalization.getComposedMessage("1.corrupted.jfif.marker", errorID));
                        }
                        boolean found = true;
                        for (int k = 0; k < bcomp.length; ++k) {
                            if (bcomp[k] != Jpeg.JFIF_ID[k]) {
                                found = false;
                                break;
                            }
                        }
                        if (!found) {
                            Utilities.skip(is, len - 2 - bcomp.length);
                        }
                        else {
                            Utilities.skip(is, 2);
                            final int units = is.read();
                            final int dx = getShort(is);
                            final int dy = getShort(is);
                            if (units == 1) {
                                this.dpiX = dx;
                                this.dpiY = dy;
                            }
                            else if (units == 2) {
                                this.dpiX = (int)(dx * 2.54f + 0.5f);
                                this.dpiY = (int)(dy * 2.54f + 0.5f);
                            }
                            Utilities.skip(is, len - 2 - bcomp.length - 7);
                        }
                    }
                }
                else if (marker == 238) {
                    final int len = getShort(is) - 2;
                    final byte[] byteappe = new byte[len];
                    for (int i = 0; i < len; ++i) {
                        byteappe[i] = (byte)is.read();
                    }
                    if (byteappe.length < 12) {
                        continue;
                    }
                    final String appe = new String(byteappe, 0, 5, "ISO-8859-1");
                    if (!appe.equals("Adobe")) {
                        continue;
                    }
                    this.invert = true;
                }
                else if (marker == 226) {
                    final int len = getShort(is) - 2;
                    final byte[] byteapp2 = new byte[len];
                    for (int i = 0; i < len; ++i) {
                        byteapp2[i] = (byte)is.read();
                    }
                    if (byteapp2.length < 14) {
                        continue;
                    }
                    final String app2 = new String(byteapp2, 0, 11, "ISO-8859-1");
                    if (!app2.equals("ICC_PROFILE")) {
                        continue;
                    }
                    int order = byteapp2[12] & 0xFF;
                    int count = byteapp2[13] & 0xFF;
                    if (order < 1) {
                        order = 1;
                    }
                    if (count < 1) {
                        count = 1;
                    }
                    if (this.icc == null) {
                        this.icc = new byte[count][];
                    }
                    this.icc[order - 1] = byteapp2;
                }
                else if (marker == 237) {
                    final int len = getShort(is) - 2;
                    final byte[] byteappd = new byte[len];
                    for (int i = 0; i < len; ++i) {
                        byteappd[i] = (byte)is.read();
                    }
                    boolean found;
                    int i;
                    int j;
                    for (i = 0, i = 0; i < len - Jpeg.PS_8BIM_RESO.length; ++i) {
                        found = true;
                        for (j = 0; j < Jpeg.PS_8BIM_RESO.length; ++j) {
                            if (byteappd[i + j] != Jpeg.PS_8BIM_RESO[j]) {
                                found = false;
                                break;
                            }
                        }
                        if (found) {
                            break;
                        }
                    }
                    i += Jpeg.PS_8BIM_RESO.length;
                    if (i >= len - Jpeg.PS_8BIM_RESO.length) {
                        continue;
                    }
                    byte namelength = byteappd[i];
                    ++namelength;
                    if (namelength % 2 == 1) {
                        ++namelength;
                    }
                    i += namelength;
                    final int resosize = (byteappd[i] << 24) + (byteappd[i + 1] << 16) + (byteappd[i + 2] << 8) + byteappd[i + 3];
                    if (resosize != 16) {
                        continue;
                    }
                    i += 4;
                    int dx = (byteappd[i] << 8) + (byteappd[i + 1] & 0xFF);
                    i += 2;
                    i += 2;
                    final int unitsx = (byteappd[i] << 8) + (byteappd[i + 1] & 0xFF);
                    i += 2;
                    i += 2;
                    int dy2 = (byteappd[i] << 8) + (byteappd[i + 1] & 0xFF);
                    i += 2;
                    i += 2;
                    final int unitsy = (byteappd[i] << 8) + (byteappd[i + 1] & 0xFF);
                    if (unitsx == 1 || unitsx == 2) {
                        dx = ((unitsx == 2) ? ((int)(dx * 2.54f + 0.5f)) : dx);
                        if (this.dpiX == 0 || this.dpiX == dx) {
                            this.dpiX = dx;
                        }
                    }
                    if (unitsy != 1 && unitsy != 2) {
                        continue;
                    }
                    dy2 = ((unitsy == 2) ? ((int)(dy2 * 2.54f + 0.5f)) : dy2);
                    if (this.dpiY != 0 && this.dpiY != dy2) {
                        continue;
                    }
                    this.dpiY = dy2;
                }
                else {
                    firstPass = false;
                    final int markertype = marker(marker);
                    if (markertype == 0) {
                        Utilities.skip(is, 2);
                        if (is.read() != 8) {
                            throw new BadElementException(MessageLocalization.getComposedMessage("1.must.have.8.bits.per.component", errorID));
                        }
                        this.setTop(this.scaledHeight = (float)getShort(is));
                        this.setRight(this.scaledWidth = (float)getShort(is));
                        this.colorspace = is.read();
                        this.bpc = 8;
                        break;
                    }
                    else {
                        if (markertype == 1) {
                            throw new BadElementException(MessageLocalization.getComposedMessage("1.unsupported.jpeg.marker.2", errorID, String.valueOf(marker)));
                        }
                        if (markertype == 2) {
                            continue;
                        }
                        Utilities.skip(is, getShort(is) - 2);
                    }
                }
            }
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
        this.plainWidth = this.getWidth();
        this.plainHeight = this.getHeight();
        if (this.icc != null) {
            int total = 0;
            for (int l = 0; l < this.icc.length; ++l) {
                if (this.icc[l] == null) {
                    this.icc = null;
                    return;
                }
                total += this.icc[l].length - 14;
            }
            final byte[] ficc = new byte[total];
            total = 0;
            for (int m = 0; m < this.icc.length; ++m) {
                System.arraycopy(this.icc[m], 14, ficc, total, this.icc[m].length - 14);
                total += this.icc[m].length - 14;
            }
            try {
                final ICC_Profile icc_prof = ICC_Profile.getInstance(ficc, this.colorspace);
                this.tagICC(icc_prof);
            }
            catch (IllegalArgumentException ex) {}
            this.icc = null;
        }
    }
    
    static {
        VALID_MARKERS = new int[] { 192, 193, 194 };
        UNSUPPORTED_MARKERS = new int[] { 195, 197, 198, 199, 200, 201, 202, 203, 205, 206, 207 };
        NOPARAM_MARKERS = new int[] { 208, 209, 210, 211, 212, 213, 214, 215, 216, 1 };
        JFIF_ID = new byte[] { 74, 70, 73, 70, 0 };
        PS_8BIM_RESO = new byte[] { 56, 66, 73, 77, 3, -19 };
    }
}
