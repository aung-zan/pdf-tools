// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import com.itextpdf.text.pdf.draw.DrawInterface;

public class TabStop
{
    protected float position;
    protected Alignment alignment;
    protected DrawInterface leader;
    protected char anchorChar;
    
    public static TabStop newInstance(float currentPosition, float tabInterval) {
        currentPosition = Math.round(currentPosition * 1000.0f) / 1000.0f;
        tabInterval = Math.round(tabInterval * 1000.0f) / 1000.0f;
        final TabStop tabStop = new TabStop(currentPosition + tabInterval - currentPosition % tabInterval);
        return tabStop;
    }
    
    public TabStop(final float position) {
        this(position, Alignment.LEFT);
    }
    
    public TabStop(final float position, final DrawInterface leader) {
        this(position, leader, Alignment.LEFT);
    }
    
    public TabStop(final float position, final Alignment alignment) {
        this(position, null, alignment);
    }
    
    public TabStop(final float position, final Alignment alignment, final char anchorChar) {
        this(position, null, alignment, anchorChar);
    }
    
    public TabStop(final float position, final DrawInterface leader, final Alignment alignment) {
        this(position, leader, alignment, '.');
    }
    
    public TabStop(final float position, final DrawInterface leader, final Alignment alignment, final char anchorChar) {
        this.alignment = Alignment.LEFT;
        this.anchorChar = '.';
        this.position = position;
        this.leader = leader;
        this.alignment = alignment;
        this.anchorChar = anchorChar;
    }
    
    public TabStop(final TabStop tabStop) {
        this(tabStop.getPosition(), tabStop.getLeader(), tabStop.getAlignment(), tabStop.getAnchorChar());
    }
    
    public float getPosition() {
        return this.position;
    }
    
    public void setPosition(final float position) {
        this.position = position;
    }
    
    public Alignment getAlignment() {
        return this.alignment;
    }
    
    public void setAlignment(final Alignment alignment) {
        this.alignment = alignment;
    }
    
    public DrawInterface getLeader() {
        return this.leader;
    }
    
    public void setLeader(final DrawInterface leader) {
        this.leader = leader;
    }
    
    public char getAnchorChar() {
        return this.anchorChar;
    }
    
    public void setAnchorChar(final char anchorChar) {
        this.anchorChar = anchorChar;
    }
    
    public float getPosition(final float tabPosition, final float currentPosition, final float anchorPosition) {
        float newPosition = this.position;
        final float textWidth = currentPosition - tabPosition;
        switch (this.alignment) {
            case RIGHT: {
                if (tabPosition + textWidth < this.position) {
                    newPosition = this.position - textWidth;
                    break;
                }
                newPosition = tabPosition;
                break;
            }
            case CENTER: {
                if (tabPosition + textWidth / 2.0f < this.position) {
                    newPosition = this.position - textWidth / 2.0f;
                    break;
                }
                newPosition = tabPosition;
                break;
            }
            case ANCHOR: {
                if (!Float.isNaN(anchorPosition)) {
                    if (anchorPosition < this.position) {
                        newPosition = this.position - (anchorPosition - tabPosition);
                        break;
                    }
                    newPosition = tabPosition;
                    break;
                }
                else {
                    if (tabPosition + textWidth < this.position) {
                        newPosition = this.position - textWidth;
                        break;
                    }
                    newPosition = tabPosition;
                    break;
                }
                break;
            }
        }
        return newPosition;
    }
    
    public enum Alignment
    {
        LEFT, 
        RIGHT, 
        CENTER, 
        ANCHOR;
    }
}
