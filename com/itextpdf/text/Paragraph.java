// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import java.util.Iterator;
import com.itextpdf.text.pdf.PdfPTable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.itextpdf.text.pdf.PdfObject;
import java.util.HashMap;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;
import com.itextpdf.text.api.Spaceable;
import com.itextpdf.text.api.Indentable;

public class Paragraph extends Phrase implements Indentable, Spaceable, IAccessibleElement
{
    private static final long serialVersionUID = 7852314969733375514L;
    protected int alignment;
    protected float indentationLeft;
    protected float indentationRight;
    private float firstLineIndent;
    protected float spacingBefore;
    protected float spacingAfter;
    private float extraParagraphSpace;
    protected boolean keeptogether;
    protected float paddingTop;
    protected PdfName role;
    protected HashMap<PdfName, PdfObject> accessibleAttributes;
    protected AccessibleElementId id;
    
    public Paragraph() {
        this.alignment = -1;
        this.firstLineIndent = 0.0f;
        this.extraParagraphSpace = 0.0f;
        this.keeptogether = false;
        this.role = PdfName.P;
        this.accessibleAttributes = null;
        this.id = null;
    }
    
    public Paragraph(final float leading) {
        super(leading);
        this.alignment = -1;
        this.firstLineIndent = 0.0f;
        this.extraParagraphSpace = 0.0f;
        this.keeptogether = false;
        this.role = PdfName.P;
        this.accessibleAttributes = null;
        this.id = null;
    }
    
    public Paragraph(final Chunk chunk) {
        super(chunk);
        this.alignment = -1;
        this.firstLineIndent = 0.0f;
        this.extraParagraphSpace = 0.0f;
        this.keeptogether = false;
        this.role = PdfName.P;
        this.accessibleAttributes = null;
        this.id = null;
    }
    
    public Paragraph(final float leading, final Chunk chunk) {
        super(leading, chunk);
        this.alignment = -1;
        this.firstLineIndent = 0.0f;
        this.extraParagraphSpace = 0.0f;
        this.keeptogether = false;
        this.role = PdfName.P;
        this.accessibleAttributes = null;
        this.id = null;
    }
    
    public Paragraph(final String string) {
        super(string);
        this.alignment = -1;
        this.firstLineIndent = 0.0f;
        this.extraParagraphSpace = 0.0f;
        this.keeptogether = false;
        this.role = PdfName.P;
        this.accessibleAttributes = null;
        this.id = null;
    }
    
    public Paragraph(final String string, final Font font) {
        super(string, font);
        this.alignment = -1;
        this.firstLineIndent = 0.0f;
        this.extraParagraphSpace = 0.0f;
        this.keeptogether = false;
        this.role = PdfName.P;
        this.accessibleAttributes = null;
        this.id = null;
    }
    
    public Paragraph(final float leading, final String string) {
        super(leading, string);
        this.alignment = -1;
        this.firstLineIndent = 0.0f;
        this.extraParagraphSpace = 0.0f;
        this.keeptogether = false;
        this.role = PdfName.P;
        this.accessibleAttributes = null;
        this.id = null;
    }
    
    public Paragraph(final float leading, final String string, final Font font) {
        super(leading, string, font);
        this.alignment = -1;
        this.firstLineIndent = 0.0f;
        this.extraParagraphSpace = 0.0f;
        this.keeptogether = false;
        this.role = PdfName.P;
        this.accessibleAttributes = null;
        this.id = null;
    }
    
    public Paragraph(final Phrase phrase) {
        super(phrase);
        this.alignment = -1;
        this.firstLineIndent = 0.0f;
        this.extraParagraphSpace = 0.0f;
        this.keeptogether = false;
        this.role = PdfName.P;
        this.accessibleAttributes = null;
        this.id = null;
        if (phrase instanceof Paragraph) {
            final Paragraph p = (Paragraph)phrase;
            this.setAlignment(p.alignment);
            this.setIndentationLeft(p.getIndentationLeft());
            this.setIndentationRight(p.getIndentationRight());
            this.setFirstLineIndent(p.getFirstLineIndent());
            this.setSpacingAfter(p.getSpacingAfter());
            this.setSpacingBefore(p.getSpacingBefore());
            this.setExtraParagraphSpace(p.getExtraParagraphSpace());
            this.setRole(p.role);
            this.id = p.getId();
            if (p.accessibleAttributes != null) {
                this.accessibleAttributes = new HashMap<PdfName, PdfObject>(p.accessibleAttributes);
            }
        }
    }
    
    public Paragraph cloneShallow(final boolean spacingBefore) {
        final Paragraph copy = new Paragraph();
        this.populateProperties(copy, spacingBefore);
        return copy;
    }
    
    protected void populateProperties(final Paragraph copy, final boolean spacingBefore) {
        copy.setFont(this.getFont());
        copy.setAlignment(this.getAlignment());
        copy.setLeading(this.getLeading(), this.multipliedLeading);
        copy.setIndentationLeft(this.getIndentationLeft());
        copy.setIndentationRight(this.getIndentationRight());
        copy.setFirstLineIndent(this.getFirstLineIndent());
        copy.setSpacingAfter(this.getSpacingAfter());
        if (spacingBefore) {
            copy.setSpacingBefore(this.getSpacingBefore());
        }
        copy.setExtraParagraphSpace(this.getExtraParagraphSpace());
        copy.setRole(this.role);
        copy.id = this.getId();
        if (this.accessibleAttributes != null) {
            copy.accessibleAttributes = new HashMap<PdfName, PdfObject>(this.accessibleAttributes);
        }
        copy.setTabSettings(this.getTabSettings());
        copy.setKeepTogether(this.getKeepTogether());
    }
    
    public List<Element> breakUp() {
        final List<Element> list = new ArrayList<Element>();
        Paragraph tmp = null;
        for (final Element e : this) {
            if (e.type() == 14 || e.type() == 23 || e.type() == 12) {
                if (tmp != null && tmp.size() > 0) {
                    tmp.setSpacingAfter(0.0f);
                    list.add(tmp);
                    tmp = this.cloneShallow(false);
                }
                if (list.size() == 0) {
                    switch (e.type()) {
                        case 23: {
                            ((PdfPTable)e).setSpacingBefore(this.getSpacingBefore());
                            break;
                        }
                        case 12: {
                            ((Paragraph)e).setSpacingBefore(this.getSpacingBefore());
                            break;
                        }
                        case 14: {
                            final ListItem firstItem = ((com.itextpdf.text.List)e).getFirstItem();
                            if (firstItem != null) {
                                firstItem.setSpacingBefore(this.getSpacingBefore());
                                break;
                            }
                            break;
                        }
                    }
                }
                list.add(e);
            }
            else {
                if (tmp == null) {
                    tmp = this.cloneShallow(list.size() == 0);
                }
                tmp.add(e);
            }
        }
        if (tmp != null && tmp.size() > 0) {
            list.add(tmp);
        }
        if (list.size() != 0) {
            final Element lastElement = list.get(list.size() - 1);
            switch (lastElement.type()) {
                case 23: {
                    ((PdfPTable)lastElement).setSpacingAfter(this.getSpacingAfter());
                    break;
                }
                case 12: {
                    ((Paragraph)lastElement).setSpacingAfter(this.getSpacingAfter());
                    break;
                }
                case 14: {
                    final ListItem lastItem = ((com.itextpdf.text.List)lastElement).getLastItem();
                    if (lastItem != null) {
                        lastItem.setSpacingAfter(this.getSpacingAfter());
                        break;
                    }
                    break;
                }
            }
        }
        return list;
    }
    
    @Override
    public int type() {
        return 12;
    }
    
    @Override
    public boolean add(final Element o) {
        if (o instanceof com.itextpdf.text.List) {
            final com.itextpdf.text.List list = (com.itextpdf.text.List)o;
            list.setIndentationLeft(list.getIndentationLeft() + this.indentationLeft);
            list.setIndentationRight(this.indentationRight);
            return super.add(list);
        }
        if (o instanceof Image) {
            super.addSpecial(o);
            return true;
        }
        if (o instanceof Paragraph) {
            super.addSpecial(o);
            return true;
        }
        return super.add(o);
    }
    
    public void setAlignment(final int alignment) {
        this.alignment = alignment;
    }
    
    @Override
    public void setIndentationLeft(final float indentation) {
        this.indentationLeft = indentation;
    }
    
    @Override
    public void setIndentationRight(final float indentation) {
        this.indentationRight = indentation;
    }
    
    public void setFirstLineIndent(final float firstLineIndent) {
        this.firstLineIndent = firstLineIndent;
    }
    
    @Override
    public void setSpacingBefore(final float spacing) {
        this.spacingBefore = spacing;
    }
    
    @Override
    public void setSpacingAfter(final float spacing) {
        this.spacingAfter = spacing;
    }
    
    public void setKeepTogether(final boolean keeptogether) {
        this.keeptogether = keeptogether;
    }
    
    public boolean getKeepTogether() {
        return this.keeptogether;
    }
    
    public int getAlignment() {
        return this.alignment;
    }
    
    @Override
    public float getIndentationLeft() {
        return this.indentationLeft;
    }
    
    @Override
    public float getIndentationRight() {
        return this.indentationRight;
    }
    
    public float getFirstLineIndent() {
        return this.firstLineIndent;
    }
    
    @Override
    public float getSpacingBefore() {
        return this.spacingBefore;
    }
    
    @Override
    public float getSpacingAfter() {
        return this.spacingAfter;
    }
    
    public float getExtraParagraphSpace() {
        return this.extraParagraphSpace;
    }
    
    public void setExtraParagraphSpace(final float extraParagraphSpace) {
        this.extraParagraphSpace = extraParagraphSpace;
    }
    
    @Deprecated
    public float spacingBefore() {
        return this.getSpacingBefore();
    }
    
    @Deprecated
    public float spacingAfter() {
        return this.spacingAfter;
    }
    
    @Override
    public PdfObject getAccessibleAttribute(final PdfName key) {
        if (this.accessibleAttributes != null) {
            return this.accessibleAttributes.get(key);
        }
        return null;
    }
    
    @Override
    public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
        if (this.accessibleAttributes == null) {
            this.accessibleAttributes = new HashMap<PdfName, PdfObject>();
        }
        this.accessibleAttributes.put(key, value);
    }
    
    @Override
    public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
        return this.accessibleAttributes;
    }
    
    @Override
    public PdfName getRole() {
        return this.role;
    }
    
    @Override
    public void setRole(final PdfName role) {
        this.role = role;
    }
    
    @Override
    public AccessibleElementId getId() {
        if (this.id == null) {
            this.id = new AccessibleElementId();
        }
        return this.id;
    }
    
    @Override
    public void setId(final AccessibleElementId id) {
        this.id = id;
    }
    
    @Override
    public boolean isInline() {
        return false;
    }
    
    @Override
    public float getPaddingTop() {
        return this.paddingTop;
    }
    
    @Override
    public void setPaddingTop(final float paddingTop) {
        this.paddingTop = paddingTop;
    }
}
