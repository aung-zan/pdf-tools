// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.util.List;
import java.util.Iterator;
import java.util.Collection;
import com.itextpdf.text.pdf.HyphenationEvent;
import java.util.ArrayList;

public class Phrase extends ArrayList<Element> implements TextElementArray
{
    private static final long serialVersionUID = 2643594602455068231L;
    protected float leading;
    protected float multipliedLeading;
    protected Font font;
    protected HyphenationEvent hyphenation;
    protected TabSettings tabSettings;
    
    public Phrase() {
        this(16.0f);
    }
    
    public Phrase(final Phrase phrase) {
        this.leading = Float.NaN;
        this.multipliedLeading = 0.0f;
        this.hyphenation = null;
        this.tabSettings = null;
        this.addAll(phrase);
        this.setLeading(phrase.getLeading(), phrase.getMultipliedLeading());
        this.font = phrase.getFont();
        this.tabSettings = phrase.getTabSettings();
        this.setHyphenation(phrase.getHyphenation());
    }
    
    public Phrase(final float leading) {
        this.leading = Float.NaN;
        this.multipliedLeading = 0.0f;
        this.hyphenation = null;
        this.tabSettings = null;
        this.leading = leading;
        this.font = new Font();
    }
    
    public Phrase(final Chunk chunk) {
        this.leading = Float.NaN;
        this.multipliedLeading = 0.0f;
        this.hyphenation = null;
        this.tabSettings = null;
        super.add(chunk);
        this.font = chunk.getFont();
        this.setHyphenation(chunk.getHyphenation());
    }
    
    public Phrase(final float leading, final Chunk chunk) {
        this.leading = Float.NaN;
        this.multipliedLeading = 0.0f;
        this.hyphenation = null;
        this.tabSettings = null;
        this.leading = leading;
        super.add(chunk);
        this.font = chunk.getFont();
        this.setHyphenation(chunk.getHyphenation());
    }
    
    public Phrase(final String string) {
        this(Float.NaN, string, new Font());
    }
    
    public Phrase(final String string, final Font font) {
        this(Float.NaN, string, font);
    }
    
    public Phrase(final float leading, final String string) {
        this(leading, string, new Font());
    }
    
    public Phrase(final float leading, final String string, final Font font) {
        this.leading = Float.NaN;
        this.multipliedLeading = 0.0f;
        this.hyphenation = null;
        this.tabSettings = null;
        this.leading = leading;
        this.font = font;
        if (string != null && string.length() != 0) {
            super.add(new Chunk(string, font));
        }
    }
    
    @Override
    public boolean process(final ElementListener listener) {
        try {
            for (final Object element : this) {
                listener.add((Element)element);
            }
            return true;
        }
        catch (DocumentException de) {
            return false;
        }
    }
    
    @Override
    public int type() {
        return 11;
    }
    
    @Override
    public List<Chunk> getChunks() {
        final List<Chunk> tmp = new ArrayList<Chunk>();
        for (final Element element : this) {
            tmp.addAll(element.getChunks());
        }
        return tmp;
    }
    
    @Override
    public boolean isContent() {
        return true;
    }
    
    @Override
    public boolean isNestable() {
        return true;
    }
    
    @Override
    public void add(final int index, final Element element) {
        if (element == null) {
            return;
        }
        switch (element.type()) {
            case 10: {
                final Chunk chunk = (Chunk)element;
                if (!this.font.isStandardFont()) {
                    chunk.setFont(this.font.difference(chunk.getFont()));
                }
                if (this.hyphenation != null && chunk.getHyphenation() == null && !chunk.isEmpty()) {
                    chunk.setHyphenation(this.hyphenation);
                }
                super.add(index, chunk);
            }
            case 11:
            case 12:
            case 14:
            case 17:
            case 23:
            case 29:
            case 37:
            case 50:
            case 55:
            case 666: {
                super.add(index, element);
            }
            default: {
                throw new ClassCastException(MessageLocalization.getComposedMessage("insertion.of.illegal.element.1", element.getClass().getName()));
            }
        }
    }
    
    public boolean add(final String s) {
        return s != null && super.add(new Chunk(s, this.font));
    }
    
    @Override
    public boolean add(final Element element) {
        if (element == null) {
            return false;
        }
        try {
            switch (element.type()) {
                case 10: {
                    return this.addChunk((Chunk)element);
                }
                case 11:
                case 12: {
                    final Phrase phrase = (Phrase)element;
                    boolean success = true;
                    for (final Object element2 : phrase) {
                        final Element e = (Element)element2;
                        if (e instanceof Chunk) {
                            success &= this.addChunk((Chunk)e);
                        }
                        else {
                            success &= this.add(e);
                        }
                    }
                    return success;
                }
                case 14:
                case 17:
                case 23:
                case 29:
                case 37:
                case 50:
                case 55:
                case 666: {
                    return super.add(element);
                }
                default: {
                    throw new ClassCastException(String.valueOf(element.type()));
                }
            }
        }
        catch (ClassCastException cce) {
            throw new ClassCastException(MessageLocalization.getComposedMessage("insertion.of.illegal.element.1", cce.getMessage()));
        }
    }
    
    @Override
    public boolean addAll(final Collection<? extends Element> collection) {
        for (final Element e : collection) {
            this.add(e);
        }
        return true;
    }
    
    protected boolean addChunk(final Chunk chunk) {
        Font f = chunk.getFont();
        final String c = chunk.getContent();
        if (this.font != null && !this.font.isStandardFont()) {
            f = this.font.difference(chunk.getFont());
        }
        if (this.size() > 0 && !chunk.hasAttributes()) {
            try {
                final Chunk previous = ((ArrayList<Chunk>)this).get(this.size() - 1);
                final PdfName previousRole = previous.getRole();
                final PdfName chunkRole = chunk.getRole();
                final boolean sameRole = previousRole == null || chunkRole == null || previousRole.equals(chunkRole);
                if (sameRole && !previous.hasAttributes() && !chunk.hasAccessibleAttributes() && !previous.hasAccessibleAttributes() && (f == null || f.compareTo(previous.getFont()) == 0) && !"".equals(previous.getContent().trim()) && !"".equals(c.trim())) {
                    previous.append(c);
                    return true;
                }
            }
            catch (ClassCastException ex) {}
        }
        final Chunk newChunk = new Chunk(c, f);
        newChunk.setAttributes(chunk.getAttributes());
        newChunk.role = chunk.getRole();
        newChunk.accessibleAttributes = chunk.getAccessibleAttributes();
        if (this.hyphenation != null && newChunk.getHyphenation() == null && !newChunk.isEmpty()) {
            newChunk.setHyphenation(this.hyphenation);
        }
        return super.add(newChunk);
    }
    
    protected void addSpecial(final Element object) {
        super.add(object);
    }
    
    public void setLeading(final float fixedLeading, final float multipliedLeading) {
        this.leading = fixedLeading;
        this.multipliedLeading = multipliedLeading;
    }
    
    public void setLeading(final float fixedLeading) {
        this.leading = fixedLeading;
        this.multipliedLeading = 0.0f;
    }
    
    public void setMultipliedLeading(final float multipliedLeading) {
        this.leading = 0.0f;
        this.multipliedLeading = multipliedLeading;
    }
    
    public void setFont(final Font font) {
        this.font = font;
    }
    
    public float getLeading() {
        if (Float.isNaN(this.leading) && this.font != null) {
            return this.font.getCalculatedLeading(1.5f);
        }
        return this.leading;
    }
    
    public float getMultipliedLeading() {
        return this.multipliedLeading;
    }
    
    public float getTotalLeading() {
        final float m = (this.font == null) ? (12.0f * this.multipliedLeading) : this.font.getCalculatedLeading(this.multipliedLeading);
        if (m > 0.0f && !this.hasLeading()) {
            return m;
        }
        return this.getLeading() + m;
    }
    
    public boolean hasLeading() {
        return !Float.isNaN(this.leading);
    }
    
    public Font getFont() {
        return this.font;
    }
    
    public String getContent() {
        final StringBuffer buf = new StringBuffer();
        for (final Chunk c : this.getChunks()) {
            buf.append(c.toString());
        }
        return buf.toString();
    }
    
    @Override
    public boolean isEmpty() {
        switch (this.size()) {
            case 0: {
                return true;
            }
            case 1: {
                final Element element = this.get(0);
                return element.type() == 10 && ((Chunk)element).isEmpty();
            }
            default: {
                return false;
            }
        }
    }
    
    public HyphenationEvent getHyphenation() {
        return this.hyphenation;
    }
    
    public void setHyphenation(final HyphenationEvent hyphenation) {
        this.hyphenation = hyphenation;
    }
    
    public TabSettings getTabSettings() {
        return this.tabSettings;
    }
    
    public void setTabSettings(final TabSettings tabSettings) {
        this.tabSettings = tabSettings;
    }
    
    private Phrase(final boolean dummy) {
        this.leading = Float.NaN;
        this.multipliedLeading = 0.0f;
        this.hyphenation = null;
        this.tabSettings = null;
    }
    
    public static final Phrase getInstance(final String string) {
        return getInstance(16, string, new Font());
    }
    
    public static final Phrase getInstance(final int leading, final String string) {
        return getInstance(leading, string, new Font());
    }
    
    public static final Phrase getInstance(final int leading, String string, final Font font) {
        final Phrase p = new Phrase(true);
        p.setLeading((float)leading);
        p.font = font;
        if (font.getFamily() != Font.FontFamily.SYMBOL && font.getFamily() != Font.FontFamily.ZAPFDINGBATS && font.getBaseFont() == null) {
            int index;
            while ((index = SpecialSymbol.index(string)) > -1) {
                if (index > 0) {
                    final String firstPart = string.substring(0, index);
                    p.add(new Chunk(firstPart, font));
                    string = string.substring(index);
                }
                final Font symbol = new Font(Font.FontFamily.SYMBOL, font.getSize(), font.getStyle(), font.getColor());
                final StringBuffer buf = new StringBuffer();
                buf.append(SpecialSymbol.getCorrespondingSymbol(string.charAt(0)));
                for (string = string.substring(1); SpecialSymbol.index(string) == 0; string = string.substring(1)) {
                    buf.append(SpecialSymbol.getCorrespondingSymbol(string.charAt(0)));
                }
                p.add(new Chunk(buf.toString(), symbol));
            }
        }
        if (string != null && string.length() != 0) {
            p.add(new Chunk(string, font));
        }
        return p;
    }
    
    public boolean trim() {
        while (this.size() > 0) {
            final Element firstChunk = this.get(0);
            if (!(firstChunk instanceof Chunk) || !((Chunk)firstChunk).isWhitespace()) {
                break;
            }
            this.remove(firstChunk);
        }
        while (this.size() > 0) {
            final Element lastChunk = this.get(this.size() - 1);
            if (!(lastChunk instanceof Chunk) || !((Chunk)lastChunk).isWhitespace()) {
                break;
            }
            this.remove(lastChunk);
        }
        return this.size() > 0;
    }
}
