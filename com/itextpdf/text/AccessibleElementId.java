// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import java.io.Serializable;

public class AccessibleElementId implements Comparable<AccessibleElementId>, Serializable
{
    private static int id_counter;
    private int id;
    
    public AccessibleElementId() {
        this.id = 0;
        this.id = ++AccessibleElementId.id_counter;
    }
    
    @Override
    public String toString() {
        return Integer.toString(this.id);
    }
    
    @Override
    public int hashCode() {
        return this.id;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof AccessibleElementId && this.id == ((AccessibleElementId)o).id;
    }
    
    @Override
    public int compareTo(final AccessibleElementId elementId) {
        if (this.id < elementId.id) {
            return -1;
        }
        if (this.id > elementId.id) {
            return 1;
        }
        return 0;
    }
    
    static {
        AccessibleElementId.id_counter = 0;
    }
}
