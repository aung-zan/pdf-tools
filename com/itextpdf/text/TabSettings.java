// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public class TabSettings
{
    public static final float DEFAULT_TAB_INTERVAL = 36.0f;
    private List<TabStop> tabStops;
    private float tabInterval;
    
    public static TabStop getTabStopNewInstance(final float currentPosition, final TabSettings tabSettings) {
        if (tabSettings != null) {
            return tabSettings.getTabStopNewInstance(currentPosition);
        }
        return TabStop.newInstance(currentPosition, 36.0f);
    }
    
    public TabSettings() {
        this.tabStops = new ArrayList<TabStop>();
        this.tabInterval = 36.0f;
    }
    
    public TabSettings(final List<TabStop> tabStops) {
        this.tabStops = new ArrayList<TabStop>();
        this.tabInterval = 36.0f;
        this.tabStops = tabStops;
    }
    
    public TabSettings(final float tabInterval) {
        this.tabStops = new ArrayList<TabStop>();
        this.tabInterval = 36.0f;
        this.tabInterval = tabInterval;
    }
    
    public TabSettings(final List<TabStop> tabStops, final float tabInterval) {
        this.tabStops = new ArrayList<TabStop>();
        this.tabInterval = 36.0f;
        this.tabStops = tabStops;
        this.tabInterval = tabInterval;
    }
    
    public List<TabStop> getTabStops() {
        return this.tabStops;
    }
    
    public void setTabStops(final List<TabStop> tabStops) {
        this.tabStops = tabStops;
    }
    
    public float getTabInterval() {
        return this.tabInterval;
    }
    
    public void setTabInterval(final float tabInterval) {
        this.tabInterval = tabInterval;
    }
    
    public TabStop getTabStopNewInstance(final float currentPosition) {
        TabStop tabStop = null;
        if (this.tabStops != null) {
            for (final TabStop currentTabStop : this.tabStops) {
                if (currentTabStop.getPosition() - currentPosition > 0.001) {
                    tabStop = new TabStop(currentTabStop);
                    break;
                }
            }
        }
        if (tabStop == null) {
            tabStop = TabStop.newInstance(currentPosition, this.tabInterval);
        }
        return tabStop;
    }
}
