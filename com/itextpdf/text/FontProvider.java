// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

public interface FontProvider
{
    boolean isRegistered(final String p0);
    
    Font getFont(final String p0, final String p1, final boolean p2, final float p3, final int p4, final BaseColor p5);
}
