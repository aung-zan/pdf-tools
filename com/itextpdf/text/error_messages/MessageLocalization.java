// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text.error_messages;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import com.itextpdf.text.io.StreamUtil;
import java.io.Reader;
import java.io.IOException;
import java.util.HashMap;

public final class MessageLocalization
{
    private static HashMap<String, String> defaultLanguage;
    private static HashMap<String, String> currentLanguage;
    private static final String BASE_PATH = "com/itextpdf/text/l10n/error/";
    
    private MessageLocalization() {
    }
    
    public static String getMessage(final String key) {
        return getMessage(key, true);
    }
    
    public static String getMessage(final String key, final boolean useDefaultLanguageIfMessageNotFound) {
        HashMap<String, String> cl = MessageLocalization.currentLanguage;
        if (cl != null) {
            final String val = cl.get(key);
            if (val != null) {
                return val;
            }
        }
        if (useDefaultLanguageIfMessageNotFound) {
            cl = MessageLocalization.defaultLanguage;
            final String val = cl.get(key);
            if (val != null) {
                return val;
            }
        }
        return "No message found for " + key;
    }
    
    public static String getComposedMessage(final String key, final int p1) {
        return getComposedMessage(key, String.valueOf(p1), null, null, null);
    }
    
    public static String getComposedMessage(final String key, final Object... param) {
        String msg = getMessage(key);
        if (null != param) {
            int i = 1;
            for (final Object o : param) {
                if (null != o) {
                    msg = msg.replace("{" + i + "}", o.toString());
                }
                ++i;
            }
        }
        return msg;
    }
    
    public static boolean setLanguage(final String language, final String country) throws IOException {
        final HashMap<String, String> lang = getLanguageMessages(language, country);
        if (lang == null) {
            return false;
        }
        MessageLocalization.currentLanguage = lang;
        return true;
    }
    
    public static void setMessages(final Reader r) throws IOException {
        MessageLocalization.currentLanguage = readLanguageStream(r);
    }
    
    private static HashMap<String, String> getLanguageMessages(final String language, final String country) throws IOException {
        if (language == null) {
            throw new IllegalArgumentException("The language cannot be null.");
        }
        InputStream is = null;
        try {
            String file;
            if (country != null) {
                file = language + "_" + country + ".lng";
            }
            else {
                file = language + ".lng";
            }
            is = StreamUtil.getResourceStream("com/itextpdf/text/l10n/error/" + file, new MessageLocalization().getClass().getClassLoader());
            if (is != null) {
                return readLanguageStream(is);
            }
            if (country == null) {
                return null;
            }
            file = language + ".lng";
            is = StreamUtil.getResourceStream("com/itextpdf/text/l10n/error/" + file, new MessageLocalization().getClass().getClassLoader());
            if (is != null) {
                return readLanguageStream(is);
            }
            return null;
        }
        finally {
            try {
                if (null != is) {
                    is.close();
                }
            }
            catch (Exception ex) {}
        }
    }
    
    private static HashMap<String, String> readLanguageStream(final InputStream is) throws IOException {
        return readLanguageStream(new InputStreamReader(is, "UTF-8"));
    }
    
    private static HashMap<String, String> readLanguageStream(final Reader r) throws IOException {
        final HashMap<String, String> lang = new HashMap<String, String>();
        final BufferedReader br = new BufferedReader(r);
        String line;
        while ((line = br.readLine()) != null) {
            final int idxeq = line.indexOf(61);
            if (idxeq < 0) {
                continue;
            }
            final String key = line.substring(0, idxeq).trim();
            if (key.startsWith("#")) {
                continue;
            }
            lang.put(key, line.substring(idxeq + 1));
        }
        return lang;
    }
    
    static {
        MessageLocalization.defaultLanguage = new HashMap<String, String>();
        try {
            MessageLocalization.defaultLanguage = getLanguageMessages("en", null);
        }
        catch (Exception ex) {}
        if (MessageLocalization.defaultLanguage == null) {
            MessageLocalization.defaultLanguage = new HashMap<String, String>();
        }
    }
}
