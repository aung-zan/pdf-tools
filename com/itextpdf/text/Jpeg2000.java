// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import java.io.ByteArrayInputStream;
import com.itextpdf.text.error_messages.MessageLocalization;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.io.InputStream;

public class Jpeg2000 extends Image
{
    public static final int JP2_JP = 1783636000;
    public static final int JP2_IHDR = 1768449138;
    public static final int JPIP_JPIP = 1785751920;
    public static final int JP2_FTYP = 1718909296;
    public static final int JP2_JP2H = 1785737832;
    public static final int JP2_COLR = 1668246642;
    public static final int JP2_JP2C = 1785737827;
    public static final int JP2_URL = 1970433056;
    public static final int JP2_DBTL = 1685348972;
    public static final int JP2_BPCC = 1651532643;
    public static final int JP2_JP2 = 1785737760;
    InputStream inp;
    int boxLength;
    int boxType;
    int numOfComps;
    ArrayList<ColorSpecBox> colorSpecBoxes;
    boolean isJp2;
    byte[] bpcBoxData;
    
    Jpeg2000(final Image image) {
        super(image);
        this.colorSpecBoxes = null;
        this.isJp2 = false;
        if (image instanceof Jpeg2000) {
            final Jpeg2000 jpeg2000 = (Jpeg2000)image;
            this.numOfComps = jpeg2000.numOfComps;
            if (this.colorSpecBoxes != null) {
                this.colorSpecBoxes = (ArrayList<ColorSpecBox>)jpeg2000.colorSpecBoxes.clone();
            }
            this.isJp2 = jpeg2000.isJp2;
            if (this.bpcBoxData != null) {
                this.bpcBoxData = jpeg2000.bpcBoxData.clone();
            }
        }
    }
    
    public Jpeg2000(final URL url) throws BadElementException, IOException {
        super(url);
        this.colorSpecBoxes = null;
        this.isJp2 = false;
        this.processParameters();
    }
    
    public Jpeg2000(final byte[] img) throws BadElementException, IOException {
        super((URL)null);
        this.colorSpecBoxes = null;
        this.isJp2 = false;
        this.rawData = img;
        this.originalData = img;
        this.processParameters();
    }
    
    public Jpeg2000(final byte[] img, final float width, final float height) throws BadElementException, IOException {
        this(img);
        this.scaledWidth = width;
        this.scaledHeight = height;
    }
    
    private int cio_read(final int n) throws IOException {
        int v = 0;
        for (int i = n - 1; i >= 0; --i) {
            v += this.inp.read() << (i << 3);
        }
        return v;
    }
    
    public void jp2_read_boxhdr() throws IOException {
        this.boxLength = this.cio_read(4);
        this.boxType = this.cio_read(4);
        if (this.boxLength == 1) {
            if (this.cio_read(4) != 0) {
                throw new IOException(MessageLocalization.getComposedMessage("cannot.handle.box.sizes.higher.than.2.32", new Object[0]));
            }
            this.boxLength = this.cio_read(4);
            if (this.boxLength == 0) {
                throw new IOException(MessageLocalization.getComposedMessage("unsupported.box.size.eq.eq.0", new Object[0]));
            }
        }
        else if (this.boxLength == 0) {
            throw new ZeroBoxSizeException(MessageLocalization.getComposedMessage("unsupported.box.size.eq.eq.0", new Object[0]));
        }
    }
    
    private void processParameters() throws IOException {
        this.type = 33;
        this.originalType = 8;
        this.inp = null;
        try {
            if (this.rawData == null) {
                this.inp = this.url.openStream();
            }
            else {
                this.inp = new ByteArrayInputStream(this.rawData);
            }
            this.boxLength = this.cio_read(4);
            if (this.boxLength == 12) {
                this.isJp2 = true;
                this.boxType = this.cio_read(4);
                if (1783636000 != this.boxType) {
                    throw new IOException(MessageLocalization.getComposedMessage("expected.jp.marker", new Object[0]));
                }
                if (218793738 != this.cio_read(4)) {
                    throw new IOException(MessageLocalization.getComposedMessage("error.with.jp.marker", new Object[0]));
                }
                this.jp2_read_boxhdr();
                if (1718909296 != this.boxType) {
                    throw new IOException(MessageLocalization.getComposedMessage("expected.ftyp.marker", new Object[0]));
                }
                Utilities.skip(this.inp, this.boxLength - 8);
                this.jp2_read_boxhdr();
                do {
                    if (1785737832 != this.boxType) {
                        if (this.boxType == 1785737827) {
                            throw new IOException(MessageLocalization.getComposedMessage("expected.jp2h.marker", new Object[0]));
                        }
                        Utilities.skip(this.inp, this.boxLength - 8);
                        this.jp2_read_boxhdr();
                    }
                } while (1785737832 != this.boxType);
                this.jp2_read_boxhdr();
                if (1768449138 != this.boxType) {
                    throw new IOException(MessageLocalization.getComposedMessage("expected.ihdr.marker", new Object[0]));
                }
                this.setTop(this.scaledHeight = (float)this.cio_read(4));
                this.setRight(this.scaledWidth = (float)this.cio_read(4));
                this.numOfComps = this.cio_read(2);
                this.bpc = -1;
                this.bpc = this.cio_read(1);
                Utilities.skip(this.inp, 3);
                this.jp2_read_boxhdr();
                if (this.boxType == 1651532643) {
                    this.bpcBoxData = new byte[this.boxLength - 8];
                    this.inp.read(this.bpcBoxData, 0, this.boxLength - 8);
                }
                else if (this.boxType == 1668246642) {
                    do {
                        if (this.colorSpecBoxes == null) {
                            this.colorSpecBoxes = new ArrayList<ColorSpecBox>();
                        }
                        this.colorSpecBoxes.add(this.jp2_read_colr());
                        try {
                            this.jp2_read_boxhdr();
                        }
                        catch (ZeroBoxSizeException ex) {}
                    } while (1668246642 == this.boxType);
                }
            }
            else {
                if (this.boxLength != -11534511) {
                    throw new IOException(MessageLocalization.getComposedMessage("not.a.valid.jpeg2000.file", new Object[0]));
                }
                Utilities.skip(this.inp, 4);
                final int x1 = this.cio_read(4);
                final int y1 = this.cio_read(4);
                final int x2 = this.cio_read(4);
                final int y2 = this.cio_read(4);
                Utilities.skip(this.inp, 16);
                this.colorspace = this.cio_read(2);
                this.bpc = 8;
                this.setTop(this.scaledHeight = (float)(y1 - y2));
                this.setRight(this.scaledWidth = (float)(x1 - x2));
            }
        }
        finally {
            if (this.inp != null) {
                try {
                    this.inp.close();
                }
                catch (Exception ex2) {}
                this.inp = null;
            }
        }
        this.plainWidth = this.getWidth();
        this.plainHeight = this.getHeight();
    }
    
    private ColorSpecBox jp2_read_colr() throws IOException {
        int readBytes = 8;
        final ColorSpecBox colr = new ColorSpecBox();
        for (int i = 0; i < 3; ++i) {
            colr.add(this.cio_read(1));
            ++readBytes;
        }
        if (colr.getMeth() == 1) {
            colr.add(this.cio_read(4));
            readBytes += 4;
        }
        else {
            colr.add(0);
        }
        if (this.boxLength - readBytes > 0) {
            final byte[] colorProfile = new byte[this.boxLength - readBytes];
            this.inp.read(colorProfile, 0, this.boxLength - readBytes);
            colr.setColorProfile(colorProfile);
        }
        return colr;
    }
    
    public int getNumOfComps() {
        return this.numOfComps;
    }
    
    public byte[] getBpcBoxData() {
        return this.bpcBoxData;
    }
    
    public ArrayList<ColorSpecBox> getColorSpecBoxes() {
        return this.colorSpecBoxes;
    }
    
    public boolean isJp2() {
        return this.isJp2;
    }
    
    public static class ColorSpecBox extends ArrayList<Integer>
    {
        private byte[] colorProfile;
        
        public int getMeth() {
            return this.get(0);
        }
        
        public int getPrec() {
            return this.get(1);
        }
        
        public int getApprox() {
            return this.get(2);
        }
        
        public int getEnumCs() {
            return this.get(3);
        }
        
        public byte[] getColorProfile() {
            return this.colorProfile;
        }
        
        void setColorProfile(final byte[] colorProfile) {
            this.colorProfile = colorProfile;
        }
    }
    
    private class ZeroBoxSizeException extends IOException
    {
        public ZeroBoxSizeException() {
        }
        
        public ZeroBoxSizeException(final String s) {
            super(s);
        }
    }
}
