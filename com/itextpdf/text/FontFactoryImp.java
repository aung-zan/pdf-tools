// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.text;

import com.itextpdf.text.log.LoggerFactory;
import java.util.Set;
import java.io.File;
import com.itextpdf.text.log.Level;
import com.itextpdf.text.pdf.BaseFont;
import java.util.Iterator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import com.itextpdf.text.log.Logger;

public class FontFactoryImp implements FontProvider
{
    private static final Logger LOGGER;
    private final Hashtable<String, String> trueTypeFonts;
    private static String[] TTFamilyOrder;
    private final Hashtable<String, ArrayList<String>> fontFamilies;
    public String defaultEncoding;
    public boolean defaultEmbedding;
    
    public FontFactoryImp() {
        this.trueTypeFonts = new Hashtable<String, String>();
        this.fontFamilies = new Hashtable<String, ArrayList<String>>();
        this.defaultEncoding = "Cp1252";
        this.defaultEmbedding = false;
        this.trueTypeFonts.put("Courier".toLowerCase(), "Courier");
        this.trueTypeFonts.put("Courier-Bold".toLowerCase(), "Courier-Bold");
        this.trueTypeFonts.put("Courier-Oblique".toLowerCase(), "Courier-Oblique");
        this.trueTypeFonts.put("Courier-BoldOblique".toLowerCase(), "Courier-BoldOblique");
        this.trueTypeFonts.put("Helvetica".toLowerCase(), "Helvetica");
        this.trueTypeFonts.put("Helvetica-Bold".toLowerCase(), "Helvetica-Bold");
        this.trueTypeFonts.put("Helvetica-Oblique".toLowerCase(), "Helvetica-Oblique");
        this.trueTypeFonts.put("Helvetica-BoldOblique".toLowerCase(), "Helvetica-BoldOblique");
        this.trueTypeFonts.put("Symbol".toLowerCase(), "Symbol");
        this.trueTypeFonts.put("Times-Roman".toLowerCase(), "Times-Roman");
        this.trueTypeFonts.put("Times-Bold".toLowerCase(), "Times-Bold");
        this.trueTypeFonts.put("Times-Italic".toLowerCase(), "Times-Italic");
        this.trueTypeFonts.put("Times-BoldItalic".toLowerCase(), "Times-BoldItalic");
        this.trueTypeFonts.put("ZapfDingbats".toLowerCase(), "ZapfDingbats");
        ArrayList<String> tmp = new ArrayList<String>();
        tmp.add("Courier");
        tmp.add("Courier-Bold");
        tmp.add("Courier-Oblique");
        tmp.add("Courier-BoldOblique");
        this.fontFamilies.put("Courier".toLowerCase(), tmp);
        tmp = new ArrayList<String>();
        tmp.add("Helvetica");
        tmp.add("Helvetica-Bold");
        tmp.add("Helvetica-Oblique");
        tmp.add("Helvetica-BoldOblique");
        this.fontFamilies.put("Helvetica".toLowerCase(), tmp);
        tmp = new ArrayList<String>();
        tmp.add("Symbol");
        this.fontFamilies.put("Symbol".toLowerCase(), tmp);
        tmp = new ArrayList<String>();
        tmp.add("Times-Roman");
        tmp.add("Times-Bold");
        tmp.add("Times-Italic");
        tmp.add("Times-BoldItalic");
        this.fontFamilies.put("Times".toLowerCase(), tmp);
        this.fontFamilies.put("Times-Roman".toLowerCase(), tmp);
        tmp = new ArrayList<String>();
        tmp.add("ZapfDingbats");
        this.fontFamilies.put("ZapfDingbats".toLowerCase(), tmp);
    }
    
    @Override
    public Font getFont(final String fontname, final String encoding, final boolean embedded, final float size, final int style, final BaseColor color) {
        return this.getFont(fontname, encoding, embedded, size, style, color, true);
    }
    
    public Font getFont(String fontname, final String encoding, final boolean embedded, final float size, int style, final BaseColor color, final boolean cached) {
        if (fontname == null) {
            return new Font(Font.FontFamily.UNDEFINED, size, style, color);
        }
        final String lowercasefontname = fontname.toLowerCase();
        final ArrayList<String> tmp = this.fontFamilies.get(lowercasefontname);
        if (tmp != null) {
            synchronized (tmp) {
                final int s = (style == -1) ? 0 : style;
                int fs = 0;
                boolean found = false;
                for (final String f : tmp) {
                    final String lcf = f.toLowerCase();
                    fs = 0;
                    if (lcf.indexOf("bold") != -1) {
                        fs |= 0x1;
                    }
                    if (lcf.indexOf("italic") != -1 || lcf.indexOf("oblique") != -1) {
                        fs |= 0x2;
                    }
                    if ((s & 0x3) == fs) {
                        fontname = f;
                        found = true;
                        break;
                    }
                }
                if (style != -1 && found) {
                    style &= ~fs;
                }
            }
        }
        BaseFont basefont = null;
        try {
            basefont = this.getBaseFont(fontname, encoding, embedded, cached);
            if (basefont == null) {
                return new Font(Font.FontFamily.UNDEFINED, size, style, color);
            }
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
        catch (IOException ioe) {
            return new Font(Font.FontFamily.UNDEFINED, size, style, color);
        }
        catch (NullPointerException npe) {
            return new Font(Font.FontFamily.UNDEFINED, size, style, color);
        }
        return new Font(basefont, size, style, color);
    }
    
    protected BaseFont getBaseFont(String fontname, final String encoding, final boolean embedded, final boolean cached) throws IOException, DocumentException {
        BaseFont basefont = null;
        try {
            basefont = BaseFont.createFont(fontname, encoding, embedded, cached, null, null, true);
        }
        catch (DocumentException ex) {}
        if (basefont == null) {
            fontname = this.trueTypeFonts.get(fontname.toLowerCase());
            if (fontname != null) {
                basefont = BaseFont.createFont(fontname, encoding, embedded, cached, null, null);
            }
        }
        return basefont;
    }
    
    public Font getFont(final String fontname, final String encoding, final boolean embedded, final float size, final int style) {
        return this.getFont(fontname, encoding, embedded, size, style, null);
    }
    
    public Font getFont(final String fontname, final String encoding, final boolean embedded, final float size) {
        return this.getFont(fontname, encoding, embedded, size, -1, null);
    }
    
    public Font getFont(final String fontname, final String encoding, final boolean embedded) {
        return this.getFont(fontname, encoding, embedded, -1.0f, -1, null);
    }
    
    public Font getFont(final String fontname, final String encoding, final float size, final int style, final BaseColor color) {
        return this.getFont(fontname, encoding, this.defaultEmbedding, size, style, color);
    }
    
    public Font getFont(final String fontname, final String encoding, final float size, final int style) {
        return this.getFont(fontname, encoding, this.defaultEmbedding, size, style, null);
    }
    
    public Font getFont(final String fontname, final String encoding, final float size) {
        return this.getFont(fontname, encoding, this.defaultEmbedding, size, -1, null);
    }
    
    public Font getFont(final String fontname, final float size, final BaseColor color) {
        return this.getFont(fontname, this.defaultEncoding, this.defaultEmbedding, size, -1, color);
    }
    
    public Font getFont(final String fontname, final String encoding) {
        return this.getFont(fontname, encoding, this.defaultEmbedding, -1.0f, -1, null);
    }
    
    public Font getFont(final String fontname, final float size, final int style, final BaseColor color) {
        return this.getFont(fontname, this.defaultEncoding, this.defaultEmbedding, size, style, color);
    }
    
    public Font getFont(final String fontname, final float size, final int style) {
        return this.getFont(fontname, this.defaultEncoding, this.defaultEmbedding, size, style, null);
    }
    
    public Font getFont(final String fontname, final float size) {
        return this.getFont(fontname, this.defaultEncoding, this.defaultEmbedding, size, -1, null);
    }
    
    public Font getFont(final String fontname) {
        return this.getFont(fontname, this.defaultEncoding, this.defaultEmbedding, -1.0f, -1, null);
    }
    
    public void registerFamily(final String familyName, final String fullName, final String path) {
        if (path != null) {
            this.trueTypeFonts.put(fullName, path);
        }
        ArrayList<String> tmp;
        synchronized (this.fontFamilies) {
            tmp = this.fontFamilies.get(familyName);
            if (tmp == null) {
                tmp = new ArrayList<String>();
                this.fontFamilies.put(familyName, tmp);
            }
        }
        synchronized (tmp) {
            if (!tmp.contains(fullName)) {
                final int fullNameLength = fullName.length();
                boolean inserted = false;
                for (int j = 0; j < tmp.size(); ++j) {
                    if (tmp.get(j).length() >= fullNameLength) {
                        tmp.add(j, fullName);
                        inserted = true;
                        break;
                    }
                }
                if (!inserted) {
                    tmp.add(fullName);
                    String newFullName = fullName.toLowerCase();
                    if (newFullName.endsWith("regular")) {
                        newFullName = newFullName.substring(0, newFullName.length() - 7).trim();
                        tmp.add(0, fullName.substring(0, newFullName.length()));
                    }
                }
            }
        }
    }
    
    public void register(final String path) {
        this.register(path, null);
    }
    
    public void register(final String path, final String alias) {
        try {
            if (path.toLowerCase().endsWith(".ttf") || path.toLowerCase().endsWith(".otf") || path.toLowerCase().indexOf(".ttc,") > 0) {
                final Object[] allNames = BaseFont.getAllFontNames(path, "Cp1252", null);
                this.trueTypeFonts.put(((String)allNames[0]).toLowerCase(), path);
                if (alias != null) {
                    final String lcAlias = alias.toLowerCase();
                    this.trueTypeFonts.put(lcAlias, path);
                    if (lcAlias.endsWith("regular")) {
                        this.saveCopyOfRegularFont(lcAlias, path);
                    }
                }
                final String[][] arr$;
                String[][] names = arr$ = (String[][])allNames[2];
                for (final String[] name : arr$) {
                    final String lcName = name[3].toLowerCase();
                    this.trueTypeFonts.put(lcName, path);
                    if (lcName.endsWith("regular")) {
                        this.saveCopyOfRegularFont(lcName, path);
                    }
                }
                String fullName = null;
                String familyName = null;
                names = (String[][])allNames[1];
                for (int k = 0; k < FontFactoryImp.TTFamilyOrder.length; k += 3) {
                    for (final String[] name2 : names) {
                        if (FontFactoryImp.TTFamilyOrder[k].equals(name2[0]) && FontFactoryImp.TTFamilyOrder[k + 1].equals(name2[1]) && FontFactoryImp.TTFamilyOrder[k + 2].equals(name2[2])) {
                            familyName = name2[3].toLowerCase();
                            k = FontFactoryImp.TTFamilyOrder.length;
                            break;
                        }
                    }
                }
                if (familyName != null) {
                    String lastName = "";
                    final String[][] arr$2;
                    names = (arr$2 = (String[][])allNames[2]);
                    for (final String[] name2 : arr$2) {
                        for (int i = 0; i < FontFactoryImp.TTFamilyOrder.length; i += 3) {
                            if (FontFactoryImp.TTFamilyOrder[i].equals(name2[0]) && FontFactoryImp.TTFamilyOrder[i + 1].equals(name2[1]) && FontFactoryImp.TTFamilyOrder[i + 2].equals(name2[2])) {
                                fullName = name2[3];
                                if (!fullName.equals(lastName)) {
                                    lastName = fullName;
                                    this.registerFamily(familyName, fullName, null);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else if (path.toLowerCase().endsWith(".ttc")) {
                if (alias != null) {
                    FontFactoryImp.LOGGER.error("You can't define an alias for a true type collection.");
                }
                final String[] names2 = BaseFont.enumerateTTCNames(path);
                for (int j = 0; j < names2.length; ++j) {
                    this.register(path + "," + j);
                }
            }
            else if (path.toLowerCase().endsWith(".afm") || path.toLowerCase().endsWith(".pfm")) {
                final BaseFont bf = BaseFont.createFont(path, "Cp1252", false);
                final String fullName2 = bf.getFullFontName()[0][3].toLowerCase();
                final String familyName2 = bf.getFamilyFontName()[0][3].toLowerCase();
                final String psName = bf.getPostscriptFontName().toLowerCase();
                this.registerFamily(familyName2, fullName2, null);
                this.trueTypeFonts.put(psName, path);
                this.trueTypeFonts.put(fullName2, path);
            }
            if (FontFactoryImp.LOGGER.isLogging(Level.TRACE)) {
                FontFactoryImp.LOGGER.trace(String.format("Registered %s", path));
            }
        }
        catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
        catch (IOException ioe) {
            throw new ExceptionConverter(ioe);
        }
    }
    
    protected boolean saveCopyOfRegularFont(final String regularFontName, final String path) {
        final String alias = regularFontName.substring(0, regularFontName.length() - 7).trim();
        if (!this.trueTypeFonts.containsKey(alias)) {
            this.trueTypeFonts.put(alias, path);
            return true;
        }
        return false;
    }
    
    public int registerDirectory(final String dir) {
        return this.registerDirectory(dir, false);
    }
    
    public int registerDirectory(final String dir, final boolean scanSubdirectories) {
        if (FontFactoryImp.LOGGER.isLogging(Level.DEBUG)) {
            FontFactoryImp.LOGGER.debug(String.format("Registering directory %s, looking for fonts", dir));
        }
        int count = 0;
        try {
            File file = new File(dir);
            if (!file.exists() || !file.isDirectory()) {
                return 0;
            }
            final String[] files = file.list();
            if (files == null) {
                return 0;
            }
            for (int k = 0; k < files.length; ++k) {
                try {
                    file = new File(dir, files[k]);
                    if (file.isDirectory()) {
                        if (scanSubdirectories) {
                            count += this.registerDirectory(file.getAbsolutePath(), true);
                        }
                    }
                    else {
                        final String name = file.getPath();
                        final String suffix = (name.length() < 4) ? null : name.substring(name.length() - 4).toLowerCase();
                        if (".afm".equals(suffix) || ".pfm".equals(suffix)) {
                            final File pfb = new File(name.substring(0, name.length() - 4) + ".pfb");
                            if (pfb.exists()) {
                                this.register(name, null);
                                ++count;
                            }
                        }
                        else if (".ttf".equals(suffix) || ".otf".equals(suffix) || ".ttc".equals(suffix)) {
                            this.register(name, null);
                            ++count;
                        }
                    }
                }
                catch (Exception ex) {}
            }
        }
        catch (Exception ex2) {}
        return count;
    }
    
    public int registerDirectories() {
        int count = 0;
        final String windir = System.getenv("windir");
        final String fileseparator = System.getProperty("file.separator");
        if (windir != null && fileseparator != null) {
            count += this.registerDirectory(windir + fileseparator + "fonts");
        }
        count += this.registerDirectory("/usr/share/X11/fonts", true);
        count += this.registerDirectory("/usr/X/lib/X11/fonts", true);
        count += this.registerDirectory("/usr/openwin/lib/X11/fonts", true);
        count += this.registerDirectory("/usr/share/fonts", true);
        count += this.registerDirectory("/usr/X11R6/lib/X11/fonts", true);
        count += this.registerDirectory("/Library/Fonts");
        count += this.registerDirectory("/System/Library/Fonts");
        return count;
    }
    
    public Set<String> getRegisteredFonts() {
        return this.trueTypeFonts.keySet();
    }
    
    public Set<String> getRegisteredFamilies() {
        return this.fontFamilies.keySet();
    }
    
    @Override
    public boolean isRegistered(final String fontname) {
        return this.trueTypeFonts.containsKey(fontname.toLowerCase());
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(FontFactoryImp.class);
        FontFactoryImp.TTFamilyOrder = new String[] { "3", "1", "1033", "3", "0", "1033", "1", "0", "0", "0", "3", "0" };
    }
}
