// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom.misc;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.HashMap;
import java.util.Map;

public class RenderingHints implements Map<Object, Object>, Cloneable
{
    public static final Key KEY_ALPHA_INTERPOLATION;
    public static final Object VALUE_ALPHA_INTERPOLATION_DEFAULT;
    public static final Object VALUE_ALPHA_INTERPOLATION_SPEED;
    public static final Object VALUE_ALPHA_INTERPOLATION_QUALITY;
    public static final Key KEY_ANTIALIASING;
    public static final Object VALUE_ANTIALIAS_DEFAULT;
    public static final Object VALUE_ANTIALIAS_ON;
    public static final Object VALUE_ANTIALIAS_OFF;
    public static final Key KEY_COLOR_RENDERING;
    public static final Object VALUE_COLOR_RENDER_DEFAULT;
    public static final Object VALUE_COLOR_RENDER_SPEED;
    public static final Object VALUE_COLOR_RENDER_QUALITY;
    public static final Key KEY_DITHERING;
    public static final Object VALUE_DITHER_DEFAULT;
    public static final Object VALUE_DITHER_DISABLE;
    public static final Object VALUE_DITHER_ENABLE;
    public static final Key KEY_FRACTIONALMETRICS;
    public static final Object VALUE_FRACTIONALMETRICS_DEFAULT;
    public static final Object VALUE_FRACTIONALMETRICS_ON;
    public static final Object VALUE_FRACTIONALMETRICS_OFF;
    public static final Key KEY_INTERPOLATION;
    public static final Object VALUE_INTERPOLATION_BICUBIC;
    public static final Object VALUE_INTERPOLATION_BILINEAR;
    public static final Object VALUE_INTERPOLATION_NEAREST_NEIGHBOR;
    public static final Key KEY_RENDERING;
    public static final Object VALUE_RENDER_DEFAULT;
    public static final Object VALUE_RENDER_SPEED;
    public static final Object VALUE_RENDER_QUALITY;
    public static final Key KEY_STROKE_CONTROL;
    public static final Object VALUE_STROKE_DEFAULT;
    public static final Object VALUE_STROKE_NORMALIZE;
    public static final Object VALUE_STROKE_PURE;
    public static final Key KEY_TEXT_ANTIALIASING;
    public static final Object VALUE_TEXT_ANTIALIAS_DEFAULT;
    public static final Object VALUE_TEXT_ANTIALIAS_ON;
    public static final Object VALUE_TEXT_ANTIALIAS_OFF;
    private HashMap<Object, Object> map;
    
    public RenderingHints(final Map<Key, ?> map) {
        this.map = new HashMap<Object, Object>();
        if (map != null) {
            this.putAll(map);
        }
    }
    
    public RenderingHints(final Key key, final Object value) {
        this.map = new HashMap<Object, Object>();
        this.put(key, value);
    }
    
    public void add(final RenderingHints hints) {
        this.map.putAll(hints.map);
    }
    
    @Override
    public Object put(final Object key, final Object value) {
        if (!((Key)key).isCompatibleValue(value)) {
            throw new IllegalArgumentException();
        }
        return this.map.put(key, value);
    }
    
    @Override
    public Object remove(final Object key) {
        return this.map.remove(key);
    }
    
    @Override
    public Object get(final Object key) {
        return this.map.get(key);
    }
    
    @Override
    public Set<Object> keySet() {
        return this.map.keySet();
    }
    
    @Override
    public Set<Entry<Object, Object>> entrySet() {
        return this.map.entrySet();
    }
    
    @Override
    public void putAll(final Map<?, ?> m) {
        if (m instanceof RenderingHints) {
            this.map.putAll(((RenderingHints)m).map);
        }
        else {
            final Set<?> entries = m.entrySet();
            if (entries != null) {
                for (final Entry<?, ?> entry : entries) {
                    final Key key = (Key)entry.getKey();
                    final Object val = entry.getValue();
                    this.put(key, val);
                }
            }
        }
    }
    
    @Override
    public Collection<Object> values() {
        return this.map.values();
    }
    
    @Override
    public boolean containsValue(final Object value) {
        return this.map.containsValue(value);
    }
    
    @Override
    public boolean containsKey(final Object key) {
        if (key == null) {
            throw new NullPointerException();
        }
        return this.map.containsKey(key);
    }
    
    @Override
    public boolean isEmpty() {
        return this.map.isEmpty();
    }
    
    @Override
    public void clear() {
        this.map.clear();
    }
    
    @Override
    public int size() {
        return this.map.size();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof Map)) {
            return false;
        }
        final Map<?, ?> m = (Map<?, ?>)o;
        final Set<?> keys = this.keySet();
        if (!keys.equals(m.keySet())) {
            return false;
        }
        for (final Key key : keys) {
            final Object v1 = this.get(key);
            final Object v2 = m.get(key);
            if (v1 == null) {
                if (v2 == null) {
                    continue;
                }
                return false;
            }
            else {
                if (!v1.equals(v2)) {
                    return false;
                }
                continue;
            }
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        return this.map.hashCode();
    }
    
    public Object clone() {
        final RenderingHints clone = new RenderingHints(null);
        clone.map = (HashMap<Object, Object>)this.map.clone();
        return clone;
    }
    
    @Override
    public String toString() {
        return "RenderingHints[" + this.map.toString() + "]";
    }
    
    static {
        KEY_ALPHA_INTERPOLATION = new KeyImpl(1);
        VALUE_ALPHA_INTERPOLATION_DEFAULT = new KeyValue(RenderingHints.KEY_ALPHA_INTERPOLATION);
        VALUE_ALPHA_INTERPOLATION_SPEED = new KeyValue(RenderingHints.KEY_ALPHA_INTERPOLATION);
        VALUE_ALPHA_INTERPOLATION_QUALITY = new KeyValue(RenderingHints.KEY_ALPHA_INTERPOLATION);
        KEY_ANTIALIASING = new KeyImpl(2);
        VALUE_ANTIALIAS_DEFAULT = new KeyValue(RenderingHints.KEY_ANTIALIASING);
        VALUE_ANTIALIAS_ON = new KeyValue(RenderingHints.KEY_ANTIALIASING);
        VALUE_ANTIALIAS_OFF = new KeyValue(RenderingHints.KEY_ANTIALIASING);
        KEY_COLOR_RENDERING = new KeyImpl(3);
        VALUE_COLOR_RENDER_DEFAULT = new KeyValue(RenderingHints.KEY_COLOR_RENDERING);
        VALUE_COLOR_RENDER_SPEED = new KeyValue(RenderingHints.KEY_COLOR_RENDERING);
        VALUE_COLOR_RENDER_QUALITY = new KeyValue(RenderingHints.KEY_COLOR_RENDERING);
        KEY_DITHERING = new KeyImpl(4);
        VALUE_DITHER_DEFAULT = new KeyValue(RenderingHints.KEY_DITHERING);
        VALUE_DITHER_DISABLE = new KeyValue(RenderingHints.KEY_DITHERING);
        VALUE_DITHER_ENABLE = new KeyValue(RenderingHints.KEY_DITHERING);
        KEY_FRACTIONALMETRICS = new KeyImpl(5);
        VALUE_FRACTIONALMETRICS_DEFAULT = new KeyValue(RenderingHints.KEY_FRACTIONALMETRICS);
        VALUE_FRACTIONALMETRICS_ON = new KeyValue(RenderingHints.KEY_FRACTIONALMETRICS);
        VALUE_FRACTIONALMETRICS_OFF = new KeyValue(RenderingHints.KEY_FRACTIONALMETRICS);
        KEY_INTERPOLATION = new KeyImpl(6);
        VALUE_INTERPOLATION_BICUBIC = new KeyValue(RenderingHints.KEY_INTERPOLATION);
        VALUE_INTERPOLATION_BILINEAR = new KeyValue(RenderingHints.KEY_INTERPOLATION);
        VALUE_INTERPOLATION_NEAREST_NEIGHBOR = new KeyValue(RenderingHints.KEY_INTERPOLATION);
        KEY_RENDERING = new KeyImpl(7);
        VALUE_RENDER_DEFAULT = new KeyValue(RenderingHints.KEY_RENDERING);
        VALUE_RENDER_SPEED = new KeyValue(RenderingHints.KEY_RENDERING);
        VALUE_RENDER_QUALITY = new KeyValue(RenderingHints.KEY_RENDERING);
        KEY_STROKE_CONTROL = new KeyImpl(8);
        VALUE_STROKE_DEFAULT = new KeyValue(RenderingHints.KEY_STROKE_CONTROL);
        VALUE_STROKE_NORMALIZE = new KeyValue(RenderingHints.KEY_STROKE_CONTROL);
        VALUE_STROKE_PURE = new KeyValue(RenderingHints.KEY_STROKE_CONTROL);
        KEY_TEXT_ANTIALIASING = new KeyImpl(9);
        VALUE_TEXT_ANTIALIAS_DEFAULT = new KeyValue(RenderingHints.KEY_TEXT_ANTIALIASING);
        VALUE_TEXT_ANTIALIAS_ON = new KeyValue(RenderingHints.KEY_TEXT_ANTIALIASING);
        VALUE_TEXT_ANTIALIAS_OFF = new KeyValue(RenderingHints.KEY_TEXT_ANTIALIASING);
    }
    
    public abstract static class Key
    {
        private final int key;
        
        protected Key(final int key) {
            this.key = key;
        }
        
        @Override
        public final boolean equals(final Object o) {
            return this == o;
        }
        
        @Override
        public final int hashCode() {
            return System.identityHashCode(this);
        }
        
        protected final int intKey() {
            return this.key;
        }
        
        public abstract boolean isCompatibleValue(final Object p0);
    }
    
    private static class KeyImpl extends Key
    {
        protected KeyImpl(final int key) {
            super(key);
        }
        
        @Override
        public boolean isCompatibleValue(final Object val) {
            return val instanceof KeyValue && ((KeyValue)val).key == this;
        }
    }
    
    private static class KeyValue
    {
        private final Key key;
        
        protected KeyValue(final Key key) {
            this.key = key;
        }
    }
}
