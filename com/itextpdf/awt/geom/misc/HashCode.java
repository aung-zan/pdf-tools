// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom.misc;

public final class HashCode
{
    public static final int EMPTY_HASH_CODE = 1;
    private int hashCode;
    
    public HashCode() {
        this.hashCode = 1;
    }
    
    @Override
    public final int hashCode() {
        return this.hashCode;
    }
    
    public static int combine(final int hashCode, final boolean value) {
        final int v = value ? 1231 : 1237;
        return combine(hashCode, v);
    }
    
    public static int combine(final int hashCode, final long value) {
        final int v = (int)(value ^ value >>> 32);
        return combine(hashCode, v);
    }
    
    public static int combine(final int hashCode, final float value) {
        final int v = Float.floatToIntBits(value);
        return combine(hashCode, v);
    }
    
    public static int combine(final int hashCode, final double value) {
        final long v = Double.doubleToLongBits(value);
        return combine(hashCode, v);
    }
    
    public static int combine(final int hashCode, final Object value) {
        return combine(hashCode, value.hashCode());
    }
    
    public static int combine(final int hashCode, final int value) {
        return 31 * hashCode + value;
    }
    
    public final HashCode append(final int value) {
        this.hashCode = combine(this.hashCode, value);
        return this;
    }
    
    public final HashCode append(final long value) {
        this.hashCode = combine(this.hashCode, value);
        return this;
    }
    
    public final HashCode append(final float value) {
        this.hashCode = combine(this.hashCode, value);
        return this;
    }
    
    public final HashCode append(final double value) {
        this.hashCode = combine(this.hashCode, value);
        return this;
    }
    
    public final HashCode append(final boolean value) {
        this.hashCode = combine(this.hashCode, value);
        return this;
    }
    
    public final HashCode append(final Object value) {
        this.hashCode = combine(this.hashCode, value);
        return this;
    }
}
