// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

import java.util.NoSuchElementException;
import com.itextpdf.awt.geom.misc.Messages;
import com.itextpdf.awt.geom.gl.Crossing;

public abstract class CubicCurve2D implements Shape, Cloneable
{
    protected CubicCurve2D() {
    }
    
    public abstract double getX1();
    
    public abstract double getY1();
    
    public abstract Point2D getP1();
    
    public abstract double getCtrlX1();
    
    public abstract double getCtrlY1();
    
    public abstract Point2D getCtrlP1();
    
    public abstract double getCtrlX2();
    
    public abstract double getCtrlY2();
    
    public abstract Point2D getCtrlP2();
    
    public abstract double getX2();
    
    public abstract double getY2();
    
    public abstract Point2D getP2();
    
    public abstract void setCurve(final double p0, final double p1, final double p2, final double p3, final double p4, final double p5, final double p6, final double p7);
    
    public void setCurve(final Point2D p1, final Point2D cp1, final Point2D cp2, final Point2D p2) {
        this.setCurve(p1.getX(), p1.getY(), cp1.getX(), cp1.getY(), cp2.getX(), cp2.getY(), p2.getX(), p2.getY());
    }
    
    public void setCurve(final double[] coords, final int offset) {
        this.setCurve(coords[offset + 0], coords[offset + 1], coords[offset + 2], coords[offset + 3], coords[offset + 4], coords[offset + 5], coords[offset + 6], coords[offset + 7]);
    }
    
    public void setCurve(final Point2D[] points, final int offset) {
        this.setCurve(points[offset + 0].getX(), points[offset + 0].getY(), points[offset + 1].getX(), points[offset + 1].getY(), points[offset + 2].getX(), points[offset + 2].getY(), points[offset + 3].getX(), points[offset + 3].getY());
    }
    
    public void setCurve(final CubicCurve2D curve) {
        this.setCurve(curve.getX1(), curve.getY1(), curve.getCtrlX1(), curve.getCtrlY1(), curve.getCtrlX2(), curve.getCtrlY2(), curve.getX2(), curve.getY2());
    }
    
    public double getFlatnessSq() {
        return getFlatnessSq(this.getX1(), this.getY1(), this.getCtrlX1(), this.getCtrlY1(), this.getCtrlX2(), this.getCtrlY2(), this.getX2(), this.getY2());
    }
    
    public static double getFlatnessSq(final double x1, final double y1, final double ctrlx1, final double ctrly1, final double ctrlx2, final double ctrly2, final double x2, final double y2) {
        return Math.max(Line2D.ptSegDistSq(x1, y1, x2, y2, ctrlx1, ctrly1), Line2D.ptSegDistSq(x1, y1, x2, y2, ctrlx2, ctrly2));
    }
    
    public static double getFlatnessSq(final double[] coords, final int offset) {
        return getFlatnessSq(coords[offset + 0], coords[offset + 1], coords[offset + 2], coords[offset + 3], coords[offset + 4], coords[offset + 5], coords[offset + 6], coords[offset + 7]);
    }
    
    public double getFlatness() {
        return getFlatness(this.getX1(), this.getY1(), this.getCtrlX1(), this.getCtrlY1(), this.getCtrlX2(), this.getCtrlY2(), this.getX2(), this.getY2());
    }
    
    public static double getFlatness(final double x1, final double y1, final double ctrlx1, final double ctrly1, final double ctrlx2, final double ctrly2, final double x2, final double y2) {
        return Math.sqrt(getFlatnessSq(x1, y1, ctrlx1, ctrly1, ctrlx2, ctrly2, x2, y2));
    }
    
    public static double getFlatness(final double[] coords, final int offset) {
        return getFlatness(coords[offset + 0], coords[offset + 1], coords[offset + 2], coords[offset + 3], coords[offset + 4], coords[offset + 5], coords[offset + 6], coords[offset + 7]);
    }
    
    public void subdivide(final CubicCurve2D left, final CubicCurve2D right) {
        subdivide(this, left, right);
    }
    
    public static void subdivide(final CubicCurve2D src, final CubicCurve2D left, final CubicCurve2D right) {
        final double x1 = src.getX1();
        final double y1 = src.getY1();
        double cx1 = src.getCtrlX1();
        double cy1 = src.getCtrlY1();
        double cx2 = src.getCtrlX2();
        double cy2 = src.getCtrlY2();
        final double x2 = src.getX2();
        final double y2 = src.getY2();
        double cx3 = (cx1 + cx2) / 2.0;
        double cy3 = (cy1 + cy2) / 2.0;
        cx1 = (x1 + cx1) / 2.0;
        cy1 = (y1 + cy1) / 2.0;
        cx2 = (x2 + cx2) / 2.0;
        cy2 = (y2 + cy2) / 2.0;
        final double ax = (cx1 + cx3) / 2.0;
        final double ay = (cy1 + cy3) / 2.0;
        final double bx = (cx2 + cx3) / 2.0;
        final double by = (cy2 + cy3) / 2.0;
        cx3 = (ax + bx) / 2.0;
        cy3 = (ay + by) / 2.0;
        if (left != null) {
            left.setCurve(x1, y1, cx1, cy1, ax, ay, cx3, cy3);
        }
        if (right != null) {
            right.setCurve(cx3, cy3, bx, by, cx2, cy2, x2, y2);
        }
    }
    
    public static void subdivide(final double[] src, final int srcOff, final double[] left, final int leftOff, final double[] right, final int rightOff) {
        final double x1 = src[srcOff + 0];
        final double y1 = src[srcOff + 1];
        double cx1 = src[srcOff + 2];
        double cy1 = src[srcOff + 3];
        double cx2 = src[srcOff + 4];
        double cy2 = src[srcOff + 5];
        final double x2 = src[srcOff + 6];
        final double y2 = src[srcOff + 7];
        double cx3 = (cx1 + cx2) / 2.0;
        double cy3 = (cy1 + cy2) / 2.0;
        cx1 = (x1 + cx1) / 2.0;
        cy1 = (y1 + cy1) / 2.0;
        cx2 = (x2 + cx2) / 2.0;
        cy2 = (y2 + cy2) / 2.0;
        final double ax = (cx1 + cx3) / 2.0;
        final double ay = (cy1 + cy3) / 2.0;
        final double bx = (cx2 + cx3) / 2.0;
        final double by = (cy2 + cy3) / 2.0;
        cx3 = (ax + bx) / 2.0;
        cy3 = (ay + by) / 2.0;
        if (left != null) {
            left[leftOff + 0] = x1;
            left[leftOff + 1] = y1;
            left[leftOff + 2] = cx1;
            left[leftOff + 3] = cy1;
            left[leftOff + 4] = ax;
            left[leftOff + 5] = ay;
            left[leftOff + 6] = cx3;
            left[leftOff + 7] = cy3;
        }
        if (right != null) {
            right[rightOff + 0] = cx3;
            right[rightOff + 1] = cy3;
            right[rightOff + 2] = bx;
            right[rightOff + 3] = by;
            right[rightOff + 4] = cx2;
            right[rightOff + 5] = cy2;
            right[rightOff + 6] = x2;
            right[rightOff + 7] = y2;
        }
    }
    
    public static int solveCubic(final double[] eqn) {
        return solveCubic(eqn, eqn);
    }
    
    public static int solveCubic(final double[] eqn, final double[] res) {
        return Crossing.solveCubic(eqn, res);
    }
    
    @Override
    public boolean contains(final double px, final double py) {
        return Crossing.isInsideEvenOdd(Crossing.crossShape(this, px, py));
    }
    
    @Override
    public boolean contains(final double rx, final double ry, final double rw, final double rh) {
        final int cross = Crossing.intersectShape(this, rx, ry, rw, rh);
        return cross != 255 && Crossing.isInsideEvenOdd(cross);
    }
    
    @Override
    public boolean intersects(final double rx, final double ry, final double rw, final double rh) {
        final int cross = Crossing.intersectShape(this, rx, ry, rw, rh);
        return cross == 255 || Crossing.isInsideEvenOdd(cross);
    }
    
    @Override
    public boolean contains(final Point2D p) {
        return this.contains(p.getX(), p.getY());
    }
    
    @Override
    public boolean intersects(final Rectangle2D r) {
        return this.intersects(r.getX(), r.getY(), r.getWidth(), r.getHeight());
    }
    
    @Override
    public boolean contains(final Rectangle2D r) {
        return this.contains(r.getX(), r.getY(), r.getWidth(), r.getHeight());
    }
    
    @Override
    public Rectangle getBounds() {
        return this.getBounds2D().getBounds();
    }
    
    @Override
    public PathIterator getPathIterator(final AffineTransform t) {
        return new Iterator(this, t);
    }
    
    @Override
    public PathIterator getPathIterator(final AffineTransform at, final double flatness) {
        return new FlatteningPathIterator(this.getPathIterator(at), flatness);
    }
    
    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }
    
    public static class Float extends CubicCurve2D
    {
        public float x1;
        public float y1;
        public float ctrlx1;
        public float ctrly1;
        public float ctrlx2;
        public float ctrly2;
        public float x2;
        public float y2;
        
        public Float() {
        }
        
        public Float(final float x1, final float y1, final float ctrlx1, final float ctrly1, final float ctrlx2, final float ctrly2, final float x2, final float y2) {
            this.setCurve(x1, y1, ctrlx1, ctrly1, ctrlx2, ctrly2, x2, y2);
        }
        
        @Override
        public double getX1() {
            return this.x1;
        }
        
        @Override
        public double getY1() {
            return this.y1;
        }
        
        @Override
        public double getCtrlX1() {
            return this.ctrlx1;
        }
        
        @Override
        public double getCtrlY1() {
            return this.ctrly1;
        }
        
        @Override
        public double getCtrlX2() {
            return this.ctrlx2;
        }
        
        @Override
        public double getCtrlY2() {
            return this.ctrly2;
        }
        
        @Override
        public double getX2() {
            return this.x2;
        }
        
        @Override
        public double getY2() {
            return this.y2;
        }
        
        @Override
        public Point2D getP1() {
            return new Point2D.Float(this.x1, this.y1);
        }
        
        @Override
        public Point2D getCtrlP1() {
            return new Point2D.Float(this.ctrlx1, this.ctrly1);
        }
        
        @Override
        public Point2D getCtrlP2() {
            return new Point2D.Float(this.ctrlx2, this.ctrly2);
        }
        
        @Override
        public Point2D getP2() {
            return new Point2D.Float(this.x2, this.y2);
        }
        
        @Override
        public void setCurve(final double x1, final double y1, final double ctrlx1, final double ctrly1, final double ctrlx2, final double ctrly2, final double x2, final double y2) {
            this.x1 = (float)x1;
            this.y1 = (float)y1;
            this.ctrlx1 = (float)ctrlx1;
            this.ctrly1 = (float)ctrly1;
            this.ctrlx2 = (float)ctrlx2;
            this.ctrly2 = (float)ctrly2;
            this.x2 = (float)x2;
            this.y2 = (float)y2;
        }
        
        public void setCurve(final float x1, final float y1, final float ctrlx1, final float ctrly1, final float ctrlx2, final float ctrly2, final float x2, final float y2) {
            this.x1 = x1;
            this.y1 = y1;
            this.ctrlx1 = ctrlx1;
            this.ctrly1 = ctrly1;
            this.ctrlx2 = ctrlx2;
            this.ctrly2 = ctrly2;
            this.x2 = x2;
            this.y2 = y2;
        }
        
        @Override
        public Rectangle2D getBounds2D() {
            final float rx1 = Math.min(Math.min(this.x1, this.x2), Math.min(this.ctrlx1, this.ctrlx2));
            final float ry1 = Math.min(Math.min(this.y1, this.y2), Math.min(this.ctrly1, this.ctrly2));
            final float rx2 = Math.max(Math.max(this.x1, this.x2), Math.max(this.ctrlx1, this.ctrlx2));
            final float ry2 = Math.max(Math.max(this.y1, this.y2), Math.max(this.ctrly1, this.ctrly2));
            return new Rectangle2D.Float(rx1, ry1, rx2 - rx1, ry2 - ry1);
        }
    }
    
    public static class Double extends CubicCurve2D
    {
        public double x1;
        public double y1;
        public double ctrlx1;
        public double ctrly1;
        public double ctrlx2;
        public double ctrly2;
        public double x2;
        public double y2;
        
        public Double() {
        }
        
        public Double(final double x1, final double y1, final double ctrlx1, final double ctrly1, final double ctrlx2, final double ctrly2, final double x2, final double y2) {
            this.setCurve(x1, y1, ctrlx1, ctrly1, ctrlx2, ctrly2, x2, y2);
        }
        
        @Override
        public double getX1() {
            return this.x1;
        }
        
        @Override
        public double getY1() {
            return this.y1;
        }
        
        @Override
        public double getCtrlX1() {
            return this.ctrlx1;
        }
        
        @Override
        public double getCtrlY1() {
            return this.ctrly1;
        }
        
        @Override
        public double getCtrlX2() {
            return this.ctrlx2;
        }
        
        @Override
        public double getCtrlY2() {
            return this.ctrly2;
        }
        
        @Override
        public double getX2() {
            return this.x2;
        }
        
        @Override
        public double getY2() {
            return this.y2;
        }
        
        @Override
        public Point2D getP1() {
            return new Point2D.Double(this.x1, this.y1);
        }
        
        @Override
        public Point2D getCtrlP1() {
            return new Point2D.Double(this.ctrlx1, this.ctrly1);
        }
        
        @Override
        public Point2D getCtrlP2() {
            return new Point2D.Double(this.ctrlx2, this.ctrly2);
        }
        
        @Override
        public Point2D getP2() {
            return new Point2D.Double(this.x2, this.y2);
        }
        
        @Override
        public void setCurve(final double x1, final double y1, final double ctrlx1, final double ctrly1, final double ctrlx2, final double ctrly2, final double x2, final double y2) {
            this.x1 = x1;
            this.y1 = y1;
            this.ctrlx1 = ctrlx1;
            this.ctrly1 = ctrly1;
            this.ctrlx2 = ctrlx2;
            this.ctrly2 = ctrly2;
            this.x2 = x2;
            this.y2 = y2;
        }
        
        @Override
        public Rectangle2D getBounds2D() {
            final double rx1 = Math.min(Math.min(this.x1, this.x2), Math.min(this.ctrlx1, this.ctrlx2));
            final double ry1 = Math.min(Math.min(this.y1, this.y2), Math.min(this.ctrly1, this.ctrly2));
            final double rx2 = Math.max(Math.max(this.x1, this.x2), Math.max(this.ctrlx1, this.ctrlx2));
            final double ry2 = Math.max(Math.max(this.y1, this.y2), Math.max(this.ctrly1, this.ctrly2));
            return new Rectangle2D.Double(rx1, ry1, rx2 - rx1, ry2 - ry1);
        }
    }
    
    class Iterator implements PathIterator
    {
        CubicCurve2D c;
        AffineTransform t;
        int index;
        
        Iterator(final CubicCurve2D c, final AffineTransform t) {
            this.c = c;
            this.t = t;
        }
        
        @Override
        public int getWindingRule() {
            return 1;
        }
        
        @Override
        public boolean isDone() {
            return this.index > 1;
        }
        
        @Override
        public void next() {
            ++this.index;
        }
        
        @Override
        public int currentSegment(final double[] coords) {
            if (this.isDone()) {
                throw new NoSuchElementException(Messages.getString("awt.4B"));
            }
            int type;
            int count;
            if (this.index == 0) {
                type = 0;
                coords[0] = this.c.getX1();
                coords[1] = this.c.getY1();
                count = 1;
            }
            else {
                type = 3;
                coords[0] = this.c.getCtrlX1();
                coords[1] = this.c.getCtrlY1();
                coords[2] = this.c.getCtrlX2();
                coords[3] = this.c.getCtrlY2();
                coords[4] = this.c.getX2();
                coords[5] = this.c.getY2();
                count = 3;
            }
            if (this.t != null) {
                this.t.transform(coords, 0, coords, 0, count);
            }
            return type;
        }
        
        @Override
        public int currentSegment(final float[] coords) {
            if (this.isDone()) {
                throw new NoSuchElementException(Messages.getString("awt.4B"));
            }
            int type;
            int count;
            if (this.index == 0) {
                type = 0;
                coords[0] = (float)this.c.getX1();
                coords[1] = (float)this.c.getY1();
                count = 1;
            }
            else {
                type = 3;
                coords[0] = (float)this.c.getCtrlX1();
                coords[1] = (float)this.c.getCtrlY1();
                coords[2] = (float)this.c.getCtrlX2();
                coords[3] = (float)this.c.getCtrlY2();
                coords[4] = (float)this.c.getX2();
                coords[5] = (float)this.c.getY2();
                count = 3;
            }
            if (this.t != null) {
                this.t.transform(coords, 0, coords, 0, count);
            }
            return type;
        }
    }
}
