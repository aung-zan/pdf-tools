// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

import com.itextpdf.awt.geom.misc.HashCode;
import java.io.Serializable;

public class Dimension extends Dimension2D implements Serializable
{
    private static final long serialVersionUID = 4723952579491349524L;
    public double width;
    public double height;
    
    public Dimension(final Dimension d) {
        this(d.width, d.height);
    }
    
    public Dimension() {
        this(0, 0);
    }
    
    public Dimension(final double width, final double height) {
        this.setSize(width, height);
    }
    
    public Dimension(final int width, final int height) {
        this.setSize(width, height);
    }
    
    @Override
    public int hashCode() {
        final HashCode hash = new HashCode();
        hash.append(this.width);
        hash.append(this.height);
        return hash.hashCode();
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Dimension) {
            final Dimension d = (Dimension)obj;
            return d.width == this.width && d.height == this.height;
        }
        return false;
    }
    
    @Override
    public String toString() {
        return this.getClass().getName() + "[width=" + this.width + ",height=" + this.height + "]";
    }
    
    public void setSize(final int width, final int height) {
        this.width = width;
        this.height = height;
    }
    
    public void setSize(final Dimension d) {
        this.setSize(d.width, d.height);
    }
    
    @Override
    public void setSize(final double width, final double height) {
        this.setSize((int)Math.ceil(width), (int)Math.ceil(height));
    }
    
    public Dimension getSize() {
        return new Dimension(this.width, this.height);
    }
    
    @Override
    public double getHeight() {
        return this.height;
    }
    
    @Override
    public double getWidth() {
        return this.width;
    }
}
