// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

import java.util.NoSuchElementException;
import com.itextpdf.awt.geom.misc.Messages;

public abstract class Line2D implements Shape, Cloneable
{
    protected Line2D() {
    }
    
    public abstract double getX1();
    
    public abstract double getY1();
    
    public abstract double getX2();
    
    public abstract double getY2();
    
    public abstract Point2D getP1();
    
    public abstract Point2D getP2();
    
    public abstract void setLine(final double p0, final double p1, final double p2, final double p3);
    
    public void setLine(final Point2D p1, final Point2D p2) {
        this.setLine(p1.getX(), p1.getY(), p2.getX(), p2.getY());
    }
    
    public void setLine(final Line2D line) {
        this.setLine(line.getX1(), line.getY1(), line.getX2(), line.getY2());
    }
    
    @Override
    public Rectangle getBounds() {
        return this.getBounds2D().getBounds();
    }
    
    public static int relativeCCW(final double x1, final double y1, double x2, double y2, double px, double py) {
        x2 -= x1;
        y2 -= y1;
        px -= x1;
        py -= y1;
        double t = px * y2 - py * x2;
        if (t == 0.0) {
            t = px * x2 + py * y2;
            if (t > 0.0) {
                px -= x2;
                py -= y2;
                t = px * x2 + py * y2;
                if (t < 0.0) {
                    t = 0.0;
                }
            }
        }
        return (t < 0.0) ? -1 : ((t > 0.0) ? 1 : 0);
    }
    
    public int relativeCCW(final double px, final double py) {
        return relativeCCW(this.getX1(), this.getY1(), this.getX2(), this.getY2(), px, py);
    }
    
    public int relativeCCW(final Point2D p) {
        return relativeCCW(this.getX1(), this.getY1(), this.getX2(), this.getY2(), p.getX(), p.getY());
    }
    
    public static boolean linesIntersect(final double x1, final double y1, double x2, double y2, double x3, double y3, double x4, double y4) {
        x2 -= x1;
        y2 -= y1;
        x3 -= x1;
        y3 -= y1;
        x4 -= x1;
        y4 -= y1;
        final double AvB = x2 * y3 - x3 * y2;
        final double AvC = x2 * y4 - x4 * y2;
        if (AvB != 0.0 || AvC != 0.0) {
            final double BvC = x3 * y4 - x4 * y3;
            return AvB * AvC <= 0.0 && BvC * (AvB + BvC - AvC) <= 0.0;
        }
        if (x2 != 0.0) {
            if (x4 * x3 > 0.0) {
                if (x3 * x2 >= 0.0) {
                    if (x2 > 0.0) {
                        if (x3 <= x2) {
                            return true;
                        }
                        if (x4 <= x2) {
                            return true;
                        }
                    }
                    else if (x3 >= x2 || x4 >= x2) {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }
        if (y2 != 0.0) {
            if (y4 * y3 > 0.0) {
                if (y3 * y2 >= 0.0) {
                    if (y2 > 0.0) {
                        if (y3 <= y2) {
                            return true;
                        }
                        if (y4 <= y2) {
                            return true;
                        }
                    }
                    else if (y3 >= y2 || y4 >= y2) {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }
        return false;
    }
    
    public boolean intersectsLine(final double x1, final double y1, final double x2, final double y2) {
        return linesIntersect(x1, y1, x2, y2, this.getX1(), this.getY1(), this.getX2(), this.getY2());
    }
    
    public boolean intersectsLine(final Line2D l) {
        return linesIntersect(l.getX1(), l.getY1(), l.getX2(), l.getY2(), this.getX1(), this.getY1(), this.getX2(), this.getY2());
    }
    
    public static double ptSegDistSq(final double x1, final double y1, double x2, double y2, double px, double py) {
        x2 -= x1;
        y2 -= y1;
        px -= x1;
        py -= y1;
        double dist;
        if (px * x2 + py * y2 <= 0.0) {
            dist = px * px + py * py;
        }
        else {
            px = x2 - px;
            py = y2 - py;
            if (px * x2 + py * y2 <= 0.0) {
                dist = px * px + py * py;
            }
            else {
                dist = px * y2 - py * x2;
                dist = dist * dist / (x2 * x2 + y2 * y2);
            }
        }
        if (dist < 0.0) {
            dist = 0.0;
        }
        return dist;
    }
    
    public static double ptSegDist(final double x1, final double y1, final double x2, final double y2, final double px, final double py) {
        return Math.sqrt(ptSegDistSq(x1, y1, x2, y2, px, py));
    }
    
    public double ptSegDistSq(final double px, final double py) {
        return ptSegDistSq(this.getX1(), this.getY1(), this.getX2(), this.getY2(), px, py);
    }
    
    public double ptSegDistSq(final Point2D p) {
        return ptSegDistSq(this.getX1(), this.getY1(), this.getX2(), this.getY2(), p.getX(), p.getY());
    }
    
    public double ptSegDist(final double px, final double py) {
        return ptSegDist(this.getX1(), this.getY1(), this.getX2(), this.getY2(), px, py);
    }
    
    public double ptSegDist(final Point2D p) {
        return ptSegDist(this.getX1(), this.getY1(), this.getX2(), this.getY2(), p.getX(), p.getY());
    }
    
    public static double ptLineDistSq(final double x1, final double y1, double x2, double y2, double px, double py) {
        x2 -= x1;
        y2 -= y1;
        px -= x1;
        py -= y1;
        final double s = px * y2 - py * x2;
        return s * s / (x2 * x2 + y2 * y2);
    }
    
    public static double ptLineDist(final double x1, final double y1, final double x2, final double y2, final double px, final double py) {
        return Math.sqrt(ptLineDistSq(x1, y1, x2, y2, px, py));
    }
    
    public double ptLineDistSq(final double px, final double py) {
        return ptLineDistSq(this.getX1(), this.getY1(), this.getX2(), this.getY2(), px, py);
    }
    
    public double ptLineDistSq(final Point2D p) {
        return ptLineDistSq(this.getX1(), this.getY1(), this.getX2(), this.getY2(), p.getX(), p.getY());
    }
    
    public double ptLineDist(final double px, final double py) {
        return ptLineDist(this.getX1(), this.getY1(), this.getX2(), this.getY2(), px, py);
    }
    
    public double ptLineDist(final Point2D p) {
        return ptLineDist(this.getX1(), this.getY1(), this.getX2(), this.getY2(), p.getX(), p.getY());
    }
    
    @Override
    public boolean contains(final double px, final double py) {
        return false;
    }
    
    @Override
    public boolean contains(final Point2D p) {
        return false;
    }
    
    @Override
    public boolean contains(final Rectangle2D r) {
        return false;
    }
    
    @Override
    public boolean contains(final double rx, final double ry, final double rw, final double rh) {
        return false;
    }
    
    @Override
    public boolean intersects(final double rx, final double ry, final double rw, final double rh) {
        return this.intersects(new Rectangle2D.Double(rx, ry, rw, rh));
    }
    
    @Override
    public boolean intersects(final Rectangle2D r) {
        return r.intersectsLine(this.getX1(), this.getY1(), this.getX2(), this.getY2());
    }
    
    @Override
    public PathIterator getPathIterator(final AffineTransform at) {
        return new Iterator(this, at);
    }
    
    @Override
    public PathIterator getPathIterator(final AffineTransform at, final double flatness) {
        return new Iterator(this, at);
    }
    
    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }
    
    public static class Float extends Line2D
    {
        public float x1;
        public float y1;
        public float x2;
        public float y2;
        
        public Float() {
        }
        
        public Float(final float x1, final float y1, final float x2, final float y2) {
            this.setLine(x1, y1, x2, y2);
        }
        
        public Float(final Point2D p1, final Point2D p2) {
            this.setLine(p1, p2);
        }
        
        @Override
        public double getX1() {
            return this.x1;
        }
        
        @Override
        public double getY1() {
            return this.y1;
        }
        
        @Override
        public double getX2() {
            return this.x2;
        }
        
        @Override
        public double getY2() {
            return this.y2;
        }
        
        @Override
        public Point2D getP1() {
            return new Point2D.Float(this.x1, this.y1);
        }
        
        @Override
        public Point2D getP2() {
            return new Point2D.Float(this.x2, this.y2);
        }
        
        @Override
        public void setLine(final double x1, final double y1, final double x2, final double y2) {
            this.x1 = (float)x1;
            this.y1 = (float)y1;
            this.x2 = (float)x2;
            this.y2 = (float)y2;
        }
        
        public void setLine(final float x1, final float y1, final float x2, final float y2) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
        }
        
        @Override
        public Rectangle2D getBounds2D() {
            float rx;
            float rw;
            if (this.x1 < this.x2) {
                rx = this.x1;
                rw = this.x2 - this.x1;
            }
            else {
                rx = this.x2;
                rw = this.x1 - this.x2;
            }
            float ry;
            float rh;
            if (this.y1 < this.y2) {
                ry = this.y1;
                rh = this.y2 - this.y1;
            }
            else {
                ry = this.y2;
                rh = this.y1 - this.y2;
            }
            return new Rectangle2D.Float(rx, ry, rw, rh);
        }
    }
    
    public static class Double extends Line2D
    {
        public double x1;
        public double y1;
        public double x2;
        public double y2;
        
        public Double() {
        }
        
        public Double(final double x1, final double y1, final double x2, final double y2) {
            this.setLine(x1, y1, x2, y2);
        }
        
        public Double(final Point2D p1, final Point2D p2) {
            this.setLine(p1, p2);
        }
        
        @Override
        public double getX1() {
            return this.x1;
        }
        
        @Override
        public double getY1() {
            return this.y1;
        }
        
        @Override
        public double getX2() {
            return this.x2;
        }
        
        @Override
        public double getY2() {
            return this.y2;
        }
        
        @Override
        public Point2D getP1() {
            return new Point2D.Double(this.x1, this.y1);
        }
        
        @Override
        public Point2D getP2() {
            return new Point2D.Double(this.x2, this.y2);
        }
        
        @Override
        public void setLine(final double x1, final double y1, final double x2, final double y2) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
        }
        
        @Override
        public Rectangle2D getBounds2D() {
            double rx;
            double rw;
            if (this.x1 < this.x2) {
                rx = this.x1;
                rw = this.x2 - this.x1;
            }
            else {
                rx = this.x2;
                rw = this.x1 - this.x2;
            }
            double ry;
            double rh;
            if (this.y1 < this.y2) {
                ry = this.y1;
                rh = this.y2 - this.y1;
            }
            else {
                ry = this.y2;
                rh = this.y1 - this.y2;
            }
            return new Rectangle2D.Double(rx, ry, rw, rh);
        }
    }
    
    class Iterator implements PathIterator
    {
        double x1;
        double y1;
        double x2;
        double y2;
        AffineTransform t;
        int index;
        
        Iterator(final Line2D l, final AffineTransform at) {
            this.x1 = l.getX1();
            this.y1 = l.getY1();
            this.x2 = l.getX2();
            this.y2 = l.getY2();
            this.t = at;
        }
        
        @Override
        public int getWindingRule() {
            return 1;
        }
        
        @Override
        public boolean isDone() {
            return this.index > 1;
        }
        
        @Override
        public void next() {
            ++this.index;
        }
        
        @Override
        public int currentSegment(final double[] coords) {
            if (this.isDone()) {
                throw new NoSuchElementException(Messages.getString("awt.4B"));
            }
            int type;
            if (this.index == 0) {
                type = 0;
                coords[0] = this.x1;
                coords[1] = this.y1;
            }
            else {
                type = 1;
                coords[0] = this.x2;
                coords[1] = this.y2;
            }
            if (this.t != null) {
                this.t.transform(coords, 0, coords, 0, 1);
            }
            return type;
        }
        
        @Override
        public int currentSegment(final float[] coords) {
            if (this.isDone()) {
                throw new NoSuchElementException(Messages.getString("awt.4B"));
            }
            int type;
            if (this.index == 0) {
                type = 0;
                coords[0] = (float)this.x1;
                coords[1] = (float)this.y1;
            }
            else {
                type = 1;
                coords[0] = (float)this.x2;
                coords[1] = (float)this.y2;
            }
            if (this.t != null) {
                this.t.transform(coords, 0, coords, 0, 1);
            }
            return type;
        }
    }
}
