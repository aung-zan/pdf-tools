// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

public interface Shape
{
    boolean contains(final double p0, final double p1);
    
    boolean contains(final double p0, final double p1, final double p2, final double p3);
    
    boolean contains(final Point2D p0);
    
    boolean contains(final Rectangle2D p0);
    
    Rectangle getBounds();
    
    Rectangle2D getBounds2D();
    
    PathIterator getPathIterator(final AffineTransform p0);
    
    PathIterator getPathIterator(final AffineTransform p0, final double p1);
    
    boolean intersects(final double p0, final double p1, final double p2, final double p3);
    
    boolean intersects(final Rectangle2D p0);
}
