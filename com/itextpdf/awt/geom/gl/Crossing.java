// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom.gl;

import com.itextpdf.awt.geom.AffineTransform;
import com.itextpdf.awt.geom.Shape;
import com.itextpdf.awt.geom.PathIterator;

public class Crossing
{
    static final double DELTA = 1.0E-5;
    static final double ROOT_DELTA = 1.0E-10;
    public static final int CROSSING = 255;
    static final int UNKNOWN = 254;
    
    public static int solveQuad(final double[] eqn, final double[] res) {
        final double a = eqn[2];
        final double b = eqn[1];
        final double c = eqn[0];
        int rc = 0;
        if (a == 0.0) {
            if (b == 0.0) {
                return -1;
            }
            res[rc++] = -c / b;
        }
        else {
            double d = b * b - 4.0 * a * c;
            if (d < 0.0) {
                return 0;
            }
            d = Math.sqrt(d);
            res[rc++] = (-b + d) / (a * 2.0);
            if (d != 0.0) {
                res[rc++] = (-b - d) / (a * 2.0);
            }
        }
        return fixRoots(res, rc);
    }
    
    public static int solveCubic(final double[] eqn, final double[] res) {
        final double d = eqn[3];
        if (d == 0.0) {
            return solveQuad(eqn, res);
        }
        final double a = eqn[2] / d;
        final double b = eqn[1] / d;
        final double c = eqn[0] / d;
        int rc = 0;
        final double Q = (a * a - 3.0 * b) / 9.0;
        final double R = (2.0 * a * a * a - 9.0 * a * b + 27.0 * c) / 54.0;
        final double Q2 = Q * Q * Q;
        final double R2 = R * R;
        final double n = -a / 3.0;
        if (R2 < Q2) {
            final double t = Math.acos(R / Math.sqrt(Q2)) / 3.0;
            final double p = 2.0943951023931953;
            final double m = -2.0 * Math.sqrt(Q);
            res[rc++] = m * Math.cos(t) + n;
            res[rc++] = m * Math.cos(t + p) + n;
            res[rc++] = m * Math.cos(t - p) + n;
        }
        else {
            double A = Math.pow(Math.abs(R) + Math.sqrt(R2 - Q2), 0.3333333333333333);
            if (R > 0.0) {
                A = -A;
            }
            if (-1.0E-10 < A && A < 1.0E-10) {
                res[rc++] = n;
            }
            else {
                final double B = Q / A;
                res[rc++] = A + B + n;
                final double delta = R2 - Q2;
                if (-1.0E-10 < delta && delta < 1.0E-10) {
                    res[rc++] = -(A + B) / 2.0 + n;
                }
            }
        }
        return fixRoots(res, rc);
    }
    
    static int fixRoots(final double[] res, final int rc) {
        int tc = 0;
        int i = 0;
    Label_0004:
        while (i < rc) {
            while (true) {
                for (int j = i + 1; j < rc; ++j) {
                    if (isZero(res[i] - res[j])) {
                        ++i;
                        continue Label_0004;
                    }
                }
                res[tc++] = res[i];
                continue;
            }
        }
        return tc;
    }
    
    public static int crossLine(final double x1, final double y1, final double x2, final double y2, final double x, final double y) {
        if ((x < x1 && x < x2) || (x > x1 && x > x2) || (y > y1 && y > y2) || x1 == x2) {
            return 0;
        }
        if (y >= y1 || y >= y2) {
            if ((y2 - y1) * (x - x1) / (x2 - x1) <= y - y1) {
                return 0;
            }
        }
        if (x == x1) {
            return (x1 < x2) ? 0 : -1;
        }
        if (x == x2) {
            return (x1 < x2) ? 1 : 0;
        }
        return (x1 < x2) ? 1 : -1;
    }
    
    public static int crossQuad(final double x1, final double y1, final double cx, final double cy, final double x2, final double y2, final double x, final double y) {
        if ((x < x1 && x < cx && x < x2) || (x > x1 && x > cx && x > x2) || (y > y1 && y > cy && y > y2) || (x1 == cx && cx == x2)) {
            return 0;
        }
        if (y >= y1 || y >= cy || y >= y2 || x == x1 || x == x2) {
            final QuadCurve c = new QuadCurve(x1, y1, cx, cy, x2, y2);
            final double px = x - x1;
            final double py = y - y1;
            final double[] res = new double[3];
            final int rc = c.solvePoint(res, px);
            return c.cross(res, rc, py, py);
        }
        if (x1 < x2) {
            return (x1 < x && x < x2) ? 1 : 0;
        }
        return (x2 < x && x < x1) ? -1 : 0;
    }
    
    public static int crossCubic(final double x1, final double y1, final double cx1, final double cy1, final double cx2, final double cy2, final double x2, final double y2, final double x, final double y) {
        if ((x < x1 && x < cx1 && x < cx2 && x < x2) || (x > x1 && x > cx1 && x > cx2 && x > x2) || (y > y1 && y > cy1 && y > cy2 && y > y2) || (x1 == cx1 && cx1 == cx2 && cx2 == x2)) {
            return 0;
        }
        if (y >= y1 || y >= cy1 || y >= cy2 || y >= y2 || x == x1 || x == x2) {
            final CubicCurve c = new CubicCurve(x1, y1, cx1, cy1, cx2, cy2, x2, y2);
            final double px = x - x1;
            final double py = y - y1;
            final double[] res = new double[3];
            final int rc = c.solvePoint(res, px);
            return c.cross(res, rc, py, py);
        }
        if (x1 < x2) {
            return (x1 < x && x < x2) ? 1 : 0;
        }
        return (x2 < x && x < x1) ? -1 : 0;
    }
    
    public static int crossPath(final PathIterator p, final double x, final double y) {
        int cross = 0;
        double cy;
        double cx;
        double mx;
        double my = mx = (cx = (cy = 0.0));
        final double[] coords = new double[6];
        while (!p.isDone()) {
            switch (p.currentSegment(coords)) {
                case 0: {
                    if (cx != mx || cy != my) {
                        cross += crossLine(cx, cy, mx, my, x, y);
                    }
                    cx = (mx = coords[0]);
                    cy = (my = coords[1]);
                    break;
                }
                case 1: {
                    cross += crossLine(cx, cy, cx = coords[0], cy = coords[1], x, y);
                    break;
                }
                case 2: {
                    cross += crossQuad(cx, cy, coords[0], coords[1], cx = coords[2], cy = coords[3], x, y);
                    break;
                }
                case 3: {
                    cross += crossCubic(cx, cy, coords[0], coords[1], coords[2], coords[3], cx = coords[4], cy = coords[5], x, y);
                    break;
                }
                case 4: {
                    if (cy != my || cx != mx) {
                        cross += crossLine(cx, cy, cx = mx, cy = my, x, y);
                        break;
                    }
                    break;
                }
            }
            if (x == cx && y == cy) {
                cross = 0;
                cy = my;
                break;
            }
            p.next();
        }
        if (cy != my) {
            cross += crossLine(cx, cy, mx, my, x, y);
        }
        return cross;
    }
    
    public static int crossShape(final Shape s, final double x, final double y) {
        if (!s.getBounds2D().contains(x, y)) {
            return 0;
        }
        return crossPath(s.getPathIterator(null), x, y);
    }
    
    public static boolean isZero(final double val) {
        return -1.0E-5 < val && val < 1.0E-5;
    }
    
    static void sortBound(final double[] bound, final int bc) {
        for (int i = 0; i < bc - 4; i += 4) {
            int k = i;
            for (int j = i + 4; j < bc; j += 4) {
                if (bound[k] > bound[j]) {
                    k = j;
                }
            }
            if (k != i) {
                double tmp = bound[i];
                bound[i] = bound[k];
                bound[k] = tmp;
                tmp = bound[i + 1];
                bound[i + 1] = bound[k + 1];
                bound[k + 1] = tmp;
                tmp = bound[i + 2];
                bound[i + 2] = bound[k + 2];
                bound[k + 2] = tmp;
                tmp = bound[i + 3];
                bound[i + 3] = bound[k + 3];
                bound[k + 3] = tmp;
            }
        }
    }
    
    static int crossBound(final double[] bound, final int bc, final double py1, final double py2) {
        if (bc == 0) {
            return 0;
        }
        int up = 0;
        int down = 0;
        for (int i = 2; i < bc; i += 4) {
            if (bound[i] < py1) {
                ++up;
            }
            else {
                if (bound[i] <= py2) {
                    return 255;
                }
                ++down;
            }
        }
        if (down == 0) {
            return 0;
        }
        if (up != 0) {
            sortBound(bound, bc);
            boolean sign = bound[2] > py2;
            for (int j = 6; j < bc; j += 4) {
                final boolean sign2 = bound[j] > py2;
                if (sign != sign2 && bound[j + 1] != bound[j - 3]) {
                    return 255;
                }
                sign = sign2;
            }
        }
        return 254;
    }
    
    public static int intersectLine(final double x1, final double y1, final double x2, final double y2, final double rx1, final double ry1, final double rx2, final double ry2) {
        if ((rx2 < x1 && rx2 < x2) || (rx1 > x1 && rx1 > x2) || (ry1 > y1 && ry1 > y2)) {
            return 0;
        }
        if (ry2 >= y1 || ry2 >= y2) {
            if (x1 == x2) {
                return 255;
            }
            double bx1;
            double bx2;
            if (x1 < x2) {
                bx1 = ((x1 < rx1) ? rx1 : x1);
                bx2 = ((x2 < rx2) ? x2 : rx2);
            }
            else {
                bx1 = ((x2 < rx1) ? rx1 : x2);
                bx2 = ((x1 < rx2) ? x1 : rx2);
            }
            final double k = (y2 - y1) / (x2 - x1);
            final double by1 = k * (bx1 - x1) + y1;
            final double by2 = k * (bx2 - x1) + y1;
            if (by1 < ry1 && by2 < ry1) {
                return 0;
            }
            if (by1 <= ry2 || by2 <= ry2) {
                return 255;
            }
        }
        if (x1 == x2) {
            return 0;
        }
        if (rx1 == x1) {
            return (x1 < x2) ? 0 : -1;
        }
        if (rx1 == x2) {
            return (x1 < x2) ? 1 : 0;
        }
        if (x1 < x2) {
            return (x1 < rx1 && rx1 < x2) ? 1 : 0;
        }
        return (x2 < rx1 && rx1 < x1) ? -1 : 0;
    }
    
    public static int intersectQuad(final double x1, final double y1, final double cx, final double cy, final double x2, final double y2, final double rx1, final double ry1, final double rx2, final double ry2) {
        if ((rx2 < x1 && rx2 < cx && rx2 < x2) || (rx1 > x1 && rx1 > cx && rx1 > x2) || (ry1 > y1 && ry1 > cy && ry1 > y2)) {
            return 0;
        }
        if (ry2 < y1 && ry2 < cy && ry2 < y2 && rx1 != x1 && rx1 != x2) {
            if (x1 < x2) {
                return (x1 < rx1 && rx1 < x2) ? 1 : 0;
            }
            return (x2 < rx1 && rx1 < x1) ? -1 : 0;
        }
        else {
            final QuadCurve c = new QuadCurve(x1, y1, cx, cy, x2, y2);
            final double px1 = rx1 - x1;
            final double py1 = ry1 - y1;
            final double px2 = rx2 - x1;
            final double py2 = ry2 - y1;
            final double[] res1 = new double[3];
            final double[] res2 = new double[3];
            final int rc1 = c.solvePoint(res1, px1);
            int rc2 = c.solvePoint(res2, px2);
            if (rc1 == 0 && rc2 == 0) {
                return 0;
            }
            final double minX = px1 - 1.0E-5;
            final double maxX = px2 + 1.0E-5;
            final double[] bound = new double[28];
            int bc = 0;
            bc = c.addBound(bound, bc, res1, rc1, minX, maxX, false, 0);
            bc = c.addBound(bound, bc, res2, rc2, minX, maxX, false, 1);
            rc2 = c.solveExtrem(res2);
            bc = c.addBound(bound, bc, res2, rc2, minX, maxX, true, 2);
            if (rx1 < x1 && x1 < rx2) {
                bound[bc++] = 0.0;
                bound[bc++] = 0.0;
                bound[bc++] = 0.0;
                bound[bc++] = 4.0;
            }
            if (rx1 < x2 && x2 < rx2) {
                bound[bc++] = 1.0;
                bound[bc++] = c.ax;
                bound[bc++] = c.ay;
                bound[bc++] = 5.0;
            }
            final int cross = crossBound(bound, bc, py1, py2);
            if (cross != 254) {
                return cross;
            }
            return c.cross(res1, rc1, py1, py2);
        }
    }
    
    public static int intersectCubic(final double x1, final double y1, final double cx1, final double cy1, final double cx2, final double cy2, final double x2, final double y2, final double rx1, final double ry1, final double rx2, final double ry2) {
        if ((rx2 < x1 && rx2 < cx1 && rx2 < cx2 && rx2 < x2) || (rx1 > x1 && rx1 > cx1 && rx1 > cx2 && rx1 > x2) || (ry1 > y1 && ry1 > cy1 && ry1 > cy2 && ry1 > y2)) {
            return 0;
        }
        if (ry2 < y1 && ry2 < cy1 && ry2 < cy2 && ry2 < y2 && rx1 != x1 && rx1 != x2) {
            if (x1 < x2) {
                return (x1 < rx1 && rx1 < x2) ? 1 : 0;
            }
            return (x2 < rx1 && rx1 < x1) ? -1 : 0;
        }
        else {
            final CubicCurve c = new CubicCurve(x1, y1, cx1, cy1, cx2, cy2, x2, y2);
            final double px1 = rx1 - x1;
            final double py1 = ry1 - y1;
            final double px2 = rx2 - x1;
            final double py2 = ry2 - y1;
            final double[] res1 = new double[3];
            final double[] res2 = new double[3];
            final int rc1 = c.solvePoint(res1, px1);
            int rc2 = c.solvePoint(res2, px2);
            if (rc1 == 0 && rc2 == 0) {
                return 0;
            }
            final double minX = px1 - 1.0E-5;
            final double maxX = px2 + 1.0E-5;
            final double[] bound = new double[40];
            int bc = 0;
            bc = c.addBound(bound, bc, res1, rc1, minX, maxX, false, 0);
            bc = c.addBound(bound, bc, res2, rc2, minX, maxX, false, 1);
            rc2 = c.solveExtremX(res2);
            bc = c.addBound(bound, bc, res2, rc2, minX, maxX, true, 2);
            rc2 = c.solveExtremY(res2);
            bc = c.addBound(bound, bc, res2, rc2, minX, maxX, true, 4);
            if (rx1 < x1 && x1 < rx2) {
                bound[bc++] = 0.0;
                bound[bc++] = 0.0;
                bound[bc++] = 0.0;
                bound[bc++] = 6.0;
            }
            if (rx1 < x2 && x2 < rx2) {
                bound[bc++] = 1.0;
                bound[bc++] = c.ax;
                bound[bc++] = c.ay;
                bound[bc++] = 7.0;
            }
            final int cross = crossBound(bound, bc, py1, py2);
            if (cross != 254) {
                return cross;
            }
            return c.cross(res1, rc1, py1, py2);
        }
    }
    
    public static int intersectPath(final PathIterator p, final double x, final double y, final double w, final double h) {
        int cross = 0;
        double cy;
        double cx;
        double mx;
        double my = mx = (cx = (cy = 0.0));
        final double[] coords = new double[6];
        final double rx1 = x;
        final double ry1 = y;
        final double rx2 = x + w;
        final double ry2 = y + h;
        while (!p.isDone()) {
            int count = 0;
            switch (p.currentSegment(coords)) {
                case 0: {
                    if (cx != mx || cy != my) {
                        count = intersectLine(cx, cy, mx, my, rx1, ry1, rx2, ry2);
                    }
                    cx = (mx = coords[0]);
                    cy = (my = coords[1]);
                    break;
                }
                case 1: {
                    count = intersectLine(cx, cy, cx = coords[0], cy = coords[1], rx1, ry1, rx2, ry2);
                    break;
                }
                case 2: {
                    count = intersectQuad(cx, cy, coords[0], coords[1], cx = coords[2], cy = coords[3], rx1, ry1, rx2, ry2);
                    break;
                }
                case 3: {
                    count = intersectCubic(cx, cy, coords[0], coords[1], coords[2], coords[3], cx = coords[4], cy = coords[5], rx1, ry1, rx2, ry2);
                    break;
                }
                case 4: {
                    if (cy != my || cx != mx) {
                        count = intersectLine(cx, cy, mx, my, rx1, ry1, rx2, ry2);
                    }
                    cx = mx;
                    cy = my;
                    break;
                }
            }
            if (count == 255) {
                return 255;
            }
            cross += count;
            p.next();
        }
        if (cy != my) {
            final int count = intersectLine(cx, cy, mx, my, rx1, ry1, rx2, ry2);
            if (count == 255) {
                return 255;
            }
            cross += count;
        }
        return cross;
    }
    
    public static int intersectShape(final Shape s, final double x, final double y, final double w, final double h) {
        if (!s.getBounds2D().intersects(x, y, w, h)) {
            return 0;
        }
        return intersectPath(s.getPathIterator(null), x, y, w, h);
    }
    
    public static boolean isInsideNonZero(final int cross) {
        return cross != 0;
    }
    
    public static boolean isInsideEvenOdd(final int cross) {
        return (cross & 0x1) != 0x0;
    }
    
    public static class QuadCurve
    {
        double ax;
        double ay;
        double bx;
        double by;
        double Ax;
        double Ay;
        double Bx;
        double By;
        
        public QuadCurve(final double x1, final double y1, final double cx, final double cy, final double x2, final double y2) {
            this.ax = x2 - x1;
            this.ay = y2 - y1;
            this.bx = cx - x1;
            this.by = cy - y1;
            this.Bx = this.bx + this.bx;
            this.Ax = this.ax - this.Bx;
            this.By = this.by + this.by;
            this.Ay = this.ay - this.By;
        }
        
        int cross(final double[] res, final int rc, final double py1, final double py2) {
            int cross = 0;
            for (final double t : res) {
                if (t >= -1.0E-5) {
                    if (t <= 1.00001) {
                        if (t < 1.0E-5) {
                            if (py1 < 0.0 && ((this.bx != 0.0) ? this.bx : (this.ax - this.bx)) < 0.0) {
                                --cross;
                            }
                        }
                        else if (t > 0.99999) {
                            if (py1 < this.ay && ((this.ax != this.bx) ? (this.ax - this.bx) : this.bx) > 0.0) {
                                ++cross;
                            }
                        }
                        else {
                            final double ry = t * (t * this.Ay + this.By);
                            if (ry > py2) {
                                final double rxt = t * this.Ax + this.bx;
                                if (rxt <= -1.0E-5 || rxt >= 1.0E-5) {
                                    cross += ((rxt > 0.0) ? 1 : -1);
                                }
                            }
                        }
                    }
                }
            }
            return cross;
        }
        
        int solvePoint(final double[] res, final double px) {
            final double[] eqn = { -px, this.Bx, this.Ax };
            return Crossing.solveQuad(eqn, res);
        }
        
        int solveExtrem(final double[] res) {
            int rc = 0;
            if (this.Ax != 0.0) {
                res[rc++] = -this.Bx / (this.Ax + this.Ax);
            }
            if (this.Ay != 0.0) {
                res[rc++] = -this.By / (this.Ay + this.Ay);
            }
            return rc;
        }
        
        int addBound(final double[] bound, int bc, final double[] res, final int rc, final double minX, final double maxX, final boolean changeId, int id) {
            for (final double t : res) {
                if (t > -1.0E-5 && t < 1.00001) {
                    final double rx = t * (t * this.Ax + this.Bx);
                    if (minX <= rx && rx <= maxX) {
                        bound[bc++] = t;
                        bound[bc++] = rx;
                        bound[bc++] = t * (t * this.Ay + this.By);
                        bound[bc++] = id;
                        if (changeId) {
                            ++id;
                        }
                    }
                }
            }
            return bc;
        }
    }
    
    public static class CubicCurve
    {
        double ax;
        double ay;
        double bx;
        double by;
        double cx;
        double cy;
        double Ax;
        double Ay;
        double Bx;
        double By;
        double Cx;
        double Cy;
        double Ax3;
        double Bx2;
        
        public CubicCurve(final double x1, final double y1, final double cx1, final double cy1, final double cx2, final double cy2, final double x2, final double y2) {
            this.ax = x2 - x1;
            this.ay = y2 - y1;
            this.bx = cx1 - x1;
            this.by = cy1 - y1;
            this.cx = cx2 - x1;
            this.cy = cy2 - y1;
            this.Cx = this.bx + this.bx + this.bx;
            this.Bx = this.cx + this.cx + this.cx - this.Cx - this.Cx;
            this.Ax = this.ax - this.Bx - this.Cx;
            this.Cy = this.by + this.by + this.by;
            this.By = this.cy + this.cy + this.cy - this.Cy - this.Cy;
            this.Ay = this.ay - this.By - this.Cy;
            this.Ax3 = this.Ax + this.Ax + this.Ax;
            this.Bx2 = this.Bx + this.Bx;
        }
        
        int cross(final double[] res, final int rc, final double py1, final double py2) {
            int cross = 0;
            for (final double t : res) {
                Label_0331: {
                    if (t >= -1.0E-5) {
                        if (t <= 1.00001) {
                            if (t < 1.0E-5) {
                                if (py1 < 0.0 && ((this.bx != 0.0) ? this.bx : ((this.cx != this.bx) ? (this.cx - this.bx) : (this.ax - this.cx))) < 0.0) {
                                    --cross;
                                }
                            }
                            else if (t > 0.99999) {
                                if (py1 < this.ay && ((this.ax != this.cx) ? (this.ax - this.cx) : ((this.cx != this.bx) ? (this.cx - this.bx) : this.bx)) > 0.0) {
                                    ++cross;
                                }
                            }
                            else {
                                final double ry = t * (t * (t * this.Ay + this.By) + this.Cy);
                                if (ry > py2) {
                                    double rxt = t * (t * this.Ax3 + this.Bx2) + this.Cx;
                                    if (rxt > -1.0E-5 && rxt < 1.0E-5) {
                                        rxt = t * (this.Ax3 + this.Ax3) + this.Bx2;
                                        if (rxt < -1.0E-5) {
                                            break Label_0331;
                                        }
                                        if (rxt > 1.0E-5) {
                                            break Label_0331;
                                        }
                                        rxt = this.ax;
                                    }
                                    cross += ((rxt > 0.0) ? 1 : -1);
                                }
                            }
                        }
                    }
                }
            }
            return cross;
        }
        
        int solvePoint(final double[] res, final double px) {
            final double[] eqn = { -px, this.Cx, this.Bx, this.Ax };
            return Crossing.solveCubic(eqn, res);
        }
        
        int solveExtremX(final double[] res) {
            final double[] eqn = { this.Cx, this.Bx2, this.Ax3 };
            return Crossing.solveQuad(eqn, res);
        }
        
        int solveExtremY(final double[] res) {
            final double[] eqn = { this.Cy, this.By + this.By, this.Ay + this.Ay + this.Ay };
            return Crossing.solveQuad(eqn, res);
        }
        
        int addBound(final double[] bound, int bc, final double[] res, final int rc, final double minX, final double maxX, final boolean changeId, int id) {
            for (final double t : res) {
                if (t > -1.0E-5 && t < 1.00001) {
                    final double rx = t * (t * (t * this.Ax + this.Bx) + this.Cx);
                    if (minX <= rx && rx <= maxX) {
                        bound[bc++] = t;
                        bound[bc++] = rx;
                        bound[bc++] = t * (t * (t * this.Ay + this.By) + this.Cy);
                        bound[bc++] = id;
                        if (changeId) {
                            ++id;
                        }
                    }
                }
            }
            return bc;
        }
    }
}
