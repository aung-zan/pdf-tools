// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

public class NoninvertibleTransformException extends Exception
{
    private static final long serialVersionUID = 6137225240503990466L;
    
    public NoninvertibleTransformException(final String s) {
        super(s);
    }
}
