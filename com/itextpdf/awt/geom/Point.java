// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

import java.io.Serializable;

public class Point extends Point2D implements Serializable
{
    private static final long serialVersionUID = -5276940640259749850L;
    public double x;
    public double y;
    
    public Point() {
        this.setLocation(0, 0);
    }
    
    public Point(final int x, final int y) {
        this.setLocation(x, y);
    }
    
    public Point(final double x, final double y) {
        this.setLocation(x, y);
    }
    
    public Point(final Point p) {
        this.setLocation(p.x, p.y);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Point) {
            final Point p = (Point)obj;
            return this.x == p.x && this.y == p.y;
        }
        return false;
    }
    
    @Override
    public String toString() {
        return this.getClass().getName() + "[x=" + this.x + ",y=" + this.y + "]";
    }
    
    @Override
    public double getX() {
        return this.x;
    }
    
    @Override
    public double getY() {
        return this.y;
    }
    
    public Point getLocation() {
        return new Point(this.x, this.y);
    }
    
    public void setLocation(final Point p) {
        this.setLocation(p.x, p.y);
    }
    
    public void setLocation(final int x, final int y) {
        this.setLocation(x, (double)y);
    }
    
    @Override
    public void setLocation(final double x, final double y) {
        this.x = x;
        this.y = y;
    }
    
    public void move(final int x, final int y) {
        this.move(x, (double)y);
    }
    
    public void move(final double x, final double y) {
        this.setLocation(x, y);
    }
    
    public void translate(final int dx, final int dy) {
        this.translate(dx, (double)dy);
    }
    
    public void translate(final double dx, final double dy) {
        this.x += dx;
        this.y += dy;
    }
}
