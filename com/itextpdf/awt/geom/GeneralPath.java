// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

import java.util.NoSuchElementException;
import com.itextpdf.awt.geom.gl.Crossing;
import com.itextpdf.awt.geom.misc.Messages;

public final class GeneralPath implements Shape, Cloneable
{
    public static final int WIND_EVEN_ODD = 0;
    public static final int WIND_NON_ZERO = 1;
    private static final int BUFFER_SIZE = 10;
    private static final int BUFFER_CAPACITY = 10;
    byte[] types;
    float[] points;
    int typeSize;
    int pointSize;
    int rule;
    static int[] pointShift;
    
    public GeneralPath() {
        this(1, 10);
    }
    
    public GeneralPath(final int rule) {
        this(rule, 10);
    }
    
    public GeneralPath(final int rule, final int initialCapacity) {
        this.setWindingRule(rule);
        this.types = new byte[initialCapacity];
        this.points = new float[initialCapacity * 2];
    }
    
    public GeneralPath(final Shape shape) {
        this(1, 10);
        final PathIterator p = shape.getPathIterator(null);
        this.setWindingRule(p.getWindingRule());
        this.append(p, false);
    }
    
    public void setWindingRule(final int rule) {
        if (rule != 0 && rule != 1) {
            throw new IllegalArgumentException(Messages.getString("awt.209"));
        }
        this.rule = rule;
    }
    
    public int getWindingRule() {
        return this.rule;
    }
    
    void checkBuf(final int pointCount, final boolean checkMove) {
        if (checkMove && this.typeSize == 0) {
            throw new IllegalPathStateException(Messages.getString("awt.20A"));
        }
        if (this.typeSize == this.types.length) {
            final byte[] tmp = new byte[this.typeSize + 10];
            System.arraycopy(this.types, 0, tmp, 0, this.typeSize);
            this.types = tmp;
        }
        if (this.pointSize + pointCount > this.points.length) {
            final float[] tmp2 = new float[this.pointSize + Math.max(20, pointCount)];
            System.arraycopy(this.points, 0, tmp2, 0, this.pointSize);
            this.points = tmp2;
        }
    }
    
    public void moveTo(final float x, final float y) {
        if (this.typeSize > 0 && this.types[this.typeSize - 1] == 0) {
            this.points[this.pointSize - 2] = x;
            this.points[this.pointSize - 1] = y;
        }
        else {
            this.checkBuf(2, false);
            this.types[this.typeSize++] = 0;
            this.points[this.pointSize++] = x;
            this.points[this.pointSize++] = y;
        }
    }
    
    public void lineTo(final float x, final float y) {
        this.checkBuf(2, true);
        this.types[this.typeSize++] = 1;
        this.points[this.pointSize++] = x;
        this.points[this.pointSize++] = y;
    }
    
    public void quadTo(final float x1, final float y1, final float x2, final float y2) {
        this.checkBuf(4, true);
        this.types[this.typeSize++] = 2;
        this.points[this.pointSize++] = x1;
        this.points[this.pointSize++] = y1;
        this.points[this.pointSize++] = x2;
        this.points[this.pointSize++] = y2;
    }
    
    public void curveTo(final float x1, final float y1, final float x2, final float y2, final float x3, final float y3) {
        this.checkBuf(6, true);
        this.types[this.typeSize++] = 3;
        this.points[this.pointSize++] = x1;
        this.points[this.pointSize++] = y1;
        this.points[this.pointSize++] = x2;
        this.points[this.pointSize++] = y2;
        this.points[this.pointSize++] = x3;
        this.points[this.pointSize++] = y3;
    }
    
    public void closePath() {
        if (this.typeSize == 0 || this.types[this.typeSize - 1] != 4) {
            this.checkBuf(0, true);
            this.types[this.typeSize++] = 4;
        }
    }
    
    public void append(final Shape shape, final boolean connect) {
        final PathIterator p = shape.getPathIterator(null);
        this.append(p, connect);
    }
    
    public void append(final PathIterator path, boolean connect) {
        while (!path.isDone()) {
            final float[] coords = new float[6];
            switch (path.currentSegment(coords)) {
                case 0: {
                    if (!connect || this.typeSize == 0) {
                        this.moveTo(coords[0], coords[1]);
                        break;
                    }
                    if (this.types[this.typeSize - 1] != 4 && this.points[this.pointSize - 2] == coords[0] && this.points[this.pointSize - 1] == coords[1]) {
                        break;
                    }
                }
                case 1: {
                    this.lineTo(coords[0], coords[1]);
                    break;
                }
                case 2: {
                    this.quadTo(coords[0], coords[1], coords[2], coords[3]);
                    break;
                }
                case 3: {
                    this.curveTo(coords[0], coords[1], coords[2], coords[3], coords[4], coords[5]);
                    break;
                }
                case 4: {
                    this.closePath();
                    break;
                }
            }
            path.next();
            connect = false;
        }
    }
    
    public Point2D getCurrentPoint() {
        if (this.typeSize == 0) {
            return null;
        }
        int j = this.pointSize - 2;
        if (this.types[this.typeSize - 1] == 4) {
            for (int i = this.typeSize - 2; i > 0; --i) {
                final int type = this.types[i];
                if (type == 0) {
                    break;
                }
                j -= GeneralPath.pointShift[type];
            }
        }
        return new Point2D.Float(this.points[j], this.points[j + 1]);
    }
    
    public void reset() {
        this.typeSize = 0;
        this.pointSize = 0;
    }
    
    public void transform(final AffineTransform t) {
        t.transform(this.points, 0, this.points, 0, this.pointSize / 2);
    }
    
    public Shape createTransformedShape(final AffineTransform t) {
        final GeneralPath p = (GeneralPath)this.clone();
        if (t != null) {
            p.transform(t);
        }
        return p;
    }
    
    @Override
    public Rectangle2D getBounds2D() {
        float ry2;
        float rx2;
        float rx1;
        float ry1;
        if (this.pointSize == 0) {
            ry1 = (rx1 = (rx2 = (ry2 = 0.0f)));
        }
        else {
            int i = this.pointSize - 1;
            ry2 = (ry1 = this.points[i--]);
            rx2 = (rx1 = this.points[i--]);
            while (i > 0) {
                final float y = this.points[i--];
                final float x = this.points[i--];
                if (x < rx1) {
                    rx1 = x;
                }
                else if (x > rx2) {
                    rx2 = x;
                }
                if (y < ry1) {
                    ry1 = y;
                }
                else {
                    if (y <= ry2) {
                        continue;
                    }
                    ry2 = y;
                }
            }
        }
        return new Rectangle2D.Float(rx1, ry1, rx2 - rx1, ry2 - ry1);
    }
    
    @Override
    public Rectangle getBounds() {
        return this.getBounds2D().getBounds();
    }
    
    boolean isInside(final int cross) {
        if (this.rule == 1) {
            return Crossing.isInsideNonZero(cross);
        }
        return Crossing.isInsideEvenOdd(cross);
    }
    
    @Override
    public boolean contains(final double px, final double py) {
        return this.isInside(Crossing.crossShape(this, px, py));
    }
    
    @Override
    public boolean contains(final double rx, final double ry, final double rw, final double rh) {
        final int cross = Crossing.intersectShape(this, rx, ry, rw, rh);
        return cross != 255 && this.isInside(cross);
    }
    
    @Override
    public boolean intersects(final double rx, final double ry, final double rw, final double rh) {
        final int cross = Crossing.intersectShape(this, rx, ry, rw, rh);
        return cross == 255 || this.isInside(cross);
    }
    
    @Override
    public boolean contains(final Point2D p) {
        return this.contains(p.getX(), p.getY());
    }
    
    @Override
    public boolean contains(final Rectangle2D r) {
        return this.contains(r.getX(), r.getY(), r.getWidth(), r.getHeight());
    }
    
    @Override
    public boolean intersects(final Rectangle2D r) {
        return this.intersects(r.getX(), r.getY(), r.getWidth(), r.getHeight());
    }
    
    @Override
    public PathIterator getPathIterator(final AffineTransform t) {
        return new Iterator(this, t);
    }
    
    @Override
    public PathIterator getPathIterator(final AffineTransform t, final double flatness) {
        return new FlatteningPathIterator(this.getPathIterator(t), flatness);
    }
    
    public Object clone() {
        try {
            final GeneralPath p = (GeneralPath)super.clone();
            p.types = this.types.clone();
            p.points = this.points.clone();
            return p;
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }
    
    static {
        GeneralPath.pointShift = new int[] { 2, 2, 4, 6, 0 };
    }
    
    class Iterator implements PathIterator
    {
        int typeIndex;
        int pointIndex;
        GeneralPath p;
        AffineTransform t;
        
        Iterator(final GeneralPath generalPath, final GeneralPath path) {
            this(generalPath, path, null);
        }
        
        Iterator(final GeneralPath path, final AffineTransform at) {
            this.p = path;
            this.t = at;
        }
        
        @Override
        public int getWindingRule() {
            return this.p.getWindingRule();
        }
        
        @Override
        public boolean isDone() {
            return this.typeIndex >= this.p.typeSize;
        }
        
        @Override
        public void next() {
            ++this.typeIndex;
        }
        
        @Override
        public int currentSegment(final double[] coords) {
            if (this.isDone()) {
                throw new NoSuchElementException(Messages.getString("awt.4B"));
            }
            final int type = this.p.types[this.typeIndex];
            final int count = GeneralPath.pointShift[type];
            for (int i = 0; i < count; ++i) {
                coords[i] = this.p.points[this.pointIndex + i];
            }
            if (this.t != null) {
                this.t.transform(coords, 0, coords, 0, count / 2);
            }
            this.pointIndex += count;
            return type;
        }
        
        @Override
        public int currentSegment(final float[] coords) {
            if (this.isDone()) {
                throw new NoSuchElementException(Messages.getString("awt.4B"));
            }
            final int type = this.p.types[this.typeIndex];
            final int count = GeneralPath.pointShift[type];
            System.arraycopy(this.p.points, this.pointIndex, coords, 0, count);
            if (this.t != null) {
                this.t.transform(coords, 0, coords, 0, count / 2);
            }
            this.pointIndex += count;
            return type;
        }
    }
}
