// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

public abstract class Dimension2D implements Cloneable
{
    protected Dimension2D() {
    }
    
    public abstract double getWidth();
    
    public abstract double getHeight();
    
    public abstract void setSize(final double p0, final double p1);
    
    public void setSize(final Dimension2D d) {
        this.setSize(d.getWidth(), d.getHeight());
    }
    
    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }
}
