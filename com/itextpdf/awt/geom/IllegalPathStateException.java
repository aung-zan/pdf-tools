// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

public class IllegalPathStateException extends RuntimeException
{
    private static final long serialVersionUID = -5158084205220481094L;
    
    public IllegalPathStateException() {
    }
    
    public IllegalPathStateException(final String s) {
        super(s);
    }
}
