// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

import java.io.Serializable;

public class Rectangle extends Rectangle2D implements Shape, Serializable
{
    private static final long serialVersionUID = -4345857070255674764L;
    public double x;
    public double y;
    public double width;
    public double height;
    
    public Rectangle() {
        this.setBounds(0, 0, 0, 0);
    }
    
    public Rectangle(final Point p) {
        this.setBounds(p.x, p.y, 0.0, 0.0);
    }
    
    public Rectangle(final Point p, final Dimension d) {
        this.setBounds(p.x, p.y, d.width, d.height);
    }
    
    public Rectangle(final double x, final double y, final double width, final double height) {
        this.setBounds(x, y, width, height);
    }
    
    public Rectangle(final int width, final int height) {
        this.setBounds(0, 0, width, height);
    }
    
    public Rectangle(final Rectangle r) {
        this.setBounds(r.x, r.y, r.width, r.height);
    }
    
    public Rectangle(final com.itextpdf.text.Rectangle r) {
        r.normalize();
        this.setBounds(r.getLeft(), r.getBottom(), r.getWidth(), r.getHeight());
    }
    
    public Rectangle(final Dimension d) {
        this.setBounds(0.0, 0.0, d.width, d.height);
    }
    
    @Override
    public double getX() {
        return this.x;
    }
    
    @Override
    public double getY() {
        return this.y;
    }
    
    @Override
    public double getHeight() {
        return this.height;
    }
    
    @Override
    public double getWidth() {
        return this.width;
    }
    
    @Override
    public boolean isEmpty() {
        return this.width <= 0.0 || this.height <= 0.0;
    }
    
    public Dimension getSize() {
        return new Dimension(this.width, this.height);
    }
    
    public void setSize(final int mx, final int my) {
        this.setSize(mx, (double)my);
    }
    
    public void setSize(final double width, final double height) {
        this.width = width;
        this.height = height;
    }
    
    public void setSize(final Dimension d) {
        this.setSize(d.width, d.height);
    }
    
    public Point getLocation() {
        return new Point(this.x, this.y);
    }
    
    public void setLocation(final int mx, final int my) {
        this.setLocation(mx, (double)my);
    }
    
    public void setLocation(final double x, final double y) {
        this.x = x;
        this.y = y;
    }
    
    public void setLocation(final Point p) {
        this.setLocation(p.x, p.y);
    }
    
    @Override
    public void setRect(final double x, final double y, final double width, final double height) {
        final int x2 = (int)Math.floor(x);
        final int y2 = (int)Math.floor(y);
        final int x3 = (int)Math.ceil(x + width);
        final int y3 = (int)Math.ceil(y + height);
        this.setBounds(x2, y2, x3 - x2, y3 - y2);
    }
    
    @Override
    public Rectangle getBounds() {
        return new Rectangle(this.x, this.y, this.width, this.height);
    }
    
    @Override
    public Rectangle2D getBounds2D() {
        return this.getBounds();
    }
    
    public void setBounds(final int x, final int y, final int width, final int height) {
        this.setBounds(x, y, width, (double)height);
    }
    
    public void setBounds(final double x, final double y, final double width, final double height) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
    }
    
    public void setBounds(final Rectangle r) {
        this.setBounds(r.x, r.y, r.width, r.height);
    }
    
    public void grow(final int mx, final int my) {
        this.translate(mx, (double)my);
    }
    
    public void grow(final double dx, final double dy) {
        this.x -= dx;
        this.y -= dy;
        this.width += dx + dx;
        this.height += dy + dy;
    }
    
    public void translate(final int mx, final int my) {
        this.translate(mx, (double)my);
    }
    
    public void translate(final double mx, final double my) {
        this.x += mx;
        this.y += my;
    }
    
    public void add(final int px, final int py) {
        this.add(px, (double)py);
    }
    
    @Override
    public void add(final double px, final double py) {
        final double x1 = Math.min(this.x, px);
        final double x2 = Math.max(this.x + this.width, px);
        final double y1 = Math.min(this.y, py);
        final double y2 = Math.max(this.y + this.height, py);
        this.setBounds(x1, y1, x2 - x1, y2 - y1);
    }
    
    public void add(final Point p) {
        this.add(p.x, p.y);
    }
    
    public void add(final Rectangle r) {
        final double x1 = Math.min(this.x, r.x);
        final double x2 = Math.max(this.x + this.width, r.x + r.width);
        final double y1 = Math.min(this.y, r.y);
        final double y2 = Math.max(this.y + this.height, r.y + r.height);
        this.setBounds(x1, y1, x2 - x1, y2 - y1);
    }
    
    public boolean contains(final int px, final int py) {
        return this.contains(px, (double)py);
    }
    
    @Override
    public boolean contains(double px, double py) {
        if (this.isEmpty()) {
            return false;
        }
        if (px < this.x || py < this.y) {
            return false;
        }
        px -= this.x;
        py -= this.y;
        return px < this.width && py < this.height;
    }
    
    public boolean contains(final Point p) {
        return this.contains(p.x, p.y);
    }
    
    public boolean contains(final int rx, final int ry, final int rw, final int rh) {
        return this.contains(rx, ry) && this.contains(rx + rw - 1, ry + rh - 1);
    }
    
    @Override
    public boolean contains(final double rx, final double ry, final double rw, final double rh) {
        return this.contains(rx, ry) && this.contains(rx + rw - 0.01, ry + rh - 0.01);
    }
    
    public boolean contains(final Rectangle r) {
        return this.contains(r.x, r.y, r.width, r.height);
    }
    
    @Override
    public Rectangle2D createIntersection(final Rectangle2D r) {
        if (r instanceof Rectangle) {
            return this.intersection((Rectangle)r);
        }
        final Rectangle2D dst = new Double();
        Rectangle2D.intersect(this, r, dst);
        return dst;
    }
    
    public Rectangle intersection(final Rectangle r) {
        final double x1 = Math.max(this.x, r.x);
        final double y1 = Math.max(this.y, r.y);
        final double x2 = Math.min(this.x + this.width, r.x + r.width);
        final double y2 = Math.min(this.y + this.height, r.y + r.height);
        return new Rectangle(x1, y1, x2 - x1, y2 - y1);
    }
    
    public boolean intersects(final Rectangle r) {
        return !this.intersection(r).isEmpty();
    }
    
    @Override
    public int outcode(final double px, final double py) {
        int code = 0;
        if (this.width <= 0.0) {
            code |= 0x5;
        }
        else if (px < this.x) {
            code |= 0x1;
        }
        else if (px > this.x + this.width) {
            code |= 0x4;
        }
        if (this.height <= 0.0) {
            code |= 0xA;
        }
        else if (py < this.y) {
            code |= 0x2;
        }
        else if (py > this.y + this.height) {
            code |= 0x8;
        }
        return code;
    }
    
    @Override
    public Rectangle2D createUnion(final Rectangle2D r) {
        if (r instanceof Rectangle) {
            return this.union((Rectangle)r);
        }
        final Rectangle2D dst = new Double();
        Rectangle2D.union(this, r, dst);
        return dst;
    }
    
    public Rectangle union(final Rectangle r) {
        final Rectangle dst = new Rectangle(this);
        dst.add(r);
        return dst;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Rectangle) {
            final Rectangle r = (Rectangle)obj;
            return r.x == this.x && r.y == this.y && r.width == this.width && r.height == this.height;
        }
        return false;
    }
    
    @Override
    public String toString() {
        return this.getClass().getName() + "[x=" + this.x + ",y=" + this.y + ",width=" + this.width + ",height=" + this.height + "]";
    }
}
