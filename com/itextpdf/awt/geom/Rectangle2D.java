// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

import java.util.NoSuchElementException;
import com.itextpdf.awt.geom.misc.Messages;
import com.itextpdf.awt.geom.misc.HashCode;

public abstract class Rectangle2D extends RectangularShape
{
    public static final int OUT_LEFT = 1;
    public static final int OUT_TOP = 2;
    public static final int OUT_RIGHT = 4;
    public static final int OUT_BOTTOM = 8;
    
    protected Rectangle2D() {
    }
    
    public abstract void setRect(final double p0, final double p1, final double p2, final double p3);
    
    public abstract int outcode(final double p0, final double p1);
    
    public abstract Rectangle2D createIntersection(final Rectangle2D p0);
    
    public abstract Rectangle2D createUnion(final Rectangle2D p0);
    
    public void setRect(final Rectangle2D r) {
        this.setRect(r.getX(), r.getY(), r.getWidth(), r.getHeight());
    }
    
    @Override
    public void setFrame(final double x, final double y, final double width, final double height) {
        this.setRect(x, y, width, height);
    }
    
    @Override
    public Rectangle2D getBounds2D() {
        return (Rectangle2D)this.clone();
    }
    
    public boolean intersectsLine(final double x1, final double y1, final double x2, final double y2) {
        final double rx1 = this.getX();
        final double ry1 = this.getY();
        final double rx2 = rx1 + this.getWidth();
        final double ry2 = ry1 + this.getHeight();
        return (rx1 <= x1 && x1 <= rx2 && ry1 <= y1 && y1 <= ry2) || (rx1 <= x2 && x2 <= rx2 && ry1 <= y2 && y2 <= ry2) || Line2D.linesIntersect(rx1, ry1, rx2, ry2, x1, y1, x2, y2) || Line2D.linesIntersect(rx2, ry1, rx1, ry2, x1, y1, x2, y2);
    }
    
    public boolean intersectsLine(final Line2D l) {
        return this.intersectsLine(l.getX1(), l.getY1(), l.getX2(), l.getY2());
    }
    
    public int outcode(final Point2D p) {
        return this.outcode(p.getX(), p.getY());
    }
    
    @Override
    public boolean contains(final double x, final double y) {
        if (this.isEmpty()) {
            return false;
        }
        final double x2 = this.getX();
        final double y2 = this.getY();
        final double x3 = x2 + this.getWidth();
        final double y3 = y2 + this.getHeight();
        return x2 <= x && x < x3 && y2 <= y && y < y3;
    }
    
    @Override
    public boolean intersects(final double x, final double y, final double width, final double height) {
        if (this.isEmpty() || width <= 0.0 || height <= 0.0) {
            return false;
        }
        final double x2 = this.getX();
        final double y2 = this.getY();
        final double x3 = x2 + this.getWidth();
        final double y3 = y2 + this.getHeight();
        return x + width > x2 && x < x3 && y + height > y2 && y < y3;
    }
    
    @Override
    public boolean contains(final double x, final double y, final double width, final double height) {
        if (this.isEmpty() || width <= 0.0 || height <= 0.0) {
            return false;
        }
        final double x2 = this.getX();
        final double y2 = this.getY();
        final double x3 = x2 + this.getWidth();
        final double y3 = y2 + this.getHeight();
        return x2 <= x && x + width <= x3 && y2 <= y && y + height <= y3;
    }
    
    public static void intersect(final Rectangle2D src1, final Rectangle2D src2, final Rectangle2D dst) {
        final double x1 = Math.max(src1.getMinX(), src2.getMinX());
        final double y1 = Math.max(src1.getMinY(), src2.getMinY());
        final double x2 = Math.min(src1.getMaxX(), src2.getMaxX());
        final double y2 = Math.min(src1.getMaxY(), src2.getMaxY());
        dst.setFrame(x1, y1, x2 - x1, y2 - y1);
    }
    
    public static void union(final Rectangle2D src1, final Rectangle2D src2, final Rectangle2D dst) {
        final double x1 = Math.min(src1.getMinX(), src2.getMinX());
        final double y1 = Math.min(src1.getMinY(), src2.getMinY());
        final double x2 = Math.max(src1.getMaxX(), src2.getMaxX());
        final double y2 = Math.max(src1.getMaxY(), src2.getMaxY());
        dst.setFrame(x1, y1, x2 - x1, y2 - y1);
    }
    
    public void add(final double x, final double y) {
        final double x2 = Math.min(this.getMinX(), x);
        final double y2 = Math.min(this.getMinY(), y);
        final double x3 = Math.max(this.getMaxX(), x);
        final double y3 = Math.max(this.getMaxY(), y);
        this.setRect(x2, y2, x3 - x2, y3 - y2);
    }
    
    public void add(final Point2D p) {
        this.add(p.getX(), p.getY());
    }
    
    public void add(final Rectangle2D r) {
        union(this, r, this);
    }
    
    @Override
    public PathIterator getPathIterator(final AffineTransform t) {
        return new Iterator(this, t);
    }
    
    @Override
    public PathIterator getPathIterator(final AffineTransform t, final double flatness) {
        return new Iterator(this, t);
    }
    
    @Override
    public int hashCode() {
        final HashCode hash = new HashCode();
        hash.append(this.getX());
        hash.append(this.getY());
        hash.append(this.getWidth());
        hash.append(this.getHeight());
        return hash.hashCode();
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Rectangle2D) {
            final Rectangle2D r = (Rectangle2D)obj;
            return this.getX() == r.getX() && this.getY() == r.getY() && this.getWidth() == r.getWidth() && this.getHeight() == r.getHeight();
        }
        return false;
    }
    
    public static class Float extends Rectangle2D
    {
        public float x;
        public float y;
        public float width;
        public float height;
        
        public Float() {
        }
        
        public Float(final float x, final float y, final float width, final float height) {
            this.setRect(x, y, width, height);
        }
        
        @Override
        public double getX() {
            return this.x;
        }
        
        @Override
        public double getY() {
            return this.y;
        }
        
        @Override
        public double getWidth() {
            return this.width;
        }
        
        @Override
        public double getHeight() {
            return this.height;
        }
        
        @Override
        public boolean isEmpty() {
            return this.width <= 0.0f || this.height <= 0.0f;
        }
        
        public void setRect(final float x, final float y, final float width, final float height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
        
        @Override
        public void setRect(final double x, final double y, final double width, final double height) {
            this.x = (float)x;
            this.y = (float)y;
            this.width = (float)width;
            this.height = (float)height;
        }
        
        @Override
        public void setRect(final Rectangle2D r) {
            this.x = (float)r.getX();
            this.y = (float)r.getY();
            this.width = (float)r.getWidth();
            this.height = (float)r.getHeight();
        }
        
        @Override
        public int outcode(final double px, final double py) {
            int code = 0;
            if (this.width <= 0.0f) {
                code |= 0x5;
            }
            else if (px < this.x) {
                code |= 0x1;
            }
            else if (px > this.x + this.width) {
                code |= 0x4;
            }
            if (this.height <= 0.0f) {
                code |= 0xA;
            }
            else if (py < this.y) {
                code |= 0x2;
            }
            else if (py > this.y + this.height) {
                code |= 0x8;
            }
            return code;
        }
        
        @Override
        public Rectangle2D getBounds2D() {
            return new Float(this.x, this.y, this.width, this.height);
        }
        
        @Override
        public Rectangle2D createIntersection(final Rectangle2D r) {
            Rectangle2D dst;
            if (r instanceof Double) {
                dst = new Double();
            }
            else {
                dst = new Float();
            }
            Rectangle2D.intersect(this, r, dst);
            return dst;
        }
        
        @Override
        public Rectangle2D createUnion(final Rectangle2D r) {
            Rectangle2D dst;
            if (r instanceof Double) {
                dst = new Double();
            }
            else {
                dst = new Float();
            }
            Rectangle2D.union(this, r, dst);
            return dst;
        }
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[x=" + this.x + ",y=" + this.y + ",width=" + this.width + ",height=" + this.height + "]";
        }
    }
    
    public static class Double extends Rectangle2D
    {
        public double x;
        public double y;
        public double width;
        public double height;
        
        public Double() {
        }
        
        public Double(final double x, final double y, final double width, final double height) {
            this.setRect(x, y, width, height);
        }
        
        @Override
        public double getX() {
            return this.x;
        }
        
        @Override
        public double getY() {
            return this.y;
        }
        
        @Override
        public double getWidth() {
            return this.width;
        }
        
        @Override
        public double getHeight() {
            return this.height;
        }
        
        @Override
        public boolean isEmpty() {
            return this.width <= 0.0 || this.height <= 0.0;
        }
        
        @Override
        public void setRect(final double x, final double y, final double width, final double height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
        
        @Override
        public void setRect(final Rectangle2D r) {
            this.x = r.getX();
            this.y = r.getY();
            this.width = r.getWidth();
            this.height = r.getHeight();
        }
        
        @Override
        public int outcode(final double px, final double py) {
            int code = 0;
            if (this.width <= 0.0) {
                code |= 0x5;
            }
            else if (px < this.x) {
                code |= 0x1;
            }
            else if (px > this.x + this.width) {
                code |= 0x4;
            }
            if (this.height <= 0.0) {
                code |= 0xA;
            }
            else if (py < this.y) {
                code |= 0x2;
            }
            else if (py > this.y + this.height) {
                code |= 0x8;
            }
            return code;
        }
        
        @Override
        public Rectangle2D getBounds2D() {
            return new Double(this.x, this.y, this.width, this.height);
        }
        
        @Override
        public Rectangle2D createIntersection(final Rectangle2D r) {
            final Rectangle2D dst = new Double();
            Rectangle2D.intersect(this, r, dst);
            return dst;
        }
        
        @Override
        public Rectangle2D createUnion(final Rectangle2D r) {
            final Rectangle2D dest = new Double();
            Rectangle2D.union(this, r, dest);
            return dest;
        }
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[x=" + this.x + ",y=" + this.y + ",width=" + this.width + ",height=" + this.height + "]";
        }
    }
    
    class Iterator implements PathIterator
    {
        double x;
        double y;
        double width;
        double height;
        AffineTransform t;
        int index;
        
        Iterator(final Rectangle2D r, final AffineTransform at) {
            this.x = r.getX();
            this.y = r.getY();
            this.width = r.getWidth();
            this.height = r.getHeight();
            this.t = at;
            if (this.width < 0.0 || this.height < 0.0) {
                this.index = 6;
            }
        }
        
        @Override
        public int getWindingRule() {
            return 1;
        }
        
        @Override
        public boolean isDone() {
            return this.index > 5;
        }
        
        @Override
        public void next() {
            ++this.index;
        }
        
        @Override
        public int currentSegment(final double[] coords) {
            if (this.isDone()) {
                throw new NoSuchElementException(Messages.getString("awt.4B"));
            }
            if (this.index == 5) {
                return 4;
            }
            int type;
            if (this.index == 0) {
                type = 0;
                coords[0] = this.x;
                coords[1] = this.y;
            }
            else {
                type = 1;
                switch (this.index) {
                    case 1: {
                        coords[0] = this.x + this.width;
                        coords[1] = this.y;
                        break;
                    }
                    case 2: {
                        coords[0] = this.x + this.width;
                        coords[1] = this.y + this.height;
                        break;
                    }
                    case 3: {
                        coords[0] = this.x;
                        coords[1] = this.y + this.height;
                        break;
                    }
                    case 4: {
                        coords[0] = this.x;
                        coords[1] = this.y;
                        break;
                    }
                }
            }
            if (this.t != null) {
                this.t.transform(coords, 0, coords, 0, 1);
            }
            return type;
        }
        
        @Override
        public int currentSegment(final float[] coords) {
            if (this.isDone()) {
                throw new NoSuchElementException(Messages.getString("awt.4B"));
            }
            if (this.index == 5) {
                return 4;
            }
            int type;
            if (this.index == 0) {
                coords[0] = (float)this.x;
                coords[1] = (float)this.y;
                type = 0;
            }
            else {
                type = 1;
                switch (this.index) {
                    case 1: {
                        coords[0] = (float)(this.x + this.width);
                        coords[1] = (float)this.y;
                        break;
                    }
                    case 2: {
                        coords[0] = (float)(this.x + this.width);
                        coords[1] = (float)(this.y + this.height);
                        break;
                    }
                    case 3: {
                        coords[0] = (float)this.x;
                        coords[1] = (float)(this.y + this.height);
                        break;
                    }
                    case 4: {
                        coords[0] = (float)this.x;
                        coords[1] = (float)this.y;
                        break;
                    }
                }
            }
            if (this.t != null) {
                this.t.transform(coords, 0, coords, 0, 1);
            }
            return type;
        }
    }
}
