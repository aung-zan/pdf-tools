// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

public abstract class RectangularShape implements Shape, Cloneable
{
    protected RectangularShape() {
    }
    
    public abstract double getX();
    
    public abstract double getY();
    
    public abstract double getWidth();
    
    public abstract double getHeight();
    
    public abstract boolean isEmpty();
    
    public abstract void setFrame(final double p0, final double p1, final double p2, final double p3);
    
    public double getMinX() {
        return this.getX();
    }
    
    public double getMinY() {
        return this.getY();
    }
    
    public double getMaxX() {
        return this.getX() + this.getWidth();
    }
    
    public double getMaxY() {
        return this.getY() + this.getHeight();
    }
    
    public double getCenterX() {
        return this.getX() + this.getWidth() / 2.0;
    }
    
    public double getCenterY() {
        return this.getY() + this.getHeight() / 2.0;
    }
    
    public Rectangle2D getFrame() {
        return new Rectangle2D.Double(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    }
    
    public void setFrame(final Point2D loc, final Dimension2D size) {
        this.setFrame(loc.getX(), loc.getY(), size.getWidth(), size.getHeight());
    }
    
    public void setFrame(final Rectangle2D r) {
        this.setFrame(r.getX(), r.getY(), r.getWidth(), r.getHeight());
    }
    
    public void setFrameFromDiagonal(final double x1, final double y1, final double x2, final double y2) {
        double rx;
        double rw;
        if (x1 < x2) {
            rx = x1;
            rw = x2 - x1;
        }
        else {
            rx = x2;
            rw = x1 - x2;
        }
        double ry;
        double rh;
        if (y1 < y2) {
            ry = y1;
            rh = y2 - y1;
        }
        else {
            ry = y2;
            rh = y1 - y2;
        }
        this.setFrame(rx, ry, rw, rh);
    }
    
    public void setFrameFromDiagonal(final Point2D p1, final Point2D p2) {
        this.setFrameFromDiagonal(p1.getX(), p1.getY(), p2.getX(), p2.getY());
    }
    
    public void setFrameFromCenter(final double centerX, final double centerY, final double cornerX, final double cornerY) {
        final double width = Math.abs(cornerX - centerX);
        final double height = Math.abs(cornerY - centerY);
        this.setFrame(centerX - width, centerY - height, width * 2.0, height * 2.0);
    }
    
    public void setFrameFromCenter(final Point2D center, final Point2D corner) {
        this.setFrameFromCenter(center.getX(), center.getY(), corner.getX(), corner.getY());
    }
    
    @Override
    public boolean contains(final Point2D point) {
        return this.contains(point.getX(), point.getY());
    }
    
    @Override
    public boolean intersects(final Rectangle2D rect) {
        return this.intersects(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
    }
    
    @Override
    public boolean contains(final Rectangle2D rect) {
        return this.contains(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
    }
    
    @Override
    public Rectangle getBounds() {
        final int x1 = (int)Math.floor(this.getMinX());
        final int y1 = (int)Math.floor(this.getMinY());
        final int x2 = (int)Math.ceil(this.getMaxX());
        final int y2 = (int)Math.ceil(this.getMaxY());
        return new Rectangle(x1, y1, x2 - x1, y2 - y1);
    }
    
    @Override
    public PathIterator getPathIterator(final AffineTransform t, final double flatness) {
        return new FlatteningPathIterator(this.getPathIterator(t), flatness);
    }
    
    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }
}
