// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

import com.itextpdf.awt.geom.misc.HashCode;

public abstract class Point2D implements Cloneable
{
    protected Point2D() {
    }
    
    public abstract double getX();
    
    public abstract double getY();
    
    public abstract void setLocation(final double p0, final double p1);
    
    public void setLocation(final Point2D p) {
        this.setLocation(p.getX(), p.getY());
    }
    
    public static double distanceSq(final double x1, final double y1, double x2, double y2) {
        x2 -= x1;
        y2 -= y1;
        return x2 * x2 + y2 * y2;
    }
    
    public double distanceSq(final double px, final double py) {
        return distanceSq(this.getX(), this.getY(), px, py);
    }
    
    public double distanceSq(final Point2D p) {
        return distanceSq(this.getX(), this.getY(), p.getX(), p.getY());
    }
    
    public static double distance(final double x1, final double y1, final double x2, final double y2) {
        return Math.sqrt(distanceSq(x1, y1, x2, y2));
    }
    
    public double distance(final double px, final double py) {
        return Math.sqrt(this.distanceSq(px, py));
    }
    
    public double distance(final Point2D p) {
        return Math.sqrt(this.distanceSq(p));
    }
    
    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }
    
    @Override
    public int hashCode() {
        final HashCode hash = new HashCode();
        hash.append(this.getX());
        hash.append(this.getY());
        return hash.hashCode();
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Point2D) {
            final Point2D p = (Point2D)obj;
            return this.getX() == p.getX() && this.getY() == p.getY();
        }
        return false;
    }
    
    public static class Float extends Point2D
    {
        public float x;
        public float y;
        
        public Float() {
        }
        
        public Float(final float x, final float y) {
            this.x = x;
            this.y = y;
        }
        
        @Override
        public double getX() {
            return this.x;
        }
        
        @Override
        public double getY() {
            return this.y;
        }
        
        public void setLocation(final float x, final float y) {
            this.x = x;
            this.y = y;
        }
        
        @Override
        public void setLocation(final double x, final double y) {
            this.x = (float)x;
            this.y = (float)y;
        }
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[x=" + this.x + ",y=" + this.y + "]";
        }
    }
    
    public static class Double extends Point2D
    {
        public double x;
        public double y;
        
        public Double() {
        }
        
        public Double(final double x, final double y) {
            this.x = x;
            this.y = y;
        }
        
        @Override
        public double getX() {
            return this.x;
        }
        
        @Override
        public double getY() {
            return this.y;
        }
        
        @Override
        public void setLocation(final double x, final double y) {
            this.x = x;
            this.y = y;
        }
        
        @Override
        public String toString() {
            return this.getClass().getName() + "[x=" + this.x + ",y=" + this.y + "]";
        }
    }
}
