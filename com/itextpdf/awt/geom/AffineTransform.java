// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import com.itextpdf.awt.geom.misc.HashCode;
import com.itextpdf.awt.geom.misc.Messages;
import java.io.Serializable;

public class AffineTransform implements Cloneable, Serializable
{
    private static final long serialVersionUID = 1330973210523860834L;
    public static final int TYPE_IDENTITY = 0;
    public static final int TYPE_TRANSLATION = 1;
    public static final int TYPE_UNIFORM_SCALE = 2;
    public static final int TYPE_GENERAL_SCALE = 4;
    public static final int TYPE_QUADRANT_ROTATION = 8;
    public static final int TYPE_GENERAL_ROTATION = 16;
    public static final int TYPE_GENERAL_TRANSFORM = 32;
    public static final int TYPE_FLIP = 64;
    public static final int TYPE_MASK_SCALE = 6;
    public static final int TYPE_MASK_ROTATION = 24;
    static final int TYPE_UNKNOWN = -1;
    static final double ZERO = 1.0E-10;
    double m00;
    double m10;
    double m01;
    double m11;
    double m02;
    double m12;
    transient int type;
    
    public AffineTransform() {
        this.type = 0;
        final double n = 1.0;
        this.m11 = n;
        this.m00 = n;
        final double n2 = 0.0;
        this.m12 = n2;
        this.m02 = n2;
        this.m01 = n2;
        this.m10 = n2;
    }
    
    public AffineTransform(final AffineTransform t) {
        this.type = t.type;
        this.m00 = t.m00;
        this.m10 = t.m10;
        this.m01 = t.m01;
        this.m11 = t.m11;
        this.m02 = t.m02;
        this.m12 = t.m12;
    }
    
    public AffineTransform(final float m00, final float m10, final float m01, final float m11, final float m02, final float m12) {
        this.type = -1;
        this.m00 = m00;
        this.m10 = m10;
        this.m01 = m01;
        this.m11 = m11;
        this.m02 = m02;
        this.m12 = m12;
    }
    
    public AffineTransform(final double m00, final double m10, final double m01, final double m11, final double m02, final double m12) {
        this.type = -1;
        this.m00 = m00;
        this.m10 = m10;
        this.m01 = m01;
        this.m11 = m11;
        this.m02 = m02;
        this.m12 = m12;
    }
    
    public AffineTransform(final float[] matrix) {
        this.type = -1;
        this.m00 = matrix[0];
        this.m10 = matrix[1];
        this.m01 = matrix[2];
        this.m11 = matrix[3];
        if (matrix.length > 4) {
            this.m02 = matrix[4];
            this.m12 = matrix[5];
        }
    }
    
    public AffineTransform(final double[] matrix) {
        this.type = -1;
        this.m00 = matrix[0];
        this.m10 = matrix[1];
        this.m01 = matrix[2];
        this.m11 = matrix[3];
        if (matrix.length > 4) {
            this.m02 = matrix[4];
            this.m12 = matrix[5];
        }
    }
    
    public int getType() {
        if (this.type != -1) {
            return this.type;
        }
        int type = 0;
        if (this.m00 * this.m01 + this.m10 * this.m11 != 0.0) {
            type |= 0x20;
            return type;
        }
        if (this.m02 != 0.0 || this.m12 != 0.0) {
            type |= 0x1;
        }
        else if (this.m00 == 1.0 && this.m11 == 1.0 && this.m01 == 0.0 && this.m10 == 0.0) {
            type = 0;
            return type;
        }
        if (this.m00 * this.m11 - this.m01 * this.m10 < 0.0) {
            type |= 0x40;
        }
        final double dx = this.m00 * this.m00 + this.m10 * this.m10;
        final double dy = this.m01 * this.m01 + this.m11 * this.m11;
        if (dx != dy) {
            type |= 0x4;
        }
        else if (dx != 1.0) {
            type |= 0x2;
        }
        if ((this.m00 == 0.0 && this.m11 == 0.0) || (this.m10 == 0.0 && this.m01 == 0.0 && (this.m00 < 0.0 || this.m11 < 0.0))) {
            type |= 0x8;
        }
        else if (this.m01 != 0.0 || this.m10 != 0.0) {
            type |= 0x10;
        }
        return type;
    }
    
    public double getScaleX() {
        return this.m00;
    }
    
    public double getScaleY() {
        return this.m11;
    }
    
    public double getShearX() {
        return this.m01;
    }
    
    public double getShearY() {
        return this.m10;
    }
    
    public double getTranslateX() {
        return this.m02;
    }
    
    public double getTranslateY() {
        return this.m12;
    }
    
    public boolean isIdentity() {
        return this.getType() == 0;
    }
    
    public void getMatrix(final double[] matrix) {
        matrix[0] = this.m00;
        matrix[1] = this.m10;
        matrix[2] = this.m01;
        matrix[3] = this.m11;
        if (matrix.length > 4) {
            matrix[4] = this.m02;
            matrix[5] = this.m12;
        }
    }
    
    public double getDeterminant() {
        return this.m00 * this.m11 - this.m01 * this.m10;
    }
    
    public void setTransform(final double m00, final double m10, final double m01, final double m11, final double m02, final double m12) {
        this.type = -1;
        this.m00 = m00;
        this.m10 = m10;
        this.m01 = m01;
        this.m11 = m11;
        this.m02 = m02;
        this.m12 = m12;
    }
    
    public void setTransform(final AffineTransform t) {
        this.type = t.type;
        this.setTransform(t.m00, t.m10, t.m01, t.m11, t.m02, t.m12);
    }
    
    public void setToIdentity() {
        this.type = 0;
        final double n = 1.0;
        this.m11 = n;
        this.m00 = n;
        final double n2 = 0.0;
        this.m12 = n2;
        this.m02 = n2;
        this.m01 = n2;
        this.m10 = n2;
    }
    
    public void setToTranslation(final double mx, final double my) {
        final double n = 1.0;
        this.m11 = n;
        this.m00 = n;
        final double n2 = 0.0;
        this.m10 = n2;
        this.m01 = n2;
        this.m02 = mx;
        this.m12 = my;
        if (mx == 0.0 && my == 0.0) {
            this.type = 0;
        }
        else {
            this.type = 1;
        }
    }
    
    public void setToScale(final double scx, final double scy) {
        this.m00 = scx;
        this.m11 = scy;
        final double n = 0.0;
        this.m12 = n;
        this.m02 = n;
        this.m01 = n;
        this.m10 = n;
        if (scx != 1.0 || scy != 1.0) {
            this.type = -1;
        }
        else {
            this.type = 0;
        }
    }
    
    public void setToShear(final double shx, final double shy) {
        final double n = 1.0;
        this.m11 = n;
        this.m00 = n;
        final double n2 = 0.0;
        this.m12 = n2;
        this.m02 = n2;
        this.m01 = shx;
        this.m10 = shy;
        if (shx != 0.0 || shy != 0.0) {
            this.type = -1;
        }
        else {
            this.type = 0;
        }
    }
    
    public void setToRotation(final double angle) {
        double sin = Math.sin(angle);
        double cos = Math.cos(angle);
        if (Math.abs(cos) < 1.0E-10) {
            cos = 0.0;
            sin = ((sin > 0.0) ? 1.0 : -1.0);
        }
        else if (Math.abs(sin) < 1.0E-10) {
            sin = 0.0;
            cos = ((cos > 0.0) ? 1.0 : -1.0);
        }
        final double n = cos;
        this.m11 = n;
        this.m00 = n;
        this.m01 = -sin;
        this.m10 = sin;
        final double n2 = 0.0;
        this.m12 = n2;
        this.m02 = n2;
        this.type = -1;
    }
    
    public void setToRotation(final double angle, final double px, final double py) {
        this.setToRotation(angle);
        this.m02 = px * (1.0 - this.m00) + py * this.m10;
        this.m12 = py * (1.0 - this.m00) - px * this.m10;
        this.type = -1;
    }
    
    public static AffineTransform getTranslateInstance(final double mx, final double my) {
        final AffineTransform t = new AffineTransform();
        t.setToTranslation(mx, my);
        return t;
    }
    
    public static AffineTransform getScaleInstance(final double scx, final double scY) {
        final AffineTransform t = new AffineTransform();
        t.setToScale(scx, scY);
        return t;
    }
    
    public static AffineTransform getShearInstance(final double shx, final double shy) {
        final AffineTransform m = new AffineTransform();
        m.setToShear(shx, shy);
        return m;
    }
    
    public static AffineTransform getRotateInstance(final double angle) {
        final AffineTransform t = new AffineTransform();
        t.setToRotation(angle);
        return t;
    }
    
    public static AffineTransform getRotateInstance(final double angle, final double x, final double y) {
        final AffineTransform t = new AffineTransform();
        t.setToRotation(angle, x, y);
        return t;
    }
    
    public void translate(final double mx, final double my) {
        this.concatenate(getTranslateInstance(mx, my));
    }
    
    public void scale(final double scx, final double scy) {
        this.concatenate(getScaleInstance(scx, scy));
    }
    
    public void shear(final double shx, final double shy) {
        this.concatenate(getShearInstance(shx, shy));
    }
    
    public void rotate(final double angle) {
        this.concatenate(getRotateInstance(angle));
    }
    
    public void rotate(final double angle, final double px, final double py) {
        this.concatenate(getRotateInstance(angle, px, py));
    }
    
    AffineTransform multiply(final AffineTransform t1, final AffineTransform t2) {
        return new AffineTransform(t1.m00 * t2.m00 + t1.m10 * t2.m01, t1.m00 * t2.m10 + t1.m10 * t2.m11, t1.m01 * t2.m00 + t1.m11 * t2.m01, t1.m01 * t2.m10 + t1.m11 * t2.m11, t1.m02 * t2.m00 + t1.m12 * t2.m01 + t2.m02, t1.m02 * t2.m10 + t1.m12 * t2.m11 + t2.m12);
    }
    
    public void concatenate(final AffineTransform t) {
        this.setTransform(this.multiply(t, this));
    }
    
    public void preConcatenate(final AffineTransform t) {
        this.setTransform(this.multiply(this, t));
    }
    
    public AffineTransform createInverse() throws NoninvertibleTransformException {
        final double det = this.getDeterminant();
        if (Math.abs(det) < 1.0E-10) {
            throw new NoninvertibleTransformException(Messages.getString("awt.204"));
        }
        return new AffineTransform(this.m11 / det, -this.m10 / det, -this.m01 / det, this.m00 / det, (this.m01 * this.m12 - this.m11 * this.m02) / det, (this.m10 * this.m02 - this.m00 * this.m12) / det);
    }
    
    public Point2D transform(final Point2D src, Point2D dst) {
        if (dst == null) {
            if (src instanceof Point2D.Double) {
                dst = new Point2D.Double();
            }
            else {
                dst = new Point2D.Float();
            }
        }
        final double x = src.getX();
        final double y = src.getY();
        dst.setLocation(x * this.m00 + y * this.m01 + this.m02, x * this.m10 + y * this.m11 + this.m12);
        return dst;
    }
    
    public void transform(final Point2D[] src, int srcOff, final Point2D[] dst, int dstOff, int length) {
        while (--length >= 0) {
            final Point2D srcPoint = src[srcOff++];
            final double x = srcPoint.getX();
            final double y = srcPoint.getY();
            Point2D dstPoint = dst[dstOff];
            if (dstPoint == null) {
                if (srcPoint instanceof Point2D.Double) {
                    dstPoint = new Point2D.Double();
                }
                else {
                    dstPoint = new Point2D.Float();
                }
            }
            dstPoint.setLocation(x * this.m00 + y * this.m01 + this.m02, x * this.m10 + y * this.m11 + this.m12);
            dst[dstOff++] = dstPoint;
        }
    }
    
    public void transform(final double[] src, int srcOff, final double[] dst, int dstOff, int length) {
        int step = 2;
        if (src == dst && srcOff < dstOff && dstOff < srcOff + length * 2) {
            srcOff = srcOff + length * 2 - 2;
            dstOff = dstOff + length * 2 - 2;
            step = -2;
        }
        while (--length >= 0) {
            final double x = src[srcOff + 0];
            final double y = src[srcOff + 1];
            dst[dstOff + 0] = x * this.m00 + y * this.m01 + this.m02;
            dst[dstOff + 1] = x * this.m10 + y * this.m11 + this.m12;
            srcOff += step;
            dstOff += step;
        }
    }
    
    public void transform(final float[] src, int srcOff, final float[] dst, int dstOff, int length) {
        int step = 2;
        if (src == dst && srcOff < dstOff && dstOff < srcOff + length * 2) {
            srcOff = srcOff + length * 2 - 2;
            dstOff = dstOff + length * 2 - 2;
            step = -2;
        }
        while (--length >= 0) {
            final float x = src[srcOff + 0];
            final float y = src[srcOff + 1];
            dst[dstOff + 0] = (float)(x * this.m00 + y * this.m01 + this.m02);
            dst[dstOff + 1] = (float)(x * this.m10 + y * this.m11 + this.m12);
            srcOff += step;
            dstOff += step;
        }
    }
    
    public void transform(final float[] src, int srcOff, final double[] dst, int dstOff, int length) {
        while (--length >= 0) {
            final float x = src[srcOff++];
            final float y = src[srcOff++];
            dst[dstOff++] = x * this.m00 + y * this.m01 + this.m02;
            dst[dstOff++] = x * this.m10 + y * this.m11 + this.m12;
        }
    }
    
    public void transform(final double[] src, int srcOff, final float[] dst, int dstOff, int length) {
        while (--length >= 0) {
            final double x = src[srcOff++];
            final double y = src[srcOff++];
            dst[dstOff++] = (float)(x * this.m00 + y * this.m01 + this.m02);
            dst[dstOff++] = (float)(x * this.m10 + y * this.m11 + this.m12);
        }
    }
    
    public Point2D deltaTransform(final Point2D src, Point2D dst) {
        if (dst == null) {
            if (src instanceof Point2D.Double) {
                dst = new Point2D.Double();
            }
            else {
                dst = new Point2D.Float();
            }
        }
        final double x = src.getX();
        final double y = src.getY();
        dst.setLocation(x * this.m00 + y * this.m01, x * this.m10 + y * this.m11);
        return dst;
    }
    
    public void deltaTransform(final double[] src, int srcOff, final double[] dst, int dstOff, int length) {
        while (--length >= 0) {
            final double x = src[srcOff++];
            final double y = src[srcOff++];
            dst[dstOff++] = x * this.m00 + y * this.m01;
            dst[dstOff++] = x * this.m10 + y * this.m11;
        }
    }
    
    public Point2D inverseTransform(final Point2D src, Point2D dst) throws NoninvertibleTransformException {
        final double det = this.getDeterminant();
        if (Math.abs(det) < 1.0E-10) {
            throw new NoninvertibleTransformException(Messages.getString("awt.204"));
        }
        if (dst == null) {
            if (src instanceof Point2D.Double) {
                dst = new Point2D.Double();
            }
            else {
                dst = new Point2D.Float();
            }
        }
        final double x = src.getX() - this.m02;
        final double y = src.getY() - this.m12;
        dst.setLocation((x * this.m11 - y * this.m01) / det, (y * this.m00 - x * this.m10) / det);
        return dst;
    }
    
    public void inverseTransform(final double[] src, int srcOff, final double[] dst, int dstOff, int length) throws NoninvertibleTransformException {
        final double det = this.getDeterminant();
        if (Math.abs(det) < 1.0E-10) {
            throw new NoninvertibleTransformException(Messages.getString("awt.204"));
        }
        while (--length >= 0) {
            final double x = src[srcOff++] - this.m02;
            final double y = src[srcOff++] - this.m12;
            dst[dstOff++] = (x * this.m11 - y * this.m01) / det;
            dst[dstOff++] = (y * this.m00 - x * this.m10) / det;
        }
    }
    
    public void inverseTransform(final float[] src, int srcOff, final float[] dst, int dstOff, int length) throws NoninvertibleTransformException {
        final float det = (float)this.getDeterminant();
        if (Math.abs(det) < 1.0E-10) {
            throw new NoninvertibleTransformException(Messages.getString("awt.204"));
        }
        while (--length >= 0) {
            final float x = src[srcOff++] - (float)this.m02;
            final float y = src[srcOff++] - (float)this.m12;
            dst[dstOff++] = (x * (float)this.m11 - y * (float)this.m01) / det;
            dst[dstOff++] = (y * (float)this.m00 - x * (float)this.m10) / det;
        }
    }
    
    public Shape createTransformedShape(final Shape src) {
        if (src == null) {
            return null;
        }
        if (src instanceof GeneralPath) {
            return ((GeneralPath)src).createTransformedShape(this);
        }
        final PathIterator path = src.getPathIterator(this);
        final GeneralPath dst = new GeneralPath(path.getWindingRule());
        dst.append(path, false);
        return dst;
    }
    
    @Override
    public String toString() {
        return this.getClass().getName() + "[[" + this.m00 + ", " + this.m01 + ", " + this.m02 + "], [" + this.m10 + ", " + this.m11 + ", " + this.m12 + "]]";
    }
    
    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }
    
    @Override
    public int hashCode() {
        final HashCode hash = new HashCode();
        hash.append(this.m00);
        hash.append(this.m01);
        hash.append(this.m02);
        hash.append(this.m10);
        hash.append(this.m11);
        hash.append(this.m12);
        return hash.hashCode();
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof AffineTransform) {
            final AffineTransform t = (AffineTransform)obj;
            return this.m00 == t.m00 && this.m01 == t.m01 && this.m02 == t.m02 && this.m10 == t.m10 && this.m11 == t.m11 && this.m12 == t.m12;
        }
        return false;
    }
    
    private void writeObject(final ObjectOutputStream stream) throws IOException {
        stream.defaultWriteObject();
    }
    
    private void readObject(final ObjectInputStream stream) throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        this.type = -1;
    }
}
