// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt.geom;

import java.util.NoSuchElementException;
import com.itextpdf.awt.geom.misc.Messages;
import com.itextpdf.awt.geom.gl.Crossing;

public abstract class QuadCurve2D implements Shape, Cloneable
{
    protected QuadCurve2D() {
    }
    
    public abstract double getX1();
    
    public abstract double getY1();
    
    public abstract Point2D getP1();
    
    public abstract double getCtrlX();
    
    public abstract double getCtrlY();
    
    public abstract Point2D getCtrlPt();
    
    public abstract double getX2();
    
    public abstract double getY2();
    
    public abstract Point2D getP2();
    
    public abstract void setCurve(final double p0, final double p1, final double p2, final double p3, final double p4, final double p5);
    
    public void setCurve(final Point2D p1, final Point2D cp, final Point2D p2) {
        this.setCurve(p1.getX(), p1.getY(), cp.getX(), cp.getY(), p2.getX(), p2.getY());
    }
    
    public void setCurve(final double[] coords, final int offset) {
        this.setCurve(coords[offset + 0], coords[offset + 1], coords[offset + 2], coords[offset + 3], coords[offset + 4], coords[offset + 5]);
    }
    
    public void setCurve(final Point2D[] points, final int offset) {
        this.setCurve(points[offset + 0].getX(), points[offset + 0].getY(), points[offset + 1].getX(), points[offset + 1].getY(), points[offset + 2].getX(), points[offset + 2].getY());
    }
    
    public void setCurve(final QuadCurve2D curve) {
        this.setCurve(curve.getX1(), curve.getY1(), curve.getCtrlX(), curve.getCtrlY(), curve.getX2(), curve.getY2());
    }
    
    public double getFlatnessSq() {
        return Line2D.ptSegDistSq(this.getX1(), this.getY1(), this.getX2(), this.getY2(), this.getCtrlX(), this.getCtrlY());
    }
    
    public static double getFlatnessSq(final double x1, final double y1, final double ctrlx, final double ctrly, final double x2, final double y2) {
        return Line2D.ptSegDistSq(x1, y1, x2, y2, ctrlx, ctrly);
    }
    
    public static double getFlatnessSq(final double[] coords, final int offset) {
        return Line2D.ptSegDistSq(coords[offset + 0], coords[offset + 1], coords[offset + 4], coords[offset + 5], coords[offset + 2], coords[offset + 3]);
    }
    
    public double getFlatness() {
        return Line2D.ptSegDist(this.getX1(), this.getY1(), this.getX2(), this.getY2(), this.getCtrlX(), this.getCtrlY());
    }
    
    public static double getFlatness(final double x1, final double y1, final double ctrlx, final double ctrly, final double x2, final double y2) {
        return Line2D.ptSegDist(x1, y1, x2, y2, ctrlx, ctrly);
    }
    
    public static double getFlatness(final double[] coords, final int offset) {
        return Line2D.ptSegDist(coords[offset + 0], coords[offset + 1], coords[offset + 4], coords[offset + 5], coords[offset + 2], coords[offset + 3]);
    }
    
    public void subdivide(final QuadCurve2D left, final QuadCurve2D right) {
        subdivide(this, left, right);
    }
    
    public static void subdivide(final QuadCurve2D src, final QuadCurve2D left, final QuadCurve2D right) {
        final double x1 = src.getX1();
        final double y1 = src.getY1();
        double cx = src.getCtrlX();
        double cy = src.getCtrlY();
        final double x2 = src.getX2();
        final double y2 = src.getY2();
        final double cx2 = (x1 + cx) / 2.0;
        final double cy2 = (y1 + cy) / 2.0;
        final double cx3 = (x2 + cx) / 2.0;
        final double cy3 = (y2 + cy) / 2.0;
        cx = (cx2 + cx3) / 2.0;
        cy = (cy2 + cy3) / 2.0;
        if (left != null) {
            left.setCurve(x1, y1, cx2, cy2, cx, cy);
        }
        if (right != null) {
            right.setCurve(cx, cy, cx3, cy3, x2, y2);
        }
    }
    
    public static void subdivide(final double[] src, final int srcoff, final double[] left, final int leftOff, final double[] right, final int rightOff) {
        final double x1 = src[srcoff + 0];
        final double y1 = src[srcoff + 1];
        double cx = src[srcoff + 2];
        double cy = src[srcoff + 3];
        final double x2 = src[srcoff + 4];
        final double y2 = src[srcoff + 5];
        final double cx2 = (x1 + cx) / 2.0;
        final double cy2 = (y1 + cy) / 2.0;
        final double cx3 = (x2 + cx) / 2.0;
        final double cy3 = (y2 + cy) / 2.0;
        cx = (cx2 + cx3) / 2.0;
        cy = (cy2 + cy3) / 2.0;
        if (left != null) {
            left[leftOff + 0] = x1;
            left[leftOff + 1] = y1;
            left[leftOff + 2] = cx2;
            left[leftOff + 3] = cy2;
            left[leftOff + 4] = cx;
            left[leftOff + 5] = cy;
        }
        if (right != null) {
            right[rightOff + 0] = cx;
            right[rightOff + 1] = cy;
            right[rightOff + 2] = cx3;
            right[rightOff + 3] = cy3;
            right[rightOff + 4] = x2;
            right[rightOff + 5] = y2;
        }
    }
    
    public static int solveQuadratic(final double[] eqn) {
        return solveQuadratic(eqn, eqn);
    }
    
    public static int solveQuadratic(final double[] eqn, final double[] res) {
        return Crossing.solveQuad(eqn, res);
    }
    
    @Override
    public boolean contains(final double px, final double py) {
        return Crossing.isInsideEvenOdd(Crossing.crossShape(this, px, py));
    }
    
    @Override
    public boolean contains(final double rx, final double ry, final double rw, final double rh) {
        final int cross = Crossing.intersectShape(this, rx, ry, rw, rh);
        return cross != 255 && Crossing.isInsideEvenOdd(cross);
    }
    
    @Override
    public boolean intersects(final double rx, final double ry, final double rw, final double rh) {
        final int cross = Crossing.intersectShape(this, rx, ry, rw, rh);
        return cross == 255 || Crossing.isInsideEvenOdd(cross);
    }
    
    @Override
    public boolean contains(final Point2D p) {
        return this.contains(p.getX(), p.getY());
    }
    
    @Override
    public boolean intersects(final Rectangle2D r) {
        return this.intersects(r.getX(), r.getY(), r.getWidth(), r.getHeight());
    }
    
    @Override
    public boolean contains(final Rectangle2D r) {
        return this.contains(r.getX(), r.getY(), r.getWidth(), r.getHeight());
    }
    
    @Override
    public Rectangle getBounds() {
        return this.getBounds2D().getBounds();
    }
    
    @Override
    public PathIterator getPathIterator(final AffineTransform t) {
        return new Iterator(this, t);
    }
    
    @Override
    public PathIterator getPathIterator(final AffineTransform t, final double flatness) {
        return new FlatteningPathIterator(this.getPathIterator(t), flatness);
    }
    
    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }
    
    public static class Float extends QuadCurve2D
    {
        public float x1;
        public float y1;
        public float ctrlx;
        public float ctrly;
        public float x2;
        public float y2;
        
        public Float() {
        }
        
        public Float(final float x1, final float y1, final float ctrlx, final float ctrly, final float x2, final float y2) {
            this.setCurve(x1, y1, ctrlx, ctrly, x2, y2);
        }
        
        @Override
        public double getX1() {
            return this.x1;
        }
        
        @Override
        public double getY1() {
            return this.y1;
        }
        
        @Override
        public double getCtrlX() {
            return this.ctrlx;
        }
        
        @Override
        public double getCtrlY() {
            return this.ctrly;
        }
        
        @Override
        public double getX2() {
            return this.x2;
        }
        
        @Override
        public double getY2() {
            return this.y2;
        }
        
        @Override
        public Point2D getP1() {
            return new Point2D.Float(this.x1, this.y1);
        }
        
        @Override
        public Point2D getCtrlPt() {
            return new Point2D.Float(this.ctrlx, this.ctrly);
        }
        
        @Override
        public Point2D getP2() {
            return new Point2D.Float(this.x2, this.y2);
        }
        
        @Override
        public void setCurve(final double x1, final double y1, final double ctrlx, final double ctrly, final double x2, final double y2) {
            this.x1 = (float)x1;
            this.y1 = (float)y1;
            this.ctrlx = (float)ctrlx;
            this.ctrly = (float)ctrly;
            this.x2 = (float)x2;
            this.y2 = (float)y2;
        }
        
        public void setCurve(final float x1, final float y1, final float ctrlx, final float ctrly, final float x2, final float y2) {
            this.x1 = x1;
            this.y1 = y1;
            this.ctrlx = ctrlx;
            this.ctrly = ctrly;
            this.x2 = x2;
            this.y2 = y2;
        }
        
        @Override
        public Rectangle2D getBounds2D() {
            final float rx0 = Math.min(Math.min(this.x1, this.x2), this.ctrlx);
            final float ry0 = Math.min(Math.min(this.y1, this.y2), this.ctrly);
            final float rx2 = Math.max(Math.max(this.x1, this.x2), this.ctrlx);
            final float ry2 = Math.max(Math.max(this.y1, this.y2), this.ctrly);
            return new Rectangle2D.Float(rx0, ry0, rx2 - rx0, ry2 - ry0);
        }
    }
    
    public static class Double extends QuadCurve2D
    {
        public double x1;
        public double y1;
        public double ctrlx;
        public double ctrly;
        public double x2;
        public double y2;
        
        public Double() {
        }
        
        public Double(final double x1, final double y1, final double ctrlx, final double ctrly, final double x2, final double y2) {
            this.setCurve(x1, y1, ctrlx, ctrly, x2, y2);
        }
        
        @Override
        public double getX1() {
            return this.x1;
        }
        
        @Override
        public double getY1() {
            return this.y1;
        }
        
        @Override
        public double getCtrlX() {
            return this.ctrlx;
        }
        
        @Override
        public double getCtrlY() {
            return this.ctrly;
        }
        
        @Override
        public double getX2() {
            return this.x2;
        }
        
        @Override
        public double getY2() {
            return this.y2;
        }
        
        @Override
        public Point2D getP1() {
            return new Point2D.Double(this.x1, this.y1);
        }
        
        @Override
        public Point2D getCtrlPt() {
            return new Point2D.Double(this.ctrlx, this.ctrly);
        }
        
        @Override
        public Point2D getP2() {
            return new Point2D.Double(this.x2, this.y2);
        }
        
        @Override
        public void setCurve(final double x1, final double y1, final double ctrlx, final double ctrly, final double x2, final double y2) {
            this.x1 = x1;
            this.y1 = y1;
            this.ctrlx = ctrlx;
            this.ctrly = ctrly;
            this.x2 = x2;
            this.y2 = y2;
        }
        
        @Override
        public Rectangle2D getBounds2D() {
            final double rx0 = Math.min(Math.min(this.x1, this.x2), this.ctrlx);
            final double ry0 = Math.min(Math.min(this.y1, this.y2), this.ctrly);
            final double rx2 = Math.max(Math.max(this.x1, this.x2), this.ctrlx);
            final double ry2 = Math.max(Math.max(this.y1, this.y2), this.ctrly);
            return new Rectangle2D.Double(rx0, ry0, rx2 - rx0, ry2 - ry0);
        }
    }
    
    class Iterator implements PathIterator
    {
        QuadCurve2D c;
        AffineTransform t;
        int index;
        
        Iterator(final QuadCurve2D q, final AffineTransform t) {
            this.c = q;
            this.t = t;
        }
        
        @Override
        public int getWindingRule() {
            return 1;
        }
        
        @Override
        public boolean isDone() {
            return this.index > 1;
        }
        
        @Override
        public void next() {
            ++this.index;
        }
        
        @Override
        public int currentSegment(final double[] coords) {
            if (this.isDone()) {
                throw new NoSuchElementException(Messages.getString("awt.4B"));
            }
            int type;
            int count;
            if (this.index == 0) {
                type = 0;
                coords[0] = this.c.getX1();
                coords[1] = this.c.getY1();
                count = 1;
            }
            else {
                type = 2;
                coords[0] = this.c.getCtrlX();
                coords[1] = this.c.getCtrlY();
                coords[2] = this.c.getX2();
                coords[3] = this.c.getY2();
                count = 2;
            }
            if (this.t != null) {
                this.t.transform(coords, 0, coords, 0, count);
            }
            return type;
        }
        
        @Override
        public int currentSegment(final float[] coords) {
            if (this.isDone()) {
                throw new NoSuchElementException(Messages.getString("awt.4B"));
            }
            int type;
            int count;
            if (this.index == 0) {
                type = 0;
                coords[0] = (float)this.c.getX1();
                coords[1] = (float)this.c.getY1();
                count = 1;
            }
            else {
                type = 2;
                coords[0] = (float)this.c.getCtrlX();
                coords[1] = (float)this.c.getCtrlY();
                coords[2] = (float)this.c.getX2();
                coords[3] = (float)this.c.getY2();
                count = 2;
            }
            if (this.t != null) {
                this.t.transform(coords, 0, coords, 0, count);
            }
            return type;
        }
    }
}
