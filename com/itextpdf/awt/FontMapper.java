// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt;

import com.itextpdf.text.pdf.BaseFont;
import java.awt.Font;

public interface FontMapper
{
    BaseFont awtToPdf(final Font p0);
    
    Font pdfToAwt(final BaseFont p0, final int p1);
}
