// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.awt;

import com.itextpdf.text.pdf.PdfContentByte;
import java.awt.print.PrinterJob;
import java.awt.print.PrinterGraphics;

public class PdfPrinterGraphics2D extends PdfGraphics2D implements PrinterGraphics
{
    private PrinterJob printerJob;
    
    public PdfPrinterGraphics2D(final PdfContentByte cb, final float width, final float height, final PrinterJob printerJob) {
        super(cb, width, height);
        this.printerJob = printerJob;
    }
    
    public PdfPrinterGraphics2D(final PdfContentByte cb, final float width, final float height, final boolean onlyShapes, final PrinterJob printerJob) {
        super(cb, width, height, onlyShapes);
        this.printerJob = printerJob;
    }
    
    public PdfPrinterGraphics2D(final PdfContentByte cb, final float width, final float height, final FontMapper fontMapper, final PrinterJob printerJob) {
        super(cb, width, height, fontMapper, false, false, 0.0f);
        this.printerJob = printerJob;
    }
    
    public PdfPrinterGraphics2D(final PdfContentByte cb, final float width, final float height, final FontMapper fontMapper, final boolean onlyShapes, final boolean convertImagesToJPEG, final float quality, final PrinterJob printerJob) {
        super(cb, width, height, fontMapper, onlyShapes, convertImagesToJPEG, quality);
        this.printerJob = printerJob;
    }
    
    @Override
    public PrinterJob getPrinterJob() {
        return this.printerJob;
    }
}
