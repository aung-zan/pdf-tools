// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.xmp;

import java.util.Iterator;

public interface XMPIterator extends Iterator
{
    void skipSubtree();
    
    void skipSiblings();
}
