// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.xmp.properties;

import com.itextpdf.xmp.options.PropertyOptions;

public interface XMPProperty
{
    String getValue();
    
    PropertyOptions getOptions();
    
    String getLanguage();
}
