// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.xmp.properties;

import com.itextpdf.xmp.options.AliasOptions;

public interface XMPAliasInfo
{
    String getNamespace();
    
    String getPrefix();
    
    String getPropName();
    
    AliasOptions getAliasForm();
}
