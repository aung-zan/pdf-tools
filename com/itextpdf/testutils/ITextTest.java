// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.testutils;

import com.itextpdf.text.log.LoggerFactory;
import java.io.File;
import java.io.IOException;
import com.itextpdf.text.log.Logger;

public abstract class ITextTest
{
    private static final Logger LOGGER;
    
    public void runTest() throws Exception {
        ITextTest.LOGGER.info("Starting test.");
        final String outPdf = this.getOutPdf();
        if (outPdf == null || outPdf.length() == 0) {
            throw new IOException("outPdf cannot be empty!");
        }
        this.makePdf(outPdf);
        this.assertPdf(outPdf);
        this.comparePdf(outPdf, this.getCmpPdf());
        ITextTest.LOGGER.info("Test complete.");
    }
    
    protected abstract void makePdf(final String p0) throws Exception;
    
    protected abstract String getOutPdf();
    
    protected void assertPdf(final String outPdf) throws Exception {
    }
    
    protected void comparePdf(final String outPdf, final String cmpPdf) throws Exception {
    }
    
    protected String getCmpPdf() {
        return "";
    }
    
    protected void deleteDirectory(final File path) {
        if (path == null) {
            return;
        }
        if (path.exists()) {
            for (final File f : path.listFiles()) {
                if (f.isDirectory()) {
                    this.deleteDirectory(f);
                    f.delete();
                }
                else {
                    f.delete();
                }
            }
            path.delete();
        }
    }
    
    protected void deleteFiles(final File path) {
        if (path != null && path.exists()) {
            for (final File f : path.listFiles()) {
                f.delete();
            }
        }
    }
    
    static {
        LOGGER = LoggerFactory.getLogger(ITextTest.class.getName());
    }
}
