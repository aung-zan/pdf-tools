// 
// Decompiled by Procyon v0.5.36
// 

package com.itextpdf.testutils;

import java.io.StringReader;
import org.xml.sax.InputSource;
import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextRenderInfo;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import com.itextpdf.text.xml.XMLUtil;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.itextpdf.text.pdf.parser.PdfContentStreamProcessor;
import com.itextpdf.text.pdf.parser.TaggedPdfReaderTool;
import javax.xml.transform.TransformerException;
import javax.xml.transform.Transformer;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.TransformerFactory;
import java.util.LinkedHashMap;
import org.w3c.dom.Element;
import java.util.Stack;
import java.util.HashMap;
import com.itextpdf.text.pdf.PdfAnnotation;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Node;
import java.io.ByteArrayInputStream;
import org.xml.sax.EntityResolver;
import javax.xml.parsers.DocumentBuilderFactory;
import com.itextpdf.xmp.XMPMeta;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import com.itextpdf.xmp.XMPException;
import com.itextpdf.xmp.options.SerializeOptions;
import com.itextpdf.xmp.XMPUtils;
import com.itextpdf.xmp.XMPMetaFactory;
import com.itextpdf.text.pdf.parser.InlineImageInfo;
import com.itextpdf.text.pdf.parser.InlineImageUtils;
import com.itextpdf.text.pdf.PdfContentParser;
import com.itextpdf.text.pdf.PRTokeniser;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.pdf.parser.ContentByteUtils;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import java.util.Set;
import java.util.Collection;
import java.util.TreeSet;
import com.itextpdf.text.pdf.PdfLiteral;
import com.itextpdf.text.pdf.PdfBoolean;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PRStream;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PRIndirectReference;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfIndirectReference;
import com.itextpdf.text.pdf.PdfName;
import java.util.ArrayList;
import java.util.StringTokenizer;
import com.itextpdf.text.pdf.PdfContentByte;
import java.util.Iterator;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Comparator;
import java.util.Arrays;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import com.itextpdf.text.BaseColor;
import java.io.OutputStream;
import com.itextpdf.text.pdf.PdfStamper;
import java.io.FileOutputStream;
import com.itextpdf.text.pdf.PdfReader;
import java.io.FileFilter;
import java.io.File;
import com.itextpdf.text.DocumentException;
import java.io.IOException;
import com.itextpdf.text.Rectangle;
import java.util.Map;
import com.itextpdf.text.pdf.RefKey;
import com.itextpdf.text.pdf.PdfDictionary;
import java.util.List;

public class CompareTool
{
    private String gsExec;
    private String compareExec;
    private final String gsParams = " -dNOPAUSE -dBATCH -sDEVICE=png16m -r150 -sOutputFile=<outputfile> <inputfile>";
    private final String compareParams = " \"<image1>\" \"<image2>\" \"<difference>\"";
    private static final String cannotOpenTargetDirectory = "Cannot open target directory for <filename>.";
    private static final String gsFailed = "GhostScript failed for <filename>.";
    private static final String unexpectedNumberOfPages = "Unexpected number of pages for <filename>.";
    private static final String differentPages = "File <filename> differs on page <pagenumber>.";
    private static final String undefinedGsPath = "Path to GhostScript is not specified. Please use -DgsExec=<path_to_ghostscript> (e.g. -DgsExec=\"C:/Program Files/gs/gs9.14/bin/gswin32c.exe\")";
    private static final String ignoredAreasPrefix = "ignored_areas_";
    private String cmpPdf;
    private String cmpPdfName;
    private String cmpImage;
    private String outPdf;
    private String outPdfName;
    private String outImage;
    List<PdfDictionary> outPages;
    List<RefKey> outPagesRef;
    List<PdfDictionary> cmpPages;
    List<RefKey> cmpPagesRef;
    private int compareByContentErrorsLimit;
    private boolean generateCompareByContentXmlReport;
    private String xmlReportName;
    private double floatComparisonError;
    private boolean absoluteError;
    
    public CompareTool() {
        this.compareByContentErrorsLimit = 1;
        this.generateCompareByContentXmlReport = false;
        this.xmlReportName = "report";
        this.floatComparisonError = 0.0;
        this.absoluteError = true;
        this.gsExec = System.getProperty("gsExec");
        if (this.gsExec == null) {
            this.gsExec = System.getenv("gsExec");
        }
        this.compareExec = System.getProperty("compareExec");
        if (this.compareExec == null) {
            this.compareExec = System.getenv("compareExec");
        }
    }
    
    private String compare(final String outPath, final String differenceImagePrefix, final Map<Integer, List<Rectangle>> ignoredAreas) throws IOException, InterruptedException, DocumentException {
        return this.compare(outPath, differenceImagePrefix, ignoredAreas, null);
    }
    
    private String compare(String outPath, final String differenceImagePrefix, final Map<Integer, List<Rectangle>> ignoredAreas, final List<Integer> equalPages) throws IOException, InterruptedException, DocumentException {
        if (this.gsExec == null) {
            return "Path to GhostScript is not specified. Please use -DgsExec=<path_to_ghostscript> (e.g. -DgsExec=\"C:/Program Files/gs/gs9.14/bin/gswin32c.exe\")";
        }
        if (!new File(this.gsExec).exists()) {
            return new File(this.gsExec).getAbsolutePath() + " does not exist";
        }
        if (!outPath.endsWith("/")) {
            outPath += "/";
        }
        final File targetDir = new File(outPath);
        if (!targetDir.exists()) {
            targetDir.mkdirs();
        }
        else {
            File[] arr$;
            final File[] imageFiles = arr$ = targetDir.listFiles(new PngFileFilter());
            for (final File file : arr$) {
                file.delete();
            }
            final File[] cmpImageFiles = arr$ = targetDir.listFiles(new CmpPngFileFilter());
            for (final File file : arr$) {
                file.delete();
            }
        }
        final File diffFile = new File(outPath + differenceImagePrefix);
        if (diffFile.exists()) {
            diffFile.delete();
        }
        if (ignoredAreas != null && !ignoredAreas.isEmpty()) {
            final PdfReader cmpReader = new PdfReader(this.cmpPdf);
            final PdfReader outReader = new PdfReader(this.outPdf);
            final PdfStamper outStamper = new PdfStamper(outReader, new FileOutputStream(outPath + "ignored_areas_" + this.outPdfName));
            final PdfStamper cmpStamper = new PdfStamper(cmpReader, new FileOutputStream(outPath + "ignored_areas_" + this.cmpPdfName));
            for (final Map.Entry<Integer, List<Rectangle>> entry : ignoredAreas.entrySet()) {
                final int pageNumber = entry.getKey();
                final List<Rectangle> rectangles = entry.getValue();
                if (rectangles != null && !rectangles.isEmpty()) {
                    final PdfContentByte outCB = outStamper.getOverContent(pageNumber);
                    final PdfContentByte cmpCB = cmpStamper.getOverContent(pageNumber);
                    for (final Rectangle rect : rectangles) {
                        rect.setBackgroundColor(BaseColor.BLACK);
                        outCB.rectangle(rect);
                        cmpCB.rectangle(rect);
                    }
                }
            }
            outStamper.close();
            cmpStamper.close();
            outReader.close();
            cmpReader.close();
            this.init(outPath + "ignored_areas_" + this.outPdfName, outPath + "ignored_areas_" + this.cmpPdfName);
        }
        if (!targetDir.exists()) {
            return "Cannot open target directory for <filename>.".replace("<filename>", this.outPdf);
        }
        this.getClass();
        String gsParams = " -dNOPAUSE -dBATCH -sDEVICE=png16m -r150 -sOutputFile=<outputfile> <inputfile>".replace("<outputfile>", outPath + this.cmpImage).replace("<inputfile>", this.cmpPdf);
        Process p = this.runProcess(this.gsExec, gsParams);
        BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        String line;
        while ((line = bri.readLine()) != null) {
            System.out.println(line);
        }
        bri.close();
        while ((line = bre.readLine()) != null) {
            System.out.println(line);
        }
        bre.close();
        if (p.waitFor() != 0) {
            return "GhostScript failed for <filename>.".replace("<filename>", this.cmpPdf);
        }
        this.getClass();
        gsParams = " -dNOPAUSE -dBATCH -sDEVICE=png16m -r150 -sOutputFile=<outputfile> <inputfile>".replace("<outputfile>", outPath + this.outImage).replace("<inputfile>", this.outPdf);
        p = this.runProcess(this.gsExec, gsParams);
        bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
        bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        while ((line = bri.readLine()) != null) {
            System.out.println(line);
        }
        bri.close();
        while ((line = bre.readLine()) != null) {
            System.out.println(line);
        }
        bre.close();
        final int exitValue = p.waitFor();
        if (exitValue != 0) {
            return "GhostScript failed for <filename>.".replace("<filename>", this.outPdf);
        }
        final File[] imageFiles = targetDir.listFiles(new PngFileFilter());
        final File[] cmpImageFiles = targetDir.listFiles(new CmpPngFileFilter());
        boolean bUnexpectedNumberOfPages = false;
        if (imageFiles.length != cmpImageFiles.length) {
            bUnexpectedNumberOfPages = true;
        }
        final int cnt = Math.min(imageFiles.length, cmpImageFiles.length);
        if (cnt < 1) {
            return "No files for comparing!!!\nThe result or sample pdf file is not processed by GhostScript.";
        }
        Arrays.sort(imageFiles, new ImageNameComparator());
        Arrays.sort(cmpImageFiles, new ImageNameComparator());
        String differentPagesFail = null;
        for (int i = 0; i < cnt; ++i) {
            if (equalPages == null || !equalPages.contains(i)) {
                System.out.print("Comparing page " + Integer.toString(i + 1) + " (" + imageFiles[i].getAbsolutePath() + ")...");
                final FileInputStream is1 = new FileInputStream(imageFiles[i]);
                final FileInputStream is2 = new FileInputStream(cmpImageFiles[i]);
                final boolean cmpResult = this.compareStreams(is1, is2);
                is1.close();
                is2.close();
                if (!cmpResult) {
                    if (this.compareExec == null || !new File(this.compareExec).exists()) {
                        differentPagesFail = "File <filename> differs on page <pagenumber>.".replace("<filename>", this.outPdf).replace("<pagenumber>", Integer.toString(i + 1));
                        differentPagesFail += "\nYou can optionally specify path to ImageMagick compare tool (e.g. -DcompareExec=\"C:/Program Files/ImageMagick-6.5.4-2/compare.exe\") to visualize differences.";
                        break;
                    }
                    this.getClass();
                    final String compareParams = " \"<image1>\" \"<image2>\" \"<difference>\"".replace("<image1>", imageFiles[i].getAbsolutePath()).replace("<image2>", cmpImageFiles[i].getAbsolutePath()).replace("<difference>", outPath + differenceImagePrefix + Integer.toString(i + 1) + ".png");
                    p = this.runProcess(this.compareExec, compareParams);
                    bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                    while ((line = bre.readLine()) != null) {
                        System.out.println(line);
                    }
                    bre.close();
                    final int cmpExitValue = p.waitFor();
                    if (cmpExitValue == 0) {
                        if (differentPagesFail == null) {
                            differentPagesFail = "File <filename> differs on page <pagenumber>.".replace("<filename>", this.outPdf).replace("<pagenumber>", Integer.toString(i + 1));
                            differentPagesFail = differentPagesFail + "\nPlease, examine " + outPath + differenceImagePrefix + Integer.toString(i + 1) + ".png for more details.";
                        }
                        else {
                            differentPagesFail = "File " + this.outPdf + " differs.\nPlease, examine difference images for more details.";
                        }
                    }
                    else {
                        differentPagesFail = "File <filename> differs on page <pagenumber>.".replace("<filename>", this.outPdf).replace("<pagenumber>", Integer.toString(i + 1));
                    }
                    System.out.println(differentPagesFail);
                }
                else {
                    System.out.println("done.");
                }
            }
        }
        if (differentPagesFail != null) {
            return differentPagesFail;
        }
        if (bUnexpectedNumberOfPages) {
            return "Unexpected number of pages for <filename>.".replace("<filename>", this.outPdf);
        }
        return null;
    }
    
    private Process runProcess(final String execPath, final String params) throws IOException, InterruptedException {
        final StringTokenizer st = new StringTokenizer(params);
        final String[] cmdArray = new String[st.countTokens() + 1];
        cmdArray[0] = execPath;
        int i = 1;
        while (st.hasMoreTokens()) {
            cmdArray[i] = st.nextToken();
            ++i;
        }
        final Process p = Runtime.getRuntime().exec(cmdArray);
        return p;
    }
    
    public String compare(final String outPdf, final String cmpPdf, final String outPath, final String differenceImagePrefix, final Map<Integer, List<Rectangle>> ignoredAreas) throws IOException, InterruptedException, DocumentException {
        this.init(outPdf, cmpPdf);
        return this.compare(outPath, differenceImagePrefix, ignoredAreas);
    }
    
    public String compare(final String outPdf, final String cmpPdf, final String outPath, final String differenceImagePrefix) throws IOException, InterruptedException, DocumentException {
        return this.compare(outPdf, cmpPdf, outPath, differenceImagePrefix, null);
    }
    
    public CompareTool setCompareByContentErrorsLimit(final int compareByContentMaxErrorCount) {
        this.compareByContentErrorsLimit = compareByContentMaxErrorCount;
        return this;
    }
    
    public void setGenerateCompareByContentXmlReport(final boolean generateCompareByContentXmlReport) {
        this.generateCompareByContentXmlReport = generateCompareByContentXmlReport;
    }
    
    public CompareTool setFloatAbsoluteError(final float error) {
        this.floatComparisonError = error;
        this.absoluteError = true;
        return this;
    }
    
    public CompareTool setFloatRelativeError(final float error) {
        this.floatComparisonError = error;
        this.absoluteError = false;
        return this;
    }
    
    public String getXmlReportName() {
        return this.xmlReportName;
    }
    
    public void setXmlReportName(final String xmlReportName) {
        this.xmlReportName = xmlReportName;
    }
    
    protected String compareByContent(final String outPath, final String differenceImagePrefix, final Map<Integer, List<Rectangle>> ignoredAreas) throws DocumentException, InterruptedException, IOException {
        System.out.print("[itext] INFO  Comparing by content..........");
        final PdfReader outReader = new PdfReader(this.outPdf);
        this.outPages = new ArrayList<PdfDictionary>();
        this.outPagesRef = new ArrayList<RefKey>();
        this.loadPagesFromReader(outReader, this.outPages, this.outPagesRef);
        final PdfReader cmpReader = new PdfReader(this.cmpPdf);
        this.cmpPages = new ArrayList<PdfDictionary>();
        this.cmpPagesRef = new ArrayList<RefKey>();
        this.loadPagesFromReader(cmpReader, this.cmpPages, this.cmpPagesRef);
        if (this.outPages.size() != this.cmpPages.size()) {
            return this.compare(outPath, differenceImagePrefix, ignoredAreas);
        }
        final CompareResult compareResult = new CompareResult(this.compareByContentErrorsLimit);
        final List<Integer> equalPages = new ArrayList<Integer>(this.cmpPages.size());
        for (int i = 0; i < this.cmpPages.size(); ++i) {
            final ObjectPath currentPath = new ObjectPath(this.cmpPagesRef.get(i), this.outPagesRef.get(i));
            if (this.compareDictionariesExtended(this.outPages.get(i), this.cmpPages.get(i), currentPath, compareResult)) {
                equalPages.add(i);
            }
        }
        final PdfObject outStructTree = outReader.getCatalog().get(PdfName.STRUCTTREEROOT);
        final PdfObject cmpStructTree = cmpReader.getCatalog().get(PdfName.STRUCTTREEROOT);
        final RefKey outStructTreeRef = (outStructTree == null) ? null : new RefKey((PdfIndirectReference)outStructTree);
        final RefKey cmpStructTreeRef = (cmpStructTree == null) ? null : new RefKey((PdfIndirectReference)cmpStructTree);
        this.compareObjects(outStructTree, cmpStructTree, new ObjectPath(outStructTreeRef, cmpStructTreeRef), compareResult);
        final PdfObject outOcProperties = outReader.getCatalog().get(PdfName.OCPROPERTIES);
        final PdfObject cmpOcProperties = cmpReader.getCatalog().get(PdfName.OCPROPERTIES);
        final RefKey outOcPropertiesRef = (outOcProperties instanceof PdfIndirectReference) ? new RefKey((PdfIndirectReference)outOcProperties) : null;
        final RefKey cmpOcPropertiesRef = (cmpOcProperties instanceof PdfIndirectReference) ? new RefKey((PdfIndirectReference)cmpOcProperties) : null;
        this.compareObjects(outOcProperties, cmpOcProperties, new ObjectPath(outOcPropertiesRef, cmpOcPropertiesRef), compareResult);
        outReader.close();
        cmpReader.close();
        if (this.generateCompareByContentXmlReport) {
            try {
                compareResult.writeReportToXml(new FileOutputStream(outPath + "/" + this.xmlReportName + ".xml"));
            }
            catch (Exception ex) {}
        }
        if (equalPages.size() == this.cmpPages.size() && compareResult.isOk()) {
            System.out.println("OK");
            System.out.flush();
            return null;
        }
        System.out.println("Fail");
        System.out.flush();
        final String compareByContentReport = "Compare by content report:\n" + compareResult.getReport();
        System.out.println(compareByContentReport);
        System.out.flush();
        final String message = this.compare(outPath, differenceImagePrefix, ignoredAreas, equalPages);
        if (message == null || message.length() == 0) {
            return "Compare by content fails. No visual differences";
        }
        return message;
    }
    
    public String compareByContent(final String outPdf, final String cmpPdf, final String outPath, final String differenceImagePrefix, final Map<Integer, List<Rectangle>> ignoredAreas) throws DocumentException, InterruptedException, IOException {
        this.init(outPdf, cmpPdf);
        return this.compareByContent(outPath, differenceImagePrefix, ignoredAreas);
    }
    
    public String compareByContent(final String outPdf, final String cmpPdf, final String outPath, final String differenceImagePrefix) throws DocumentException, InterruptedException, IOException {
        return this.compareByContent(outPdf, cmpPdf, outPath, differenceImagePrefix, null);
    }
    
    private void loadPagesFromReader(final PdfReader reader, final List<PdfDictionary> pages, final List<RefKey> pagesRef) {
        final PdfObject pagesDict = reader.getCatalog().get(PdfName.PAGES);
        this.addPagesFromDict(pagesDict, pages, pagesRef);
    }
    
    private void addPagesFromDict(final PdfObject dictRef, final List<PdfDictionary> pages, final List<RefKey> pagesRef) {
        final PdfDictionary dict = (PdfDictionary)PdfReader.getPdfObject(dictRef);
        if (dict.isPages()) {
            final PdfArray kids = dict.getAsArray(PdfName.KIDS);
            if (kids == null) {
                return;
            }
            for (final PdfObject kid : kids) {
                this.addPagesFromDict(kid, pages, pagesRef);
            }
        }
        else if (dict.isPage()) {
            pages.add(dict);
            pagesRef.add(new RefKey((PdfIndirectReference)dictRef));
        }
    }
    
    private boolean compareObjects(final PdfObject outObj, final PdfObject cmpObj, ObjectPath currentPath, final CompareResult compareResult) throws IOException {
        final PdfObject outDirectObj = PdfReader.getPdfObject(outObj);
        final PdfObject cmpDirectObj = PdfReader.getPdfObject(cmpObj);
        if (cmpDirectObj == null && outDirectObj == null) {
            return true;
        }
        if (outDirectObj == null) {
            compareResult.addError(currentPath, "Expected object was not found.");
            return false;
        }
        if (cmpDirectObj == null) {
            compareResult.addError(currentPath, "Found object which was not expected to be found.");
            return false;
        }
        if (cmpDirectObj.type() != outDirectObj.type()) {
            compareResult.addError(currentPath, String.format("Types do not match. Expected: %s. Found: %s.", cmpDirectObj.getClass().getSimpleName(), outDirectObj.getClass().getSimpleName()));
            return false;
        }
        if (cmpObj.isIndirect() && outObj.isIndirect()) {
            if (currentPath.isComparing(new RefKey((PdfIndirectReference)cmpObj), new RefKey((PdfIndirectReference)outObj))) {
                return true;
            }
            currentPath = currentPath.resetDirectPath(new RefKey((PdfIndirectReference)cmpObj), new RefKey((PdfIndirectReference)outObj));
        }
        if (!cmpDirectObj.isDictionary() || !((PdfDictionary)cmpDirectObj).isPage()) {
            if (cmpDirectObj.isDictionary()) {
                if (!this.compareDictionariesExtended((PdfDictionary)outDirectObj, (PdfDictionary)cmpDirectObj, currentPath, compareResult)) {
                    return false;
                }
            }
            else if (cmpDirectObj.isStream()) {
                if (!this.compareStreamsExtended((PRStream)outDirectObj, (PRStream)cmpDirectObj, currentPath, compareResult)) {
                    return false;
                }
            }
            else if (cmpDirectObj.isArray()) {
                if (!this.compareArraysExtended((PdfArray)outDirectObj, (PdfArray)cmpDirectObj, currentPath, compareResult)) {
                    return false;
                }
            }
            else if (cmpDirectObj.isName()) {
                if (!this.compareNamesExtended((PdfName)outDirectObj, (PdfName)cmpDirectObj, currentPath, compareResult)) {
                    return false;
                }
            }
            else if (cmpDirectObj.isNumber()) {
                if (!this.compareNumbersExtended((PdfNumber)outDirectObj, (PdfNumber)cmpDirectObj, currentPath, compareResult)) {
                    return false;
                }
            }
            else if (cmpDirectObj.isString()) {
                if (!this.compareStringsExtended((PdfString)outDirectObj, (PdfString)cmpDirectObj, currentPath, compareResult)) {
                    return false;
                }
            }
            else if (cmpDirectObj.isBoolean()) {
                if (!this.compareBooleansExtended((PdfBoolean)outDirectObj, (PdfBoolean)cmpDirectObj, currentPath, compareResult)) {
                    return false;
                }
            }
            else if (cmpDirectObj instanceof PdfLiteral) {
                if (!this.compareLiteralsExtended((PdfLiteral)outDirectObj, (PdfLiteral)cmpDirectObj, currentPath, compareResult)) {
                    return false;
                }
            }
            else if (!outDirectObj.isNull() || !cmpDirectObj.isNull()) {
                throw new UnsupportedOperationException();
            }
            return true;
        }
        if (!outDirectObj.isDictionary() || !((PdfDictionary)outDirectObj).isPage()) {
            if (compareResult != null && currentPath != null) {
                compareResult.addError(currentPath, "Expected a page. Found not a page.");
            }
            return false;
        }
        final RefKey cmpRefKey = new RefKey((PdfIndirectReference)cmpObj);
        final RefKey outRefKey = new RefKey((PdfIndirectReference)outObj);
        if (this.cmpPagesRef.contains(cmpRefKey) && this.cmpPagesRef.indexOf(cmpRefKey) == this.outPagesRef.indexOf(outRefKey)) {
            return true;
        }
        if (compareResult != null && currentPath != null) {
            compareResult.addError(currentPath, String.format("The dictionaries refer to different pages. Expected page number: %s. Found: %s", this.cmpPagesRef.indexOf(cmpRefKey), this.outPagesRef.indexOf(outRefKey)));
        }
        return false;
    }
    
    public boolean compareDictionaries(final PdfDictionary outDict, final PdfDictionary cmpDict) throws IOException {
        return this.compareDictionariesExtended(outDict, cmpDict, null, null);
    }
    
    private boolean compareDictionariesExtended(final PdfDictionary outDict, final PdfDictionary cmpDict, final ObjectPath currentPath, final CompareResult compareResult) throws IOException {
        if ((cmpDict != null && outDict == null) || (outDict != null && cmpDict == null)) {
            compareResult.addError(currentPath, "One of the dictionaries is null, the other is not.");
            return false;
        }
        boolean dictsAreSame = true;
        final Set<PdfName> mergedKeys = new TreeSet<PdfName>(cmpDict.getKeys());
        mergedKeys.addAll(outDict.getKeys());
        for (final PdfName key : mergedKeys) {
            if (key.compareTo(PdfName.PARENT) != 0) {
                if (key.compareTo(PdfName.P) == 0) {
                    continue;
                }
                if (outDict.isStream() && cmpDict.isStream()) {
                    if (key.equals(PdfName.FILTER)) {
                        continue;
                    }
                    if (key.equals(PdfName.LENGTH)) {
                        continue;
                    }
                }
                if (key.compareTo(PdfName.BASEFONT) == 0 || key.compareTo(PdfName.FONTNAME) == 0) {
                    final PdfObject cmpObj = cmpDict.getDirectObject(key);
                    if (cmpObj.isName() && cmpObj.toString().indexOf(43) > 0) {
                        final PdfObject outObj = outDict.getDirectObject(key);
                        if (!outObj.isName() || outObj.toString().indexOf(43) == -1) {
                            if (compareResult != null && currentPath != null) {
                                compareResult.addError(currentPath, String.format("PdfDictionary %s entry: Expected: %s. Found: %s", key.toString(), cmpObj.toString(), outObj.toString()));
                            }
                            dictsAreSame = false;
                        }
                        final String cmpName = cmpObj.toString().substring(cmpObj.toString().indexOf(43));
                        final String outName = outObj.toString().substring(outObj.toString().indexOf(43));
                        if (!cmpName.equals(outName)) {
                            if (compareResult != null && currentPath != null) {
                                compareResult.addError(currentPath, String.format("PdfDictionary %s entry: Expected: %s. Found: %s", key.toString(), cmpObj.toString(), outObj.toString()));
                            }
                            dictsAreSame = false;
                            continue;
                        }
                        continue;
                    }
                }
                if (this.floatComparisonError != 0.0 && cmpDict.isPage() && outDict.isPage() && key.equals(PdfName.CONTENTS)) {
                    if (this.compareContentStreamsByParsingExtended(outDict.getDirectObject(key), cmpDict.getDirectObject(key), (PdfDictionary)outDict.getDirectObject(PdfName.RESOURCES), (PdfDictionary)cmpDict.getDirectObject(PdfName.RESOURCES), currentPath, compareResult)) {
                        continue;
                    }
                    dictsAreSame = false;
                }
                else {
                    if (currentPath != null) {
                        currentPath.pushDictItemToPath(key.toString());
                    }
                    dictsAreSame = (this.compareObjects(outDict.get(key), cmpDict.get(key), currentPath, compareResult) && dictsAreSame);
                    if (currentPath != null) {
                        currentPath.pop();
                    }
                    if (!dictsAreSame && (currentPath == null || compareResult == null || compareResult.isMessageLimitReached())) {
                        return false;
                    }
                    continue;
                }
            }
        }
        return dictsAreSame;
    }
    
    public boolean compareContentStreamsByParsing(final PdfObject outObj, final PdfObject cmpObj) throws IOException {
        return this.compareContentStreamsByParsingExtended(outObj, cmpObj, null, null, null, null);
    }
    
    public boolean compareContentStreamsByParsing(final PdfObject outObj, final PdfObject cmpObj, final PdfDictionary outResources, final PdfDictionary cmpResources) throws IOException {
        return this.compareContentStreamsByParsingExtended(outObj, cmpObj, outResources, cmpResources, null, null);
    }
    
    private boolean compareContentStreamsByParsingExtended(final PdfObject outObj, final PdfObject cmpObj, PdfDictionary outResources, PdfDictionary cmpResources, final ObjectPath currentPath, final CompareResult compareResult) throws IOException {
        if (outObj.type() != outObj.type()) {
            compareResult.addError(currentPath, String.format("PdfObject. Types are different. Expected: %s. Found: %s", cmpObj.type(), outObj.type()));
            return false;
        }
        if (outObj.isArray()) {
            final PdfArray outArr = (PdfArray)outObj;
            final PdfArray cmpArr = (PdfArray)cmpObj;
            if (cmpArr.size() != outArr.size()) {
                compareResult.addError(currentPath, String.format("PdfArray. Sizes are different. Expected: %s. Found: %s", cmpArr.size(), outArr.size()));
                return false;
            }
            for (int i = 0; i < cmpArr.size(); ++i) {
                if (!this.compareContentStreamsByParsingExtended(outArr.getPdfObject(i), cmpArr.getPdfObject(i), outResources, cmpResources, currentPath, compareResult)) {
                    return false;
                }
            }
        }
        final PRTokeniser cmpTokeniser = new PRTokeniser(new RandomAccessFileOrArray(new RandomAccessSourceFactory().createSource(ContentByteUtils.getContentBytesFromContentObject(cmpObj))));
        final PRTokeniser outTokeniser = new PRTokeniser(new RandomAccessFileOrArray(new RandomAccessSourceFactory().createSource(ContentByteUtils.getContentBytesFromContentObject(outObj))));
        final PdfContentParser cmpPs = new PdfContentParser(cmpTokeniser);
        final PdfContentParser outPs = new PdfContentParser(outTokeniser);
        final ArrayList<PdfObject> cmpOperands = new ArrayList<PdfObject>();
        final ArrayList<PdfObject> outOperands = new ArrayList<PdfObject>();
        while (cmpPs.parse(cmpOperands).size() > 0) {
            outPs.parse(outOperands);
            if (cmpOperands.size() != outOperands.size()) {
                compareResult.addError(currentPath, String.format("PdfObject. Different commands lengths. Expected: %s. Found: %s", cmpOperands.size(), outOperands.size()));
                return false;
            }
            if (cmpOperands.size() == 1 && this.compareLiterals(cmpOperands.get(0), new PdfLiteral("BI")) && this.compareLiterals(outOperands.get(0), new PdfLiteral("BI"))) {
                final PRStream cmpStr = (PRStream)cmpObj;
                final PRStream outStr = (PRStream)outObj;
                if (null != outStr.getDirectObject(PdfName.RESOURCES) && null != cmpStr.getDirectObject(PdfName.RESOURCES)) {
                    outResources = (PdfDictionary)outStr.getDirectObject(PdfName.RESOURCES);
                    cmpResources = (PdfDictionary)cmpStr.getDirectObject(PdfName.RESOURCES);
                }
                if (!this.compareInlineImagesExtended(outPs, cmpPs, outResources, cmpResources, currentPath, compareResult)) {
                    return false;
                }
                continue;
            }
            else {
                for (int j = 0; j < cmpOperands.size(); ++j) {
                    if (!this.compareObjects(outOperands.get(j), cmpOperands.get(j), currentPath, compareResult)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    private boolean compareInlineImagesExtended(final PdfContentParser outPs, final PdfContentParser cmpPs, final PdfDictionary outDict, final PdfDictionary cmpDict, final ObjectPath currentPath, final CompareResult compareResult) throws IOException {
        final InlineImageInfo cmpInfo = InlineImageUtils.parseInlineImage(cmpPs, cmpDict);
        final InlineImageInfo outInfo = InlineImageUtils.parseInlineImage(outPs, outDict);
        return this.compareObjects(outInfo.getImageDictionary(), cmpInfo.getImageDictionary(), currentPath, compareResult) && Arrays.equals(outInfo.getSamples(), cmpInfo.getSamples());
    }
    
    public boolean compareStreams(final PRStream outStream, final PRStream cmpStream) throws IOException {
        return this.compareStreamsExtended(outStream, cmpStream, null, null);
    }
    
    private boolean compareStreamsExtended(final PRStream outStream, final PRStream cmpStream, final ObjectPath currentPath, final CompareResult compareResult) throws IOException {
        final boolean decodeStreams = PdfName.FLATEDECODE.equals(outStream.get(PdfName.FILTER));
        byte[] outStreamBytes = PdfReader.getStreamBytesRaw(outStream);
        byte[] cmpStreamBytes = PdfReader.getStreamBytesRaw(cmpStream);
        if (decodeStreams) {
            outStreamBytes = PdfReader.decodeBytes(outStreamBytes, outStream);
            cmpStreamBytes = PdfReader.decodeBytes(cmpStreamBytes, cmpStream);
        }
        if (this.floatComparisonError != 0.0 && PdfName.XOBJECT.equals(cmpStream.getDirectObject(PdfName.TYPE)) && PdfName.XOBJECT.equals(outStream.getDirectObject(PdfName.TYPE)) && PdfName.FORM.equals(cmpStream.getDirectObject(PdfName.SUBTYPE)) && PdfName.FORM.equals(outStream.getDirectObject(PdfName.SUBTYPE))) {
            return this.compareContentStreamsByParsingExtended(outStream, cmpStream, outStream.getAsDict(PdfName.RESOURCES), cmpStream.getAsDict(PdfName.RESOURCES), currentPath, compareResult) && this.compareDictionariesExtended(outStream, cmpStream, currentPath, compareResult);
        }
        if (Arrays.equals(outStreamBytes, cmpStreamBytes)) {
            return this.compareDictionariesExtended(outStream, cmpStream, currentPath, compareResult);
        }
        if (cmpStreamBytes.length != outStreamBytes.length) {
            if (compareResult != null && currentPath != null) {
                compareResult.addError(currentPath, String.format("PRStream. Lengths are different. Expected: %s. Found: %s", cmpStreamBytes.length, outStreamBytes.length));
            }
        }
        else {
            for (int i = 0; i < cmpStreamBytes.length; ++i) {
                if (cmpStreamBytes[i] != outStreamBytes[i]) {
                    final int l = Math.max(0, i - 10);
                    final int r = Math.min(cmpStreamBytes.length, i + 10);
                    if (compareResult != null && currentPath != null) {
                        currentPath.pushOffsetToPath(i);
                        compareResult.addError(currentPath, String.format("PRStream. The bytes differ at index %s. Expected: %s (%s). Found: %s (%s)", i, new String(new byte[] { cmpStreamBytes[i] }), new String(cmpStreamBytes, l, r - l).replaceAll("\\n", "\\\\n"), new String(new byte[] { outStreamBytes[i] }), new String(outStreamBytes, l, r - l).replaceAll("\\n", "\\\\n")));
                        currentPath.pop();
                    }
                }
            }
        }
        return false;
    }
    
    public boolean compareArrays(final PdfArray outArray, final PdfArray cmpArray) throws IOException {
        return this.compareArraysExtended(outArray, cmpArray, null, null);
    }
    
    private boolean compareArraysExtended(final PdfArray outArray, final PdfArray cmpArray, final ObjectPath currentPath, final CompareResult compareResult) throws IOException {
        if (outArray == null) {
            if (compareResult != null && currentPath != null) {
                compareResult.addError(currentPath, "Found null. Expected PdfArray.");
            }
            return false;
        }
        if (outArray.size() != cmpArray.size()) {
            if (compareResult != null && currentPath != null) {
                compareResult.addError(currentPath, String.format("PdfArrays. Lengths are different. Expected: %s. Found: %s.", cmpArray.size(), outArray.size()));
            }
            return false;
        }
        boolean arraysAreEqual = true;
        for (int i = 0; i < cmpArray.size(); ++i) {
            if (currentPath != null) {
                currentPath.pushArrayItemToPath(i);
            }
            arraysAreEqual = (this.compareObjects(outArray.getPdfObject(i), cmpArray.getPdfObject(i), currentPath, compareResult) && arraysAreEqual);
            if (currentPath != null) {
                currentPath.pop();
            }
            if (!arraysAreEqual && (currentPath == null || compareResult == null || compareResult.isMessageLimitReached())) {
                return false;
            }
        }
        return arraysAreEqual;
    }
    
    public boolean compareNames(final PdfName outName, final PdfName cmpName) {
        return cmpName.compareTo(outName) == 0;
    }
    
    private boolean compareNamesExtended(final PdfName outName, final PdfName cmpName, final ObjectPath currentPath, final CompareResult compareResult) {
        if (cmpName.compareTo(outName) == 0) {
            return true;
        }
        if (compareResult != null && currentPath != null) {
            compareResult.addError(currentPath, String.format("PdfName. Expected: %s. Found: %s", cmpName.toString(), outName.toString()));
        }
        return false;
    }
    
    public boolean compareNumbers(final PdfNumber outNumber, final PdfNumber cmpNumber) {
        double difference = Math.abs(outNumber.doubleValue() - cmpNumber.doubleValue());
        if (!this.absoluteError && cmpNumber.doubleValue() != 0.0) {
            difference /= cmpNumber.doubleValue();
        }
        return difference <= this.floatComparisonError;
    }
    
    private boolean compareNumbersExtended(final PdfNumber outNumber, final PdfNumber cmpNumber, final ObjectPath currentPath, final CompareResult compareResult) {
        if (this.compareNumbers(outNumber, cmpNumber)) {
            return true;
        }
        if (compareResult != null && currentPath != null) {
            compareResult.addError(currentPath, String.format("PdfNumber. Expected: %s. Found: %s", cmpNumber, outNumber));
        }
        return false;
    }
    
    public boolean compareStrings(final PdfString outString, final PdfString cmpString) {
        return Arrays.equals(cmpString.getBytes(), outString.getBytes());
    }
    
    private boolean compareStringsExtended(final PdfString outString, final PdfString cmpString, final ObjectPath currentPath, final CompareResult compareResult) {
        if (Arrays.equals(cmpString.getBytes(), outString.getBytes())) {
            return true;
        }
        final String cmpStr = cmpString.toUnicodeString();
        final String outStr = outString.toUnicodeString();
        if (cmpStr.length() != outStr.length()) {
            if (compareResult != null && currentPath != null) {
                compareResult.addError(currentPath, String.format("PdfString. Lengths are different. Expected: %s. Found: %s", cmpStr.length(), outStr.length()));
            }
        }
        else {
            int i = 0;
            while (i < cmpStr.length()) {
                if (cmpStr.charAt(i) != outStr.charAt(i)) {
                    final int l = Math.max(0, i - 10);
                    final int r = Math.min(cmpStr.length(), i + 10);
                    if (compareResult != null && currentPath != null) {
                        currentPath.pushOffsetToPath(i);
                        compareResult.addError(currentPath, String.format("PdfString. Characters differ at position %s. Expected: %s (%s). Found: %s (%s).", i, Character.toString(cmpStr.charAt(i)), cmpStr.substring(l, r).replace("\n", "\\n"), Character.toString(outStr.charAt(i)), outStr.substring(l, r).replace("\n", "\\n")));
                        currentPath.pop();
                        break;
                    }
                    break;
                }
                else {
                    ++i;
                }
            }
        }
        return false;
    }
    
    public boolean compareLiterals(final PdfLiteral outLiteral, final PdfLiteral cmpLiteral) {
        return Arrays.equals(cmpLiteral.getBytes(), outLiteral.getBytes());
    }
    
    private boolean compareLiteralsExtended(final PdfLiteral outLiteral, final PdfLiteral cmpLiteral, final ObjectPath currentPath, final CompareResult compareResult) {
        if (this.compareLiterals(outLiteral, cmpLiteral)) {
            return true;
        }
        if (compareResult != null && currentPath != null) {
            compareResult.addError(currentPath, String.format("PdfLiteral. Expected: %s. Found: %s", cmpLiteral, outLiteral));
        }
        return false;
    }
    
    public boolean compareBooleans(final PdfBoolean outBoolean, final PdfBoolean cmpBoolean) {
        return Arrays.equals(cmpBoolean.getBytes(), outBoolean.getBytes());
    }
    
    private boolean compareBooleansExtended(final PdfBoolean outBoolean, final PdfBoolean cmpBoolean, final ObjectPath currentPath, final CompareResult compareResult) {
        if (cmpBoolean.booleanValue() == outBoolean.booleanValue()) {
            return true;
        }
        if (compareResult != null && currentPath != null) {
            compareResult.addError(currentPath, String.format("PdfBoolean. Expected: %s. Found: %s.", cmpBoolean.booleanValue(), outBoolean.booleanValue()));
        }
        return false;
    }
    
    public String compareXmp(final byte[] xmp1, final byte[] xmp2) {
        return this.compareXmp(xmp1, xmp2, false);
    }
    
    public String compareXmp(byte[] xmp1, byte[] xmp2, final boolean ignoreDateAndProducerProperties) {
        try {
            if (ignoreDateAndProducerProperties) {
                XMPMeta xmpMeta = XMPMetaFactory.parseFromBuffer(xmp1);
                XMPUtils.removeProperties(xmpMeta, "http://ns.adobe.com/xap/1.0/", "CreateDate", true, true);
                XMPUtils.removeProperties(xmpMeta, "http://ns.adobe.com/xap/1.0/", "ModifyDate", true, true);
                XMPUtils.removeProperties(xmpMeta, "http://ns.adobe.com/xap/1.0/", "MetadataDate", true, true);
                XMPUtils.removeProperties(xmpMeta, "http://ns.adobe.com/pdf/1.3/", "Producer", true, true);
                xmp1 = XMPMetaFactory.serializeToBuffer(xmpMeta, new SerializeOptions(8192));
                xmpMeta = XMPMetaFactory.parseFromBuffer(xmp2);
                XMPUtils.removeProperties(xmpMeta, "http://ns.adobe.com/xap/1.0/", "CreateDate", true, true);
                XMPUtils.removeProperties(xmpMeta, "http://ns.adobe.com/xap/1.0/", "ModifyDate", true, true);
                XMPUtils.removeProperties(xmpMeta, "http://ns.adobe.com/xap/1.0/", "MetadataDate", true, true);
                XMPUtils.removeProperties(xmpMeta, "http://ns.adobe.com/pdf/1.3/", "Producer", true, true);
                xmp2 = XMPMetaFactory.serializeToBuffer(xmpMeta, new SerializeOptions(8192));
            }
            if (!this.compareXmls(xmp1, xmp2)) {
                return "The XMP packages different!";
            }
        }
        catch (XMPException xmpExc) {
            return "XMP parsing failure!";
        }
        catch (IOException ioExc) {
            return "XMP parsing failure!";
        }
        catch (ParserConfigurationException parseExc) {
            return "XMP parsing failure!";
        }
        catch (SAXException parseExc2) {
            return "XMP parsing failure!";
        }
        return null;
    }
    
    public String compareXmp(final String outPdf, final String cmpPdf) {
        return this.compareXmp(outPdf, cmpPdf, false);
    }
    
    public String compareXmp(final String outPdf, final String cmpPdf, final boolean ignoreDateAndProducerProperties) {
        this.init(outPdf, cmpPdf);
        PdfReader cmpReader = null;
        PdfReader outReader = null;
        try {
            cmpReader = new PdfReader(this.cmpPdf);
            outReader = new PdfReader(this.outPdf);
            final byte[] cmpBytes = cmpReader.getMetadata();
            final byte[] outBytes = outReader.getMetadata();
            return this.compareXmp(cmpBytes, outBytes, ignoreDateAndProducerProperties);
        }
        catch (IOException e) {
            return "XMP parsing failure!";
        }
        finally {
            if (cmpReader != null) {
                cmpReader.close();
            }
            if (outReader != null) {
                outReader.close();
            }
        }
    }
    
    public boolean compareXmls(final byte[] xml1, final byte[] xml2) throws ParserConfigurationException, SAXException, IOException {
        final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        dbf.setCoalescing(true);
        dbf.setIgnoringElementContentWhitespace(true);
        dbf.setIgnoringComments(true);
        final DocumentBuilder db = dbf.newDocumentBuilder();
        db.setEntityResolver(new SafeEmptyEntityResolver());
        final Document doc1 = db.parse(new ByteArrayInputStream(xml1));
        doc1.normalizeDocument();
        final Document doc2 = db.parse(new ByteArrayInputStream(xml2));
        doc2.normalizeDocument();
        return doc2.isEqualNode(doc1);
    }
    
    public String compareDocumentInfo(final String outPdf, final String cmpPdf) throws IOException {
        System.out.print("[itext] INFO  Comparing document info.......");
        String message = null;
        final PdfReader outReader = new PdfReader(outPdf);
        final PdfReader cmpReader = new PdfReader(cmpPdf);
        final String[] cmpInfo = this.convertInfo(cmpReader.getInfo());
        final String[] outInfo = this.convertInfo(outReader.getInfo());
        for (int i = 0; i < cmpInfo.length; ++i) {
            if (!cmpInfo[i].equals(outInfo[i])) {
                message = "Document info fail";
                break;
            }
        }
        outReader.close();
        cmpReader.close();
        if (message == null) {
            System.out.println("OK");
        }
        else {
            System.out.println("Fail");
        }
        System.out.flush();
        return message;
    }
    
    private boolean linksAreSame(final PdfAnnotation.PdfImportedLink cmpLink, final PdfAnnotation.PdfImportedLink outLink) {
        if (cmpLink.getDestinationPage() != outLink.getDestinationPage()) {
            return false;
        }
        if (!cmpLink.getRect().toString().equals(outLink.getRect().toString())) {
            return false;
        }
        final Map<PdfName, PdfObject> cmpParams = cmpLink.getParameters();
        final Map<PdfName, PdfObject> outParams = outLink.getParameters();
        if (cmpParams.size() != outParams.size()) {
            return false;
        }
        for (final Map.Entry<PdfName, PdfObject> cmpEntry : cmpParams.entrySet()) {
            final PdfObject cmpObj = cmpEntry.getValue();
            if (!outParams.containsKey(cmpEntry.getKey())) {
                return false;
            }
            final PdfObject outObj = outParams.get(cmpEntry.getKey());
            if (cmpObj.type() != outObj.type()) {
                return false;
            }
            switch (cmpObj.type()) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 8: {
                    if (!cmpObj.toString().equals(outObj.toString())) {
                        return false;
                    }
                    continue;
                }
            }
        }
        return true;
    }
    
    public String compareLinks(final String outPdf, final String cmpPdf) throws IOException {
        System.out.print("[itext] INFO  Comparing link annotations....");
        String message = null;
        final PdfReader outReader = new PdfReader(outPdf);
        final PdfReader cmpReader = new PdfReader(cmpPdf);
        for (int i = 0; i < outReader.getNumberOfPages() && i < cmpReader.getNumberOfPages(); ++i) {
            final List<PdfAnnotation.PdfImportedLink> outLinks = outReader.getLinks(i + 1);
            final List<PdfAnnotation.PdfImportedLink> cmpLinks = cmpReader.getLinks(i + 1);
            if (cmpLinks.size() != outLinks.size()) {
                message = String.format("Different number of links on page %d.", i + 1);
                break;
            }
            for (int j = 0; j < cmpLinks.size(); ++j) {
                if (!this.linksAreSame(cmpLinks.get(j), outLinks.get(j))) {
                    message = String.format("Different links on page %d.\n%s\n%s", i + 1, cmpLinks.get(j).toString(), outLinks.get(j).toString());
                    break;
                }
            }
        }
        outReader.close();
        cmpReader.close();
        if (message == null) {
            System.out.println("OK");
        }
        else {
            System.out.println("Fail");
        }
        System.out.flush();
        return message;
    }
    
    public String compareTagStructures(final String outPdf, final String cmpPdf) throws IOException, ParserConfigurationException, SAXException {
        System.out.print("[itext] INFO  Comparing tag structures......");
        final String outXml = outPdf.replace(".pdf", ".xml");
        final String cmpXml = outPdf.replace(".pdf", ".cmp.xml");
        String message = null;
        PdfReader reader = new PdfReader(outPdf);
        final FileOutputStream xmlOut1 = new FileOutputStream(outXml);
        new CmpTaggedPdfReaderTool().convertToXml(reader, xmlOut1);
        reader.close();
        reader = new PdfReader(cmpPdf);
        final FileOutputStream xmlOut2 = new FileOutputStream(cmpXml);
        new CmpTaggedPdfReaderTool().convertToXml(reader, xmlOut2);
        reader.close();
        if (!this.compareXmls(outXml, cmpXml)) {
            message = "The tag structures are different.";
        }
        xmlOut1.close();
        xmlOut2.close();
        if (message == null) {
            System.out.println("OK");
        }
        else {
            System.out.println("Fail");
        }
        System.out.flush();
        return message;
    }
    
    private String[] convertInfo(final HashMap<String, String> info) {
        final String[] convertedInfo = { "", "", "", "" };
        for (final Map.Entry<String, String> entry : info.entrySet()) {
            if ("title".equalsIgnoreCase(entry.getKey())) {
                convertedInfo[0] = entry.getValue();
            }
            else if ("author".equalsIgnoreCase(entry.getKey())) {
                convertedInfo[1] = entry.getValue();
            }
            else if ("subject".equalsIgnoreCase(entry.getKey())) {
                convertedInfo[2] = entry.getValue();
            }
            else {
                if (!"keywords".equalsIgnoreCase(entry.getKey())) {
                    continue;
                }
                convertedInfo[3] = entry.getValue();
            }
        }
        return convertedInfo;
    }
    
    public boolean compareXmls(final String xml1, final String xml2) throws ParserConfigurationException, SAXException, IOException {
        final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        dbf.setCoalescing(true);
        dbf.setIgnoringElementContentWhitespace(true);
        dbf.setIgnoringComments(true);
        final DocumentBuilder db = dbf.newDocumentBuilder();
        db.setEntityResolver(new SafeEmptyEntityResolver());
        final Document doc1 = db.parse(new File(xml1));
        doc1.normalizeDocument();
        final Document doc2 = db.parse(new File(xml2));
        doc2.normalizeDocument();
        return doc2.isEqualNode(doc1);
    }
    
    private void init(final String outPdf, final String cmpPdf) {
        this.outPdf = outPdf;
        this.cmpPdf = cmpPdf;
        this.outPdfName = new File(outPdf).getName();
        this.cmpPdfName = new File(cmpPdf).getName();
        this.outImage = this.outPdfName + "-%03d.png";
        if (this.cmpPdfName.startsWith("cmp_")) {
            this.cmpImage = this.cmpPdfName + "-%03d.png";
        }
        else {
            this.cmpImage = "cmp_" + this.cmpPdfName + "-%03d.png";
        }
    }
    
    private boolean compareStreams(final InputStream is1, final InputStream is2) throws IOException {
        final byte[] buffer1 = new byte[65536];
        final byte[] buffer2 = new byte[65536];
        int len1;
        do {
            len1 = is1.read(buffer1);
            final int len2 = is2.read(buffer2);
            if (len1 != len2) {
                return false;
            }
            if (!Arrays.equals(buffer1, buffer2)) {
                return false;
            }
        } while (len1 != -1);
        return true;
    }
    
    private class ObjectPath
    {
        protected RefKey baseCmpObject;
        protected RefKey baseOutObject;
        protected Stack<PathItem> path;
        protected Stack<Pair<RefKey>> indirects;
        
        public ObjectPath() {
            this.path = new Stack<PathItem>();
            this.indirects = new Stack<Pair<RefKey>>();
        }
        
        protected ObjectPath(final RefKey baseCmpObject, final RefKey baseOutObject) {
            this.path = new Stack<PathItem>();
            this.indirects = new Stack<Pair<RefKey>>();
            this.baseCmpObject = baseCmpObject;
            this.baseOutObject = baseOutObject;
        }
        
        private ObjectPath(final RefKey baseCmpObject, final RefKey baseOutObject, final Stack<PathItem> path) {
            this.path = new Stack<PathItem>();
            this.indirects = new Stack<Pair<RefKey>>();
            this.baseCmpObject = baseCmpObject;
            this.baseOutObject = baseOutObject;
            this.path = path;
        }
        
        public ObjectPath resetDirectPath(final RefKey baseCmpObject, final RefKey baseOutObject) {
            final ObjectPath newPath = new ObjectPath(baseCmpObject, baseOutObject);
            (newPath.indirects = (Stack<Pair<RefKey>>)this.indirects.clone()).add(new Pair<RefKey>(baseCmpObject, baseOutObject));
            return newPath;
        }
        
        public boolean isComparing(final RefKey baseCmpObject, final RefKey baseOutObject) {
            return this.indirects.contains(new Pair(baseCmpObject, baseOutObject));
        }
        
        public void pushArrayItemToPath(final int index) {
            this.path.add(new ArrayPathItem(index));
        }
        
        public void pushDictItemToPath(final String key) {
            this.path.add(new DictPathItem(key));
        }
        
        public void pushOffsetToPath(final int offset) {
            this.path.add(new OffsetPathItem(offset));
        }
        
        public void pop() {
            this.path.pop();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(String.format("Base cmp object: %s obj. Base out object: %s obj", this.baseCmpObject, this.baseOutObject));
            for (final PathItem pathItem : this.path) {
                sb.append("\n");
                sb.append(pathItem.toString());
            }
            return sb.toString();
        }
        
        @Override
        public int hashCode() {
            final int hashCode1 = (this.baseCmpObject != null) ? this.baseCmpObject.hashCode() : 1;
            final int hashCode2 = (this.baseOutObject != null) ? this.baseOutObject.hashCode() : 1;
            int hashCode3 = hashCode1 * 31 + hashCode2;
            for (final PathItem pathItem : this.path) {
                hashCode3 *= 31;
                hashCode3 += pathItem.hashCode();
            }
            return hashCode3;
        }
        
        @Override
        public boolean equals(final Object obj) {
            return obj instanceof ObjectPath && this.baseCmpObject.equals(((ObjectPath)obj).baseCmpObject) && this.baseOutObject.equals(((ObjectPath)obj).baseOutObject) && this.path.equals(((ObjectPath)obj).path);
        }
        
        @Override
        protected Object clone() {
            return new ObjectPath(this.baseCmpObject, this.baseOutObject, (Stack<PathItem>)this.path.clone());
        }
        
        public Node toXmlNode(final Document document) {
            final Element element = document.createElement("path");
            final Element baseNode = document.createElement("base");
            baseNode.setAttribute("cmp", this.baseCmpObject.toString() + " obj");
            baseNode.setAttribute("out", this.baseOutObject.toString() + " obj");
            element.appendChild(baseNode);
            for (final PathItem pathItem : this.path) {
                element.appendChild(pathItem.toXmlNode(document));
            }
            return element;
        }
        
        private class Pair<T>
        {
            private T first;
            private T second;
            
            public Pair(final T first, final T second) {
                this.first = first;
                this.second = second;
            }
            
            @Override
            public int hashCode() {
                return this.first.hashCode() * 31 + this.second.hashCode();
            }
            
            @Override
            public boolean equals(final Object obj) {
                return obj instanceof Pair && this.first.equals(((Pair)obj).first) && this.second.equals(((Pair)obj).second);
            }
        }
        
        private abstract class PathItem
        {
            protected abstract Node toXmlNode(final Document p0);
        }
        
        private class DictPathItem extends PathItem
        {
            String key;
            
            public DictPathItem(final String key) {
                this.key = key;
            }
            
            @Override
            public String toString() {
                return "Dict key: " + this.key;
            }
            
            @Override
            public int hashCode() {
                return this.key.hashCode();
            }
            
            @Override
            public boolean equals(final Object obj) {
                return obj instanceof DictPathItem && this.key.equals(((DictPathItem)obj).key);
            }
            
            @Override
            protected Node toXmlNode(final Document document) {
                final Node element = document.createElement("dictKey");
                element.appendChild(document.createTextNode(this.key));
                return element;
            }
        }
        
        private class ArrayPathItem extends PathItem
        {
            int index;
            
            public ArrayPathItem(final int index) {
                this.index = index;
            }
            
            @Override
            public String toString() {
                return "Array index: " + String.valueOf(this.index);
            }
            
            @Override
            public int hashCode() {
                return this.index;
            }
            
            @Override
            public boolean equals(final Object obj) {
                return obj instanceof ArrayPathItem && this.index == ((ArrayPathItem)obj).index;
            }
            
            @Override
            protected Node toXmlNode(final Document document) {
                final Node element = document.createElement("arrayIndex");
                element.appendChild(document.createTextNode(String.valueOf(this.index)));
                return element;
            }
        }
        
        private class OffsetPathItem extends PathItem
        {
            int offset;
            
            public OffsetPathItem(final int offset) {
                this.offset = offset;
            }
            
            @Override
            public String toString() {
                return "Offset: " + String.valueOf(this.offset);
            }
            
            @Override
            public int hashCode() {
                return this.offset;
            }
            
            @Override
            public boolean equals(final Object obj) {
                return obj instanceof OffsetPathItem && this.offset == ((OffsetPathItem)obj).offset;
            }
            
            @Override
            protected Node toXmlNode(final Document document) {
                final Node element = document.createElement("offset");
                element.appendChild(document.createTextNode(String.valueOf(this.offset)));
                return element;
            }
        }
    }
    
    protected class CompareResult
    {
        protected Map<ObjectPath, String> differences;
        protected int messageLimit;
        
        public CompareResult(final int messageLimit) {
            this.differences = new LinkedHashMap<ObjectPath, String>();
            this.messageLimit = 1;
            this.messageLimit = messageLimit;
        }
        
        public boolean isOk() {
            return this.differences.size() == 0;
        }
        
        public int getErrorCount() {
            return this.differences.size();
        }
        
        protected boolean isMessageLimitReached() {
            return this.differences.size() >= this.messageLimit;
        }
        
        public String getReport() {
            final StringBuilder sb = new StringBuilder();
            boolean firstEntry = true;
            for (final Map.Entry<ObjectPath, String> entry : this.differences.entrySet()) {
                if (!firstEntry) {
                    sb.append("-----------------------------").append("\n");
                }
                final ObjectPath diffPath = entry.getKey();
                sb.append(entry.getValue()).append("\n").append(diffPath.toString()).append("\n");
                firstEntry = false;
            }
            return sb.toString();
        }
        
        protected void addError(final ObjectPath path, final String message) {
            if (this.differences.size() < this.messageLimit) {
                this.differences.put((ObjectPath)path.clone(), message);
            }
        }
        
        public void writeReportToXml(final OutputStream stream) throws ParserConfigurationException, TransformerException {
            final Document xmlReport = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            final Element root = xmlReport.createElement("report");
            final Element errors = xmlReport.createElement("errors");
            errors.setAttribute("count", String.valueOf(this.differences.size()));
            root.appendChild(errors);
            for (final Map.Entry<ObjectPath, String> entry : this.differences.entrySet()) {
                final Node errorNode = xmlReport.createElement("error");
                final Node message = xmlReport.createElement("message");
                message.appendChild(xmlReport.createTextNode(entry.getValue()));
                final Node path = entry.getKey().toXmlNode(xmlReport);
                errorNode.appendChild(message);
                errorNode.appendChild(path);
                errors.appendChild(errorNode);
            }
            xmlReport.appendChild(root);
            final TransformerFactory tFactory = TransformerFactory.newInstance();
            try {
                tFactory.setAttribute("http://javax.xml.XMLConstants/feature/secure-processing", true);
            }
            catch (Exception ex) {}
            final Transformer transformer = tFactory.newTransformer();
            transformer.setOutputProperty("indent", "yes");
            final DOMSource source = new DOMSource(xmlReport);
            final StreamResult result = new StreamResult(stream);
            transformer.transform(source, result);
        }
    }
    
    class PngFileFilter implements FileFilter
    {
        @Override
        public boolean accept(final File pathname) {
            final String ap = pathname.getAbsolutePath();
            final boolean b1 = ap.endsWith(".png");
            final boolean b2 = ap.contains("cmp_");
            return b1 && !b2 && ap.contains(CompareTool.this.outPdfName);
        }
    }
    
    class CmpPngFileFilter implements FileFilter
    {
        @Override
        public boolean accept(final File pathname) {
            final String ap = pathname.getAbsolutePath();
            final boolean b1 = ap.endsWith(".png");
            final boolean b2 = ap.contains("cmp_");
            return b1 && b2 && ap.contains(CompareTool.this.cmpPdfName);
        }
    }
    
    class ImageNameComparator implements Comparator<File>
    {
        @Override
        public int compare(final File f1, final File f2) {
            final String f1Name = f1.getAbsolutePath();
            final String f2Name = f2.getAbsolutePath();
            return f1Name.compareTo(f2Name);
        }
    }
    
    class CmpTaggedPdfReaderTool extends TaggedPdfReaderTool
    {
        Map<PdfDictionary, Map<Integer, String>> parsedTags;
        
        CmpTaggedPdfReaderTool() {
            this.parsedTags = new HashMap<PdfDictionary, Map<Integer, String>>();
        }
        
        @Override
        public void parseTag(final String tag, final PdfObject object, final PdfDictionary page) throws IOException {
            if (object instanceof PdfNumber) {
                if (!this.parsedTags.containsKey(page)) {
                    final CmpMarkedContentRenderFilter listener = new CmpMarkedContentRenderFilter();
                    final PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
                    processor.processContent(PdfReader.getPageContent(page), page.getAsDict(PdfName.RESOURCES));
                    this.parsedTags.put(page, listener.getParsedTagContent());
                }
                String tagContent = "";
                if (this.parsedTags.get(page).containsKey(((PdfNumber)object).intValue())) {
                    tagContent = this.parsedTags.get(page).get(((PdfNumber)object).intValue());
                }
                this.out.print(XMLUtil.escapeXML(tagContent, true));
            }
            else {
                super.parseTag(tag, object, page);
            }
        }
        
        @Override
        public void inspectChildDictionary(final PdfDictionary k) throws IOException {
            this.inspectChildDictionary(k, true);
        }
    }
    
    class CmpMarkedContentRenderFilter implements RenderListener
    {
        Map<Integer, TextExtractionStrategy> tagsByMcid;
        
        CmpMarkedContentRenderFilter() {
            this.tagsByMcid = new HashMap<Integer, TextExtractionStrategy>();
        }
        
        public Map<Integer, String> getParsedTagContent() {
            final Map<Integer, String> content = new HashMap<Integer, String>();
            for (final int id : this.tagsByMcid.keySet()) {
                content.put(id, this.tagsByMcid.get(id).getResultantText());
            }
            return content;
        }
        
        @Override
        public void beginTextBlock() {
            for (final int id : this.tagsByMcid.keySet()) {
                this.tagsByMcid.get(id).beginTextBlock();
            }
        }
        
        @Override
        public void renderText(final TextRenderInfo renderInfo) {
            final Integer mcid = renderInfo.getMcid();
            if (mcid != null && this.tagsByMcid.containsKey(mcid)) {
                this.tagsByMcid.get(mcid).renderText(renderInfo);
            }
            else if (mcid != null) {
                this.tagsByMcid.put(mcid, new SimpleTextExtractionStrategy());
                this.tagsByMcid.get(mcid).renderText(renderInfo);
            }
        }
        
        @Override
        public void endTextBlock() {
            for (final int id : this.tagsByMcid.keySet()) {
                this.tagsByMcid.get(id).endTextBlock();
            }
        }
        
        @Override
        public void renderImage(final ImageRenderInfo renderInfo) {
        }
    }
    
    private static class SafeEmptyEntityResolver implements EntityResolver
    {
        @Override
        public InputSource resolveEntity(final String publicId, final String systemId) throws SAXException, IOException {
            return new InputSource(new StringReader(""));
        }
    }
}
