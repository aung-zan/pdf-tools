// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import org.bouncycastle.util.Arrays;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DERApplicationSpecific extends ASN1Primitive
{
    private final boolean isConstructed;
    private final int tag;
    private final byte[] octets;
    
    DERApplicationSpecific(final boolean isConstructed, final int tag, final byte[] octets) {
        this.isConstructed = isConstructed;
        this.tag = tag;
        this.octets = octets;
    }
    
    public DERApplicationSpecific(final int n, final byte[] array) {
        this(false, n, array);
    }
    
    public DERApplicationSpecific(final int n, final ASN1Encodable asn1Encodable) throws IOException {
        this(true, n, asn1Encodable);
    }
    
    public DERApplicationSpecific(final boolean b, final int tag, final ASN1Encodable asn1Encodable) throws IOException {
        final ASN1Primitive asn1Primitive = asn1Encodable.toASN1Primitive();
        final byte[] encoded = asn1Primitive.getEncoded("DER");
        this.isConstructed = (b || asn1Primitive instanceof ASN1Set || asn1Primitive instanceof ASN1Sequence);
        this.tag = tag;
        if (b) {
            this.octets = encoded;
        }
        else {
            final int lengthOfHeader = this.getLengthOfHeader(encoded);
            final byte[] octets = new byte[encoded.length - lengthOfHeader];
            System.arraycopy(encoded, lengthOfHeader, octets, 0, octets.length);
            this.octets = octets;
        }
    }
    
    public DERApplicationSpecific(final int tag, final ASN1EncodableVector asn1EncodableVector) {
        this.tag = tag;
        this.isConstructed = true;
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (int i = 0; i != asn1EncodableVector.size(); ++i) {
            try {
                byteArrayOutputStream.write(((ASN1Object)asn1EncodableVector.get(i)).getEncoded("DER"));
            }
            catch (IOException obj) {
                throw new ASN1ParsingException("malformed object: " + obj, obj);
            }
        }
        this.octets = byteArrayOutputStream.toByteArray();
    }
    
    public static DERApplicationSpecific getInstance(final Object o) {
        if (o == null || o instanceof DERApplicationSpecific) {
            return (DERApplicationSpecific)o;
        }
        if (o instanceof byte[]) {
            try {
                return getInstance(ASN1Primitive.fromByteArray((byte[])o));
            }
            catch (IOException ex) {
                throw new IllegalArgumentException("failed to construct object from byte[]: " + ex.getMessage());
            }
        }
        if (o instanceof ASN1Encodable) {
            final ASN1Primitive asn1Primitive = ((ASN1Encodable)o).toASN1Primitive();
            if (asn1Primitive instanceof ASN1Sequence) {
                return (DERApplicationSpecific)asn1Primitive;
            }
        }
        throw new IllegalArgumentException("unknown object in getInstance: " + o.getClass().getName());
    }
    
    private int getLengthOfHeader(final byte[] array) {
        final int n = array[1] & 0xFF;
        if (n == 128) {
            return 2;
        }
        if (n <= 127) {
            return 2;
        }
        final int i = n & 0x7F;
        if (i > 4) {
            throw new IllegalStateException("DER length more than 4 bytes: " + i);
        }
        return i + 2;
    }
    
    public boolean isConstructed() {
        return this.isConstructed;
    }
    
    public byte[] getContents() {
        return this.octets;
    }
    
    public int getApplicationTag() {
        return this.tag;
    }
    
    public ASN1Primitive getObject() throws IOException {
        return new ASN1InputStream(this.getContents()).readObject();
    }
    
    public ASN1Primitive getObject(final int n) throws IOException {
        if (n >= 31) {
            throw new IOException("unsupported tag number");
        }
        final byte[] encoded = this.getEncoded();
        final byte[] replaceTagNumber = this.replaceTagNumber(n, encoded);
        if ((encoded[0] & 0x20) != 0x0) {
            final byte[] array = replaceTagNumber;
            final int n2 = 0;
            array[n2] |= 0x20;
        }
        return new ASN1InputStream(replaceTagNumber).readObject();
    }
    
    @Override
    int encodedLength() throws IOException {
        return StreamUtil.calculateTagLength(this.tag) + StreamUtil.calculateBodyLength(this.octets.length) + this.octets.length;
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream) throws IOException {
        int n = 64;
        if (this.isConstructed) {
            n |= 0x20;
        }
        asn1OutputStream.writeEncoded(n, this.tag, this.octets);
    }
    
    @Override
    boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        if (!(asn1Primitive instanceof DERApplicationSpecific)) {
            return false;
        }
        final DERApplicationSpecific derApplicationSpecific = (DERApplicationSpecific)asn1Primitive;
        return this.isConstructed == derApplicationSpecific.isConstructed && this.tag == derApplicationSpecific.tag && Arrays.areEqual(this.octets, derApplicationSpecific.octets);
    }
    
    @Override
    public int hashCode() {
        return (this.isConstructed ? 1 : 0) ^ this.tag ^ Arrays.hashCode(this.octets);
    }
    
    private byte[] replaceTagNumber(final int n, final byte[] array) throws IOException {
        final int n2 = array[0] & 0x1F;
        int n3 = 1;
        if (n2 == 31) {
            int n4 = 0;
            int n5 = array[n3++] & 0xFF;
            if ((n5 & 0x7F) == 0x0) {
                throw new ASN1ParsingException("corrupted stream - invalid high tag number found");
            }
            while (n5 >= 0 && (n5 & 0x80) != 0x0) {
                n4 = (n4 | (n5 & 0x7F)) << 7;
                n5 = (array[n3++] & 0xFF);
            }
        }
        final byte[] array2 = new byte[array.length - n3 + 1];
        System.arraycopy(array, n3, array2, 1, array2.length - 1);
        array2[0] = (byte)n;
        return array2;
    }
}
