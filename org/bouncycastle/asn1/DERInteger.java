// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import org.bouncycastle.util.Arrays;
import java.io.IOException;
import java.math.BigInteger;

public class DERInteger extends ASN1Primitive
{
    byte[] bytes;
    
    public static ASN1Integer getInstance(final Object o) {
        if (o == null || o instanceof ASN1Integer) {
            return (ASN1Integer)o;
        }
        if (o instanceof DERInteger) {
            return new ASN1Integer(((DERInteger)o).getValue());
        }
        if (o instanceof byte[]) {
            try {
                return (ASN1Integer)ASN1Primitive.fromByteArray((byte[])o);
            }
            catch (Exception ex) {
                throw new IllegalArgumentException("encoding error in getInstance: " + ex.toString());
            }
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + o.getClass().getName());
    }
    
    public static ASN1Integer getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        final ASN1Primitive object = asn1TaggedObject.getObject();
        if (b || object instanceof DERInteger) {
            return getInstance(object);
        }
        return new ASN1Integer(ASN1OctetString.getInstance(asn1TaggedObject.getObject()).getOctets());
    }
    
    public DERInteger(final long val) {
        this.bytes = BigInteger.valueOf(val).toByteArray();
    }
    
    public DERInteger(final BigInteger bigInteger) {
        this.bytes = bigInteger.toByteArray();
    }
    
    public DERInteger(final byte[] bytes) {
        this.bytes = bytes;
    }
    
    public BigInteger getValue() {
        return new BigInteger(this.bytes);
    }
    
    public BigInteger getPositiveValue() {
        return new BigInteger(1, this.bytes);
    }
    
    @Override
    boolean isConstructed() {
        return false;
    }
    
    @Override
    int encodedLength() {
        return 1 + StreamUtil.calculateBodyLength(this.bytes.length) + this.bytes.length;
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream) throws IOException {
        asn1OutputStream.writeEncoded(2, this.bytes);
    }
    
    @Override
    public int hashCode() {
        int n = 0;
        for (int i = 0; i != this.bytes.length; ++i) {
            n ^= (this.bytes[i] & 0xFF) << i % 4;
        }
        return n;
    }
    
    @Override
    boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        return asn1Primitive instanceof DERInteger && Arrays.areEqual(this.bytes, ((DERInteger)asn1Primitive).bytes);
    }
    
    @Override
    public String toString() {
        return this.getValue().toString();
    }
}
