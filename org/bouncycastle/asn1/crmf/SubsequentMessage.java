// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.crmf;

import org.bouncycastle.asn1.ASN1Integer;

public class SubsequentMessage extends ASN1Integer
{
    public static final SubsequentMessage encrCert;
    public static final SubsequentMessage challengeResp;
    
    private SubsequentMessage(final int n) {
        super(n);
    }
    
    public static SubsequentMessage valueOf(final int i) {
        if (i == 0) {
            return SubsequentMessage.encrCert;
        }
        if (i == 1) {
            return SubsequentMessage.challengeResp;
        }
        throw new IllegalArgumentException("unknown value: " + i);
    }
    
    static {
        encrCert = new SubsequentMessage(0);
        challengeResp = new SubsequentMessage(1);
    }
}
