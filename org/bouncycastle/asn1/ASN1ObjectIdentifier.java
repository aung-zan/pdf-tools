// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

public class ASN1ObjectIdentifier extends DERObjectIdentifier
{
    public ASN1ObjectIdentifier(final String s) {
        super(s);
    }
    
    ASN1ObjectIdentifier(final byte[] array) {
        super(array);
    }
    
    ASN1ObjectIdentifier(final ASN1ObjectIdentifier asn1ObjectIdentifier, final String s) {
        super(asn1ObjectIdentifier, s);
    }
    
    public ASN1ObjectIdentifier branch(final String s) {
        return new ASN1ObjectIdentifier(this, s);
    }
    
    public boolean on(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        final String id = this.getId();
        final String id2 = asn1ObjectIdentifier.getId();
        return id.length() > id2.length() && id.charAt(id2.length()) == '.' && id.startsWith(id2);
    }
}
