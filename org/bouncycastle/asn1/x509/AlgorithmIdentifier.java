// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x509;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Object;

public class AlgorithmIdentifier extends ASN1Object
{
    private ASN1ObjectIdentifier objectId;
    private ASN1Encodable parameters;
    private boolean parametersDefined;
    
    public static AlgorithmIdentifier getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, b));
    }
    
    public static AlgorithmIdentifier getInstance(final Object o) {
        if (o == null || o instanceof AlgorithmIdentifier) {
            return (AlgorithmIdentifier)o;
        }
        if (o instanceof ASN1ObjectIdentifier) {
            return new AlgorithmIdentifier((ASN1ObjectIdentifier)o);
        }
        if (o instanceof String) {
            return new AlgorithmIdentifier((String)o);
        }
        return new AlgorithmIdentifier(ASN1Sequence.getInstance(o));
    }
    
    public AlgorithmIdentifier(final ASN1ObjectIdentifier objectId) {
        this.parametersDefined = false;
        this.objectId = objectId;
    }
    
    @Deprecated
    public AlgorithmIdentifier(final String s) {
        this.parametersDefined = false;
        this.objectId = new ASN1ObjectIdentifier(s);
    }
    
    @Deprecated
    public AlgorithmIdentifier(final DERObjectIdentifier derObjectIdentifier) {
        this.parametersDefined = false;
        this.objectId = new ASN1ObjectIdentifier(derObjectIdentifier.getId());
    }
    
    @Deprecated
    public AlgorithmIdentifier(final DERObjectIdentifier derObjectIdentifier, final ASN1Encodable parameters) {
        this.parametersDefined = false;
        this.parametersDefined = true;
        this.objectId = new ASN1ObjectIdentifier(derObjectIdentifier.getId());
        this.parameters = parameters;
    }
    
    public AlgorithmIdentifier(final ASN1ObjectIdentifier objectId, final ASN1Encodable parameters) {
        this.parametersDefined = false;
        this.parametersDefined = true;
        this.objectId = objectId;
        this.parameters = parameters;
    }
    
    @Deprecated
    public AlgorithmIdentifier(final ASN1Sequence asn1Sequence) {
        this.parametersDefined = false;
        if (asn1Sequence.size() < 1 || asn1Sequence.size() > 2) {
            throw new IllegalArgumentException("Bad sequence size: " + asn1Sequence.size());
        }
        this.objectId = DERObjectIdentifier.getInstance(asn1Sequence.getObjectAt(0));
        if (asn1Sequence.size() == 2) {
            this.parametersDefined = true;
            this.parameters = asn1Sequence.getObjectAt(1);
        }
        else {
            this.parameters = null;
        }
    }
    
    public ASN1ObjectIdentifier getAlgorithm() {
        return new ASN1ObjectIdentifier(this.objectId.getId());
    }
    
    @Deprecated
    public ASN1ObjectIdentifier getObjectId() {
        return this.objectId;
    }
    
    public ASN1Encodable getParameters() {
        return this.parameters;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        asn1EncodableVector.add(this.objectId);
        if (this.parametersDefined) {
            if (this.parameters != null) {
                asn1EncodableVector.add(this.parameters);
            }
            else {
                asn1EncodableVector.add(DERNull.INSTANCE);
            }
        }
        return new DERSequence(asn1EncodableVector);
    }
}
