// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.pkcs;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import java.math.BigInteger;
import java.util.Enumeration;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Object;

public class PBKDF2Params extends ASN1Object
{
    private ASN1OctetString octStr;
    private ASN1Integer iterationCount;
    private ASN1Integer keyLength;
    
    public static PBKDF2Params getInstance(final Object o) {
        if (o instanceof PBKDF2Params) {
            return (PBKDF2Params)o;
        }
        if (o != null) {
            return new PBKDF2Params(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public PBKDF2Params(final byte[] array, final int n) {
        this.octStr = new DEROctetString(array);
        this.iterationCount = new ASN1Integer(n);
    }
    
    public PBKDF2Params(final byte[] array, final int n, final int n2) {
        this(array, n);
        this.keyLength = new ASN1Integer(n2);
    }
    
    private PBKDF2Params(final ASN1Sequence asn1Sequence) {
        final Enumeration objects = asn1Sequence.getObjects();
        this.octStr = objects.nextElement();
        this.iterationCount = (ASN1Integer)objects.nextElement();
        if (objects.hasMoreElements()) {
            this.keyLength = (ASN1Integer)objects.nextElement();
        }
        else {
            this.keyLength = null;
        }
    }
    
    public byte[] getSalt() {
        return this.octStr.getOctets();
    }
    
    public BigInteger getIterationCount() {
        return this.iterationCount.getValue();
    }
    
    public BigInteger getKeyLength() {
        if (this.keyLength != null) {
            return this.keyLength.getValue();
        }
        return null;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        asn1EncodableVector.add(this.octStr);
        asn1EncodableVector.add(this.iterationCount);
        if (this.keyLength != null) {
            asn1EncodableVector.add(this.keyLength);
        }
        return new DERSequence(asn1EncodableVector);
    }
}
