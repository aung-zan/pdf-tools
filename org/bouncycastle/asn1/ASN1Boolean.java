// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

public class ASN1Boolean extends DERBoolean
{
    public ASN1Boolean(final boolean b) {
        super(b);
    }
    
    ASN1Boolean(final byte[] array) {
        super(array);
    }
}
