// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x500.style;

import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.asn1.x500.AttributeTypeAndValue;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.DERPrintableString;
import org.bouncycastle.asn1.ASN1GeneralizedTime;
import org.bouncycastle.asn1.DERIA5String;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Encodable;
import java.util.Hashtable;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.X500NameStyle;

public class BCStyle implements X500NameStyle
{
    public static final X500NameStyle INSTANCE;
    public static final ASN1ObjectIdentifier C;
    public static final ASN1ObjectIdentifier O;
    public static final ASN1ObjectIdentifier OU;
    public static final ASN1ObjectIdentifier T;
    public static final ASN1ObjectIdentifier CN;
    public static final ASN1ObjectIdentifier SN;
    public static final ASN1ObjectIdentifier STREET;
    public static final ASN1ObjectIdentifier SERIALNUMBER;
    public static final ASN1ObjectIdentifier L;
    public static final ASN1ObjectIdentifier ST;
    public static final ASN1ObjectIdentifier SURNAME;
    public static final ASN1ObjectIdentifier GIVENNAME;
    public static final ASN1ObjectIdentifier INITIALS;
    public static final ASN1ObjectIdentifier GENERATION;
    public static final ASN1ObjectIdentifier UNIQUE_IDENTIFIER;
    public static final ASN1ObjectIdentifier BUSINESS_CATEGORY;
    public static final ASN1ObjectIdentifier POSTAL_CODE;
    public static final ASN1ObjectIdentifier DN_QUALIFIER;
    public static final ASN1ObjectIdentifier PSEUDONYM;
    public static final ASN1ObjectIdentifier DATE_OF_BIRTH;
    public static final ASN1ObjectIdentifier PLACE_OF_BIRTH;
    public static final ASN1ObjectIdentifier GENDER;
    public static final ASN1ObjectIdentifier COUNTRY_OF_CITIZENSHIP;
    public static final ASN1ObjectIdentifier COUNTRY_OF_RESIDENCE;
    public static final ASN1ObjectIdentifier NAME_AT_BIRTH;
    public static final ASN1ObjectIdentifier POSTAL_ADDRESS;
    public static final ASN1ObjectIdentifier DMD_NAME;
    public static final ASN1ObjectIdentifier TELEPHONE_NUMBER;
    public static final ASN1ObjectIdentifier NAME;
    public static final ASN1ObjectIdentifier EmailAddress;
    public static final ASN1ObjectIdentifier UnstructuredName;
    public static final ASN1ObjectIdentifier UnstructuredAddress;
    public static final ASN1ObjectIdentifier E;
    public static final ASN1ObjectIdentifier DC;
    public static final ASN1ObjectIdentifier UID;
    private static final Hashtable DefaultSymbols;
    private static final Hashtable DefaultLookUp;
    
    protected BCStyle() {
    }
    
    public ASN1Encodable stringToValue(final ASN1ObjectIdentifier asn1ObjectIdentifier, String substring) {
        if (substring.length() != 0 && substring.charAt(0) == '#') {
            try {
                return IETFUtils.valueFromHexString(substring, 1);
            }
            catch (IOException ex) {
                throw new RuntimeException("can't recode value for oid " + asn1ObjectIdentifier.getId());
            }
        }
        if (substring.length() != 0 && substring.charAt(0) == '\\') {
            substring = substring.substring(1);
        }
        if (asn1ObjectIdentifier.equals(BCStyle.EmailAddress) || asn1ObjectIdentifier.equals(BCStyle.DC)) {
            return new DERIA5String(substring);
        }
        if (asn1ObjectIdentifier.equals(BCStyle.DATE_OF_BIRTH)) {
            return new ASN1GeneralizedTime(substring);
        }
        if (asn1ObjectIdentifier.equals(BCStyle.C) || asn1ObjectIdentifier.equals(BCStyle.SN) || asn1ObjectIdentifier.equals(BCStyle.DN_QUALIFIER) || asn1ObjectIdentifier.equals(BCStyle.TELEPHONE_NUMBER)) {
            return new DERPrintableString(substring);
        }
        return new DERUTF8String(substring);
    }
    
    public String oidToDisplayName(final ASN1ObjectIdentifier key) {
        return BCStyle.DefaultSymbols.get(key);
    }
    
    public String[] oidToAttrNames(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        return IETFUtils.findAttrNamesForOID(asn1ObjectIdentifier, BCStyle.DefaultLookUp);
    }
    
    public ASN1ObjectIdentifier attrNameToOID(final String s) {
        return IETFUtils.decodeAttrName(s, BCStyle.DefaultLookUp);
    }
    
    public boolean areEqual(final X500Name x500Name, final X500Name x500Name2) {
        final RDN[] rdNs = x500Name.getRDNs();
        final RDN[] rdNs2 = x500Name2.getRDNs();
        if (rdNs.length != rdNs2.length) {
            return false;
        }
        boolean b = false;
        if (rdNs[0].getFirst() != null && rdNs2[0].getFirst() != null) {
            b = !rdNs[0].getFirst().getType().equals(rdNs2[0].getFirst().getType());
        }
        for (int i = 0; i != rdNs.length; ++i) {
            if (!this.foundMatch(b, rdNs[i], rdNs2)) {
                return false;
            }
        }
        return true;
    }
    
    private boolean foundMatch(final boolean b, final RDN rdn, final RDN[] array) {
        if (b) {
            for (int i = array.length - 1; i >= 0; --i) {
                if (array[i] != null && this.rdnAreEqual(rdn, array[i])) {
                    array[i] = null;
                    return true;
                }
            }
        }
        else {
            for (int j = 0; j != array.length; ++j) {
                if (array[j] != null && this.rdnAreEqual(rdn, array[j])) {
                    array[j] = null;
                    return true;
                }
            }
        }
        return false;
    }
    
    protected boolean rdnAreEqual(final RDN rdn, final RDN rdn2) {
        return IETFUtils.rDNAreEqual(rdn, rdn2);
    }
    
    public RDN[] fromString(final String s) {
        return IETFUtils.rDNsFromString(s, this);
    }
    
    public int calculateHashCode(final X500Name x500Name) {
        int n = 0;
        final RDN[] rdNs = x500Name.getRDNs();
        for (int i = 0; i != rdNs.length; ++i) {
            if (rdNs[i].isMultiValued()) {
                final AttributeTypeAndValue[] typesAndValues = rdNs[i].getTypesAndValues();
                for (int j = 0; j != typesAndValues.length; ++j) {
                    n = (n ^ typesAndValues[j].getType().hashCode() ^ this.calcHashCode(typesAndValues[j].getValue()));
                }
            }
            else {
                n = (n ^ rdNs[i].getFirst().getType().hashCode() ^ this.calcHashCode(rdNs[i].getFirst().getValue()));
            }
        }
        return n;
    }
    
    private int calcHashCode(final ASN1Encodable asn1Encodable) {
        return IETFUtils.canonicalize(IETFUtils.valueToString(asn1Encodable)).hashCode();
    }
    
    public String toString(final X500Name x500Name) {
        final StringBuffer sb = new StringBuffer();
        int n = 1;
        final RDN[] rdNs = x500Name.getRDNs();
        for (int i = 0; i < rdNs.length; ++i) {
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(',');
            }
            IETFUtils.appendRDN(sb, rdNs[i], BCStyle.DefaultSymbols);
        }
        return sb.toString();
    }
    
    static {
        INSTANCE = new BCStyle();
        C = new ASN1ObjectIdentifier("2.5.4.6");
        O = new ASN1ObjectIdentifier("2.5.4.10");
        OU = new ASN1ObjectIdentifier("2.5.4.11");
        T = new ASN1ObjectIdentifier("2.5.4.12");
        CN = new ASN1ObjectIdentifier("2.5.4.3");
        SN = new ASN1ObjectIdentifier("2.5.4.5");
        STREET = new ASN1ObjectIdentifier("2.5.4.9");
        SERIALNUMBER = BCStyle.SN;
        L = new ASN1ObjectIdentifier("2.5.4.7");
        ST = new ASN1ObjectIdentifier("2.5.4.8");
        SURNAME = new ASN1ObjectIdentifier("2.5.4.4");
        GIVENNAME = new ASN1ObjectIdentifier("2.5.4.42");
        INITIALS = new ASN1ObjectIdentifier("2.5.4.43");
        GENERATION = new ASN1ObjectIdentifier("2.5.4.44");
        UNIQUE_IDENTIFIER = new ASN1ObjectIdentifier("2.5.4.45");
        BUSINESS_CATEGORY = new ASN1ObjectIdentifier("2.5.4.15");
        POSTAL_CODE = new ASN1ObjectIdentifier("2.5.4.17");
        DN_QUALIFIER = new ASN1ObjectIdentifier("2.5.4.46");
        PSEUDONYM = new ASN1ObjectIdentifier("2.5.4.65");
        DATE_OF_BIRTH = new ASN1ObjectIdentifier("1.3.6.1.5.5.7.9.1");
        PLACE_OF_BIRTH = new ASN1ObjectIdentifier("1.3.6.1.5.5.7.9.2");
        GENDER = new ASN1ObjectIdentifier("1.3.6.1.5.5.7.9.3");
        COUNTRY_OF_CITIZENSHIP = new ASN1ObjectIdentifier("1.3.6.1.5.5.7.9.4");
        COUNTRY_OF_RESIDENCE = new ASN1ObjectIdentifier("1.3.6.1.5.5.7.9.5");
        NAME_AT_BIRTH = new ASN1ObjectIdentifier("1.3.36.8.3.14");
        POSTAL_ADDRESS = new ASN1ObjectIdentifier("2.5.4.16");
        DMD_NAME = new ASN1ObjectIdentifier("2.5.4.54");
        TELEPHONE_NUMBER = X509ObjectIdentifiers.id_at_telephoneNumber;
        NAME = X509ObjectIdentifiers.id_at_name;
        EmailAddress = PKCSObjectIdentifiers.pkcs_9_at_emailAddress;
        UnstructuredName = PKCSObjectIdentifiers.pkcs_9_at_unstructuredName;
        UnstructuredAddress = PKCSObjectIdentifiers.pkcs_9_at_unstructuredAddress;
        E = BCStyle.EmailAddress;
        DC = new ASN1ObjectIdentifier("0.9.2342.19200300.100.1.25");
        UID = new ASN1ObjectIdentifier("0.9.2342.19200300.100.1.1");
        DefaultSymbols = new Hashtable();
        DefaultLookUp = new Hashtable();
        BCStyle.DefaultSymbols.put(BCStyle.C, "C");
        BCStyle.DefaultSymbols.put(BCStyle.O, "O");
        BCStyle.DefaultSymbols.put(BCStyle.T, "T");
        BCStyle.DefaultSymbols.put(BCStyle.OU, "OU");
        BCStyle.DefaultSymbols.put(BCStyle.CN, "CN");
        BCStyle.DefaultSymbols.put(BCStyle.L, "L");
        BCStyle.DefaultSymbols.put(BCStyle.ST, "ST");
        BCStyle.DefaultSymbols.put(BCStyle.SN, "SERIALNUMBER");
        BCStyle.DefaultSymbols.put(BCStyle.EmailAddress, "E");
        BCStyle.DefaultSymbols.put(BCStyle.DC, "DC");
        BCStyle.DefaultSymbols.put(BCStyle.UID, "UID");
        BCStyle.DefaultSymbols.put(BCStyle.STREET, "STREET");
        BCStyle.DefaultSymbols.put(BCStyle.SURNAME, "SURNAME");
        BCStyle.DefaultSymbols.put(BCStyle.GIVENNAME, "GIVENNAME");
        BCStyle.DefaultSymbols.put(BCStyle.INITIALS, "INITIALS");
        BCStyle.DefaultSymbols.put(BCStyle.GENERATION, "GENERATION");
        BCStyle.DefaultSymbols.put(BCStyle.UnstructuredAddress, "unstructuredAddress");
        BCStyle.DefaultSymbols.put(BCStyle.UnstructuredName, "unstructuredName");
        BCStyle.DefaultSymbols.put(BCStyle.UNIQUE_IDENTIFIER, "UniqueIdentifier");
        BCStyle.DefaultSymbols.put(BCStyle.DN_QUALIFIER, "DN");
        BCStyle.DefaultSymbols.put(BCStyle.PSEUDONYM, "Pseudonym");
        BCStyle.DefaultSymbols.put(BCStyle.POSTAL_ADDRESS, "PostalAddress");
        BCStyle.DefaultSymbols.put(BCStyle.NAME_AT_BIRTH, "NameAtBirth");
        BCStyle.DefaultSymbols.put(BCStyle.COUNTRY_OF_CITIZENSHIP, "CountryOfCitizenship");
        BCStyle.DefaultSymbols.put(BCStyle.COUNTRY_OF_RESIDENCE, "CountryOfResidence");
        BCStyle.DefaultSymbols.put(BCStyle.GENDER, "Gender");
        BCStyle.DefaultSymbols.put(BCStyle.PLACE_OF_BIRTH, "PlaceOfBirth");
        BCStyle.DefaultSymbols.put(BCStyle.DATE_OF_BIRTH, "DateOfBirth");
        BCStyle.DefaultSymbols.put(BCStyle.POSTAL_CODE, "PostalCode");
        BCStyle.DefaultSymbols.put(BCStyle.BUSINESS_CATEGORY, "BusinessCategory");
        BCStyle.DefaultSymbols.put(BCStyle.TELEPHONE_NUMBER, "TelephoneNumber");
        BCStyle.DefaultSymbols.put(BCStyle.NAME, "Name");
        BCStyle.DefaultLookUp.put("c", BCStyle.C);
        BCStyle.DefaultLookUp.put("o", BCStyle.O);
        BCStyle.DefaultLookUp.put("t", BCStyle.T);
        BCStyle.DefaultLookUp.put("ou", BCStyle.OU);
        BCStyle.DefaultLookUp.put("cn", BCStyle.CN);
        BCStyle.DefaultLookUp.put("l", BCStyle.L);
        BCStyle.DefaultLookUp.put("st", BCStyle.ST);
        BCStyle.DefaultLookUp.put("sn", BCStyle.SN);
        BCStyle.DefaultLookUp.put("serialnumber", BCStyle.SN);
        BCStyle.DefaultLookUp.put("street", BCStyle.STREET);
        BCStyle.DefaultLookUp.put("emailaddress", BCStyle.E);
        BCStyle.DefaultLookUp.put("dc", BCStyle.DC);
        BCStyle.DefaultLookUp.put("e", BCStyle.E);
        BCStyle.DefaultLookUp.put("uid", BCStyle.UID);
        BCStyle.DefaultLookUp.put("surname", BCStyle.SURNAME);
        BCStyle.DefaultLookUp.put("givenname", BCStyle.GIVENNAME);
        BCStyle.DefaultLookUp.put("initials", BCStyle.INITIALS);
        BCStyle.DefaultLookUp.put("generation", BCStyle.GENERATION);
        BCStyle.DefaultLookUp.put("unstructuredaddress", BCStyle.UnstructuredAddress);
        BCStyle.DefaultLookUp.put("unstructuredname", BCStyle.UnstructuredName);
        BCStyle.DefaultLookUp.put("uniqueidentifier", BCStyle.UNIQUE_IDENTIFIER);
        BCStyle.DefaultLookUp.put("dn", BCStyle.DN_QUALIFIER);
        BCStyle.DefaultLookUp.put("pseudonym", BCStyle.PSEUDONYM);
        BCStyle.DefaultLookUp.put("postaladdress", BCStyle.POSTAL_ADDRESS);
        BCStyle.DefaultLookUp.put("nameofbirth", BCStyle.NAME_AT_BIRTH);
        BCStyle.DefaultLookUp.put("countryofcitizenship", BCStyle.COUNTRY_OF_CITIZENSHIP);
        BCStyle.DefaultLookUp.put("countryofresidence", BCStyle.COUNTRY_OF_RESIDENCE);
        BCStyle.DefaultLookUp.put("gender", BCStyle.GENDER);
        BCStyle.DefaultLookUp.put("placeofbirth", BCStyle.PLACE_OF_BIRTH);
        BCStyle.DefaultLookUp.put("dateofbirth", BCStyle.DATE_OF_BIRTH);
        BCStyle.DefaultLookUp.put("postalcode", BCStyle.POSTAL_CODE);
        BCStyle.DefaultLookUp.put("businesscategory", BCStyle.BUSINESS_CATEGORY);
        BCStyle.DefaultLookUp.put("telephonenumber", BCStyle.TELEPHONE_NUMBER);
        BCStyle.DefaultLookUp.put("name", BCStyle.NAME);
    }
}
