// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x500.style;

import org.bouncycastle.asn1.x500.AttributeTypeAndValue;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.DERPrintableString;
import org.bouncycastle.asn1.DERIA5String;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Encodable;
import java.util.Hashtable;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.X500NameStyle;

public class RFC4519Style implements X500NameStyle
{
    public static final X500NameStyle INSTANCE;
    public static final ASN1ObjectIdentifier businessCategory;
    public static final ASN1ObjectIdentifier c;
    public static final ASN1ObjectIdentifier cn;
    public static final ASN1ObjectIdentifier dc;
    public static final ASN1ObjectIdentifier description;
    public static final ASN1ObjectIdentifier destinationIndicator;
    public static final ASN1ObjectIdentifier distinguishedName;
    public static final ASN1ObjectIdentifier dnQualifier;
    public static final ASN1ObjectIdentifier enhancedSearchGuide;
    public static final ASN1ObjectIdentifier facsimileTelephoneNumber;
    public static final ASN1ObjectIdentifier generationQualifier;
    public static final ASN1ObjectIdentifier givenName;
    public static final ASN1ObjectIdentifier houseIdentifier;
    public static final ASN1ObjectIdentifier initials;
    public static final ASN1ObjectIdentifier internationalISDNNumber;
    public static final ASN1ObjectIdentifier l;
    public static final ASN1ObjectIdentifier member;
    public static final ASN1ObjectIdentifier name;
    public static final ASN1ObjectIdentifier o;
    public static final ASN1ObjectIdentifier ou;
    public static final ASN1ObjectIdentifier owner;
    public static final ASN1ObjectIdentifier physicalDeliveryOfficeName;
    public static final ASN1ObjectIdentifier postalAddress;
    public static final ASN1ObjectIdentifier postalCode;
    public static final ASN1ObjectIdentifier postOfficeBox;
    public static final ASN1ObjectIdentifier preferredDeliveryMethod;
    public static final ASN1ObjectIdentifier registeredAddress;
    public static final ASN1ObjectIdentifier roleOccupant;
    public static final ASN1ObjectIdentifier searchGuide;
    public static final ASN1ObjectIdentifier seeAlso;
    public static final ASN1ObjectIdentifier serialNumber;
    public static final ASN1ObjectIdentifier sn;
    public static final ASN1ObjectIdentifier st;
    public static final ASN1ObjectIdentifier street;
    public static final ASN1ObjectIdentifier telephoneNumber;
    public static final ASN1ObjectIdentifier teletexTerminalIdentifier;
    public static final ASN1ObjectIdentifier telexNumber;
    public static final ASN1ObjectIdentifier title;
    public static final ASN1ObjectIdentifier uid;
    public static final ASN1ObjectIdentifier uniqueMember;
    public static final ASN1ObjectIdentifier userPassword;
    public static final ASN1ObjectIdentifier x121Address;
    public static final ASN1ObjectIdentifier x500UniqueIdentifier;
    private static final Hashtable DefaultSymbols;
    private static final Hashtable DefaultLookUp;
    
    protected RFC4519Style() {
    }
    
    public ASN1Encodable stringToValue(final ASN1ObjectIdentifier asn1ObjectIdentifier, String substring) {
        if (substring.length() != 0 && substring.charAt(0) == '#') {
            try {
                return IETFUtils.valueFromHexString(substring, 1);
            }
            catch (IOException ex) {
                throw new RuntimeException("can't recode value for oid " + asn1ObjectIdentifier.getId());
            }
        }
        if (substring.length() != 0 && substring.charAt(0) == '\\') {
            substring = substring.substring(1);
        }
        if (asn1ObjectIdentifier.equals(RFC4519Style.dc)) {
            return new DERIA5String(substring);
        }
        if (asn1ObjectIdentifier.equals(RFC4519Style.c) || asn1ObjectIdentifier.equals(RFC4519Style.serialNumber) || asn1ObjectIdentifier.equals(RFC4519Style.dnQualifier) || asn1ObjectIdentifier.equals(RFC4519Style.telephoneNumber)) {
            return new DERPrintableString(substring);
        }
        return new DERUTF8String(substring);
    }
    
    public String oidToDisplayName(final ASN1ObjectIdentifier key) {
        return RFC4519Style.DefaultSymbols.get(key);
    }
    
    public String[] oidToAttrNames(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        return IETFUtils.findAttrNamesForOID(asn1ObjectIdentifier, RFC4519Style.DefaultLookUp);
    }
    
    public ASN1ObjectIdentifier attrNameToOID(final String s) {
        return IETFUtils.decodeAttrName(s, RFC4519Style.DefaultLookUp);
    }
    
    public boolean areEqual(final X500Name x500Name, final X500Name x500Name2) {
        final RDN[] rdNs = x500Name.getRDNs();
        final RDN[] rdNs2 = x500Name2.getRDNs();
        if (rdNs.length != rdNs2.length) {
            return false;
        }
        boolean b = false;
        if (rdNs[0].getFirst() != null && rdNs2[0].getFirst() != null) {
            b = !rdNs[0].getFirst().getType().equals(rdNs2[0].getFirst().getType());
        }
        for (int i = 0; i != rdNs.length; ++i) {
            if (!this.foundMatch(b, rdNs[i], rdNs2)) {
                return false;
            }
        }
        return true;
    }
    
    private boolean foundMatch(final boolean b, final RDN rdn, final RDN[] array) {
        if (b) {
            for (int i = array.length - 1; i >= 0; --i) {
                if (array[i] != null && this.rdnAreEqual(rdn, array[i])) {
                    array[i] = null;
                    return true;
                }
            }
        }
        else {
            for (int j = 0; j != array.length; ++j) {
                if (array[j] != null && this.rdnAreEqual(rdn, array[j])) {
                    array[j] = null;
                    return true;
                }
            }
        }
        return false;
    }
    
    protected boolean rdnAreEqual(final RDN rdn, final RDN rdn2) {
        return IETFUtils.rDNAreEqual(rdn, rdn2);
    }
    
    public RDN[] fromString(final String s) {
        final RDN[] rdNsFromString = IETFUtils.rDNsFromString(s, this);
        final RDN[] array = new RDN[rdNsFromString.length];
        for (int i = 0; i != rdNsFromString.length; ++i) {
            array[array.length - i - 1] = rdNsFromString[i];
        }
        return array;
    }
    
    public int calculateHashCode(final X500Name x500Name) {
        int n = 0;
        final RDN[] rdNs = x500Name.getRDNs();
        for (int i = 0; i != rdNs.length; ++i) {
            if (rdNs[i].isMultiValued()) {
                final AttributeTypeAndValue[] typesAndValues = rdNs[i].getTypesAndValues();
                for (int j = 0; j != typesAndValues.length; ++j) {
                    n = (n ^ typesAndValues[j].getType().hashCode() ^ this.calcHashCode(typesAndValues[j].getValue()));
                }
            }
            else {
                n = (n ^ rdNs[i].getFirst().getType().hashCode() ^ this.calcHashCode(rdNs[i].getFirst().getValue()));
            }
        }
        return n;
    }
    
    private int calcHashCode(final ASN1Encodable asn1Encodable) {
        return IETFUtils.canonicalize(IETFUtils.valueToString(asn1Encodable)).hashCode();
    }
    
    public String toString(final X500Name x500Name) {
        final StringBuffer sb = new StringBuffer();
        int n = 1;
        final RDN[] rdNs = x500Name.getRDNs();
        for (int i = rdNs.length - 1; i >= 0; --i) {
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(',');
            }
            IETFUtils.appendRDN(sb, rdNs[i], RFC4519Style.DefaultSymbols);
        }
        return sb.toString();
    }
    
    static {
        INSTANCE = new RFC4519Style();
        businessCategory = new ASN1ObjectIdentifier("2.5.4.15");
        c = new ASN1ObjectIdentifier("2.5.4.6");
        cn = new ASN1ObjectIdentifier("2.5.4.3");
        dc = new ASN1ObjectIdentifier("0.9.2342.19200300.100.1.25");
        description = new ASN1ObjectIdentifier("2.5.4.13");
        destinationIndicator = new ASN1ObjectIdentifier("2.5.4.27");
        distinguishedName = new ASN1ObjectIdentifier("2.5.4.49");
        dnQualifier = new ASN1ObjectIdentifier("2.5.4.46");
        enhancedSearchGuide = new ASN1ObjectIdentifier("2.5.4.47");
        facsimileTelephoneNumber = new ASN1ObjectIdentifier("2.5.4.23");
        generationQualifier = new ASN1ObjectIdentifier("2.5.4.44");
        givenName = new ASN1ObjectIdentifier("2.5.4.42");
        houseIdentifier = new ASN1ObjectIdentifier("2.5.4.51");
        initials = new ASN1ObjectIdentifier("2.5.4.43");
        internationalISDNNumber = new ASN1ObjectIdentifier("2.5.4.25");
        l = new ASN1ObjectIdentifier("2.5.4.7");
        member = new ASN1ObjectIdentifier("2.5.4.31");
        name = new ASN1ObjectIdentifier("2.5.4.41");
        o = new ASN1ObjectIdentifier("2.5.4.10");
        ou = new ASN1ObjectIdentifier("2.5.4.11");
        owner = new ASN1ObjectIdentifier("2.5.4.32");
        physicalDeliveryOfficeName = new ASN1ObjectIdentifier("2.5.4.19");
        postalAddress = new ASN1ObjectIdentifier("2.5.4.16");
        postalCode = new ASN1ObjectIdentifier("2.5.4.17");
        postOfficeBox = new ASN1ObjectIdentifier("2.5.4.18");
        preferredDeliveryMethod = new ASN1ObjectIdentifier("2.5.4.28");
        registeredAddress = new ASN1ObjectIdentifier("2.5.4.26");
        roleOccupant = new ASN1ObjectIdentifier("2.5.4.33");
        searchGuide = new ASN1ObjectIdentifier("2.5.4.14");
        seeAlso = new ASN1ObjectIdentifier("2.5.4.34");
        serialNumber = new ASN1ObjectIdentifier("2.5.4.5");
        sn = new ASN1ObjectIdentifier("2.5.4.4");
        st = new ASN1ObjectIdentifier("2.5.4.8");
        street = new ASN1ObjectIdentifier("2.5.4.9");
        telephoneNumber = new ASN1ObjectIdentifier("2.5.4.20");
        teletexTerminalIdentifier = new ASN1ObjectIdentifier("2.5.4.22");
        telexNumber = new ASN1ObjectIdentifier("2.5.4.21");
        title = new ASN1ObjectIdentifier("2.5.4.12");
        uid = new ASN1ObjectIdentifier("0.9.2342.19200300.100.1.1");
        uniqueMember = new ASN1ObjectIdentifier("2.5.4.50");
        userPassword = new ASN1ObjectIdentifier("2.5.4.35");
        x121Address = new ASN1ObjectIdentifier("2.5.4.24");
        x500UniqueIdentifier = new ASN1ObjectIdentifier("2.5.4.45");
        DefaultSymbols = new Hashtable();
        DefaultLookUp = new Hashtable();
        RFC4519Style.DefaultSymbols.put(RFC4519Style.businessCategory, "businessCategory");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.c, "c");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.cn, "cn");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.dc, "dc");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.description, "description");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.destinationIndicator, "destinationIndicator");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.distinguishedName, "distinguishedName");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.dnQualifier, "dnQualifier");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.enhancedSearchGuide, "enhancedSearchGuide");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.facsimileTelephoneNumber, "facsimileTelephoneNumber");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.generationQualifier, "generationQualifier");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.givenName, "givenName");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.houseIdentifier, "houseIdentifier");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.initials, "initials");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.internationalISDNNumber, "internationalISDNNumber");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.l, "l");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.member, "member");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.name, "name");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.o, "o");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.ou, "ou");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.owner, "owner");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.physicalDeliveryOfficeName, "physicalDeliveryOfficeName");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.postalAddress, "postalAddress");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.postalCode, "postalCode");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.postOfficeBox, "postOfficeBox");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.preferredDeliveryMethod, "preferredDeliveryMethod");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.registeredAddress, "registeredAddress");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.roleOccupant, "roleOccupant");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.searchGuide, "searchGuide");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.seeAlso, "seeAlso");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.serialNumber, "serialNumber");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.sn, "sn");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.st, "st");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.street, "street");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.telephoneNumber, "telephoneNumber");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.teletexTerminalIdentifier, "teletexTerminalIdentifier");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.telexNumber, "telexNumber");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.title, "title");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.uid, "uid");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.uniqueMember, "uniqueMember");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.userPassword, "userPassword");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.x121Address, "x121Address");
        RFC4519Style.DefaultSymbols.put(RFC4519Style.x500UniqueIdentifier, "x500UniqueIdentifier");
        RFC4519Style.DefaultLookUp.put("businesscategory", RFC4519Style.businessCategory);
        RFC4519Style.DefaultLookUp.put("c", RFC4519Style.c);
        RFC4519Style.DefaultLookUp.put("cn", RFC4519Style.cn);
        RFC4519Style.DefaultLookUp.put("dc", RFC4519Style.dc);
        RFC4519Style.DefaultLookUp.put("description", RFC4519Style.description);
        RFC4519Style.DefaultLookUp.put("destinationindicator", RFC4519Style.destinationIndicator);
        RFC4519Style.DefaultLookUp.put("distinguishedname", RFC4519Style.distinguishedName);
        RFC4519Style.DefaultLookUp.put("dnqualifier", RFC4519Style.dnQualifier);
        RFC4519Style.DefaultLookUp.put("enhancedsearchguide", RFC4519Style.enhancedSearchGuide);
        RFC4519Style.DefaultLookUp.put("facsimiletelephonenumber", RFC4519Style.facsimileTelephoneNumber);
        RFC4519Style.DefaultLookUp.put("generationqualifier", RFC4519Style.generationQualifier);
        RFC4519Style.DefaultLookUp.put("givenname", RFC4519Style.givenName);
        RFC4519Style.DefaultLookUp.put("houseidentifier", RFC4519Style.houseIdentifier);
        RFC4519Style.DefaultLookUp.put("initials", RFC4519Style.initials);
        RFC4519Style.DefaultLookUp.put("internationalisdnnumber", RFC4519Style.internationalISDNNumber);
        RFC4519Style.DefaultLookUp.put("l", RFC4519Style.l);
        RFC4519Style.DefaultLookUp.put("member", RFC4519Style.member);
        RFC4519Style.DefaultLookUp.put("name", RFC4519Style.name);
        RFC4519Style.DefaultLookUp.put("o", RFC4519Style.o);
        RFC4519Style.DefaultLookUp.put("ou", RFC4519Style.ou);
        RFC4519Style.DefaultLookUp.put("owner", RFC4519Style.owner);
        RFC4519Style.DefaultLookUp.put("physicaldeliveryofficename", RFC4519Style.physicalDeliveryOfficeName);
        RFC4519Style.DefaultLookUp.put("postaladdress", RFC4519Style.postalAddress);
        RFC4519Style.DefaultLookUp.put("postalcode", RFC4519Style.postalCode);
        RFC4519Style.DefaultLookUp.put("postofficebox", RFC4519Style.postOfficeBox);
        RFC4519Style.DefaultLookUp.put("preferreddeliverymethod", RFC4519Style.preferredDeliveryMethod);
        RFC4519Style.DefaultLookUp.put("registeredaddress", RFC4519Style.registeredAddress);
        RFC4519Style.DefaultLookUp.put("roleoccupant", RFC4519Style.roleOccupant);
        RFC4519Style.DefaultLookUp.put("searchguide", RFC4519Style.searchGuide);
        RFC4519Style.DefaultLookUp.put("seealso", RFC4519Style.seeAlso);
        RFC4519Style.DefaultLookUp.put("serialnumber", RFC4519Style.serialNumber);
        RFC4519Style.DefaultLookUp.put("sn", RFC4519Style.sn);
        RFC4519Style.DefaultLookUp.put("st", RFC4519Style.st);
        RFC4519Style.DefaultLookUp.put("street", RFC4519Style.street);
        RFC4519Style.DefaultLookUp.put("telephonenumber", RFC4519Style.telephoneNumber);
        RFC4519Style.DefaultLookUp.put("teletexterminalidentifier", RFC4519Style.teletexTerminalIdentifier);
        RFC4519Style.DefaultLookUp.put("telexnumber", RFC4519Style.telexNumber);
        RFC4519Style.DefaultLookUp.put("title", RFC4519Style.title);
        RFC4519Style.DefaultLookUp.put("uid", RFC4519Style.uid);
        RFC4519Style.DefaultLookUp.put("uniquemember", RFC4519Style.uniqueMember);
        RFC4519Style.DefaultLookUp.put("userpassword", RFC4519Style.userPassword);
        RFC4519Style.DefaultLookUp.put("x121address", RFC4519Style.x121Address);
        RFC4519Style.DefaultLookUp.put("x500uniqueidentifier", RFC4519Style.x500UniqueIdentifier);
    }
}
