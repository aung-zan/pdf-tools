// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.util.Arrays;
import java.math.BigInteger;

public class DERObjectIdentifier extends ASN1Primitive
{
    String identifier;
    private byte[] body;
    private static final long LONG_LIMIT = 72057594037927808L;
    private static ASN1ObjectIdentifier[][] cache;
    
    public static ASN1ObjectIdentifier getInstance(final Object o) {
        if (o == null || o instanceof ASN1ObjectIdentifier) {
            return (ASN1ObjectIdentifier)o;
        }
        if (o instanceof DERObjectIdentifier) {
            return new ASN1ObjectIdentifier(((DERObjectIdentifier)o).getId());
        }
        if (o instanceof ASN1Encodable && ((ASN1Encodable)o).toASN1Primitive() instanceof ASN1ObjectIdentifier) {
            return (ASN1ObjectIdentifier)((ASN1Encodable)o).toASN1Primitive();
        }
        if (o instanceof byte[]) {
            return fromOctetString((byte[])o);
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + o.getClass().getName());
    }
    
    public static ASN1ObjectIdentifier getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        final ASN1Primitive object = asn1TaggedObject.getObject();
        if (b || object instanceof DERObjectIdentifier) {
            return getInstance(object);
        }
        return fromOctetString(ASN1OctetString.getInstance(asn1TaggedObject.getObject()).getOctets());
    }
    
    DERObjectIdentifier(final byte[] array) {
        final StringBuffer sb = new StringBuffer();
        long val = 0L;
        BigInteger bigInteger = null;
        int n = 1;
        for (int i = 0; i != array.length; ++i) {
            final int n2 = array[i] & 0xFF;
            if (val <= 72057594037927808L) {
                long lng = val + (n2 & 0x7F);
                if ((n2 & 0x80) == 0x0) {
                    if (n != 0) {
                        if (lng < 40L) {
                            sb.append('0');
                        }
                        else if (lng < 80L) {
                            sb.append('1');
                            lng -= 40L;
                        }
                        else {
                            sb.append('2');
                            lng -= 80L;
                        }
                        n = 0;
                    }
                    sb.append('.');
                    sb.append(lng);
                    val = 0L;
                }
                else {
                    val = lng << 7;
                }
            }
            else {
                if (bigInteger == null) {
                    bigInteger = BigInteger.valueOf(val);
                }
                BigInteger obj = bigInteger.or(BigInteger.valueOf(n2 & 0x7F));
                if ((n2 & 0x80) == 0x0) {
                    if (n != 0) {
                        sb.append('2');
                        obj = obj.subtract(BigInteger.valueOf(80L));
                        n = 0;
                    }
                    sb.append('.');
                    sb.append(obj);
                    bigInteger = null;
                    val = 0L;
                }
                else {
                    bigInteger = obj.shiftLeft(7);
                }
            }
        }
        this.identifier = sb.toString();
        this.body = Arrays.clone(array);
    }
    
    public DERObjectIdentifier(final String s) {
        if (s == null) {
            throw new IllegalArgumentException("'identifier' cannot be null");
        }
        if (!isValidIdentifier(s)) {
            throw new IllegalArgumentException("string " + s + " not an OID");
        }
        this.identifier = s;
    }
    
    DERObjectIdentifier(final DERObjectIdentifier derObjectIdentifier, final String s) {
        if (!isValidBranchID(s, 0)) {
            throw new IllegalArgumentException("string " + s + " not a valid OID branch");
        }
        this.identifier = derObjectIdentifier.getId() + "." + s;
    }
    
    public String getId() {
        return this.identifier;
    }
    
    private void writeField(final ByteArrayOutputStream byteArrayOutputStream, long n) {
        final byte[] b = new byte[9];
        int off = 8;
        b[off] = (byte)((int)n & 0x7F);
        while (n >= 128L) {
            n >>= 7;
            b[--off] = (byte)(((int)n & 0x7F) | 0x80);
        }
        byteArrayOutputStream.write(b, off, 9 - off);
    }
    
    private void writeField(final ByteArrayOutputStream byteArrayOutputStream, final BigInteger bigInteger) {
        final int n = (bigInteger.bitLength() + 6) / 7;
        if (n == 0) {
            byteArrayOutputStream.write(0);
        }
        else {
            BigInteger shiftRight = bigInteger;
            final byte[] b = new byte[n];
            for (int i = n - 1; i >= 0; --i) {
                b[i] = (byte)((shiftRight.intValue() & 0x7F) | 0x80);
                shiftRight = shiftRight.shiftRight(7);
            }
            final byte[] array = b;
            final int n2 = n - 1;
            array[n2] &= 0x7F;
            byteArrayOutputStream.write(b, 0, b.length);
        }
    }
    
    private void doOutput(final ByteArrayOutputStream byteArrayOutputStream) {
        final OIDTokenizer oidTokenizer = new OIDTokenizer(this.identifier);
        final int n = Integer.parseInt(oidTokenizer.nextToken()) * 40;
        final String nextToken = oidTokenizer.nextToken();
        if (nextToken.length() <= 18) {
            this.writeField(byteArrayOutputStream, n + Long.parseLong(nextToken));
        }
        else {
            this.writeField(byteArrayOutputStream, new BigInteger(nextToken).add(BigInteger.valueOf(n)));
        }
        while (oidTokenizer.hasMoreTokens()) {
            final String nextToken2 = oidTokenizer.nextToken();
            if (nextToken2.length() <= 18) {
                this.writeField(byteArrayOutputStream, Long.parseLong(nextToken2));
            }
            else {
                this.writeField(byteArrayOutputStream, new BigInteger(nextToken2));
            }
        }
    }
    
    protected synchronized byte[] getBody() {
        if (this.body == null) {
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            this.doOutput(byteArrayOutputStream);
            this.body = byteArrayOutputStream.toByteArray();
        }
        return this.body;
    }
    
    @Override
    boolean isConstructed() {
        return false;
    }
    
    @Override
    int encodedLength() throws IOException {
        final int length = this.getBody().length;
        return 1 + StreamUtil.calculateBodyLength(length) + length;
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream) throws IOException {
        final byte[] body = this.getBody();
        asn1OutputStream.write(6);
        asn1OutputStream.writeLength(body.length);
        asn1OutputStream.write(body);
    }
    
    @Override
    public int hashCode() {
        return this.identifier.hashCode();
    }
    
    @Override
    boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        return asn1Primitive instanceof DERObjectIdentifier && this.identifier.equals(((DERObjectIdentifier)asn1Primitive).identifier);
    }
    
    @Override
    public String toString() {
        return this.getId();
    }
    
    private static boolean isValidBranchID(final String s, final int n) {
        boolean b = false;
        int length = s.length();
        while (--length >= n) {
            final char char1 = s.charAt(length);
            if ('0' <= char1 && char1 <= '9') {
                b = true;
            }
            else {
                if (char1 != '.') {
                    return false;
                }
                if (!b) {
                    return false;
                }
                b = false;
            }
        }
        return b;
    }
    
    private static boolean isValidIdentifier(final String s) {
        if (s.length() < 3 || s.charAt(1) != '.') {
            return false;
        }
        final char char1 = s.charAt(0);
        return char1 >= '0' && char1 <= '2' && isValidBranchID(s, 2);
    }
    
    static ASN1ObjectIdentifier fromOctetString(final byte[] array) {
        if (array.length < 3) {
            return new ASN1ObjectIdentifier(array);
        }
        final int n = array[array.length - 2] & 0xFF;
        final int n2 = array[array.length - 1] & 0x7F;
        ASN1ObjectIdentifier asn1ObjectIdentifier;
        synchronized (DERObjectIdentifier.cache) {
            ASN1ObjectIdentifier[] array2 = DERObjectIdentifier.cache[n];
            if (array2 == null) {
                final ASN1ObjectIdentifier[][] cache = DERObjectIdentifier.cache;
                final int n3 = n;
                final ASN1ObjectIdentifier[] array3 = new ASN1ObjectIdentifier[128];
                cache[n3] = array3;
                array2 = array3;
            }
            asn1ObjectIdentifier = array2[n2];
            if (asn1ObjectIdentifier == null) {
                return array2[n2] = new ASN1ObjectIdentifier(array);
            }
            if (Arrays.areEqual(array, asn1ObjectIdentifier.getBody())) {
                return asn1ObjectIdentifier;
            }
            final int n4 = n + 1 & 0xFF;
            ASN1ObjectIdentifier[] array4 = DERObjectIdentifier.cache[n4];
            if (array4 == null) {
                final ASN1ObjectIdentifier[][] cache2 = DERObjectIdentifier.cache;
                final int n5 = n4;
                final ASN1ObjectIdentifier[] array5 = new ASN1ObjectIdentifier[128];
                cache2[n5] = array5;
                array4 = array5;
            }
            asn1ObjectIdentifier = array4[n2];
            if (asn1ObjectIdentifier == null) {
                return array4[n2] = new ASN1ObjectIdentifier(array);
            }
            if (Arrays.areEqual(array, asn1ObjectIdentifier.getBody())) {
                return asn1ObjectIdentifier;
            }
            final int n6 = n2 + 1 & 0x7F;
            asn1ObjectIdentifier = array4[n6];
            if (asn1ObjectIdentifier == null) {
                return array4[n6] = new ASN1ObjectIdentifier(array);
            }
        }
        if (Arrays.areEqual(array, asn1ObjectIdentifier.getBody())) {
            return asn1ObjectIdentifier;
        }
        return new ASN1ObjectIdentifier(array);
    }
    
    static {
        DERObjectIdentifier.cache = new ASN1ObjectIdentifier[256][];
    }
}
