// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;
import org.bouncycastle.util.Arrays;

public class DERBoolean extends ASN1Primitive
{
    private static final byte[] TRUE_VALUE;
    private static final byte[] FALSE_VALUE;
    private byte[] value;
    public static final ASN1Boolean FALSE;
    public static final ASN1Boolean TRUE;
    
    public static ASN1Boolean getInstance(final Object o) {
        if (o == null || o instanceof ASN1Boolean) {
            return (ASN1Boolean)o;
        }
        if (o instanceof DERBoolean) {
            return ((DERBoolean)o).isTrue() ? DERBoolean.TRUE : DERBoolean.FALSE;
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + o.getClass().getName());
    }
    
    public static ASN1Boolean getInstance(final boolean b) {
        return b ? DERBoolean.TRUE : DERBoolean.FALSE;
    }
    
    public static ASN1Boolean getInstance(final int n) {
        return (n != 0) ? DERBoolean.TRUE : DERBoolean.FALSE;
    }
    
    public static ASN1Boolean getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        final ASN1Primitive object = asn1TaggedObject.getObject();
        if (b || object instanceof DERBoolean) {
            return getInstance(object);
        }
        return fromOctetString(((ASN1OctetString)object).getOctets());
    }
    
    DERBoolean(final byte[] array) {
        if (array.length != 1) {
            throw new IllegalArgumentException("byte value should have 1 byte in it");
        }
        if (array[0] == 0) {
            this.value = DERBoolean.FALSE_VALUE;
        }
        else if (array[0] == 255) {
            this.value = DERBoolean.TRUE_VALUE;
        }
        else {
            this.value = Arrays.clone(array);
        }
    }
    
    @Deprecated
    public DERBoolean(final boolean b) {
        this.value = (b ? DERBoolean.TRUE_VALUE : DERBoolean.FALSE_VALUE);
    }
    
    public boolean isTrue() {
        return this.value[0] != 0;
    }
    
    @Override
    boolean isConstructed() {
        return false;
    }
    
    @Override
    int encodedLength() {
        return 3;
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream) throws IOException {
        asn1OutputStream.writeEncoded(1, this.value);
    }
    
    protected boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        return asn1Primitive != null && asn1Primitive instanceof DERBoolean && this.value[0] == ((DERBoolean)asn1Primitive).value[0];
    }
    
    @Override
    public int hashCode() {
        return this.value[0];
    }
    
    @Override
    public String toString() {
        return (this.value[0] != 0) ? "TRUE" : "FALSE";
    }
    
    static ASN1Boolean fromOctetString(final byte[] array) {
        if (array.length != 1) {
            throw new IllegalArgumentException("byte value should have 1 byte in it");
        }
        if (array[0] == 0) {
            return DERBoolean.FALSE;
        }
        if (array[0] == 255) {
            return DERBoolean.TRUE;
        }
        return new ASN1Boolean(array);
    }
    
    static {
        TRUE_VALUE = new byte[] { -1 };
        FALSE_VALUE = new byte[] { 0 };
        FALSE = new ASN1Boolean(false);
        TRUE = new ASN1Boolean(true);
    }
}
