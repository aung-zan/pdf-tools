// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.eac;

import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1ParsingException;
import java.util.Enumeration;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERApplicationSpecific;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Object;

public class CVCertificateRequest extends ASN1Object
{
    private CertificateBody certificateBody;
    private byte[] innerSignature;
    private byte[] outerSignature;
    private int valid;
    private static int bodyValid;
    private static int signValid;
    ASN1ObjectIdentifier signOid;
    ASN1ObjectIdentifier keyOid;
    public static byte[] ZeroArray;
    String strCertificateHolderReference;
    byte[] encodedAuthorityReference;
    int ProfileId;
    byte[] certificate;
    protected String overSignerReference;
    byte[] encoded;
    PublicKeyDataObject iso7816PubKey;
    
    private CVCertificateRequest(final DERApplicationSpecific derApplicationSpecific) throws IOException {
        this.innerSignature = null;
        this.outerSignature = null;
        this.signOid = null;
        this.keyOid = null;
        this.certificate = null;
        this.overSignerReference = null;
        this.iso7816PubKey = null;
        if (derApplicationSpecific.getApplicationTag() == 103) {
            final ASN1Sequence instance = ASN1Sequence.getInstance(derApplicationSpecific.getObject(16));
            this.initCertBody(DERApplicationSpecific.getInstance(instance.getObjectAt(0)));
            this.outerSignature = DERApplicationSpecific.getInstance(instance.getObjectAt(instance.size() - 1)).getContents();
        }
        else {
            this.initCertBody(derApplicationSpecific);
        }
    }
    
    private void initCertBody(final DERApplicationSpecific derApplicationSpecific) throws IOException {
        if (derApplicationSpecific.getApplicationTag() == 33) {
            final Enumeration objects = ASN1Sequence.getInstance(derApplicationSpecific.getObject(16)).getObjects();
            while (objects.hasMoreElements()) {
                final DERApplicationSpecific instance = DERApplicationSpecific.getInstance(objects.nextElement());
                switch (instance.getApplicationTag()) {
                    case 78: {
                        this.certificateBody = CertificateBody.getInstance(instance);
                        this.valid |= CVCertificateRequest.bodyValid;
                        continue;
                    }
                    case 55: {
                        this.innerSignature = instance.getContents();
                        this.valid |= CVCertificateRequest.signValid;
                        continue;
                    }
                    default: {
                        throw new IOException("Invalid tag, not an CV Certificate Request element:" + instance.getApplicationTag());
                    }
                }
            }
            return;
        }
        throw new IOException("not a CARDHOLDER_CERTIFICATE in request:" + derApplicationSpecific.getApplicationTag());
    }
    
    public static CVCertificateRequest getInstance(final Object o) {
        if (o instanceof CVCertificateRequest) {
            return (CVCertificateRequest)o;
        }
        if (o != null) {
            try {
                return new CVCertificateRequest(DERApplicationSpecific.getInstance(o));
            }
            catch (IOException ex) {
                throw new ASN1ParsingException("unable to parse data: " + ex.getMessage(), ex);
            }
        }
        return null;
    }
    
    public CertificateBody getCertificateBody() {
        return this.certificateBody;
    }
    
    public PublicKeyDataObject getPublicKey() {
        return this.certificateBody.getPublicKey();
    }
    
    public byte[] getInnerSignature() {
        return this.innerSignature;
    }
    
    public byte[] getOuterSignature() {
        return this.outerSignature;
    }
    
    public boolean hasOuterSignature() {
        return this.outerSignature != null;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        asn1EncodableVector.add(this.certificateBody);
        try {
            asn1EncodableVector.add(new DERApplicationSpecific(false, 55, new DEROctetString(this.innerSignature)));
        }
        catch (IOException ex) {
            throw new IllegalStateException("unable to convert signature!");
        }
        return new DERApplicationSpecific(33, asn1EncodableVector);
    }
    
    static {
        CVCertificateRequest.bodyValid = 1;
        CVCertificateRequest.signValid = 2;
        CVCertificateRequest.ZeroArray = new byte[] { 0 };
    }
}
