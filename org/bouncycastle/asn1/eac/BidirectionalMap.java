// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.eac;

import java.util.Hashtable;

public class BidirectionalMap extends Hashtable
{
    private static final long serialVersionUID = -7457289971962812909L;
    Hashtable reverseMap;
    
    public BidirectionalMap() {
        this.reverseMap = new Hashtable();
    }
    
    public Object getReverse(final Object key) {
        return this.reverseMap.get(key);
    }
    
    @Override
    public Object put(final Object o, final Object o2) {
        this.reverseMap.put(o2, o);
        return super.put(o, o2);
    }
}
