// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.ua;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPoint;
import java.util.Random;
import org.bouncycastle.math.ec.ECConstants;
import java.math.BigInteger;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.asn1.x9.X9IntegerConverter;

public abstract class DSTU4145PointEncoder
{
    private static X9IntegerConverter converter;
    
    private static BigInteger trace(final ECFieldElement ecFieldElement) {
        ECFieldElement add = ecFieldElement;
        for (int i = 0; i < ecFieldElement.getFieldSize() - 1; ++i) {
            add = add.square().add(ecFieldElement);
        }
        return add.toBigInteger();
    }
    
    private static ECFieldElement solveQuadradicEquation(final ECFieldElement ecFieldElement) {
        final ECFieldElement.F2m f2m = (ECFieldElement.F2m)ecFieldElement;
        final ECFieldElement.F2m f2m2 = new ECFieldElement.F2m(f2m.getM(), f2m.getK1(), f2m.getK2(), f2m.getK3(), ECConstants.ZERO);
        if (ecFieldElement.toBigInteger().equals(ECConstants.ZERO)) {
            return f2m2;
        }
        final Random rnd = new Random();
        final int m = f2m.getM();
        ECFieldElement add;
        do {
            final ECFieldElement.F2m f2m3 = new ECFieldElement.F2m(f2m.getM(), f2m.getK1(), f2m.getK2(), f2m.getK3(), new BigInteger(m, rnd));
            add = f2m2;
            ECFieldElement add2 = ecFieldElement;
            for (int i = 1; i <= m - 1; ++i) {
                final ECFieldElement square = add2.square();
                add = add.square().add(square.multiply(f2m3));
                add2 = square.add(ecFieldElement);
            }
            if (!add2.toBigInteger().equals(ECConstants.ZERO)) {
                return null;
            }
        } while (add.square().add(add).toBigInteger().equals(ECConstants.ZERO));
        return add;
    }
    
    public static byte[] encodePoint(final ECPoint ecPoint) {
        final byte[] integerToBytes = DSTU4145PointEncoder.converter.integerToBytes(ecPoint.getX().toBigInteger(), DSTU4145PointEncoder.converter.getByteLength(ecPoint.getX()));
        if (!ecPoint.getX().toBigInteger().equals(ECConstants.ZERO)) {
            if (trace(ecPoint.getY().multiply(ecPoint.getX().invert())).equals(ECConstants.ONE)) {
                final byte[] array = integerToBytes;
                final int n = integerToBytes.length - 1;
                array[n] |= 0x1;
            }
            else {
                final byte[] array2 = integerToBytes;
                final int n2 = integerToBytes.length - 1;
                array2[n2] &= (byte)254;
            }
        }
        return integerToBytes;
    }
    
    public static ECPoint decodePoint(final ECCurve ecCurve, byte[] array) {
        final BigInteger value = BigInteger.valueOf(array[array.length - 1] & 0x1);
        if (!trace(ecCurve.fromBigInteger(new BigInteger(1, array))).equals(ecCurve.getA().toBigInteger())) {
            final byte[] clone;
            array = (clone = Arrays.clone(array));
            final int n = array.length - 1;
            clone[n] ^= 0x1;
        }
        final ECCurve.F2m f2m = (ECCurve.F2m)ecCurve;
        final ECFieldElement fromBigInteger = ecCurve.fromBigInteger(new BigInteger(1, array));
        ECFieldElement ecFieldElement;
        if (fromBigInteger.toBigInteger().equals(ECConstants.ZERO)) {
            ecFieldElement = ecCurve.getB();
            for (int i = 0; i < f2m.getM() - 1; ++i) {
                ecFieldElement = ecFieldElement.square();
            }
        }
        else {
            ECFieldElement ecFieldElement2 = solveQuadradicEquation(fromBigInteger.add(ecCurve.getA()).add(ecCurve.getB().multiply(fromBigInteger.square().invert())));
            if (ecFieldElement2 == null) {
                throw new RuntimeException("Invalid point compression");
            }
            if (!trace(ecFieldElement2).equals(value)) {
                ecFieldElement2 = ecFieldElement2.add(ecCurve.fromBigInteger(ECConstants.ONE));
            }
            ecFieldElement = fromBigInteger.multiply(ecFieldElement2);
        }
        return new ECPoint.F2m(ecCurve, fromBigInteger, ecFieldElement);
    }
    
    static {
        DSTU4145PointEncoder.converter = new X9IntegerConverter();
    }
}
