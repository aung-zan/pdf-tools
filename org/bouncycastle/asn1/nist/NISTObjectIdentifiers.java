// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.nist;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public interface NISTObjectIdentifiers
{
    public static final ASN1ObjectIdentifier nistAlgorithm = new ASN1ObjectIdentifier("2.16.840.1.101.3.4");
    public static final ASN1ObjectIdentifier hashAlgs = NISTObjectIdentifiers.nistAlgorithm.branch("2");
    public static final ASN1ObjectIdentifier id_sha256 = NISTObjectIdentifiers.hashAlgs.branch("1");
    public static final ASN1ObjectIdentifier id_sha384 = NISTObjectIdentifiers.hashAlgs.branch("2");
    public static final ASN1ObjectIdentifier id_sha512 = NISTObjectIdentifiers.hashAlgs.branch("3");
    public static final ASN1ObjectIdentifier id_sha224 = NISTObjectIdentifiers.hashAlgs.branch("4");
    public static final ASN1ObjectIdentifier id_sha512_224 = NISTObjectIdentifiers.hashAlgs.branch("5");
    public static final ASN1ObjectIdentifier id_sha512_256 = NISTObjectIdentifiers.hashAlgs.branch("6");
    public static final ASN1ObjectIdentifier aes = NISTObjectIdentifiers.nistAlgorithm.branch("1");
    public static final ASN1ObjectIdentifier id_aes128_ECB = NISTObjectIdentifiers.aes.branch("1");
    public static final ASN1ObjectIdentifier id_aes128_CBC = NISTObjectIdentifiers.aes.branch("2");
    public static final ASN1ObjectIdentifier id_aes128_OFB = NISTObjectIdentifiers.aes.branch("3");
    public static final ASN1ObjectIdentifier id_aes128_CFB = NISTObjectIdentifiers.aes.branch("4");
    public static final ASN1ObjectIdentifier id_aes128_wrap = NISTObjectIdentifiers.aes.branch("5");
    public static final ASN1ObjectIdentifier id_aes128_GCM = NISTObjectIdentifiers.aes.branch("6");
    public static final ASN1ObjectIdentifier id_aes128_CCM = NISTObjectIdentifiers.aes.branch("7");
    public static final ASN1ObjectIdentifier id_aes192_ECB = NISTObjectIdentifiers.aes.branch("21");
    public static final ASN1ObjectIdentifier id_aes192_CBC = NISTObjectIdentifiers.aes.branch("22");
    public static final ASN1ObjectIdentifier id_aes192_OFB = NISTObjectIdentifiers.aes.branch("23");
    public static final ASN1ObjectIdentifier id_aes192_CFB = NISTObjectIdentifiers.aes.branch("24");
    public static final ASN1ObjectIdentifier id_aes192_wrap = NISTObjectIdentifiers.aes.branch("25");
    public static final ASN1ObjectIdentifier id_aes192_GCM = NISTObjectIdentifiers.aes.branch("26");
    public static final ASN1ObjectIdentifier id_aes192_CCM = NISTObjectIdentifiers.aes.branch("27");
    public static final ASN1ObjectIdentifier id_aes256_ECB = NISTObjectIdentifiers.aes.branch("41");
    public static final ASN1ObjectIdentifier id_aes256_CBC = NISTObjectIdentifiers.aes.branch("42");
    public static final ASN1ObjectIdentifier id_aes256_OFB = NISTObjectIdentifiers.aes.branch("43");
    public static final ASN1ObjectIdentifier id_aes256_CFB = NISTObjectIdentifiers.aes.branch("44");
    public static final ASN1ObjectIdentifier id_aes256_wrap = NISTObjectIdentifiers.aes.branch("45");
    public static final ASN1ObjectIdentifier id_aes256_GCM = NISTObjectIdentifiers.aes.branch("46");
    public static final ASN1ObjectIdentifier id_aes256_CCM = NISTObjectIdentifiers.aes.branch("47");
    public static final ASN1ObjectIdentifier id_dsa_with_sha2 = NISTObjectIdentifiers.nistAlgorithm.branch("3");
    public static final ASN1ObjectIdentifier dsa_with_sha224 = NISTObjectIdentifiers.id_dsa_with_sha2.branch("1");
    public static final ASN1ObjectIdentifier dsa_with_sha256 = NISTObjectIdentifiers.id_dsa_with_sha2.branch("2");
    public static final ASN1ObjectIdentifier dsa_with_sha384 = NISTObjectIdentifiers.id_dsa_with_sha2.branch("3");
    public static final ASN1ObjectIdentifier dsa_with_sha512 = NISTObjectIdentifiers.id_dsa_with_sha2.branch("4");
}
