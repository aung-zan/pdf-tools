// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.util.Date;

public class ASN1GeneralizedTime extends DERGeneralizedTime
{
    ASN1GeneralizedTime(final byte[] array) {
        super(array);
    }
    
    public ASN1GeneralizedTime(final Date date) {
        super(date);
    }
    
    public ASN1GeneralizedTime(final String s) {
        super(s);
    }
}
