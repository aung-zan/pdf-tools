// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.util.Enumeration;
import java.util.Vector;

public class ASN1EncodableVector
{
    Vector v;
    
    public ASN1EncodableVector() {
        this.v = new Vector();
    }
    
    public void add(final ASN1Encodable obj) {
        this.v.addElement(obj);
    }
    
    public void addAll(final ASN1EncodableVector asn1EncodableVector) {
        final Enumeration<Object> elements = asn1EncodableVector.v.elements();
        while (elements.hasMoreElements()) {
            this.v.addElement(elements.nextElement());
        }
    }
    
    public ASN1Encodable get(final int index) {
        return this.v.elementAt(index);
    }
    
    public int size() {
        return this.v.size();
    }
}
