// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

public interface ASN1Encodable
{
    ASN1Primitive toASN1Primitive();
}
