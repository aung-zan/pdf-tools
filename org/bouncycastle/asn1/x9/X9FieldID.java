// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x9;

import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Integer;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Object;

public class X9FieldID extends ASN1Object implements X9ObjectIdentifiers
{
    private ASN1ObjectIdentifier id;
    private ASN1Primitive parameters;
    
    public X9FieldID(final BigInteger bigInteger) {
        this.id = X9FieldID.prime_field;
        this.parameters = new ASN1Integer(bigInteger);
    }
    
    public X9FieldID(final int n, final int n2, final int n3, final int n4) {
        this.id = X9FieldID.characteristic_two_field;
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        asn1EncodableVector.add(new ASN1Integer(n));
        if (n3 == 0) {
            asn1EncodableVector.add(X9FieldID.tpBasis);
            asn1EncodableVector.add(new ASN1Integer(n2));
        }
        else {
            asn1EncodableVector.add(X9FieldID.ppBasis);
            final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
            asn1EncodableVector2.add(new ASN1Integer(n2));
            asn1EncodableVector2.add(new ASN1Integer(n3));
            asn1EncodableVector2.add(new ASN1Integer(n4));
            asn1EncodableVector.add(new DERSequence(asn1EncodableVector2));
        }
        this.parameters = new DERSequence(asn1EncodableVector);
    }
    
    public X9FieldID(final ASN1Sequence asn1Sequence) {
        this.id = (ASN1ObjectIdentifier)asn1Sequence.getObjectAt(0);
        this.parameters = (ASN1Primitive)asn1Sequence.getObjectAt(1);
    }
    
    public ASN1ObjectIdentifier getIdentifier() {
        return this.id;
    }
    
    public ASN1Primitive getParameters() {
        return this.parameters;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        asn1EncodableVector.add(this.id);
        asn1EncodableVector.add(this.parameters);
        return new DERSequence(asn1EncodableVector);
    }
}
