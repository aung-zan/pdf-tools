// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x9;

import java.util.Vector;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.nist.NISTNamedCurves;
import org.bouncycastle.asn1.teletrust.TeleTrusTNamedCurves;
import org.bouncycastle.asn1.sec.SECNamedCurves;

public class ECNamedCurveTable
{
    public static X9ECParameters getByName(final String s) {
        X9ECParameters x9ECParameters = X962NamedCurves.getByName(s);
        if (x9ECParameters == null) {
            x9ECParameters = SECNamedCurves.getByName(s);
        }
        if (x9ECParameters == null) {
            x9ECParameters = TeleTrusTNamedCurves.getByName(s);
        }
        if (x9ECParameters == null) {
            x9ECParameters = NISTNamedCurves.getByName(s);
        }
        return x9ECParameters;
    }
    
    public static X9ECParameters getByOID(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        X9ECParameters x9ECParameters = X962NamedCurves.getByOID(asn1ObjectIdentifier);
        if (x9ECParameters == null) {
            x9ECParameters = SECNamedCurves.getByOID(asn1ObjectIdentifier);
        }
        if (x9ECParameters == null) {
            x9ECParameters = TeleTrusTNamedCurves.getByOID(asn1ObjectIdentifier);
        }
        return x9ECParameters;
    }
    
    public static Enumeration getNames() {
        final Vector vector = new Vector();
        addEnumeration(vector, X962NamedCurves.getNames());
        addEnumeration(vector, SECNamedCurves.getNames());
        addEnumeration(vector, NISTNamedCurves.getNames());
        addEnumeration(vector, TeleTrusTNamedCurves.getNames());
        return vector.elements();
    }
    
    private static void addEnumeration(final Vector vector, final Enumeration enumeration) {
        while (enumeration.hasMoreElements()) {
            vector.addElement(enumeration.nextElement());
        }
    }
}
