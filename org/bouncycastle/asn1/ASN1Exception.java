// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;

public class ASN1Exception extends IOException
{
    private Throwable cause;
    
    ASN1Exception(final String message) {
        super(message);
    }
    
    ASN1Exception(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
