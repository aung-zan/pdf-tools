// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.util.Date;

public class ASN1UTCTime extends DERUTCTime
{
    ASN1UTCTime(final byte[] array) {
        super(array);
    }
    
    public ASN1UTCTime(final Date date) {
        super(date);
    }
    
    public ASN1UTCTime(final String s) {
        super(s);
    }
}
