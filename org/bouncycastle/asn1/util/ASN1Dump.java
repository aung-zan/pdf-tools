// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.util;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.util.encoders.Hex;
import java.io.IOException;
import java.util.Enumeration;
import org.bouncycastle.asn1.DERExternal;
import org.bouncycastle.asn1.DEREnumerated;
import org.bouncycastle.asn1.DERApplicationSpecific;
import org.bouncycastle.asn1.BERApplicationSpecific;
import org.bouncycastle.asn1.DERGeneralizedTime;
import org.bouncycastle.asn1.DERUTCTime;
import org.bouncycastle.asn1.DERT61String;
import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.DERVisibleString;
import org.bouncycastle.asn1.DERPrintableString;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.DERBoolean;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.BERConstructedOctetString;
import org.bouncycastle.asn1.BEROctetString;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Primitive;

public class ASN1Dump
{
    private static final String TAB = "    ";
    private static final int SAMPLE_SIZE = 32;
    
    static void _dumpAsString(final String s, final boolean b, final ASN1Primitive asn1Primitive, final StringBuffer sb) {
        final String property = System.getProperty("line.separator");
        if (asn1Primitive instanceof ASN1Sequence) {
            final Enumeration objects = ((ASN1Sequence)asn1Primitive).getObjects();
            final String string = s + "    ";
            sb.append(s);
            if (asn1Primitive instanceof BERSequence) {
                sb.append("BER Sequence");
            }
            else if (asn1Primitive instanceof DERSequence) {
                sb.append("DER Sequence");
            }
            else {
                sb.append("Sequence");
            }
            sb.append(property);
            while (objects.hasMoreElements()) {
                final ASN1Primitive nextElement = objects.nextElement();
                if (nextElement == null || nextElement.equals(DERNull.INSTANCE)) {
                    sb.append(string);
                    sb.append("NULL");
                    sb.append(property);
                }
                else if (nextElement instanceof ASN1Primitive) {
                    _dumpAsString(string, b, nextElement, sb);
                }
                else {
                    _dumpAsString(string, b, nextElement.toASN1Primitive(), sb);
                }
            }
        }
        else if (asn1Primitive instanceof ASN1TaggedObject) {
            final String string2 = s + "    ";
            sb.append(s);
            if (asn1Primitive instanceof BERTaggedObject) {
                sb.append("BER Tagged [");
            }
            else {
                sb.append("Tagged [");
            }
            final ASN1TaggedObject asn1TaggedObject = (ASN1TaggedObject)asn1Primitive;
            sb.append(Integer.toString(asn1TaggedObject.getTagNo()));
            sb.append(']');
            if (!asn1TaggedObject.isExplicit()) {
                sb.append(" IMPLICIT ");
            }
            sb.append(property);
            if (asn1TaggedObject.isEmpty()) {
                sb.append(string2);
                sb.append("EMPTY");
                sb.append(property);
            }
            else {
                _dumpAsString(string2, b, asn1TaggedObject.getObject(), sb);
            }
        }
        else if (asn1Primitive instanceof ASN1Set) {
            final Enumeration objects2 = ((ASN1Set)asn1Primitive).getObjects();
            final String string3 = s + "    ";
            sb.append(s);
            if (asn1Primitive instanceof BERSet) {
                sb.append("BER Set");
            }
            else {
                sb.append("DER Set");
            }
            sb.append(property);
            while (objects2.hasMoreElements()) {
                final ASN1Primitive nextElement2 = objects2.nextElement();
                if (nextElement2 == null) {
                    sb.append(string3);
                    sb.append("NULL");
                    sb.append(property);
                }
                else if (nextElement2 instanceof ASN1Primitive) {
                    _dumpAsString(string3, b, nextElement2, sb);
                }
                else {
                    _dumpAsString(string3, b, nextElement2.toASN1Primitive(), sb);
                }
            }
        }
        else if (asn1Primitive instanceof ASN1OctetString) {
            final ASN1OctetString asn1OctetString = (ASN1OctetString)asn1Primitive;
            if (asn1Primitive instanceof BEROctetString || asn1Primitive instanceof BERConstructedOctetString) {
                sb.append(s + "BER Constructed Octet String" + "[" + asn1OctetString.getOctets().length + "] ");
            }
            else {
                sb.append(s + "DER Octet String" + "[" + asn1OctetString.getOctets().length + "] ");
            }
            if (b) {
                sb.append(dumpBinaryDataAsString(s, asn1OctetString.getOctets()));
            }
            else {
                sb.append(property);
            }
        }
        else if (asn1Primitive instanceof ASN1ObjectIdentifier) {
            sb.append(s + "ObjectIdentifier(" + ((ASN1ObjectIdentifier)asn1Primitive).getId() + ")" + property);
        }
        else if (asn1Primitive instanceof DERBoolean) {
            sb.append(s + "Boolean(" + ((DERBoolean)asn1Primitive).isTrue() + ")" + property);
        }
        else if (asn1Primitive instanceof ASN1Integer) {
            sb.append(s + "Integer(" + ((ASN1Integer)asn1Primitive).getValue() + ")" + property);
        }
        else if (asn1Primitive instanceof DERBitString) {
            final DERBitString derBitString = (DERBitString)asn1Primitive;
            sb.append(s + "DER Bit String" + "[" + derBitString.getBytes().length + ", " + derBitString.getPadBits() + "] ");
            if (b) {
                sb.append(dumpBinaryDataAsString(s, derBitString.getBytes()));
            }
            else {
                sb.append(property);
            }
        }
        else if (asn1Primitive instanceof DERIA5String) {
            sb.append(s + "IA5String(" + ((DERIA5String)asn1Primitive).getString() + ") " + property);
        }
        else if (asn1Primitive instanceof DERUTF8String) {
            sb.append(s + "UTF8String(" + ((DERUTF8String)asn1Primitive).getString() + ") " + property);
        }
        else if (asn1Primitive instanceof DERPrintableString) {
            sb.append(s + "PrintableString(" + ((DERPrintableString)asn1Primitive).getString() + ") " + property);
        }
        else if (asn1Primitive instanceof DERVisibleString) {
            sb.append(s + "VisibleString(" + ((DERVisibleString)asn1Primitive).getString() + ") " + property);
        }
        else if (asn1Primitive instanceof DERBMPString) {
            sb.append(s + "BMPString(" + ((DERBMPString)asn1Primitive).getString() + ") " + property);
        }
        else if (asn1Primitive instanceof DERT61String) {
            sb.append(s + "T61String(" + ((DERT61String)asn1Primitive).getString() + ") " + property);
        }
        else if (asn1Primitive instanceof DERUTCTime) {
            sb.append(s + "UTCTime(" + ((DERUTCTime)asn1Primitive).getTime() + ") " + property);
        }
        else if (asn1Primitive instanceof DERGeneralizedTime) {
            sb.append(s + "GeneralizedTime(" + ((DERGeneralizedTime)asn1Primitive).getTime() + ") " + property);
        }
        else if (asn1Primitive instanceof BERApplicationSpecific) {
            sb.append(outputApplicationSpecific("BER", s, b, asn1Primitive, property));
        }
        else if (asn1Primitive instanceof DERApplicationSpecific) {
            sb.append(outputApplicationSpecific("DER", s, b, asn1Primitive, property));
        }
        else if (asn1Primitive instanceof DEREnumerated) {
            sb.append(s + "DER Enumerated(" + ((DEREnumerated)asn1Primitive).getValue() + ")" + property);
        }
        else if (asn1Primitive instanceof DERExternal) {
            final DERExternal derExternal = (DERExternal)asn1Primitive;
            sb.append(s + "External " + property);
            final String string4 = s + "    ";
            if (derExternal.getDirectReference() != null) {
                sb.append(string4 + "Direct Reference: " + derExternal.getDirectReference().getId() + property);
            }
            if (derExternal.getIndirectReference() != null) {
                sb.append(string4 + "Indirect Reference: " + derExternal.getIndirectReference().toString() + property);
            }
            if (derExternal.getDataValueDescriptor() != null) {
                _dumpAsString(string4, b, derExternal.getDataValueDescriptor(), sb);
            }
            sb.append(string4 + "Encoding: " + derExternal.getEncoding() + property);
            _dumpAsString(string4, b, derExternal.getExternalContent(), sb);
        }
        else {
            sb.append(s + asn1Primitive.toString() + property);
        }
    }
    
    private static String outputApplicationSpecific(final String s, final String str, final boolean b, final ASN1Primitive asn1Primitive, final String s2) {
        final DERApplicationSpecific derApplicationSpecific = (DERApplicationSpecific)asn1Primitive;
        final StringBuffer sb = new StringBuffer();
        if (derApplicationSpecific.isConstructed()) {
            try {
                final ASN1Sequence instance = ASN1Sequence.getInstance(derApplicationSpecific.getObject(16));
                sb.append(str + s + " ApplicationSpecific[" + derApplicationSpecific.getApplicationTag() + "]" + s2);
                final Enumeration objects = instance.getObjects();
                while (objects.hasMoreElements()) {
                    _dumpAsString(str + "    ", b, objects.nextElement(), sb);
                }
            }
            catch (IOException obj) {
                sb.append(obj);
            }
            return sb.toString();
        }
        return str + s + " ApplicationSpecific[" + derApplicationSpecific.getApplicationTag() + "] (" + new String(Hex.encode(derApplicationSpecific.getContents())) + ")" + s2;
    }
    
    public static String dumpAsString(final Object o) {
        return dumpAsString(o, false);
    }
    
    public static String dumpAsString(final Object o, final boolean b) {
        final StringBuffer sb = new StringBuffer();
        if (o instanceof ASN1Primitive) {
            _dumpAsString("", b, (ASN1Primitive)o, sb);
        }
        else {
            if (!(o instanceof ASN1Encodable)) {
                return "unknown object type " + o.toString();
            }
            _dumpAsString("", b, ((ASN1Encodable)o).toASN1Primitive(), sb);
        }
        return sb.toString();
    }
    
    private static String dumpBinaryDataAsString(String string, final byte[] array) {
        final String property = System.getProperty("line.separator");
        final StringBuffer sb = new StringBuffer();
        string += "    ";
        sb.append(property);
        for (int i = 0; i < array.length; i += 32) {
            if (array.length - i > 32) {
                sb.append(string);
                sb.append(new String(Hex.encode(array, i, 32)));
                sb.append("    ");
                sb.append(calculateAscString(array, i, 32));
                sb.append(property);
            }
            else {
                sb.append(string);
                sb.append(new String(Hex.encode(array, i, array.length - i)));
                for (int j = array.length - i; j != 32; ++j) {
                    sb.append("  ");
                }
                sb.append("    ");
                sb.append(calculateAscString(array, i, array.length - i));
                sb.append(property);
            }
        }
        return sb.toString();
    }
    
    private static String calculateAscString(final byte[] array, final int n, final int n2) {
        final StringBuffer sb = new StringBuffer();
        for (int i = n; i != n + n2; ++i) {
            if (array[i] >= 32 && array[i] <= 126) {
                sb.append((char)array[i]);
            }
        }
        return sb.toString();
    }
}
