// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;

public interface InMemoryRepresentable
{
    ASN1Primitive getLoadedObject() throws IOException;
}
