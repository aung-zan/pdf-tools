// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.math.BigInteger;

public class ASN1Enumerated extends DEREnumerated
{
    ASN1Enumerated(final byte[] array) {
        super(array);
    }
    
    public ASN1Enumerated(final BigInteger bigInteger) {
        super(bigInteger);
    }
    
    public ASN1Enumerated(final int n) {
        super(n);
    }
}
