// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.math.BigInteger;

public class ASN1Integer extends DERInteger
{
    ASN1Integer(final byte[] array) {
        super(array);
    }
    
    public ASN1Integer(final BigInteger bigInteger) {
        super(bigInteger);
    }
    
    public ASN1Integer(final long n) {
        super(n);
    }
}
