// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.misc;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public interface MiscObjectIdentifiers
{
    public static final ASN1ObjectIdentifier netscape = new ASN1ObjectIdentifier("2.16.840.1.113730.1");
    public static final ASN1ObjectIdentifier netscapeCertType = MiscObjectIdentifiers.netscape.branch("1");
    public static final ASN1ObjectIdentifier netscapeBaseURL = MiscObjectIdentifiers.netscape.branch("2");
    public static final ASN1ObjectIdentifier netscapeRevocationURL = MiscObjectIdentifiers.netscape.branch("3");
    public static final ASN1ObjectIdentifier netscapeCARevocationURL = MiscObjectIdentifiers.netscape.branch("4");
    public static final ASN1ObjectIdentifier netscapeRenewalURL = MiscObjectIdentifiers.netscape.branch("7");
    public static final ASN1ObjectIdentifier netscapeCApolicyURL = MiscObjectIdentifiers.netscape.branch("8");
    public static final ASN1ObjectIdentifier netscapeSSLServerName = MiscObjectIdentifiers.netscape.branch("12");
    public static final ASN1ObjectIdentifier netscapeCertComment = MiscObjectIdentifiers.netscape.branch("13");
    public static final ASN1ObjectIdentifier verisign = new ASN1ObjectIdentifier("2.16.840.1.113733.1");
    public static final ASN1ObjectIdentifier verisignCzagExtension = MiscObjectIdentifiers.verisign.branch("6.3");
    public static final ASN1ObjectIdentifier verisignDnbDunsNumber = MiscObjectIdentifiers.verisign.branch("6.15");
    public static final ASN1ObjectIdentifier novell = new ASN1ObjectIdentifier("2.16.840.1.113719");
    public static final ASN1ObjectIdentifier novellSecurityAttribs = MiscObjectIdentifiers.novell.branch("1.9.4.1");
    public static final ASN1ObjectIdentifier entrust = new ASN1ObjectIdentifier("1.2.840.113533.7");
    public static final ASN1ObjectIdentifier entrustVersionExtension = MiscObjectIdentifiers.entrust.branch("65.0");
}
