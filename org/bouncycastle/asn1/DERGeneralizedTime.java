// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import org.bouncycastle.util.Arrays;
import java.io.IOException;
import java.util.TimeZone;
import java.util.SimpleTimeZone;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import org.bouncycastle.util.Strings;

public class DERGeneralizedTime extends ASN1Primitive
{
    private byte[] time;
    
    public static ASN1GeneralizedTime getInstance(final Object o) {
        if (o == null || o instanceof ASN1GeneralizedTime) {
            return (ASN1GeneralizedTime)o;
        }
        if (o instanceof DERGeneralizedTime) {
            return new ASN1GeneralizedTime(((DERGeneralizedTime)o).time);
        }
        if (o instanceof byte[]) {
            try {
                return (ASN1GeneralizedTime)ASN1Primitive.fromByteArray((byte[])o);
            }
            catch (Exception ex) {
                throw new IllegalArgumentException("encoding error in getInstance: " + ex.toString());
            }
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + o.getClass().getName());
    }
    
    public static ASN1GeneralizedTime getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        final ASN1Primitive object = asn1TaggedObject.getObject();
        if (b || object instanceof DERGeneralizedTime) {
            return getInstance(object);
        }
        return new ASN1GeneralizedTime(((ASN1OctetString)object).getOctets());
    }
    
    public DERGeneralizedTime(final String s) {
        this.time = Strings.toByteArray(s);
        try {
            this.getDate();
        }
        catch (ParseException ex) {
            throw new IllegalArgumentException("invalid date string: " + ex.getMessage());
        }
    }
    
    public DERGeneralizedTime(final Date date) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss'Z'");
        simpleDateFormat.setTimeZone(new SimpleTimeZone(0, "Z"));
        this.time = Strings.toByteArray(simpleDateFormat.format(date));
    }
    
    DERGeneralizedTime(final byte[] time) {
        this.time = time;
    }
    
    public String getTimeString() {
        return Strings.fromByteArray(this.time);
    }
    
    public String getTime() {
        final String fromByteArray = Strings.fromByteArray(this.time);
        if (fromByteArray.charAt(fromByteArray.length() - 1) == 'Z') {
            return fromByteArray.substring(0, fromByteArray.length() - 1) + "GMT+00:00";
        }
        final int beginIndex = fromByteArray.length() - 5;
        final char char1 = fromByteArray.charAt(beginIndex);
        if (char1 == '-' || char1 == '+') {
            return fromByteArray.substring(0, beginIndex) + "GMT" + fromByteArray.substring(beginIndex, beginIndex + 3) + ":" + fromByteArray.substring(beginIndex + 3);
        }
        final int beginIndex2 = fromByteArray.length() - 3;
        final char char2 = fromByteArray.charAt(beginIndex2);
        if (char2 == '-' || char2 == '+') {
            return fromByteArray.substring(0, beginIndex2) + "GMT" + fromByteArray.substring(beginIndex2) + ":00";
        }
        return fromByteArray + this.calculateGMTOffset();
    }
    
    private String calculateGMTOffset() {
        String str = "+";
        final TimeZone default1 = TimeZone.getDefault();
        int rawOffset = default1.getRawOffset();
        if (rawOffset < 0) {
            str = "-";
            rawOffset = -rawOffset;
        }
        int n = rawOffset / 3600000;
        final int n2 = (rawOffset - n * 60 * 60 * 1000) / 60000;
        try {
            if (default1.useDaylightTime() && default1.inDaylightTime(this.getDate())) {
                n += (str.equals("+") ? 1 : -1);
            }
        }
        catch (ParseException ex) {}
        return "GMT" + str + this.convert(n) + ":" + this.convert(n2);
    }
    
    private String convert(final int n) {
        if (n < 10) {
            return "0" + n;
        }
        return Integer.toString(n);
    }
    
    public Date getDate() throws ParseException {
        String source;
        final String s = source = Strings.fromByteArray(this.time);
        SimpleDateFormat simpleDateFormat;
        if (s.endsWith("Z")) {
            if (this.hasFractionalSeconds()) {
                simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss.SSS'Z'");
            }
            else {
                simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss'Z'");
            }
            simpleDateFormat.setTimeZone(new SimpleTimeZone(0, "Z"));
        }
        else if (s.indexOf(45) > 0 || s.indexOf(43) > 0) {
            source = this.getTime();
            if (this.hasFractionalSeconds()) {
                simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss.SSSz");
            }
            else {
                simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssz");
            }
            simpleDateFormat.setTimeZone(new SimpleTimeZone(0, "Z"));
        }
        else {
            if (this.hasFractionalSeconds()) {
                simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss.SSS");
            }
            else {
                simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            }
            simpleDateFormat.setTimeZone(new SimpleTimeZone(0, TimeZone.getDefault().getID()));
        }
        if (this.hasFractionalSeconds()) {
            String substring;
            int i;
            char char1;
            for (substring = source.substring(14), i = 1; i < substring.length(); ++i) {
                char1 = substring.charAt(i);
                if ('0' > char1) {
                    break;
                }
                if (char1 > '9') {
                    break;
                }
            }
            if (i - 1 > 3) {
                source = source.substring(0, 14) + (substring.substring(0, 4) + substring.substring(i));
            }
            else if (i - 1 == 1) {
                source = source.substring(0, 14) + (substring.substring(0, i) + "00" + substring.substring(i));
            }
            else if (i - 1 == 2) {
                source = source.substring(0, 14) + (substring.substring(0, i) + "0" + substring.substring(i));
            }
        }
        return simpleDateFormat.parse(source);
    }
    
    private boolean hasFractionalSeconds() {
        for (int i = 0; i != this.time.length; ++i) {
            if (this.time[i] == 46 && i == 14) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    boolean isConstructed() {
        return false;
    }
    
    @Override
    int encodedLength() {
        final int length = this.time.length;
        return 1 + StreamUtil.calculateBodyLength(length) + length;
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream) throws IOException {
        asn1OutputStream.writeEncoded(24, this.time);
    }
    
    @Override
    boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        return asn1Primitive instanceof DERGeneralizedTime && Arrays.areEqual(this.time, ((DERGeneralizedTime)asn1Primitive).time);
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.time);
    }
}
