// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.util.io.TeeOutputStream;
import org.bouncycastle.util.io.TeeInputStream;
import org.bouncycastle.operator.DigestCalculator;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.Provider;
import org.bouncycastle.util.io.Streams;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.x509.TBSCertificate;
import org.bouncycastle.asn1.BEROctetStringGenerator;
import java.io.OutputStream;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.OtherRevocationInfoFormat;
import org.bouncycastle.asn1.ocsp.OCSPResponse;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import java.util.Collection;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.cert.X509CRLHolder;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.X509CRL;
import java.security.cert.CRLSelector;
import org.bouncycastle.asn1.x509.CertificateList;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.cert.X509AttributeCertificateHolder;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.util.Selector;
import org.bouncycastle.util.Store;
import java.security.cert.CertStoreException;
import java.util.Iterator;
import java.security.cert.CertificateEncodingException;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Primitive;
import java.security.cert.X509Certificate;
import java.security.cert.CertSelector;
import org.bouncycastle.asn1.x509.Certificate;
import java.util.ArrayList;
import java.util.List;
import java.security.cert.CertStore;
import java.io.InputStream;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.cms.ContentInfo;

class CMSUtils
{
    static ContentInfo readContentInfo(final byte[] array) throws CMSException {
        return readContentInfo(new ASN1InputStream(array));
    }
    
    static ContentInfo readContentInfo(final InputStream inputStream) throws CMSException {
        return readContentInfo(new ASN1InputStream(inputStream));
    }
    
    static List getCertificatesFromStore(final CertStore certStore) throws CertStoreException, CMSException {
        final ArrayList<Certificate> list = new ArrayList<Certificate>();
        try {
            final Iterator<? extends java.security.cert.Certificate> iterator = certStore.getCertificates(null).iterator();
            while (iterator.hasNext()) {
                list.add(Certificate.getInstance(ASN1Primitive.fromByteArray(((X509Certificate)iterator.next()).getEncoded())));
            }
            return list;
        }
        catch (IllegalArgumentException ex) {
            throw new CMSException("error processing certs", ex);
        }
        catch (IOException ex2) {
            throw new CMSException("error processing certs", ex2);
        }
        catch (CertificateEncodingException ex3) {
            throw new CMSException("error encoding certs", ex3);
        }
    }
    
    static List getCertificatesFromStore(final Store store) throws CMSException {
        final ArrayList<Certificate> list = new ArrayList<Certificate>();
        try {
            final Iterator<X509CertificateHolder> iterator = store.getMatches(null).iterator();
            while (iterator.hasNext()) {
                list.add(iterator.next().toASN1Structure());
            }
            return list;
        }
        catch (ClassCastException ex) {
            throw new CMSException("error processing certs", ex);
        }
    }
    
    static List getAttributeCertificatesFromStore(final Store store) throws CMSException {
        final ArrayList<DERTaggedObject> list = new ArrayList<DERTaggedObject>();
        try {
            final Iterator<X509AttributeCertificateHolder> iterator = store.getMatches(null).iterator();
            while (iterator.hasNext()) {
                list.add(new DERTaggedObject(false, 2, iterator.next().toASN1Structure()));
            }
            return list;
        }
        catch (ClassCastException ex) {
            throw new CMSException("error processing certs", ex);
        }
    }
    
    static List getCRLsFromStore(final CertStore certStore) throws CertStoreException, CMSException {
        final ArrayList<CertificateList> list = new ArrayList<CertificateList>();
        try {
            final Iterator<? extends CRL> iterator = certStore.getCRLs(null).iterator();
            while (iterator.hasNext()) {
                list.add(CertificateList.getInstance(ASN1Primitive.fromByteArray(((X509CRL)iterator.next()).getEncoded())));
            }
            return list;
        }
        catch (IllegalArgumentException ex) {
            throw new CMSException("error processing crls", ex);
        }
        catch (IOException ex2) {
            throw new CMSException("error processing crls", ex2);
        }
        catch (CRLException ex3) {
            throw new CMSException("error encoding crls", ex3);
        }
    }
    
    static List getCRLsFromStore(final Store store) throws CMSException {
        final ArrayList<CertificateList> list = new ArrayList<CertificateList>();
        try {
            final Iterator<X509CRLHolder> iterator = store.getMatches(null).iterator();
            while (iterator.hasNext()) {
                list.add(iterator.next().toASN1Structure());
            }
            return list;
        }
        catch (ClassCastException ex) {
            throw new CMSException("error processing certs", ex);
        }
    }
    
    static Collection getOthersFromStore(final ASN1ObjectIdentifier asn1ObjectIdentifier, final Store store) {
        final ArrayList<DERTaggedObject> list = new ArrayList<DERTaggedObject>();
        for (final ASN1Encodable asn1Encodable : store.getMatches(null)) {
            if (CMSObjectIdentifiers.id_ri_ocsp_response.equals(asn1ObjectIdentifier) && OCSPResponse.getInstance(asn1Encodable).getResponseStatus().getValue().intValue() != 0) {
                throw new IllegalArgumentException("cannot add unsuccessful OCSP response to CMS SignedData");
            }
            list.add(new DERTaggedObject(false, 1, new OtherRevocationInfoFormat(asn1ObjectIdentifier, asn1Encodable)));
        }
        return list;
    }
    
    static ASN1Set createBerSetFromList(final List list) {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final Iterator<ASN1Encodable> iterator = list.iterator();
        while (iterator.hasNext()) {
            asn1EncodableVector.add(iterator.next());
        }
        return new BERSet(asn1EncodableVector);
    }
    
    static ASN1Set createDerSetFromList(final List list) {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final Iterator<ASN1Encodable> iterator = list.iterator();
        while (iterator.hasNext()) {
            asn1EncodableVector.add(iterator.next());
        }
        return new DERSet(asn1EncodableVector);
    }
    
    static OutputStream createBEROctetOutputStream(final OutputStream outputStream, final int n, final boolean b, final int n2) throws IOException {
        final BEROctetStringGenerator berOctetStringGenerator = new BEROctetStringGenerator(outputStream, n, b);
        if (n2 != 0) {
            return berOctetStringGenerator.getOctetOutputStream(new byte[n2]);
        }
        return berOctetStringGenerator.getOctetOutputStream();
    }
    
    static TBSCertificate getTBSCertificateStructure(final X509Certificate x509Certificate) {
        try {
            return TBSCertificate.getInstance(ASN1Primitive.fromByteArray(x509Certificate.getTBSCertificate()));
        }
        catch (Exception ex) {
            throw new IllegalArgumentException("can't extract TBS structure from this cert");
        }
    }
    
    static IssuerAndSerialNumber getIssuerAndSerialNumber(final X509Certificate x509Certificate) {
        final TBSCertificate tbsCertificateStructure = getTBSCertificateStructure(x509Certificate);
        return new IssuerAndSerialNumber(tbsCertificateStructure.getIssuer(), tbsCertificateStructure.getSerialNumber().getValue());
    }
    
    private static ContentInfo readContentInfo(final ASN1InputStream asn1InputStream) throws CMSException {
        try {
            return ContentInfo.getInstance(asn1InputStream.readObject());
        }
        catch (IOException ex) {
            throw new CMSException("IOException reading content.", ex);
        }
        catch (ClassCastException ex2) {
            throw new CMSException("Malformed content.", ex2);
        }
        catch (IllegalArgumentException ex3) {
            throw new CMSException("Malformed content.", ex3);
        }
    }
    
    public static byte[] streamToByteArray(final InputStream inputStream) throws IOException {
        return Streams.readAll(inputStream);
    }
    
    public static byte[] streamToByteArray(final InputStream inputStream, final int n) throws IOException {
        return Streams.readAllLimited(inputStream, n);
    }
    
    public static Provider getProvider(final String s) throws NoSuchProviderException {
        if (s == null) {
            return null;
        }
        final Provider provider = Security.getProvider(s);
        if (provider != null) {
            return provider;
        }
        throw new NoSuchProviderException("provider " + s + " not found.");
    }
    
    static InputStream attachDigestsToInputStream(final Collection collection, final InputStream inputStream) {
        InputStream inputStream2 = inputStream;
        final Iterator<DigestCalculator> iterator = collection.iterator();
        while (iterator.hasNext()) {
            inputStream2 = new TeeInputStream(inputStream2, iterator.next().getOutputStream());
        }
        return inputStream2;
    }
    
    static OutputStream attachSignersToOutputStream(final Collection collection, final OutputStream outputStream) {
        OutputStream safeTeeOutputStream = outputStream;
        final Iterator<SignerInfoGenerator> iterator = collection.iterator();
        while (iterator.hasNext()) {
            safeTeeOutputStream = getSafeTeeOutputStream(safeTeeOutputStream, iterator.next().getCalculatingOutputStream());
        }
        return safeTeeOutputStream;
    }
    
    static OutputStream getSafeOutputStream(final OutputStream outputStream) {
        return (outputStream == null) ? new NullOutputStream() : outputStream;
    }
    
    static OutputStream getSafeTeeOutputStream(final OutputStream outputStream, final OutputStream outputStream2) {
        return (outputStream == null) ? getSafeOutputStream(outputStream2) : ((outputStream2 == null) ? getSafeOutputStream(outputStream) : new TeeOutputStream(outputStream, outputStream2));
    }
}
