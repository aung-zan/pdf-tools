// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.x509.CertificateList;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.asn1.ASN1Primitive;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Set;
import java.util.Collection;
import org.bouncycastle.util.CollectionStore;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.cert.X509CertificateHolder;
import java.util.ArrayList;
import org.bouncycastle.util.Store;
import org.bouncycastle.asn1.cms.OriginatorInfo;

public class OriginatorInformation
{
    private OriginatorInfo originatorInfo;
    
    OriginatorInformation(final OriginatorInfo originatorInfo) {
        this.originatorInfo = originatorInfo;
    }
    
    public Store getCertificates() {
        final ASN1Set certificates = this.originatorInfo.getCertificates();
        if (certificates != null) {
            final ArrayList list = new ArrayList<X509CertificateHolder>(certificates.size());
            final Enumeration objects = certificates.getObjects();
            while (objects.hasMoreElements()) {
                final ASN1Primitive asn1Primitive = objects.nextElement().toASN1Primitive();
                if (asn1Primitive instanceof ASN1Sequence) {
                    list.add(new X509CertificateHolder(Certificate.getInstance(asn1Primitive)));
                }
            }
            return new CollectionStore(list);
        }
        return new CollectionStore(new ArrayList());
    }
    
    public Store getCRLs() {
        final ASN1Set crLs = this.originatorInfo.getCRLs();
        if (crLs != null) {
            final ArrayList list = new ArrayList<X509CRLHolder>(crLs.size());
            final Enumeration objects = crLs.getObjects();
            while (objects.hasMoreElements()) {
                final ASN1Primitive asn1Primitive = objects.nextElement().toASN1Primitive();
                if (asn1Primitive instanceof ASN1Sequence) {
                    list.add(new X509CRLHolder(CertificateList.getInstance(asn1Primitive)));
                }
            }
            return new CollectionStore(list);
        }
        return new CollectionStore(new ArrayList());
    }
    
    public OriginatorInfo toASN1Structure() {
        return this.originatorInfo;
    }
}
