// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.cms.SignerInfo;
import java.util.List;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Integer;
import java.util.Iterator;
import java.util.Collection;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.BERSequenceGenerator;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import java.io.IOException;
import java.io.OutputStream;
import org.bouncycastle.operator.ContentSigner;
import java.security.cert.CertificateEncodingException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.asn1.cms.AttributeTable;
import java.security.Provider;
import java.security.InvalidKeyException;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.security.PrivateKey;
import java.security.SecureRandom;

public class CMSSignedDataStreamGenerator extends CMSSignedGenerator
{
    private int _bufferSize;
    
    public CMSSignedDataStreamGenerator() {
    }
    
    @Deprecated
    public CMSSignedDataStreamGenerator(final SecureRandom secureRandom) {
        super(secureRandom);
    }
    
    public void setBufferSize(final int bufferSize) {
        this._bufferSize = bufferSize;
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, new DefaultSignedAttributeTableGenerator(), null, provider);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final String s3) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, s2, CMSUtils.getProvider(s3));
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, s2, new DefaultSignedAttributeTableGenerator(), null, provider);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, attributeTable, attributeTable2, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), provider);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final AttributeTable attributeTable, final AttributeTable attributeTable2, final String s3) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, s2, attributeTable, attributeTable2, CMSUtils.getProvider(s3));
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final AttributeTable attributeTable, final AttributeTable attributeTable2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, s2, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), provider);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, this.getEncOID(privateKey, s), s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, provider);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final String s3) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, s2, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, CMSUtils.getProvider(s3));
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        this.addSigner(privateKey, x509Certificate, s, s2, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, provider, provider);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, new DefaultSignedAttributeTableGenerator(), null, provider);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2, final String s3) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, s2, CMSUtils.getProvider(s3));
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, s2, new DefaultSignedAttributeTableGenerator(), null, provider);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, attributeTable, attributeTable2, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        this.addSigner(privateKey, array, s, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), provider);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        this.addSigner(privateKey, array, this.getEncOID(privateKey, s), s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, provider);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final String s3) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        this.addSigner(privateKey, array, s, s2, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, CMSUtils.getProvider(s3));
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        this.addSigner(privateKey, array, s, s2, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, provider, provider);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final Provider provider, final Provider provider2) throws NoSuchAlgorithmException, InvalidKeyException {
        this.doAddSigner(privateKey, x509Certificate, s, s2, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, provider, provider2);
    }
    
    private void doAddSigner(final PrivateKey privateKey, final Object o, final String s, final String s2, final CMSAttributeTableGenerator signedAttributeGenerator, final CMSAttributeTableGenerator unsignedAttributeGenerator, final Provider provider, final Provider provider2) throws NoSuchAlgorithmException, InvalidKeyException {
        final String string = CMSSignedHelper.INSTANCE.getDigestAlgName(s2) + "with" + CMSSignedHelper.INSTANCE.getEncryptionAlgName(s);
        JcaContentSignerBuilder setSecureRandom;
        try {
            setSecureRandom = new JcaContentSignerBuilder(string).setSecureRandom(this.rand);
        }
        catch (IllegalArgumentException ex) {
            throw new NoSuchAlgorithmException(ex.getMessage());
        }
        if (provider != null) {
            setSecureRandom.setProvider(provider);
        }
        try {
            final JcaDigestCalculatorProviderBuilder jcaDigestCalculatorProviderBuilder = new JcaDigestCalculatorProviderBuilder();
            if (provider2 != null && !provider2.getName().equalsIgnoreCase("SunRsaSign")) {
                jcaDigestCalculatorProviderBuilder.setProvider(provider2);
            }
            final JcaSignerInfoGeneratorBuilder jcaSignerInfoGeneratorBuilder = new JcaSignerInfoGeneratorBuilder(jcaDigestCalculatorProviderBuilder.build());
            jcaSignerInfoGeneratorBuilder.setSignedAttributeGenerator(signedAttributeGenerator);
            jcaSignerInfoGeneratorBuilder.setUnsignedAttributeGenerator(unsignedAttributeGenerator);
            try {
                final ContentSigner build = setSecureRandom.build(privateKey);
                if (o instanceof X509Certificate) {
                    this.addSignerInfoGenerator(jcaSignerInfoGeneratorBuilder.build(build, (X509Certificate)o));
                }
                else {
                    this.addSignerInfoGenerator(jcaSignerInfoGeneratorBuilder.build(build, (byte[])o));
                }
            }
            catch (OperatorCreationException ex2) {
                if (ex2.getCause() instanceof NoSuchAlgorithmException) {
                    throw (NoSuchAlgorithmException)ex2.getCause();
                }
                if (ex2.getCause() instanceof InvalidKeyException) {
                    throw (InvalidKeyException)ex2.getCause();
                }
            }
        }
        catch (OperatorCreationException ex3) {
            throw new NoSuchAlgorithmException("unable to create operators: " + ex3.getMessage());
        }
        catch (CertificateEncodingException ex4) {
            throw new IllegalStateException("unable to encode certificate");
        }
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final Provider provider, final Provider provider2) throws NoSuchAlgorithmException, InvalidKeyException {
        this.doAddSigner(privateKey, array, s, s2, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, provider, provider2);
    }
    
    public OutputStream open(final OutputStream outputStream) throws IOException {
        return this.open(outputStream, false);
    }
    
    public OutputStream open(final OutputStream outputStream, final boolean b) throws IOException {
        return this.open(CMSObjectIdentifiers.data, outputStream, b);
    }
    
    public OutputStream open(final OutputStream outputStream, final boolean b, final OutputStream outputStream2) throws IOException {
        return this.open(CMSObjectIdentifiers.data, outputStream, b, outputStream2);
    }
    
    @Deprecated
    public OutputStream open(final OutputStream outputStream, final String s, final boolean b) throws IOException {
        return this.open(outputStream, s, b, null);
    }
    
    public OutputStream open(final ASN1ObjectIdentifier asn1ObjectIdentifier, final OutputStream outputStream, final boolean b) throws IOException {
        return this.open(asn1ObjectIdentifier, outputStream, b, null);
    }
    
    @Deprecated
    public OutputStream open(final OutputStream outputStream, final String s, final boolean b, final OutputStream outputStream2) throws IOException {
        return this.open(new ASN1ObjectIdentifier(s), outputStream, b, outputStream2);
    }
    
    public OutputStream open(final ASN1ObjectIdentifier asn1ObjectIdentifier, final OutputStream outputStream, final boolean b, final OutputStream outputStream2) throws IOException {
        final BERSequenceGenerator berSequenceGenerator = new BERSequenceGenerator(outputStream);
        berSequenceGenerator.addObject(CMSObjectIdentifiers.signedData);
        final BERSequenceGenerator berSequenceGenerator2 = new BERSequenceGenerator(berSequenceGenerator.getRawOutputStream(), 0, true);
        berSequenceGenerator2.addObject(this.calculateVersion(asn1ObjectIdentifier));
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final Iterator<SignerInformation> iterator = this._signers.iterator();
        while (iterator.hasNext()) {
            asn1EncodableVector.add(CMSSignedHelper.INSTANCE.fixAlgID(iterator.next().getDigestAlgorithmID()));
        }
        final Iterator<SignerInfoGenerator> iterator2 = this.signerGens.iterator();
        while (iterator2.hasNext()) {
            asn1EncodableVector.add(iterator2.next().getDigestAlgorithm());
        }
        berSequenceGenerator2.getRawOutputStream().write(new DERSet(asn1EncodableVector).getEncoded());
        final BERSequenceGenerator berSequenceGenerator3 = new BERSequenceGenerator(berSequenceGenerator2.getRawOutputStream());
        berSequenceGenerator3.addObject(asn1ObjectIdentifier);
        return new CmsSignedDataOutputStream(CMSUtils.attachSignersToOutputStream(this.signerGens, CMSUtils.getSafeTeeOutputStream(outputStream2, b ? CMSUtils.createBEROctetOutputStream(berSequenceGenerator3.getRawOutputStream(), 0, true, this._bufferSize) : null)), asn1ObjectIdentifier, berSequenceGenerator, berSequenceGenerator2, berSequenceGenerator3);
    }
    
    void generate(final OutputStream outputStream, final String s, final boolean b, final OutputStream outputStream2, final CMSProcessable cmsProcessable) throws CMSException, IOException {
        final OutputStream open = this.open(outputStream, s, b, outputStream2);
        if (cmsProcessable != null) {
            cmsProcessable.write(open);
        }
        open.close();
    }
    
    private ASN1Integer calculateVersion(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        boolean b = false;
        boolean b2 = false;
        boolean b3 = false;
        boolean b4 = false;
        if (this.certs != null) {
            for (final ASN1TaggedObject next : this.certs) {
                if (next instanceof ASN1TaggedObject) {
                    final ASN1TaggedObject asn1TaggedObject = next;
                    if (asn1TaggedObject.getTagNo() == 1) {
                        b3 = true;
                    }
                    else if (asn1TaggedObject.getTagNo() == 2) {
                        b4 = true;
                    }
                    else {
                        if (asn1TaggedObject.getTagNo() != 3) {
                            continue;
                        }
                        b = true;
                    }
                }
            }
        }
        if (b) {
            return new ASN1Integer(5L);
        }
        if (this.crls != null) {
            final Iterator<Object> iterator2 = this.crls.iterator();
            while (iterator2.hasNext()) {
                if (iterator2.next() instanceof ASN1TaggedObject) {
                    b2 = true;
                }
            }
        }
        if (b2) {
            return new ASN1Integer(5L);
        }
        if (b4) {
            return new ASN1Integer(4L);
        }
        if (b3) {
            return new ASN1Integer(3L);
        }
        if (this.checkForVersion3(this._signers, this.signerGens)) {
            return new ASN1Integer(3L);
        }
        if (!CMSObjectIdentifiers.data.equals(asn1ObjectIdentifier)) {
            return new ASN1Integer(3L);
        }
        return new ASN1Integer(1L);
    }
    
    private boolean checkForVersion3(final List list, final List list2) {
        final Iterator<SignerInformation> iterator = list.iterator();
        while (iterator.hasNext()) {
            if (SignerInfo.getInstance(iterator.next().toASN1Structure()).getVersion().getValue().intValue() == 3) {
                return true;
            }
        }
        final Iterator<SignerInfoGenerator> iterator2 = list2.iterator();
        while (iterator2.hasNext()) {
            if (iterator2.next().getGeneratedVersion().getValue().intValue() == 3) {
                return true;
            }
        }
        return false;
    }
    
    private class CmsSignedDataOutputStream extends OutputStream
    {
        private OutputStream _out;
        private ASN1ObjectIdentifier _contentOID;
        private BERSequenceGenerator _sGen;
        private BERSequenceGenerator _sigGen;
        private BERSequenceGenerator _eiGen;
        
        public CmsSignedDataOutputStream(final OutputStream out, final ASN1ObjectIdentifier contentOID, final BERSequenceGenerator sGen, final BERSequenceGenerator sigGen, final BERSequenceGenerator eiGen) {
            this._out = out;
            this._contentOID = contentOID;
            this._sGen = sGen;
            this._sigGen = sigGen;
            this._eiGen = eiGen;
        }
        
        @Override
        public void write(final int n) throws IOException {
            this._out.write(n);
        }
        
        @Override
        public void write(final byte[] b, final int off, final int len) throws IOException {
            this._out.write(b, off, len);
        }
        
        @Override
        public void write(final byte[] b) throws IOException {
            this._out.write(b);
        }
        
        @Override
        public void close() throws IOException {
            this._out.close();
            this._eiGen.close();
            CMSSignedDataStreamGenerator.this.digests.clear();
            if (CMSSignedDataStreamGenerator.this.certs.size() != 0) {
                this._sigGen.getRawOutputStream().write(new BERTaggedObject(false, 0, CMSUtils.createBerSetFromList(CMSSignedDataStreamGenerator.this.certs)).getEncoded());
            }
            if (CMSSignedDataStreamGenerator.this.crls.size() != 0) {
                this._sigGen.getRawOutputStream().write(new BERTaggedObject(false, 1, CMSUtils.createBerSetFromList(CMSSignedDataStreamGenerator.this.crls)).getEncoded());
            }
            final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
            for (final SignerInfoGenerator signerInfoGenerator : CMSSignedDataStreamGenerator.this.signerGens) {
                try {
                    asn1EncodableVector.add(signerInfoGenerator.generate(this._contentOID));
                    CMSSignedDataStreamGenerator.this.digests.put(signerInfoGenerator.getDigestAlgorithm().getAlgorithm().getId(), signerInfoGenerator.getCalculatedDigest());
                }
                catch (CMSException ex) {
                    throw new CMSStreamException("exception generating signers: " + ex.getMessage(), ex);
                }
            }
            final Iterator<SignerInformation> iterator2 = CMSSignedDataStreamGenerator.this._signers.iterator();
            while (iterator2.hasNext()) {
                asn1EncodableVector.add(iterator2.next().toASN1Structure());
            }
            this._sigGen.getRawOutputStream().write(new DERSet(asn1EncodableVector).getEncoded());
            this._sigGen.close();
            this._sGen.close();
        }
    }
}
