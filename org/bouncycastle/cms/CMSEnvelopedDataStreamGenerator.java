// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.util.Map;
import java.util.HashMap;
import javax.crypto.KeyGenerator;
import java.security.NoSuchProviderException;
import org.bouncycastle.asn1.cms.EnvelopedData;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.BERSequenceGenerator;
import java.util.Iterator;
import org.bouncycastle.operator.GenericKey;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.operator.OutputEncryptor;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.cms.jcajce.JceCMSContentEncryptorBuilder;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.Provider;
import java.io.OutputStream;
import org.bouncycastle.asn1.ASN1Integer;
import java.security.SecureRandom;
import org.bouncycastle.asn1.ASN1Set;

public class CMSEnvelopedDataStreamGenerator extends CMSEnvelopedGenerator
{
    private ASN1Set _unprotectedAttributes;
    private int _bufferSize;
    private boolean _berEncodeRecipientSet;
    
    public CMSEnvelopedDataStreamGenerator() {
        this._unprotectedAttributes = null;
    }
    
    @Deprecated
    public CMSEnvelopedDataStreamGenerator(final SecureRandom secureRandom) {
        super(secureRandom);
        this._unprotectedAttributes = null;
    }
    
    public void setBufferSize(final int bufferSize) {
        this._bufferSize = bufferSize;
    }
    
    public void setBEREncodeRecipients(final boolean berEncodeRecipientSet) {
        this._berEncodeRecipientSet = berEncodeRecipientSet;
    }
    
    private ASN1Integer getVersion() {
        if (this.originatorInfo != null || this._unprotectedAttributes != null) {
            return new ASN1Integer(2L);
        }
        return new ASN1Integer(0L);
    }
    
    @Deprecated
    private OutputStream open(final OutputStream outputStream, final String s, final int n, final Provider provider, final Provider provider2) throws NoSuchAlgorithmException, CMSException, IOException {
        this.convertOldRecipients(this.rand, provider2);
        JceCMSContentEncryptorBuilder jceCMSContentEncryptorBuilder;
        if (n != -1) {
            jceCMSContentEncryptorBuilder = new JceCMSContentEncryptorBuilder(new ASN1ObjectIdentifier(s), n);
        }
        else {
            jceCMSContentEncryptorBuilder = new JceCMSContentEncryptorBuilder(new ASN1ObjectIdentifier(s));
        }
        jceCMSContentEncryptorBuilder.setProvider(provider);
        jceCMSContentEncryptorBuilder.setSecureRandom(this.rand);
        return this.doOpen(CMSObjectIdentifiers.data, outputStream, jceCMSContentEncryptorBuilder.build());
    }
    
    private OutputStream doOpen(final ASN1ObjectIdentifier asn1ObjectIdentifier, final OutputStream outputStream, final OutputEncryptor outputEncryptor) throws IOException, CMSException {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final GenericKey key = outputEncryptor.getKey();
        final Iterator<RecipientInfoGenerator> iterator = this.recipientInfoGenerators.iterator();
        while (iterator.hasNext()) {
            asn1EncodableVector.add(iterator.next().generate(key));
        }
        return this.open(asn1ObjectIdentifier, outputStream, asn1EncodableVector, outputEncryptor);
    }
    
    protected OutputStream open(final ASN1ObjectIdentifier asn1ObjectIdentifier, final OutputStream outputStream, final ASN1EncodableVector asn1EncodableVector, final OutputEncryptor outputEncryptor) throws IOException {
        final BERSequenceGenerator berSequenceGenerator = new BERSequenceGenerator(outputStream);
        berSequenceGenerator.addObject(CMSObjectIdentifiers.envelopedData);
        final BERSequenceGenerator berSequenceGenerator2 = new BERSequenceGenerator(berSequenceGenerator.getRawOutputStream(), 0, true);
        berSequenceGenerator2.addObject(this.getVersion());
        if (this.originatorInfo != null) {
            berSequenceGenerator2.addObject(new DERTaggedObject(false, 0, this.originatorInfo));
        }
        if (this._berEncodeRecipientSet) {
            berSequenceGenerator2.getRawOutputStream().write(new BERSet(asn1EncodableVector).getEncoded());
        }
        else {
            berSequenceGenerator2.getRawOutputStream().write(new DERSet(asn1EncodableVector).getEncoded());
        }
        final BERSequenceGenerator berSequenceGenerator3 = new BERSequenceGenerator(berSequenceGenerator2.getRawOutputStream());
        berSequenceGenerator3.addObject(asn1ObjectIdentifier);
        berSequenceGenerator3.getRawOutputStream().write(outputEncryptor.getAlgorithmIdentifier().getEncoded());
        return new CmsEnvelopedDataOutputStream(outputEncryptor.getOutputStream(CMSUtils.createBEROctetOutputStream(berSequenceGenerator3.getRawOutputStream(), 0, false, this._bufferSize)), berSequenceGenerator, berSequenceGenerator2, berSequenceGenerator3);
    }
    
    protected OutputStream open(final OutputStream outputStream, final ASN1EncodableVector asn1EncodableVector, final OutputEncryptor outputEncryptor) throws CMSException {
        try {
            final BERSequenceGenerator berSequenceGenerator = new BERSequenceGenerator(outputStream);
            berSequenceGenerator.addObject(CMSObjectIdentifiers.envelopedData);
            final BERSequenceGenerator berSequenceGenerator2 = new BERSequenceGenerator(berSequenceGenerator.getRawOutputStream(), 0, true);
            ASN1Set set;
            if (this._berEncodeRecipientSet) {
                set = new BERSet(asn1EncodableVector);
            }
            else {
                set = new DERSet(asn1EncodableVector);
            }
            berSequenceGenerator2.addObject(new ASN1Integer(EnvelopedData.calculateVersion(this.originatorInfo, set, this._unprotectedAttributes)));
            if (this.originatorInfo != null) {
                berSequenceGenerator2.addObject(new DERTaggedObject(false, 0, this.originatorInfo));
            }
            berSequenceGenerator2.getRawOutputStream().write(set.getEncoded());
            final BERSequenceGenerator berSequenceGenerator3 = new BERSequenceGenerator(berSequenceGenerator2.getRawOutputStream());
            berSequenceGenerator3.addObject(CMSObjectIdentifiers.data);
            berSequenceGenerator3.getRawOutputStream().write(outputEncryptor.getAlgorithmIdentifier().getEncoded());
            return new CmsEnvelopedDataOutputStream(outputEncryptor.getOutputStream(CMSUtils.createBEROctetOutputStream(berSequenceGenerator3.getRawOutputStream(), 0, false, this._bufferSize)), berSequenceGenerator, berSequenceGenerator2, berSequenceGenerator3);
        }
        catch (IOException ex) {
            throw new CMSException("exception decoding algorithm parameters.", ex);
        }
    }
    
    @Deprecated
    public OutputStream open(final OutputStream outputStream, final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException, IOException {
        return this.open(outputStream, s, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public OutputStream open(final OutputStream outputStream, final String s, final Provider provider) throws NoSuchAlgorithmException, CMSException, IOException {
        final KeyGenerator symmetricKeyGenerator = CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(s, provider);
        symmetricKeyGenerator.init(this.rand);
        return this.open(outputStream, s, -1, symmetricKeyGenerator.getProvider(), provider);
    }
    
    @Deprecated
    public OutputStream open(final OutputStream outputStream, final String s, final int n, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException, IOException {
        return this.open(outputStream, s, n, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public OutputStream open(final OutputStream outputStream, final String s, final int keysize, final Provider provider) throws NoSuchAlgorithmException, CMSException, IOException {
        final KeyGenerator symmetricKeyGenerator = CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(s, provider);
        symmetricKeyGenerator.init(keysize, this.rand);
        return this.open(outputStream, s, -1, symmetricKeyGenerator.getProvider(), provider);
    }
    
    public OutputStream open(final OutputStream outputStream, final OutputEncryptor outputEncryptor) throws CMSException, IOException {
        return this.doOpen(new ASN1ObjectIdentifier(CMSObjectIdentifiers.data.getId()), outputStream, outputEncryptor);
    }
    
    public OutputStream open(final ASN1ObjectIdentifier asn1ObjectIdentifier, final OutputStream outputStream, final OutputEncryptor outputEncryptor) throws CMSException, IOException {
        return this.doOpen(asn1ObjectIdentifier, outputStream, outputEncryptor);
    }
    
    private class CmsEnvelopedDataOutputStream extends OutputStream
    {
        private OutputStream _out;
        private BERSequenceGenerator _cGen;
        private BERSequenceGenerator _envGen;
        private BERSequenceGenerator _eiGen;
        
        public CmsEnvelopedDataOutputStream(final OutputStream out, final BERSequenceGenerator cGen, final BERSequenceGenerator envGen, final BERSequenceGenerator eiGen) {
            this._out = out;
            this._cGen = cGen;
            this._envGen = envGen;
            this._eiGen = eiGen;
        }
        
        @Override
        public void write(final int n) throws IOException {
            this._out.write(n);
        }
        
        @Override
        public void write(final byte[] b, final int off, final int len) throws IOException {
            this._out.write(b, off, len);
        }
        
        @Override
        public void write(final byte[] b) throws IOException {
            this._out.write(b);
        }
        
        @Override
        public void close() throws IOException {
            this._out.close();
            this._eiGen.close();
            if (CMSEnvelopedDataStreamGenerator.this.unprotectedAttributeGenerator != null) {
                this._envGen.addObject(new DERTaggedObject(false, 1, new BERSet(CMSEnvelopedDataStreamGenerator.this.unprotectedAttributeGenerator.getAttributes(new HashMap()).toASN1EncodableVector())));
            }
            this._envGen.close();
            this._cGen.close();
        }
    }
}
