// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.DigestCalculatorProvider;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
import org.bouncycastle.asn1.cms.SignerInfo;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.SignedData;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.BEROctetString;
import java.util.Collection;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.util.Iterator;
import java.io.OutputStream;
import java.security.cert.CertificateEncodingException;
import java.io.IOException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.asn1.cms.AttributeTable;
import java.security.cert.X509Certificate;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class CMSSignedDataGenerator extends CMSSignedGenerator
{
    private List signerInfs;
    
    public CMSSignedDataGenerator() {
        this.signerInfs = new ArrayList();
    }
    
    @Deprecated
    public CMSSignedDataGenerator(final SecureRandom secureRandom) {
        super(secureRandom);
        this.signerInfs = new ArrayList();
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s) throws IllegalArgumentException {
        this.addSigner(privateKey, x509Certificate, this.getEncOID(privateKey, s), s);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2) throws IllegalArgumentException {
        this.doAddSigner(privateKey, x509Certificate, s, s2, new DefaultSignedAttributeTableGenerator(), null, null);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s) throws IllegalArgumentException {
        this.addSigner(privateKey, array, this.getEncOID(privateKey, s), s);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2) throws IllegalArgumentException {
        this.doAddSigner(privateKey, array, s, s2, new DefaultSignedAttributeTableGenerator(), null, null);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2) throws IllegalArgumentException {
        this.addSigner(privateKey, x509Certificate, this.getEncOID(privateKey, s), s, attributeTable, attributeTable2);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final AttributeTable attributeTable, final AttributeTable attributeTable2) throws IllegalArgumentException {
        this.doAddSigner(privateKey, x509Certificate, s, s2, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), attributeTable);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final AttributeTable attributeTable, final AttributeTable attributeTable2) throws IllegalArgumentException {
        this.addSigner(privateKey, array, this.getEncOID(privateKey, s), s, attributeTable, attributeTable2);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2, final AttributeTable attributeTable, final AttributeTable attributeTable2) throws IllegalArgumentException {
        this.doAddSigner(privateKey, array, s, s2, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), attributeTable);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2) throws IllegalArgumentException {
        this.addSigner(privateKey, x509Certificate, this.getEncOID(privateKey, s), s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2) throws IllegalArgumentException {
        this.doAddSigner(privateKey, x509Certificate, s, s2, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, null);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2) throws IllegalArgumentException {
        this.addSigner(privateKey, array, this.getEncOID(privateKey, s), s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2);
    }
    
    @Deprecated
    public void addSigner(final PrivateKey privateKey, final byte[] array, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2) throws IllegalArgumentException {
        this.doAddSigner(privateKey, array, s, s2, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, null);
    }
    
    private void doAddSigner(final PrivateKey privateKey, final Object o, final String s, final String s2, final CMSAttributeTableGenerator cmsAttributeTableGenerator, final CMSAttributeTableGenerator cmsAttributeTableGenerator2, final AttributeTable attributeTable) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(privateKey, o, s2, s, cmsAttributeTableGenerator, cmsAttributeTableGenerator2, attributeTable));
    }
    
    @Deprecated
    public CMSSignedData generate(final CMSProcessable cmsProcessable, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(cmsProcessable, CMSUtils.getProvider(s));
    }
    
    @Deprecated
    public CMSSignedData generate(final CMSProcessable cmsProcessable, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        return this.generate(cmsProcessable, false, provider);
    }
    
    @Deprecated
    public CMSSignedData generate(final String s, final CMSProcessable cmsProcessable, final boolean b, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(s, cmsProcessable, b, CMSUtils.getProvider(s2), true);
    }
    
    @Deprecated
    public CMSSignedData generate(final String s, final CMSProcessable cmsProcessable, final boolean b, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        return this.generate(s, cmsProcessable, b, provider, true);
    }
    
    @Deprecated
    public CMSSignedData generate(final String s, final CMSProcessable cmsProcessable, final boolean b, final String s2, final boolean b2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(s, cmsProcessable, b, CMSUtils.getProvider(s2), b2);
    }
    
    @Deprecated
    public CMSSignedData generate(final String s, final CMSProcessable cmsProcessable, final boolean b, final Provider provider, final boolean b2) throws NoSuchAlgorithmException, CMSException {
        final ASN1ObjectIdentifier asn1ObjectIdentifier = (s == null) ? null : new ASN1ObjectIdentifier(s);
        for (final SignerInf signerInf : this.signerInfs) {
            try {
                this.signerGens.add(signerInf.toSignerInfoGenerator(this.rand, provider, b2));
            }
            catch (OperatorCreationException ex) {
                throw new CMSException("exception creating signerInf", ex);
            }
            catch (IOException ex2) {
                throw new CMSException("exception encoding attributes", ex2);
            }
            catch (CertificateEncodingException ex3) {
                throw new CMSException("error creating sid.", ex3);
            }
        }
        this.signerInfs.clear();
        if (cmsProcessable != null) {
            return this.generate(new CMSTypedData() {
                public ASN1ObjectIdentifier getContentType() {
                    return asn1ObjectIdentifier;
                }
                
                public void write(final OutputStream outputStream) throws IOException, CMSException {
                    cmsProcessable.write(outputStream);
                }
                
                public Object getContent() {
                    return cmsProcessable.getContent();
                }
            }, b);
        }
        return this.generate(new CMSAbsentContent(asn1ObjectIdentifier), b);
    }
    
    @Deprecated
    public CMSSignedData generate(final CMSProcessable cmsProcessable, final boolean b, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        if (cmsProcessable instanceof CMSTypedData) {
            return this.generate(((CMSTypedData)cmsProcessable).getContentType().getId(), cmsProcessable, b, s);
        }
        return this.generate(CMSSignedDataGenerator.DATA, cmsProcessable, b, s);
    }
    
    @Deprecated
    public CMSSignedData generate(final CMSProcessable cmsProcessable, final boolean b, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        if (cmsProcessable instanceof CMSTypedData) {
            return this.generate(((CMSTypedData)cmsProcessable).getContentType().getId(), cmsProcessable, b, provider);
        }
        return this.generate(CMSSignedDataGenerator.DATA, cmsProcessable, b, provider);
    }
    
    public CMSSignedData generate(final CMSTypedData cmsTypedData) throws CMSException {
        return this.generate(cmsTypedData, false);
    }
    
    public CMSSignedData generate(final CMSTypedData cmsTypedData, final boolean b) throws CMSException {
        if (!this.signerInfs.isEmpty()) {
            throw new IllegalStateException("this method can only be used with SignerInfoGenerator");
        }
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
        this.digests.clear();
        for (final SignerInformation signerInformation : this._signers) {
            asn1EncodableVector.add(CMSSignedHelper.INSTANCE.fixAlgID(signerInformation.getDigestAlgorithmID()));
            asn1EncodableVector2.add(signerInformation.toASN1Structure());
        }
        final ASN1ObjectIdentifier contentType = cmsTypedData.getContentType();
        ASN1Encodable asn1Encodable = null;
        if (cmsTypedData != null) {
            ByteArrayOutputStream byteArrayOutputStream = null;
            if (b) {
                byteArrayOutputStream = new ByteArrayOutputStream();
            }
            final OutputStream safeOutputStream = CMSUtils.getSafeOutputStream(CMSUtils.attachSignersToOutputStream(this.signerGens, byteArrayOutputStream));
            try {
                cmsTypedData.write(safeOutputStream);
                safeOutputStream.close();
            }
            catch (IOException ex) {
                throw new CMSException("data processing exception: " + ex.getMessage(), ex);
            }
            if (b) {
                asn1Encodable = new BEROctetString(byteArrayOutputStream.toByteArray());
            }
        }
        for (final SignerInfoGenerator signerInfoGenerator : this.signerGens) {
            final SignerInfo generate = signerInfoGenerator.generate(contentType);
            asn1EncodableVector.add(generate.getDigestAlgorithm());
            asn1EncodableVector2.add(generate);
            final byte[] calculatedDigest = signerInfoGenerator.getCalculatedDigest();
            if (calculatedDigest != null) {
                this.digests.put(generate.getDigestAlgorithm().getAlgorithm().getId(), calculatedDigest);
            }
        }
        ASN1Set berSetFromList = null;
        if (this.certs.size() != 0) {
            berSetFromList = CMSUtils.createBerSetFromList(this.certs);
        }
        ASN1Set berSetFromList2 = null;
        if (this.crls.size() != 0) {
            berSetFromList2 = CMSUtils.createBerSetFromList(this.crls);
        }
        return new CMSSignedData(cmsTypedData, new ContentInfo(CMSObjectIdentifiers.signedData, new SignedData(new DERSet(asn1EncodableVector), new ContentInfo(contentType, asn1Encodable), berSetFromList, berSetFromList2, new DERSet(asn1EncodableVector2))));
    }
    
    @Deprecated
    public SignerInformationStore generateCounterSigners(final SignerInformation signerInformation, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        return this.generate(null, new CMSProcessableByteArray(signerInformation.getSignature()), false, provider).getSignerInfos();
    }
    
    @Deprecated
    public SignerInformationStore generateCounterSigners(final SignerInformation signerInformation, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(null, new CMSProcessableByteArray(signerInformation.getSignature()), false, CMSUtils.getProvider(s)).getSignerInfos();
    }
    
    public SignerInformationStore generateCounterSigners(final SignerInformation signerInformation) throws CMSException {
        return this.generate(new CMSProcessableByteArray(null, signerInformation.getSignature()), false).getSignerInfos();
    }
    
    private class SignerInf
    {
        final PrivateKey key;
        final Object signerIdentifier;
        final String digestOID;
        final String encOID;
        final CMSAttributeTableGenerator sAttr;
        final CMSAttributeTableGenerator unsAttr;
        final AttributeTable baseSignedTable;
        
        SignerInf(final PrivateKey key, final Object signerIdentifier, final String digestOID, final String encOID, final CMSAttributeTableGenerator sAttr, final CMSAttributeTableGenerator unsAttr, final AttributeTable baseSignedTable) {
            this.key = key;
            this.signerIdentifier = signerIdentifier;
            this.digestOID = digestOID;
            this.encOID = encOID;
            this.sAttr = sAttr;
            this.unsAttr = unsAttr;
            this.baseSignedTable = baseSignedTable;
        }
        
        SignerInfoGenerator toSignerInfoGenerator(final SecureRandom secureRandom, final Provider provider, final boolean b) throws IOException, CertificateEncodingException, CMSException, OperatorCreationException, NoSuchAlgorithmException {
            final String string = CMSSignedHelper.INSTANCE.getDigestAlgName(this.digestOID) + "with" + CMSSignedHelper.INSTANCE.getEncryptionAlgName(this.encOID);
            final JcaSignerInfoGeneratorBuilder jcaSignerInfoGeneratorBuilder = new JcaSignerInfoGeneratorBuilder(new BcDigestCalculatorProvider());
            if (b) {
                jcaSignerInfoGeneratorBuilder.setSignedAttributeGenerator(this.sAttr);
            }
            jcaSignerInfoGeneratorBuilder.setDirectSignature(!b);
            jcaSignerInfoGeneratorBuilder.setUnsignedAttributeGenerator(this.unsAttr);
            JcaContentSignerBuilder setSecureRandom;
            try {
                setSecureRandom = new JcaContentSignerBuilder(string).setSecureRandom(secureRandom);
            }
            catch (IllegalArgumentException ex) {
                throw new NoSuchAlgorithmException(ex.getMessage());
            }
            if (provider != null) {
                setSecureRandom.setProvider(provider);
            }
            final ContentSigner build = setSecureRandom.build(this.key);
            if (this.signerIdentifier instanceof X509Certificate) {
                return jcaSignerInfoGeneratorBuilder.build(build, (X509Certificate)this.signerIdentifier);
            }
            return jcaSignerInfoGeneratorBuilder.build(build, (byte[])this.signerIdentifier);
        }
    }
}
