// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

public class CMSVerifierCertificateNotValidException extends CMSException
{
    public CMSVerifierCertificateNotValidException(final String s) {
        super(s);
    }
}
