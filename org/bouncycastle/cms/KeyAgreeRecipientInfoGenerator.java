// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.cms.OriginatorPublicKey;
import org.bouncycastle.asn1.ASN1Sequence;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.cms.KeyAgreeRecipientInfo;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.cms.OriginatorIdentifierOrKey;
import org.bouncycastle.asn1.cms.RecipientInfo;
import org.bouncycastle.operator.GenericKey;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public abstract class KeyAgreeRecipientInfoGenerator implements RecipientInfoGenerator
{
    private ASN1ObjectIdentifier keyAgreementOID;
    private ASN1ObjectIdentifier keyEncryptionOID;
    private SubjectPublicKeyInfo originatorKeyInfo;
    
    protected KeyAgreeRecipientInfoGenerator(final ASN1ObjectIdentifier keyAgreementOID, final SubjectPublicKeyInfo originatorKeyInfo, final ASN1ObjectIdentifier keyEncryptionOID) {
        this.originatorKeyInfo = originatorKeyInfo;
        this.keyAgreementOID = keyAgreementOID;
        this.keyEncryptionOID = keyEncryptionOID;
    }
    
    public RecipientInfo generate(final GenericKey genericKey) throws CMSException {
        final OriginatorIdentifierOrKey originatorIdentifierOrKey = new OriginatorIdentifierOrKey(this.createOriginatorPublicKey(this.originatorKeyInfo));
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        asn1EncodableVector.add(this.keyEncryptionOID);
        asn1EncodableVector.add(DERNull.INSTANCE);
        final AlgorithmIdentifier algorithmIdentifier = new AlgorithmIdentifier(this.keyEncryptionOID, DERNull.INSTANCE);
        final AlgorithmIdentifier algorithmIdentifier2 = new AlgorithmIdentifier(this.keyAgreementOID, algorithmIdentifier);
        final ASN1Sequence generateRecipientEncryptedKeys = this.generateRecipientEncryptedKeys(algorithmIdentifier2, algorithmIdentifier, genericKey);
        final ASN1Encodable userKeyingMaterial = this.getUserKeyingMaterial(algorithmIdentifier2);
        if (userKeyingMaterial != null) {
            try {
                return new RecipientInfo(new KeyAgreeRecipientInfo(originatorIdentifierOrKey, new DEROctetString(userKeyingMaterial), algorithmIdentifier2, generateRecipientEncryptedKeys));
            }
            catch (IOException ex) {
                throw new CMSException("unable to encode userKeyingMaterial: " + ex.getMessage(), ex);
            }
        }
        return new RecipientInfo(new KeyAgreeRecipientInfo(originatorIdentifierOrKey, null, algorithmIdentifier2, generateRecipientEncryptedKeys));
    }
    
    protected OriginatorPublicKey createOriginatorPublicKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        return new OriginatorPublicKey(new AlgorithmIdentifier(subjectPublicKeyInfo.getAlgorithm().getAlgorithm(), DERNull.INSTANCE), subjectPublicKeyInfo.getPublicKeyData().getBytes());
    }
    
    protected abstract ASN1Sequence generateRecipientEncryptedKeys(final AlgorithmIdentifier p0, final AlgorithmIdentifier p1, final GenericKey p2) throws CMSException;
    
    protected abstract ASN1Encodable getUserKeyingMaterial(final AlgorithmIdentifier p0) throws CMSException;
}
