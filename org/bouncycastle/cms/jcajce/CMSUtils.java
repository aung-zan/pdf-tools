// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms.jcajce;

import java.security.Provider;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import java.security.cert.CertificateEncodingException;
import org.bouncycastle.asn1.x509.TBSCertificateStructure;
import java.security.cert.X509Certificate;

class CMSUtils
{
    static TBSCertificateStructure getTBSCertificateStructure(final X509Certificate x509Certificate) throws CertificateEncodingException {
        return TBSCertificateStructure.getInstance(x509Certificate.getTBSCertificate());
    }
    
    static IssuerAndSerialNumber getIssuerAndSerialNumber(final X509Certificate x509Certificate) throws CertificateEncodingException {
        return new IssuerAndSerialNumber(Certificate.getInstance(x509Certificate.getEncoded()).getIssuer(), x509Certificate.getSerialNumber());
    }
    
    static byte[] getSubjectKeyId(final X509Certificate x509Certificate) {
        final byte[] extensionValue = x509Certificate.getExtensionValue(X509Extension.subjectKeyIdentifier.getId());
        if (extensionValue != null) {
            return ASN1OctetString.getInstance(ASN1OctetString.getInstance(extensionValue).getOctets()).getOctets();
        }
        return null;
    }
    
    static EnvelopedDataHelper createContentHelper(final Provider provider) {
        if (provider != null) {
            return new EnvelopedDataHelper(new ProviderJcaJceExtHelper(provider));
        }
        return new EnvelopedDataHelper(new DefaultJcaJceExtHelper());
    }
    
    static EnvelopedDataHelper createContentHelper(final String s) {
        if (s != null) {
            return new EnvelopedDataHelper(new NamedJcaJceExtHelper(s));
        }
        return new EnvelopedDataHelper(new DefaultJcaJceExtHelper());
    }
}
