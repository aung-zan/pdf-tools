// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms.jcajce;

import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import javax.crypto.NoSuchPaddingException;
import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import javax.crypto.Cipher;
import java.io.IOException;
import java.security.GeneralSecurityException;
import org.bouncycastle.cms.CMSException;
import javax.crypto.KeyAgreement;
import java.security.Key;
import org.bouncycastle.jce.spec.MQVPrivateKeySpec;
import org.bouncycastle.jce.spec.MQVPublicKeySpec;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.cms.ecc.MQVuserKeyingMaterial;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.cms.CMSEnvelopedGenerator;
import javax.crypto.SecretKey;
import org.bouncycastle.asn1.ASN1OctetString;
import java.security.PublicKey;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.security.Provider;
import java.security.PrivateKey;
import org.bouncycastle.cms.KeyAgreeRecipient;

public abstract class JceKeyAgreeRecipient implements KeyAgreeRecipient
{
    private PrivateKey recipientKey;
    protected EnvelopedDataHelper helper;
    protected EnvelopedDataHelper contentHelper;
    
    public JceKeyAgreeRecipient(final PrivateKey recipientKey) {
        this.helper = new EnvelopedDataHelper(new DefaultJcaJceExtHelper());
        this.contentHelper = this.helper;
        this.recipientKey = recipientKey;
    }
    
    public JceKeyAgreeRecipient setProvider(final Provider provider) {
        this.helper = new EnvelopedDataHelper(new ProviderJcaJceExtHelper(provider));
        this.contentHelper = this.helper;
        return this;
    }
    
    public JceKeyAgreeRecipient setProvider(final String s) {
        this.helper = new EnvelopedDataHelper(new NamedJcaJceExtHelper(s));
        this.contentHelper = this.helper;
        return this;
    }
    
    public JceKeyAgreeRecipient setContentProvider(final Provider provider) {
        this.contentHelper = CMSUtils.createContentHelper(provider);
        return this;
    }
    
    public JceKeyAgreeRecipient setContentProvider(final String s) {
        this.contentHelper = CMSUtils.createContentHelper(s);
        return this;
    }
    
    private SecretKey calculateAgreedWrapKey(final AlgorithmIdentifier algorithmIdentifier, final ASN1ObjectIdentifier asn1ObjectIdentifier, PublicKey key, final ASN1OctetString asn1OctetString, PrivateKey key2) throws CMSException, GeneralSecurityException, IOException {
        if (algorithmIdentifier.getAlgorithm().getId().equals(CMSEnvelopedGenerator.ECMQV_SHA1KDF)) {
            key = new MQVPublicKeySpec(key, this.helper.createKeyFactory(algorithmIdentifier.getAlgorithm()).generatePublic(new X509EncodedKeySpec(new SubjectPublicKeyInfo(this.getPrivateKeyAlgorithmIdentifier(), MQVuserKeyingMaterial.getInstance(ASN1Primitive.fromByteArray(asn1OctetString.getOctets())).getEphemeralPublicKey().getPublicKey().getBytes()).getEncoded())));
            key2 = new MQVPrivateKeySpec(key2, key2);
        }
        final KeyAgreement keyAgreement = this.helper.createKeyAgreement(algorithmIdentifier.getAlgorithm());
        keyAgreement.init(key2);
        keyAgreement.doPhase(key, true);
        return keyAgreement.generateSecret(asn1ObjectIdentifier.getId());
    }
    
    private Key unwrapSessionKey(final ASN1ObjectIdentifier asn1ObjectIdentifier, final SecretKey key, final ASN1ObjectIdentifier asn1ObjectIdentifier2, final byte[] wrappedKey) throws CMSException, InvalidKeyException, NoSuchAlgorithmException {
        final Cipher cipher = this.helper.createCipher(asn1ObjectIdentifier);
        cipher.init(4, key);
        return cipher.unwrap(wrappedKey, this.helper.getBaseCipherName(asn1ObjectIdentifier2), 3);
    }
    
    protected Key extractSecretKey(final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final SubjectPublicKeyInfo subjectPublicKeyInfo, final ASN1OctetString asn1OctetString, final byte[] array) throws CMSException {
        try {
            final ASN1ObjectIdentifier algorithm = AlgorithmIdentifier.getInstance(algorithmIdentifier.getParameters()).getAlgorithm();
            return this.unwrapSessionKey(algorithm, this.calculateAgreedWrapKey(algorithmIdentifier, algorithm, this.helper.createKeyFactory(algorithmIdentifier.getAlgorithm()).generatePublic(new X509EncodedKeySpec(subjectPublicKeyInfo.getEncoded())), asn1OctetString, this.recipientKey), algorithmIdentifier2.getAlgorithm(), array);
        }
        catch (NoSuchAlgorithmException ex) {
            throw new CMSException("can't find algorithm.", ex);
        }
        catch (InvalidKeyException ex2) {
            throw new CMSException("key invalid in message.", ex2);
        }
        catch (InvalidKeySpecException ex3) {
            throw new CMSException("originator key spec invalid.", ex3);
        }
        catch (NoSuchPaddingException ex4) {
            throw new CMSException("required padding not supported.", ex4);
        }
        catch (Exception ex5) {
            throw new CMSException("originator key invalid.", ex5);
        }
    }
    
    public AlgorithmIdentifier getPrivateKeyAlgorithmIdentifier() {
        return PrivateKeyInfo.getInstance(this.recipientKey.getEncoded()).getPrivateKeyAlgorithm();
    }
}
