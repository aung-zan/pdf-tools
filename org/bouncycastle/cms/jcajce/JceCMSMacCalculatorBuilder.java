// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms.jcajce;

import java.security.GeneralSecurityException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.security.spec.AlgorithmParameterSpec;
import org.bouncycastle.operator.jcajce.JceGenericKey;
import org.bouncycastle.operator.GenericKey;
import org.bouncycastle.jcajce.io.MacOutputStream;
import java.io.OutputStream;
import javax.crypto.KeyGenerator;
import java.security.Key;
import javax.crypto.Mac;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import javax.crypto.SecretKey;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.MacCalculator;
import java.security.Provider;
import java.security.SecureRandom;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public class JceCMSMacCalculatorBuilder
{
    private final ASN1ObjectIdentifier macOID;
    private final int keySize;
    private EnvelopedDataHelper helper;
    private SecureRandom random;
    
    public JceCMSMacCalculatorBuilder(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        this(asn1ObjectIdentifier, -1);
    }
    
    public JceCMSMacCalculatorBuilder(final ASN1ObjectIdentifier macOID, final int keySize) {
        this.helper = new EnvelopedDataHelper(new DefaultJcaJceExtHelper());
        this.macOID = macOID;
        this.keySize = keySize;
    }
    
    public JceCMSMacCalculatorBuilder setProvider(final Provider provider) {
        this.helper = new EnvelopedDataHelper(new ProviderJcaJceExtHelper(provider));
        return this;
    }
    
    public JceCMSMacCalculatorBuilder setProvider(final String s) {
        this.helper = new EnvelopedDataHelper(new NamedJcaJceExtHelper(s));
        return this;
    }
    
    public JceCMSMacCalculatorBuilder setSecureRandom(final SecureRandom random) {
        this.random = random;
        return this;
    }
    
    public MacCalculator build() throws CMSException {
        return new CMSMacCalculator(this.macOID, this.keySize, this.random);
    }
    
    private class CMSMacCalculator implements MacCalculator
    {
        private SecretKey encKey;
        private AlgorithmIdentifier algorithmIdentifier;
        private Mac mac;
        private SecureRandom random;
        
        CMSMacCalculator(final ASN1ObjectIdentifier asn1ObjectIdentifier, final int keysize, SecureRandom random) throws CMSException {
            final KeyGenerator keyGenerator = JceCMSMacCalculatorBuilder.this.helper.createKeyGenerator(asn1ObjectIdentifier);
            if (random == null) {
                random = new SecureRandom();
            }
            this.random = random;
            if (keysize < 0) {
                keyGenerator.init(random);
            }
            else {
                keyGenerator.init(keysize, random);
            }
            this.encKey = keyGenerator.generateKey();
            this.algorithmIdentifier = JceCMSMacCalculatorBuilder.this.helper.getAlgorithmIdentifier(asn1ObjectIdentifier, this.generateParameterSpec(asn1ObjectIdentifier, this.encKey));
            this.mac = JceCMSMacCalculatorBuilder.this.helper.createContentMac(this.encKey, this.algorithmIdentifier);
        }
        
        public AlgorithmIdentifier getAlgorithmIdentifier() {
            return this.algorithmIdentifier;
        }
        
        public OutputStream getOutputStream() {
            return new MacOutputStream(this.mac);
        }
        
        public byte[] getMac() {
            return this.mac.doFinal();
        }
        
        public GenericKey getKey() {
            return new JceGenericKey(this.algorithmIdentifier, this.encKey);
        }
        
        protected AlgorithmParameterSpec generateParameterSpec(final ASN1ObjectIdentifier asn1ObjectIdentifier, final SecretKey secretKey) throws CMSException {
            try {
                if (asn1ObjectIdentifier.equals(PKCSObjectIdentifiers.RC2_CBC)) {
                    final byte[] array = new byte[8];
                    this.random.nextBytes(array);
                    return new RC2ParameterSpec(secretKey.getEncoded().length * 8, array);
                }
                return JceCMSMacCalculatorBuilder.this.helper.createAlgorithmParameterGenerator(asn1ObjectIdentifier).generateParameters().getParameterSpec(IvParameterSpec.class);
            }
            catch (GeneralSecurityException ex) {
                return null;
            }
        }
    }
}
