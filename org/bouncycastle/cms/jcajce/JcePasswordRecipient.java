// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms.jcajce;

import javax.crypto.Cipher;
import java.security.GeneralSecurityException;
import org.bouncycastle.cms.CMSException;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.asn1.ASN1OctetString;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.security.Provider;
import org.bouncycastle.cms.PasswordRecipient;

public abstract class JcePasswordRecipient implements PasswordRecipient
{
    private int schemeID;
    protected EnvelopedDataHelper helper;
    private char[] password;
    
    JcePasswordRecipient(final char[] password) {
        this.schemeID = 1;
        this.helper = new EnvelopedDataHelper(new DefaultJcaJceExtHelper());
        this.password = password;
    }
    
    public JcePasswordRecipient setPasswordConversionScheme(final int schemeID) {
        this.schemeID = schemeID;
        return this;
    }
    
    public JcePasswordRecipient setProvider(final Provider provider) {
        this.helper = new EnvelopedDataHelper(new ProviderJcaJceExtHelper(provider));
        return this;
    }
    
    public JcePasswordRecipient setProvider(final String s) {
        this.helper = new EnvelopedDataHelper(new NamedJcaJceExtHelper(s));
        return this;
    }
    
    protected Key extractSecretKey(final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final byte[] key, final byte[] wrappedKey) throws CMSException {
        final Cipher rfc3211Wrapper = this.helper.createRFC3211Wrapper(algorithmIdentifier.getAlgorithm());
        try {
            rfc3211Wrapper.init(4, new SecretKeySpec(key, rfc3211Wrapper.getAlgorithm()), new IvParameterSpec(ASN1OctetString.getInstance(algorithmIdentifier.getParameters()).getOctets()));
            return rfc3211Wrapper.unwrap(wrappedKey, algorithmIdentifier2.getAlgorithm().getId(), 3);
        }
        catch (GeneralSecurityException ex) {
            throw new CMSException("cannot process content encryption key: " + ex.getMessage(), ex);
        }
    }
    
    public int getPasswordConversionScheme() {
        return this.schemeID;
    }
    
    public char[] getPassword() {
        return this.password;
    }
}
