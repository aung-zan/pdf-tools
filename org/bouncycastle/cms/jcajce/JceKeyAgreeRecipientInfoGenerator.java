// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms.jcajce;

import java.security.KeyPairGenerator;
import java.security.spec.ECParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.interfaces.ECPublicKey;
import org.bouncycastle.cms.CMSAlgorithm;
import org.bouncycastle.asn1.cms.ecc.MQVuserKeyingMaterial;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.KeyAgreement;
import org.bouncycastle.asn1.DERSequence;
import java.security.GeneralSecurityException;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.cms.RecipientEncryptedKey;
import org.bouncycastle.asn1.DEROctetString;
import java.security.Key;
import org.bouncycastle.jce.spec.MQVPublicKeySpec;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.jce.spec.MQVPrivateKeySpec;
import org.bouncycastle.cms.CMSEnvelopedGenerator;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.operator.GenericKey;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.RecipientKeyIdentifier;
import java.security.cert.CertificateEncodingException;
import org.bouncycastle.asn1.cms.KeyAgreeRecipientIdentifier;
import java.security.cert.X509Certificate;
import java.security.Provider;
import java.util.ArrayList;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.KeyPair;
import java.security.SecureRandom;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;
import org.bouncycastle.cms.KeyAgreeRecipientInfoGenerator;

public class JceKeyAgreeRecipientInfoGenerator extends KeyAgreeRecipientInfoGenerator
{
    private List recipientIDs;
    private List recipientKeys;
    private PublicKey senderPublicKey;
    private PrivateKey senderPrivateKey;
    private EnvelopedDataHelper helper;
    private SecureRandom random;
    private KeyPair ephemeralKP;
    
    public JceKeyAgreeRecipientInfoGenerator(final ASN1ObjectIdentifier asn1ObjectIdentifier, final PrivateKey senderPrivateKey, final PublicKey senderPublicKey, final ASN1ObjectIdentifier asn1ObjectIdentifier2) {
        super(asn1ObjectIdentifier, SubjectPublicKeyInfo.getInstance(senderPublicKey.getEncoded()), asn1ObjectIdentifier2);
        this.recipientIDs = new ArrayList();
        this.recipientKeys = new ArrayList();
        this.helper = new EnvelopedDataHelper(new DefaultJcaJceExtHelper());
        this.senderPublicKey = senderPublicKey;
        this.senderPrivateKey = senderPrivateKey;
    }
    
    public JceKeyAgreeRecipientInfoGenerator setProvider(final Provider provider) {
        this.helper = new EnvelopedDataHelper(new ProviderJcaJceExtHelper(provider));
        return this;
    }
    
    public JceKeyAgreeRecipientInfoGenerator setProvider(final String s) {
        this.helper = new EnvelopedDataHelper(new NamedJcaJceExtHelper(s));
        return this;
    }
    
    public JceKeyAgreeRecipientInfoGenerator setSecureRandom(final SecureRandom random) {
        this.random = random;
        return this;
    }
    
    public JceKeyAgreeRecipientInfoGenerator addRecipient(final X509Certificate x509Certificate) throws CertificateEncodingException {
        this.recipientIDs.add(new KeyAgreeRecipientIdentifier(CMSUtils.getIssuerAndSerialNumber(x509Certificate)));
        this.recipientKeys.add(x509Certificate.getPublicKey());
        return this;
    }
    
    public JceKeyAgreeRecipientInfoGenerator addRecipient(final byte[] array, final PublicKey publicKey) throws CertificateEncodingException {
        this.recipientIDs.add(new KeyAgreeRecipientIdentifier(new RecipientKeyIdentifier(array)));
        this.recipientKeys.add(publicKey);
        return this;
    }
    
    public ASN1Sequence generateRecipientEncryptedKeys(final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final GenericKey genericKey) throws CMSException {
        this.init(algorithmIdentifier.getAlgorithm());
        PrivateKey senderPrivateKey = this.senderPrivateKey;
        final ASN1ObjectIdentifier algorithm = algorithmIdentifier.getAlgorithm();
        if (algorithm.getId().equals(CMSEnvelopedGenerator.ECMQV_SHA1KDF)) {
            senderPrivateKey = new MQVPrivateKeySpec(senderPrivateKey, this.ephemeralKP.getPrivate(), this.ephemeralKP.getPublic());
        }
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        for (int i = 0; i != this.recipientIDs.size(); ++i) {
            PublicKey key = this.recipientKeys.get(i);
            final KeyAgreeRecipientIdentifier keyAgreeRecipientIdentifier = this.recipientIDs.get(i);
            if (algorithm.getId().equals(CMSEnvelopedGenerator.ECMQV_SHA1KDF)) {
                key = new MQVPublicKeySpec(key, key);
            }
            try {
                final KeyAgreement keyAgreement = this.helper.createKeyAgreement(algorithm);
                keyAgreement.init(senderPrivateKey, this.random);
                keyAgreement.doPhase(key, true);
                final SecretKey generateSecret = keyAgreement.generateSecret(algorithmIdentifier2.getAlgorithm().getId());
                final Cipher cipher = this.helper.createCipher(algorithmIdentifier2.getAlgorithm());
                cipher.init(3, generateSecret, this.random);
                asn1EncodableVector.add(new RecipientEncryptedKey(keyAgreeRecipientIdentifier, new DEROctetString(cipher.wrap(this.helper.getJceKey(genericKey)))));
            }
            catch (GeneralSecurityException ex) {
                throw new CMSException("cannot perform agreement step: " + ex.getMessage(), ex);
            }
        }
        return new DERSequence(asn1EncodableVector);
    }
    
    @Override
    protected ASN1Encodable getUserKeyingMaterial(final AlgorithmIdentifier algorithmIdentifier) throws CMSException {
        this.init(algorithmIdentifier.getAlgorithm());
        if (this.ephemeralKP != null) {
            return new MQVuserKeyingMaterial(this.createOriginatorPublicKey(SubjectPublicKeyInfo.getInstance(this.ephemeralKP.getPublic().getEncoded())), null);
        }
        return null;
    }
    
    private void init(final ASN1ObjectIdentifier asn1ObjectIdentifier) throws CMSException {
        if (this.random == null) {
            this.random = new SecureRandom();
        }
        if (asn1ObjectIdentifier.equals(CMSAlgorithm.ECMQV_SHA1KDF) && this.ephemeralKP == null) {
            try {
                final ECParameterSpec params = ((ECPublicKey)this.senderPublicKey).getParams();
                final KeyPairGenerator keyPairGenerator = this.helper.createKeyPairGenerator(asn1ObjectIdentifier);
                keyPairGenerator.initialize(params, this.random);
                this.ephemeralKP = keyPairGenerator.generateKeyPair();
            }
            catch (InvalidAlgorithmParameterException obj) {
                throw new CMSException("cannot determine MQV ephemeral key pair parameters from public key: " + obj);
            }
        }
    }
}
