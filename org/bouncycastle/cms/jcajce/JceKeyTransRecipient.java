// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms.jcajce;

import java.util.Iterator;
import org.bouncycastle.operator.jcajce.JceAsymmetricKeyUnwrapper;
import org.bouncycastle.operator.OperatorException;
import org.bouncycastle.cms.CMSException;
import java.security.Key;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.Provider;
import java.util.HashMap;
import java.util.Map;
import java.security.PrivateKey;
import org.bouncycastle.cms.KeyTransRecipient;

public abstract class JceKeyTransRecipient implements KeyTransRecipient
{
    private PrivateKey recipientKey;
    protected EnvelopedDataHelper helper;
    protected EnvelopedDataHelper contentHelper;
    protected Map extraMappings;
    
    public JceKeyTransRecipient(final PrivateKey recipientKey) {
        this.helper = new EnvelopedDataHelper(new DefaultJcaJceExtHelper());
        this.contentHelper = this.helper;
        this.extraMappings = new HashMap();
        this.recipientKey = recipientKey;
    }
    
    public JceKeyTransRecipient setProvider(final Provider provider) {
        this.helper = new EnvelopedDataHelper(new ProviderJcaJceExtHelper(provider));
        this.contentHelper = this.helper;
        return this;
    }
    
    public JceKeyTransRecipient setProvider(final String s) {
        this.helper = new EnvelopedDataHelper(new NamedJcaJceExtHelper(s));
        this.contentHelper = this.helper;
        return this;
    }
    
    public JceKeyTransRecipient setAlgorithmMapping(final ASN1ObjectIdentifier asn1ObjectIdentifier, final String s) {
        this.extraMappings.put(asn1ObjectIdentifier, s);
        return this;
    }
    
    public JceKeyTransRecipient setContentProvider(final Provider provider) {
        this.contentHelper = CMSUtils.createContentHelper(provider);
        return this;
    }
    
    public JceKeyTransRecipient setContentProvider(final String s) {
        this.contentHelper = CMSUtils.createContentHelper(s);
        return this;
    }
    
    protected Key extractSecretKey(final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final byte[] array) throws CMSException {
        final JceAsymmetricKeyUnwrapper asymmetricUnwrapper = this.helper.createAsymmetricUnwrapper(algorithmIdentifier, this.recipientKey);
        if (!this.extraMappings.isEmpty()) {
            for (final ASN1ObjectIdentifier asn1ObjectIdentifier : this.extraMappings.keySet()) {
                asymmetricUnwrapper.setAlgorithmMapping(asn1ObjectIdentifier, (String)this.extraMappings.get(asn1ObjectIdentifier));
            }
        }
        try {
            return this.helper.getJceKey(algorithmIdentifier2.getAlgorithm(), asymmetricUnwrapper.generateUnwrappedKey(algorithmIdentifier2, array));
        }
        catch (OperatorException ex) {
            throw new CMSException("exception unwrapping key: " + ex.getMessage(), ex);
        }
    }
}
