// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms.jcajce;

import org.bouncycastle.operator.jcajce.JceGenericKey;
import org.bouncycastle.operator.GenericKey;
import javax.crypto.CipherOutputStream;
import java.io.OutputStream;
import java.security.AlgorithmParameters;
import javax.crypto.KeyGenerator;
import java.security.GeneralSecurityException;
import java.security.Key;
import javax.crypto.Cipher;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import javax.crypto.SecretKey;
import org.bouncycastle.util.Integers;
import org.bouncycastle.cms.CMSAlgorithm;
import java.util.HashMap;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OutputEncryptor;
import java.security.Provider;
import java.security.SecureRandom;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.Map;

public class JceCMSContentEncryptorBuilder
{
    private static Map keySizes;
    private final ASN1ObjectIdentifier encryptionOID;
    private final int keySize;
    private EnvelopedDataHelper helper;
    private SecureRandom random;
    
    private static int getKeySize(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        final Integer n = JceCMSContentEncryptorBuilder.keySizes.get(asn1ObjectIdentifier);
        if (n != null) {
            return n;
        }
        return -1;
    }
    
    public JceCMSContentEncryptorBuilder(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        this(asn1ObjectIdentifier, getKeySize(asn1ObjectIdentifier));
    }
    
    public JceCMSContentEncryptorBuilder(final ASN1ObjectIdentifier encryptionOID, final int keySize) {
        this.helper = new EnvelopedDataHelper(new DefaultJcaJceExtHelper());
        this.encryptionOID = encryptionOID;
        this.keySize = keySize;
    }
    
    public JceCMSContentEncryptorBuilder setProvider(final Provider provider) {
        this.helper = new EnvelopedDataHelper(new ProviderJcaJceExtHelper(provider));
        return this;
    }
    
    public JceCMSContentEncryptorBuilder setProvider(final String s) {
        this.helper = new EnvelopedDataHelper(new NamedJcaJceExtHelper(s));
        return this;
    }
    
    public JceCMSContentEncryptorBuilder setSecureRandom(final SecureRandom random) {
        this.random = random;
        return this;
    }
    
    public OutputEncryptor build() throws CMSException {
        return new CMSOutputEncryptor(this.encryptionOID, this.keySize, this.random);
    }
    
    static {
        (JceCMSContentEncryptorBuilder.keySizes = new HashMap()).put(CMSAlgorithm.AES128_CBC, Integers.valueOf(128));
        JceCMSContentEncryptorBuilder.keySizes.put(CMSAlgorithm.AES192_CBC, Integers.valueOf(192));
        JceCMSContentEncryptorBuilder.keySizes.put(CMSAlgorithm.AES256_CBC, Integers.valueOf(256));
        JceCMSContentEncryptorBuilder.keySizes.put(CMSAlgorithm.CAMELLIA128_CBC, Integers.valueOf(128));
        JceCMSContentEncryptorBuilder.keySizes.put(CMSAlgorithm.CAMELLIA192_CBC, Integers.valueOf(192));
        JceCMSContentEncryptorBuilder.keySizes.put(CMSAlgorithm.CAMELLIA256_CBC, Integers.valueOf(256));
    }
    
    private class CMSOutputEncryptor implements OutputEncryptor
    {
        private SecretKey encKey;
        private AlgorithmIdentifier algorithmIdentifier;
        private Cipher cipher;
        
        CMSOutputEncryptor(final ASN1ObjectIdentifier asn1ObjectIdentifier, final int keysize, SecureRandom random) throws CMSException {
            final KeyGenerator keyGenerator = JceCMSContentEncryptorBuilder.this.helper.createKeyGenerator(asn1ObjectIdentifier);
            if (random == null) {
                random = new SecureRandom();
            }
            if (keysize < 0) {
                keyGenerator.init(random);
            }
            else {
                keyGenerator.init(keysize, random);
            }
            this.cipher = JceCMSContentEncryptorBuilder.this.helper.createCipher(asn1ObjectIdentifier);
            this.encKey = keyGenerator.generateKey();
            AlgorithmParameters params = JceCMSContentEncryptorBuilder.this.helper.generateParameters(asn1ObjectIdentifier, this.encKey, random);
            try {
                this.cipher.init(1, this.encKey, params, random);
            }
            catch (GeneralSecurityException ex) {
                throw new CMSException("unable to initialize cipher: " + ex.getMessage(), ex);
            }
            if (params == null) {
                params = this.cipher.getParameters();
            }
            this.algorithmIdentifier = JceCMSContentEncryptorBuilder.this.helper.getAlgorithmIdentifier(asn1ObjectIdentifier, params);
        }
        
        public AlgorithmIdentifier getAlgorithmIdentifier() {
            return this.algorithmIdentifier;
        }
        
        public OutputStream getOutputStream(final OutputStream os) {
            return new CipherOutputStream(os, this.cipher);
        }
        
        public GenericKey getKey() {
            return new JceGenericKey(this.algorithmIdentifier, this.encKey);
        }
    }
}
