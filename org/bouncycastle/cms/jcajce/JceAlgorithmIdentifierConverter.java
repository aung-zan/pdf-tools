// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms.jcajce;

import org.bouncycastle.asn1.ASN1Encodable;
import java.security.NoSuchProviderException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.cms.CMSException;
import java.security.AlgorithmParameters;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.security.Provider;
import java.security.SecureRandom;

public class JceAlgorithmIdentifierConverter
{
    private EnvelopedDataHelper helper;
    private SecureRandom random;
    
    public JceAlgorithmIdentifierConverter() {
        this.helper = new EnvelopedDataHelper(new DefaultJcaJceExtHelper());
    }
    
    public JceAlgorithmIdentifierConverter setProvider(final Provider provider) {
        this.helper = new EnvelopedDataHelper(new ProviderJcaJceExtHelper(provider));
        return this;
    }
    
    public JceAlgorithmIdentifierConverter setProvider(final String s) {
        this.helper = new EnvelopedDataHelper(new NamedJcaJceExtHelper(s));
        return this;
    }
    
    public AlgorithmParameters getAlgorithmParameters(final AlgorithmIdentifier algorithmIdentifier) throws CMSException {
        final ASN1Encodable parameters = algorithmIdentifier.getParameters();
        if (parameters == null) {
            return null;
        }
        try {
            final AlgorithmParameters algorithmParameters = this.helper.createAlgorithmParameters(algorithmIdentifier.getAlgorithm());
            algorithmParameters.init(parameters.toASN1Primitive().getEncoded(), "ASN.1");
            return algorithmParameters;
        }
        catch (NoSuchAlgorithmException ex) {
            throw new CMSException("can't find parameters for algorithm", ex);
        }
        catch (IOException ex2) {
            throw new CMSException("can't parse parameters", ex2);
        }
        catch (NoSuchProviderException ex3) {
            throw new CMSException("can't find provider for algorithm", ex3);
        }
    }
}
