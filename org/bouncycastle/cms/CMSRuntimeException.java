// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

public class CMSRuntimeException extends RuntimeException
{
    Exception e;
    
    public CMSRuntimeException(final String message) {
        super(message);
    }
    
    public CMSRuntimeException(final String message, final Exception e) {
        super(message);
        this.e = e;
    }
    
    public Exception getUnderlyingException() {
        return this.e;
    }
    
    @Override
    public Throwable getCause() {
        return this.e;
    }
}
