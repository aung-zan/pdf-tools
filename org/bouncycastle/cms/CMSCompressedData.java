// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.operator.InputExpanderProvider;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.io.IOException;
import java.util.zip.InflaterInputStream;
import org.bouncycastle.asn1.ASN1OctetString;
import java.io.InputStream;
import org.bouncycastle.asn1.cms.CompressedData;
import org.bouncycastle.asn1.cms.ContentInfo;

public class CMSCompressedData
{
    ContentInfo contentInfo;
    CompressedData comData;
    
    public CMSCompressedData(final byte[] array) throws CMSException {
        this(CMSUtils.readContentInfo(array));
    }
    
    public CMSCompressedData(final InputStream inputStream) throws CMSException {
        this(CMSUtils.readContentInfo(inputStream));
    }
    
    public CMSCompressedData(final ContentInfo contentInfo) throws CMSException {
        this.contentInfo = contentInfo;
        try {
            this.comData = CompressedData.getInstance(contentInfo.getContent());
        }
        catch (ClassCastException ex) {
            throw new CMSException("Malformed content.", ex);
        }
        catch (IllegalArgumentException ex2) {
            throw new CMSException("Malformed content.", ex2);
        }
    }
    
    @Deprecated
    public byte[] getContent() throws CMSException {
        final InflaterInputStream inflaterInputStream = new InflaterInputStream(((ASN1OctetString)this.comData.getEncapContentInfo().getContent()).getOctetStream());
        try {
            return CMSUtils.streamToByteArray(inflaterInputStream);
        }
        catch (IOException ex) {
            throw new CMSException("exception reading compressed stream.", ex);
        }
    }
    
    @Deprecated
    public byte[] getContent(final int n) throws CMSException {
        final InflaterInputStream inflaterInputStream = new InflaterInputStream(((ASN1OctetString)this.comData.getEncapContentInfo().getContent()).getOctetStream());
        try {
            return CMSUtils.streamToByteArray(inflaterInputStream, n);
        }
        catch (IOException ex) {
            throw new CMSException("exception reading compressed stream.", ex);
        }
    }
    
    public ASN1ObjectIdentifier getContentType() {
        return this.contentInfo.getContentType();
    }
    
    public byte[] getContent(final InputExpanderProvider inputExpanderProvider) throws CMSException {
        final InputStream inputStream = inputExpanderProvider.get(this.comData.getCompressionAlgorithmIdentifier()).getInputStream(((ASN1OctetString)this.comData.getEncapContentInfo().getContent()).getOctetStream());
        try {
            return CMSUtils.streamToByteArray(inputStream);
        }
        catch (IOException ex) {
            throw new CMSException("exception reading compressed stream.", ex);
        }
    }
    
    @Deprecated
    public ContentInfo getContentInfo() {
        return this.contentInfo;
    }
    
    public ContentInfo toASN1Structure() {
        return this.contentInfo;
    }
    
    public byte[] getEncoded() throws IOException {
        return this.contentInfo.getEncoded();
    }
}
