// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.x500.X500Name;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collection;
import java.util.Map;
import java.util.List;

public class RecipientInformationStore
{
    private final List all;
    private final Map table;
    
    public RecipientInformationStore(final Collection c) {
        this.table = new HashMap();
        for (final RecipientInformation recipientInformation : c) {
            final RecipientId rid = recipientInformation.getRID();
            ArrayList<RecipientInformation> list = this.table.get(rid);
            if (list == null) {
                list = new ArrayList<RecipientInformation>(1);
                this.table.put(rid, list);
            }
            list.add(recipientInformation);
        }
        this.all = new ArrayList(c);
    }
    
    public RecipientInformation get(final RecipientId recipientId) {
        final Collection recipients = this.getRecipients(recipientId);
        return (recipients.size() == 0) ? null : recipients.iterator().next();
    }
    
    public int size() {
        return this.all.size();
    }
    
    public Collection getRecipients() {
        return new ArrayList(this.all);
    }
    
    public Collection getRecipients(final RecipientId recipientId) {
        if (recipientId instanceof KeyTransRecipientId) {
            final KeyTransRecipientId keyTransRecipientId = (KeyTransRecipientId)recipientId;
            final X500Name issuer = keyTransRecipientId.getIssuer();
            final byte[] subjectKeyIdentifier = keyTransRecipientId.getSubjectKeyIdentifier();
            if (issuer != null && subjectKeyIdentifier != null) {
                final ArrayList list = new ArrayList();
                final Collection recipients = this.getRecipients(new KeyTransRecipientId(issuer, keyTransRecipientId.getSerialNumber()));
                if (recipients != null) {
                    list.addAll(recipients);
                }
                final Collection recipients2 = this.getRecipients(new KeyTransRecipientId(subjectKeyIdentifier));
                if (recipients2 != null) {
                    list.addAll(recipients2);
                }
                return list;
            }
        }
        final ArrayList c = this.table.get(recipientId);
        return (c == null) ? new ArrayList() : new ArrayList(c);
    }
}
