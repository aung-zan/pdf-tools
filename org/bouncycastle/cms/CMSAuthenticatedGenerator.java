// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.util.HashMap;
import java.util.Map;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.SecureRandom;

public class CMSAuthenticatedGenerator extends CMSEnvelopedGenerator
{
    protected CMSAttributeTableGenerator authGen;
    protected CMSAttributeTableGenerator unauthGen;
    
    public CMSAuthenticatedGenerator() {
    }
    
    public CMSAuthenticatedGenerator(final SecureRandom secureRandom) {
        super(secureRandom);
    }
    
    public void setAuthenticatedAttributeGenerator(final CMSAttributeTableGenerator authGen) {
        this.authGen = authGen;
    }
    
    public void setUnauthenticatedAttributeGenerator(final CMSAttributeTableGenerator unauthGen) {
        this.unauthGen = unauthGen;
    }
    
    protected Map getBaseParameters(final ASN1ObjectIdentifier asn1ObjectIdentifier, final AlgorithmIdentifier algorithmIdentifier, final byte[] array) {
        final HashMap<String, ASN1ObjectIdentifier> hashMap = new HashMap<String, ASN1ObjectIdentifier>();
        hashMap.put("contentType", asn1ObjectIdentifier);
        hashMap.put("digestAlgID", (ASN1ObjectIdentifier)algorithmIdentifier);
        hashMap.put("digest", (ASN1ObjectIdentifier)(Object)array.clone());
        return hashMap;
    }
}
