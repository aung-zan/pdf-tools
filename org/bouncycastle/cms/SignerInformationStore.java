// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.util.Iterator;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.List;

public class SignerInformationStore
{
    private List all;
    private Map table;
    
    public SignerInformationStore(final Collection c) {
        this.all = new ArrayList();
        this.table = new HashMap();
        for (final SignerInformation signerInformation : c) {
            final SignerId sid = signerInformation.getSID();
            ArrayList<SignerInformation> list = this.table.get(sid);
            if (list == null) {
                list = new ArrayList<SignerInformation>(1);
                this.table.put(sid, list);
            }
            list.add(signerInformation);
        }
        this.all = new ArrayList(c);
    }
    
    public SignerInformation get(final SignerId signerId) {
        final Collection signers = this.getSigners(signerId);
        return (signers.size() == 0) ? null : signers.iterator().next();
    }
    
    public int size() {
        return this.all.size();
    }
    
    public Collection getSigners() {
        return new ArrayList(this.all);
    }
    
    public Collection getSigners(final SignerId signerId) {
        if (signerId.getIssuer() != null && signerId.getSubjectKeyIdentifier() != null) {
            final ArrayList list = new ArrayList();
            final Collection signers = this.getSigners(new SignerId(signerId.getIssuer(), signerId.getSerialNumber()));
            if (signers != null) {
                list.addAll(signers);
            }
            final Collection signers2 = this.getSigners(new SignerId(signerId.getSubjectKeyIdentifier()));
            if (signers2 != null) {
                list.addAll(signers2);
            }
            return list;
        }
        final ArrayList c = this.table.get(signerId);
        return (c == null) ? new ArrayList() : new ArrayList(c);
    }
}
