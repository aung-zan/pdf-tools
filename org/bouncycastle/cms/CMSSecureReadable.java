// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.io.IOException;
import java.io.InputStream;

interface CMSSecureReadable
{
    InputStream getInputStream() throws IOException, CMSException;
}
