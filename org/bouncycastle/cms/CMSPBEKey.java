// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.spec.InvalidParameterSpecException;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.spec.PBEParameterSpec;
import java.security.AlgorithmParameters;
import javax.crypto.interfaces.PBEKey;

public abstract class CMSPBEKey implements PBEKey
{
    private char[] password;
    private byte[] salt;
    private int iterationCount;
    
    protected static PBEParameterSpec getParamSpec(final AlgorithmParameters algorithmParameters) throws InvalidAlgorithmParameterException {
        try {
            return algorithmParameters.getParameterSpec(PBEParameterSpec.class);
        }
        catch (InvalidParameterSpecException ex) {
            throw new InvalidAlgorithmParameterException("cannot process PBE spec: " + ex.getMessage());
        }
    }
    
    public CMSPBEKey(final char[] password, final byte[] salt, final int iterationCount) {
        this.password = password;
        this.salt = salt;
        this.iterationCount = iterationCount;
    }
    
    public CMSPBEKey(final char[] array, final PBEParameterSpec pbeParameterSpec) {
        this(array, pbeParameterSpec.getSalt(), pbeParameterSpec.getIterationCount());
    }
    
    public char[] getPassword() {
        return this.password;
    }
    
    public byte[] getSalt() {
        return this.salt;
    }
    
    public int getIterationCount() {
        return this.iterationCount;
    }
    
    public String getAlgorithm() {
        return "PKCS5S2";
    }
    
    public String getFormat() {
        return "RAW";
    }
    
    public byte[] getEncoded() {
        return null;
    }
    
    abstract byte[] getEncoded(final String p0);
}
