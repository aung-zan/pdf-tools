// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cms.CMSAttributes;
import java.util.Map;
import org.bouncycastle.asn1.cms.AttributeTable;
import java.util.Hashtable;

public class DefaultAuthenticatedAttributeTableGenerator implements CMSAttributeTableGenerator
{
    private final Hashtable table;
    
    public DefaultAuthenticatedAttributeTableGenerator() {
        this.table = new Hashtable();
    }
    
    public DefaultAuthenticatedAttributeTableGenerator(final AttributeTable attributeTable) {
        if (attributeTable != null) {
            this.table = attributeTable.toHashtable();
        }
        else {
            this.table = new Hashtable();
        }
    }
    
    protected Hashtable createStandardAttributeTable(final Map map) {
        final Hashtable hashtable = (Hashtable)this.table.clone();
        if (!hashtable.containsKey(CMSAttributes.contentType)) {
            final Attribute value = new Attribute(CMSAttributes.contentType, new DERSet(DERObjectIdentifier.getInstance(map.get("contentType"))));
            hashtable.put(value.getAttrType(), value);
        }
        if (!hashtable.containsKey(CMSAttributes.messageDigest)) {
            final Attribute value2 = new Attribute(CMSAttributes.messageDigest, new DERSet(new DEROctetString(map.get("digest"))));
            hashtable.put(value2.getAttrType(), value2);
        }
        return hashtable;
    }
    
    public AttributeTable getAttributes(final Map map) {
        return new AttributeTable(this.createStandardAttributeTable(map));
    }
}
