// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

public interface PasswordRecipient extends Recipient
{
    public static final int PKCS5_SCHEME2 = 0;
    public static final int PKCS5_SCHEME2_UTF8 = 1;
    
    RecipientOperator getRecipientOperator(final AlgorithmIdentifier p0, final AlgorithmIdentifier p1, final byte[] p2, final byte[] p3) throws CMSException;
    
    int getPasswordConversionScheme();
    
    char[] getPassword();
}
