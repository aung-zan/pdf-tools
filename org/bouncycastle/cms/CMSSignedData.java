// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.util.List;
import java.security.cert.CertStoreException;
import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.util.Iterator;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.util.Store;
import org.bouncycastle.cert.jcajce.JcaCertStoreBuilder;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertStore;
import java.security.Provider;
import java.security.NoSuchProviderException;
import org.bouncycastle.x509.NoSuchStoreException;
import org.bouncycastle.asn1.ASN1Set;
import java.util.Collection;
import org.bouncycastle.asn1.cms.SignerInfo;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import java.util.ArrayList;
import org.bouncycastle.asn1.ASN1OctetString;
import java.io.OutputStream;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.InputStream;
import java.util.Map;
import org.bouncycastle.x509.X509Store;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.cms.SignedData;

public class CMSSignedData
{
    private static final CMSSignedHelper HELPER;
    SignedData signedData;
    ContentInfo contentInfo;
    CMSTypedData signedContent;
    SignerInformationStore signerInfoStore;
    X509Store attributeStore;
    X509Store certificateStore;
    X509Store crlStore;
    private Map hashes;
    
    private CMSSignedData(final CMSSignedData cmsSignedData) {
        this.signedData = cmsSignedData.signedData;
        this.contentInfo = cmsSignedData.contentInfo;
        this.signedContent = cmsSignedData.signedContent;
        this.signerInfoStore = cmsSignedData.signerInfoStore;
    }
    
    public CMSSignedData(final byte[] array) throws CMSException {
        this(CMSUtils.readContentInfo(array));
    }
    
    public CMSSignedData(final CMSProcessable cmsProcessable, final byte[] array) throws CMSException {
        this(cmsProcessable, CMSUtils.readContentInfo(array));
    }
    
    public CMSSignedData(final Map map, final byte[] array) throws CMSException {
        this(map, CMSUtils.readContentInfo(array));
    }
    
    public CMSSignedData(final CMSProcessable cmsProcessable, final InputStream inputStream) throws CMSException {
        this(cmsProcessable, CMSUtils.readContentInfo((InputStream)new ASN1InputStream(inputStream)));
    }
    
    public CMSSignedData(final InputStream inputStream) throws CMSException {
        this(CMSUtils.readContentInfo(inputStream));
    }
    
    public CMSSignedData(final CMSProcessable cmsProcessable, final ContentInfo contentInfo) throws CMSException {
        if (cmsProcessable instanceof CMSTypedData) {
            this.signedContent = (CMSTypedData)cmsProcessable;
        }
        else {
            this.signedContent = new CMSTypedData() {
                public ASN1ObjectIdentifier getContentType() {
                    return CMSSignedData.this.signedData.getEncapContentInfo().getContentType();
                }
                
                public void write(final OutputStream outputStream) throws IOException, CMSException {
                    cmsProcessable.write(outputStream);
                }
                
                public Object getContent() {
                    return cmsProcessable.getContent();
                }
            };
        }
        this.contentInfo = contentInfo;
        this.signedData = this.getSignedData();
    }
    
    public CMSSignedData(final Map hashes, final ContentInfo contentInfo) throws CMSException {
        this.hashes = hashes;
        this.contentInfo = contentInfo;
        this.signedData = this.getSignedData();
    }
    
    public CMSSignedData(final ContentInfo contentInfo) throws CMSException {
        this.contentInfo = contentInfo;
        this.signedData = this.getSignedData();
        if (this.signedData.getEncapContentInfo().getContent() != null) {
            this.signedContent = new CMSProcessableByteArray(this.signedData.getEncapContentInfo().getContentType(), ((ASN1OctetString)this.signedData.getEncapContentInfo().getContent()).getOctets());
        }
        else {
            this.signedContent = null;
        }
    }
    
    private SignedData getSignedData() throws CMSException {
        try {
            return SignedData.getInstance(this.contentInfo.getContent());
        }
        catch (ClassCastException ex) {
            throw new CMSException("Malformed content.", ex);
        }
        catch (IllegalArgumentException ex2) {
            throw new CMSException("Malformed content.", ex2);
        }
    }
    
    public int getVersion() {
        return this.signedData.getVersion().getValue().intValue();
    }
    
    public SignerInformationStore getSignerInfos() {
        if (this.signerInfoStore == null) {
            final ASN1Set signerInfos = this.signedData.getSignerInfos();
            final ArrayList<SignerInformation> list = new ArrayList<SignerInformation>();
            final DefaultSignatureAlgorithmIdentifierFinder defaultSignatureAlgorithmIdentifierFinder = new DefaultSignatureAlgorithmIdentifierFinder();
            for (int i = 0; i != signerInfos.size(); ++i) {
                final SignerInfo instance = SignerInfo.getInstance(signerInfos.getObjectAt(i));
                final ASN1ObjectIdentifier contentType = this.signedData.getEncapContentInfo().getContentType();
                if (this.hashes == null) {
                    list.add(new SignerInformation(instance, contentType, this.signedContent, null));
                }
                else {
                    list.add(new SignerInformation(instance, contentType, null, (this.hashes.keySet().iterator().next() instanceof String) ? this.hashes.get(instance.getDigestAlgorithm().getAlgorithm().getId()) : this.hashes.get(instance.getDigestAlgorithm().getAlgorithm())));
                }
            }
            this.signerInfoStore = new SignerInformationStore(list);
        }
        return this.signerInfoStore;
    }
    
    @Deprecated
    public X509Store getAttributeCertificates(final String s, final String s2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        return this.getAttributeCertificates(s, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public X509Store getAttributeCertificates(final String s, final Provider provider) throws NoSuchStoreException, CMSException {
        if (this.attributeStore == null) {
            this.attributeStore = CMSSignedData.HELPER.createAttributeStore(s, provider, this.getAttributeCertificates());
        }
        return this.attributeStore;
    }
    
    @Deprecated
    public X509Store getCertificates(final String s, final String s2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        return this.getCertificates(s, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public X509Store getCertificates(final String s, final Provider provider) throws NoSuchStoreException, CMSException {
        if (this.certificateStore == null) {
            this.certificateStore = CMSSignedData.HELPER.createCertificateStore(s, provider, this.getCertificates());
        }
        return this.certificateStore;
    }
    
    @Deprecated
    public X509Store getCRLs(final String s, final String s2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        return this.getCRLs(s, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public X509Store getCRLs(final String s, final Provider provider) throws NoSuchStoreException, CMSException {
        if (this.crlStore == null) {
            this.crlStore = CMSSignedData.HELPER.createCRLsStore(s, provider, this.getCRLs());
        }
        return this.crlStore;
    }
    
    @Deprecated
    public CertStore getCertificatesAndCRLs(final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.getCertificatesAndCRLs(s, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public CertStore getCertificatesAndCRLs(final String type, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        try {
            final JcaCertStoreBuilder setType = new JcaCertStoreBuilder().setType(type);
            if (provider != null) {
                setType.setProvider(provider);
            }
            setType.addCertificates(this.getCertificates());
            setType.addCRLs(this.getCRLs());
            return setType.build();
        }
        catch (NoSuchAlgorithmException ex) {
            throw ex;
        }
        catch (Exception ex2) {
            throw new CMSException("exception creating CertStore: " + ex2.getMessage(), ex2);
        }
    }
    
    public Store getCertificates() {
        return CMSSignedData.HELPER.getCertificates(this.signedData.getCertificates());
    }
    
    public Store getCRLs() {
        return CMSSignedData.HELPER.getCRLs(this.signedData.getCRLs());
    }
    
    public Store getAttributeCertificates() {
        return CMSSignedData.HELPER.getAttributeCertificates(this.signedData.getCertificates());
    }
    
    public Store getOtherRevocationInfo(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        return CMSSignedData.HELPER.getOtherRevocationInfo(asn1ObjectIdentifier, this.signedData.getCRLs());
    }
    
    public String getSignedContentTypeOID() {
        return this.signedData.getEncapContentInfo().getContentType().getId();
    }
    
    public CMSTypedData getSignedContent() {
        return this.signedContent;
    }
    
    @Deprecated
    public ContentInfo getContentInfo() {
        return this.contentInfo;
    }
    
    public ContentInfo toASN1Structure() {
        return this.contentInfo;
    }
    
    public byte[] getEncoded() throws IOException {
        return this.contentInfo.getEncoded();
    }
    
    public boolean verifySignatures(final SignerInformationVerifierProvider signerInformationVerifierProvider) throws CMSException {
        return this.verifySignatures(signerInformationVerifierProvider, false);
    }
    
    public boolean verifySignatures(final SignerInformationVerifierProvider signerInformationVerifierProvider, final boolean b) throws CMSException {
        for (final SignerInformation signerInformation : this.getSignerInfos().getSigners()) {
            try {
                if (!signerInformation.verify(signerInformationVerifierProvider.get(signerInformation.getSID()))) {
                    return false;
                }
                if (b) {
                    continue;
                }
                final Iterator iterator2 = signerInformation.getCounterSignatures().getSigners().iterator();
                while (iterator2.hasNext()) {
                    if (!iterator2.next().verify(signerInformationVerifierProvider.get(signerInformation.getSID()))) {
                        return false;
                    }
                }
            }
            catch (OperatorCreationException ex) {
                throw new CMSException("failure in verifier provider: " + ex.getMessage(), ex);
            }
        }
        return true;
    }
    
    public static CMSSignedData replaceSigners(final CMSSignedData cmsSignedData, final SignerInformationStore signerInfoStore) {
        final CMSSignedData cmsSignedData2 = new CMSSignedData(cmsSignedData);
        cmsSignedData2.signerInfoStore = signerInfoStore;
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
        for (final SignerInformation signerInformation : signerInfoStore.getSigners()) {
            asn1EncodableVector.add(CMSSignedHelper.INSTANCE.fixAlgID(signerInformation.getDigestAlgorithmID()));
            asn1EncodableVector2.add(signerInformation.toASN1Structure());
        }
        final DERSet set = new DERSet(asn1EncodableVector);
        final DERSet set2 = new DERSet(asn1EncodableVector2);
        final ASN1Sequence asn1Sequence = (ASN1Sequence)cmsSignedData.signedData.toASN1Primitive();
        final ASN1EncodableVector asn1EncodableVector3 = new ASN1EncodableVector();
        asn1EncodableVector3.add(asn1Sequence.getObjectAt(0));
        asn1EncodableVector3.add(set);
        for (int i = 2; i != asn1Sequence.size() - 1; ++i) {
            asn1EncodableVector3.add(asn1Sequence.getObjectAt(i));
        }
        asn1EncodableVector3.add(set2);
        cmsSignedData2.signedData = SignedData.getInstance(new BERSequence(asn1EncodableVector3));
        cmsSignedData2.contentInfo = new ContentInfo(cmsSignedData2.contentInfo.getContentType(), cmsSignedData2.signedData);
        return cmsSignedData2;
    }
    
    @Deprecated
    public static CMSSignedData replaceCertificatesAndCRLs(final CMSSignedData cmsSignedData, final CertStore certStore) throws CMSException {
        final CMSSignedData cmsSignedData2 = new CMSSignedData(cmsSignedData);
        ASN1Set set = null;
        ASN1Set set2 = null;
        try {
            final ASN1Set berSetFromList = CMSUtils.createBerSetFromList(CMSUtils.getCertificatesFromStore(certStore));
            if (berSetFromList.size() != 0) {
                set = berSetFromList;
            }
        }
        catch (CertStoreException ex) {
            throw new CMSException("error getting certs from certStore", ex);
        }
        try {
            final ASN1Set berSetFromList2 = CMSUtils.createBerSetFromList(CMSUtils.getCRLsFromStore(certStore));
            if (berSetFromList2.size() != 0) {
                set2 = berSetFromList2;
            }
        }
        catch (CertStoreException ex2) {
            throw new CMSException("error getting crls from certStore", ex2);
        }
        cmsSignedData2.signedData = new SignedData(cmsSignedData.signedData.getDigestAlgorithms(), cmsSignedData.signedData.getEncapContentInfo(), set, set2, cmsSignedData.signedData.getSignerInfos());
        cmsSignedData2.contentInfo = new ContentInfo(cmsSignedData2.contentInfo.getContentType(), cmsSignedData2.signedData);
        return cmsSignedData2;
    }
    
    public static CMSSignedData replaceCertificatesAndCRLs(final CMSSignedData cmsSignedData, final Store store, final Store store2, final Store store3) throws CMSException {
        final CMSSignedData cmsSignedData2 = new CMSSignedData(cmsSignedData);
        ASN1Set set = null;
        ASN1Set set2 = null;
        if (store != null || store2 != null) {
            final ArrayList list = new ArrayList();
            if (store != null) {
                list.addAll(CMSUtils.getCertificatesFromStore(store));
            }
            if (store2 != null) {
                list.addAll(CMSUtils.getAttributeCertificatesFromStore(store2));
            }
            final ASN1Set berSetFromList = CMSUtils.createBerSetFromList(list);
            if (berSetFromList.size() != 0) {
                set = berSetFromList;
            }
        }
        if (store3 != null) {
            final ASN1Set berSetFromList2 = CMSUtils.createBerSetFromList(CMSUtils.getCRLsFromStore(store3));
            if (berSetFromList2.size() != 0) {
                set2 = berSetFromList2;
            }
        }
        cmsSignedData2.signedData = new SignedData(cmsSignedData.signedData.getDigestAlgorithms(), cmsSignedData.signedData.getEncapContentInfo(), set, set2, cmsSignedData.signedData.getSignerInfos());
        cmsSignedData2.contentInfo = new ContentInfo(cmsSignedData2.contentInfo.getContentType(), cmsSignedData2.signedData);
        return cmsSignedData2;
    }
    
    static {
        HELPER = CMSSignedHelper.INSTANCE;
    }
}
