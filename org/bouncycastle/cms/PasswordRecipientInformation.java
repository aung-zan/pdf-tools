// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.util.Integers;
import java.util.HashMap;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.asn1.pkcs.PBKDF2Params;
import org.bouncycastle.cms.jcajce.JcePasswordRecipient;
import java.io.IOException;
import org.bouncycastle.cms.jcajce.JcePasswordAuthenticatedRecipient;
import org.bouncycastle.cms.jcajce.JcePasswordEnvelopedRecipient;
import java.security.Key;
import org.bouncycastle.cms.jcajce.JceAlgorithmIdentifierConverter;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.AlgorithmParameters;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.PasswordRecipientInfo;
import java.util.Map;

public class PasswordRecipientInformation extends RecipientInformation
{
    static Map KEYSIZES;
    static Map BLOCKSIZES;
    private PasswordRecipientInfo info;
    
    PasswordRecipientInformation(final PasswordRecipientInfo info, final AlgorithmIdentifier algorithmIdentifier, final CMSSecureReadable cmsSecureReadable, final AuthAttributesProvider authAttributesProvider) {
        super(info.getKeyEncryptionAlgorithm(), algorithmIdentifier, cmsSecureReadable, authAttributesProvider);
        this.info = info;
        this.rid = new PasswordRecipientId();
    }
    
    public String getKeyDerivationAlgOID() {
        if (this.info.getKeyDerivationAlgorithm() != null) {
            return this.info.getKeyDerivationAlgorithm().getAlgorithm().getId();
        }
        return null;
    }
    
    public byte[] getKeyDerivationAlgParams() {
        try {
            if (this.info.getKeyDerivationAlgorithm() != null) {
                final ASN1Encodable parameters = this.info.getKeyDerivationAlgorithm().getParameters();
                if (parameters != null) {
                    return parameters.toASN1Primitive().getEncoded();
                }
            }
            return null;
        }
        catch (Exception obj) {
            throw new RuntimeException("exception getting encryption parameters " + obj);
        }
    }
    
    public AlgorithmIdentifier getKeyDerivationAlgorithm() {
        return this.info.getKeyDerivationAlgorithm();
    }
    
    @Deprecated
    public AlgorithmParameters getKeyDerivationAlgParameters(final String s) throws NoSuchProviderException {
        return this.getKeyDerivationAlgParameters(CMSUtils.getProvider(s));
    }
    
    @Deprecated
    public AlgorithmParameters getKeyDerivationAlgParameters(final Provider provider) {
        try {
            return new JceAlgorithmIdentifierConverter().setProvider(provider).getAlgorithmParameters(this.info.getKeyDerivationAlgorithm());
        }
        catch (Exception obj) {
            throw new RuntimeException("exception getting encryption parameters " + obj);
        }
    }
    
    @Override
    @Deprecated
    public CMSTypedStream getContentStream(final Key key, final String s) throws CMSException, NoSuchProviderException {
        return this.getContentStream(key, CMSUtils.getProvider(s));
    }
    
    @Override
    @Deprecated
    public CMSTypedStream getContentStream(final Key key, final Provider provider) throws CMSException {
        try {
            final CMSPBEKey cmspbeKey = (CMSPBEKey)key;
            JcePasswordRecipient jcePasswordRecipient;
            if (this.secureReadable instanceof CMSEnvelopedHelper.CMSEnvelopedSecureReadable) {
                jcePasswordRecipient = new JcePasswordEnvelopedRecipient(cmspbeKey.getPassword());
            }
            else {
                jcePasswordRecipient = new JcePasswordAuthenticatedRecipient(cmspbeKey.getPassword());
            }
            jcePasswordRecipient.setPasswordConversionScheme((cmspbeKey instanceof PKCS5Scheme2UTF8PBEKey) ? 1 : 0);
            if (provider != null) {
                jcePasswordRecipient.setProvider(provider);
            }
            return this.getContentStream(jcePasswordRecipient);
        }
        catch (IOException ex) {
            throw new CMSException("encoding error: " + ex.getMessage(), ex);
        }
    }
    
    @Override
    protected RecipientOperator getRecipientOperator(final Recipient recipient) throws CMSException, IOException {
        final PasswordRecipient passwordRecipient = (PasswordRecipient)recipient;
        final AlgorithmIdentifier instance = AlgorithmIdentifier.getInstance(AlgorithmIdentifier.getInstance(this.info.getKeyEncryptionAlgorithm()).getParameters());
        final byte[] passwordBytes = this.getPasswordBytes(passwordRecipient.getPasswordConversionScheme(), passwordRecipient.getPassword());
        final PBKDF2Params instance2 = PBKDF2Params.getInstance(this.info.getKeyDerivationAlgorithm().getParameters());
        final PKCS5S2ParametersGenerator pkcs5S2ParametersGenerator = new PKCS5S2ParametersGenerator();
        pkcs5S2ParametersGenerator.init(passwordBytes, instance2.getSalt(), instance2.getIterationCount().intValue());
        return passwordRecipient.getRecipientOperator(instance, this.messageAlgorithm, ((KeyParameter)pkcs5S2ParametersGenerator.generateDerivedParameters(PasswordRecipientInformation.KEYSIZES.get(instance.getAlgorithm()))).getKey(), this.info.getEncryptedKey().getOctets());
    }
    
    protected byte[] getPasswordBytes(final int n, final char[] array) {
        if (n == 0) {
            return PBEParametersGenerator.PKCS5PasswordToBytes(array);
        }
        return PBEParametersGenerator.PKCS5PasswordToUTF8Bytes(array);
    }
    
    static {
        PasswordRecipientInformation.KEYSIZES = new HashMap();
        (PasswordRecipientInformation.BLOCKSIZES = new HashMap()).put(CMSAlgorithm.DES_EDE3_CBC, Integers.valueOf(8));
        PasswordRecipientInformation.BLOCKSIZES.put(CMSAlgorithm.AES128_CBC, Integers.valueOf(16));
        PasswordRecipientInformation.BLOCKSIZES.put(CMSAlgorithm.AES192_CBC, Integers.valueOf(16));
        PasswordRecipientInformation.BLOCKSIZES.put(CMSAlgorithm.AES256_CBC, Integers.valueOf(16));
        PasswordRecipientInformation.KEYSIZES.put(CMSAlgorithm.DES_EDE3_CBC, Integers.valueOf(192));
        PasswordRecipientInformation.KEYSIZES.put(CMSAlgorithm.AES128_CBC, Integers.valueOf(128));
        PasswordRecipientInformation.KEYSIZES.put(CMSAlgorithm.AES192_CBC, Integers.valueOf(192));
        PasswordRecipientInformation.KEYSIZES.put(CMSAlgorithm.AES256_CBC, Integers.valueOf(256));
    }
}
