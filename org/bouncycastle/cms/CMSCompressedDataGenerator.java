// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.operator.OutputCompressor;
import org.bouncycastle.asn1.cms.CompressedData;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import java.io.IOException;
import org.bouncycastle.asn1.BEROctetString;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.io.OutputStream;
import java.util.zip.DeflaterOutputStream;
import java.io.ByteArrayOutputStream;

public class CMSCompressedDataGenerator
{
    public static final String ZLIB = "1.2.840.113549.1.9.16.3.8";
    
    @Deprecated
    public CMSCompressedData generate(final CMSProcessable cmsProcessable, final String s) throws CMSException {
        AlgorithmIdentifier algorithmIdentifier;
        BEROctetString berOctetString;
        try {
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(out);
            cmsProcessable.write(deflaterOutputStream);
            deflaterOutputStream.close();
            algorithmIdentifier = new AlgorithmIdentifier(new ASN1ObjectIdentifier(s));
            berOctetString = new BEROctetString(out.toByteArray());
        }
        catch (IOException ex) {
            throw new CMSException("exception encoding data.", ex);
        }
        return new CMSCompressedData(new ContentInfo(CMSObjectIdentifiers.compressedData, new CompressedData(algorithmIdentifier, new ContentInfo(CMSObjectIdentifiers.data, berOctetString))));
    }
    
    public CMSCompressedData generate(final CMSTypedData cmsTypedData, final OutputCompressor outputCompressor) throws CMSException {
        AlgorithmIdentifier algorithmIdentifier;
        BEROctetString berOctetString;
        try {
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            final OutputStream outputStream = outputCompressor.getOutputStream(byteArrayOutputStream);
            cmsTypedData.write(outputStream);
            outputStream.close();
            algorithmIdentifier = outputCompressor.getAlgorithmIdentifier();
            berOctetString = new BEROctetString(byteArrayOutputStream.toByteArray());
        }
        catch (IOException ex) {
            throw new CMSException("exception encoding data.", ex);
        }
        return new CMSCompressedData(new ContentInfo(CMSObjectIdentifiers.compressedData, new CompressedData(algorithmIdentifier, new ContentInfo(cmsTypedData.getContentType(), berOctetString))));
    }
}
