// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import java.security.InvalidAlgorithmParameterException;
import java.security.AlgorithmParameters;

public class PKCS5Scheme2PBEKey extends CMSPBEKey
{
    public PKCS5Scheme2PBEKey(final char[] array, final byte[] array2, final int n) {
        super(array, array2, n);
    }
    
    public PKCS5Scheme2PBEKey(final char[] array, final AlgorithmParameters algorithmParameters) throws InvalidAlgorithmParameterException {
        super(array, CMSPBEKey.getParamSpec(algorithmParameters));
    }
    
    @Override
    byte[] getEncoded(final String s) {
        final PKCS5S2ParametersGenerator pkcs5S2ParametersGenerator = new PKCS5S2ParametersGenerator();
        pkcs5S2ParametersGenerator.init(PBEParametersGenerator.PKCS5PasswordToBytes(this.getPassword()), this.getSalt(), this.getIterationCount());
        return ((KeyParameter)pkcs5S2ParametersGenerator.generateDerivedParameters(CMSEnvelopedHelper.INSTANCE.getKeySize(s))).getKey();
    }
}
