// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.cms.PasswordRecipientInfo;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.asn1.cms.RecipientInfo;
import org.bouncycastle.operator.GenericKey;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.pkcs.PBKDF2Params;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.security.SecureRandom;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

public abstract class PasswordRecipientInfoGenerator implements RecipientInfoGenerator
{
    private char[] password;
    private AlgorithmIdentifier keyDerivationAlgorithm;
    private ASN1ObjectIdentifier kekAlgorithm;
    private SecureRandom random;
    private int schemeID;
    private int keySize;
    private int blockSize;
    
    protected PasswordRecipientInfoGenerator(final ASN1ObjectIdentifier asn1ObjectIdentifier, final char[] array) {
        this(asn1ObjectIdentifier, array, getKeySize(asn1ObjectIdentifier), PasswordRecipientInformation.BLOCKSIZES.get(asn1ObjectIdentifier));
    }
    
    protected PasswordRecipientInfoGenerator(final ASN1ObjectIdentifier kekAlgorithm, final char[] password, final int keySize, final int blockSize) {
        this.password = password;
        this.schemeID = 1;
        this.kekAlgorithm = kekAlgorithm;
        this.keySize = keySize;
        this.blockSize = blockSize;
    }
    
    private static int getKeySize(final ASN1ObjectIdentifier obj) {
        final Integer n = PasswordRecipientInformation.KEYSIZES.get(obj);
        if (n == null) {
            throw new IllegalArgumentException("cannot find key size for algorithm: " + obj);
        }
        return n;
    }
    
    public PasswordRecipientInfoGenerator setPasswordConversionScheme(final int schemeID) {
        this.schemeID = schemeID;
        return this;
    }
    
    public PasswordRecipientInfoGenerator setSaltAndIterationCount(final byte[] array, final int n) {
        this.keyDerivationAlgorithm = new AlgorithmIdentifier(PKCSObjectIdentifiers.id_PBKDF2, new PBKDF2Params(array, n));
        return this;
    }
    
    public PasswordRecipientInfoGenerator setSecureRandom(final SecureRandom random) {
        this.random = random;
        return this;
    }
    
    public RecipientInfo generate(final GenericKey genericKey) throws CMSException {
        final byte[] bytes = new byte[this.blockSize];
        if (this.random == null) {
            this.random = new SecureRandom();
        }
        this.random.nextBytes(bytes);
        if (this.keyDerivationAlgorithm == null) {
            final byte[] bytes2 = new byte[20];
            this.random.nextBytes(bytes2);
            this.keyDerivationAlgorithm = new AlgorithmIdentifier(PKCSObjectIdentifiers.id_PBKDF2, new PBKDF2Params(bytes2, 1024));
        }
        final PBKDF2Params instance = PBKDF2Params.getInstance(this.keyDerivationAlgorithm.getParameters());
        byte[] array;
        if (this.schemeID == 0) {
            final PKCS5S2ParametersGenerator pkcs5S2ParametersGenerator = new PKCS5S2ParametersGenerator();
            pkcs5S2ParametersGenerator.init(PBEParametersGenerator.PKCS5PasswordToBytes(this.password), instance.getSalt(), instance.getIterationCount().intValue());
            array = ((KeyParameter)pkcs5S2ParametersGenerator.generateDerivedParameters(this.keySize)).getKey();
        }
        else {
            final PKCS5S2ParametersGenerator pkcs5S2ParametersGenerator2 = new PKCS5S2ParametersGenerator();
            pkcs5S2ParametersGenerator2.init(PBEParametersGenerator.PKCS5PasswordToUTF8Bytes(this.password), instance.getSalt(), instance.getIterationCount().intValue());
            array = ((KeyParameter)pkcs5S2ParametersGenerator2.generateDerivedParameters(this.keySize)).getKey();
        }
        final DEROctetString derOctetString = new DEROctetString(this.generateEncryptedBytes(new AlgorithmIdentifier(this.kekAlgorithm, new DEROctetString(bytes)), array, genericKey));
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        asn1EncodableVector.add(this.kekAlgorithm);
        asn1EncodableVector.add(new DEROctetString(bytes));
        return new RecipientInfo(new PasswordRecipientInfo(this.keyDerivationAlgorithm, new AlgorithmIdentifier(PKCSObjectIdentifiers.id_alg_PWRI_KEK, new DERSequence(asn1EncodableVector)), derOctetString));
    }
    
    protected abstract byte[] generateEncryptedBytes(final AlgorithmIdentifier p0, final byte[] p1, final GenericKey p2) throws CMSException;
}
