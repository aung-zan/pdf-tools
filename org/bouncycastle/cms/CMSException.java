// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

public class CMSException extends Exception
{
    Exception e;
    
    public CMSException(final String message) {
        super(message);
    }
    
    public CMSException(final String message, final Exception e) {
        super(message);
        this.e = e;
    }
    
    public Exception getUnderlyingException() {
        return this.e;
    }
    
    @Override
    public Throwable getCause() {
        return this.e;
    }
}
