// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.util.io.Streams;
import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.BERSetParser;
import org.bouncycastle.asn1.ASN1SetParser;
import java.util.List;
import org.bouncycastle.asn1.DERTaggedObject;
import java.security.cert.CertStoreException;
import org.bouncycastle.asn1.ASN1Generator;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.BERSequenceGenerator;
import org.bouncycastle.asn1.ASN1StreamParser;
import org.bouncycastle.asn1.ASN1SequenceParser;
import java.io.OutputStream;
import org.bouncycastle.util.Store;
import org.bouncycastle.cert.jcajce.JcaCertStoreBuilder;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertStore;
import java.security.Provider;
import java.security.NoSuchProviderException;
import org.bouncycastle.x509.NoSuchStoreException;
import java.util.Iterator;
import java.util.Collection;
import org.bouncycastle.asn1.cms.SignerInfo;
import java.util.ArrayList;
import org.bouncycastle.asn1.cms.ContentInfoParser;
import org.bouncycastle.operator.DigestCalculator;
import org.bouncycastle.asn1.ASN1Encodable;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1OctetStringParser;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.util.HashMap;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
import org.bouncycastle.operator.DigestCalculatorProvider;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.x509.X509Store;
import java.util.Map;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.cms.SignedDataParser;

public class CMSSignedDataParser extends CMSContentInfoParser
{
    private static final CMSSignedHelper HELPER;
    private SignedDataParser _signedData;
    private ASN1ObjectIdentifier _signedContentType;
    private CMSTypedStream _signedContent;
    private Map digests;
    private SignerInformationStore _signerInfoStore;
    private X509Store _attributeStore;
    private ASN1Set _certSet;
    private ASN1Set _crlSet;
    private boolean _isCertCrlParsed;
    private X509Store _certificateStore;
    private X509Store _crlStore;
    
    @Deprecated
    public CMSSignedDataParser(final byte[] buf) throws CMSException {
        this(createDefaultDigestProvider(), new ByteArrayInputStream(buf));
    }
    
    public CMSSignedDataParser(final DigestCalculatorProvider digestCalculatorProvider, final byte[] buf) throws CMSException {
        this(digestCalculatorProvider, new ByteArrayInputStream(buf));
    }
    
    @Deprecated
    public CMSSignedDataParser(final CMSTypedStream cmsTypedStream, final byte[] buf) throws CMSException {
        this(createDefaultDigestProvider(), cmsTypedStream, new ByteArrayInputStream(buf));
    }
    
    public CMSSignedDataParser(final DigestCalculatorProvider digestCalculatorProvider, final CMSTypedStream cmsTypedStream, final byte[] buf) throws CMSException {
        this(digestCalculatorProvider, cmsTypedStream, new ByteArrayInputStream(buf));
    }
    
    private static DigestCalculatorProvider createDefaultDigestProvider() throws CMSException {
        return new BcDigestCalculatorProvider();
    }
    
    @Deprecated
    public CMSSignedDataParser(final InputStream inputStream) throws CMSException {
        this(createDefaultDigestProvider(), null, inputStream);
    }
    
    public CMSSignedDataParser(final DigestCalculatorProvider digestCalculatorProvider, final InputStream inputStream) throws CMSException {
        this(digestCalculatorProvider, null, inputStream);
    }
    
    @Deprecated
    public CMSSignedDataParser(final CMSTypedStream cmsTypedStream, final InputStream inputStream) throws CMSException {
        this(createDefaultDigestProvider(), cmsTypedStream, inputStream);
    }
    
    public CMSSignedDataParser(final DigestCalculatorProvider digestCalculatorProvider, final CMSTypedStream signedContent, final InputStream inputStream) throws CMSException {
        super(inputStream);
        try {
            this._signedContent = signedContent;
            this._signedData = SignedDataParser.getInstance(this._contentInfo.getContent(16));
            this.digests = new HashMap();
            ASN1Encodable object;
            while ((object = this._signedData.getDigestAlgorithms().readObject()) != null) {
                final AlgorithmIdentifier instance = AlgorithmIdentifier.getInstance(object);
                try {
                    final DigestCalculator value = digestCalculatorProvider.get(instance);
                    if (value == null) {
                        continue;
                    }
                    this.digests.put(instance.getAlgorithm(), value);
                }
                catch (OperatorCreationException ex2) {}
            }
            final ContentInfoParser encapContentInfo = this._signedData.getEncapContentInfo();
            final ASN1OctetStringParser asn1OctetStringParser = (ASN1OctetStringParser)encapContentInfo.getContent(4);
            if (asn1OctetStringParser != null) {
                final CMSTypedStream signedContent2 = new CMSTypedStream(encapContentInfo.getContentType().getId(), asn1OctetStringParser.getOctetStream());
                if (this._signedContent == null) {
                    this._signedContent = signedContent2;
                }
                else {
                    signedContent2.drain();
                }
            }
            if (signedContent == null) {
                this._signedContentType = encapContentInfo.getContentType();
            }
            else {
                this._signedContentType = this._signedContent.getContentType();
            }
        }
        catch (IOException ex) {
            throw new CMSException("io exception: " + ex.getMessage(), ex);
        }
        if (this.digests.isEmpty()) {
            throw new CMSException("no digests could be created for message.");
        }
    }
    
    public int getVersion() {
        return this._signedData.getVersion().getValue().intValue();
    }
    
    public SignerInformationStore getSignerInfos() throws CMSException {
        if (this._signerInfoStore == null) {
            this.populateCertCrlSets();
            final ArrayList<SignerInformation> list = new ArrayList<SignerInformation>();
            final HashMap<Object, Object> hashMap = new HashMap<Object, Object>();
            for (final Object next : this.digests.keySet()) {
                hashMap.put(next, ((DigestCalculator)this.digests.get(next)).getDigest());
            }
            try {
                ASN1Encodable object;
                while ((object = this._signedData.getSignerInfos().readObject()) != null) {
                    final SignerInfo instance = SignerInfo.getInstance(object.toASN1Primitive());
                    list.add(new SignerInformation(instance, this._signedContentType, null, hashMap.get(instance.getDigestAlgorithm().getAlgorithm())));
                }
            }
            catch (IOException ex) {
                throw new CMSException("io exception: " + ex.getMessage(), ex);
            }
            this._signerInfoStore = new SignerInformationStore(list);
        }
        return this._signerInfoStore;
    }
    
    @Deprecated
    public X509Store getAttributeCertificates(final String s, final String s2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        return this.getAttributeCertificates(s, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public X509Store getAttributeCertificates(final String s, final Provider provider) throws NoSuchStoreException, CMSException {
        if (this._attributeStore == null) {
            this.populateCertCrlSets();
            this._attributeStore = CMSSignedDataParser.HELPER.createAttributeStore(s, provider, this.getAttributeCertificates());
        }
        return this._attributeStore;
    }
    
    @Deprecated
    public X509Store getCertificates(final String s, final String s2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        return this.getCertificates(s, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public X509Store getCertificates(final String s, final Provider provider) throws NoSuchStoreException, CMSException {
        if (this._certificateStore == null) {
            this.populateCertCrlSets();
            this._certificateStore = CMSSignedDataParser.HELPER.createCertificateStore(s, provider, this.getCertificates());
        }
        return this._certificateStore;
    }
    
    @Deprecated
    public X509Store getCRLs(final String s, final String s2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        return this.getCRLs(s, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public X509Store getCRLs(final String s, final Provider provider) throws NoSuchStoreException, CMSException {
        if (this._crlStore == null) {
            this.populateCertCrlSets();
            this._crlStore = CMSSignedDataParser.HELPER.createCRLsStore(s, provider, this.getCRLs());
        }
        return this._crlStore;
    }
    
    @Deprecated
    public CertStore getCertificatesAndCRLs(final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.getCertificatesAndCRLs(s, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public CertStore getCertificatesAndCRLs(final String type, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        this.populateCertCrlSets();
        try {
            final JcaCertStoreBuilder setType = new JcaCertStoreBuilder().setType(type);
            if (provider != null) {
                setType.setProvider(provider);
            }
            setType.addCertificates(this.getCertificates());
            setType.addCRLs(this.getCRLs());
            return setType.build();
        }
        catch (NoSuchAlgorithmException ex) {
            throw ex;
        }
        catch (Exception ex2) {
            throw new CMSException("exception creating CertStore: " + ex2.getMessage(), ex2);
        }
    }
    
    public Store getCertificates() throws CMSException {
        this.populateCertCrlSets();
        return CMSSignedDataParser.HELPER.getCertificates(this._certSet);
    }
    
    public Store getCRLs() throws CMSException {
        this.populateCertCrlSets();
        return CMSSignedDataParser.HELPER.getCRLs(this._crlSet);
    }
    
    public Store getAttributeCertificates() throws CMSException {
        this.populateCertCrlSets();
        return CMSSignedDataParser.HELPER.getAttributeCertificates(this._certSet);
    }
    
    public Store getOtherRevocationInfo(final ASN1ObjectIdentifier asn1ObjectIdentifier) throws CMSException {
        this.populateCertCrlSets();
        return CMSSignedDataParser.HELPER.getOtherRevocationInfo(asn1ObjectIdentifier, this._crlSet);
    }
    
    private void populateCertCrlSets() throws CMSException {
        if (this._isCertCrlParsed) {
            return;
        }
        this._isCertCrlParsed = true;
        try {
            this._certSet = getASN1Set(this._signedData.getCertificates());
            this._crlSet = getASN1Set(this._signedData.getCrls());
        }
        catch (IOException ex) {
            throw new CMSException("problem parsing cert/crl sets", ex);
        }
    }
    
    public String getSignedContentTypeOID() {
        return this._signedContentType.getId();
    }
    
    public CMSTypedStream getSignedContent() {
        if (this._signedContent == null) {
            return null;
        }
        return new CMSTypedStream(this._signedContent.getContentType(), CMSUtils.attachDigestsToInputStream(this.digests.values(), this._signedContent.getContentStream()));
    }
    
    public static OutputStream replaceSigners(final InputStream inputStream, final SignerInformationStore signerInformationStore, final OutputStream outputStream) throws CMSException, IOException {
        final SignedDataParser instance = SignedDataParser.getInstance(new ContentInfoParser((ASN1SequenceParser)new ASN1StreamParser(inputStream).readObject()).getContent(16));
        final BERSequenceGenerator berSequenceGenerator = new BERSequenceGenerator(outputStream);
        berSequenceGenerator.addObject(CMSObjectIdentifiers.signedData);
        final BERSequenceGenerator berSequenceGenerator2 = new BERSequenceGenerator(berSequenceGenerator.getRawOutputStream(), 0, true);
        berSequenceGenerator2.addObject(instance.getVersion());
        instance.getDigestAlgorithms().toASN1Primitive();
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final Iterator<SignerInformation> iterator = signerInformationStore.getSigners().iterator();
        while (iterator.hasNext()) {
            asn1EncodableVector.add(CMSSignedHelper.INSTANCE.fixAlgID(iterator.next().getDigestAlgorithmID()));
        }
        berSequenceGenerator2.getRawOutputStream().write(new DERSet(asn1EncodableVector).getEncoded());
        final ContentInfoParser encapContentInfo = instance.getEncapContentInfo();
        final BERSequenceGenerator berSequenceGenerator3 = new BERSequenceGenerator(berSequenceGenerator2.getRawOutputStream());
        berSequenceGenerator3.addObject(encapContentInfo.getContentType());
        pipeEncapsulatedOctetString(encapContentInfo, berSequenceGenerator3.getRawOutputStream());
        berSequenceGenerator3.close();
        writeSetToGeneratorTagged(berSequenceGenerator2, instance.getCertificates(), 0);
        writeSetToGeneratorTagged(berSequenceGenerator2, instance.getCrls(), 1);
        final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
        final Iterator<SignerInformation> iterator2 = signerInformationStore.getSigners().iterator();
        while (iterator2.hasNext()) {
            asn1EncodableVector2.add(iterator2.next().toASN1Structure());
        }
        berSequenceGenerator2.getRawOutputStream().write(new DERSet(asn1EncodableVector2).getEncoded());
        berSequenceGenerator2.close();
        berSequenceGenerator.close();
        return outputStream;
    }
    
    @Deprecated
    public static OutputStream replaceCertificatesAndCRLs(final InputStream inputStream, final CertStore certStore, final OutputStream outputStream) throws CMSException, IOException {
        final SignedDataParser instance = SignedDataParser.getInstance(new ContentInfoParser((ASN1SequenceParser)new ASN1StreamParser(inputStream).readObject()).getContent(16));
        final BERSequenceGenerator berSequenceGenerator = new BERSequenceGenerator(outputStream);
        berSequenceGenerator.addObject(CMSObjectIdentifiers.signedData);
        final BERSequenceGenerator berSequenceGenerator2 = new BERSequenceGenerator(berSequenceGenerator.getRawOutputStream(), 0, true);
        berSequenceGenerator2.addObject(instance.getVersion());
        berSequenceGenerator2.getRawOutputStream().write(instance.getDigestAlgorithms().toASN1Primitive().getEncoded());
        final ContentInfoParser encapContentInfo = instance.getEncapContentInfo();
        final BERSequenceGenerator berSequenceGenerator3 = new BERSequenceGenerator(berSequenceGenerator2.getRawOutputStream());
        berSequenceGenerator3.addObject(encapContentInfo.getContentType());
        pipeEncapsulatedOctetString(encapContentInfo, berSequenceGenerator3.getRawOutputStream());
        berSequenceGenerator3.close();
        getASN1Set(instance.getCertificates());
        getASN1Set(instance.getCrls());
        ASN1Set berSetFromList;
        try {
            berSetFromList = CMSUtils.createBerSetFromList(CMSUtils.getCertificatesFromStore(certStore));
        }
        catch (CertStoreException ex) {
            throw new CMSException("error getting certs from certStore", ex);
        }
        if (berSetFromList.size() > 0) {
            berSequenceGenerator2.getRawOutputStream().write(new DERTaggedObject(false, 0, berSetFromList).getEncoded());
        }
        ASN1Set berSetFromList2;
        try {
            berSetFromList2 = CMSUtils.createBerSetFromList(CMSUtils.getCRLsFromStore(certStore));
        }
        catch (CertStoreException ex2) {
            throw new CMSException("error getting crls from certStore", ex2);
        }
        if (berSetFromList2.size() > 0) {
            berSequenceGenerator2.getRawOutputStream().write(new DERTaggedObject(false, 1, berSetFromList2).getEncoded());
        }
        berSequenceGenerator2.getRawOutputStream().write(instance.getSignerInfos().toASN1Primitive().getEncoded());
        berSequenceGenerator2.close();
        berSequenceGenerator.close();
        return outputStream;
    }
    
    public static OutputStream replaceCertificatesAndCRLs(final InputStream inputStream, final Store store, final Store store2, final Store store3, final OutputStream outputStream) throws CMSException, IOException {
        final SignedDataParser instance = SignedDataParser.getInstance(new ContentInfoParser((ASN1SequenceParser)new ASN1StreamParser(inputStream).readObject()).getContent(16));
        final BERSequenceGenerator berSequenceGenerator = new BERSequenceGenerator(outputStream);
        berSequenceGenerator.addObject(CMSObjectIdentifiers.signedData);
        final BERSequenceGenerator berSequenceGenerator2 = new BERSequenceGenerator(berSequenceGenerator.getRawOutputStream(), 0, true);
        berSequenceGenerator2.addObject(instance.getVersion());
        berSequenceGenerator2.getRawOutputStream().write(instance.getDigestAlgorithms().toASN1Primitive().getEncoded());
        final ContentInfoParser encapContentInfo = instance.getEncapContentInfo();
        final BERSequenceGenerator berSequenceGenerator3 = new BERSequenceGenerator(berSequenceGenerator2.getRawOutputStream());
        berSequenceGenerator3.addObject(encapContentInfo.getContentType());
        pipeEncapsulatedOctetString(encapContentInfo, berSequenceGenerator3.getRawOutputStream());
        berSequenceGenerator3.close();
        getASN1Set(instance.getCertificates());
        getASN1Set(instance.getCrls());
        if (store != null || store3 != null) {
            final ArrayList list = new ArrayList();
            if (store != null) {
                list.addAll(CMSUtils.getCertificatesFromStore(store));
            }
            if (store3 != null) {
                list.addAll(CMSUtils.getAttributeCertificatesFromStore(store3));
            }
            final ASN1Set berSetFromList = CMSUtils.createBerSetFromList(list);
            if (berSetFromList.size() > 0) {
                berSequenceGenerator2.getRawOutputStream().write(new DERTaggedObject(false, 0, berSetFromList).getEncoded());
            }
        }
        if (store2 != null) {
            final ASN1Set berSetFromList2 = CMSUtils.createBerSetFromList(CMSUtils.getCRLsFromStore(store2));
            if (berSetFromList2.size() > 0) {
                berSequenceGenerator2.getRawOutputStream().write(new DERTaggedObject(false, 1, berSetFromList2).getEncoded());
            }
        }
        berSequenceGenerator2.getRawOutputStream().write(instance.getSignerInfos().toASN1Primitive().getEncoded());
        berSequenceGenerator2.close();
        berSequenceGenerator.close();
        return outputStream;
    }
    
    private static void writeSetToGeneratorTagged(final ASN1Generator asn1Generator, final ASN1SetParser asn1SetParser, final int n) throws IOException {
        final ASN1Set asn1Set = getASN1Set(asn1SetParser);
        if (asn1Set != null) {
            if (asn1SetParser instanceof BERSetParser) {
                asn1Generator.getRawOutputStream().write(new BERTaggedObject(false, n, asn1Set).getEncoded());
            }
            else {
                asn1Generator.getRawOutputStream().write(new DERTaggedObject(false, n, asn1Set).getEncoded());
            }
        }
    }
    
    private static ASN1Set getASN1Set(final ASN1SetParser asn1SetParser) {
        return (asn1SetParser == null) ? null : ASN1Set.getInstance(asn1SetParser.toASN1Primitive());
    }
    
    private static void pipeEncapsulatedOctetString(final ContentInfoParser contentInfoParser, final OutputStream outputStream) throws IOException {
        final ASN1OctetStringParser asn1OctetStringParser = (ASN1OctetStringParser)contentInfoParser.getContent(4);
        if (asn1OctetStringParser != null) {
            pipeOctetString(asn1OctetStringParser, outputStream);
        }
    }
    
    private static void pipeOctetString(final ASN1OctetStringParser asn1OctetStringParser, final OutputStream outputStream) throws IOException {
        final OutputStream berOctetOutputStream = CMSUtils.createBEROctetOutputStream(outputStream, 0, true, 0);
        Streams.pipeAll(asn1OctetStringParser.getOctetStream(), berOctetOutputStream);
        berOctetOutputStream.close();
    }
    
    static {
        HELPER = CMSSignedHelper.INSTANCE;
    }
}
