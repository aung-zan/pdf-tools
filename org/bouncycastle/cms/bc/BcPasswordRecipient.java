// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms.bc;

import org.bouncycastle.crypto.Wrapper;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.cms.PasswordRecipient;

public abstract class BcPasswordRecipient implements PasswordRecipient
{
    private int schemeID;
    private char[] password;
    
    BcPasswordRecipient(final char[] password) {
        this.schemeID = 1;
        this.password = password;
    }
    
    public BcPasswordRecipient setPasswordConversionScheme(final int schemeID) {
        this.schemeID = schemeID;
        return this;
    }
    
    protected KeyParameter extractSecretKey(final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final byte[] array, final byte[] array2) throws CMSException {
        final Wrapper rfc3211Wrapper = EnvelopedDataHelper.createRFC3211Wrapper(algorithmIdentifier.getAlgorithm());
        rfc3211Wrapper.init(false, new ParametersWithIV(new KeyParameter(array), ASN1OctetString.getInstance(algorithmIdentifier.getParameters()).getOctets()));
        try {
            return new KeyParameter(rfc3211Wrapper.unwrap(array2, 0, array2.length));
        }
        catch (InvalidCipherTextException ex) {
            throw new CMSException("unable to unwrap key: " + ex.getMessage(), ex);
        }
    }
    
    public int getPasswordConversionScheme() {
        return this.schemeID;
    }
    
    public char[] getPassword() {
        return this.password;
    }
}
