// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms.bc;

import org.bouncycastle.cms.CMSException;
import org.bouncycastle.crypto.Wrapper;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.operator.GenericKey;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.cms.PasswordRecipientInfoGenerator;

public class BcPasswordRecipientInfoGenerator extends PasswordRecipientInfoGenerator
{
    public BcPasswordRecipientInfoGenerator(final ASN1ObjectIdentifier asn1ObjectIdentifier, final char[] array) {
        super(asn1ObjectIdentifier, array);
    }
    
    public byte[] generateEncryptedBytes(final AlgorithmIdentifier algorithmIdentifier, final byte[] array, final GenericKey genericKey) throws CMSException {
        final byte[] key = ((KeyParameter)CMSUtils.getBcKey(genericKey)).getKey();
        final Wrapper rfc3211Wrapper = EnvelopedDataHelper.createRFC3211Wrapper(algorithmIdentifier.getAlgorithm());
        rfc3211Wrapper.init(true, new ParametersWithIV(new KeyParameter(array), ASN1OctetString.getInstance(algorithmIdentifier.getParameters()).getOctets()));
        return rfc3211Wrapper.wrap(key, 0, key.length);
    }
}
