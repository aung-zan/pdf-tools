// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.operator.InputExpanderProvider;
import org.bouncycastle.asn1.cms.ContentInfoParser;
import java.io.IOException;
import java.util.zip.InflaterInputStream;
import org.bouncycastle.asn1.ASN1OctetStringParser;
import org.bouncycastle.asn1.cms.CompressedDataParser;
import org.bouncycastle.asn1.ASN1SequenceParser;
import java.io.InputStream;
import java.io.ByteArrayInputStream;

public class CMSCompressedDataParser extends CMSContentInfoParser
{
    public CMSCompressedDataParser(final byte[] buf) throws CMSException {
        this(new ByteArrayInputStream(buf));
    }
    
    public CMSCompressedDataParser(final InputStream inputStream) throws CMSException {
        super(inputStream);
    }
    
    @Deprecated
    public CMSTypedStream getContent() throws CMSException {
        try {
            final ContentInfoParser encapContentInfo = new CompressedDataParser((ASN1SequenceParser)this._contentInfo.getContent(16)).getEncapContentInfo();
            return new CMSTypedStream(encapContentInfo.getContentType().toString(), new InflaterInputStream(((ASN1OctetStringParser)encapContentInfo.getContent(4)).getOctetStream()));
        }
        catch (IOException ex) {
            throw new CMSException("IOException reading compressed content.", ex);
        }
    }
    
    public CMSTypedStream getContent(final InputExpanderProvider inputExpanderProvider) throws CMSException {
        try {
            final CompressedDataParser compressedDataParser = new CompressedDataParser((ASN1SequenceParser)this._contentInfo.getContent(16));
            final ContentInfoParser encapContentInfo = compressedDataParser.getEncapContentInfo();
            return new CMSTypedStream(encapContentInfo.getContentType().getId(), inputExpanderProvider.get(compressedDataParser.getCompressionAlgorithmIdentifier()).getInputStream(((ASN1OctetStringParser)encapContentInfo.getContent(4)).getOctetStream()));
        }
        catch (IOException ex) {
            throw new CMSException("IOException reading compressed content.", ex);
        }
    }
}
