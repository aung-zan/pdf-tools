// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.cms.jcajce.JceKEKRecipient;
import java.io.IOException;
import org.bouncycastle.cms.jcajce.JceKEKAuthenticatedRecipient;
import org.bouncycastle.cms.jcajce.JceKEKEnvelopedRecipient;
import javax.crypto.SecretKey;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.Key;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.KEKRecipientInfo;

public class KEKRecipientInformation extends RecipientInformation
{
    private KEKRecipientInfo info;
    
    KEKRecipientInformation(final KEKRecipientInfo info, final AlgorithmIdentifier algorithmIdentifier, final CMSSecureReadable cmsSecureReadable, final AuthAttributesProvider authAttributesProvider) {
        super(info.getKeyEncryptionAlgorithm(), algorithmIdentifier, cmsSecureReadable, authAttributesProvider);
        this.info = info;
        this.rid = new KEKRecipientId(info.getKekid().getKeyIdentifier().getOctets());
    }
    
    @Override
    public CMSTypedStream getContentStream(final Key key, final String s) throws CMSException, NoSuchProviderException {
        return this.getContentStream(key, CMSUtils.getProvider(s));
    }
    
    @Override
    @Deprecated
    public CMSTypedStream getContentStream(final Key key, final Provider provider) throws CMSException {
        try {
            JceKEKRecipient jceKEKRecipient;
            if (this.secureReadable instanceof CMSEnvelopedHelper.CMSEnvelopedSecureReadable) {
                jceKEKRecipient = new JceKEKEnvelopedRecipient((SecretKey)key);
            }
            else {
                jceKEKRecipient = new JceKEKAuthenticatedRecipient((SecretKey)key);
            }
            if (provider != null) {
                jceKEKRecipient.setProvider(provider);
            }
            return this.getContentStream(jceKEKRecipient);
        }
        catch (IOException ex) {
            throw new CMSException("encoding error: " + ex.getMessage(), ex);
        }
    }
    
    @Override
    protected RecipientOperator getRecipientOperator(final Recipient recipient) throws CMSException, IOException {
        return ((KEKRecipient)recipient).getRecipientOperator(this.keyEncAlg, this.messageAlgorithm, this.info.getEncryptedKey().getOctets());
    }
}
