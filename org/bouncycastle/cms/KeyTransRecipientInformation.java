// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.cms.jcajce.JceKeyTransRecipient;
import java.io.IOException;
import org.bouncycastle.cms.jcajce.JceKeyTransAuthenticatedRecipient;
import org.bouncycastle.cms.jcajce.JceKeyTransEnvelopedRecipient;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.Key;
import org.bouncycastle.asn1.cms.RecipientIdentifier;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.KeyTransRecipientInfo;

public class KeyTransRecipientInformation extends RecipientInformation
{
    private KeyTransRecipientInfo info;
    
    KeyTransRecipientInformation(final KeyTransRecipientInfo info, final AlgorithmIdentifier algorithmIdentifier, final CMSSecureReadable cmsSecureReadable, final AuthAttributesProvider authAttributesProvider) {
        super(info.getKeyEncryptionAlgorithm(), algorithmIdentifier, cmsSecureReadable, authAttributesProvider);
        this.info = info;
        final RecipientIdentifier recipientIdentifier = info.getRecipientIdentifier();
        if (recipientIdentifier.isTagged()) {
            this.rid = new KeyTransRecipientId(ASN1OctetString.getInstance(recipientIdentifier.getId()).getOctets());
        }
        else {
            final IssuerAndSerialNumber instance = IssuerAndSerialNumber.getInstance(recipientIdentifier.getId());
            this.rid = new KeyTransRecipientId(instance.getName(), instance.getSerialNumber().getValue());
        }
    }
    
    @Override
    @Deprecated
    public CMSTypedStream getContentStream(final Key key, final String s) throws CMSException, NoSuchProviderException {
        return this.getContentStream(key, CMSUtils.getProvider(s));
    }
    
    @Override
    @Deprecated
    public CMSTypedStream getContentStream(final Key key, final Provider provider) throws CMSException {
        try {
            JceKeyTransRecipient jceKeyTransRecipient;
            if (this.secureReadable instanceof CMSEnvelopedHelper.CMSEnvelopedSecureReadable) {
                jceKeyTransRecipient = new JceKeyTransEnvelopedRecipient((PrivateKey)key);
            }
            else {
                jceKeyTransRecipient = new JceKeyTransAuthenticatedRecipient((PrivateKey)key);
            }
            if (provider != null) {
                jceKeyTransRecipient.setProvider(provider);
                if (provider.getName().equalsIgnoreCase("SunJCE")) {
                    jceKeyTransRecipient.setContentProvider((String)null);
                }
            }
            return this.getContentStream(jceKeyTransRecipient);
        }
        catch (IOException ex) {
            throw new CMSException("encoding error: " + ex.getMessage(), ex);
        }
    }
    
    @Override
    protected RecipientOperator getRecipientOperator(final Recipient recipient) throws CMSException {
        return ((KeyTransRecipient)recipient).getRecipientOperator(this.keyEncAlg, this.messageAlgorithm, this.info.getEncryptedKey().getOctets());
    }
}
