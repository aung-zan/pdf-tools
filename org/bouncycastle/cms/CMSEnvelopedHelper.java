// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.io.FilterInputStream;
import org.bouncycastle.operator.DigestCalculator;
import java.io.IOException;
import java.io.InputStream;
import org.bouncycastle.util.Integers;
import java.util.HashMap;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.cms.PasswordRecipientInfo;
import org.bouncycastle.asn1.cms.KeyAgreeRecipientInfo;
import org.bouncycastle.asn1.cms.KEKRecipientInfo;
import org.bouncycastle.asn1.cms.KeyTransRecipientInfo;
import java.util.Collection;
import java.util.List;
import org.bouncycastle.asn1.cms.RecipientInfo;
import java.util.ArrayList;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1Set;
import java.security.NoSuchAlgorithmException;
import javax.crypto.KeyGenerator;
import java.security.Provider;
import java.util.Map;

class CMSEnvelopedHelper
{
    static final CMSEnvelopedHelper INSTANCE;
    private static final Map KEYSIZES;
    private static final Map BASE_CIPHER_NAMES;
    private static final Map CIPHER_ALG_NAMES;
    private static final Map MAC_ALG_NAMES;
    
    KeyGenerator createSymmetricKeyGenerator(final String s, final Provider provider) throws NoSuchAlgorithmException {
        try {
            return this.createKeyGenerator(s, provider);
        }
        catch (NoSuchAlgorithmException ex) {
            try {
                final String s2 = CMSEnvelopedHelper.BASE_CIPHER_NAMES.get(s);
                if (s2 != null) {
                    return this.createKeyGenerator(s2, provider);
                }
            }
            catch (NoSuchAlgorithmException ex2) {}
            if (provider != null) {
                return this.createSymmetricKeyGenerator(s, null);
            }
            throw ex;
        }
    }
    
    int getKeySize(final String str) {
        final Integer n = CMSEnvelopedHelper.KEYSIZES.get(str);
        if (n == null) {
            throw new IllegalArgumentException("no keysize for " + str);
        }
        return n;
    }
    
    private KeyGenerator createKeyGenerator(final String s, final Provider provider) throws NoSuchAlgorithmException {
        if (provider != null) {
            return KeyGenerator.getInstance(s, provider);
        }
        return KeyGenerator.getInstance(s);
    }
    
    static RecipientInformationStore buildRecipientInformationStore(final ASN1Set set, final AlgorithmIdentifier algorithmIdentifier, final CMSSecureReadable cmsSecureReadable) {
        return buildRecipientInformationStore(set, algorithmIdentifier, cmsSecureReadable, null);
    }
    
    static RecipientInformationStore buildRecipientInformationStore(final ASN1Set set, final AlgorithmIdentifier algorithmIdentifier, final CMSSecureReadable cmsSecureReadable, final AuthAttributesProvider authAttributesProvider) {
        final ArrayList list = new ArrayList();
        for (int i = 0; i != set.size(); ++i) {
            readRecipientInfo(list, RecipientInfo.getInstance(set.getObjectAt(i)), algorithmIdentifier, cmsSecureReadable, authAttributesProvider);
        }
        return new RecipientInformationStore(list);
    }
    
    private static void readRecipientInfo(final List list, final RecipientInfo recipientInfo, final AlgorithmIdentifier algorithmIdentifier, final CMSSecureReadable cmsSecureReadable, final AuthAttributesProvider authAttributesProvider) {
        final ASN1Encodable info = recipientInfo.getInfo();
        if (info instanceof KeyTransRecipientInfo) {
            list.add(new KeyTransRecipientInformation((KeyTransRecipientInfo)info, algorithmIdentifier, cmsSecureReadable, authAttributesProvider));
        }
        else if (info instanceof KEKRecipientInfo) {
            list.add(new KEKRecipientInformation((KEKRecipientInfo)info, algorithmIdentifier, cmsSecureReadable, authAttributesProvider));
        }
        else if (info instanceof KeyAgreeRecipientInfo) {
            KeyAgreeRecipientInformation.readRecipientInfo(list, (KeyAgreeRecipientInfo)info, algorithmIdentifier, cmsSecureReadable, authAttributesProvider);
        }
        else if (info instanceof PasswordRecipientInfo) {
            list.add(new PasswordRecipientInformation((PasswordRecipientInfo)info, algorithmIdentifier, cmsSecureReadable, authAttributesProvider));
        }
    }
    
    static {
        INSTANCE = new CMSEnvelopedHelper();
        KEYSIZES = new HashMap();
        BASE_CIPHER_NAMES = new HashMap();
        CIPHER_ALG_NAMES = new HashMap();
        MAC_ALG_NAMES = new HashMap();
        CMSEnvelopedHelper.KEYSIZES.put(CMSEnvelopedGenerator.DES_EDE3_CBC, Integers.valueOf(192));
        CMSEnvelopedHelper.KEYSIZES.put(CMSEnvelopedGenerator.AES128_CBC, Integers.valueOf(128));
        CMSEnvelopedHelper.KEYSIZES.put(CMSEnvelopedGenerator.AES192_CBC, Integers.valueOf(192));
        CMSEnvelopedHelper.KEYSIZES.put(CMSEnvelopedGenerator.AES256_CBC, Integers.valueOf(256));
        CMSEnvelopedHelper.BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.DES_EDE3_CBC, "DESEDE");
        CMSEnvelopedHelper.BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.AES128_CBC, "AES");
        CMSEnvelopedHelper.BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.AES192_CBC, "AES");
        CMSEnvelopedHelper.BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.AES256_CBC, "AES");
        CMSEnvelopedHelper.CIPHER_ALG_NAMES.put(CMSEnvelopedGenerator.DES_EDE3_CBC, "DESEDE/CBC/PKCS5Padding");
        CMSEnvelopedHelper.CIPHER_ALG_NAMES.put(CMSEnvelopedGenerator.AES128_CBC, "AES/CBC/PKCS5Padding");
        CMSEnvelopedHelper.CIPHER_ALG_NAMES.put(CMSEnvelopedGenerator.AES192_CBC, "AES/CBC/PKCS5Padding");
        CMSEnvelopedHelper.CIPHER_ALG_NAMES.put(CMSEnvelopedGenerator.AES256_CBC, "AES/CBC/PKCS5Padding");
        CMSEnvelopedHelper.MAC_ALG_NAMES.put(CMSEnvelopedGenerator.DES_EDE3_CBC, "DESEDEMac");
        CMSEnvelopedHelper.MAC_ALG_NAMES.put(CMSEnvelopedGenerator.AES128_CBC, "AESMac");
        CMSEnvelopedHelper.MAC_ALG_NAMES.put(CMSEnvelopedGenerator.AES192_CBC, "AESMac");
        CMSEnvelopedHelper.MAC_ALG_NAMES.put(CMSEnvelopedGenerator.AES256_CBC, "AESMac");
    }
    
    static class CMSAuthenticatedSecureReadable implements CMSSecureReadable
    {
        private AlgorithmIdentifier algorithm;
        private CMSReadable readable;
        
        CMSAuthenticatedSecureReadable(final AlgorithmIdentifier algorithm, final CMSReadable readable) {
            this.algorithm = algorithm;
            this.readable = readable;
        }
        
        public InputStream getInputStream() throws IOException, CMSException {
            return this.readable.getInputStream();
        }
    }
    
    static class CMSDigestAuthenticatedSecureReadable implements CMSSecureReadable
    {
        private DigestCalculator digestCalculator;
        private CMSReadable readable;
        
        public CMSDigestAuthenticatedSecureReadable(final DigestCalculator digestCalculator, final CMSReadable readable) {
            this.digestCalculator = digestCalculator;
            this.readable = readable;
        }
        
        public InputStream getInputStream() throws IOException, CMSException {
            return new FilterInputStream(this.readable.getInputStream()) {
                @Override
                public int read() throws IOException {
                    final int read = this.in.read();
                    if (read >= 0) {
                        CMSDigestAuthenticatedSecureReadable.this.digestCalculator.getOutputStream().write(read);
                    }
                    return read;
                }
                
                @Override
                public int read(final byte[] array, final int n, final int len) throws IOException {
                    final int read = this.in.read(array, n, len);
                    if (read >= 0) {
                        CMSDigestAuthenticatedSecureReadable.this.digestCalculator.getOutputStream().write(array, n, read);
                    }
                    return read;
                }
            };
        }
        
        public byte[] getDigest() {
            return this.digestCalculator.getDigest();
        }
    }
    
    static class CMSEnvelopedSecureReadable implements CMSSecureReadable
    {
        private AlgorithmIdentifier algorithm;
        private CMSReadable readable;
        
        CMSEnvelopedSecureReadable(final AlgorithmIdentifier algorithm, final CMSReadable readable) {
            this.algorithm = algorithm;
            this.readable = readable;
        }
        
        public InputStream getInputStream() throws IOException, CMSException {
            return this.readable.getInputStream();
        }
    }
}
