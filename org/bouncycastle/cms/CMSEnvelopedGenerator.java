// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.asn1.kisa.KISAObjectIdentifiers;
import org.bouncycastle.asn1.ntt.NTTObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.security.AlgorithmParameters;
import java.util.Iterator;
import org.bouncycastle.cms.jcajce.JceKeyAgreeRecipientInfoGenerator;
import java.util.Collection;
import java.security.Provider;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import org.bouncycastle.cms.jcajce.JcePasswordRecipientInfoGenerator;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.cms.jcajce.JceKEKRecipientInfoGenerator;
import org.bouncycastle.asn1.cms.OtherKeyAttribute;
import org.bouncycastle.asn1.ASN1GeneralizedTime;
import org.bouncycastle.asn1.cms.KEKIdentifier;
import javax.crypto.SecretKey;
import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipientInfoGenerator;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import org.bouncycastle.asn1.cms.OriginatorInfo;
import java.security.SecureRandom;
import java.util.List;

public class CMSEnvelopedGenerator
{
    public static final String DES_EDE3_CBC;
    public static final String RC2_CBC;
    public static final String IDEA_CBC = "1.3.6.1.4.1.188.7.1.1.2";
    public static final String CAST5_CBC = "1.2.840.113533.7.66.10";
    public static final String AES128_CBC;
    public static final String AES192_CBC;
    public static final String AES256_CBC;
    public static final String CAMELLIA128_CBC;
    public static final String CAMELLIA192_CBC;
    public static final String CAMELLIA256_CBC;
    public static final String SEED_CBC;
    public static final String DES_EDE3_WRAP;
    public static final String AES128_WRAP;
    public static final String AES192_WRAP;
    public static final String AES256_WRAP;
    public static final String CAMELLIA128_WRAP;
    public static final String CAMELLIA192_WRAP;
    public static final String CAMELLIA256_WRAP;
    public static final String SEED_WRAP;
    public static final String ECDH_SHA1KDF;
    public static final String ECMQV_SHA1KDF;
    final List oldRecipientInfoGenerators;
    final List recipientInfoGenerators;
    protected CMSAttributeTableGenerator unprotectedAttributeGenerator;
    final SecureRandom rand;
    protected OriginatorInfo originatorInfo;
    
    public CMSEnvelopedGenerator() {
        this(new SecureRandom());
    }
    
    public CMSEnvelopedGenerator(final SecureRandom rand) {
        this.oldRecipientInfoGenerators = new ArrayList();
        this.recipientInfoGenerators = new ArrayList();
        this.unprotectedAttributeGenerator = null;
        this.rand = rand;
    }
    
    public void setUnprotectedAttributeGenerator(final CMSAttributeTableGenerator unprotectedAttributeGenerator) {
        this.unprotectedAttributeGenerator = unprotectedAttributeGenerator;
    }
    
    public void setOriginatorInfo(final OriginatorInformation originatorInformation) {
        this.originatorInfo = originatorInformation.toASN1Structure();
    }
    
    @Deprecated
    public void addKeyTransRecipient(final X509Certificate x509Certificate) throws IllegalArgumentException {
        try {
            this.oldRecipientInfoGenerators.add(new JceKeyTransRecipientInfoGenerator(x509Certificate));
        }
        catch (CertificateEncodingException ex) {
            throw new IllegalArgumentException("unable to encode certificate: " + ex.getMessage());
        }
    }
    
    @Deprecated
    public void addKeyTransRecipient(final PublicKey publicKey, final byte[] array) throws IllegalArgumentException {
        this.oldRecipientInfoGenerators.add(new JceKeyTransRecipientInfoGenerator(array, publicKey));
    }
    
    @Deprecated
    public void addKEKRecipient(final SecretKey secretKey, final byte[] array) {
        this.addKEKRecipient(secretKey, new KEKIdentifier(array, null, null));
    }
    
    @Deprecated
    public void addKEKRecipient(final SecretKey secretKey, final KEKIdentifier kekIdentifier) {
        this.oldRecipientInfoGenerators.add(new JceKEKRecipientInfoGenerator(kekIdentifier, secretKey));
    }
    
    @Deprecated
    public void addPasswordRecipient(final CMSPBEKey cmspbeKey, final String s) {
        this.oldRecipientInfoGenerators.add(new JcePasswordRecipientInfoGenerator(new ASN1ObjectIdentifier(s), cmspbeKey.getPassword()).setSaltAndIterationCount(cmspbeKey.getSalt(), cmspbeKey.getIterationCount()).setPasswordConversionScheme((int)((cmspbeKey instanceof PKCS5Scheme2UTF8PBEKey) ? 1 : 0)));
    }
    
    @Deprecated
    public void addKeyAgreementRecipient(final String s, final PrivateKey privateKey, final PublicKey publicKey, final X509Certificate x509Certificate, final String s2, final String s3) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException {
        this.addKeyAgreementRecipient(s, privateKey, publicKey, x509Certificate, s2, CMSUtils.getProvider(s3));
    }
    
    @Deprecated
    public void addKeyAgreementRecipient(final String s, final PrivateKey privateKey, final PublicKey publicKey, final X509Certificate x509Certificate, final String s2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        final ArrayList<X509Certificate> list = new ArrayList<X509Certificate>();
        list.add(x509Certificate);
        this.addKeyAgreementRecipients(s, privateKey, publicKey, list, s2, provider);
    }
    
    @Deprecated
    public void addKeyAgreementRecipients(final String s, final PrivateKey privateKey, final PublicKey publicKey, final Collection collection, final String s2, final String s3) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException {
        this.addKeyAgreementRecipients(s, privateKey, publicKey, collection, s2, CMSUtils.getProvider(s3));
    }
    
    @Deprecated
    public void addKeyAgreementRecipients(final String s, final PrivateKey privateKey, final PublicKey publicKey, final Collection collection, final String s2, final Provider provider) throws NoSuchAlgorithmException, InvalidKeyException {
        final JceKeyAgreeRecipientInfoGenerator setProvider = new JceKeyAgreeRecipientInfoGenerator(new ASN1ObjectIdentifier(s), privateKey, publicKey, new ASN1ObjectIdentifier(s2)).setProvider(provider);
        final Iterator<X509Certificate> iterator = collection.iterator();
        while (iterator.hasNext()) {
            try {
                setProvider.addRecipient(iterator.next());
                continue;
            }
            catch (CertificateEncodingException ex) {
                throw new IllegalArgumentException("unable to encode certificate: " + ex.getMessage());
            }
            break;
        }
        this.oldRecipientInfoGenerators.add(setProvider);
    }
    
    public void addRecipientInfoGenerator(final RecipientInfoGenerator recipientInfoGenerator) {
        this.recipientInfoGenerators.add(recipientInfoGenerator);
    }
    
    protected AlgorithmIdentifier getAlgorithmIdentifier(final String s, final AlgorithmParameters algorithmParameters) throws IOException {
        ASN1Primitive asn1Primitive;
        if (algorithmParameters != null) {
            asn1Primitive = ASN1Primitive.fromByteArray(algorithmParameters.getEncoded("ASN.1"));
        }
        else {
            asn1Primitive = DERNull.INSTANCE;
        }
        return new AlgorithmIdentifier(new ASN1ObjectIdentifier(s), asn1Primitive);
    }
    
    protected void convertOldRecipients(final SecureRandom secureRandom, final Provider provider) {
        for (final JceKeyTransRecipientInfoGenerator next : this.oldRecipientInfoGenerators) {
            if (next instanceof JceKeyTransRecipientInfoGenerator) {
                final JceKeyTransRecipientInfoGenerator jceKeyTransRecipientInfoGenerator = next;
                if (provider != null) {
                    jceKeyTransRecipientInfoGenerator.setProvider(provider);
                }
                this.recipientInfoGenerators.add(jceKeyTransRecipientInfoGenerator);
            }
            else if (next instanceof KEKRecipientInfoGenerator) {
                final JceKEKRecipientInfoGenerator jceKEKRecipientInfoGenerator = (JceKEKRecipientInfoGenerator)next;
                if (provider != null) {
                    jceKEKRecipientInfoGenerator.setProvider(provider);
                }
                jceKEKRecipientInfoGenerator.setSecureRandom(secureRandom);
                this.recipientInfoGenerators.add(jceKEKRecipientInfoGenerator);
            }
            else if (next instanceof JcePasswordRecipientInfoGenerator) {
                final JcePasswordRecipientInfoGenerator jcePasswordRecipientInfoGenerator = (JcePasswordRecipientInfoGenerator)next;
                if (provider != null) {
                    jcePasswordRecipientInfoGenerator.setProvider(provider);
                }
                jcePasswordRecipientInfoGenerator.setSecureRandom(secureRandom);
                this.recipientInfoGenerators.add(jcePasswordRecipientInfoGenerator);
            }
            else {
                if (!(next instanceof JceKeyAgreeRecipientInfoGenerator)) {
                    continue;
                }
                final JceKeyAgreeRecipientInfoGenerator jceKeyAgreeRecipientInfoGenerator = (JceKeyAgreeRecipientInfoGenerator)next;
                if (provider != null) {
                    jceKeyAgreeRecipientInfoGenerator.setProvider(provider);
                }
                jceKeyAgreeRecipientInfoGenerator.setSecureRandom(secureRandom);
                this.recipientInfoGenerators.add(jceKeyAgreeRecipientInfoGenerator);
            }
        }
        this.oldRecipientInfoGenerators.clear();
    }
    
    static {
        DES_EDE3_CBC = PKCSObjectIdentifiers.des_EDE3_CBC.getId();
        RC2_CBC = PKCSObjectIdentifiers.RC2_CBC.getId();
        AES128_CBC = NISTObjectIdentifiers.id_aes128_CBC.getId();
        AES192_CBC = NISTObjectIdentifiers.id_aes192_CBC.getId();
        AES256_CBC = NISTObjectIdentifiers.id_aes256_CBC.getId();
        CAMELLIA128_CBC = NTTObjectIdentifiers.id_camellia128_cbc.getId();
        CAMELLIA192_CBC = NTTObjectIdentifiers.id_camellia192_cbc.getId();
        CAMELLIA256_CBC = NTTObjectIdentifiers.id_camellia256_cbc.getId();
        SEED_CBC = KISAObjectIdentifiers.id_seedCBC.getId();
        DES_EDE3_WRAP = PKCSObjectIdentifiers.id_alg_CMS3DESwrap.getId();
        AES128_WRAP = NISTObjectIdentifiers.id_aes128_wrap.getId();
        AES192_WRAP = NISTObjectIdentifiers.id_aes192_wrap.getId();
        AES256_WRAP = NISTObjectIdentifiers.id_aes256_wrap.getId();
        CAMELLIA128_WRAP = NTTObjectIdentifiers.id_camellia128_wrap.getId();
        CAMELLIA192_WRAP = NTTObjectIdentifiers.id_camellia192_wrap.getId();
        CAMELLIA256_WRAP = NTTObjectIdentifiers.id_camellia256_wrap.getId();
        SEED_WRAP = KISAObjectIdentifiers.id_npki_app_cmsSeed_wrap.getId();
        ECDH_SHA1KDF = X9ObjectIdentifiers.dhSinglePass_stdDH_sha1kdf_scheme.getId();
        ECMQV_SHA1KDF = X9ObjectIdentifiers.mqvSinglePass_sha1kdf_scheme.getId();
    }
}
