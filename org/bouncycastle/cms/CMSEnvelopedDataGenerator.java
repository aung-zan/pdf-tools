// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.NoSuchProviderException;
import java.util.Iterator;
import org.bouncycastle.operator.GenericKey;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.EnvelopedData;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.BERSet;
import java.util.Map;
import java.util.HashMap;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.cms.EncryptedContentInfo;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.BEROctetString;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.operator.OutputEncryptor;
import java.security.NoSuchAlgorithmException;
import java.io.OutputStream;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import java.io.IOException;
import org.bouncycastle.cms.jcajce.JceCMSContentEncryptorBuilder;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.Provider;
import java.security.SecureRandom;

public class CMSEnvelopedDataGenerator extends CMSEnvelopedGenerator
{
    public CMSEnvelopedDataGenerator() {
    }
    
    @Deprecated
    public CMSEnvelopedDataGenerator(final SecureRandom secureRandom) {
        super(secureRandom);
    }
    
    private CMSEnvelopedData generate(final CMSProcessable cmsProcessable, final String s, final int n, final Provider provider, final Provider provider2) throws NoSuchAlgorithmException, CMSException {
        this.convertOldRecipients(this.rand, provider2);
        JceCMSContentEncryptorBuilder jceCMSContentEncryptorBuilder;
        if (n != -1) {
            jceCMSContentEncryptorBuilder = new JceCMSContentEncryptorBuilder(new ASN1ObjectIdentifier(s), n);
        }
        else {
            jceCMSContentEncryptorBuilder = new JceCMSContentEncryptorBuilder(new ASN1ObjectIdentifier(s));
        }
        jceCMSContentEncryptorBuilder.setProvider(provider);
        jceCMSContentEncryptorBuilder.setSecureRandom(this.rand);
        return this.doGenerate(new CMSTypedData() {
            public ASN1ObjectIdentifier getContentType() {
                return CMSObjectIdentifiers.data;
            }
            
            public void write(final OutputStream outputStream) throws IOException, CMSException {
                cmsProcessable.write(outputStream);
            }
            
            public Object getContent() {
                return cmsProcessable;
            }
        }, jceCMSContentEncryptorBuilder.build());
    }
    
    private CMSEnvelopedData doGenerate(final CMSTypedData cmsTypedData, final OutputEncryptor outputEncryptor) throws CMSException {
        if (!this.oldRecipientInfoGenerators.isEmpty()) {
            throw new IllegalStateException("can only use addRecipientGenerator() with this method");
        }
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            final OutputStream outputStream = outputEncryptor.getOutputStream(byteArrayOutputStream);
            cmsTypedData.write(outputStream);
            outputStream.close();
        }
        catch (IOException ex) {
            throw new CMSException("");
        }
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        final AlgorithmIdentifier algorithmIdentifier = outputEncryptor.getAlgorithmIdentifier();
        final BEROctetString berOctetString = new BEROctetString(byteArray);
        final GenericKey key = outputEncryptor.getKey();
        final Iterator<RecipientInfoGenerator> iterator = this.recipientInfoGenerators.iterator();
        while (iterator.hasNext()) {
            asn1EncodableVector.add(iterator.next().generate(key));
        }
        final EncryptedContentInfo encryptedContentInfo = new EncryptedContentInfo(cmsTypedData.getContentType(), algorithmIdentifier, berOctetString);
        ASN1Set set = null;
        if (this.unprotectedAttributeGenerator != null) {
            set = new BERSet(this.unprotectedAttributeGenerator.getAttributes(new HashMap()).toASN1EncodableVector());
        }
        return new CMSEnvelopedData(new ContentInfo(CMSObjectIdentifiers.envelopedData, new EnvelopedData(this.originatorInfo, new DERSet(asn1EncodableVector), encryptedContentInfo, set)));
    }
    
    @Deprecated
    public CMSEnvelopedData generate(final CMSProcessable cmsProcessable, final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(cmsProcessable, s, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public CMSEnvelopedData generate(final CMSProcessable cmsProcessable, final String s, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        return this.generate(cmsProcessable, s, -1, CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(s, provider).getProvider(), provider);
    }
    
    @Deprecated
    public CMSEnvelopedData generate(final CMSProcessable cmsProcessable, final String s, final int n, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(cmsProcessable, s, n, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public CMSEnvelopedData generate(final CMSProcessable cmsProcessable, final String s, final int n, final Provider provider) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(cmsProcessable, s, n, CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(s, provider).getProvider(), provider);
    }
    
    public CMSEnvelopedData generate(final CMSTypedData cmsTypedData, final OutputEncryptor outputEncryptor) throws CMSException {
        return this.doGenerate(cmsTypedData, outputEncryptor);
    }
}
