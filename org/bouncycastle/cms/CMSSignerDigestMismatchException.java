// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

public class CMSSignerDigestMismatchException extends CMSException
{
    public CMSSignerDigestMismatchException(final String s) {
        super(s);
    }
}
