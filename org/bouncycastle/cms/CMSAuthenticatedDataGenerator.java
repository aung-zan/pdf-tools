// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cms;

import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.cms.jcajce.JceCMSMacCalculatorBuilder;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.Provider;
import javax.crypto.KeyGenerator;
import java.security.SecureRandom;
import java.util.Iterator;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.DigestCalculatorProvider;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.util.HashMap;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.AuthenticatedData;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSet;
import java.util.Map;
import java.util.Collections;
import java.io.IOException;
import org.bouncycastle.asn1.BEROctetString;
import java.io.OutputStream;
import org.bouncycastle.util.io.TeeOutputStream;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.operator.DigestCalculator;
import org.bouncycastle.operator.MacCalculator;

public class CMSAuthenticatedDataGenerator extends CMSAuthenticatedGenerator
{
    public CMSAuthenticatedDataGenerator() {
    }
    
    public CMSAuthenticatedData generate(final CMSTypedData cmsTypedData, final MacCalculator macCalculator) throws CMSException {
        return this.generate(cmsTypedData, macCalculator, null);
    }
    
    public CMSAuthenticatedData generate(final CMSTypedData cmsTypedData, final MacCalculator macCalculator, final DigestCalculator digestCalculator) throws CMSException {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final Iterator<RecipientInfoGenerator> iterator = this.recipientInfoGenerators.iterator();
        while (iterator.hasNext()) {
            asn1EncodableVector.add(iterator.next().generate(macCalculator.getKey()));
        }
        AuthenticatedData authenticatedData;
        if (digestCalculator != null) {
            BEROctetString berOctetString;
            try {
                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                final TeeOutputStream teeOutputStream = new TeeOutputStream(digestCalculator.getOutputStream(), byteArrayOutputStream);
                cmsTypedData.write(teeOutputStream);
                teeOutputStream.close();
                berOctetString = new BEROctetString(byteArrayOutputStream.toByteArray());
            }
            catch (IOException ex) {
                throw new CMSException("unable to perform digest calculation: " + ex.getMessage(), ex);
            }
            final Map baseParameters = this.getBaseParameters(cmsTypedData.getContentType(), digestCalculator.getAlgorithmIdentifier(), digestCalculator.getDigest());
            if (this.authGen == null) {
                this.authGen = new DefaultAuthenticatedAttributeTableGenerator();
            }
            final DERSet set = new DERSet(this.authGen.getAttributes(Collections.unmodifiableMap((Map<?, ?>)baseParameters)).toASN1EncodableVector());
            DEROctetString derOctetString;
            try {
                final OutputStream outputStream = macCalculator.getOutputStream();
                outputStream.write(set.getEncoded("DER"));
                outputStream.close();
                derOctetString = new DEROctetString(macCalculator.getMac());
            }
            catch (IOException ex2) {
                throw new CMSException("exception decoding algorithm parameters.", ex2);
            }
            authenticatedData = new AuthenticatedData(this.originatorInfo, new DERSet(asn1EncodableVector), macCalculator.getAlgorithmIdentifier(), digestCalculator.getAlgorithmIdentifier(), new ContentInfo(CMSObjectIdentifiers.data, berOctetString), set, derOctetString, (this.unauthGen != null) ? new BERSet(this.unauthGen.getAttributes(Collections.unmodifiableMap((Map<?, ?>)baseParameters)).toASN1EncodableVector()) : null);
        }
        else {
            BEROctetString berOctetString2;
            DEROctetString derOctetString2;
            try {
                final ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                final TeeOutputStream teeOutputStream2 = new TeeOutputStream(byteArrayOutputStream2, macCalculator.getOutputStream());
                cmsTypedData.write(teeOutputStream2);
                teeOutputStream2.close();
                berOctetString2 = new BEROctetString(byteArrayOutputStream2.toByteArray());
                derOctetString2 = new DEROctetString(macCalculator.getMac());
            }
            catch (IOException ex3) {
                throw new CMSException("exception decoding algorithm parameters.", ex3);
            }
            authenticatedData = new AuthenticatedData(this.originatorInfo, new DERSet(asn1EncodableVector), macCalculator.getAlgorithmIdentifier(), null, new ContentInfo(CMSObjectIdentifiers.data, berOctetString2), null, derOctetString2, (this.unauthGen != null) ? new BERSet(this.unauthGen.getAttributes(new HashMap()).toASN1EncodableVector()) : null);
        }
        return new CMSAuthenticatedData(new ContentInfo(CMSObjectIdentifiers.authenticatedData, authenticatedData), new DigestCalculatorProvider() {
            public DigestCalculator get(final AlgorithmIdentifier algorithmIdentifier) throws OperatorCreationException {
                return digestCalculator;
            }
        });
    }
    
    @Deprecated
    public CMSAuthenticatedDataGenerator(final SecureRandom secureRandom) {
        super(secureRandom);
    }
    
    @Deprecated
    private CMSAuthenticatedData generate(final CMSProcessable cmsProcessable, final String s, final KeyGenerator keyGenerator, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        final Provider provider2 = keyGenerator.getProvider();
        this.convertOldRecipients(this.rand, provider);
        return this.generate(new CMSTypedData() {
            public ASN1ObjectIdentifier getContentType() {
                return CMSObjectIdentifiers.data;
            }
            
            public void write(final OutputStream outputStream) throws IOException, CMSException {
                cmsProcessable.write(outputStream);
            }
            
            public Object getContent() {
                return cmsProcessable;
            }
        }, new JceCMSMacCalculatorBuilder(new ASN1ObjectIdentifier(s)).setProvider(provider2).setSecureRandom(this.rand).build());
    }
    
    @Deprecated
    public CMSAuthenticatedData generate(final CMSProcessable cmsProcessable, final String s, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return this.generate(cmsProcessable, s, CMSUtils.getProvider(s2));
    }
    
    @Deprecated
    public CMSAuthenticatedData generate(final CMSProcessable cmsProcessable, final String s, final Provider provider) throws NoSuchAlgorithmException, CMSException {
        return this.generate(cmsProcessable, s, CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(s, provider), provider);
    }
}
