// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

public interface Memoable
{
    Memoable copy();
    
    void reset(final Memoable p0);
}
