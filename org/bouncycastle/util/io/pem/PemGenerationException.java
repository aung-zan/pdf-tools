// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.io.pem;

import java.io.IOException;

public class PemGenerationException extends IOException
{
    private Throwable cause;
    
    public PemGenerationException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    public PemGenerationException(final String message) {
        super(message);
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
