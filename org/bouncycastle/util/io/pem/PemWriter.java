// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.io.pem;

import org.bouncycastle.util.encoders.Base64;
import java.io.IOException;
import java.util.Iterator;
import java.io.Writer;
import java.io.BufferedWriter;

public class PemWriter extends BufferedWriter
{
    private static final int LINE_LENGTH = 64;
    private final int nlLength;
    private char[] buf;
    
    public PemWriter(final Writer out) {
        super(out);
        this.buf = new char[64];
        final String property = System.getProperty("line.separator");
        if (property != null) {
            this.nlLength = property.length();
        }
        else {
            this.nlLength = 2;
        }
    }
    
    public int getOutputSize(final PemObject pemObject) {
        int n = 2 * (pemObject.getType().length() + 10 + this.nlLength) + 6 + 4;
        if (!pemObject.getHeaders().isEmpty()) {
            for (final PemHeader pemHeader : pemObject.getHeaders()) {
                n += pemHeader.getName().length() + ": ".length() + pemHeader.getValue().length() + this.nlLength;
            }
            n += this.nlLength;
        }
        final int n2 = (pemObject.getContent().length + 2) / 3 * 4;
        return n + (n2 + (n2 + 64 - 1) / 64 * this.nlLength);
    }
    
    public void writeObject(final PemObjectGenerator pemObjectGenerator) throws IOException {
        final PemObject generate = pemObjectGenerator.generate();
        this.writePreEncapsulationBoundary(generate.getType());
        if (!generate.getHeaders().isEmpty()) {
            for (final PemHeader pemHeader : generate.getHeaders()) {
                this.write(pemHeader.getName());
                this.write(": ");
                this.write(pemHeader.getValue());
                this.newLine();
            }
            this.newLine();
        }
        this.writeEncoded(generate.getContent());
        this.writePostEncapsulationBoundary(generate.getType());
    }
    
    private void writeEncoded(byte[] encode) throws IOException {
        encode = Base64.encode(encode);
        for (int i = 0; i < encode.length; i += this.buf.length) {
            int len;
            for (len = 0; len != this.buf.length && i + len < encode.length; ++len) {
                this.buf[len] = (char)encode[i + len];
            }
            this.write(this.buf, 0, len);
            this.newLine();
        }
    }
    
    private void writePreEncapsulationBoundary(final String str) throws IOException {
        this.write("-----BEGIN " + str + "-----");
        this.newLine();
    }
    
    private void writePostEncapsulationBoundary(final String str) throws IOException {
        this.write("-----END " + str + "-----");
        this.newLine();
    }
}
