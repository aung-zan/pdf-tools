// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.io.pem;

public interface PemObjectGenerator
{
    PemObject generate() throws PemGenerationException;
}
