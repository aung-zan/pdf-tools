// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.encoders;

public class DecoderException extends IllegalStateException
{
    private Throwable cause;
    
    DecoderException(final String s, final Throwable cause) {
        super(s);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
