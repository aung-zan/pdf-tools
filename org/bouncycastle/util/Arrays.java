// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

import java.math.BigInteger;

public final class Arrays
{
    private Arrays() {
    }
    
    public static boolean areEqual(final boolean[] array, final boolean[] array2) {
        if (array == array2) {
            return true;
        }
        if (array == null || array2 == null) {
            return false;
        }
        if (array.length != array2.length) {
            return false;
        }
        for (int i = 0; i != array.length; ++i) {
            if (array[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean areEqual(final char[] array, final char[] array2) {
        if (array == array2) {
            return true;
        }
        if (array == null || array2 == null) {
            return false;
        }
        if (array.length != array2.length) {
            return false;
        }
        for (int i = 0; i != array.length; ++i) {
            if (array[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean areEqual(final byte[] array, final byte[] array2) {
        if (array == array2) {
            return true;
        }
        if (array == null || array2 == null) {
            return false;
        }
        if (array.length != array2.length) {
            return false;
        }
        for (int i = 0; i != array.length; ++i) {
            if (array[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean constantTimeAreEqual(final byte[] array, final byte[] array2) {
        if (array == array2) {
            return true;
        }
        if (array == null || array2 == null) {
            return false;
        }
        if (array.length != array2.length) {
            return false;
        }
        int n = 0;
        for (int i = 0; i != array.length; ++i) {
            n |= (array[i] ^ array2[i]);
        }
        return n == 0;
    }
    
    public static boolean areEqual(final int[] array, final int[] array2) {
        if (array == array2) {
            return true;
        }
        if (array == null || array2 == null) {
            return false;
        }
        if (array.length != array2.length) {
            return false;
        }
        for (int i = 0; i != array.length; ++i) {
            if (array[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean areEqual(final long[] array, final long[] array2) {
        if (array == array2) {
            return true;
        }
        if (array == null || array2 == null) {
            return false;
        }
        if (array.length != array2.length) {
            return false;
        }
        for (int i = 0; i != array.length; ++i) {
            if (array[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean areEqual(final BigInteger[] array, final BigInteger[] array2) {
        if (array == array2) {
            return true;
        }
        if (array == null || array2 == null) {
            return false;
        }
        if (array.length != array2.length) {
            return false;
        }
        for (int i = 0; i != array.length; ++i) {
            if (!array[i].equals(array2[i])) {
                return false;
            }
        }
        return true;
    }
    
    public static void fill(final byte[] array, final byte b) {
        for (int i = 0; i < array.length; ++i) {
            array[i] = b;
        }
    }
    
    public static void fill(final char[] array, final char c) {
        for (int i = 0; i < array.length; ++i) {
            array[i] = c;
        }
    }
    
    public static void fill(final long[] array, final long n) {
        for (int i = 0; i < array.length; ++i) {
            array[i] = n;
        }
    }
    
    public static void fill(final short[] array, final short n) {
        for (int i = 0; i < array.length; ++i) {
            array[i] = n;
        }
    }
    
    public static void fill(final int[] array, final int n) {
        for (int i = 0; i < array.length; ++i) {
            array[i] = n;
        }
    }
    
    public static int hashCode(final byte[] array) {
        if (array == null) {
            return 0;
        }
        int length = array.length;
        int n = length + 1;
        while (--length >= 0) {
            n = (n * 257 ^ array[length]);
        }
        return n;
    }
    
    public static int hashCode(final char[] array) {
        if (array == null) {
            return 0;
        }
        int length = array.length;
        int n = length + 1;
        while (--length >= 0) {
            n = (n * 257 ^ array[length]);
        }
        return n;
    }
    
    public static int hashCode(final int[][] array) {
        int n = 0;
        for (int i = 0; i != array.length; ++i) {
            n = n * 257 + hashCode(array[i]);
        }
        return n;
    }
    
    public static int hashCode(final int[] array) {
        if (array == null) {
            return 0;
        }
        int length = array.length;
        int n = length + 1;
        while (--length >= 0) {
            n = (n * 257 ^ array[length]);
        }
        return n;
    }
    
    public static int hashCode(final short[][][] array) {
        int n = 0;
        for (int i = 0; i != array.length; ++i) {
            n = n * 257 + hashCode(array[i]);
        }
        return n;
    }
    
    public static int hashCode(final short[][] array) {
        int n = 0;
        for (int i = 0; i != array.length; ++i) {
            n = n * 257 + hashCode(array[i]);
        }
        return n;
    }
    
    public static int hashCode(final short[] array) {
        if (array == null) {
            return 0;
        }
        int length = array.length;
        int n = length + 1;
        while (--length >= 0) {
            n = (n * 257 ^ (array[length] & 0xFF));
        }
        return n;
    }
    
    public static int hashCode(final BigInteger[] array) {
        if (array == null) {
            return 0;
        }
        int length = array.length;
        int n = length + 1;
        while (--length >= 0) {
            n = (n * 257 ^ array[length].hashCode());
        }
        return n;
    }
    
    public static byte[] clone(final byte[] array) {
        if (array == null) {
            return null;
        }
        final byte[] array2 = new byte[array.length];
        System.arraycopy(array, 0, array2, 0, array.length);
        return array2;
    }
    
    public static byte[][] clone(final byte[][] array) {
        if (array == null) {
            return null;
        }
        final byte[][] array2 = new byte[array.length][];
        for (int i = 0; i != array2.length; ++i) {
            array2[i] = clone(array[i]);
        }
        return array2;
    }
    
    public static byte[][][] clone(final byte[][][] array) {
        if (array == null) {
            return null;
        }
        final byte[][][] array2 = new byte[array.length][][];
        for (int i = 0; i != array2.length; ++i) {
            array2[i] = clone(array[i]);
        }
        return array2;
    }
    
    public static int[] clone(final int[] array) {
        if (array == null) {
            return null;
        }
        final int[] array2 = new int[array.length];
        System.arraycopy(array, 0, array2, 0, array.length);
        return array2;
    }
    
    public static short[] clone(final short[] array) {
        if (array == null) {
            return null;
        }
        final short[] array2 = new short[array.length];
        System.arraycopy(array, 0, array2, 0, array.length);
        return array2;
    }
    
    public static BigInteger[] clone(final BigInteger[] array) {
        if (array == null) {
            return null;
        }
        final BigInteger[] array2 = new BigInteger[array.length];
        System.arraycopy(array, 0, array2, 0, array.length);
        return array2;
    }
    
    public static byte[] copyOf(final byte[] array, final int n) {
        final byte[] array2 = new byte[n];
        if (n < array.length) {
            System.arraycopy(array, 0, array2, 0, n);
        }
        else {
            System.arraycopy(array, 0, array2, 0, array.length);
        }
        return array2;
    }
    
    public static char[] copyOf(final char[] array, final int n) {
        final char[] array2 = new char[n];
        if (n < array.length) {
            System.arraycopy(array, 0, array2, 0, n);
        }
        else {
            System.arraycopy(array, 0, array2, 0, array.length);
        }
        return array2;
    }
    
    public static int[] copyOf(final int[] array, final int n) {
        final int[] array2 = new int[n];
        if (n < array.length) {
            System.arraycopy(array, 0, array2, 0, n);
        }
        else {
            System.arraycopy(array, 0, array2, 0, array.length);
        }
        return array2;
    }
    
    public static long[] copyOf(final long[] array, final int n) {
        final long[] array2 = new long[n];
        if (n < array.length) {
            System.arraycopy(array, 0, array2, 0, n);
        }
        else {
            System.arraycopy(array, 0, array2, 0, array.length);
        }
        return array2;
    }
    
    public static BigInteger[] copyOf(final BigInteger[] array, final int n) {
        final BigInteger[] array2 = new BigInteger[n];
        if (n < array.length) {
            System.arraycopy(array, 0, array2, 0, n);
        }
        else {
            System.arraycopy(array, 0, array2, 0, array.length);
        }
        return array2;
    }
    
    public static byte[] copyOfRange(final byte[] array, final int n, final int n2) {
        final int length = getLength(n, n2);
        final byte[] array2 = new byte[length];
        if (array.length - n < length) {
            System.arraycopy(array, n, array2, 0, array.length - n);
        }
        else {
            System.arraycopy(array, n, array2, 0, length);
        }
        return array2;
    }
    
    public static int[] copyOfRange(final int[] array, final int n, final int n2) {
        final int length = getLength(n, n2);
        final int[] array2 = new int[length];
        if (array.length - n < length) {
            System.arraycopy(array, n, array2, 0, array.length - n);
        }
        else {
            System.arraycopy(array, n, array2, 0, length);
        }
        return array2;
    }
    
    public static long[] copyOfRange(final long[] array, final int n, final int n2) {
        final int length = getLength(n, n2);
        final long[] array2 = new long[length];
        if (array.length - n < length) {
            System.arraycopy(array, n, array2, 0, array.length - n);
        }
        else {
            System.arraycopy(array, n, array2, 0, length);
        }
        return array2;
    }
    
    public static BigInteger[] copyOfRange(final BigInteger[] array, final int n, final int n2) {
        final int length = getLength(n, n2);
        final BigInteger[] array2 = new BigInteger[length];
        if (array.length - n < length) {
            System.arraycopy(array, n, array2, 0, array.length - n);
        }
        else {
            System.arraycopy(array, n, array2, 0, length);
        }
        return array2;
    }
    
    private static int getLength(final int capacity, final int i) {
        final int n = i - capacity;
        if (n < 0) {
            final StringBuffer sb = new StringBuffer(capacity);
            sb.append(" > ").append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        return n;
    }
    
    public static byte[] concatenate(final byte[] array, final byte[] array2) {
        if (array != null && array2 != null) {
            final byte[] array3 = new byte[array.length + array2.length];
            System.arraycopy(array, 0, array3, 0, array.length);
            System.arraycopy(array2, 0, array3, array.length, array2.length);
            return array3;
        }
        if (array2 != null) {
            return clone(array2);
        }
        return clone(array);
    }
    
    public static byte[] concatenate(final byte[] array, final byte[] array2, final byte[] array3) {
        if (array != null && array2 != null && array3 != null) {
            final byte[] array4 = new byte[array.length + array2.length + array3.length];
            System.arraycopy(array, 0, array4, 0, array.length);
            System.arraycopy(array2, 0, array4, array.length, array2.length);
            System.arraycopy(array3, 0, array4, array.length + array2.length, array3.length);
            return array4;
        }
        if (array2 == null) {
            return concatenate(array, array3);
        }
        return concatenate(array, array2);
    }
    
    public static byte[] concatenate(final byte[] array, final byte[] array2, final byte[] array3, final byte[] array4) {
        if (array != null && array2 != null && array3 != null && array4 != null) {
            final byte[] array5 = new byte[array.length + array2.length + array3.length + array4.length];
            System.arraycopy(array, 0, array5, 0, array.length);
            System.arraycopy(array2, 0, array5, array.length, array2.length);
            System.arraycopy(array3, 0, array5, array.length + array2.length, array3.length);
            System.arraycopy(array4, 0, array5, array.length + array2.length + array3.length, array4.length);
            return array5;
        }
        if (array4 == null) {
            return concatenate(array, array2, array3);
        }
        if (array3 == null) {
            return concatenate(array, array2, array4);
        }
        if (array2 == null) {
            return concatenate(array, array3, array4);
        }
        return concatenate(array2, array3, array4);
    }
}
