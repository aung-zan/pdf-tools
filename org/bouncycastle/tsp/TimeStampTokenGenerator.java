// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.tsp;

import org.bouncycastle.cms.CMSSignedGenerator;
import org.bouncycastle.jce.interfaces.GOST3410PrivateKey;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.RSAPrivateKey;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.CMSProcessableByteArray;
import java.util.Collection;
import org.bouncycastle.util.CollectionStore;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.asn1.tsp.TSTInfo;
import org.bouncycastle.asn1.ASN1GeneralizedTime;
import org.bouncycastle.asn1.ASN1Boolean;
import org.bouncycastle.asn1.tsp.Accuracy;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.tsp.MessageImprint;
import java.security.NoSuchProviderException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.cms.SimpleAttributeTableGenerator;
import org.bouncycastle.cms.DefaultSignedAttributeTableGenerator;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import java.util.Date;
import java.math.BigInteger;
import org.bouncycastle.util.Selector;
import org.bouncycastle.util.Store;
import java.security.cert.CertStoreException;
import java.security.cert.CRL;
import java.security.cert.Certificate;
import java.util.Iterator;
import java.security.cert.CRLException;
import org.bouncycastle.cert.jcajce.JcaX509CRLHolder;
import java.security.cert.X509CRL;
import java.security.cert.CRLSelector;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.CertificateEncodingException;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.Attribute;
import java.util.Hashtable;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import org.bouncycastle.asn1.DERNull;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.IOException;
import org.bouncycastle.asn1.ess.SigningCertificateV2;
import org.bouncycastle.asn1.ess.ESSCertIDv2;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.cms.CMSAttributeTableGenerationException;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ess.SigningCertificate;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.util.Map;
import org.bouncycastle.cms.CMSAttributeTableGenerator;
import org.bouncycastle.asn1.ess.ESSCertID;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import java.util.ArrayList;
import org.bouncycastle.operator.DigestCalculator;
import org.bouncycastle.cms.SignerInfoGenerator;
import java.util.List;
import org.bouncycastle.asn1.cms.AttributeTable;
import java.security.cert.X509Certificate;
import java.security.PrivateKey;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x509.GeneralName;

public class TimeStampTokenGenerator
{
    int accuracySeconds;
    int accuracyMillis;
    int accuracyMicros;
    boolean ordering;
    GeneralName tsa;
    private ASN1ObjectIdentifier tsaPolicyOID;
    PrivateKey key;
    X509Certificate cert;
    String digestOID;
    AttributeTable signedAttr;
    AttributeTable unsignedAttr;
    private List certs;
    private List crls;
    private List attrCerts;
    private SignerInfoGenerator signerInfoGen;
    
    public TimeStampTokenGenerator(final SignerInfoGenerator signerInfoGen, final DigestCalculator digestCalculator, final ASN1ObjectIdentifier tsaPolicyOID) throws IllegalArgumentException, TSPException {
        this.accuracySeconds = -1;
        this.accuracyMillis = -1;
        this.accuracyMicros = -1;
        this.ordering = false;
        this.tsa = null;
        this.certs = new ArrayList();
        this.crls = new ArrayList();
        this.attrCerts = new ArrayList();
        this.signerInfoGen = signerInfoGen;
        this.tsaPolicyOID = tsaPolicyOID;
        if (!signerInfoGen.hasAssociatedCertificate()) {
            throw new IllegalArgumentException("SignerInfoGenerator must have an associated certificate");
        }
        TSPUtil.validateCertificate(signerInfoGen.getAssociatedCertificate());
        try {
            final OutputStream outputStream = digestCalculator.getOutputStream();
            outputStream.write(signerInfoGen.getAssociatedCertificate().getEncoded());
            outputStream.close();
            if (digestCalculator.getAlgorithmIdentifier().getAlgorithm().equals(OIWObjectIdentifiers.idSHA1)) {
                this.signerInfoGen = new SignerInfoGenerator(signerInfoGen, new CMSAttributeTableGenerator() {
                    final /* synthetic */ ESSCertID val$essCertid = new ESSCertID(digestCalculator.getDigest());
                    
                    public AttributeTable getAttributes(final Map map) throws CMSAttributeTableGenerationException {
                        final AttributeTable attributes = signerInfoGen.getSignedAttributeTableGenerator().getAttributes(map);
                        if (attributes.get(PKCSObjectIdentifiers.id_aa_signingCertificate) == null) {
                            return attributes.add(PKCSObjectIdentifiers.id_aa_signingCertificate, new SigningCertificate(this.val$essCertid));
                        }
                        return attributes;
                    }
                }, signerInfoGen.getUnsignedAttributeTableGenerator());
            }
            else {
                this.signerInfoGen = new SignerInfoGenerator(signerInfoGen, new CMSAttributeTableGenerator() {
                    final /* synthetic */ ESSCertIDv2 val$essCertid = new ESSCertIDv2(new AlgorithmIdentifier(digestCalculator.getAlgorithmIdentifier().getAlgorithm()), digestCalculator.getDigest());
                    
                    public AttributeTable getAttributes(final Map map) throws CMSAttributeTableGenerationException {
                        final AttributeTable attributes = signerInfoGen.getSignedAttributeTableGenerator().getAttributes(map);
                        if (attributes.get(PKCSObjectIdentifiers.id_aa_signingCertificateV2) == null) {
                            return attributes.add(PKCSObjectIdentifiers.id_aa_signingCertificateV2, new SigningCertificateV2(this.val$essCertid));
                        }
                        return attributes;
                    }
                }, signerInfoGen.getUnsignedAttributeTableGenerator());
            }
        }
        catch (IOException ex) {
            throw new TSPException("Exception processing certificate.", ex);
        }
    }
    
    @Deprecated
    public TimeStampTokenGenerator(final DigestCalculator digestCalculator, final SignerInfoGenerator signerInfoGenerator, final ASN1ObjectIdentifier asn1ObjectIdentifier) throws IllegalArgumentException, TSPException {
        this(signerInfoGenerator, digestCalculator, asn1ObjectIdentifier);
    }
    
    @Deprecated
    public TimeStampTokenGenerator(final SignerInfoGenerator signerInfoGenerator, final ASN1ObjectIdentifier asn1ObjectIdentifier) throws IllegalArgumentException, TSPException {
        this(new DigestCalculator() {
            private ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            
            public AlgorithmIdentifier getAlgorithmIdentifier() {
                return new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1, DERNull.INSTANCE);
            }
            
            public OutputStream getOutputStream() {
                return this.bOut;
            }
            
            public byte[] getDigest() {
                try {
                    return MessageDigest.getInstance("SHA-1").digest(this.bOut.toByteArray());
                }
                catch (NoSuchAlgorithmException ex) {
                    throw new IllegalStateException("cannot find sha-1: " + ex.getMessage());
                }
            }
        }, signerInfoGenerator, asn1ObjectIdentifier);
    }
    
    @Deprecated
    public TimeStampTokenGenerator(final PrivateKey privateKey, final X509Certificate x509Certificate, final String s, final String s2) throws IllegalArgumentException, TSPException {
        this(privateKey, x509Certificate, s, s2, null, null);
    }
    
    @Deprecated
    public TimeStampTokenGenerator(final PrivateKey privateKey, final X509Certificate x509Certificate, final ASN1ObjectIdentifier asn1ObjectIdentifier, final String s) throws IllegalArgumentException, TSPException {
        this(privateKey, x509Certificate, asn1ObjectIdentifier.getId(), s, null, null);
    }
    
    @Deprecated
    public TimeStampTokenGenerator(final PrivateKey key, final X509Certificate cert, final String digestOID, final String s, final AttributeTable attributeTable, final AttributeTable unsignedAttr) throws IllegalArgumentException, TSPException {
        this.accuracySeconds = -1;
        this.accuracyMillis = -1;
        this.accuracyMicros = -1;
        this.ordering = false;
        this.tsa = null;
        this.certs = new ArrayList();
        this.crls = new ArrayList();
        this.attrCerts = new ArrayList();
        this.key = key;
        this.cert = cert;
        this.digestOID = digestOID;
        this.tsaPolicyOID = new ASN1ObjectIdentifier(s);
        this.unsignedAttr = unsignedAttr;
        Hashtable<ASN1ObjectIdentifier, Attribute> hashtable;
        if (attributeTable != null) {
            hashtable = (Hashtable<ASN1ObjectIdentifier, Attribute>)attributeTable.toHashtable();
        }
        else {
            hashtable = new Hashtable<ASN1ObjectIdentifier, Attribute>();
        }
        TSPUtil.validateCertificate(cert);
        try {
            hashtable.put(PKCSObjectIdentifiers.id_aa_signingCertificate, new Attribute(PKCSObjectIdentifiers.id_aa_signingCertificate, new DERSet(new SigningCertificate(new ESSCertID(MessageDigest.getInstance("SHA-1").digest(cert.getEncoded()))))));
        }
        catch (NoSuchAlgorithmException ex) {
            throw new TSPException("Can't find a SHA-1 implementation.", ex);
        }
        catch (CertificateEncodingException ex2) {
            throw new TSPException("Exception processing certificate.", ex2);
        }
        this.signedAttr = new AttributeTable(hashtable);
    }
    
    @Deprecated
    public void setCertificatesAndCRLs(final CertStore certStore) throws CertStoreException, TSPException {
        final Iterator<? extends Certificate> iterator = certStore.getCertificates(null).iterator();
        while (iterator.hasNext()) {
            try {
                this.certs.add(new JcaX509CertificateHolder((X509Certificate)iterator.next()));
                continue;
            }
            catch (CertificateEncodingException ex) {
                throw new TSPException("cannot encode certificate: " + ex.getMessage(), ex);
            }
            break;
        }
        final Iterator<? extends CRL> iterator2 = certStore.getCRLs(null).iterator();
        while (iterator2.hasNext()) {
            try {
                this.crls.add(new JcaX509CRLHolder((X509CRL)iterator2.next()));
                continue;
            }
            catch (CRLException ex2) {
                throw new TSPException("cannot encode CRL: " + ex2.getMessage(), ex2);
            }
            break;
        }
    }
    
    public void addCertificates(final Store store) {
        this.certs.addAll(store.getMatches(null));
    }
    
    public void addCRLs(final Store store) {
        this.crls.addAll(store.getMatches(null));
    }
    
    public void addAttributeCertificates(final Store store) {
        this.attrCerts.addAll(store.getMatches(null));
    }
    
    public void setAccuracySeconds(final int accuracySeconds) {
        this.accuracySeconds = accuracySeconds;
    }
    
    public void setAccuracyMillis(final int accuracyMillis) {
        this.accuracyMillis = accuracyMillis;
    }
    
    public void setAccuracyMicros(final int accuracyMicros) {
        this.accuracyMicros = accuracyMicros;
    }
    
    public void setOrdering(final boolean ordering) {
        this.ordering = ordering;
    }
    
    public void setTSA(final GeneralName tsa) {
        this.tsa = tsa;
    }
    
    public TimeStampToken generate(final TimeStampRequest timeStampRequest, final BigInteger bigInteger, final Date date, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, TSPException {
        if (this.signerInfoGen == null) {
            try {
                final JcaSignerInfoGeneratorBuilder jcaSignerInfoGeneratorBuilder = new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().setProvider(s).build());
                jcaSignerInfoGeneratorBuilder.setSignedAttributeGenerator(new DefaultSignedAttributeTableGenerator(this.signedAttr));
                if (this.unsignedAttr != null) {
                    jcaSignerInfoGeneratorBuilder.setUnsignedAttributeGenerator(new SimpleAttributeTableGenerator(this.unsignedAttr));
                }
                this.signerInfoGen = jcaSignerInfoGeneratorBuilder.build(new JcaContentSignerBuilder(this.getSigAlgorithm(this.key, this.digestOID)).setProvider(s).build(this.key), this.cert);
            }
            catch (OperatorCreationException ex) {
                throw new TSPException("Error generating signing operator", ex);
            }
            catch (CertificateEncodingException ex2) {
                throw new TSPException("Error encoding certificate", ex2);
            }
        }
        return this.generate(timeStampRequest, bigInteger, date);
    }
    
    public TimeStampToken generate(final TimeStampRequest timeStampRequest, final BigInteger bigInteger, final Date date) throws TSPException {
        if (this.signerInfoGen == null) {
            throw new IllegalStateException("can only use this method with SignerInfoGenerator constructor");
        }
        final MessageImprint messageImprint = new MessageImprint(new AlgorithmIdentifier(timeStampRequest.getMessageImprintAlgOID(), DERNull.INSTANCE), timeStampRequest.getMessageImprintDigest());
        Accuracy accuracy = null;
        if (this.accuracySeconds > 0 || this.accuracyMillis > 0 || this.accuracyMicros > 0) {
            ASN1Integer asn1Integer = null;
            if (this.accuracySeconds > 0) {
                asn1Integer = new ASN1Integer(this.accuracySeconds);
            }
            ASN1Integer asn1Integer2 = null;
            if (this.accuracyMillis > 0) {
                asn1Integer2 = new ASN1Integer(this.accuracyMillis);
            }
            ASN1Integer asn1Integer3 = null;
            if (this.accuracyMicros > 0) {
                asn1Integer3 = new ASN1Integer(this.accuracyMicros);
            }
            accuracy = new Accuracy(asn1Integer, asn1Integer2, asn1Integer3);
        }
        ASN1Boolean asn1Boolean = null;
        if (this.ordering) {
            asn1Boolean = new ASN1Boolean(this.ordering);
        }
        ASN1Integer asn1Integer4 = null;
        if (timeStampRequest.getNonce() != null) {
            asn1Integer4 = new ASN1Integer(timeStampRequest.getNonce());
        }
        ASN1ObjectIdentifier asn1ObjectIdentifier = this.tsaPolicyOID;
        if (timeStampRequest.getReqPolicy() != null) {
            asn1ObjectIdentifier = timeStampRequest.getReqPolicy();
        }
        final TSTInfo tstInfo = new TSTInfo(asn1ObjectIdentifier, messageImprint, new ASN1Integer(bigInteger), new ASN1GeneralizedTime(date), accuracy, asn1Boolean, asn1Integer4, this.tsa, timeStampRequest.getExtensions());
        try {
            final CMSSignedDataGenerator cmsSignedDataGenerator = new CMSSignedDataGenerator();
            if (timeStampRequest.getCertReq()) {
                cmsSignedDataGenerator.addCertificates(new CollectionStore(this.certs));
                cmsSignedDataGenerator.addCRLs(new CollectionStore(this.crls));
                cmsSignedDataGenerator.addAttributeCertificates(new CollectionStore(this.attrCerts));
            }
            else {
                cmsSignedDataGenerator.addCRLs(new CollectionStore(this.crls));
            }
            cmsSignedDataGenerator.addSignerInfoGenerator(this.signerInfoGen);
            return new TimeStampToken(cmsSignedDataGenerator.generate(new CMSProcessableByteArray(PKCSObjectIdentifiers.id_ct_TSTInfo, tstInfo.getEncoded("DER")), true));
        }
        catch (CMSException ex) {
            throw new TSPException("Error generating time-stamp token", ex);
        }
        catch (IOException ex2) {
            throw new TSPException("Exception encoding info", ex2);
        }
    }
    
    private String getSigAlgorithm(final PrivateKey privateKey, final String s) {
        String encryption_ECGOST3410 = null;
        if (privateKey instanceof RSAPrivateKey || "RSA".equalsIgnoreCase(privateKey.getAlgorithm())) {
            encryption_ECGOST3410 = "RSA";
        }
        else if (privateKey instanceof DSAPrivateKey || "DSA".equalsIgnoreCase(privateKey.getAlgorithm())) {
            encryption_ECGOST3410 = "DSA";
        }
        else if ("ECDSA".equalsIgnoreCase(privateKey.getAlgorithm()) || "EC".equalsIgnoreCase(privateKey.getAlgorithm())) {
            encryption_ECGOST3410 = "ECDSA";
        }
        else if (privateKey instanceof GOST3410PrivateKey || "GOST3410".equalsIgnoreCase(privateKey.getAlgorithm())) {
            encryption_ECGOST3410 = "GOST3410";
        }
        else if ("ECGOST3410".equalsIgnoreCase(privateKey.getAlgorithm())) {
            encryption_ECGOST3410 = CMSSignedGenerator.ENCRYPTION_ECGOST3410;
        }
        return TSPUtil.getDigestAlgName(s) + "with" + encryption_ECGOST3410;
    }
}
