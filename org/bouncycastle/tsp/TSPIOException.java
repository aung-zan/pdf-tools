// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.tsp;

import java.io.IOException;

public class TSPIOException extends IOException
{
    Throwable underlyingException;
    
    public TSPIOException(final String message) {
        super(message);
    }
    
    public TSPIOException(final String message, final Throwable underlyingException) {
        super(message);
        this.underlyingException = underlyingException;
    }
    
    public Exception getUnderlyingException() {
        return (Exception)this.underlyingException;
    }
    
    @Override
    public Throwable getCause() {
        return this.underlyingException;
    }
}
