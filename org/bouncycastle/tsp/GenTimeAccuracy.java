// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.tsp;

import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.tsp.Accuracy;

public class GenTimeAccuracy
{
    private Accuracy accuracy;
    
    public GenTimeAccuracy(final Accuracy accuracy) {
        this.accuracy = accuracy;
    }
    
    public int getSeconds() {
        return this.getTimeComponent(this.accuracy.getSeconds());
    }
    
    public int getMillis() {
        return this.getTimeComponent(this.accuracy.getMillis());
    }
    
    public int getMicros() {
        return this.getTimeComponent(this.accuracy.getMicros());
    }
    
    private int getTimeComponent(final DERInteger derInteger) {
        if (derInteger != null) {
            return derInteger.getValue().intValue();
        }
        return 0;
    }
    
    @Override
    public String toString() {
        return this.getSeconds() + "." + this.format(this.getMillis()) + this.format(this.getMicros());
    }
    
    private String format(final int i) {
        if (i < 10) {
            return "00" + i;
        }
        if (i < 100) {
            return "0" + i;
        }
        return Integer.toString(i);
    }
}
