// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.tsp;

public class TSPException extends Exception
{
    Throwable underlyingException;
    
    public TSPException(final String message) {
        super(message);
    }
    
    public TSPException(final String message, final Throwable underlyingException) {
        super(message);
        this.underlyingException = underlyingException;
    }
    
    public Exception getUnderlyingException() {
        return (Exception)this.underlyingException;
    }
    
    @Override
    public Throwable getCause() {
        return this.underlyingException;
    }
}
