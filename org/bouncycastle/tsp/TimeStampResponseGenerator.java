// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.tsp;

import org.bouncycastle.asn1.DERBitString;
import java.util.Iterator;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.HashSet;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.asn1.tsp.TimeStampResp;
import java.io.IOException;
import org.bouncycastle.asn1.cms.ContentInfo;
import java.io.InputStream;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.ByteArrayInputStream;
import java.util.Date;
import java.math.BigInteger;
import org.bouncycastle.asn1.cmp.PKIFreeText;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.cmp.PKIStatusInfo;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERUTF8String;
import java.util.Set;
import org.bouncycastle.asn1.ASN1EncodableVector;

public class TimeStampResponseGenerator
{
    int status;
    ASN1EncodableVector statusStrings;
    int failInfo;
    private TimeStampTokenGenerator tokenGenerator;
    private Set acceptedAlgorithms;
    private Set acceptedPolicies;
    private Set acceptedExtensions;
    
    public TimeStampResponseGenerator(final TimeStampTokenGenerator timeStampTokenGenerator, final Set set) {
        this(timeStampTokenGenerator, set, null, null);
    }
    
    public TimeStampResponseGenerator(final TimeStampTokenGenerator timeStampTokenGenerator, final Set set, final Set set2) {
        this(timeStampTokenGenerator, set, set2, null);
    }
    
    public TimeStampResponseGenerator(final TimeStampTokenGenerator tokenGenerator, final Set set, final Set set2, final Set set3) {
        this.tokenGenerator = tokenGenerator;
        this.acceptedAlgorithms = this.convert(set);
        this.acceptedPolicies = this.convert(set2);
        this.acceptedExtensions = this.convert(set3);
        this.statusStrings = new ASN1EncodableVector();
    }
    
    private void addStatusString(final String s) {
        this.statusStrings.add(new DERUTF8String(s));
    }
    
    private void setFailInfoField(final int n) {
        this.failInfo |= n;
    }
    
    private PKIStatusInfo getPKIStatusInfo() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        asn1EncodableVector.add(new DERInteger(this.status));
        if (this.statusStrings.size() > 0) {
            asn1EncodableVector.add(PKIFreeText.getInstance(new DERSequence(this.statusStrings)));
        }
        if (this.failInfo != 0) {
            asn1EncodableVector.add(new FailInfo(this.failInfo));
        }
        return PKIStatusInfo.getInstance(new DERSequence(asn1EncodableVector));
    }
    
    @Deprecated
    public TimeStampResponse generate(final TimeStampRequest timeStampRequest, final BigInteger bigInteger, final Date date, final String s) throws NoSuchAlgorithmException, NoSuchProviderException, TSPException {
        TimeStampResp timeStampResp;
        try {
            if (date == null) {
                throw new TSPValidationException("The time source is not available.", 512);
            }
            timeStampRequest.validate(this.acceptedAlgorithms, this.acceptedPolicies, this.acceptedExtensions, s);
            this.status = 0;
            this.addStatusString("Operation Okay");
            final PKIStatusInfo pkiStatusInfo = this.getPKIStatusInfo();
            ContentInfo instance;
            try {
                instance = ContentInfo.getInstance(new ASN1InputStream(new ByteArrayInputStream(this.tokenGenerator.generate(timeStampRequest, bigInteger, date, s).toCMSSignedData().getEncoded())).readObject());
            }
            catch (IOException ex) {
                throw new TSPException("Timestamp token received cannot be converted to ContentInfo", ex);
            }
            timeStampResp = new TimeStampResp(pkiStatusInfo, instance);
        }
        catch (TSPValidationException ex2) {
            this.status = 2;
            this.setFailInfoField(ex2.getFailureCode());
            this.addStatusString(ex2.getMessage());
            timeStampResp = new TimeStampResp(this.getPKIStatusInfo(), null);
        }
        try {
            return new TimeStampResponse(timeStampResp);
        }
        catch (IOException ex3) {
            throw new TSPException("created badly formatted response!");
        }
    }
    
    public TimeStampResponse generate(final TimeStampRequest timeStampRequest, final BigInteger bigInteger, final Date date) throws TSPException {
        try {
            return this.generateGrantedResponse(timeStampRequest, bigInteger, date, "Operation Okay");
        }
        catch (Exception ex) {
            return this.generateRejectedResponse(ex);
        }
    }
    
    public TimeStampResponse generateGrantedResponse(final TimeStampRequest timeStampRequest, final BigInteger bigInteger, final Date date) throws TSPException {
        return this.generateGrantedResponse(timeStampRequest, bigInteger, date, null);
    }
    
    public TimeStampResponse generateGrantedResponse(final TimeStampRequest timeStampRequest, final BigInteger bigInteger, final Date date, final String s) throws TSPException {
        if (date == null) {
            throw new TSPValidationException("The time source is not available.", 512);
        }
        timeStampRequest.validate(this.acceptedAlgorithms, this.acceptedPolicies, this.acceptedExtensions);
        this.status = 0;
        this.statusStrings = new ASN1EncodableVector();
        if (s != null) {
            this.addStatusString(s);
        }
        final PKIStatusInfo pkiStatusInfo = this.getPKIStatusInfo();
        ContentInfo asn1Structure;
        try {
            asn1Structure = this.tokenGenerator.generate(timeStampRequest, bigInteger, date).toCMSSignedData().toASN1Structure();
        }
        catch (TSPException ex) {
            throw ex;
        }
        catch (Exception ex2) {
            throw new TSPException("Timestamp token received cannot be converted to ContentInfo", ex2);
        }
        final TimeStampResp timeStampResp = new TimeStampResp(pkiStatusInfo, asn1Structure);
        try {
            return new TimeStampResponse(timeStampResp);
        }
        catch (IOException ex3) {
            throw new TSPException("created badly formatted response!");
        }
    }
    
    public TimeStampResponse generateRejectedResponse(final Exception ex) throws TSPException {
        if (ex instanceof TSPValidationException) {
            return this.generateFailResponse(2, ((TSPValidationException)ex).getFailureCode(), ex.getMessage());
        }
        return this.generateFailResponse(2, 1073741824, ex.getMessage());
    }
    
    public TimeStampResponse generateFailResponse(final int status, final int failInfoField, final String s) throws TSPException {
        this.status = status;
        this.statusStrings = new ASN1EncodableVector();
        this.setFailInfoField(failInfoField);
        if (s != null) {
            this.addStatusString(s);
        }
        final TimeStampResp timeStampResp = new TimeStampResp(this.getPKIStatusInfo(), null);
        try {
            return new TimeStampResponse(timeStampResp);
        }
        catch (IOException ex) {
            throw new TSPException("created badly formatted response!");
        }
    }
    
    private Set convert(final Set set) {
        if (set == null) {
            return set;
        }
        final HashSet<ASN1ObjectIdentifier> set2 = new HashSet<ASN1ObjectIdentifier>(set.size());
        for (final String next : set) {
            if (next instanceof String) {
                set2.add(new ASN1ObjectIdentifier(next));
            }
            else {
                set2.add((ASN1ObjectIdentifier)next);
            }
        }
        return set2;
    }
    
    class FailInfo extends DERBitString
    {
        FailInfo(final int n) {
            super(DERBitString.getBytes(n), DERBitString.getPadBits(n));
        }
    }
}
