// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.tsp.cms;

import org.bouncycastle.tsp.TimeStampToken;

public class ImprintDigestInvalidException extends Exception
{
    private TimeStampToken token;
    
    public ImprintDigestInvalidException(final String message, final TimeStampToken token) {
        super(message);
        this.token = token;
    }
    
    public TimeStampToken getTimeStampToken() {
        return this.token;
    }
}
