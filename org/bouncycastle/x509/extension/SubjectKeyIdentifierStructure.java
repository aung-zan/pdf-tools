// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.x509.extension;

import java.security.InvalidKeyException;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.security.PublicKey;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;

public class SubjectKeyIdentifierStructure extends SubjectKeyIdentifier
{
    public SubjectKeyIdentifierStructure(final byte[] array) throws IOException {
        super((ASN1OctetString)X509ExtensionUtil.fromExtensionValue(array));
    }
    
    private static ASN1OctetString fromPublicKey(final PublicKey publicKey) throws InvalidKeyException {
        try {
            return (ASN1OctetString)new SubjectKeyIdentifier(SubjectPublicKeyInfo.getInstance(publicKey.getEncoded())).toASN1Object();
        }
        catch (Exception ex) {
            throw new InvalidKeyException("Exception extracting key details: " + ex.toString());
        }
    }
    
    public SubjectKeyIdentifierStructure(final PublicKey publicKey) throws InvalidKeyException {
        super(fromPublicKey(publicKey));
    }
}
