// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.operator;

import java.io.IOException;

public class OperatorStreamException extends IOException
{
    private Throwable cause;
    
    public OperatorStreamException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
