// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.operator;

import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

public interface InputExpanderProvider
{
    InputExpander get(final AlgorithmIdentifier p0);
}
