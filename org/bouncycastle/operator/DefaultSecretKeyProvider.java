// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.operator;

import java.util.Collections;
import org.bouncycastle.asn1.ntt.NTTObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.util.Integers;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.HashMap;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.util.Map;

public class DefaultSecretKeyProvider implements SecretKeySizeProvider
{
    public static final SecretKeySizeProvider INSTANCE;
    private static final Map KEY_SIZES;
    
    public int getKeySize(final AlgorithmIdentifier algorithmIdentifier) {
        final Integer n = DefaultSecretKeyProvider.KEY_SIZES.get(algorithmIdentifier.getAlgorithm());
        if (n != null) {
            return n;
        }
        return -1;
    }
    
    static {
        INSTANCE = new DefaultSecretKeyProvider();
        final HashMap<ASN1ObjectIdentifier, Integer> m = new HashMap<ASN1ObjectIdentifier, Integer>();
        m.put(new ASN1ObjectIdentifier("1.2.840.113533.7.66.10"), Integers.valueOf(128));
        m.put((ASN1ObjectIdentifier)PKCSObjectIdentifiers.des_EDE3_CBC.getId(), Integers.valueOf(192));
        m.put(NISTObjectIdentifiers.id_aes128_CBC, Integers.valueOf(128));
        m.put(NISTObjectIdentifiers.id_aes192_CBC, Integers.valueOf(192));
        m.put(NISTObjectIdentifiers.id_aes256_CBC, Integers.valueOf(256));
        m.put(NTTObjectIdentifiers.id_camellia128_cbc, Integers.valueOf(128));
        m.put(NTTObjectIdentifiers.id_camellia192_cbc, Integers.valueOf(192));
        m.put(NTTObjectIdentifiers.id_camellia256_cbc, Integers.valueOf(256));
        KEY_SIZES = Collections.unmodifiableMap((Map<?, ?>)m);
    }
}
