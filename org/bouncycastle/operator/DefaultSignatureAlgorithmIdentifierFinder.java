// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.operator;

import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import java.util.HashSet;
import java.util.HashMap;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.pkcs.RSASSAPSSparams;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.util.Strings;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.Set;
import java.util.Map;

public class DefaultSignatureAlgorithmIdentifierFinder implements SignatureAlgorithmIdentifierFinder
{
    private static Map algorithms;
    private static Set noParams;
    private static Map params;
    private static Set pkcs15RsaEncryption;
    private static Map digestOids;
    private static final ASN1ObjectIdentifier ENCRYPTION_RSA;
    private static final ASN1ObjectIdentifier ENCRYPTION_DSA;
    private static final ASN1ObjectIdentifier ENCRYPTION_ECDSA;
    private static final ASN1ObjectIdentifier ENCRYPTION_RSA_PSS;
    private static final ASN1ObjectIdentifier ENCRYPTION_GOST3410;
    private static final ASN1ObjectIdentifier ENCRYPTION_ECGOST3410;
    
    private static AlgorithmIdentifier generate(final String s) {
        final String upperCase = Strings.toUpperCase(s);
        final ASN1ObjectIdentifier asn1ObjectIdentifier = DefaultSignatureAlgorithmIdentifierFinder.algorithms.get(upperCase);
        if (asn1ObjectIdentifier == null) {
            throw new IllegalArgumentException("Unknown signature type requested: " + upperCase);
        }
        AlgorithmIdentifier algorithmIdentifier;
        if (DefaultSignatureAlgorithmIdentifierFinder.noParams.contains(asn1ObjectIdentifier)) {
            algorithmIdentifier = new AlgorithmIdentifier(asn1ObjectIdentifier);
        }
        else if (DefaultSignatureAlgorithmIdentifierFinder.params.containsKey(upperCase)) {
            algorithmIdentifier = new AlgorithmIdentifier(asn1ObjectIdentifier, DefaultSignatureAlgorithmIdentifierFinder.params.get(upperCase));
        }
        else {
            algorithmIdentifier = new AlgorithmIdentifier(asn1ObjectIdentifier, DERNull.INSTANCE);
        }
        if (DefaultSignatureAlgorithmIdentifierFinder.pkcs15RsaEncryption.contains(asn1ObjectIdentifier)) {
            final AlgorithmIdentifier algorithmIdentifier2 = new AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, DERNull.INSTANCE);
        }
        if (algorithmIdentifier.getAlgorithm().equals(PKCSObjectIdentifiers.id_RSASSA_PSS)) {
            ((RSASSAPSSparams)algorithmIdentifier.getParameters()).getHashAlgorithm();
        }
        else {
            final AlgorithmIdentifier algorithmIdentifier3 = new AlgorithmIdentifier(DefaultSignatureAlgorithmIdentifierFinder.digestOids.get(asn1ObjectIdentifier), DERNull.INSTANCE);
        }
        return algorithmIdentifier;
    }
    
    private static RSASSAPSSparams createPSSParams(final AlgorithmIdentifier algorithmIdentifier, final int n) {
        return new RSASSAPSSparams(algorithmIdentifier, new AlgorithmIdentifier(PKCSObjectIdentifiers.id_mgf1, algorithmIdentifier), new ASN1Integer(n), new ASN1Integer(1L));
    }
    
    public AlgorithmIdentifier find(final String s) {
        return generate(s);
    }
    
    static {
        DefaultSignatureAlgorithmIdentifierFinder.algorithms = new HashMap();
        DefaultSignatureAlgorithmIdentifierFinder.noParams = new HashSet();
        DefaultSignatureAlgorithmIdentifierFinder.params = new HashMap();
        DefaultSignatureAlgorithmIdentifierFinder.pkcs15RsaEncryption = new HashSet();
        DefaultSignatureAlgorithmIdentifierFinder.digestOids = new HashMap();
        ENCRYPTION_RSA = PKCSObjectIdentifiers.rsaEncryption;
        ENCRYPTION_DSA = X9ObjectIdentifiers.id_dsa_with_sha1;
        ENCRYPTION_ECDSA = X9ObjectIdentifiers.ecdsa_with_SHA1;
        ENCRYPTION_RSA_PSS = PKCSObjectIdentifiers.id_RSASSA_PSS;
        ENCRYPTION_GOST3410 = CryptoProObjectIdentifiers.gostR3410_94;
        ENCRYPTION_ECGOST3410 = CryptoProObjectIdentifiers.gostR3410_2001;
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("MD2WITHRSAENCRYPTION", PKCSObjectIdentifiers.md2WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("MD2WITHRSA", PKCSObjectIdentifiers.md2WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("MD5WITHRSAENCRYPTION", PKCSObjectIdentifiers.md5WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("MD5WITHRSA", PKCSObjectIdentifiers.md5WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA1WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha1WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA1WITHRSA", PKCSObjectIdentifiers.sha1WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA224WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha224WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA224WITHRSA", PKCSObjectIdentifiers.sha224WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA256WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha256WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA256WITHRSA", PKCSObjectIdentifiers.sha256WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA384WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha384WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA384WITHRSA", PKCSObjectIdentifiers.sha384WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA512WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha512WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA512WITHRSA", PKCSObjectIdentifiers.sha512WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA1WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA224WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA256WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA384WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA512WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("RIPEMD160WITHRSAENCRYPTION", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd160);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("RIPEMD160WITHRSA", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd160);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("RIPEMD128WITHRSAENCRYPTION", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd128);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("RIPEMD128WITHRSA", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd128);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("RIPEMD256WITHRSAENCRYPTION", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd256);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("RIPEMD256WITHRSA", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd256);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA1WITHDSA", X9ObjectIdentifiers.id_dsa_with_sha1);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("DSAWITHSHA1", X9ObjectIdentifiers.id_dsa_with_sha1);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA224WITHDSA", NISTObjectIdentifiers.dsa_with_sha224);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA256WITHDSA", NISTObjectIdentifiers.dsa_with_sha256);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA384WITHDSA", NISTObjectIdentifiers.dsa_with_sha384);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA512WITHDSA", NISTObjectIdentifiers.dsa_with_sha512);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA1WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA1);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("ECDSAWITHSHA1", X9ObjectIdentifiers.ecdsa_with_SHA1);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA224WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA224);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA256WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA256);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA384WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA384);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("SHA512WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA512);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("GOST3411WITHGOST3410", CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_94);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("GOST3411WITHGOST3410-94", CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_94);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("GOST3411WITHECGOST3410", CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_2001);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("GOST3411WITHECGOST3410-2001", CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_2001);
        DefaultSignatureAlgorithmIdentifierFinder.algorithms.put("GOST3411WITHGOST3410-2001", CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_2001);
        DefaultSignatureAlgorithmIdentifierFinder.noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA1);
        DefaultSignatureAlgorithmIdentifierFinder.noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA224);
        DefaultSignatureAlgorithmIdentifierFinder.noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA256);
        DefaultSignatureAlgorithmIdentifierFinder.noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA384);
        DefaultSignatureAlgorithmIdentifierFinder.noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA512);
        DefaultSignatureAlgorithmIdentifierFinder.noParams.add(X9ObjectIdentifiers.id_dsa_with_sha1);
        DefaultSignatureAlgorithmIdentifierFinder.noParams.add(NISTObjectIdentifiers.dsa_with_sha224);
        DefaultSignatureAlgorithmIdentifierFinder.noParams.add(NISTObjectIdentifiers.dsa_with_sha256);
        DefaultSignatureAlgorithmIdentifierFinder.noParams.add(NISTObjectIdentifiers.dsa_with_sha384);
        DefaultSignatureAlgorithmIdentifierFinder.noParams.add(NISTObjectIdentifiers.dsa_with_sha512);
        DefaultSignatureAlgorithmIdentifierFinder.noParams.add(CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_94);
        DefaultSignatureAlgorithmIdentifierFinder.noParams.add(CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_2001);
        DefaultSignatureAlgorithmIdentifierFinder.pkcs15RsaEncryption.add(PKCSObjectIdentifiers.sha1WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.pkcs15RsaEncryption.add(PKCSObjectIdentifiers.sha224WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.pkcs15RsaEncryption.add(PKCSObjectIdentifiers.sha256WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.pkcs15RsaEncryption.add(PKCSObjectIdentifiers.sha384WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.pkcs15RsaEncryption.add(PKCSObjectIdentifiers.sha512WithRSAEncryption);
        DefaultSignatureAlgorithmIdentifierFinder.pkcs15RsaEncryption.add(TeleTrusTObjectIdentifiers.rsaSignatureWithripemd128);
        DefaultSignatureAlgorithmIdentifierFinder.pkcs15RsaEncryption.add(TeleTrusTObjectIdentifiers.rsaSignatureWithripemd160);
        DefaultSignatureAlgorithmIdentifierFinder.pkcs15RsaEncryption.add(TeleTrusTObjectIdentifiers.rsaSignatureWithripemd256);
        DefaultSignatureAlgorithmIdentifierFinder.params.put("SHA1WITHRSAANDMGF1", createPSSParams(new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1, DERNull.INSTANCE), 20));
        DefaultSignatureAlgorithmIdentifierFinder.params.put("SHA224WITHRSAANDMGF1", createPSSParams(new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha224, DERNull.INSTANCE), 28));
        DefaultSignatureAlgorithmIdentifierFinder.params.put("SHA256WITHRSAANDMGF1", createPSSParams(new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha256, DERNull.INSTANCE), 32));
        DefaultSignatureAlgorithmIdentifierFinder.params.put("SHA384WITHRSAANDMGF1", createPSSParams(new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha384, DERNull.INSTANCE), 48));
        DefaultSignatureAlgorithmIdentifierFinder.params.put("SHA512WITHRSAANDMGF1", createPSSParams(new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha512, DERNull.INSTANCE), 64));
        DefaultSignatureAlgorithmIdentifierFinder.digestOids.put(PKCSObjectIdentifiers.sha224WithRSAEncryption, NISTObjectIdentifiers.id_sha224);
        DefaultSignatureAlgorithmIdentifierFinder.digestOids.put(PKCSObjectIdentifiers.sha256WithRSAEncryption, NISTObjectIdentifiers.id_sha256);
        DefaultSignatureAlgorithmIdentifierFinder.digestOids.put(PKCSObjectIdentifiers.sha384WithRSAEncryption, NISTObjectIdentifiers.id_sha384);
        DefaultSignatureAlgorithmIdentifierFinder.digestOids.put(PKCSObjectIdentifiers.sha512WithRSAEncryption, NISTObjectIdentifiers.id_sha512);
        DefaultSignatureAlgorithmIdentifierFinder.digestOids.put(PKCSObjectIdentifiers.md2WithRSAEncryption, PKCSObjectIdentifiers.md2);
        DefaultSignatureAlgorithmIdentifierFinder.digestOids.put(PKCSObjectIdentifiers.md4WithRSAEncryption, PKCSObjectIdentifiers.md4);
        DefaultSignatureAlgorithmIdentifierFinder.digestOids.put(PKCSObjectIdentifiers.md5WithRSAEncryption, PKCSObjectIdentifiers.md5);
        DefaultSignatureAlgorithmIdentifierFinder.digestOids.put(PKCSObjectIdentifiers.sha1WithRSAEncryption, OIWObjectIdentifiers.idSHA1);
        DefaultSignatureAlgorithmIdentifierFinder.digestOids.put(TeleTrusTObjectIdentifiers.rsaSignatureWithripemd128, TeleTrusTObjectIdentifiers.ripemd128);
        DefaultSignatureAlgorithmIdentifierFinder.digestOids.put(TeleTrusTObjectIdentifiers.rsaSignatureWithripemd160, TeleTrusTObjectIdentifiers.ripemd160);
        DefaultSignatureAlgorithmIdentifierFinder.digestOids.put(TeleTrusTObjectIdentifiers.rsaSignatureWithripemd256, TeleTrusTObjectIdentifiers.ripemd256);
        DefaultSignatureAlgorithmIdentifierFinder.digestOids.put(CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_94, CryptoProObjectIdentifiers.gostR3411);
        DefaultSignatureAlgorithmIdentifierFinder.digestOids.put(CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_2001, CryptoProObjectIdentifiers.gostR3411);
    }
}
