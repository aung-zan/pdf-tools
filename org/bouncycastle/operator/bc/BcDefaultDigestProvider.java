// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.operator.bc;

import org.bouncycastle.operator.OperatorCreationException;
import java.util.Collections;
import org.bouncycastle.crypto.digests.RIPEMD256Digest;
import org.bouncycastle.crypto.digests.RIPEMD160Digest;
import org.bouncycastle.crypto.digests.RIPEMD128Digest;
import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;
import org.bouncycastle.crypto.digests.GOST3411Digest;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.crypto.digests.MD2Digest;
import org.bouncycastle.crypto.digests.MD4Digest;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.digests.SHA384Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.digests.SHA224Digest;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.ExtendedDigest;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.HashMap;
import java.util.Map;

public class BcDefaultDigestProvider implements BcDigestProvider
{
    private static final Map lookup;
    public static final BcDigestProvider INSTANCE;
    
    private static Map createTable() {
        final HashMap<ASN1ObjectIdentifier, BcDefaultDigestProvider$12> m = new HashMap<ASN1ObjectIdentifier, BcDefaultDigestProvider$12>();
        m.put(OIWObjectIdentifiers.idSHA1, new BcDigestProvider() {
            public ExtendedDigest get(final AlgorithmIdentifier algorithmIdentifier) {
                return new SHA1Digest();
            }
        });
        m.put(NISTObjectIdentifiers.id_sha224, (BcDefaultDigestProvider$1)new BcDigestProvider() {
            public ExtendedDigest get(final AlgorithmIdentifier algorithmIdentifier) {
                return new SHA224Digest();
            }
        });
        m.put(NISTObjectIdentifiers.id_sha256, (BcDefaultDigestProvider$1)new BcDigestProvider() {
            public ExtendedDigest get(final AlgorithmIdentifier algorithmIdentifier) {
                return new SHA256Digest();
            }
        });
        m.put(NISTObjectIdentifiers.id_sha384, (BcDefaultDigestProvider$1)new BcDigestProvider() {
            public ExtendedDigest get(final AlgorithmIdentifier algorithmIdentifier) {
                return new SHA384Digest();
            }
        });
        m.put(NISTObjectIdentifiers.id_sha512, (BcDefaultDigestProvider$1)new BcDigestProvider() {
            public ExtendedDigest get(final AlgorithmIdentifier algorithmIdentifier) {
                return new SHA512Digest();
            }
        });
        m.put(PKCSObjectIdentifiers.md5, (BcDefaultDigestProvider$1)new BcDigestProvider() {
            public ExtendedDigest get(final AlgorithmIdentifier algorithmIdentifier) {
                return new MD5Digest();
            }
        });
        m.put(PKCSObjectIdentifiers.md4, (BcDefaultDigestProvider$1)new BcDigestProvider() {
            public ExtendedDigest get(final AlgorithmIdentifier algorithmIdentifier) {
                return new MD4Digest();
            }
        });
        m.put(PKCSObjectIdentifiers.md2, (BcDefaultDigestProvider$1)new BcDigestProvider() {
            public ExtendedDigest get(final AlgorithmIdentifier algorithmIdentifier) {
                return new MD2Digest();
            }
        });
        m.put(CryptoProObjectIdentifiers.gostR3411, (BcDefaultDigestProvider$1)new BcDigestProvider() {
            public ExtendedDigest get(final AlgorithmIdentifier algorithmIdentifier) {
                return new GOST3411Digest();
            }
        });
        m.put(TeleTrusTObjectIdentifiers.ripemd128, (BcDefaultDigestProvider$1)new BcDigestProvider() {
            public ExtendedDigest get(final AlgorithmIdentifier algorithmIdentifier) {
                return new RIPEMD128Digest();
            }
        });
        m.put(TeleTrusTObjectIdentifiers.ripemd160, (BcDefaultDigestProvider$1)new BcDigestProvider() {
            public ExtendedDigest get(final AlgorithmIdentifier algorithmIdentifier) {
                return new RIPEMD160Digest();
            }
        });
        m.put(TeleTrusTObjectIdentifiers.ripemd256, (BcDefaultDigestProvider$1)new BcDigestProvider() {
            public ExtendedDigest get(final AlgorithmIdentifier algorithmIdentifier) {
                return new RIPEMD256Digest();
            }
        });
        return Collections.unmodifiableMap((Map<?, ?>)m);
    }
    
    private BcDefaultDigestProvider() {
    }
    
    public ExtendedDigest get(final AlgorithmIdentifier algorithmIdentifier) throws OperatorCreationException {
        final BcDigestProvider bcDigestProvider = BcDefaultDigestProvider.lookup.get(algorithmIdentifier.getAlgorithm());
        if (bcDigestProvider == null) {
            throw new OperatorCreationException("cannot recognise digest");
        }
        return bcDigestProvider.get(algorithmIdentifier);
    }
    
    static {
        lookup = createTable();
        INSTANCE = new BcDefaultDigestProvider();
    }
}
