// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.operator;

public class RuntimeOperatorException extends RuntimeException
{
    private Throwable cause;
    
    public RuntimeOperatorException(final String message) {
        super(message);
    }
    
    public RuntimeOperatorException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
