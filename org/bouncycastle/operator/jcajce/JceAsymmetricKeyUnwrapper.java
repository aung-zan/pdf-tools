// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.operator.jcajce;

import javax.crypto.Cipher;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.security.InvalidKeyException;
import org.bouncycastle.operator.OperatorException;
import javax.crypto.spec.SecretKeySpec;
import java.security.ProviderException;
import java.security.GeneralSecurityException;
import java.security.Key;
import org.bouncycastle.operator.GenericKey;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import java.util.HashMap;
import org.bouncycastle.jcajce.JcaJceHelper;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.security.PrivateKey;
import java.util.Map;
import org.bouncycastle.operator.AsymmetricKeyUnwrapper;

public class JceAsymmetricKeyUnwrapper extends AsymmetricKeyUnwrapper
{
    private OperatorHelper helper;
    private Map extraMappings;
    private PrivateKey privKey;
    
    public JceAsymmetricKeyUnwrapper(final AlgorithmIdentifier algorithmIdentifier, final PrivateKey privKey) {
        super(algorithmIdentifier);
        this.helper = new OperatorHelper(new DefaultJcaJceHelper());
        this.extraMappings = new HashMap();
        this.privKey = privKey;
    }
    
    public JceAsymmetricKeyUnwrapper setProvider(final Provider provider) {
        this.helper = new OperatorHelper(new ProviderJcaJceHelper(provider));
        return this;
    }
    
    public JceAsymmetricKeyUnwrapper setProvider(final String s) {
        this.helper = new OperatorHelper(new NamedJcaJceHelper(s));
        return this;
    }
    
    public JceAsymmetricKeyUnwrapper setAlgorithmMapping(final ASN1ObjectIdentifier asn1ObjectIdentifier, final String s) {
        this.extraMappings.put(asn1ObjectIdentifier, s);
        return this;
    }
    
    public GenericKey generateUnwrappedKey(final AlgorithmIdentifier algorithmIdentifier, final byte[] array) throws OperatorException {
        try {
            Key unwrap = null;
            final Cipher asymmetricWrapper = this.helper.createAsymmetricWrapper(this.getAlgorithmIdentifier().getAlgorithm(), this.extraMappings);
            try {
                asymmetricWrapper.init(4, this.privKey);
                unwrap = asymmetricWrapper.unwrap(array, this.helper.getKeyAlgorithmName(algorithmIdentifier.getAlgorithm()), 3);
            }
            catch (GeneralSecurityException ex4) {}
            catch (IllegalStateException ex5) {}
            catch (UnsupportedOperationException ex6) {}
            catch (ProviderException ex7) {}
            if (unwrap == null) {
                asymmetricWrapper.init(2, this.privKey);
                unwrap = new SecretKeySpec(asymmetricWrapper.doFinal(array), algorithmIdentifier.getAlgorithm().getId());
            }
            return new JceGenericKey(algorithmIdentifier, unwrap);
        }
        catch (InvalidKeyException ex) {
            throw new OperatorException("key invalid: " + ex.getMessage(), ex);
        }
        catch (IllegalBlockSizeException ex2) {
            throw new OperatorException("illegal blocksize: " + ex2.getMessage(), ex2);
        }
        catch (BadPaddingException ex3) {
            throw new OperatorException("bad padding: " + ex3.getMessage(), ex3);
        }
    }
}
