// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.operator.jcajce;

import org.bouncycastle.asn1.kisa.KISAObjectIdentifiers;
import org.bouncycastle.asn1.ntt.NTTObjectIdentifiers;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import java.util.HashMap;
import java.security.cert.CertificateException;
import java.security.NoSuchProviderException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.security.cert.X509Certificate;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.pkcs.RSASSAPSSparams;
import org.bouncycastle.asn1.DERNull;
import java.security.AlgorithmParameters;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.PSSParameterSpec;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.security.Signature;
import java.security.MessageDigest;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.security.GeneralSecurityException;
import org.bouncycastle.operator.OperatorCreationException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.jcajce.JcaJceHelper;
import java.util.Map;

class OperatorHelper
{
    private static final Map oids;
    private static final Map asymmetricWrapperAlgNames;
    private static final Map symmetricWrapperAlgNames;
    private static final Map symmetricKeyAlgNames;
    private JcaJceHelper helper;
    
    OperatorHelper(final JcaJceHelper helper) {
        this.helper = helper;
    }
    
    Cipher createAsymmetricWrapper(final ASN1ObjectIdentifier asn1ObjectIdentifier, final Map map) throws OperatorCreationException {
        try {
            String s = null;
            if (!map.isEmpty()) {
                s = map.get(asn1ObjectIdentifier);
            }
            if (s == null) {
                s = OperatorHelper.asymmetricWrapperAlgNames.get(asn1ObjectIdentifier);
            }
            if (s != null) {
                try {
                    return this.helper.createCipher(s);
                }
                catch (NoSuchAlgorithmException ex2) {
                    if (s.equals("RSA/ECB/PKCS1Padding")) {
                        try {
                            return this.helper.createCipher("RSA/NONE/PKCS1Padding");
                        }
                        catch (NoSuchAlgorithmException ex3) {}
                    }
                }
            }
            return this.helper.createCipher(asn1ObjectIdentifier.getId());
        }
        catch (GeneralSecurityException ex) {
            throw new OperatorCreationException("cannot create cipher: " + ex.getMessage(), ex);
        }
    }
    
    Cipher createSymmetricWrapper(final ASN1ObjectIdentifier asn1ObjectIdentifier) throws OperatorCreationException {
        try {
            final String s = OperatorHelper.symmetricWrapperAlgNames.get(asn1ObjectIdentifier);
            if (s != null) {
                try {
                    return this.helper.createCipher(s);
                }
                catch (NoSuchAlgorithmException ex2) {}
            }
            return this.helper.createCipher(asn1ObjectIdentifier.getId());
        }
        catch (GeneralSecurityException ex) {
            throw new OperatorCreationException("cannot create cipher: " + ex.getMessage(), ex);
        }
    }
    
    MessageDigest createDigest(final AlgorithmIdentifier algorithmIdentifier) throws GeneralSecurityException {
        MessageDigest messageDigest;
        try {
            messageDigest = this.helper.createDigest(getDigestAlgName(algorithmIdentifier.getAlgorithm()));
        }
        catch (NoSuchAlgorithmException ex) {
            if (OperatorHelper.oids.get(algorithmIdentifier.getAlgorithm()) == null) {
                throw ex;
            }
            messageDigest = this.helper.createDigest(OperatorHelper.oids.get(algorithmIdentifier.getAlgorithm()));
        }
        return messageDigest;
    }
    
    Signature createSignature(final AlgorithmIdentifier algorithmIdentifier) throws GeneralSecurityException {
        Signature signature;
        try {
            signature = this.helper.createSignature(getSignatureName(algorithmIdentifier));
        }
        catch (NoSuchAlgorithmException ex) {
            if (OperatorHelper.oids.get(algorithmIdentifier.getAlgorithm()) == null) {
                throw ex;
            }
            signature = this.helper.createSignature(OperatorHelper.oids.get(algorithmIdentifier.getAlgorithm()));
        }
        return signature;
    }
    
    public Signature createRawSignature(final AlgorithmIdentifier algorithmIdentifier) {
        Signature signature;
        try {
            final String signatureName = getSignatureName(algorithmIdentifier);
            final String string = "NONE" + signatureName.substring(signatureName.indexOf("WITH"));
            signature = this.helper.createSignature(string);
            if (algorithmIdentifier.getAlgorithm().equals(PKCSObjectIdentifiers.id_RSASSA_PSS)) {
                final AlgorithmParameters algorithmParameters = this.helper.createAlgorithmParameters(string);
                algorithmParameters.init(algorithmIdentifier.getParameters().toASN1Primitive().getEncoded(), "ASN.1");
                signature.setParameter(algorithmParameters.getParameterSpec(PSSParameterSpec.class));
            }
        }
        catch (Exception ex) {
            return null;
        }
        return signature;
    }
    
    private static String getSignatureName(final AlgorithmIdentifier algorithmIdentifier) {
        final ASN1Encodable parameters = algorithmIdentifier.getParameters();
        if (parameters != null && !DERNull.INSTANCE.equals(parameters) && algorithmIdentifier.getAlgorithm().equals(PKCSObjectIdentifiers.id_RSASSA_PSS)) {
            return getDigestAlgName(RSASSAPSSparams.getInstance(parameters).getHashAlgorithm().getAlgorithm()) + "WITHRSAANDMGF1";
        }
        if (OperatorHelper.oids.containsKey(algorithmIdentifier.getAlgorithm())) {
            return OperatorHelper.oids.get(algorithmIdentifier.getAlgorithm());
        }
        return algorithmIdentifier.getAlgorithm().getId();
    }
    
    private static String getDigestAlgName(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        if (PKCSObjectIdentifiers.md5.equals(asn1ObjectIdentifier)) {
            return "MD5";
        }
        if (OIWObjectIdentifiers.idSHA1.equals(asn1ObjectIdentifier)) {
            return "SHA1";
        }
        if (NISTObjectIdentifiers.id_sha224.equals(asn1ObjectIdentifier)) {
            return "SHA224";
        }
        if (NISTObjectIdentifiers.id_sha256.equals(asn1ObjectIdentifier)) {
            return "SHA256";
        }
        if (NISTObjectIdentifiers.id_sha384.equals(asn1ObjectIdentifier)) {
            return "SHA384";
        }
        if (NISTObjectIdentifiers.id_sha512.equals(asn1ObjectIdentifier)) {
            return "SHA512";
        }
        if (TeleTrusTObjectIdentifiers.ripemd128.equals(asn1ObjectIdentifier)) {
            return "RIPEMD128";
        }
        if (TeleTrusTObjectIdentifiers.ripemd160.equals(asn1ObjectIdentifier)) {
            return "RIPEMD160";
        }
        if (TeleTrusTObjectIdentifiers.ripemd256.equals(asn1ObjectIdentifier)) {
            return "RIPEMD256";
        }
        if (CryptoProObjectIdentifiers.gostR3411.equals(asn1ObjectIdentifier)) {
            return "GOST3411";
        }
        return asn1ObjectIdentifier.getId();
    }
    
    public X509Certificate convertCertificate(final X509CertificateHolder x509CertificateHolder) throws CertificateException {
        try {
            return (X509Certificate)this.helper.createCertificateFactory("X.509").generateCertificate(new ByteArrayInputStream(x509CertificateHolder.getEncoded()));
        }
        catch (IOException ex) {
            throw new OpCertificateException("cannot get encoded form of certificate: " + ex.getMessage(), ex);
        }
        catch (NoSuchAlgorithmException ex2) {
            throw new OpCertificateException("cannot create certificate factory: " + ex2.getMessage(), ex2);
        }
        catch (NoSuchProviderException ex3) {
            throw new OpCertificateException("cannot find factory provider: " + ex3.getMessage(), ex3);
        }
    }
    
    String getKeyAlgorithmName(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        final String s = OperatorHelper.symmetricKeyAlgNames.get(asn1ObjectIdentifier);
        if (s != null) {
            return s;
        }
        return asn1ObjectIdentifier.getId();
    }
    
    static {
        oids = new HashMap();
        asymmetricWrapperAlgNames = new HashMap();
        symmetricWrapperAlgNames = new HashMap();
        symmetricKeyAlgNames = new HashMap();
        OperatorHelper.oids.put(new ASN1ObjectIdentifier("1.2.840.113549.1.1.5"), "SHA1WITHRSA");
        OperatorHelper.oids.put(PKCSObjectIdentifiers.sha224WithRSAEncryption, "SHA224WITHRSA");
        OperatorHelper.oids.put(PKCSObjectIdentifiers.sha256WithRSAEncryption, "SHA256WITHRSA");
        OperatorHelper.oids.put(PKCSObjectIdentifiers.sha384WithRSAEncryption, "SHA384WITHRSA");
        OperatorHelper.oids.put(PKCSObjectIdentifiers.sha512WithRSAEncryption, "SHA512WITHRSA");
        OperatorHelper.oids.put(CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_94, "GOST3411WITHGOST3410");
        OperatorHelper.oids.put(CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_2001, "GOST3411WITHECGOST3410");
        OperatorHelper.oids.put(new ASN1ObjectIdentifier("1.2.840.113549.1.1.4"), "MD5WITHRSA");
        OperatorHelper.oids.put(new ASN1ObjectIdentifier("1.2.840.113549.1.1.2"), "MD2WITHRSA");
        OperatorHelper.oids.put(new ASN1ObjectIdentifier("1.2.840.10040.4.3"), "SHA1WITHDSA");
        OperatorHelper.oids.put(X9ObjectIdentifiers.ecdsa_with_SHA1, "SHA1WITHECDSA");
        OperatorHelper.oids.put(X9ObjectIdentifiers.ecdsa_with_SHA224, "SHA224WITHECDSA");
        OperatorHelper.oids.put(X9ObjectIdentifiers.ecdsa_with_SHA256, "SHA256WITHECDSA");
        OperatorHelper.oids.put(X9ObjectIdentifiers.ecdsa_with_SHA384, "SHA384WITHECDSA");
        OperatorHelper.oids.put(X9ObjectIdentifiers.ecdsa_with_SHA512, "SHA512WITHECDSA");
        OperatorHelper.oids.put(OIWObjectIdentifiers.sha1WithRSA, "SHA1WITHRSA");
        OperatorHelper.oids.put(OIWObjectIdentifiers.dsaWithSHA1, "SHA1WITHDSA");
        OperatorHelper.oids.put(NISTObjectIdentifiers.dsa_with_sha224, "SHA224WITHDSA");
        OperatorHelper.oids.put(NISTObjectIdentifiers.dsa_with_sha256, "SHA256WITHDSA");
        OperatorHelper.oids.put(OIWObjectIdentifiers.idSHA1, "SHA-1");
        OperatorHelper.oids.put(NISTObjectIdentifiers.id_sha224, "SHA-224");
        OperatorHelper.oids.put(NISTObjectIdentifiers.id_sha256, "SHA-256");
        OperatorHelper.oids.put(NISTObjectIdentifiers.id_sha384, "SHA-384");
        OperatorHelper.oids.put(NISTObjectIdentifiers.id_sha512, "SHA-512");
        OperatorHelper.oids.put(TeleTrusTObjectIdentifiers.ripemd128, "RIPEMD-128");
        OperatorHelper.oids.put(TeleTrusTObjectIdentifiers.ripemd160, "RIPEMD-160");
        OperatorHelper.oids.put(TeleTrusTObjectIdentifiers.ripemd256, "RIPEMD-256");
        OperatorHelper.asymmetricWrapperAlgNames.put(PKCSObjectIdentifiers.rsaEncryption, "RSA/ECB/PKCS1Padding");
        OperatorHelper.symmetricWrapperAlgNames.put(PKCSObjectIdentifiers.id_alg_CMS3DESwrap, "DESEDEWrap");
        OperatorHelper.symmetricWrapperAlgNames.put(PKCSObjectIdentifiers.id_alg_CMSRC2wrap, "RC2Wrap");
        OperatorHelper.symmetricWrapperAlgNames.put(NISTObjectIdentifiers.id_aes128_wrap, "AESWrap");
        OperatorHelper.symmetricWrapperAlgNames.put(NISTObjectIdentifiers.id_aes192_wrap, "AESWrap");
        OperatorHelper.symmetricWrapperAlgNames.put(NISTObjectIdentifiers.id_aes256_wrap, "AESWrap");
        OperatorHelper.symmetricWrapperAlgNames.put(NTTObjectIdentifiers.id_camellia128_wrap, "CamelliaWrap");
        OperatorHelper.symmetricWrapperAlgNames.put(NTTObjectIdentifiers.id_camellia192_wrap, "CamelliaWrap");
        OperatorHelper.symmetricWrapperAlgNames.put(NTTObjectIdentifiers.id_camellia256_wrap, "CamelliaWrap");
        OperatorHelper.symmetricWrapperAlgNames.put(KISAObjectIdentifiers.id_npki_app_cmsSeed_wrap, "SEEDWrap");
        OperatorHelper.symmetricWrapperAlgNames.put(PKCSObjectIdentifiers.des_EDE3_CBC, "DESede");
        OperatorHelper.symmetricKeyAlgNames.put(NISTObjectIdentifiers.aes, "AES");
        OperatorHelper.symmetricKeyAlgNames.put(NISTObjectIdentifiers.id_aes128_CBC, "AES");
        OperatorHelper.symmetricKeyAlgNames.put(NISTObjectIdentifiers.id_aes192_CBC, "AES");
        OperatorHelper.symmetricKeyAlgNames.put(NISTObjectIdentifiers.id_aes256_CBC, "AES");
        OperatorHelper.symmetricKeyAlgNames.put(PKCSObjectIdentifiers.des_EDE3_CBC, "DESede");
        OperatorHelper.symmetricKeyAlgNames.put(PKCSObjectIdentifiers.RC2_CBC, "RC2");
    }
    
    private static class OpCertificateException extends CertificateException
    {
        private Throwable cause;
        
        public OpCertificateException(final String msg, final Throwable cause) {
            super(msg);
            this.cause = cause;
        }
        
        @Override
        public Throwable getCause() {
            return this.cause;
        }
    }
}
