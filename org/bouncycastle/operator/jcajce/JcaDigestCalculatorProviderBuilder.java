// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.operator.jcajce;

import java.io.IOException;
import java.security.MessageDigest;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import org.bouncycastle.operator.DigestCalculator;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.DigestCalculatorProvider;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import org.bouncycastle.jcajce.JcaJceHelper;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;

public class JcaDigestCalculatorProviderBuilder
{
    private OperatorHelper helper;
    
    public JcaDigestCalculatorProviderBuilder() {
        this.helper = new OperatorHelper(new DefaultJcaJceHelper());
    }
    
    public JcaDigestCalculatorProviderBuilder setProvider(final Provider provider) {
        this.helper = new OperatorHelper(new ProviderJcaJceHelper(provider));
        return this;
    }
    
    public JcaDigestCalculatorProviderBuilder setProvider(final String s) {
        this.helper = new OperatorHelper(new NamedJcaJceHelper(s));
        return this;
    }
    
    public DigestCalculatorProvider build() throws OperatorCreationException {
        return new DigestCalculatorProvider() {
            public DigestCalculator get(final AlgorithmIdentifier algorithmIdentifier) throws OperatorCreationException {
                DigestOutputStream digestOutputStream;
                try {
                    digestOutputStream = new DigestOutputStream(JcaDigestCalculatorProviderBuilder.this.helper.createDigest(algorithmIdentifier));
                }
                catch (GeneralSecurityException obj) {
                    throw new OperatorCreationException("exception on setup: " + obj, obj);
                }
                return new DigestCalculator() {
                    public AlgorithmIdentifier getAlgorithmIdentifier() {
                        return algorithmIdentifier;
                    }
                    
                    public OutputStream getOutputStream() {
                        return digestOutputStream;
                    }
                    
                    public byte[] getDigest() {
                        return digestOutputStream.getDigest();
                    }
                };
            }
        };
    }
    
    private class DigestOutputStream extends OutputStream
    {
        private MessageDigest dig;
        
        DigestOutputStream(final MessageDigest dig) {
            this.dig = dig;
        }
        
        @Override
        public void write(final byte[] input, final int offset, final int len) throws IOException {
            this.dig.update(input, offset, len);
        }
        
        @Override
        public void write(final byte[] input) throws IOException {
            this.dig.update(input);
        }
        
        @Override
        public void write(final int n) throws IOException {
            this.dig.update((byte)n);
        }
        
        byte[] getDigest() {
            return this.dig.digest();
        }
    }
}
