// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.operator.jcajce;

import javax.crypto.Cipher;
import org.bouncycastle.operator.OperatorException;
import java.security.ProviderException;
import java.security.GeneralSecurityException;
import java.security.Key;
import org.bouncycastle.operator.GenericKey;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import org.bouncycastle.jcajce.JcaJceHelper;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.security.SecureRandom;
import java.security.PublicKey;
import java.util.Map;
import org.bouncycastle.operator.AsymmetricKeyWrapper;

public class JceAsymmetricKeyWrapper extends AsymmetricKeyWrapper
{
    private OperatorHelper helper;
    private Map extraMappings;
    private PublicKey publicKey;
    private SecureRandom random;
    
    public JceAsymmetricKeyWrapper(final PublicKey publicKey) {
        super(SubjectPublicKeyInfo.getInstance(publicKey.getEncoded()).getAlgorithm());
        this.helper = new OperatorHelper(new DefaultJcaJceHelper());
        this.extraMappings = new HashMap();
        this.publicKey = publicKey;
    }
    
    public JceAsymmetricKeyWrapper(final X509Certificate x509Certificate) {
        this(x509Certificate.getPublicKey());
    }
    
    public JceAsymmetricKeyWrapper setProvider(final Provider provider) {
        this.helper = new OperatorHelper(new ProviderJcaJceHelper(provider));
        return this;
    }
    
    public JceAsymmetricKeyWrapper setProvider(final String s) {
        this.helper = new OperatorHelper(new NamedJcaJceHelper(s));
        return this;
    }
    
    public JceAsymmetricKeyWrapper setSecureRandom(final SecureRandom random) {
        this.random = random;
        return this;
    }
    
    public JceAsymmetricKeyWrapper setAlgorithmMapping(final ASN1ObjectIdentifier asn1ObjectIdentifier, final String s) {
        this.extraMappings.put(asn1ObjectIdentifier, s);
        return this;
    }
    
    public byte[] generateWrappedKey(final GenericKey genericKey) throws OperatorException {
        final Cipher asymmetricWrapper = this.helper.createAsymmetricWrapper(this.getAlgorithmIdentifier().getAlgorithm(), this.extraMappings);
        byte[] array = null;
        try {
            asymmetricWrapper.init(3, this.publicKey, this.random);
            array = asymmetricWrapper.wrap(OperatorUtils.getJceKey(genericKey));
        }
        catch (GeneralSecurityException ex2) {}
        catch (IllegalStateException ex3) {}
        catch (UnsupportedOperationException ex4) {}
        catch (ProviderException ex5) {}
        if (array == null) {
            try {
                asymmetricWrapper.init(1, this.publicKey, this.random);
                array = asymmetricWrapper.doFinal(OperatorUtils.getJceKey(genericKey).getEncoded());
            }
            catch (GeneralSecurityException ex) {
                throw new OperatorException("unable to encrypt contents key", ex);
            }
        }
        return array;
    }
}
