// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.operator;

import java.io.InputStream;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

public interface InputDecryptor
{
    AlgorithmIdentifier getAlgorithmIdentifier();
    
    InputStream getInputStream(final InputStream p0);
}
