// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.ocsp;

public class OCSPException extends Exception
{
    private Throwable cause;
    
    public OCSPException(final String message) {
        super(message);
    }
    
    public OCSPException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
