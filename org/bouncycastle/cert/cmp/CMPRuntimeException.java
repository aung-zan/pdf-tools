// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.cmp;

public class CMPRuntimeException extends RuntimeException
{
    private Throwable cause;
    
    public CMPRuntimeException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
