// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.cmp;

public class CMPException extends Exception
{
    private Throwable cause;
    
    public CMPException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    public CMPException(final String message) {
        super(message);
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
