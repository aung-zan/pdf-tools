// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.selector;

import java.io.IOException;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;

class MSOutlookKeyIdCalculator
{
    static byte[] calculateKeyId(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        final SHA1Digest sha1Digest = new SHA1Digest();
        final byte[] array = new byte[sha1Digest.getDigestSize()];
        final byte[] array2 = new byte[0];
        byte[] encoded;
        try {
            encoded = subjectPublicKeyInfo.getEncoded("DER");
        }
        catch (IOException ex) {
            return new byte[0];
        }
        sha1Digest.update(encoded, 0, encoded.length);
        sha1Digest.doFinal(array, 0);
        return array;
    }
}
