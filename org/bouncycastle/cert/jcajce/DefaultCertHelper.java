// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.jcajce;

import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

class DefaultCertHelper extends CertHelper
{
    @Override
    protected CertificateFactory createCertificateFactory(final String type) throws CertificateException {
        return CertificateFactory.getInstance(type);
    }
}
