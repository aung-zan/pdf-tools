// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.jcajce;

import java.util.Iterator;
import java.io.IOException;
import java.security.cert.X509Certificate;
import org.bouncycastle.cert.X509CertificateHolder;
import java.util.ArrayList;
import java.security.cert.CertificateEncodingException;
import java.util.Collection;
import org.bouncycastle.util.CollectionStore;

public class JcaCertStore extends CollectionStore
{
    public JcaCertStore(final Collection collection) throws CertificateEncodingException {
        super(convertCerts(collection));
    }
    
    private static Collection convertCerts(final Collection collection) throws CertificateEncodingException {
        final ArrayList<X509CertificateHolder> list = new ArrayList<X509CertificateHolder>(collection.size());
        for (final X509Certificate next : collection) {
            if (next instanceof X509Certificate) {
                final X509Certificate x509Certificate = next;
                try {
                    list.add(new X509CertificateHolder(x509Certificate.getEncoded()));
                }
                catch (IOException ex) {
                    throw new CertificateEncodingException("unable to read encoding: " + ex.getMessage());
                }
            }
            else {
                list.add((X509CertificateHolder)next);
            }
        }
        return list;
    }
}
