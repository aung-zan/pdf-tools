// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.jcajce;

import java.util.Iterator;
import java.io.IOException;
import java.security.cert.X509CRL;
import org.bouncycastle.cert.X509CRLHolder;
import java.util.ArrayList;
import java.security.cert.CRLException;
import java.util.Collection;
import org.bouncycastle.util.CollectionStore;

public class JcaCRLStore extends CollectionStore
{
    public JcaCRLStore(final Collection collection) throws CRLException {
        super(convertCRLs(collection));
    }
    
    private static Collection convertCRLs(final Collection collection) throws CRLException {
        final ArrayList<X509CRLHolder> list = new ArrayList<X509CRLHolder>(collection.size());
        for (final X509CRL next : collection) {
            if (next instanceof X509CRL) {
                try {
                    list.add(new X509CRLHolder(next.getEncoded()));
                    continue;
                }
                catch (IOException ex) {
                    throw new CRLException("cannot read encoding: " + ex.getMessage());
                }
            }
            list.add((X509CRLHolder)next);
        }
        return list;
    }
}
