// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.jcajce;

import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.Provider;

class ProviderCertHelper extends CertHelper
{
    private final Provider provider;
    
    ProviderCertHelper(final Provider provider) {
        this.provider = provider;
    }
    
    @Override
    protected CertificateFactory createCertificateFactory(final String type) throws CertificateException {
        return CertificateFactory.getInstance(type, this.provider);
    }
}
