// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.jcajce;

import java.io.IOException;
import org.bouncycastle.asn1.x509.AttributeCertificate;
import org.bouncycastle.x509.X509AttributeCertificate;
import org.bouncycastle.cert.X509AttributeCertificateHolder;

public class JcaX509AttributeCertificateHolder extends X509AttributeCertificateHolder
{
    public JcaX509AttributeCertificateHolder(final X509AttributeCertificate x509AttributeCertificate) throws IOException {
        super(AttributeCertificate.getInstance(x509AttributeCertificate.getEncoded()));
    }
}
