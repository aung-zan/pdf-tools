// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.jcajce;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;
import org.bouncycastle.x509.X509AttributeCertificate;
import java.io.IOException;
import java.util.Collection;
import org.bouncycastle.util.CollectionStore;

public class JcaAttrCertStore extends CollectionStore
{
    public JcaAttrCertStore(final Collection collection) throws IOException {
        super(convertCerts(collection));
    }
    
    public JcaAttrCertStore(final X509AttributeCertificate o) throws IOException {
        this(Collections.singletonList(o));
    }
    
    private static Collection convertCerts(final Collection collection) throws IOException {
        final ArrayList<JcaX509AttributeCertificateHolder> list = new ArrayList<JcaX509AttributeCertificateHolder>(collection.size());
        for (final X509AttributeCertificate next : collection) {
            if (next instanceof X509AttributeCertificate) {
                list.add(new JcaX509AttributeCertificateHolder(next));
            }
            else {
                list.add((JcaX509AttributeCertificateHolder)next);
            }
        }
        return list;
    }
}
