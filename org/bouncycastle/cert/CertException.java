// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert;

public class CertException extends Exception
{
    private Throwable cause;
    
    public CertException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    public CertException(final String message) {
        super(message);
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
