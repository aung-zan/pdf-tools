// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert;

import java.io.OutputStream;
import org.bouncycastle.operator.ContentVerifier;
import org.bouncycastle.asn1.x509.TBSCertificate;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DEROutputStream;
import org.bouncycastle.operator.ContentVerifierProvider;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.util.Date;
import org.bouncycastle.asn1.x500.X500Name;
import java.math.BigInteger;
import java.util.Set;
import java.util.List;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.x509.Certificate;

public class X509CertificateHolder
{
    private Certificate x509Certificate;
    private Extensions extensions;
    
    private static Certificate parseBytes(final byte[] array) throws IOException {
        try {
            return Certificate.getInstance(ASN1Primitive.fromByteArray(array));
        }
        catch (ClassCastException ex) {
            throw new CertIOException("malformed data: " + ex.getMessage(), ex);
        }
        catch (IllegalArgumentException ex2) {
            throw new CertIOException("malformed data: " + ex2.getMessage(), ex2);
        }
    }
    
    public X509CertificateHolder(final byte[] array) throws IOException {
        this(parseBytes(array));
    }
    
    public X509CertificateHolder(final Certificate x509Certificate) {
        this.x509Certificate = x509Certificate;
        this.extensions = x509Certificate.getTBSCertificate().getExtensions();
    }
    
    public int getVersionNumber() {
        return this.x509Certificate.getVersionNumber();
    }
    
    @Deprecated
    public int getVersion() {
        return this.x509Certificate.getVersionNumber();
    }
    
    public boolean hasExtensions() {
        return this.extensions != null;
    }
    
    public Extension getExtension(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        if (this.extensions != null) {
            return this.extensions.getExtension(asn1ObjectIdentifier);
        }
        return null;
    }
    
    public Extensions getExtensions() {
        return this.extensions;
    }
    
    public List getExtensionOIDs() {
        return CertUtils.getExtensionOIDs(this.extensions);
    }
    
    public Set getCriticalExtensionOIDs() {
        return CertUtils.getCriticalExtensionOIDs(this.extensions);
    }
    
    public Set getNonCriticalExtensionOIDs() {
        return CertUtils.getNonCriticalExtensionOIDs(this.extensions);
    }
    
    public BigInteger getSerialNumber() {
        return this.x509Certificate.getSerialNumber().getValue();
    }
    
    public X500Name getIssuer() {
        return X500Name.getInstance(this.x509Certificate.getIssuer());
    }
    
    public X500Name getSubject() {
        return X500Name.getInstance(this.x509Certificate.getSubject());
    }
    
    public Date getNotBefore() {
        return this.x509Certificate.getStartDate().getDate();
    }
    
    public Date getNotAfter() {
        return this.x509Certificate.getEndDate().getDate();
    }
    
    public SubjectPublicKeyInfo getSubjectPublicKeyInfo() {
        return this.x509Certificate.getSubjectPublicKeyInfo();
    }
    
    public Certificate toASN1Structure() {
        return this.x509Certificate;
    }
    
    public AlgorithmIdentifier getSignatureAlgorithm() {
        return this.x509Certificate.getSignatureAlgorithm();
    }
    
    public byte[] getSignature() {
        return this.x509Certificate.getSignature().getBytes();
    }
    
    public boolean isValidOn(final Date date) {
        return !date.before(this.x509Certificate.getStartDate().getDate()) && !date.after(this.x509Certificate.getEndDate().getDate());
    }
    
    public boolean isSignatureValid(final ContentVerifierProvider contentVerifierProvider) throws CertException {
        final TBSCertificate tbsCertificate = this.x509Certificate.getTBSCertificate();
        if (!CertUtils.isAlgIdEqual(tbsCertificate.getSignature(), this.x509Certificate.getSignatureAlgorithm())) {
            throw new CertException("signature invalid - algorithm identifier mismatch");
        }
        ContentVerifier value;
        try {
            value = contentVerifierProvider.get(tbsCertificate.getSignature());
            final OutputStream outputStream = value.getOutputStream();
            new DEROutputStream(outputStream).writeObject(tbsCertificate);
            outputStream.close();
        }
        catch (Exception ex) {
            throw new CertException("unable to process signature: " + ex.getMessage(), ex);
        }
        return value.verify(this.x509Certificate.getSignature().getBytes());
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof X509CertificateHolder && this.x509Certificate.equals(((X509CertificateHolder)o).x509Certificate));
    }
    
    @Override
    public int hashCode() {
        return this.x509Certificate.hashCode();
    }
    
    public byte[] getEncoded() throws IOException {
        return this.x509Certificate.getEncoded();
    }
}
