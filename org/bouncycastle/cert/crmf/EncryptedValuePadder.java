// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.crmf;

public interface EncryptedValuePadder
{
    byte[] getPaddedData(final byte[] p0);
    
    byte[] getUnpaddedData(final byte[] p0);
}
