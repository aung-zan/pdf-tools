// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.crmf.jcajce;

import javax.crypto.CipherInputStream;
import java.io.InputStream;
import org.bouncycastle.operator.InputDecryptor;
import javax.crypto.Cipher;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.security.InvalidKeyException;
import org.bouncycastle.cert.crmf.CRMFException;
import javax.crypto.spec.SecretKeySpec;
import java.security.ProviderException;
import java.security.GeneralSecurityException;
import java.security.Key;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import org.bouncycastle.jcajce.JcaJceHelper;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;
import java.security.PrivateKey;
import org.bouncycastle.cert.crmf.ValueDecryptorGenerator;

public class JceAsymmetricValueDecryptorGenerator implements ValueDecryptorGenerator
{
    private PrivateKey recipientKey;
    private CRMFHelper helper;
    
    public JceAsymmetricValueDecryptorGenerator(final PrivateKey recipientKey) {
        this.helper = new CRMFHelper(new DefaultJcaJceHelper());
        this.recipientKey = recipientKey;
    }
    
    public JceAsymmetricValueDecryptorGenerator setProvider(final Provider provider) {
        this.helper = new CRMFHelper(new ProviderJcaJceHelper(provider));
        return this;
    }
    
    public JceAsymmetricValueDecryptorGenerator setProvider(final String s) {
        this.helper = new CRMFHelper(new NamedJcaJceHelper(s));
        return this;
    }
    
    private Key extractSecretKey(final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final byte[] array) throws CRMFException {
        try {
            Key unwrap = null;
            final Cipher cipher = this.helper.createCipher(algorithmIdentifier.getAlgorithm());
            try {
                cipher.init(4, this.recipientKey);
                unwrap = cipher.unwrap(array, algorithmIdentifier2.getAlgorithm().getId(), 3);
            }
            catch (GeneralSecurityException ex4) {}
            catch (IllegalStateException ex5) {}
            catch (UnsupportedOperationException ex6) {}
            catch (ProviderException ex7) {}
            if (unwrap == null) {
                cipher.init(2, this.recipientKey);
                unwrap = new SecretKeySpec(cipher.doFinal(array), algorithmIdentifier2.getAlgorithm().getId());
            }
            return unwrap;
        }
        catch (InvalidKeyException ex) {
            throw new CRMFException("key invalid in message.", ex);
        }
        catch (IllegalBlockSizeException ex2) {
            throw new CRMFException("illegal blocksize in message.", ex2);
        }
        catch (BadPaddingException ex3) {
            throw new CRMFException("bad padding in message.", ex3);
        }
    }
    
    public InputDecryptor getValueDecryptor(final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2, final byte[] array) throws CRMFException {
        return new InputDecryptor() {
            final /* synthetic */ Cipher val$dataCipher = JceAsymmetricValueDecryptorGenerator.this.helper.createContentCipher(JceAsymmetricValueDecryptorGenerator.this.extractSecretKey(algorithmIdentifier, algorithmIdentifier2, array), algorithmIdentifier2);
            
            public AlgorithmIdentifier getAlgorithmIdentifier() {
                return algorithmIdentifier2;
            }
            
            public InputStream getInputStream(final InputStream is) {
                return new CipherInputStream(is, this.val$dataCipher);
            }
        };
    }
}
