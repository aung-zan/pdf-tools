// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.crmf.jcajce;

import java.security.GeneralSecurityException;
import java.security.Key;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.cert.crmf.CRMFException;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import org.bouncycastle.jcajce.JcaJceHelper;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;
import javax.crypto.Mac;
import java.security.MessageDigest;
import org.bouncycastle.cert.crmf.PKMACValuesCalculator;

public class JcePKMACValuesCalculator implements PKMACValuesCalculator
{
    private MessageDigest digest;
    private Mac mac;
    private CRMFHelper helper;
    
    public JcePKMACValuesCalculator() {
        this.helper = new CRMFHelper(new DefaultJcaJceHelper());
    }
    
    public JcePKMACValuesCalculator setProvider(final Provider provider) {
        this.helper = new CRMFHelper(new ProviderJcaJceHelper(provider));
        return this;
    }
    
    public JcePKMACValuesCalculator setProvider(final String s) {
        this.helper = new CRMFHelper(new NamedJcaJceHelper(s));
        return this;
    }
    
    public void setup(final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2) throws CRMFException {
        this.digest = this.helper.createDigest(algorithmIdentifier.getAlgorithm());
        this.mac = this.helper.createMac(algorithmIdentifier2.getAlgorithm());
    }
    
    public byte[] calculateDigest(final byte[] input) {
        return this.digest.digest(input);
    }
    
    public byte[] calculateMac(final byte[] key, final byte[] input) throws CRMFException {
        try {
            this.mac.init(new SecretKeySpec(key, this.mac.getAlgorithm()));
            return this.mac.doFinal(input);
        }
        catch (GeneralSecurityException ex) {
            throw new CRMFException("failure in setup: " + ex.getMessage(), ex);
        }
    }
}
