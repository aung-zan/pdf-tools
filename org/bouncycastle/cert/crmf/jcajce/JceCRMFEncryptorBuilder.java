// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.crmf.jcajce;

import org.bouncycastle.operator.jcajce.JceGenericKey;
import org.bouncycastle.operator.GenericKey;
import javax.crypto.CipherOutputStream;
import java.io.OutputStream;
import java.security.AlgorithmParameters;
import javax.crypto.KeyGenerator;
import java.security.GeneralSecurityException;
import java.security.Key;
import javax.crypto.Cipher;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import javax.crypto.SecretKey;
import org.bouncycastle.cert.crmf.CRMFException;
import org.bouncycastle.operator.OutputEncryptor;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import org.bouncycastle.jcajce.JcaJceHelper;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;
import java.security.SecureRandom;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public class JceCRMFEncryptorBuilder
{
    private final ASN1ObjectIdentifier encryptionOID;
    private final int keySize;
    private CRMFHelper helper;
    private SecureRandom random;
    
    public JceCRMFEncryptorBuilder(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        this(asn1ObjectIdentifier, -1);
    }
    
    public JceCRMFEncryptorBuilder(final ASN1ObjectIdentifier encryptionOID, final int keySize) {
        this.helper = new CRMFHelper(new DefaultJcaJceHelper());
        this.encryptionOID = encryptionOID;
        this.keySize = keySize;
    }
    
    public JceCRMFEncryptorBuilder setProvider(final Provider provider) {
        this.helper = new CRMFHelper(new ProviderJcaJceHelper(provider));
        return this;
    }
    
    public JceCRMFEncryptorBuilder setProvider(final String s) {
        this.helper = new CRMFHelper(new NamedJcaJceHelper(s));
        return this;
    }
    
    public JceCRMFEncryptorBuilder setSecureRandom(final SecureRandom random) {
        this.random = random;
        return this;
    }
    
    public OutputEncryptor build() throws CRMFException {
        return new CRMFOutputEncryptor(this.encryptionOID, this.keySize, this.random);
    }
    
    private class CRMFOutputEncryptor implements OutputEncryptor
    {
        private SecretKey encKey;
        private AlgorithmIdentifier algorithmIdentifier;
        private Cipher cipher;
        
        CRMFOutputEncryptor(final ASN1ObjectIdentifier asn1ObjectIdentifier, final int keysize, SecureRandom random) throws CRMFException {
            final KeyGenerator keyGenerator = JceCRMFEncryptorBuilder.this.helper.createKeyGenerator(asn1ObjectIdentifier);
            if (random == null) {
                random = new SecureRandom();
            }
            if (keysize < 0) {
                keyGenerator.init(random);
            }
            else {
                keyGenerator.init(keysize, random);
            }
            this.cipher = JceCRMFEncryptorBuilder.this.helper.createCipher(asn1ObjectIdentifier);
            this.encKey = keyGenerator.generateKey();
            AlgorithmParameters params = JceCRMFEncryptorBuilder.this.helper.generateParameters(asn1ObjectIdentifier, this.encKey, random);
            try {
                this.cipher.init(1, this.encKey, params, random);
            }
            catch (GeneralSecurityException ex) {
                throw new CRMFException("unable to initialize cipher: " + ex.getMessage(), ex);
            }
            if (params == null) {
                params = this.cipher.getParameters();
            }
            this.algorithmIdentifier = JceCRMFEncryptorBuilder.this.helper.getAlgorithmIdentifier(asn1ObjectIdentifier, params);
        }
        
        public AlgorithmIdentifier getAlgorithmIdentifier() {
            return this.algorithmIdentifier;
        }
        
        public OutputStream getOutputStream(final OutputStream os) {
            return new CipherOutputStream(os, this.cipher);
        }
        
        public GenericKey getKey() {
            return new JceGenericKey(this.algorithmIdentifier, this.encKey);
        }
    }
}
