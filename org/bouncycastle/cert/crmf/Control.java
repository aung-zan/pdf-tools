// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.crmf;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public interface Control
{
    ASN1ObjectIdentifier getType();
    
    ASN1Encodable getValue();
}
