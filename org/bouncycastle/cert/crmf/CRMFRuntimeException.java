// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert.crmf;

public class CRMFRuntimeException extends RuntimeException
{
    private Throwable cause;
    
    public CRMFRuntimeException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
