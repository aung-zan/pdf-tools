// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert;

public class CertRuntimeException extends RuntimeException
{
    private Throwable cause;
    
    public CertRuntimeException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
