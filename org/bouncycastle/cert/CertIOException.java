// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.cert;

import java.io.IOException;

public class CertIOException extends IOException
{
    private Throwable cause;
    
    public CertIOException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    public CertIOException(final String message) {
        super(message);
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
