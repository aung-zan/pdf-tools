// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.eac.jcajce;

import java.security.NoSuchAlgorithmException;
import java.security.KeyFactory;

class DefaultEACHelper implements EACHelper
{
    public KeyFactory createKeyFactory(final String algorithm) throws NoSuchAlgorithmException {
        return KeyFactory.getInstance(algorithm);
    }
}
