// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.eac.jcajce;

import java.math.BigInteger;
import java.security.spec.ECField;
import java.security.spec.EllipticCurve;
import java.security.spec.ECPoint;
import java.security.spec.ECFieldFp;
import java.security.interfaces.ECPublicKey;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.math.ec.ECCurve;
import java.security.KeyFactory;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import org.bouncycastle.eac.EACException;
import java.security.spec.KeySpec;
import java.security.spec.RSAPublicKeySpec;
import org.bouncycastle.asn1.eac.RSAPublicKey;
import org.bouncycastle.asn1.eac.ECDSAPublicKey;
import org.bouncycastle.asn1.eac.EACObjectIdentifiers;
import java.security.PublicKey;
import org.bouncycastle.asn1.eac.PublicKeyDataObject;
import java.security.Provider;

public class JcaPublicKeyConverter
{
    private EACHelper helper;
    
    public JcaPublicKeyConverter() {
        this.helper = new DefaultEACHelper();
    }
    
    public JcaPublicKeyConverter setProvider(final String s) {
        this.helper = new NamedEACHelper(s);
        return this;
    }
    
    public JcaPublicKeyConverter setProvider(final Provider provider) {
        this.helper = new ProviderEACHelper(provider);
        return this;
    }
    
    public PublicKey getKey(final PublicKeyDataObject publicKeyDataObject) throws EACException, InvalidKeySpecException {
        if (publicKeyDataObject.getUsage().on(EACObjectIdentifiers.id_TA_ECDSA)) {
            return this.getECPublicKeyPublicKey((ECDSAPublicKey)publicKeyDataObject);
        }
        final RSAPublicKey rsaPublicKey = (RSAPublicKey)publicKeyDataObject;
        final RSAPublicKeySpec keySpec = new RSAPublicKeySpec(rsaPublicKey.getModulus(), rsaPublicKey.getPublicExponent());
        try {
            return this.helper.createKeyFactory("RSA").generatePublic(keySpec);
        }
        catch (NoSuchProviderException ex) {
            throw new EACException("cannot find provider: " + ex.getMessage(), ex);
        }
        catch (NoSuchAlgorithmException ex2) {
            throw new EACException("cannot find algorithm ECDSA: " + ex2.getMessage(), ex2);
        }
    }
    
    private PublicKey getECPublicKeyPublicKey(final ECDSAPublicKey ecdsaPublicKey) throws EACException, InvalidKeySpecException {
        final ECParameterSpec params = this.getParams(ecdsaPublicKey);
        final ECPublicKeySpec keySpec = new ECPublicKeySpec(params.getCurve().decodePoint(ecdsaPublicKey.getPublicPointY()), params);
        KeyFactory keyFactory;
        try {
            keyFactory = this.helper.createKeyFactory("ECDSA");
        }
        catch (NoSuchProviderException ex) {
            throw new EACException("cannot find provider: " + ex.getMessage(), ex);
        }
        catch (NoSuchAlgorithmException ex2) {
            throw new EACException("cannot find algorithm ECDSA: " + ex2.getMessage(), ex2);
        }
        return keyFactory.generatePublic(keySpec);
    }
    
    private ECParameterSpec getParams(final ECDSAPublicKey ecdsaPublicKey) {
        if (!ecdsaPublicKey.hasParameters()) {
            throw new IllegalArgumentException("Public key does not contains EC Params");
        }
        final ECCurve.Fp fp = new ECCurve.Fp(ecdsaPublicKey.getPrimeModulusP(), ecdsaPublicKey.getFirstCoefA(), ecdsaPublicKey.getSecondCoefB());
        return new ECParameterSpec(fp, fp.decodePoint(ecdsaPublicKey.getBasePointG()), ecdsaPublicKey.getOrderOfBasePointR(), ecdsaPublicKey.getCofactorF());
    }
    
    public PublicKeyDataObject getPublicKeyDataObject(final ASN1ObjectIdentifier asn1ObjectIdentifier, final PublicKey publicKey) {
        if (publicKey instanceof java.security.interfaces.RSAPublicKey) {
            final java.security.interfaces.RSAPublicKey rsaPublicKey = (java.security.interfaces.RSAPublicKey)publicKey;
            return new RSAPublicKey(asn1ObjectIdentifier, rsaPublicKey.getModulus(), rsaPublicKey.getPublicExponent());
        }
        final ECPublicKey ecPublicKey = (ECPublicKey)publicKey;
        final java.security.spec.ECParameterSpec params = ecPublicKey.getParams();
        return new ECDSAPublicKey(asn1ObjectIdentifier, ((ECFieldFp)params.getCurve().getField()).getP(), params.getCurve().getA(), params.getCurve().getB(), convertPoint(convertCurve(params.getCurve()), params.getGenerator(), false).getEncoded(), params.getOrder(), convertPoint(convertCurve(params.getCurve()), ecPublicKey.getW(), false).getEncoded(), params.getCofactor());
    }
    
    private static org.bouncycastle.math.ec.ECPoint convertPoint(final ECCurve ecCurve, final ECPoint ecPoint, final boolean b) {
        return ecCurve.createPoint(ecPoint.getAffineX(), ecPoint.getAffineY(), b);
    }
    
    private static ECCurve convertCurve(final EllipticCurve ellipticCurve) {
        final ECField field = ellipticCurve.getField();
        final BigInteger a = ellipticCurve.getA();
        final BigInteger b = ellipticCurve.getB();
        if (field instanceof ECFieldFp) {
            return new ECCurve.Fp(((ECFieldFp)field).getP(), a, b);
        }
        throw new IllegalStateException("not implemented yet!!!");
    }
}
