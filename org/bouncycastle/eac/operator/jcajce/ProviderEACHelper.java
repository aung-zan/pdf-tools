// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.eac.operator.jcajce;

import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.Provider;

class ProviderEACHelper extends EACHelper
{
    private final Provider provider;
    
    ProviderEACHelper(final Provider provider) {
        this.provider = provider;
    }
    
    @Override
    protected Signature createSignature(final String algorithm) throws NoSuchAlgorithmException {
        return Signature.getInstance(algorithm, this.provider);
    }
}
