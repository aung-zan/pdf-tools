// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.eac.operator.jcajce;

import org.bouncycastle.operator.OperatorStreamException;
import java.io.IOException;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERInteger;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.security.Signature;
import java.security.SignatureException;
import org.bouncycastle.operator.RuntimeOperatorException;
import org.bouncycastle.asn1.eac.EACObjectIdentifiers;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.eac.operator.EACSignatureVerifier;
import java.security.PublicKey;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.Provider;

public class JcaEACSignatureVerifierBuilder
{
    private EACHelper helper;
    
    public JcaEACSignatureVerifierBuilder() {
        this.helper = new DefaultEACHelper();
    }
    
    public JcaEACSignatureVerifierBuilder setProvider(final String s) {
        this.helper = new NamedEACHelper(s);
        return this;
    }
    
    public JcaEACSignatureVerifierBuilder setProvider(final Provider provider) {
        this.helper = new ProviderEACHelper(provider);
        return this;
    }
    
    public EACSignatureVerifier build(final ASN1ObjectIdentifier asn1ObjectIdentifier, final PublicKey publicKey) throws OperatorCreationException {
        Signature signature;
        try {
            signature = this.helper.getSignature(asn1ObjectIdentifier);
            signature.initVerify(publicKey);
        }
        catch (NoSuchAlgorithmException ex) {
            throw new OperatorCreationException("unable to find algorithm: " + ex.getMessage(), ex);
        }
        catch (NoSuchProviderException ex2) {
            throw new OperatorCreationException("unable to find provider: " + ex2.getMessage(), ex2);
        }
        catch (InvalidKeyException ex3) {
            throw new OperatorCreationException("invalid key: " + ex3.getMessage(), ex3);
        }
        return new EACSignatureVerifier() {
            final /* synthetic */ SignatureOutputStream val$sigStream = new SignatureOutputStream(signature);
            
            public ASN1ObjectIdentifier getUsageIdentifier() {
                return asn1ObjectIdentifier;
            }
            
            public OutputStream getOutputStream() {
                return this.val$sigStream;
            }
            
            public boolean verify(final byte[] array) {
                try {
                    if (asn1ObjectIdentifier.on(EACObjectIdentifiers.id_TA_ECDSA)) {
                        try {
                            return this.val$sigStream.verify(derEncode(array));
                        }
                        catch (Exception ex2) {
                            return false;
                        }
                    }
                    return this.val$sigStream.verify(array);
                }
                catch (SignatureException ex) {
                    throw new RuntimeOperatorException("exception obtaining signature: " + ex.getMessage(), ex);
                }
            }
        };
    }
    
    private static byte[] derEncode(final byte[] array) throws IOException {
        final int n = array.length / 2;
        final byte[] magnitude = new byte[n];
        final byte[] magnitude2 = new byte[n];
        System.arraycopy(array, 0, magnitude, 0, n);
        System.arraycopy(array, n, magnitude2, 0, n);
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        asn1EncodableVector.add(new DERInteger(new BigInteger(1, magnitude)));
        asn1EncodableVector.add(new DERInteger(new BigInteger(1, magnitude2)));
        return new DERSequence(asn1EncodableVector).getEncoded();
    }
    
    private class SignatureOutputStream extends OutputStream
    {
        private Signature sig;
        
        SignatureOutputStream(final Signature sig) {
            this.sig = sig;
        }
        
        @Override
        public void write(final byte[] data, final int off, final int len) throws IOException {
            try {
                this.sig.update(data, off, len);
            }
            catch (SignatureException ex) {
                throw new OperatorStreamException("exception in content signer: " + ex.getMessage(), ex);
            }
        }
        
        @Override
        public void write(final byte[] data) throws IOException {
            try {
                this.sig.update(data);
            }
            catch (SignatureException ex) {
                throw new OperatorStreamException("exception in content signer: " + ex.getMessage(), ex);
            }
        }
        
        @Override
        public void write(final int n) throws IOException {
            try {
                this.sig.update((byte)n);
            }
            catch (SignatureException ex) {
                throw new OperatorStreamException("exception in content signer: " + ex.getMessage(), ex);
            }
        }
        
        boolean verify(final byte[] signature) throws SignatureException {
            return this.sig.verify(signature);
        }
    }
}
