// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.eac.operator.jcajce;

import java.security.NoSuchAlgorithmException;
import java.security.Signature;

class DefaultEACHelper extends EACHelper
{
    @Override
    protected Signature createSignature(final String algorithm) throws NoSuchAlgorithmException {
        return Signature.getInstance(algorithm);
    }
}
