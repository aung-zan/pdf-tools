// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.eac.operator.jcajce;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Signature;

class NamedEACHelper extends EACHelper
{
    private final String providerName;
    
    NamedEACHelper(final String providerName) {
        this.providerName = providerName;
    }
    
    @Override
    protected Signature createSignature(final String algorithm) throws NoSuchProviderException, NoSuchAlgorithmException {
        return Signature.getInstance(algorithm, this.providerName);
    }
}
