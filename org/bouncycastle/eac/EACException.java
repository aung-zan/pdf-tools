// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.eac;

public class EACException extends Exception
{
    private Throwable cause;
    
    public EACException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    public EACException(final String message) {
        super(message);
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
