// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.eac;

import java.io.IOException;

public class EACIOException extends IOException
{
    private Throwable cause;
    
    public EACIOException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    public EACIOException(final String message) {
        super(message);
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
