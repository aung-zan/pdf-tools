// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pkcs;

import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

public interface PKCS12MacCalculatorBuilderProvider
{
    PKCS12MacCalculatorBuilder get(final AlgorithmIdentifier p0);
}
