// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pkcs;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.pkcs.PKCS12PBEParams;
import org.bouncycastle.asn1.pkcs.MacData;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.pkcs.ContentInfo;
import java.io.IOException;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.pkcs.Pfx;

public class PKCS12PfxPdu
{
    private Pfx pfx;
    
    private static Pfx parseBytes(final byte[] array) throws IOException {
        try {
            return Pfx.getInstance(ASN1Primitive.fromByteArray(array));
        }
        catch (ClassCastException ex) {
            throw new CertIOException("malformed data: " + ex.getMessage(), ex);
        }
        catch (IllegalArgumentException ex2) {
            throw new CertIOException("malformed data: " + ex2.getMessage(), ex2);
        }
    }
    
    public PKCS12PfxPdu(final Pfx pfx) {
        this.pfx = pfx;
    }
    
    public PKCS12PfxPdu(final byte[] array) throws IOException {
        this(parseBytes(array));
    }
    
    public ContentInfo[] getContentInfos() {
        final ASN1Sequence instance = ASN1Sequence.getInstance(ASN1OctetString.getInstance(this.pfx.getAuthSafe().getContent()).getOctets());
        final ContentInfo[] array = new ContentInfo[instance.size()];
        for (int i = 0; i != instance.size(); ++i) {
            array[i] = ContentInfo.getInstance(instance.getObjectAt(i));
        }
        return array;
    }
    
    public boolean hasMac() {
        return this.pfx.getMacData() != null;
    }
    
    public AlgorithmIdentifier getMacAlgorithmID() {
        final MacData macData = this.pfx.getMacData();
        if (macData != null) {
            return macData.getMac().getAlgorithmId();
        }
        return null;
    }
    
    public boolean isMacValid(final PKCS12MacCalculatorBuilderProvider pkcs12MacCalculatorBuilderProvider, final char[] array) throws PKCSException {
        if (this.hasMac()) {
            final MacData macData = this.pfx.getMacData();
            final MacDataGenerator macDataGenerator = new MacDataGenerator(pkcs12MacCalculatorBuilderProvider.get(new AlgorithmIdentifier(macData.getMac().getAlgorithmId().getAlgorithm(), new PKCS12PBEParams(macData.getSalt(), macData.getIterationCount().intValue()))));
            try {
                return Arrays.constantTimeAreEqual(macDataGenerator.build(array, ASN1OctetString.getInstance(this.pfx.getAuthSafe().getContent()).getOctets()).getEncoded(), this.pfx.getMacData().getEncoded());
            }
            catch (IOException ex) {
                throw new PKCSException("unable to process AuthSafe: " + ex.getMessage());
            }
        }
        throw new IllegalStateException("no MAC present on PFX");
    }
    
    public Pfx toASN1Structure() {
        return this.pfx;
    }
    
    public byte[] getEncoded() throws IOException {
        return this.toASN1Structure().getEncoded();
    }
    
    public byte[] getEncoded(final String s) throws IOException {
        return this.toASN1Structure().getEncoded(s);
    }
}
