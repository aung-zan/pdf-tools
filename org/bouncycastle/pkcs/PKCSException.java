// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pkcs;

public class PKCSException extends Exception
{
    private Throwable cause;
    
    public PKCSException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    public PKCSException(final String message) {
        super(message);
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
