// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pkcs;

import java.io.IOException;

public class PKCSIOException extends IOException
{
    private Throwable cause;
    
    public PKCSIOException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    public PKCSIOException(final String message) {
        super(message);
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
