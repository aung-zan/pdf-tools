// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pkcs.jcajce;

import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.operator.GenericKey;
import org.bouncycastle.jcajce.io.MacOutputStream;
import java.io.OutputStream;
import org.bouncycastle.asn1.ASN1Encodable;
import javax.crypto.Mac;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Key;
import javax.crypto.spec.PBEParameterSpec;
import java.security.spec.KeySpec;
import javax.crypto.spec.PBEKeySpec;
import org.bouncycastle.asn1.pkcs.PKCS12PBEParams;
import org.bouncycastle.operator.MacCalculator;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCS12MacCalculatorBuilder;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;
import org.bouncycastle.jcajce.JcaJceHelper;
import org.bouncycastle.pkcs.PKCS12MacCalculatorBuilderProvider;

public class JcePKCS12MacCalculatorBuilderProvider implements PKCS12MacCalculatorBuilderProvider
{
    private JcaJceHelper helper;
    
    public JcePKCS12MacCalculatorBuilderProvider() {
        this.helper = new DefaultJcaJceHelper();
    }
    
    public JcePKCS12MacCalculatorBuilderProvider setProvider(final Provider provider) {
        this.helper = new ProviderJcaJceHelper(provider);
        return this;
    }
    
    public JcePKCS12MacCalculatorBuilderProvider setProvider(final String s) {
        this.helper = new NamedJcaJceHelper(s);
        return this;
    }
    
    public PKCS12MacCalculatorBuilder get(final AlgorithmIdentifier algorithmIdentifier) {
        return new PKCS12MacCalculatorBuilder() {
            public MacCalculator build(final char[] password) throws OperatorCreationException {
                final PKCS12PBEParams instance = PKCS12PBEParams.getInstance(algorithmIdentifier.getParameters());
                try {
                    final ASN1ObjectIdentifier algorithm = algorithmIdentifier.getAlgorithm();
                    final Mac mac = JcePKCS12MacCalculatorBuilderProvider.this.helper.createMac(algorithm.getId());
                    mac.init(JcePKCS12MacCalculatorBuilderProvider.this.helper.createSecretKeyFactory(algorithm.getId()).generateSecret(new PBEKeySpec(password)), new PBEParameterSpec(instance.getIV(), instance.getIterations().intValue()));
                    return new MacCalculator() {
                        public AlgorithmIdentifier getAlgorithmIdentifier() {
                            return new AlgorithmIdentifier(algorithm, instance);
                        }
                        
                        public OutputStream getOutputStream() {
                            return new MacOutputStream(mac);
                        }
                        
                        public byte[] getMac() {
                            return mac.doFinal();
                        }
                        
                        public GenericKey getKey() {
                            return new GenericKey(this.getAlgorithmIdentifier(), PBEParametersGenerator.PKCS12PasswordToBytes(password));
                        }
                    };
                }
                catch (Exception ex) {
                    throw new OperatorCreationException("unable to create MAC calculator: " + ex.getMessage(), ex);
                }
            }
            
            public AlgorithmIdentifier getDigestAlgorithmIdentifier() {
                return new AlgorithmIdentifier(algorithmIdentifier.getAlgorithm(), DERNull.INSTANCE);
            }
        };
    }
}
