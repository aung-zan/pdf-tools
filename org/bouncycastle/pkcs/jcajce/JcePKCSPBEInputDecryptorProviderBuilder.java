// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pkcs.jcajce;

import javax.crypto.SecretKeyFactory;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.operator.jcajce.JceGenericKey;
import org.bouncycastle.operator.GenericKey;
import javax.crypto.CipherInputStream;
import java.io.InputStream;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.pkcs.PBKDF2Params;
import org.bouncycastle.asn1.pkcs.PBES2Parameters;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Key;
import org.bouncycastle.jcajce.provider.symmetric.util.BCPBEKey;
import java.security.spec.KeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import org.bouncycastle.asn1.pkcs.PKCS12PBEParams;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.operator.InputDecryptor;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import javax.crypto.SecretKey;
import javax.crypto.Cipher;
import org.bouncycastle.operator.InputDecryptorProvider;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import org.bouncycastle.operator.DefaultSecretKeyProvider;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;
import org.bouncycastle.operator.SecretKeySizeProvider;
import org.bouncycastle.jcajce.JcaJceHelper;

public class JcePKCSPBEInputDecryptorProviderBuilder
{
    private JcaJceHelper helper;
    private boolean wrongPKCS12Zero;
    private SecretKeySizeProvider keySizeProvider;
    
    public JcePKCSPBEInputDecryptorProviderBuilder() {
        this.helper = new DefaultJcaJceHelper();
        this.wrongPKCS12Zero = false;
        this.keySizeProvider = DefaultSecretKeyProvider.INSTANCE;
    }
    
    public JcePKCSPBEInputDecryptorProviderBuilder setProvider(final Provider provider) {
        this.helper = new ProviderJcaJceHelper(provider);
        return this;
    }
    
    public JcePKCSPBEInputDecryptorProviderBuilder setProvider(final String s) {
        this.helper = new NamedJcaJceHelper(s);
        return this;
    }
    
    public JcePKCSPBEInputDecryptorProviderBuilder setTryWrongPKCS12Zero(final boolean wrongPKCS12Zero) {
        this.wrongPKCS12Zero = wrongPKCS12Zero;
        return this;
    }
    
    public JcePKCSPBEInputDecryptorProviderBuilder setKeySizeProvider(final SecretKeySizeProvider keySizeProvider) {
        this.keySizeProvider = keySizeProvider;
        return this;
    }
    
    public InputDecryptorProvider build(final char[] array) {
        return new InputDecryptorProvider() {
            private Cipher cipher;
            private SecretKey key;
            private AlgorithmIdentifier encryptionAlg;
            
            public InputDecryptor get(final AlgorithmIdentifier encryptionAlg) throws OperatorCreationException {
                final ASN1ObjectIdentifier algorithm = encryptionAlg.getAlgorithm();
                try {
                    if (algorithm.on(PKCSObjectIdentifiers.pkcs_12PbeIds)) {
                        final PKCS12PBEParams instance = PKCS12PBEParams.getInstance(encryptionAlg.getParameters());
                        final PBEKeySpec keySpec = new PBEKeySpec(array);
                        final SecretKeyFactory secretKeyFactory = JcePKCSPBEInputDecryptorProviderBuilder.this.helper.createSecretKeyFactory(algorithm.getId());
                        final PBEParameterSpec params = new PBEParameterSpec(instance.getIV(), instance.getIterations().intValue());
                        this.key = secretKeyFactory.generateSecret(keySpec);
                        if (this.key instanceof BCPBEKey) {
                            ((BCPBEKey)this.key).setTryWrongPKCS12Zero(JcePKCSPBEInputDecryptorProviderBuilder.this.wrongPKCS12Zero);
                        }
                        (this.cipher = JcePKCSPBEInputDecryptorProviderBuilder.this.helper.createCipher(algorithm.getId())).init(2, this.key, params);
                        this.encryptionAlg = encryptionAlg;
                    }
                    else if (algorithm.equals(PKCSObjectIdentifiers.id_PBES2)) {
                        final PBES2Parameters instance2 = PBES2Parameters.getInstance(encryptionAlg.getParameters());
                        final PBKDF2Params instance3 = PBKDF2Params.getInstance(instance2.getKeyDerivationFunc().getParameters());
                        this.key = JcePKCSPBEInputDecryptorProviderBuilder.this.helper.createSecretKeyFactory(instance2.getKeyDerivationFunc().getAlgorithm().getId()).generateSecret(new PBEKeySpec(array, instance3.getSalt(), instance3.getIterationCount().intValue(), JcePKCSPBEInputDecryptorProviderBuilder.this.keySizeProvider.getKeySize(AlgorithmIdentifier.getInstance(instance2.getEncryptionScheme()))));
                        this.cipher = JcePKCSPBEInputDecryptorProviderBuilder.this.helper.createCipher(instance2.getEncryptionScheme().getAlgorithm().getId());
                        this.encryptionAlg = AlgorithmIdentifier.getInstance(instance2.getEncryptionScheme());
                        this.cipher.init(2, this.key, new IvParameterSpec(ASN1OctetString.getInstance(instance2.getEncryptionScheme().getParameters()).getOctets()));
                    }
                }
                catch (Exception ex) {
                    throw new OperatorCreationException("unable to create InputDecryptor: " + ex.getMessage(), ex);
                }
                return new InputDecryptor() {
                    public AlgorithmIdentifier getAlgorithmIdentifier() {
                        return InputDecryptorProvider.this.encryptionAlg;
                    }
                    
                    public InputStream getInputStream(final InputStream is) {
                        return new CipherInputStream(is, InputDecryptorProvider.this.cipher);
                    }
                    
                    public GenericKey getKey() {
                        return new JceGenericKey(InputDecryptorProvider.this.encryptionAlg, InputDecryptorProvider.this.key);
                    }
                };
            }
        };
    }
}
