// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pkcs.jcajce;

import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.operator.GenericKey;
import org.bouncycastle.jcajce.io.MacOutputStream;
import java.io.OutputStream;
import org.bouncycastle.asn1.pkcs.PKCS12PBEParams;
import javax.crypto.Mac;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Key;
import javax.crypto.spec.PBEParameterSpec;
import java.security.spec.KeySpec;
import javax.crypto.spec.PBEKeySpec;
import org.bouncycastle.operator.MacCalculator;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import java.security.SecureRandom;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.crypto.ExtendedDigest;
import org.bouncycastle.jcajce.JcaJceHelper;
import org.bouncycastle.pkcs.PKCS12MacCalculatorBuilder;

public class JcePKCS12MacCalculatorBuilder implements PKCS12MacCalculatorBuilder
{
    private JcaJceHelper helper;
    private ExtendedDigest digest;
    private ASN1ObjectIdentifier algorithm;
    private SecureRandom random;
    private int saltLength;
    private int iterationCount;
    
    public JcePKCS12MacCalculatorBuilder() {
        this(OIWObjectIdentifiers.idSHA1);
    }
    
    public JcePKCS12MacCalculatorBuilder(final ASN1ObjectIdentifier algorithm) {
        this.helper = new DefaultJcaJceHelper();
        this.iterationCount = 1024;
        this.algorithm = algorithm;
    }
    
    public JcePKCS12MacCalculatorBuilder setProvider(final Provider provider) {
        this.helper = new ProviderJcaJceHelper(provider);
        return this;
    }
    
    public JcePKCS12MacCalculatorBuilder setProvider(final String s) {
        this.helper = new NamedJcaJceHelper(s);
        return this;
    }
    
    public AlgorithmIdentifier getDigestAlgorithmIdentifier() {
        return new AlgorithmIdentifier(this.algorithm, DERNull.INSTANCE);
    }
    
    public MacCalculator build(final char[] password) throws OperatorCreationException {
        if (this.random == null) {
            this.random = new SecureRandom();
        }
        try {
            final Mac mac = this.helper.createMac(this.algorithm.getId());
            this.saltLength = mac.getMacLength();
            final byte[] array = new byte[this.saltLength];
            this.random.nextBytes(array);
            mac.init(this.helper.createSecretKeyFactory(this.algorithm.getId()).generateSecret(new PBEKeySpec(password)), new PBEParameterSpec(array, this.iterationCount));
            return new MacCalculator() {
                public AlgorithmIdentifier getAlgorithmIdentifier() {
                    return new AlgorithmIdentifier(JcePKCS12MacCalculatorBuilder.this.algorithm, new PKCS12PBEParams(array, JcePKCS12MacCalculatorBuilder.this.iterationCount));
                }
                
                public OutputStream getOutputStream() {
                    return new MacOutputStream(mac);
                }
                
                public byte[] getMac() {
                    return mac.doFinal();
                }
                
                public GenericKey getKey() {
                    return new GenericKey(this.getAlgorithmIdentifier(), PBEParametersGenerator.PKCS12PasswordToBytes(password));
                }
            };
        }
        catch (Exception ex) {
            throw new OperatorCreationException("unable to create MAC calculator: " + ex.getMessage(), ex);
        }
    }
}
