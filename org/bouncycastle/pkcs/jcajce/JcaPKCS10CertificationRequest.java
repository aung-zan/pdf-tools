// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pkcs.jcajce;

import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.security.KeyFactory;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.InvalidKeyException;
import java.security.spec.KeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.X509EncodedKeySpec;
import java.security.PublicKey;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import java.io.IOException;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;
import org.bouncycastle.asn1.pkcs.CertificationRequest;
import org.bouncycastle.jcajce.JcaJceHelper;
import java.util.Hashtable;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

public class JcaPKCS10CertificationRequest extends PKCS10CertificationRequest
{
    private static Hashtable keyAlgorithms;
    private JcaJceHelper helper;
    
    public JcaPKCS10CertificationRequest(final CertificationRequest certificationRequest) {
        super(certificationRequest);
        this.helper = new DefaultJcaJceHelper();
    }
    
    public JcaPKCS10CertificationRequest(final byte[] array) throws IOException {
        super(array);
        this.helper = new DefaultJcaJceHelper();
    }
    
    public JcaPKCS10CertificationRequest(final PKCS10CertificationRequest pkcs10CertificationRequest) {
        super(pkcs10CertificationRequest.toASN1Structure());
        this.helper = new DefaultJcaJceHelper();
    }
    
    public JcaPKCS10CertificationRequest setProvider(final String s) {
        this.helper = new NamedJcaJceHelper(s);
        return this;
    }
    
    public JcaPKCS10CertificationRequest setProvider(final Provider provider) {
        this.helper = new ProviderJcaJceHelper(provider);
        return this;
    }
    
    public PublicKey getPublicKey() throws InvalidKeyException, NoSuchAlgorithmException {
        try {
            final SubjectPublicKeyInfo subjectPublicKeyInfo = this.getSubjectPublicKeyInfo();
            final X509EncodedKeySpec keySpec = new X509EncodedKeySpec(subjectPublicKeyInfo.getEncoded());
            KeyFactory keyFactory;
            try {
                keyFactory = this.helper.createKeyFactory(subjectPublicKeyInfo.getAlgorithm().getAlgorithm().getId());
            }
            catch (NoSuchAlgorithmException ex) {
                if (JcaPKCS10CertificationRequest.keyAlgorithms.get(subjectPublicKeyInfo.getAlgorithm().getAlgorithm()) == null) {
                    throw ex;
                }
                keyFactory = this.helper.createKeyFactory(JcaPKCS10CertificationRequest.keyAlgorithms.get(subjectPublicKeyInfo.getAlgorithm().getAlgorithm()));
            }
            return keyFactory.generatePublic(keySpec);
        }
        catch (InvalidKeySpecException ex3) {
            throw new InvalidKeyException("error decoding public key");
        }
        catch (IOException ex4) {
            throw new InvalidKeyException("error extracting key encoding");
        }
        catch (NoSuchProviderException ex2) {
            throw new NoSuchAlgorithmException("cannot find provider: " + ex2.getMessage());
        }
    }
    
    static {
        (JcaPKCS10CertificationRequest.keyAlgorithms = new Hashtable()).put(PKCSObjectIdentifiers.rsaEncryption, "RSA");
        JcaPKCS10CertificationRequest.keyAlgorithms.put(X9ObjectIdentifiers.id_dsa, "DSA");
    }
}
