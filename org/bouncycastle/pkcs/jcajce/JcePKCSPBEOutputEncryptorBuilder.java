// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pkcs.jcajce;

import org.bouncycastle.asn1.bc.BCObjectIdentifiers;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.operator.GenericKey;
import javax.crypto.CipherOutputStream;
import java.io.OutputStream;
import javax.crypto.Cipher;
import org.bouncycastle.asn1.pkcs.PBES2Parameters;
import org.bouncycastle.asn1.pkcs.EncryptionScheme;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.pkcs.KeyDerivationFunc;
import org.bouncycastle.asn1.pkcs.PBKDF2Params;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.pkcs.PKCS12PBEParams;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Key;
import java.security.spec.KeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import org.bouncycastle.operator.OutputEncryptor;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.operator.DefaultSecretKeyProvider;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;
import org.bouncycastle.operator.SecretKeySizeProvider;
import java.security.SecureRandom;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.jcajce.JcaJceHelper;

public class JcePKCSPBEOutputEncryptorBuilder
{
    private JcaJceHelper helper;
    private ASN1ObjectIdentifier algorithm;
    private ASN1ObjectIdentifier keyEncAlgorithm;
    private SecureRandom random;
    private SecretKeySizeProvider keySizeProvider;
    
    public JcePKCSPBEOutputEncryptorBuilder(final ASN1ObjectIdentifier keyEncAlgorithm) {
        this.helper = new DefaultJcaJceHelper();
        this.keySizeProvider = DefaultSecretKeyProvider.INSTANCE;
        if (this.isPKCS12(keyEncAlgorithm)) {
            this.algorithm = keyEncAlgorithm;
            this.keyEncAlgorithm = keyEncAlgorithm;
        }
        else {
            this.algorithm = PKCSObjectIdentifiers.id_PBES2;
            this.keyEncAlgorithm = keyEncAlgorithm;
        }
    }
    
    public JcePKCSPBEOutputEncryptorBuilder setProvider(final Provider provider) {
        this.helper = new ProviderJcaJceHelper(provider);
        return this;
    }
    
    public JcePKCSPBEOutputEncryptorBuilder setProvider(final String s) {
        this.helper = new NamedJcaJceHelper(s);
        return this;
    }
    
    public JcePKCSPBEOutputEncryptorBuilder setKeySizeProvider(final SecretKeySizeProvider keySizeProvider) {
        this.keySizeProvider = keySizeProvider;
        return this;
    }
    
    public OutputEncryptor build(final char[] array) throws OperatorCreationException {
        if (this.random == null) {
            this.random = new SecureRandom();
        }
        final byte[] salt = new byte[20];
        this.random.nextBytes(salt);
        try {
            Cipher cipher;
            AlgorithmIdentifier algorithmIdentifier;
            if (this.algorithm.on(PKCSObjectIdentifiers.pkcs_12PbeIds)) {
                final PBEKeySpec keySpec = new PBEKeySpec(array);
                final SecretKeyFactory secretKeyFactory = this.helper.createSecretKeyFactory(this.algorithm.getId());
                final PBEParameterSpec params = new PBEParameterSpec(salt, 1024);
                final SecretKey generateSecret = secretKeyFactory.generateSecret(keySpec);
                cipher = this.helper.createCipher(this.algorithm.getId());
                cipher.init(1, generateSecret, params);
                algorithmIdentifier = new AlgorithmIdentifier(this.algorithm, new PKCS12PBEParams(salt, 1024));
            }
            else {
                if (!this.algorithm.equals(PKCSObjectIdentifiers.id_PBES2)) {
                    throw new OperatorCreationException("unrecognised algorithm");
                }
                final SecretKey generateSecret2 = this.helper.createSecretKeyFactory(PKCSObjectIdentifiers.id_PBKDF2.getId()).generateSecret(new PBEKeySpec(array, salt, 1024, this.keySizeProvider.getKeySize(new AlgorithmIdentifier(this.keyEncAlgorithm))));
                cipher = this.helper.createCipher(this.keyEncAlgorithm.getId());
                cipher.init(1, generateSecret2, this.random);
                algorithmIdentifier = new AlgorithmIdentifier(this.algorithm, new PBES2Parameters(new KeyDerivationFunc(PKCSObjectIdentifiers.id_PBKDF2, new PBKDF2Params(salt, 1024)), new EncryptionScheme(this.keyEncAlgorithm, ASN1Primitive.fromByteArray(cipher.getParameters().getEncoded()))));
            }
            return new OutputEncryptor() {
                public AlgorithmIdentifier getAlgorithmIdentifier() {
                    return algorithmIdentifier;
                }
                
                public OutputStream getOutputStream(final OutputStream os) {
                    return new CipherOutputStream(os, cipher);
                }
                
                public GenericKey getKey() {
                    if (JcePKCSPBEOutputEncryptorBuilder.this.isPKCS12(algorithmIdentifier.getAlgorithm())) {
                        return new GenericKey(algorithmIdentifier, PBEParametersGenerator.PKCS5PasswordToBytes(array));
                    }
                    return new GenericKey(algorithmIdentifier, PBEParametersGenerator.PKCS12PasswordToBytes(array));
                }
            };
        }
        catch (Exception ex) {
            throw new OperatorCreationException("unable to create OutputEncryptor: " + ex.getMessage(), ex);
        }
    }
    
    private boolean isPKCS12(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        return asn1ObjectIdentifier.on(PKCSObjectIdentifiers.pkcs_12PbeIds) || asn1ObjectIdentifier.on(BCObjectIdentifiers.bc_pbe_sha1_pkcs12) || asn1ObjectIdentifier.on(BCObjectIdentifiers.bc_pbe_sha256_pkcs12);
    }
}
