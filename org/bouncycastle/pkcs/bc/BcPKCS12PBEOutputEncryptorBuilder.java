// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pkcs.bc;

import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.operator.GenericKey;
import org.bouncycastle.crypto.io.CipherOutputStream;
import java.io.OutputStream;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.pkcs.PKCS12PBEParams;
import org.bouncycastle.operator.OutputEncryptor;
import org.bouncycastle.crypto.paddings.BlockCipherPadding;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.paddings.PKCS7Padding;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.BlockCipher;
import java.security.SecureRandom;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.ExtendedDigest;

public class BcPKCS12PBEOutputEncryptorBuilder
{
    private ExtendedDigest digest;
    private BufferedBlockCipher engine;
    private ASN1ObjectIdentifier algorithm;
    private SecureRandom random;
    
    public BcPKCS12PBEOutputEncryptorBuilder(final ASN1ObjectIdentifier asn1ObjectIdentifier, final BlockCipher blockCipher) {
        this(asn1ObjectIdentifier, blockCipher, new SHA1Digest());
    }
    
    public BcPKCS12PBEOutputEncryptorBuilder(final ASN1ObjectIdentifier algorithm, final BlockCipher blockCipher, final ExtendedDigest digest) {
        this.algorithm = algorithm;
        this.engine = new PaddedBufferedBlockCipher(blockCipher, new PKCS7Padding());
        this.digest = digest;
    }
    
    public OutputEncryptor build(final char[] array) {
        if (this.random == null) {
            this.random = new SecureRandom();
        }
        final byte[] bytes = new byte[20];
        this.random.nextBytes(bytes);
        final PKCS12PBEParams pkcs12PBEParams = new PKCS12PBEParams(bytes, 1024);
        this.engine.init(true, PKCS12PBEUtils.createCipherParameters(this.algorithm, this.digest, this.engine.getBlockSize(), pkcs12PBEParams, array));
        return new OutputEncryptor() {
            public AlgorithmIdentifier getAlgorithmIdentifier() {
                return new AlgorithmIdentifier(BcPKCS12PBEOutputEncryptorBuilder.this.algorithm, pkcs12PBEParams);
            }
            
            public OutputStream getOutputStream(final OutputStream outputStream) {
                return new CipherOutputStream(outputStream, BcPKCS12PBEOutputEncryptorBuilder.this.engine);
            }
            
            public GenericKey getKey() {
                return new GenericKey(new AlgorithmIdentifier(BcPKCS12PBEOutputEncryptorBuilder.this.algorithm, pkcs12PBEParams), PBEParametersGenerator.PKCS12PasswordToBytes(array));
            }
        };
    }
}
