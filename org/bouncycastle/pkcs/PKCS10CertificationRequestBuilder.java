// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pkcs;

import java.io.OutputStream;
import java.util.Iterator;
import java.io.IOException;
import org.bouncycastle.asn1.pkcs.CertificationRequest;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.pkcs.CertificationRequestInfo;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.pkcs.Attribute;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.ArrayList;
import java.util.List;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;

public class PKCS10CertificationRequestBuilder
{
    private SubjectPublicKeyInfo publicKeyInfo;
    private X500Name subject;
    private List attributes;
    private boolean leaveOffEmpty;
    
    public PKCS10CertificationRequestBuilder(final X500Name subject, final SubjectPublicKeyInfo publicKeyInfo) {
        this.attributes = new ArrayList();
        this.leaveOffEmpty = false;
        this.subject = subject;
        this.publicKeyInfo = publicKeyInfo;
    }
    
    public PKCS10CertificationRequestBuilder addAttribute(final ASN1ObjectIdentifier asn1ObjectIdentifier, final ASN1Encodable asn1Encodable) {
        this.attributes.add(new Attribute(asn1ObjectIdentifier, new DERSet(asn1Encodable)));
        return this;
    }
    
    public PKCS10CertificationRequestBuilder addAttribute(final ASN1ObjectIdentifier asn1ObjectIdentifier, final ASN1Encodable[] array) {
        this.attributes.add(new Attribute(asn1ObjectIdentifier, new DERSet(array)));
        return this;
    }
    
    public PKCS10CertificationRequestBuilder setLeaveOffEmptyAttributes(final boolean leaveOffEmpty) {
        this.leaveOffEmpty = leaveOffEmpty;
        return this;
    }
    
    public PKCS10CertificationRequest build(final ContentSigner contentSigner) {
        CertificationRequestInfo certificationRequestInfo;
        if (this.attributes.isEmpty()) {
            if (this.leaveOffEmpty) {
                certificationRequestInfo = new CertificationRequestInfo(this.subject, this.publicKeyInfo, null);
            }
            else {
                certificationRequestInfo = new CertificationRequestInfo(this.subject, this.publicKeyInfo, new DERSet());
            }
        }
        else {
            final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
            final Iterator<Object> iterator = this.attributes.iterator();
            while (iterator.hasNext()) {
                asn1EncodableVector.add(Attribute.getInstance(iterator.next()));
            }
            certificationRequestInfo = new CertificationRequestInfo(this.subject, this.publicKeyInfo, new DERSet(asn1EncodableVector));
        }
        try {
            final OutputStream outputStream = contentSigner.getOutputStream();
            outputStream.write(certificationRequestInfo.getEncoded("DER"));
            outputStream.close();
            return new PKCS10CertificationRequest(new CertificationRequest(certificationRequestInfo, contentSigner.getAlgorithmIdentifier(), new DERBitString(contentSigner.getSignature())));
        }
        catch (IOException ex) {
            throw new IllegalStateException("cannot produce certification request signature");
        }
    }
}
