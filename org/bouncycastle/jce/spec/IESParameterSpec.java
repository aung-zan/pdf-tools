// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.spec;

import java.security.spec.AlgorithmParameterSpec;

public class IESParameterSpec implements AlgorithmParameterSpec
{
    private byte[] derivation;
    private byte[] encoding;
    private int macKeySize;
    private int cipherKeySize;
    
    public IESParameterSpec(final byte[] array, final byte[] array2, final int n) {
        this(array, array2, n, -1);
    }
    
    public IESParameterSpec(final byte[] array, final byte[] array2, final int macKeySize, final int cipherKeySize) {
        if (array != null) {
            System.arraycopy(array, 0, this.derivation = new byte[array.length], 0, array.length);
        }
        else {
            this.derivation = null;
        }
        if (array2 != null) {
            System.arraycopy(array2, 0, this.encoding = new byte[array2.length], 0, array2.length);
        }
        else {
            this.encoding = null;
        }
        this.macKeySize = macKeySize;
        this.cipherKeySize = cipherKeySize;
    }
    
    public byte[] getDerivationV() {
        return this.derivation;
    }
    
    public byte[] getEncodingV() {
        return this.encoding;
    }
    
    public int getMacKeySize() {
        return this.macKeySize;
    }
    
    public int getCipherKeySize() {
        return this.cipherKeySize;
    }
}
