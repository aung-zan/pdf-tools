// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.spec;

import javax.crypto.SecretKey;

public class RepeatedSecretKeySpec implements SecretKey
{
    private String algorithm;
    
    public RepeatedSecretKeySpec(final String algorithm) {
        this.algorithm = algorithm;
    }
    
    public String getAlgorithm() {
        return this.algorithm;
    }
    
    public String getFormat() {
        return null;
    }
    
    public byte[] getEncoded() {
        return null;
    }
}
