// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import org.bouncycastle.jcajce.provider.config.ProviderConfigurationPermission;
import javax.crypto.spec.DHParameterSpec;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.jce.spec.ECParameterSpec;
import java.security.Permission;
import org.bouncycastle.jcajce.provider.config.ProviderConfiguration;

class BouncyCastleProviderConfiguration implements ProviderConfiguration
{
    private static Permission BC_EC_LOCAL_PERMISSION;
    private static Permission BC_EC_PERMISSION;
    private static Permission BC_DH_LOCAL_PERMISSION;
    private static Permission BC_DH_PERMISSION;
    private ThreadLocal ecThreadSpec;
    private ThreadLocal dhThreadSpec;
    private volatile ECParameterSpec ecImplicitCaParams;
    private volatile Object dhDefaultParams;
    
    BouncyCastleProviderConfiguration() {
        this.ecThreadSpec = new ThreadLocal();
        this.dhThreadSpec = new ThreadLocal();
    }
    
    void setParameter(final String s, final Object o) {
        final SecurityManager securityManager = System.getSecurityManager();
        if (s.equals("threadLocalEcImplicitlyCa")) {
            if (securityManager != null) {
                securityManager.checkPermission(BouncyCastleProviderConfiguration.BC_EC_LOCAL_PERMISSION);
            }
            ECParameterSpec convertSpec;
            if (o instanceof ECParameterSpec || o == null) {
                convertSpec = (ECParameterSpec)o;
            }
            else {
                convertSpec = EC5Util.convertSpec((java.security.spec.ECParameterSpec)o, false);
            }
            if (convertSpec == null) {
                this.ecThreadSpec.remove();
            }
            else {
                this.ecThreadSpec.set(convertSpec);
            }
        }
        else if (s.equals("ecImplicitlyCa")) {
            if (securityManager != null) {
                securityManager.checkPermission(BouncyCastleProviderConfiguration.BC_EC_PERMISSION);
            }
            if (o instanceof ECParameterSpec || o == null) {
                this.ecImplicitCaParams = (ECParameterSpec)o;
            }
            else {
                this.ecImplicitCaParams = EC5Util.convertSpec((java.security.spec.ECParameterSpec)o, false);
            }
        }
        else if (s.equals("threadLocalDhDefaultParams")) {
            if (securityManager != null) {
                securityManager.checkPermission(BouncyCastleProviderConfiguration.BC_DH_LOCAL_PERMISSION);
            }
            if (!(o instanceof DHParameterSpec) && !(o instanceof DHParameterSpec[]) && o != null) {
                throw new IllegalArgumentException("not a valid DHParameterSpec");
            }
            if (o == null) {
                this.dhThreadSpec.remove();
            }
            else {
                this.dhThreadSpec.set(o);
            }
        }
        else if (s.equals("DhDefaultParams")) {
            if (securityManager != null) {
                securityManager.checkPermission(BouncyCastleProviderConfiguration.BC_DH_PERMISSION);
            }
            if (!(o instanceof DHParameterSpec) && !(o instanceof DHParameterSpec[]) && o != null) {
                throw new IllegalArgumentException("not a valid DHParameterSpec or DHParameterSpec[]");
            }
            this.dhDefaultParams = o;
        }
    }
    
    public ECParameterSpec getEcImplicitlyCa() {
        final ECParameterSpec ecParameterSpec = this.ecThreadSpec.get();
        if (ecParameterSpec != null) {
            return ecParameterSpec;
        }
        return this.ecImplicitCaParams;
    }
    
    public DHParameterSpec getDHDefaultParameters(final int n) {
        Object o = this.dhThreadSpec.get();
        if (o == null) {
            o = this.dhDefaultParams;
        }
        if (o instanceof DHParameterSpec) {
            final DHParameterSpec dhParameterSpec = (DHParameterSpec)o;
            if (dhParameterSpec.getP().bitLength() == n) {
                return dhParameterSpec;
            }
        }
        else if (o instanceof DHParameterSpec[]) {
            final DHParameterSpec[] array = (DHParameterSpec[])o;
            for (int i = 0; i != array.length; ++i) {
                if (array[i].getP().bitLength() == n) {
                    return array[i];
                }
            }
        }
        return null;
    }
    
    static {
        BouncyCastleProviderConfiguration.BC_EC_LOCAL_PERMISSION = new ProviderConfigurationPermission("BC", "threadLocalEcImplicitlyCa");
        BouncyCastleProviderConfiguration.BC_EC_PERMISSION = new ProviderConfigurationPermission("BC", "ecImplicitlyCa");
        BouncyCastleProviderConfiguration.BC_DH_LOCAL_PERMISSION = new ProviderConfigurationPermission("BC", "threadLocalDhDefaultParams");
        BouncyCastleProviderConfiguration.BC_DH_PERMISSION = new ProviderConfigurationPermission("BC", "DhDefaultParams");
    }
}
