// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import org.bouncycastle.crypto.engines.TwofishEngine;
import org.bouncycastle.crypto.engines.SkipjackEngine;
import org.bouncycastle.crypto.engines.DESedeEngine;
import org.bouncycastle.crypto.engines.DESEngine;
import org.bouncycastle.crypto.modes.OFBBlockCipher;
import org.bouncycastle.crypto.modes.CFBBlockCipher;
import org.bouncycastle.crypto.engines.BlowfishEngine;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.security.KeyFactory;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.BadPaddingException;
import org.bouncycastle.crypto.DataLengthException;
import javax.crypto.ShortBufferException;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import java.security.InvalidAlgorithmParameterException;
import org.bouncycastle.jcajce.provider.symmetric.util.BCPBEKey;
import java.security.InvalidKeyException;
import javax.crypto.SecretKey;
import java.security.SecureRandom;
import javax.crypto.NoSuchPaddingException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Key;
import org.bouncycastle.crypto.StreamBlockCipher;
import org.bouncycastle.crypto.BlockCipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.RC5ParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import java.security.AlgorithmParameters;
import javax.crypto.spec.PBEParameterSpec;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.StreamCipher;
import org.bouncycastle.jcajce.provider.symmetric.util.PBE;
import javax.crypto.CipherSpi;

public class JCEStreamCipher extends CipherSpi implements PBE
{
    private Class[] availableSpecs;
    private StreamCipher cipher;
    private ParametersWithIV ivParam;
    private int ivLength;
    private PBEParameterSpec pbeSpec;
    private String pbeAlgorithm;
    private AlgorithmParameters engineParams;
    
    protected JCEStreamCipher(final StreamCipher cipher, final int ivLength) {
        this.availableSpecs = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, IvParameterSpec.class, PBEParameterSpec.class };
        this.ivLength = 0;
        this.pbeSpec = null;
        this.pbeAlgorithm = null;
        this.cipher = cipher;
        this.ivLength = ivLength;
    }
    
    protected JCEStreamCipher(final BlockCipher blockCipher, final int ivLength) {
        this.availableSpecs = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, IvParameterSpec.class, PBEParameterSpec.class };
        this.ivLength = 0;
        this.pbeSpec = null;
        this.pbeAlgorithm = null;
        this.ivLength = ivLength;
        this.cipher = new StreamBlockCipher(blockCipher);
    }
    
    @Override
    protected int engineGetBlockSize() {
        return 0;
    }
    
    @Override
    protected byte[] engineGetIV() {
        return (byte[])((this.ivParam != null) ? this.ivParam.getIV() : null);
    }
    
    @Override
    protected int engineGetKeySize(final Key key) {
        return key.getEncoded().length * 8;
    }
    
    @Override
    protected int engineGetOutputSize(final int n) {
        return n;
    }
    
    @Override
    protected AlgorithmParameters engineGetParameters() {
        if (this.engineParams == null && this.pbeSpec != null) {
            try {
                final AlgorithmParameters instance = AlgorithmParameters.getInstance(this.pbeAlgorithm, "BC");
                instance.init(this.pbeSpec);
                return instance;
            }
            catch (Exception ex) {
                return null;
            }
        }
        return this.engineParams;
    }
    
    @Override
    protected void engineSetMode(final String str) {
        if (!str.equalsIgnoreCase("ECB")) {
            throw new IllegalArgumentException("can't support mode " + str);
        }
    }
    
    @Override
    protected void engineSetPadding(final String str) throws NoSuchPaddingException {
        if (!str.equalsIgnoreCase("NoPadding")) {
            throw new NoSuchPaddingException("Padding " + str + " unknown.");
        }
    }
    
    @Override
    protected void engineInit(final int n, final Key key, final AlgorithmParameterSpec algorithmParameterSpec, final SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        this.pbeSpec = null;
        this.pbeAlgorithm = null;
        this.engineParams = null;
        if (!(key instanceof SecretKey)) {
            throw new InvalidKeyException("Key for algorithm " + key.getAlgorithm() + " not suitable for symmetric enryption.");
        }
        CipherParameters cipherParameters;
        if (key instanceof BCPBEKey) {
            final BCPBEKey bcpbeKey = (BCPBEKey)key;
            if (bcpbeKey.getOID() != null) {
                this.pbeAlgorithm = bcpbeKey.getOID().getId();
            }
            else {
                this.pbeAlgorithm = bcpbeKey.getAlgorithm();
            }
            if (bcpbeKey.getParam() != null) {
                cipherParameters = bcpbeKey.getParam();
                this.pbeSpec = new PBEParameterSpec(bcpbeKey.getSalt(), bcpbeKey.getIterationCount());
            }
            else {
                if (!(algorithmParameterSpec instanceof PBEParameterSpec)) {
                    throw new InvalidAlgorithmParameterException("PBE requires PBE parameters to be set.");
                }
                cipherParameters = Util.makePBEParameters(bcpbeKey, algorithmParameterSpec, this.cipher.getAlgorithmName());
                this.pbeSpec = (PBEParameterSpec)algorithmParameterSpec;
            }
            if (bcpbeKey.getIvSize() != 0) {
                this.ivParam = (ParametersWithIV)cipherParameters;
            }
        }
        else if (algorithmParameterSpec == null) {
            cipherParameters = new KeyParameter(key.getEncoded());
        }
        else {
            if (!(algorithmParameterSpec instanceof IvParameterSpec)) {
                throw new IllegalArgumentException("unknown parameter type.");
            }
            cipherParameters = new ParametersWithIV(new KeyParameter(key.getEncoded()), ((IvParameterSpec)algorithmParameterSpec).getIV());
            this.ivParam = (ParametersWithIV)cipherParameters;
        }
        if (this.ivLength != 0 && !(cipherParameters instanceof ParametersWithIV)) {
            SecureRandom secureRandom2 = secureRandom;
            if (secureRandom2 == null) {
                secureRandom2 = new SecureRandom();
            }
            if (n != 1 && n != 3) {
                throw new InvalidAlgorithmParameterException("no IV set when one expected");
            }
            final byte[] bytes = new byte[this.ivLength];
            secureRandom2.nextBytes(bytes);
            cipherParameters = new ParametersWithIV(cipherParameters, bytes);
            this.ivParam = (ParametersWithIV)cipherParameters;
        }
        switch (n) {
            case 1:
            case 3: {
                this.cipher.init(true, cipherParameters);
                break;
            }
            case 2:
            case 4: {
                this.cipher.init(false, cipherParameters);
                break;
            }
            default: {
                System.out.println("eeek!");
                break;
            }
        }
    }
    
    @Override
    protected void engineInit(final int n, final Key key, final AlgorithmParameters engineParams, final SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        AlgorithmParameterSpec parameterSpec = null;
        if (engineParams != null) {
            int i = 0;
            while (i != this.availableSpecs.length) {
                try {
                    parameterSpec = engineParams.getParameterSpec((Class<AlgorithmParameterSpec>)this.availableSpecs[i]);
                }
                catch (Exception ex) {
                    ++i;
                    continue;
                }
                break;
            }
            if (parameterSpec == null) {
                throw new InvalidAlgorithmParameterException("can't handle parameter " + engineParams.toString());
            }
        }
        this.engineInit(n, key, parameterSpec, secureRandom);
        this.engineParams = engineParams;
    }
    
    @Override
    protected void engineInit(final int n, final Key key, final SecureRandom secureRandom) throws InvalidKeyException {
        try {
            this.engineInit(n, key, (AlgorithmParameterSpec)null, secureRandom);
        }
        catch (InvalidAlgorithmParameterException ex) {
            throw new InvalidKeyException(ex.getMessage());
        }
    }
    
    @Override
    protected byte[] engineUpdate(final byte[] array, final int n, final int n2) {
        final byte[] array2 = new byte[n2];
        this.cipher.processBytes(array, n, n2, array2, 0);
        return array2;
    }
    
    @Override
    protected int engineUpdate(final byte[] array, final int n, final int n2, final byte[] array2, final int n3) throws ShortBufferException {
        try {
            this.cipher.processBytes(array, n, n2, array2, n3);
            return n2;
        }
        catch (DataLengthException ex) {
            throw new ShortBufferException(ex.getMessage());
        }
    }
    
    @Override
    protected byte[] engineDoFinal(final byte[] array, final int n, final int n2) throws BadPaddingException, IllegalBlockSizeException {
        if (n2 != 0) {
            final byte[] engineUpdate = this.engineUpdate(array, n, n2);
            this.cipher.reset();
            return engineUpdate;
        }
        this.cipher.reset();
        return new byte[0];
    }
    
    @Override
    protected int engineDoFinal(final byte[] array, final int n, final int n2, final byte[] array2, final int n3) throws BadPaddingException {
        if (n2 != 0) {
            this.cipher.processBytes(array, n, n2, array2, n3);
        }
        this.cipher.reset();
        return n2;
    }
    
    @Override
    protected byte[] engineWrap(final Key key) throws IllegalBlockSizeException, InvalidKeyException {
        final byte[] encoded = key.getEncoded();
        if (encoded == null) {
            throw new InvalidKeyException("Cannot wrap key, null encoding.");
        }
        try {
            return this.engineDoFinal(encoded, 0, encoded.length);
        }
        catch (BadPaddingException ex) {
            throw new IllegalBlockSizeException(ex.getMessage());
        }
    }
    
    @Override
    protected Key engineUnwrap(final byte[] array, final String s, final int i) throws InvalidKeyException {
        byte[] engineDoFinal;
        try {
            engineDoFinal = this.engineDoFinal(array, 0, array.length);
        }
        catch (BadPaddingException ex) {
            throw new InvalidKeyException(ex.getMessage());
        }
        catch (IllegalBlockSizeException ex2) {
            throw new InvalidKeyException(ex2.getMessage());
        }
        if (i == 3) {
            return new SecretKeySpec(engineDoFinal, s);
        }
        if (s.equals("") && i == 2) {
            try {
                final PrivateKeyInfo instance = PrivateKeyInfo.getInstance(engineDoFinal);
                final PrivateKey privateKey = BouncyCastleProvider.getPrivateKey(instance);
                if (privateKey != null) {
                    return privateKey;
                }
                throw new InvalidKeyException("algorithm " + instance.getPrivateKeyAlgorithm().getAlgorithm() + " not supported");
            }
            catch (Exception ex6) {
                throw new InvalidKeyException("Invalid key encoding.");
            }
        }
        try {
            final KeyFactory instance2 = KeyFactory.getInstance(s, "BC");
            if (i == 1) {
                return instance2.generatePublic(new X509EncodedKeySpec(engineDoFinal));
            }
            if (i == 2) {
                return instance2.generatePrivate(new PKCS8EncodedKeySpec(engineDoFinal));
            }
        }
        catch (NoSuchProviderException ex3) {
            throw new InvalidKeyException("Unknown key type " + ex3.getMessage());
        }
        catch (NoSuchAlgorithmException ex4) {
            throw new InvalidKeyException("Unknown key type " + ex4.getMessage());
        }
        catch (InvalidKeySpecException ex5) {
            throw new InvalidKeyException("Unknown key type " + ex5.getMessage());
        }
        throw new InvalidKeyException("Unknown key type " + i);
    }
    
    public static class Blowfish_CFB8 extends JCEStreamCipher
    {
        public Blowfish_CFB8() {
            super(new CFBBlockCipher(new BlowfishEngine(), 8), 64);
        }
    }
    
    public static class Blowfish_OFB8 extends JCEStreamCipher
    {
        public Blowfish_OFB8() {
            super(new OFBBlockCipher(new BlowfishEngine(), 8), 64);
        }
    }
    
    public static class DES_CFB8 extends JCEStreamCipher
    {
        public DES_CFB8() {
            super(new CFBBlockCipher(new DESEngine(), 8), 64);
        }
    }
    
    public static class DES_OFB8 extends JCEStreamCipher
    {
        public DES_OFB8() {
            super(new OFBBlockCipher(new DESEngine(), 8), 64);
        }
    }
    
    public static class DESede_CFB8 extends JCEStreamCipher
    {
        public DESede_CFB8() {
            super(new CFBBlockCipher(new DESedeEngine(), 8), 64);
        }
    }
    
    public static class DESede_OFB8 extends JCEStreamCipher
    {
        public DESede_OFB8() {
            super(new OFBBlockCipher(new DESedeEngine(), 8), 64);
        }
    }
    
    public static class Skipjack_CFB8 extends JCEStreamCipher
    {
        public Skipjack_CFB8() {
            super(new CFBBlockCipher(new SkipjackEngine(), 8), 64);
        }
    }
    
    public static class Skipjack_OFB8 extends JCEStreamCipher
    {
        public Skipjack_OFB8() {
            super(new OFBBlockCipher(new SkipjackEngine(), 8), 64);
        }
    }
    
    public static class Twofish_CFB8 extends JCEStreamCipher
    {
        public Twofish_CFB8() {
            super(new CFBBlockCipher(new TwofishEngine(), 8), 128);
        }
    }
    
    public static class Twofish_OFB8 extends JCEStreamCipher
    {
        public Twofish_OFB8() {
            super(new OFBBlockCipher(new TwofishEngine(), 8), 128);
        }
    }
}
