// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import java.security.cert.CertStoreException;
import java.security.cert.CRLSelector;
import java.security.cert.CertStore;
import org.bouncycastle.util.StoreException;
import org.bouncycastle.util.Selector;
import org.bouncycastle.x509.X509Store;
import java.security.cert.PKIXParameters;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.List;
import java.util.Collection;
import java.security.cert.X509CRL;
import java.util.HashSet;
import java.util.Set;
import java.util.Date;
import org.bouncycastle.x509.ExtendedPKIXParameters;
import org.bouncycastle.x509.X509CRLStoreSelector;

public class PKIXCRLUtil
{
    public Set findCRLs(final X509CRLStoreSelector x509CRLStoreSelector, final ExtendedPKIXParameters extendedPKIXParameters, final Date date) throws AnnotatedException {
        final HashSet<X509CRL> set = new HashSet<X509CRL>();
        try {
            set.addAll((Collection<?>)this.findCRLs(x509CRLStoreSelector, extendedPKIXParameters.getAdditionalStores()));
            set.addAll((Collection<?>)this.findCRLs(x509CRLStoreSelector, extendedPKIXParameters.getStores()));
            set.addAll((Collection<?>)this.findCRLs(x509CRLStoreSelector, extendedPKIXParameters.getCertStores()));
        }
        catch (AnnotatedException ex) {
            throw new AnnotatedException("Exception obtaining complete CRLs.", ex);
        }
        final HashSet<X509CRL> set2 = new HashSet<X509CRL>();
        Date date2 = date;
        if (extendedPKIXParameters.getDate() != null) {
            date2 = extendedPKIXParameters.getDate();
        }
        for (final X509CRL x509CRL : set) {
            if (x509CRL.getNextUpdate().after(date2)) {
                final X509Certificate certificateChecking = x509CRLStoreSelector.getCertificateChecking();
                if (certificateChecking != null) {
                    if (!x509CRL.getThisUpdate().before(certificateChecking.getNotAfter())) {
                        continue;
                    }
                    set2.add(x509CRL);
                }
                else {
                    set2.add(x509CRL);
                }
            }
        }
        return set2;
    }
    
    public Set findCRLs(final X509CRLStoreSelector x509CRLStoreSelector, final PKIXParameters pkixParameters) throws AnnotatedException {
        final HashSet set = new HashSet();
        try {
            set.addAll(this.findCRLs(x509CRLStoreSelector, pkixParameters.getCertStores()));
        }
        catch (AnnotatedException ex) {
            throw new AnnotatedException("Exception obtaining complete CRLs.", ex);
        }
        return set;
    }
    
    private final Collection findCRLs(final X509CRLStoreSelector selector, final List list) throws AnnotatedException {
        final HashSet set = new HashSet();
        final Iterator<X509Store> iterator = list.iterator();
        Object o = null;
        boolean b = false;
        while (iterator.hasNext()) {
            final X509Store next = iterator.next();
            if (next instanceof X509Store) {
                final X509Store x509Store = next;
                try {
                    set.addAll(x509Store.getMatches(selector));
                    b = true;
                }
                catch (StoreException ex) {
                    o = new AnnotatedException("Exception searching in X.509 CRL store.", ex);
                }
            }
            else {
                final CertStore certStore = (CertStore)next;
                try {
                    set.addAll(certStore.getCRLs(selector));
                    b = true;
                }
                catch (CertStoreException ex2) {
                    o = new AnnotatedException("Exception searching in X.509 CRL store.", ex2);
                }
            }
        }
        if (!b && o != null) {
            throw o;
        }
        return set;
    }
}
