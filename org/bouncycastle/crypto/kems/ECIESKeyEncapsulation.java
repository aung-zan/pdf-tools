// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.kems;

import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.DerivationParameters;
import org.bouncycastle.crypto.params.KDFParameters;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ECKeyParameters;
import java.security.SecureRandom;
import org.bouncycastle.crypto.DerivationFunction;
import java.math.BigInteger;
import org.bouncycastle.crypto.KeyEncapsulation;

public class ECIESKeyEncapsulation implements KeyEncapsulation
{
    private static final BigInteger ONE;
    private DerivationFunction kdf;
    private SecureRandom rnd;
    private ECKeyParameters key;
    private boolean CofactorMode;
    private boolean OldCofactorMode;
    private boolean SingleHashMode;
    
    public ECIESKeyEncapsulation(final DerivationFunction kdf, final SecureRandom rnd) {
        this.kdf = kdf;
        this.rnd = rnd;
        this.CofactorMode = false;
        this.OldCofactorMode = false;
        this.SingleHashMode = false;
    }
    
    public ECIESKeyEncapsulation(final DerivationFunction kdf, final SecureRandom rnd, final boolean cofactorMode, final boolean oldCofactorMode, final boolean singleHashMode) {
        this.kdf = kdf;
        this.rnd = rnd;
        this.CofactorMode = cofactorMode;
        this.OldCofactorMode = oldCofactorMode;
        this.SingleHashMode = singleHashMode;
    }
    
    public void init(final CipherParameters cipherParameters) throws IllegalArgumentException {
        if (!(cipherParameters instanceof ECKeyParameters)) {
            throw new IllegalArgumentException("EC key required");
        }
        this.key = (ECKeyParameters)cipherParameters;
    }
    
    public CipherParameters encrypt(final byte[] array, final int n, final int n2) throws IllegalArgumentException {
        if (!(this.key instanceof ECPublicKeyParameters)) {
            throw new IllegalArgumentException("Public key required for encryption");
        }
        final BigInteger n3 = this.key.getParameters().getN();
        final BigInteger h = this.key.getParameters().getH();
        final BigInteger randomInRange = BigIntegers.createRandomInRange(ECIESKeyEncapsulation.ONE, n3, this.rnd);
        final byte[] encoded = this.key.getParameters().getG().multiply(randomInRange).getEncoded();
        System.arraycopy(encoded, 0, array, n, encoded.length);
        BigInteger mod;
        if (this.CofactorMode) {
            mod = randomInRange.multiply(h).mod(n3);
        }
        else {
            mod = randomInRange;
        }
        final byte[] unsignedByteArray = BigIntegers.asUnsignedByteArray((this.key.getParameters().getCurve().getFieldSize() + 7) / 8, ((ECPublicKeyParameters)this.key).getQ().multiply(mod).getX().toBigInteger());
        byte[] array2;
        if (this.SingleHashMode) {
            array2 = new byte[encoded.length + unsignedByteArray.length];
            System.arraycopy(encoded, 0, array2, 0, encoded.length);
            System.arraycopy(unsignedByteArray, 0, array2, encoded.length, unsignedByteArray.length);
        }
        else {
            array2 = unsignedByteArray;
        }
        this.kdf.init(new KDFParameters(array2, null));
        final byte[] array3 = new byte[n2];
        this.kdf.generateBytes(array3, 0, array3.length);
        return new KeyParameter(array3);
    }
    
    public CipherParameters encrypt(final byte[] array, final int n) {
        return this.encrypt(array, 0, n);
    }
    
    public CipherParameters decrypt(final byte[] array, final int n, final int n2, final int n3) throws IllegalArgumentException {
        if (!(this.key instanceof ECPrivateKeyParameters)) {
            throw new IllegalArgumentException("Private key required for encryption");
        }
        final BigInteger n4 = this.key.getParameters().getN();
        final BigInteger h = this.key.getParameters().getH();
        final byte[] array2 = new byte[n2];
        System.arraycopy(array, n, array2, 0, n2);
        final ECPoint decodePoint = this.key.getParameters().getCurve().decodePoint(array2);
        ECPoint multiply;
        if (this.CofactorMode || this.OldCofactorMode) {
            multiply = decodePoint.multiply(h);
        }
        else {
            multiply = decodePoint;
        }
        BigInteger bigInteger;
        if (this.CofactorMode) {
            bigInteger = ((ECPrivateKeyParameters)this.key).getD().multiply(h.modInverse(n4)).mod(n4);
        }
        else {
            bigInteger = ((ECPrivateKeyParameters)this.key).getD();
        }
        final byte[] unsignedByteArray = BigIntegers.asUnsignedByteArray((this.key.getParameters().getCurve().getFieldSize() + 7) / 8, multiply.multiply(bigInteger).getX().toBigInteger());
        byte[] array3;
        if (this.SingleHashMode) {
            array3 = new byte[array2.length + unsignedByteArray.length];
            System.arraycopy(array2, 0, array3, 0, array2.length);
            System.arraycopy(unsignedByteArray, 0, array3, array2.length, unsignedByteArray.length);
        }
        else {
            array3 = unsignedByteArray;
        }
        this.kdf.init(new KDFParameters(array3, null));
        final byte[] array4 = new byte[n3];
        this.kdf.generateBytes(array4, 0, array4.length);
        return new KeyParameter(array4);
    }
    
    public CipherParameters decrypt(final byte[] array, final int n) {
        return this.decrypt(array, 0, array.length, n);
    }
    
    static {
        ONE = BigInteger.valueOf(1L);
    }
}
