// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.macs;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.util.Pack;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.Mac;

public class SipHash implements Mac
{
    protected final int c;
    protected final int d;
    protected long k0;
    protected long k1;
    protected long v0;
    protected long v1;
    protected long v2;
    protected long v3;
    protected long v4;
    protected byte[] buf;
    protected int bufPos;
    protected int wordCount;
    
    public SipHash() {
        this.buf = new byte[8];
        this.bufPos = 0;
        this.wordCount = 0;
        this.c = 2;
        this.d = 4;
    }
    
    public SipHash(final int c, final int d) {
        this.buf = new byte[8];
        this.bufPos = 0;
        this.wordCount = 0;
        this.c = c;
        this.d = d;
    }
    
    public String getAlgorithmName() {
        return "SipHash-" + this.c + "-" + this.d;
    }
    
    public int getMacSize() {
        return 8;
    }
    
    public void init(final CipherParameters cipherParameters) throws IllegalArgumentException {
        if (!(cipherParameters instanceof KeyParameter)) {
            throw new IllegalArgumentException("'params' must be an instance of KeyParameter");
        }
        final byte[] key = ((KeyParameter)cipherParameters).getKey();
        if (key.length != 16) {
            throw new IllegalArgumentException("'params' must be a 128-bit key");
        }
        this.k0 = Pack.littleEndianToLong(key, 0);
        this.k1 = Pack.littleEndianToLong(key, 8);
        this.reset();
    }
    
    public void update(final byte b) throws IllegalStateException {
        this.buf[this.bufPos] = b;
        if (++this.bufPos == this.buf.length) {
            this.processMessageWord();
            this.bufPos = 0;
        }
    }
    
    public void update(final byte[] array, final int n, final int n2) throws DataLengthException, IllegalStateException {
        for (int i = 0; i < n2; ++i) {
            this.buf[this.bufPos] = array[n + i];
            if (++this.bufPos == this.buf.length) {
                this.processMessageWord();
                this.bufPos = 0;
            }
        }
    }
    
    public long doFinal() throws DataLengthException, IllegalStateException {
        this.buf[7] = (byte)((this.wordCount << 3) + this.bufPos & 0xFF);
        while (this.bufPos < 7) {
            this.buf[this.bufPos++] = 0;
        }
        this.processMessageWord();
        this.v2 ^= 0xFFL;
        this.applySipRounds(this.d);
        final long n = this.v0 ^ this.v1 ^ this.v2 ^ this.v3;
        this.reset();
        return n;
    }
    
    public int doFinal(final byte[] array, final int n) throws DataLengthException, IllegalStateException {
        Pack.longToLittleEndian(this.doFinal(), array, n);
        return 8;
    }
    
    public void reset() {
        this.v0 = (this.k0 ^ 0x736F6D6570736575L);
        this.v1 = (this.k1 ^ 0x646F72616E646F6DL);
        this.v2 = (this.k0 ^ 0x6C7967656E657261L);
        this.v3 = (this.k1 ^ 0x7465646279746573L);
        Arrays.fill(this.buf, (byte)0);
        this.bufPos = 0;
        this.wordCount = 0;
    }
    
    protected void processMessageWord() {
        ++this.wordCount;
        final long littleEndianToLong = Pack.littleEndianToLong(this.buf, 0);
        this.v3 ^= littleEndianToLong;
        this.applySipRounds(this.c);
        this.v0 ^= littleEndianToLong;
    }
    
    protected void applySipRounds(final int n) {
        for (int i = 0; i < n; ++i) {
            this.v0 += this.v1;
            this.v2 += this.v3;
            this.v1 = rotateLeft(this.v1, 13);
            this.v3 = rotateLeft(this.v3, 16);
            this.v1 ^= this.v0;
            this.v3 ^= this.v2;
            this.v0 = rotateLeft(this.v0, 32);
            this.v2 += this.v1;
            this.v0 += this.v3;
            this.v1 = rotateLeft(this.v1, 17);
            this.v3 = rotateLeft(this.v3, 21);
            this.v1 ^= this.v2;
            this.v3 ^= this.v0;
            this.v2 = rotateLeft(this.v2, 32);
        }
    }
    
    protected static long rotateLeft(final long n, final int n2) {
        return n << n2 | n >>> 64 - n2;
    }
}
