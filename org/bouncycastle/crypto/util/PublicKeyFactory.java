// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.util;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x9.DHValidationParms;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x9.X9ECPoint;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.asn1.teletrust.TeleTrusTNamedCurves;
import org.bouncycastle.asn1.nist.NISTNamedCurves;
import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X962NamedCurves;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x9.X962Parameters;
import org.bouncycastle.crypto.params.DSAPublicKeyParameters;
import org.bouncycastle.crypto.params.DSAParameters;
import org.bouncycastle.asn1.x509.DSAParameter;
import org.bouncycastle.crypto.params.ElGamalPublicKeyParameters;
import org.bouncycastle.crypto.params.ElGamalParameters;
import org.bouncycastle.asn1.oiw.ElGamalParameter;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.pkcs.DHParameter;
import org.bouncycastle.crypto.params.DHPublicKeyParameters;
import org.bouncycastle.crypto.params.DHParameters;
import org.bouncycastle.crypto.params.DHValidationParameters;
import org.bouncycastle.asn1.x9.DHDomainParameters;
import org.bouncycastle.asn1.x9.DHPublicKey;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.asn1.pkcs.RSAPublicKey;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.InputStream;
import java.io.IOException;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class PublicKeyFactory
{
    public static AsymmetricKeyParameter createKey(final byte[] array) throws IOException {
        return createKey(SubjectPublicKeyInfo.getInstance(ASN1Primitive.fromByteArray(array)));
    }
    
    public static AsymmetricKeyParameter createKey(final InputStream inputStream) throws IOException {
        return createKey(SubjectPublicKeyInfo.getInstance(new ASN1InputStream(inputStream).readObject()));
    }
    
    public static AsymmetricKeyParameter createKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        final AlgorithmIdentifier algorithm = subjectPublicKeyInfo.getAlgorithm();
        if (algorithm.getAlgorithm().equals(PKCSObjectIdentifiers.rsaEncryption) || algorithm.getAlgorithm().equals(X509ObjectIdentifiers.id_ea_rsa)) {
            final RSAPublicKey instance = RSAPublicKey.getInstance(subjectPublicKeyInfo.parsePublicKey());
            return new RSAKeyParameters(false, instance.getModulus(), instance.getPublicExponent());
        }
        if (algorithm.getAlgorithm().equals(X9ObjectIdentifiers.dhpublicnumber)) {
            final BigInteger value = DHPublicKey.getInstance(subjectPublicKeyInfo.parsePublicKey()).getY().getValue();
            final DHDomainParameters instance2 = DHDomainParameters.getInstance(algorithm.getParameters());
            final BigInteger value2 = instance2.getP().getValue();
            final BigInteger value3 = instance2.getG().getValue();
            final BigInteger value4 = instance2.getQ().getValue();
            BigInteger value5 = null;
            if (instance2.getJ() != null) {
                value5 = instance2.getJ().getValue();
            }
            DHValidationParameters dhValidationParameters = null;
            final DHValidationParms validationParms = instance2.getValidationParms();
            if (validationParms != null) {
                dhValidationParameters = new DHValidationParameters(validationParms.getSeed().getBytes(), validationParms.getPgenCounter().getValue().intValue());
            }
            return new DHPublicKeyParameters(value, new DHParameters(value2, value3, value4, value5, dhValidationParameters));
        }
        if (algorithm.getAlgorithm().equals(PKCSObjectIdentifiers.dhKeyAgreement)) {
            final DHParameter instance3 = DHParameter.getInstance(algorithm.getParameters());
            final ASN1Integer asn1Integer = (ASN1Integer)subjectPublicKeyInfo.parsePublicKey();
            final BigInteger l = instance3.getL();
            return new DHPublicKeyParameters(asn1Integer.getValue(), new DHParameters(instance3.getP(), instance3.getG(), null, (l == null) ? 0 : l.intValue()));
        }
        if (algorithm.getAlgorithm().equals(OIWObjectIdentifiers.elGamalAlgorithm)) {
            final ElGamalParameter elGamalParameter = new ElGamalParameter((ASN1Sequence)algorithm.getParameters());
            return new ElGamalPublicKeyParameters(((ASN1Integer)subjectPublicKeyInfo.parsePublicKey()).getValue(), new ElGamalParameters(elGamalParameter.getP(), elGamalParameter.getG()));
        }
        if (algorithm.getAlgorithm().equals(X9ObjectIdentifiers.id_dsa) || algorithm.getAlgorithm().equals(OIWObjectIdentifiers.dsaWithSHA1)) {
            final ASN1Integer asn1Integer2 = (ASN1Integer)subjectPublicKeyInfo.parsePublicKey();
            final ASN1Encodable parameters = algorithm.getParameters();
            DSAParameters dsaParameters = null;
            if (parameters != null) {
                final DSAParameter instance4 = DSAParameter.getInstance(parameters.toASN1Primitive());
                dsaParameters = new DSAParameters(instance4.getP(), instance4.getQ(), instance4.getG());
            }
            return new DSAPublicKeyParameters(asn1Integer2.getValue(), dsaParameters);
        }
        if (algorithm.getAlgorithm().equals(X9ObjectIdentifiers.id_ecPublicKey)) {
            final X962Parameters x962Parameters = new X962Parameters((ASN1Primitive)algorithm.getParameters());
            X9ECParameters x9ECParameters;
            if (x962Parameters.isNamedCurve()) {
                final ASN1ObjectIdentifier asn1ObjectIdentifier = (ASN1ObjectIdentifier)x962Parameters.getParameters();
                x9ECParameters = X962NamedCurves.getByOID(asn1ObjectIdentifier);
                if (x9ECParameters == null) {
                    x9ECParameters = SECNamedCurves.getByOID(asn1ObjectIdentifier);
                    if (x9ECParameters == null) {
                        x9ECParameters = NISTNamedCurves.getByOID(asn1ObjectIdentifier);
                        if (x9ECParameters == null) {
                            x9ECParameters = TeleTrusTNamedCurves.getByOID(asn1ObjectIdentifier);
                        }
                    }
                }
            }
            else {
                x9ECParameters = X9ECParameters.getInstance(x962Parameters.getParameters());
            }
            return new ECPublicKeyParameters(new X9ECPoint(x9ECParameters.getCurve(), new DEROctetString(subjectPublicKeyInfo.getPublicKeyData().getBytes())).getPoint(), new ECDomainParameters(x9ECParameters.getCurve(), x9ECParameters.getG(), x9ECParameters.getN(), x9ECParameters.getH(), x9ECParameters.getSeed()));
        }
        throw new RuntimeException("algorithm identifier in key not recognised");
    }
}
