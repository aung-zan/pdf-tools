// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.util;

import org.bouncycastle.crypto.params.DSAParameters;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.x509.DSAParameter;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.crypto.params.DSAPrivateKeyParameters;
import org.bouncycastle.asn1.pkcs.RSAPrivateKey;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class PrivateKeyInfoFactory
{
    public static PrivateKeyInfo createPrivateKeyInfo(final AsymmetricKeyParameter asymmetricKeyParameter) throws IOException {
        if (asymmetricKeyParameter instanceof RSAKeyParameters) {
            final RSAPrivateCrtKeyParameters rsaPrivateCrtKeyParameters = (RSAPrivateCrtKeyParameters)asymmetricKeyParameter;
            return new PrivateKeyInfo(new AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, DERNull.INSTANCE), new RSAPrivateKey(rsaPrivateCrtKeyParameters.getModulus(), rsaPrivateCrtKeyParameters.getPublicExponent(), rsaPrivateCrtKeyParameters.getExponent(), rsaPrivateCrtKeyParameters.getP(), rsaPrivateCrtKeyParameters.getQ(), rsaPrivateCrtKeyParameters.getDP(), rsaPrivateCrtKeyParameters.getDQ(), rsaPrivateCrtKeyParameters.getQInv()));
        }
        if (asymmetricKeyParameter instanceof DSAPrivateKeyParameters) {
            final DSAPrivateKeyParameters dsaPrivateKeyParameters = (DSAPrivateKeyParameters)asymmetricKeyParameter;
            final DSAParameters parameters = dsaPrivateKeyParameters.getParameters();
            return new PrivateKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_dsa, new DSAParameter(parameters.getP(), parameters.getQ(), parameters.getG())), new ASN1Integer(dsaPrivateKeyParameters.getX()));
        }
        throw new IOException("key parameters not recognised.");
    }
}
