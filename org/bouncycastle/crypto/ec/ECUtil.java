// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.ec;

import org.bouncycastle.math.ec.ECConstants;
import java.util.Random;
import java.security.SecureRandom;
import java.math.BigInteger;

class ECUtil
{
    static BigInteger generateK(final BigInteger val, final SecureRandom secureRandom) {
        int bitLength;
        BigInteger bigInteger;
        for (bitLength = val.bitLength(), bigInteger = new BigInteger(bitLength, secureRandom); bigInteger.equals(ECConstants.ZERO) || bigInteger.compareTo(val) >= 0; bigInteger = new BigInteger(bitLength, secureRandom)) {}
        return bigInteger;
    }
}
