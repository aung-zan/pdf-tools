// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.ec;

import java.math.BigInteger;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;

public class ECElGamalEncryptor implements ECEncryptor
{
    private ECPublicKeyParameters key;
    private SecureRandom random;
    
    public void init(final CipherParameters cipherParameters) {
        if (cipherParameters instanceof ParametersWithRandom) {
            final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)cipherParameters;
            if (!(parametersWithRandom.getParameters() instanceof ECPublicKeyParameters)) {
                throw new IllegalArgumentException("ECPublicKeyParameters are required for encryption.");
            }
            this.key = (ECPublicKeyParameters)parametersWithRandom.getParameters();
            this.random = parametersWithRandom.getRandom();
        }
        else {
            if (!(cipherParameters instanceof ECPublicKeyParameters)) {
                throw new IllegalArgumentException("ECPublicKeyParameters are required for encryption.");
            }
            this.key = (ECPublicKeyParameters)cipherParameters;
            this.random = new SecureRandom();
        }
    }
    
    public ECPair encrypt(final ECPoint ecPoint) {
        if (this.key == null) {
            throw new IllegalStateException("ECElGamalEncryptor not initialised");
        }
        final BigInteger generateK = ECUtil.generateK(this.key.getParameters().getN(), this.random);
        return new ECPair(this.key.getParameters().getG().multiply(generateK), this.key.getQ().multiply(generateK).add(ecPoint));
    }
}
