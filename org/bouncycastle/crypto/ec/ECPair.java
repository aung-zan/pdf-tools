// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.ec;

import org.bouncycastle.math.ec.ECPoint;

public class ECPair
{
    private final ECPoint x;
    private final ECPoint y;
    
    public ECPair(final ECPoint x, final ECPoint y) {
        this.x = x;
        this.y = y;
    }
    
    public ECPoint getX() {
        return this.x;
    }
    
    public ECPoint getY() {
        return this.y;
    }
    
    public byte[] getEncoded() {
        final byte[] encoded = this.x.getEncoded();
        final byte[] encoded2 = this.y.getEncoded();
        final byte[] array = new byte[encoded.length + encoded2.length];
        System.arraycopy(encoded, 0, array, 0, encoded.length);
        System.arraycopy(encoded2, 0, array, encoded.length, encoded2.length);
        return array;
    }
}
