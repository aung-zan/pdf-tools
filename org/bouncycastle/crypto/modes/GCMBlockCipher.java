// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes;

import org.bouncycastle.crypto.modes.gcm.Tables1kGCMExponentiator;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.util.Pack;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.params.AEADParameters;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.modes.gcm.Tables8kGCMMultiplier;
import org.bouncycastle.crypto.modes.gcm.GCMExponentiator;
import org.bouncycastle.crypto.modes.gcm.GCMMultiplier;
import org.bouncycastle.crypto.BlockCipher;

public class GCMBlockCipher implements AEADBlockCipher
{
    private static final int BLOCK_SIZE = 16;
    private BlockCipher cipher;
    private GCMMultiplier multiplier;
    private GCMExponentiator exp;
    private boolean forEncryption;
    private int macSize;
    private byte[] nonce;
    private byte[] initialAssociatedText;
    private byte[] H;
    private byte[] J0;
    private byte[] bufBlock;
    private byte[] macBlock;
    private byte[] S;
    private byte[] S_at;
    private byte[] S_atPre;
    private byte[] counter;
    private int bufOff;
    private long totalLength;
    private byte[] atBlock;
    private int atBlockPos;
    private long atLength;
    private long atLengthPre;
    
    public GCMBlockCipher(final BlockCipher blockCipher) {
        this(blockCipher, null);
    }
    
    public GCMBlockCipher(final BlockCipher cipher, GCMMultiplier multiplier) {
        if (cipher.getBlockSize() != 16) {
            throw new IllegalArgumentException("cipher required with a block size of 16.");
        }
        if (multiplier == null) {
            multiplier = new Tables8kGCMMultiplier();
        }
        this.cipher = cipher;
        this.multiplier = multiplier;
    }
    
    public BlockCipher getUnderlyingCipher() {
        return this.cipher;
    }
    
    public String getAlgorithmName() {
        return this.cipher.getAlgorithmName() + "/GCM";
    }
    
    public void init(final boolean forEncryption, final CipherParameters cipherParameters) throws IllegalArgumentException {
        this.forEncryption = forEncryption;
        this.macBlock = null;
        KeyParameter key;
        if (cipherParameters instanceof AEADParameters) {
            final AEADParameters aeadParameters = (AEADParameters)cipherParameters;
            this.nonce = aeadParameters.getNonce();
            this.initialAssociatedText = aeadParameters.getAssociatedText();
            final int macSize = aeadParameters.getMacSize();
            if (macSize < 96 || macSize > 128 || macSize % 8 != 0) {
                throw new IllegalArgumentException("Invalid value for MAC size: " + macSize);
            }
            this.macSize = macSize / 8;
            key = aeadParameters.getKey();
        }
        else {
            if (!(cipherParameters instanceof ParametersWithIV)) {
                throw new IllegalArgumentException("invalid parameters passed to GCM");
            }
            final ParametersWithIV parametersWithIV = (ParametersWithIV)cipherParameters;
            this.nonce = parametersWithIV.getIV();
            this.initialAssociatedText = null;
            this.macSize = 16;
            key = (KeyParameter)parametersWithIV.getParameters();
        }
        this.bufBlock = new byte[forEncryption ? 16 : (16 + this.macSize)];
        if (this.nonce == null || this.nonce.length < 1) {
            throw new IllegalArgumentException("IV must be at least 1 byte");
        }
        if (key != null) {
            this.cipher.init(true, key);
            this.H = new byte[16];
            this.cipher.processBlock(this.H, 0, this.H, 0);
            this.multiplier.init(this.H);
            this.exp = null;
        }
        this.J0 = new byte[16];
        if (this.nonce.length == 12) {
            System.arraycopy(this.nonce, 0, this.J0, 0, this.nonce.length);
            this.J0[15] = 1;
        }
        else {
            this.gHASH(this.J0, this.nonce, this.nonce.length);
            final byte[] array = new byte[16];
            Pack.longToBigEndian(this.nonce.length * 8L, array, 8);
            this.gHASHBlock(this.J0, array);
        }
        this.S = new byte[16];
        this.S_at = new byte[16];
        this.S_atPre = new byte[16];
        this.atBlock = new byte[16];
        this.atBlockPos = 0;
        this.atLength = 0L;
        this.atLengthPre = 0L;
        this.counter = Arrays.clone(this.J0);
        this.bufOff = 0;
        this.totalLength = 0L;
        if (this.initialAssociatedText != null) {
            this.processAADBytes(this.initialAssociatedText, 0, this.initialAssociatedText.length);
        }
    }
    
    public byte[] getMac() {
        return Arrays.clone(this.macBlock);
    }
    
    public int getOutputSize(final int n) {
        final int n2 = n + this.bufOff;
        if (this.forEncryption) {
            return n2 + this.macSize;
        }
        return (n2 < this.macSize) ? 0 : (n2 - this.macSize);
    }
    
    public int getUpdateOutputSize(final int n) {
        int n2 = n + this.bufOff;
        if (!this.forEncryption) {
            if (n2 < this.macSize) {
                return 0;
            }
            n2 -= this.macSize;
        }
        return n2 - n2 % 16;
    }
    
    public void processAADByte(final byte b) {
        this.atBlock[this.atBlockPos] = b;
        if (++this.atBlockPos == 16) {
            this.gHASHBlock(this.S_at, this.atBlock);
            this.atBlockPos = 0;
            this.atLength += 16L;
        }
    }
    
    public void processAADBytes(final byte[] array, final int n, final int n2) {
        for (int i = 0; i < n2; ++i) {
            this.atBlock[this.atBlockPos] = array[n + i];
            if (++this.atBlockPos == 16) {
                this.gHASHBlock(this.S_at, this.atBlock);
                this.atBlockPos = 0;
                this.atLength += 16L;
            }
        }
    }
    
    private void initCipher() {
        if (this.atLength > 0L) {
            System.arraycopy(this.S_at, 0, this.S_atPre, 0, 16);
            this.atLengthPre = this.atLength;
        }
        if (this.atBlockPos > 0) {
            this.gHASHPartial(this.S_atPre, this.atBlock, 0, this.atBlockPos);
            this.atLengthPre += this.atBlockPos;
        }
        if (this.atLengthPre > 0L) {
            System.arraycopy(this.S_atPre, 0, this.S, 0, 16);
        }
    }
    
    public int processByte(final byte b, final byte[] array, final int n) throws DataLengthException {
        this.bufBlock[this.bufOff] = b;
        if (++this.bufOff == this.bufBlock.length) {
            this.outputBlock(array, n);
            return 16;
        }
        return 0;
    }
    
    public int processBytes(final byte[] array, final int n, final int n2, final byte[] array2, final int n3) throws DataLengthException {
        int n4 = 0;
        for (int i = 0; i < n2; ++i) {
            this.bufBlock[this.bufOff] = array[n + i];
            if (++this.bufOff == this.bufBlock.length) {
                this.outputBlock(array2, n3 + n4);
                n4 += 16;
            }
        }
        return n4;
    }
    
    private void outputBlock(final byte[] array, final int n) {
        if (this.totalLength == 0L) {
            this.initCipher();
        }
        this.gCTRBlock(this.bufBlock, array, n);
        if (this.forEncryption) {
            this.bufOff = 0;
        }
        else {
            System.arraycopy(this.bufBlock, 16, this.bufBlock, 0, this.macSize);
            this.bufOff = this.macSize;
        }
    }
    
    public int doFinal(final byte[] array, final int n) throws IllegalStateException, InvalidCipherTextException {
        if (this.totalLength == 0L) {
            this.initCipher();
        }
        int bufOff = this.bufOff;
        if (!this.forEncryption) {
            if (bufOff < this.macSize) {
                throw new InvalidCipherTextException("data too short");
            }
            bufOff -= this.macSize;
        }
        if (bufOff > 0) {
            this.gCTRPartial(this.bufBlock, 0, bufOff, array, n);
        }
        this.atLength += this.atBlockPos;
        if (this.atLength > this.atLengthPre) {
            if (this.atBlockPos > 0) {
                this.gHASHPartial(this.S_at, this.atBlock, 0, this.atBlockPos);
            }
            if (this.atLengthPre > 0L) {
                xor(this.S_at, this.S_atPre);
            }
            final long n2 = this.totalLength * 8L + 127L >>> 7;
            final byte[] array2 = new byte[16];
            if (this.exp == null) {
                (this.exp = new Tables1kGCMExponentiator()).init(this.H);
            }
            this.exp.exponentiateX(n2, array2);
            multiply(this.S_at, array2);
            xor(this.S, this.S_at);
        }
        final byte[] array3 = new byte[16];
        Pack.longToBigEndian(this.atLength * 8L, array3, 0);
        Pack.longToBigEndian(this.totalLength * 8L, array3, 8);
        this.gHASHBlock(this.S, array3);
        final byte[] array4 = new byte[16];
        this.cipher.processBlock(this.J0, 0, array4, 0);
        xor(array4, this.S);
        int n3 = bufOff;
        System.arraycopy(array4, 0, this.macBlock = new byte[this.macSize], 0, this.macSize);
        if (this.forEncryption) {
            System.arraycopy(this.macBlock, 0, array, n + this.bufOff, this.macSize);
            n3 += this.macSize;
        }
        else {
            final byte[] array5 = new byte[this.macSize];
            System.arraycopy(this.bufBlock, bufOff, array5, 0, this.macSize);
            if (!Arrays.constantTimeAreEqual(this.macBlock, array5)) {
                throw new InvalidCipherTextException("mac check in GCM failed");
            }
        }
        this.reset(false);
        return n3;
    }
    
    public void reset() {
        this.reset(true);
    }
    
    private void reset(final boolean b) {
        this.cipher.reset();
        this.S = new byte[16];
        this.S_at = new byte[16];
        this.S_atPre = new byte[16];
        this.atBlock = new byte[16];
        this.atBlockPos = 0;
        this.atLength = 0L;
        this.atLengthPre = 0L;
        this.counter = Arrays.clone(this.J0);
        this.bufOff = 0;
        this.totalLength = 0L;
        if (this.bufBlock != null) {
            Arrays.fill(this.bufBlock, (byte)0);
        }
        if (b) {
            this.macBlock = null;
        }
        if (this.initialAssociatedText != null) {
            this.processAADBytes(this.initialAssociatedText, 0, this.initialAssociatedText.length);
        }
    }
    
    private void gCTRBlock(final byte[] array, final byte[] array2, final int n) {
        final byte[] nextCounterBlock = this.getNextCounterBlock();
        xor(nextCounterBlock, array);
        System.arraycopy(nextCounterBlock, 0, array2, n, 16);
        this.gHASHBlock(this.S, this.forEncryption ? nextCounterBlock : array);
        this.totalLength += 16L;
    }
    
    private void gCTRPartial(final byte[] array, final int n, final int n2, final byte[] array2, final int n3) {
        final byte[] nextCounterBlock = this.getNextCounterBlock();
        xor(nextCounterBlock, array, n, n2);
        System.arraycopy(nextCounterBlock, 0, array2, n3, n2);
        this.gHASHPartial(this.S, this.forEncryption ? nextCounterBlock : array, 0, n2);
        this.totalLength += n2;
    }
    
    private void gHASH(final byte[] array, final byte[] array2, final int n) {
        for (int i = 0; i < n; i += 16) {
            this.gHASHPartial(array, array2, i, Math.min(n - i, 16));
        }
    }
    
    private void gHASHBlock(final byte[] array, final byte[] array2) {
        xor(array, array2);
        this.multiplier.multiplyH(array);
    }
    
    private void gHASHPartial(final byte[] array, final byte[] array2, final int n, final int n2) {
        xor(array, array2, n, n2);
        this.multiplier.multiplyH(array);
    }
    
    private byte[] getNextCounterBlock() {
        for (int n = 15; n >= 12 && (this.counter[n] = (byte)(this.counter[n] + 1 & 0xFF)) == 0; --n) {}
        final byte[] array = new byte[16];
        this.cipher.processBlock(this.counter, 0, array, 0);
        return array;
    }
    
    private static void multiply(final byte[] array, final byte[] array2) {
        final byte[] clone = Arrays.clone(array);
        final byte[] array3 = new byte[16];
        for (int i = 0; i < 16; ++i) {
            final byte b = array2[i];
            for (int j = 7; j >= 0; --j) {
                if ((b & 1 << j) != 0x0) {
                    xor(array3, clone);
                }
                final boolean b2 = (clone[15] & 0x1) != 0x0;
                shiftRight(clone);
                if (b2) {
                    final byte[] array4 = clone;
                    final int n = 0;
                    array4[n] ^= 0xFFFFFFE1;
                }
            }
        }
        System.arraycopy(array3, 0, array, 0, 16);
    }
    
    private static void shiftRight(final byte[] array) {
        int n = 0;
        int n2 = 0;
        while (true) {
            final int n3 = array[n] & 0xFF;
            array[n] = (byte)(n3 >>> 1 | n2);
            if (++n == 16) {
                break;
            }
            n2 = (n3 & 0x1) << 7;
        }
    }
    
    private static void xor(final byte[] array, final byte[] array2) {
        for (int i = 15; i >= 0; --i) {
            final int n = i;
            array[n] ^= array2[i];
        }
    }
    
    private static void xor(final byte[] array, final byte[] array2, final int n, int n2) {
        while (n2-- > 0) {
            final int n3 = n2;
            array[n3] ^= array2[n + n2];
        }
    }
}
