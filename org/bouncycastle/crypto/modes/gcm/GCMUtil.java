// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.gcm;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.util.Pack;

abstract class GCMUtil
{
    static byte[] oneAsBytes() {
        final byte[] array = new byte[16];
        array[0] = -128;
        return array;
    }
    
    static int[] oneAsInts() {
        final int[] array = new int[4];
        array[0] = Integer.MIN_VALUE;
        return array;
    }
    
    static byte[] asBytes(final int[] array) {
        final byte[] array2 = new byte[16];
        Pack.intToBigEndian(array, array2, 0);
        return array2;
    }
    
    static int[] asInts(final byte[] array) {
        final int[] array2 = new int[4];
        Pack.bigEndianToInt(array, 0, array2);
        return array2;
    }
    
    static void asInts(final byte[] array, final int[] array2) {
        Pack.bigEndianToInt(array, 0, array2);
    }
    
    static void multiply(final byte[] array, final byte[] array2) {
        final byte[] clone = Arrays.clone(array);
        final byte[] array3 = new byte[16];
        for (int i = 0; i < 16; ++i) {
            final byte b = array2[i];
            for (int j = 7; j >= 0; --j) {
                if ((b & 1 << j) != 0x0) {
                    xor(array3, clone);
                }
                final boolean b2 = (clone[15] & 0x1) != 0x0;
                shiftRight(clone);
                if (b2) {
                    final byte[] array4 = clone;
                    final int n = 0;
                    array4[n] ^= 0xFFFFFFE1;
                }
            }
        }
        System.arraycopy(array3, 0, array, 0, 16);
    }
    
    static void multiplyP(final int[] array) {
        final boolean b = (array[3] & 0x1) != 0x0;
        shiftRight(array);
        if (b) {
            final int n = 0;
            array[n] ^= 0xE1000000;
        }
    }
    
    static void multiplyP(final int[] array, final int[] array2) {
        final boolean b = (array[3] & 0x1) != 0x0;
        shiftRight(array, array2);
        if (b) {
            final int n = 0;
            array2[n] ^= 0xE1000000;
        }
    }
    
    static void multiplyP8(final int[] array) {
        final int n = array[3];
        shiftRightN(array, 8);
        for (int i = 7; i >= 0; --i) {
            if ((n & 1 << i) != 0x0) {
                final int n2 = 0;
                array[n2] ^= -520093696 >>> 7 - i;
            }
        }
    }
    
    static void multiplyP8(final int[] array, final int[] array2) {
        final int n = array[3];
        shiftRightN(array, 8, array2);
        for (int i = 7; i >= 0; --i) {
            if ((n & 1 << i) != 0x0) {
                final int n2 = 0;
                array2[n2] ^= -520093696 >>> 7 - i;
            }
        }
    }
    
    static void shiftRight(final byte[] array) {
        int n = 0;
        int n2 = 0;
        while (true) {
            final int n3 = array[n] & 0xFF;
            array[n] = (byte)(n3 >>> 1 | n2);
            if (++n == 16) {
                break;
            }
            n2 = (n3 & 0x1) << 7;
        }
    }
    
    static void shiftRight(final byte[] array, final byte[] array2) {
        int n = 0;
        int n2 = 0;
        while (true) {
            final int n3 = array[n] & 0xFF;
            array2[n] = (byte)(n3 >>> 1 | n2);
            if (++n == 16) {
                break;
            }
            n2 = (n3 & 0x1) << 7;
        }
    }
    
    static void shiftRight(final int[] array) {
        int n = 0;
        int n2 = 0;
        while (true) {
            final int n3 = array[n];
            array[n] = (n3 >>> 1 | n2);
            if (++n == 4) {
                break;
            }
            n2 = n3 << 31;
        }
    }
    
    static void shiftRight(final int[] array, final int[] array2) {
        int n = 0;
        int n2 = 0;
        while (true) {
            final int n3 = array[n];
            array2[n] = (n3 >>> 1 | n2);
            if (++n == 4) {
                break;
            }
            n2 = n3 << 31;
        }
    }
    
    static void shiftRightN(final int[] array, final int n) {
        int n2 = 0;
        int n3 = 0;
        while (true) {
            final int n4 = array[n2];
            array[n2] = (n4 >>> n | n3);
            if (++n2 == 4) {
                break;
            }
            n3 = n4 << 32 - n;
        }
    }
    
    static void shiftRightN(final int[] array, final int n, final int[] array2) {
        int n2 = 0;
        int n3 = 0;
        while (true) {
            final int n4 = array[n2];
            array2[n2] = (n4 >>> n | n3);
            if (++n2 == 4) {
                break;
            }
            n3 = n4 << 32 - n;
        }
    }
    
    static void xor(final byte[] array, final byte[] array2) {
        for (int i = 15; i >= 0; --i) {
            final int n = i;
            array[n] ^= array2[i];
        }
    }
    
    static void xor(final byte[] array, final byte[] array2, final int n, int n2) {
        while (n2-- > 0) {
            final int n3 = n2;
            array[n3] ^= array2[n + n2];
        }
    }
    
    static void xor(final byte[] array, final byte[] array2, final byte[] array3) {
        for (int i = 15; i >= 0; --i) {
            array3[i] = (byte)(array[i] ^ array2[i]);
        }
    }
    
    static void xor(final int[] array, final int[] array2) {
        for (int i = 3; i >= 0; --i) {
            final int n = i;
            array[n] ^= array2[i];
        }
    }
    
    static void xor(final int[] array, final int[] array2, final int[] array3) {
        for (int i = 3; i >= 0; --i) {
            array3[i] = (array[i] ^ array2[i]);
        }
    }
}
