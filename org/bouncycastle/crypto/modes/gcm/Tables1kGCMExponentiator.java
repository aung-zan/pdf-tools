// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.gcm;

import org.bouncycastle.util.Arrays;
import java.util.Vector;

public class Tables1kGCMExponentiator implements GCMExponentiator
{
    private Vector lookupPowX2;
    
    public void init(final byte[] array) {
        if (this.lookupPowX2 != null && Arrays.areEqual(array, this.lookupPowX2.elementAt(0))) {
            return;
        }
        (this.lookupPowX2 = new Vector(8)).addElement(Arrays.clone(array));
    }
    
    public void exponentiateX(long n, final byte[] array) {
        final byte[] oneAsBytes = GCMUtil.oneAsBytes();
        int index = 0;
        while (n > 0L) {
            if ((n & 0x1L) != 0x0L) {
                this.ensureAvailable(index);
                GCMUtil.multiply(oneAsBytes, (byte[])this.lookupPowX2.elementAt(index));
            }
            ++index;
            n >>>= 1;
        }
        System.arraycopy(oneAsBytes, 0, array, 0, 16);
    }
    
    private void ensureAvailable(final int n) {
        int size = this.lookupPowX2.size();
        if (size <= n) {
            byte[] clone = this.lookupPowX2.elementAt(size - 1);
            do {
                clone = Arrays.clone(clone);
                GCMUtil.multiply(clone, clone);
                this.lookupPowX2.addElement(clone);
            } while (++size <= n);
        }
    }
}
