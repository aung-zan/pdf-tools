// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.gcm;

import org.bouncycastle.util.Arrays;

public class BasicGCMMultiplier implements GCMMultiplier
{
    private byte[] H;
    
    public void init(final byte[] array) {
        this.H = Arrays.clone(array);
    }
    
    public void multiplyH(final byte[] array) {
        GCMUtil.multiply(array, this.H);
    }
}
