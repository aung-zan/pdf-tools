// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.gcm;

import org.bouncycastle.crypto.util.Pack;
import org.bouncycastle.util.Arrays;

public class Tables8kGCMMultiplier implements GCMMultiplier
{
    private byte[] H;
    private int[][][] M;
    
    public void init(final byte[] array) {
        if (this.M == null) {
            this.M = new int[32][16][4];
        }
        else if (Arrays.areEqual(this.H, array)) {
            return;
        }
        this.H = Arrays.clone(array);
        GCMUtil.asInts(array, this.M[1][8]);
        for (int i = 4; i >= 1; i >>= 1) {
            GCMUtil.multiplyP(this.M[1][i + i], this.M[1][i]);
        }
        GCMUtil.multiplyP(this.M[1][1], this.M[0][8]);
        for (int j = 4; j >= 1; j >>= 1) {
            GCMUtil.multiplyP(this.M[0][j + j], this.M[0][j]);
        }
        int n = 0;
        while (true) {
            for (int k = 2; k < 16; k += k) {
                for (int l = 1; l < k; ++l) {
                    GCMUtil.xor(this.M[n][k], this.M[n][l], this.M[n][k + l]);
                }
            }
            if (++n == 32) {
                break;
            }
            if (n <= 1) {
                continue;
            }
            for (int n2 = 8; n2 > 0; n2 >>= 1) {
                GCMUtil.multiplyP8(this.M[n - 2][n2], this.M[n][n2]);
            }
        }
    }
    
    public void multiplyH(final byte[] array) {
        final int[] array2 = new int[4];
        for (int i = 15; i >= 0; --i) {
            final int[] array3 = this.M[i + i][array[i] & 0xF];
            final int[] array4 = array2;
            final int n = 0;
            array4[n] ^= array3[0];
            final int[] array5 = array2;
            final int n2 = 1;
            array5[n2] ^= array3[1];
            final int[] array6 = array2;
            final int n3 = 2;
            array6[n3] ^= array3[2];
            final int[] array7 = array2;
            final int n4 = 3;
            array7[n4] ^= array3[3];
            final int[] array8 = this.M[i + i + 1][(array[i] & 0xF0) >>> 4];
            final int[] array9 = array2;
            final int n5 = 0;
            array9[n5] ^= array8[0];
            final int[] array10 = array2;
            final int n6 = 1;
            array10[n6] ^= array8[1];
            final int[] array11 = array2;
            final int n7 = 2;
            array11[n7] ^= array8[2];
            final int[] array12 = array2;
            final int n8 = 3;
            array12[n8] ^= array8[3];
        }
        Pack.intToBigEndian(array2, array, 0);
    }
}
