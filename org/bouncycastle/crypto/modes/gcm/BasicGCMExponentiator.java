// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.gcm;

import org.bouncycastle.util.Arrays;

public class BasicGCMExponentiator implements GCMExponentiator
{
    private byte[] x;
    
    public void init(final byte[] array) {
        this.x = Arrays.clone(array);
    }
    
    public void exponentiateX(long n, final byte[] array) {
        final byte[] oneAsBytes = GCMUtil.oneAsBytes();
        if (n > 0L) {
            final byte[] clone = Arrays.clone(this.x);
            do {
                if ((n & 0x1L) != 0x0L) {
                    GCMUtil.multiply(oneAsBytes, clone);
                }
                GCMUtil.multiply(clone, clone);
                n >>>= 1;
            } while (n > 0L);
        }
        System.arraycopy(oneAsBytes, 0, array, 0, 16);
    }
}
