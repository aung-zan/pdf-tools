// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.gcm;

import org.bouncycastle.crypto.util.Pack;
import org.bouncycastle.util.Arrays;

public class Tables64kGCMMultiplier implements GCMMultiplier
{
    private byte[] H;
    private int[][][] M;
    
    public void init(final byte[] array) {
        if (this.M == null) {
            this.M = new int[16][256][4];
        }
        else if (Arrays.areEqual(this.H, array)) {
            return;
        }
        this.H = Arrays.clone(array);
        GCMUtil.asInts(array, this.M[0][128]);
        for (int i = 64; i >= 1; i >>= 1) {
            GCMUtil.multiplyP(this.M[0][i + i], this.M[0][i]);
        }
        int n = 0;
        while (true) {
            for (int j = 2; j < 256; j += j) {
                for (int k = 1; k < j; ++k) {
                    GCMUtil.xor(this.M[n][j], this.M[n][k], this.M[n][j + k]);
                }
            }
            if (++n == 16) {
                break;
            }
            for (int l = 128; l > 0; l >>= 1) {
                GCMUtil.multiplyP8(this.M[n - 1][l], this.M[n][l]);
            }
        }
    }
    
    public void multiplyH(final byte[] array) {
        final int[] array2 = new int[4];
        for (int i = 15; i >= 0; --i) {
            final int[] array3 = this.M[i][array[i] & 0xFF];
            final int[] array4 = array2;
            final int n = 0;
            array4[n] ^= array3[0];
            final int[] array5 = array2;
            final int n2 = 1;
            array5[n2] ^= array3[1];
            final int[] array6 = array2;
            final int n3 = 2;
            array6[n3] ^= array3[2];
            final int[] array7 = array2;
            final int n4 = 3;
            array7[n4] ^= array3[3];
        }
        Pack.intToBigEndian(array2, array, 0);
    }
}
