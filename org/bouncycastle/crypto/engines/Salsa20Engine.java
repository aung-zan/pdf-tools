// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.util.Strings;
import org.bouncycastle.crypto.util.Pack;
import org.bouncycastle.crypto.OutputLengthException;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.MaxBytesExceededException;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.StreamCipher;

public class Salsa20Engine implements StreamCipher
{
    private static final int STATE_SIZE = 16;
    private static final byte[] sigma;
    private static final byte[] tau;
    private int index;
    private int[] engineState;
    private int[] x;
    private byte[] keyStream;
    private byte[] workingKey;
    private byte[] workingIV;
    private boolean initialised;
    private int cW0;
    private int cW1;
    private int cW2;
    
    public Salsa20Engine() {
        this.index = 0;
        this.engineState = new int[16];
        this.x = new int[16];
        this.keyStream = new byte[64];
        this.workingKey = null;
        this.workingIV = null;
        this.initialised = false;
    }
    
    public void init(final boolean b, final CipherParameters cipherParameters) {
        if (!(cipherParameters instanceof ParametersWithIV)) {
            throw new IllegalArgumentException("Salsa20 Init parameters must include an IV");
        }
        final ParametersWithIV parametersWithIV = (ParametersWithIV)cipherParameters;
        final byte[] iv = parametersWithIV.getIV();
        if (iv == null || iv.length != 8) {
            throw new IllegalArgumentException("Salsa20 requires exactly 8 bytes of IV");
        }
        if (!(parametersWithIV.getParameters() instanceof KeyParameter)) {
            throw new IllegalArgumentException("Salsa20 Init parameters must include a key");
        }
        this.workingKey = ((KeyParameter)parametersWithIV.getParameters()).getKey();
        this.workingIV = iv;
        this.setKey(this.workingKey, this.workingIV);
    }
    
    public String getAlgorithmName() {
        return "Salsa20";
    }
    
    public byte returnByte(final byte b) {
        if (this.limitExceeded()) {
            throw new MaxBytesExceededException("2^70 byte limit per IV; Change IV");
        }
        if (this.index == 0) {
            this.generateKeyStream(this.keyStream);
            if (++this.engineState[8] == 0) {
                final int[] engineState = this.engineState;
                final int n = 9;
                ++engineState[n];
            }
        }
        final byte b2 = (byte)(this.keyStream[this.index] ^ b);
        this.index = (this.index + 1 & 0x3F);
        return b2;
    }
    
    public void processBytes(final byte[] array, final int n, final int n2, final byte[] array2, final int n3) {
        if (!this.initialised) {
            throw new IllegalStateException(this.getAlgorithmName() + " not initialised");
        }
        if (n + n2 > array.length) {
            throw new DataLengthException("input buffer too short");
        }
        if (n3 + n2 > array2.length) {
            throw new OutputLengthException("output buffer too short");
        }
        if (this.limitExceeded(n2)) {
            throw new MaxBytesExceededException("2^70 byte limit per IV would be exceeded; Change IV");
        }
        for (int i = 0; i < n2; ++i) {
            if (this.index == 0) {
                this.generateKeyStream(this.keyStream);
                if (++this.engineState[8] == 0) {
                    final int[] engineState = this.engineState;
                    final int n4 = 9;
                    ++engineState[n4];
                }
            }
            array2[i + n3] = (byte)(this.keyStream[this.index] ^ array[i + n]);
            this.index = (this.index + 1 & 0x3F);
        }
    }
    
    public void reset() {
        this.setKey(this.workingKey, this.workingIV);
    }
    
    private void setKey(final byte[] workingKey, final byte[] workingIV) {
        this.workingKey = workingKey;
        this.workingIV = workingIV;
        this.index = 0;
        this.resetCounter();
        int n = 0;
        this.engineState[1] = Pack.littleEndianToInt(this.workingKey, 0);
        this.engineState[2] = Pack.littleEndianToInt(this.workingKey, 4);
        this.engineState[3] = Pack.littleEndianToInt(this.workingKey, 8);
        this.engineState[4] = Pack.littleEndianToInt(this.workingKey, 12);
        byte[] array;
        if (this.workingKey.length == 32) {
            array = Salsa20Engine.sigma;
            n = 16;
        }
        else {
            array = Salsa20Engine.tau;
        }
        this.engineState[11] = Pack.littleEndianToInt(this.workingKey, n);
        this.engineState[12] = Pack.littleEndianToInt(this.workingKey, n + 4);
        this.engineState[13] = Pack.littleEndianToInt(this.workingKey, n + 8);
        this.engineState[14] = Pack.littleEndianToInt(this.workingKey, n + 12);
        this.engineState[0] = Pack.littleEndianToInt(array, 0);
        this.engineState[5] = Pack.littleEndianToInt(array, 4);
        this.engineState[10] = Pack.littleEndianToInt(array, 8);
        this.engineState[15] = Pack.littleEndianToInt(array, 12);
        this.engineState[6] = Pack.littleEndianToInt(this.workingIV, 0);
        this.engineState[7] = Pack.littleEndianToInt(this.workingIV, 4);
        this.engineState[8] = (this.engineState[9] = 0);
        this.initialised = true;
    }
    
    private void generateKeyStream(final byte[] array) {
        salsaCore(20, this.engineState, this.x);
        Pack.intToLittleEndian(this.x, array, 0);
    }
    
    public static void salsaCore(final int n, final int[] array, final int[] array2) {
        System.arraycopy(array, 0, array2, 0, array.length);
        for (int i = n; i > 0; i -= 2) {
            final int n2 = 4;
            array2[n2] ^= rotl(array2[0] + array2[12], 7);
            final int n3 = 8;
            array2[n3] ^= rotl(array2[4] + array2[0], 9);
            final int n4 = 12;
            array2[n4] ^= rotl(array2[8] + array2[4], 13);
            final int n5 = 0;
            array2[n5] ^= rotl(array2[12] + array2[8], 18);
            final int n6 = 9;
            array2[n6] ^= rotl(array2[5] + array2[1], 7);
            final int n7 = 13;
            array2[n7] ^= rotl(array2[9] + array2[5], 9);
            final int n8 = 1;
            array2[n8] ^= rotl(array2[13] + array2[9], 13);
            final int n9 = 5;
            array2[n9] ^= rotl(array2[1] + array2[13], 18);
            final int n10 = 14;
            array2[n10] ^= rotl(array2[10] + array2[6], 7);
            final int n11 = 2;
            array2[n11] ^= rotl(array2[14] + array2[10], 9);
            final int n12 = 6;
            array2[n12] ^= rotl(array2[2] + array2[14], 13);
            final int n13 = 10;
            array2[n13] ^= rotl(array2[6] + array2[2], 18);
            final int n14 = 3;
            array2[n14] ^= rotl(array2[15] + array2[11], 7);
            final int n15 = 7;
            array2[n15] ^= rotl(array2[3] + array2[15], 9);
            final int n16 = 11;
            array2[n16] ^= rotl(array2[7] + array2[3], 13);
            final int n17 = 15;
            array2[n17] ^= rotl(array2[11] + array2[7], 18);
            final int n18 = 1;
            array2[n18] ^= rotl(array2[0] + array2[3], 7);
            final int n19 = 2;
            array2[n19] ^= rotl(array2[1] + array2[0], 9);
            final int n20 = 3;
            array2[n20] ^= rotl(array2[2] + array2[1], 13);
            final int n21 = 0;
            array2[n21] ^= rotl(array2[3] + array2[2], 18);
            final int n22 = 6;
            array2[n22] ^= rotl(array2[5] + array2[4], 7);
            final int n23 = 7;
            array2[n23] ^= rotl(array2[6] + array2[5], 9);
            final int n24 = 4;
            array2[n24] ^= rotl(array2[7] + array2[6], 13);
            final int n25 = 5;
            array2[n25] ^= rotl(array2[4] + array2[7], 18);
            final int n26 = 11;
            array2[n26] ^= rotl(array2[10] + array2[9], 7);
            final int n27 = 8;
            array2[n27] ^= rotl(array2[11] + array2[10], 9);
            final int n28 = 9;
            array2[n28] ^= rotl(array2[8] + array2[11], 13);
            final int n29 = 10;
            array2[n29] ^= rotl(array2[9] + array2[8], 18);
            final int n30 = 12;
            array2[n30] ^= rotl(array2[15] + array2[14], 7);
            final int n31 = 13;
            array2[n31] ^= rotl(array2[12] + array2[15], 9);
            final int n32 = 14;
            array2[n32] ^= rotl(array2[13] + array2[12], 13);
            final int n33 = 15;
            array2[n33] ^= rotl(array2[14] + array2[13], 18);
        }
        for (int j = 0; j < 16; ++j) {
            final int n34 = j;
            array2[n34] += array[j];
        }
    }
    
    private static int rotl(final int n, final int n2) {
        return n << n2 | n >>> -n2;
    }
    
    private void resetCounter() {
        this.cW0 = 0;
        this.cW1 = 0;
        this.cW2 = 0;
    }
    
    private boolean limitExceeded() {
        return ++this.cW0 == 0 && ++this.cW1 == 0 && (++this.cW2 & 0x20) != 0x0;
    }
    
    private boolean limitExceeded(final int n) {
        this.cW0 += n;
        return this.cW0 < n && this.cW0 >= 0 && ++this.cW1 == 0 && (++this.cW2 & 0x20) != 0x0;
    }
    
    static {
        sigma = Strings.toByteArray("expand 32-byte k");
        tau = Strings.toByteArray("expand 16-byte k");
    }
}
