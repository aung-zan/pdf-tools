// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.EphemeralKeyPair;
import org.bouncycastle.crypto.DerivationParameters;
import org.bouncycastle.crypto.params.KDFParameters;
import org.bouncycastle.util.BigIntegers;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.util.Pack;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.IESWithCipherParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.KeyParser;
import org.bouncycastle.crypto.generators.EphemeralKeyPairGenerator;
import org.bouncycastle.crypto.params.IESParameters;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.DerivationFunction;
import org.bouncycastle.crypto.BasicAgreement;

public class IESEngine
{
    BasicAgreement agree;
    DerivationFunction kdf;
    Mac mac;
    BufferedBlockCipher cipher;
    byte[] macBuf;
    boolean forEncryption;
    CipherParameters privParam;
    CipherParameters pubParam;
    IESParameters param;
    byte[] V;
    private EphemeralKeyPairGenerator keyPairGenerator;
    private KeyParser keyParser;
    
    public IESEngine(final BasicAgreement agree, final DerivationFunction kdf, final Mac mac) {
        this.agree = agree;
        this.kdf = kdf;
        this.mac = mac;
        this.macBuf = new byte[mac.getMacSize()];
        this.cipher = null;
    }
    
    public IESEngine(final BasicAgreement agree, final DerivationFunction kdf, final Mac mac, final BufferedBlockCipher cipher) {
        this.agree = agree;
        this.kdf = kdf;
        this.mac = mac;
        this.macBuf = new byte[mac.getMacSize()];
        this.cipher = cipher;
    }
    
    public void init(final boolean forEncryption, final CipherParameters privParam, final CipherParameters pubParam, final CipherParameters cipherParameters) {
        this.forEncryption = forEncryption;
        this.privParam = privParam;
        this.pubParam = pubParam;
        this.param = (IESParameters)cipherParameters;
        this.V = new byte[0];
    }
    
    public void init(final AsymmetricKeyParameter pubParam, final CipherParameters cipherParameters, final EphemeralKeyPairGenerator keyPairGenerator) {
        this.forEncryption = true;
        this.pubParam = pubParam;
        this.param = (IESParameters)cipherParameters;
        this.keyPairGenerator = keyPairGenerator;
    }
    
    public void init(final AsymmetricKeyParameter privParam, final CipherParameters cipherParameters, final KeyParser keyParser) {
        this.forEncryption = false;
        this.privParam = privParam;
        this.param = (IESParameters)cipherParameters;
        this.keyParser = keyParser;
    }
    
    public BufferedBlockCipher getCipher() {
        return this.cipher;
    }
    
    public Mac getMac() {
        return this.mac;
    }
    
    private byte[] encryptBlock(final byte[] array, final int n, final int n2) throws InvalidCipherTextException {
        byte[] array3;
        byte[] array5;
        int n3;
        if (this.cipher == null) {
            final byte[] array2 = new byte[n2];
            array3 = new byte[this.param.getMacKeySize() / 8];
            final byte[] array4 = new byte[array2.length + array3.length];
            this.kdf.generateBytes(array4, 0, array4.length);
            if (this.V.length != 0) {
                System.arraycopy(array4, 0, array3, 0, array3.length);
                System.arraycopy(array4, array3.length, array2, 0, array2.length);
            }
            else {
                System.arraycopy(array4, 0, array2, 0, array2.length);
                System.arraycopy(array4, n2, array3, 0, array3.length);
            }
            array5 = new byte[n2];
            for (int i = 0; i != n2; ++i) {
                array5[i] = (byte)(array[n + i] ^ array2[i]);
            }
            n3 = n2;
        }
        else {
            final byte[] array6 = new byte[((IESWithCipherParameters)this.param).getCipherKeySize() / 8];
            array3 = new byte[this.param.getMacKeySize() / 8];
            final byte[] array7 = new byte[array6.length + array3.length];
            this.kdf.generateBytes(array7, 0, array7.length);
            System.arraycopy(array7, 0, array6, 0, array6.length);
            System.arraycopy(array7, array6.length, array3, 0, array3.length);
            this.cipher.init(true, new KeyParameter(array6));
            array5 = new byte[this.cipher.getOutputSize(n2)];
            final int processBytes = this.cipher.processBytes(array, n, n2, array5, 0);
            n3 = processBytes + this.cipher.doFinal(array5, processBytes);
        }
        final byte[] encodingV = this.param.getEncodingV();
        final byte[] array8 = new byte[4];
        if (this.V.length != 0 && encodingV != null) {
            Pack.intToBigEndian(encodingV.length * 8, array8, 0);
        }
        final byte[] array9 = new byte[this.mac.getMacSize()];
        this.mac.init(new KeyParameter(array3));
        this.mac.update(array5, 0, array5.length);
        if (encodingV != null) {
            this.mac.update(encodingV, 0, encodingV.length);
        }
        if (this.V.length != 0) {
            this.mac.update(array8, 0, array8.length);
        }
        this.mac.doFinal(array9, 0);
        final byte[] array10 = new byte[this.V.length + n3 + array9.length];
        System.arraycopy(this.V, 0, array10, 0, this.V.length);
        System.arraycopy(array5, 0, array10, this.V.length, n3);
        System.arraycopy(array9, 0, array10, this.V.length + n3, array9.length);
        return array10;
    }
    
    private byte[] decryptBlock(final byte[] array, final int n, final int n2) throws InvalidCipherTextException {
        byte[] array3;
        byte[] array5;
        int length;
        if (this.cipher == null) {
            final byte[] array2 = new byte[n2 - this.V.length - this.mac.getMacSize()];
            array3 = new byte[this.param.getMacKeySize() / 8];
            final byte[] array4 = new byte[array2.length + array3.length];
            this.kdf.generateBytes(array4, 0, array4.length);
            if (this.V.length != 0) {
                System.arraycopy(array4, 0, array3, 0, array3.length);
                System.arraycopy(array4, array3.length, array2, 0, array2.length);
            }
            else {
                System.arraycopy(array4, 0, array2, 0, array2.length);
                System.arraycopy(array4, array2.length, array3, 0, array3.length);
            }
            array5 = new byte[array2.length];
            for (int i = 0; i != array2.length; ++i) {
                array5[i] = (byte)(array[n + this.V.length + i] ^ array2[i]);
            }
            length = array2.length;
        }
        else {
            final byte[] array6 = new byte[((IESWithCipherParameters)this.param).getCipherKeySize() / 8];
            array3 = new byte[this.param.getMacKeySize() / 8];
            final byte[] array7 = new byte[array6.length + array3.length];
            this.kdf.generateBytes(array7, 0, array7.length);
            System.arraycopy(array7, 0, array6, 0, array6.length);
            System.arraycopy(array7, array6.length, array3, 0, array3.length);
            this.cipher.init(false, new KeyParameter(array6));
            array5 = new byte[this.cipher.getOutputSize(n2 - this.V.length - this.mac.getMacSize())];
            final int processBytes = this.cipher.processBytes(array, n + this.V.length, n2 - this.V.length - this.mac.getMacSize(), array5, 0);
            length = processBytes + this.cipher.doFinal(array5, processBytes);
        }
        final byte[] encodingV = this.param.getEncodingV();
        final byte[] array8 = new byte[4];
        if (this.V.length != 0 && encodingV != null) {
            Pack.intToBigEndian(encodingV.length * 8, array8, 0);
        }
        final int n3 = n + n2;
        final byte[] copyOfRange = Arrays.copyOfRange(array, n3 - this.mac.getMacSize(), n3);
        final byte[] array9 = new byte[copyOfRange.length];
        this.mac.init(new KeyParameter(array3));
        this.mac.update(array, n + this.V.length, n2 - this.V.length - array9.length);
        if (encodingV != null) {
            this.mac.update(encodingV, 0, encodingV.length);
        }
        if (this.V.length != 0) {
            this.mac.update(array8, 0, array8.length);
        }
        this.mac.doFinal(array9, 0);
        if (!Arrays.constantTimeAreEqual(copyOfRange, array9)) {
            throw new InvalidCipherTextException("Invalid MAC.");
        }
        return Arrays.copyOfRange(array5, 0, length);
    }
    
    public byte[] processBlock(final byte[] buf, final int offset, final int length) throws InvalidCipherTextException {
        if (this.forEncryption) {
            if (this.keyPairGenerator != null) {
                final EphemeralKeyPair generate = this.keyPairGenerator.generate();
                this.privParam = generate.getKeyPair().getPrivate();
                this.V = generate.getEncodedPublicKey();
            }
        }
        else if (this.keyParser != null) {
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf, offset, length);
            try {
                this.pubParam = this.keyParser.readKey(byteArrayInputStream);
            }
            catch (IOException ex) {
                throw new InvalidCipherTextException("unable to recover ephemeral public key: " + ex.getMessage(), ex);
            }
            this.V = Arrays.copyOfRange(buf, offset, offset + (length - byteArrayInputStream.available()));
        }
        this.agree.init(this.privParam);
        final byte[] unsignedByteArray = BigIntegers.asUnsignedByteArray(this.agree.getFieldSize(), this.agree.calculateAgreement(this.pubParam));
        byte[] array;
        if (this.V.length != 0) {
            array = new byte[this.V.length + unsignedByteArray.length];
            System.arraycopy(this.V, 0, array, 0, this.V.length);
            System.arraycopy(unsignedByteArray, 0, array, this.V.length, unsignedByteArray.length);
        }
        else {
            array = unsignedByteArray;
        }
        this.kdf.init(new KDFParameters(array, this.param.getDerivationV()));
        return this.forEncryption ? this.encryptBlock(buf, offset, length) : this.decryptBlock(buf, offset, length);
    }
}
