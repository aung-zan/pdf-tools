// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.io;

import java.io.IOException;
import java.io.OutputStream;
import org.bouncycastle.crypto.StreamCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import java.io.FilterOutputStream;

public class CipherOutputStream extends FilterOutputStream
{
    private BufferedBlockCipher bufferedBlockCipher;
    private StreamCipher streamCipher;
    private byte[] oneByte;
    private byte[] buf;
    
    public CipherOutputStream(final OutputStream out, final BufferedBlockCipher bufferedBlockCipher) {
        super(out);
        this.oneByte = new byte[1];
        this.bufferedBlockCipher = bufferedBlockCipher;
        this.buf = new byte[bufferedBlockCipher.getBlockSize()];
    }
    
    public CipherOutputStream(final OutputStream out, final StreamCipher streamCipher) {
        super(out);
        this.oneByte = new byte[1];
        this.streamCipher = streamCipher;
    }
    
    @Override
    public void write(final int n) throws IOException {
        this.oneByte[0] = (byte)n;
        if (this.bufferedBlockCipher != null) {
            final int processBytes = this.bufferedBlockCipher.processBytes(this.oneByte, 0, 1, this.buf, 0);
            if (processBytes != 0) {
                this.out.write(this.buf, 0, processBytes);
            }
        }
        else {
            this.out.write(this.streamCipher.returnByte((byte)n));
        }
    }
    
    @Override
    public void write(final byte[] array) throws IOException {
        this.write(array, 0, array.length);
    }
    
    @Override
    public void write(final byte[] array, final int n, final int len) throws IOException {
        if (this.bufferedBlockCipher != null) {
            final byte[] b = new byte[this.bufferedBlockCipher.getOutputSize(len)];
            final int processBytes = this.bufferedBlockCipher.processBytes(array, n, len, b, 0);
            if (processBytes != 0) {
                this.out.write(b, 0, processBytes);
            }
        }
        else {
            final byte[] b2 = new byte[len];
            this.streamCipher.processBytes(array, n, len, b2, 0);
            this.out.write(b2, 0, len);
        }
    }
    
    @Override
    public void flush() throws IOException {
        super.flush();
    }
    
    @Override
    public void close() throws IOException {
        try {
            if (this.bufferedBlockCipher != null) {
                final byte[] b = new byte[this.bufferedBlockCipher.getOutputSize(0)];
                final int doFinal = this.bufferedBlockCipher.doFinal(b, 0);
                if (doFinal != 0) {
                    this.out.write(b, 0, doFinal);
                }
            }
        }
        catch (Exception ex) {
            throw new IOException("Error closing stream: " + ex.toString());
        }
        this.flush();
        super.close();
    }
}
