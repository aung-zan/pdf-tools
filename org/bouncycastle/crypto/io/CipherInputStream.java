// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.io;

import java.io.IOException;
import java.io.InputStream;
import org.bouncycastle.crypto.StreamCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import java.io.FilterInputStream;

public class CipherInputStream extends FilterInputStream
{
    private BufferedBlockCipher bufferedBlockCipher;
    private StreamCipher streamCipher;
    private byte[] buf;
    private byte[] inBuf;
    private int bufOff;
    private int maxBuf;
    private boolean finalized;
    private static final int INPUT_BUF_SIZE = 2048;
    
    public CipherInputStream(final InputStream in, final BufferedBlockCipher bufferedBlockCipher) {
        super(in);
        this.bufferedBlockCipher = bufferedBlockCipher;
        this.buf = new byte[bufferedBlockCipher.getOutputSize(2048)];
        this.inBuf = new byte[2048];
    }
    
    public CipherInputStream(final InputStream in, final StreamCipher streamCipher) {
        super(in);
        this.streamCipher = streamCipher;
        this.buf = new byte[2048];
        this.inBuf = new byte[2048];
    }
    
    private int nextChunk() throws IOException {
        int available = super.available();
        if (available <= 0) {
            available = 1;
        }
        int maxBuf;
        if (available > this.inBuf.length) {
            maxBuf = super.read(this.inBuf, 0, this.inBuf.length);
        }
        else {
            maxBuf = super.read(this.inBuf, 0, available);
        }
        if (maxBuf < 0) {
            if (this.finalized) {
                return -1;
            }
            try {
                if (this.bufferedBlockCipher != null) {
                    this.maxBuf = this.bufferedBlockCipher.doFinal(this.buf, 0);
                }
                else {
                    this.maxBuf = 0;
                }
            }
            catch (Exception ex) {
                throw new IOException("error processing stream: " + ex.toString());
            }
            this.bufOff = 0;
            this.finalized = true;
            if (this.bufOff == this.maxBuf) {
                return -1;
            }
        }
        else {
            this.bufOff = 0;
            try {
                if (this.bufferedBlockCipher != null) {
                    this.maxBuf = this.bufferedBlockCipher.processBytes(this.inBuf, 0, maxBuf, this.buf, 0);
                }
                else {
                    this.streamCipher.processBytes(this.inBuf, 0, maxBuf, this.buf, 0);
                    this.maxBuf = maxBuf;
                }
            }
            catch (Exception ex2) {
                throw new IOException("error processing stream: " + ex2.toString());
            }
            if (this.maxBuf == 0) {
                return this.nextChunk();
            }
        }
        return this.maxBuf;
    }
    
    @Override
    public int read() throws IOException {
        if (this.bufOff == this.maxBuf && this.nextChunk() < 0) {
            return -1;
        }
        return this.buf[this.bufOff++] & 0xFF;
    }
    
    @Override
    public int read(final byte[] array) throws IOException {
        return this.read(array, 0, array.length);
    }
    
    @Override
    public int read(final byte[] array, final int n, final int n2) throws IOException {
        if (this.bufOff == this.maxBuf && this.nextChunk() < 0) {
            return -1;
        }
        final int n3 = this.maxBuf - this.bufOff;
        if (n2 > n3) {
            System.arraycopy(this.buf, this.bufOff, array, n, n3);
            this.bufOff = this.maxBuf;
            return n3;
        }
        System.arraycopy(this.buf, this.bufOff, array, n, n2);
        this.bufOff += n2;
        return n2;
    }
    
    @Override
    public long skip(final long n) throws IOException {
        if (n <= 0L) {
            return 0L;
        }
        final int n2 = this.maxBuf - this.bufOff;
        if (n > n2) {
            this.bufOff = this.maxBuf;
            return n2;
        }
        this.bufOff += (int)n;
        return (int)n;
    }
    
    @Override
    public int available() throws IOException {
        return this.maxBuf - this.bufOff;
    }
    
    @Override
    public void close() throws IOException {
        super.close();
    }
    
    @Override
    public boolean markSupported() {
        return false;
    }
}
