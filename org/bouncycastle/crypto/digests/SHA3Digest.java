// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.digests;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.ExtendedDigest;

public class SHA3Digest implements ExtendedDigest
{
    private static long[] KeccakRoundConstants;
    private static int[] KeccakRhoOffsets;
    private byte[] state;
    private byte[] dataQueue;
    private int rate;
    private int bitsInQueue;
    private int fixedOutputLength;
    private boolean squeezing;
    private int bitsAvailableForSqueezing;
    private byte[] chunk;
    private byte[] oneByte;
    long[] C;
    long[] tempA;
    long[] chiC;
    
    private static long[] keccakInitializeRoundConstants() {
        final long[] array = new long[24];
        final byte[] array2 = { 1 };
        for (int i = 0; i < 24; ++i) {
            array[i] = 0L;
            for (int j = 0; j < 7; ++j) {
                final int n = (1 << j) - 1;
                if (LFSR86540(array2)) {
                    final long[] array3 = array;
                    final int n2 = i;
                    array3[n2] ^= 1L << n;
                }
            }
        }
        return array;
    }
    
    private static boolean LFSR86540(final byte[] array) {
        final boolean b = (array[0] & 0x1) != 0x0;
        if ((array[0] & 0x80) != 0x0) {
            array[0] = (byte)(array[0] << 1 ^ 0x71);
        }
        else {
            final int n = 0;
            array[n] <<= 1;
        }
        return b;
    }
    
    private static int[] keccakInitializeRhoOffsets() {
        final int[] array = new int[25];
        array[0] = 0;
        int n = 1;
        int n2 = 0;
        for (int i = 0; i < 24; ++i) {
            array[n % 5 + 5 * (n2 % 5)] = (i + 1) * (i + 2) / 2 % 64;
            final int n3 = (0 * n + 1 * n2) % 5;
            final int n4 = (2 * n + 3 * n2) % 5;
            n = n3;
            n2 = n4;
        }
        return array;
    }
    
    private void clearDataQueueSection(final int n, final int n2) {
        for (int i = n; i != n + n2; ++i) {
            this.dataQueue[i] = 0;
        }
    }
    
    public SHA3Digest() {
        this.state = new byte[200];
        this.dataQueue = new byte[192];
        this.C = new long[5];
        this.tempA = new long[25];
        this.chiC = new long[5];
        this.init(0);
    }
    
    public SHA3Digest(final int n) {
        this.state = new byte[200];
        this.dataQueue = new byte[192];
        this.C = new long[5];
        this.tempA = new long[25];
        this.chiC = new long[5];
        this.init(n);
    }
    
    public SHA3Digest(final SHA3Digest sha3Digest) {
        this.state = new byte[200];
        this.dataQueue = new byte[192];
        this.C = new long[5];
        this.tempA = new long[25];
        this.chiC = new long[5];
        System.arraycopy(sha3Digest.state, 0, this.state, 0, sha3Digest.state.length);
        System.arraycopy(sha3Digest.dataQueue, 0, this.dataQueue, 0, sha3Digest.dataQueue.length);
        this.rate = sha3Digest.rate;
        this.bitsInQueue = sha3Digest.bitsInQueue;
        this.fixedOutputLength = sha3Digest.fixedOutputLength;
        this.squeezing = sha3Digest.squeezing;
        this.bitsAvailableForSqueezing = sha3Digest.bitsAvailableForSqueezing;
        this.chunk = Arrays.clone(sha3Digest.chunk);
        this.oneByte = Arrays.clone(sha3Digest.oneByte);
    }
    
    public String getAlgorithmName() {
        return "SHA3-" + this.fixedOutputLength;
    }
    
    public int getDigestSize() {
        return this.fixedOutputLength / 8;
    }
    
    public void update(final byte b) {
        this.oneByte[0] = b;
        this.doUpdate(this.oneByte, 0, 8L);
    }
    
    public void update(final byte[] array, final int n, final int n2) {
        this.doUpdate(array, n, n2 * 8L);
    }
    
    public int doFinal(final byte[] array, final int n) {
        this.squeeze(array, n, this.fixedOutputLength);
        this.reset();
        return this.getDigestSize();
    }
    
    public void reset() {
        this.init(this.fixedOutputLength);
    }
    
    public int getByteLength() {
        return this.rate / 8;
    }
    
    private void init(final int n) {
        switch (n) {
            case 0:
            case 288: {
                this.initSponge(1024, 576);
                break;
            }
            case 224: {
                this.initSponge(1152, 448);
                break;
            }
            case 256: {
                this.initSponge(1088, 512);
                break;
            }
            case 384: {
                this.initSponge(832, 768);
                break;
            }
            case 512: {
                this.initSponge(576, 1024);
                break;
            }
            default: {
                throw new IllegalArgumentException("bitLength must be one of 224, 256, 384, or 512.");
            }
        }
    }
    
    private void doUpdate(final byte[] array, final int n, final long n2) {
        if (n2 % 8L == 0L) {
            this.absorb(array, n, n2);
        }
        else {
            this.absorb(array, n, n2 - n2 % 8L);
            this.absorb(new byte[] { (byte)(array[n + (int)(n2 / 8L)] >> (int)(8L - n2 % 8L)) }, n, n2 % 8L);
        }
    }
    
    private void initSponge(final int rate, final int n) {
        if (rate + n != 1600) {
            throw new IllegalStateException("rate + capacity != 1600");
        }
        if (rate <= 0 || rate >= 1600 || rate % 64 != 0) {
            throw new IllegalStateException("invalid rate value");
        }
        this.rate = rate;
        this.fixedOutputLength = 0;
        Arrays.fill(this.state, (byte)0);
        Arrays.fill(this.dataQueue, (byte)0);
        this.bitsInQueue = 0;
        this.squeezing = false;
        this.bitsAvailableForSqueezing = 0;
        this.fixedOutputLength = n / 2;
        this.chunk = new byte[rate / 8];
        this.oneByte = new byte[1];
    }
    
    private void absorbQueue() {
        this.KeccakAbsorb(this.state, this.dataQueue, this.rate / 8);
        this.bitsInQueue = 0;
    }
    
    private void absorb(final byte[] array, final int n, final long n2) {
        if (this.bitsInQueue % 8 != 0) {
            throw new IllegalStateException("attempt to absorb with odd length queue.");
        }
        if (this.squeezing) {
            throw new IllegalStateException("attempt to absorb while squeezing.");
        }
        long n3 = 0L;
        while (n3 < n2) {
            if (this.bitsInQueue == 0 && n2 >= this.rate && n3 <= n2 - this.rate) {
                final long n4 = (n2 - n3) / this.rate;
                for (long n5 = 0L; n5 < n4; ++n5) {
                    System.arraycopy(array, (int)(n + n3 / 8L + n5 * this.chunk.length), this.chunk, 0, this.chunk.length);
                    this.KeccakAbsorb(this.state, this.chunk, this.chunk.length);
                }
                n3 += n4 * this.rate;
            }
            else {
                int n6 = (int)(n2 - n3);
                if (n6 + this.bitsInQueue > this.rate) {
                    n6 = this.rate - this.bitsInQueue;
                }
                final int n7 = n6 % 8;
                final int n8 = n6 - n7;
                System.arraycopy(array, n + (int)(n3 / 8L), this.dataQueue, this.bitsInQueue / 8, n8 / 8);
                this.bitsInQueue += n8;
                n3 += n8;
                if (this.bitsInQueue == this.rate) {
                    this.absorbQueue();
                }
                if (n7 <= 0) {
                    continue;
                }
                this.dataQueue[this.bitsInQueue / 8] = (byte)(array[n + (int)(n3 / 8L)] & (1 << n7) - 1);
                this.bitsInQueue += n7;
                n3 += n7;
            }
        }
    }
    
    private void padAndSwitchToSqueezingPhase() {
        if (this.bitsInQueue + 1 == this.rate) {
            final byte[] dataQueue = this.dataQueue;
            final int n = this.bitsInQueue / 8;
            dataQueue[n] |= (byte)(1 << this.bitsInQueue % 8);
            this.absorbQueue();
            this.clearDataQueueSection(0, this.rate / 8);
        }
        else {
            this.clearDataQueueSection((this.bitsInQueue + 7) / 8, this.rate / 8 - (this.bitsInQueue + 7) / 8);
            final byte[] dataQueue2 = this.dataQueue;
            final int n2 = this.bitsInQueue / 8;
            dataQueue2[n2] |= (byte)(1 << this.bitsInQueue % 8);
        }
        final byte[] dataQueue3 = this.dataQueue;
        final int n3 = (this.rate - 1) / 8;
        dataQueue3[n3] |= (byte)(1 << (this.rate - 1) % 8);
        this.absorbQueue();
        if (this.rate == 1024) {
            this.KeccakExtract1024bits(this.state, this.dataQueue);
            this.bitsAvailableForSqueezing = 1024;
        }
        else {
            this.KeccakExtract(this.state, this.dataQueue, this.rate / 64);
            this.bitsAvailableForSqueezing = this.rate;
        }
        this.squeezing = true;
    }
    
    private void squeeze(final byte[] array, final int n, final long n2) {
        if (!this.squeezing) {
            this.padAndSwitchToSqueezingPhase();
        }
        if (n2 % 8L != 0L) {
            throw new IllegalStateException("outputLength not a multiple of 8");
        }
        int bitsAvailableForSqueezing;
        for (long n3 = 0L; n3 < n2; n3 += bitsAvailableForSqueezing) {
            if (this.bitsAvailableForSqueezing == 0) {
                this.keccakPermutation(this.state);
                if (this.rate == 1024) {
                    this.KeccakExtract1024bits(this.state, this.dataQueue);
                    this.bitsAvailableForSqueezing = 1024;
                }
                else {
                    this.KeccakExtract(this.state, this.dataQueue, this.rate / 64);
                    this.bitsAvailableForSqueezing = this.rate;
                }
            }
            bitsAvailableForSqueezing = this.bitsAvailableForSqueezing;
            if (bitsAvailableForSqueezing > n2 - n3) {
                bitsAvailableForSqueezing = (int)(n2 - n3);
            }
            System.arraycopy(this.dataQueue, (this.rate - this.bitsAvailableForSqueezing) / 8, array, n + (int)(n3 / 8L), bitsAvailableForSqueezing / 8);
            this.bitsAvailableForSqueezing -= bitsAvailableForSqueezing;
        }
    }
    
    private void fromBytesToWords(final long[] array, final byte[] array2) {
        for (int i = 0; i < 25; ++i) {
            array[i] = 0L;
            final int n = i * 8;
            for (int j = 0; j < 8; ++j) {
                final int n2 = i;
                array[n2] |= ((long)array2[n + j] & 0xFFL) << 8 * j;
            }
        }
    }
    
    private void fromWordsToBytes(final byte[] array, final long[] array2) {
        for (int i = 0; i < 25; ++i) {
            final int n = i * 8;
            for (int j = 0; j < 8; ++j) {
                array[n + j] = (byte)(array2[i] >>> 8 * j & 0xFFL);
            }
        }
    }
    
    private void keccakPermutation(final byte[] array) {
        final long[] array2 = new long[array.length / 8];
        this.fromBytesToWords(array2, array);
        this.keccakPermutationOnWords(array2);
        this.fromWordsToBytes(array, array2);
    }
    
    private void keccakPermutationAfterXor(final byte[] array, final byte[] array2, final int n) {
        for (int i = 0; i < n; ++i) {
            final int n2 = i;
            array[n2] ^= array2[i];
        }
        this.keccakPermutation(array);
    }
    
    private void keccakPermutationOnWords(final long[] array) {
        for (int i = 0; i < 24; ++i) {
            this.theta(array);
            this.rho(array);
            this.pi(array);
            this.chi(array);
            this.iota(array, i);
        }
    }
    
    private void theta(final long[] array) {
        for (int i = 0; i < 5; ++i) {
            this.C[i] = 0L;
            for (int j = 0; j < 5; ++j) {
                final long[] c = this.C;
                final int n = i;
                c[n] ^= array[i + 5 * j];
            }
        }
        for (int k = 0; k < 5; ++k) {
            final long n2 = this.C[(k + 1) % 5] << 1 ^ this.C[(k + 1) % 5] >>> 63 ^ this.C[(k + 4) % 5];
            for (int l = 0; l < 5; ++l) {
                final int n3 = k + 5 * l;
                array[n3] ^= n2;
            }
        }
    }
    
    private void rho(final long[] array) {
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                final int n = i + 5 * j;
                array[n] = ((SHA3Digest.KeccakRhoOffsets[n] != 0) ? (array[n] << SHA3Digest.KeccakRhoOffsets[n] ^ array[n] >>> 64 - SHA3Digest.KeccakRhoOffsets[n]) : array[n]);
            }
        }
    }
    
    private void pi(final long[] array) {
        System.arraycopy(array, 0, this.tempA, 0, this.tempA.length);
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                array[j + 5 * ((2 * i + 3 * j) % 5)] = this.tempA[i + 5 * j];
            }
        }
    }
    
    private void chi(final long[] array) {
        for (int i = 0; i < 5; ++i) {
            for (int j = 0; j < 5; ++j) {
                this.chiC[j] = (array[j + 5 * i] ^ (~array[(j + 1) % 5 + 5 * i] & array[(j + 2) % 5 + 5 * i]));
            }
            for (int k = 0; k < 5; ++k) {
                array[k + 5 * i] = this.chiC[k];
            }
        }
    }
    
    private void iota(final long[] array, final int n) {
        final int n2 = 0;
        array[n2] ^= SHA3Digest.KeccakRoundConstants[n];
    }
    
    private void KeccakAbsorb(final byte[] array, final byte[] array2, final int n) {
        this.keccakPermutationAfterXor(array, array2, n);
    }
    
    private void KeccakExtract1024bits(final byte[] array, final byte[] array2) {
        System.arraycopy(array, 0, array2, 0, 128);
    }
    
    private void KeccakExtract(final byte[] array, final byte[] array2, final int n) {
        System.arraycopy(array, 0, array2, 0, n * 8);
    }
    
    static {
        SHA3Digest.KeccakRoundConstants = keccakInitializeRoundConstants();
        SHA3Digest.KeccakRhoOffsets = keccakInitializeRhoOffsets();
    }
}
