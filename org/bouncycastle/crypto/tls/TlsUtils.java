// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.util.Integers;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.DSAPublicKeyParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.digests.SHA384Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.digests.SHA224Digest;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.util.Strings;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Hashtable;
import java.util.Vector;
import org.bouncycastle.util.io.Streams;
import java.io.EOFException;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;

public class TlsUtils
{
    public static byte[] EMPTY_BYTES;
    public static final Integer EXT_signature_algorithms;
    static final byte[] SSL_CLIENT;
    static final byte[] SSL_SERVER;
    static final byte[][] SSL3_CONST;
    
    public static boolean isValidUint8(final short n) {
        return (n & 0xFF) == n;
    }
    
    public static boolean isValidUint16(final int n) {
        return (n & 0xFFFF) == n;
    }
    
    public static boolean isValidUint24(final int n) {
        return (n & 0xFFFFFF) == n;
    }
    
    public static boolean isValidUint32(final long n) {
        return (n & 0xFFFFFFFFL) == n;
    }
    
    public static boolean isValidUint48(final long n) {
        return (n & 0xFFFFFFFFFFFFL) == n;
    }
    
    public static boolean isValidUint64(final long n) {
        return true;
    }
    
    public static void writeUint8(final short n, final OutputStream outputStream) throws IOException {
        outputStream.write(n);
    }
    
    public static void writeUint8(final short n, final byte[] array, final int n2) {
        array[n2] = (byte)n;
    }
    
    public static void writeUint16(final int n, final OutputStream outputStream) throws IOException {
        outputStream.write(n >> 8);
        outputStream.write(n);
    }
    
    public static void writeUint16(final int n, final byte[] array, final int n2) {
        array[n2] = (byte)(n >> 8);
        array[n2 + 1] = (byte)n;
    }
    
    public static void writeUint24(final int n, final OutputStream outputStream) throws IOException {
        outputStream.write(n >> 16);
        outputStream.write(n >> 8);
        outputStream.write(n);
    }
    
    public static void writeUint24(final int n, final byte[] array, final int n2) {
        array[n2] = (byte)(n >> 16);
        array[n2 + 1] = (byte)(n >> 8);
        array[n2 + 2] = (byte)n;
    }
    
    public static void writeUint32(final long n, final OutputStream outputStream) throws IOException {
        outputStream.write((int)(n >> 24));
        outputStream.write((int)(n >> 16));
        outputStream.write((int)(n >> 8));
        outputStream.write((int)n);
    }
    
    public static void writeUint32(final long n, final byte[] array, final int n2) {
        array[n2] = (byte)(n >> 24);
        array[n2 + 1] = (byte)(n >> 16);
        array[n2 + 2] = (byte)(n >> 8);
        array[n2 + 3] = (byte)n;
    }
    
    public static void writeUint48(final long n, final byte[] array, final int n2) {
        array[n2] = (byte)(n >> 40);
        array[n2 + 1] = (byte)(n >> 32);
        array[n2 + 2] = (byte)(n >> 24);
        array[n2 + 3] = (byte)(n >> 16);
        array[n2 + 4] = (byte)(n >> 8);
        array[n2 + 5] = (byte)n;
    }
    
    public static void writeUint64(final long n, final OutputStream outputStream) throws IOException {
        outputStream.write((int)(n >> 56));
        outputStream.write((int)(n >> 48));
        outputStream.write((int)(n >> 40));
        outputStream.write((int)(n >> 32));
        outputStream.write((int)(n >> 24));
        outputStream.write((int)(n >> 16));
        outputStream.write((int)(n >> 8));
        outputStream.write((int)n);
    }
    
    public static void writeUint64(final long n, final byte[] array, final int n2) {
        array[n2] = (byte)(n >> 56);
        array[n2 + 1] = (byte)(n >> 48);
        array[n2 + 2] = (byte)(n >> 40);
        array[n2 + 3] = (byte)(n >> 32);
        array[n2 + 4] = (byte)(n >> 24);
        array[n2 + 5] = (byte)(n >> 16);
        array[n2 + 6] = (byte)(n >> 8);
        array[n2 + 7] = (byte)n;
    }
    
    public static void writeOpaque8(final byte[] b, final OutputStream outputStream) throws IOException {
        writeUint8((short)b.length, outputStream);
        outputStream.write(b);
    }
    
    public static void writeOpaque16(final byte[] b, final OutputStream outputStream) throws IOException {
        writeUint16(b.length, outputStream);
        outputStream.write(b);
    }
    
    public static void writeOpaque24(final byte[] b, final OutputStream outputStream) throws IOException {
        writeUint24(b.length, outputStream);
        outputStream.write(b);
    }
    
    public static void writeUint8Array(final short[] array, final OutputStream outputStream) throws IOException {
        for (int i = 0; i < array.length; ++i) {
            writeUint8(array[i], outputStream);
        }
    }
    
    public static void writeUint16Array(final int[] array, final OutputStream outputStream) throws IOException {
        for (int i = 0; i < array.length; ++i) {
            writeUint16(array[i], outputStream);
        }
    }
    
    public static short readUint8(final InputStream inputStream) throws IOException {
        final int read = inputStream.read();
        if (read < 0) {
            throw new EOFException();
        }
        return (short)read;
    }
    
    public static short readUint8(final byte[] array, final int n) {
        return array[n];
    }
    
    public static int readUint16(final InputStream inputStream) throws IOException {
        final int read = inputStream.read();
        final int read2 = inputStream.read();
        if (read2 < 0) {
            throw new EOFException();
        }
        return read << 8 | read2;
    }
    
    public static int readUint16(final byte[] array, int n) {
        return (array[n] & 0xFF) << 8 | (array[++n] & 0xFF);
    }
    
    public static int readUint24(final InputStream inputStream) throws IOException {
        final int read = inputStream.read();
        final int read2 = inputStream.read();
        final int read3 = inputStream.read();
        if (read3 < 0) {
            throw new EOFException();
        }
        return read << 16 | read2 << 8 | read3;
    }
    
    public static int readUint24(final byte[] array, int n) {
        return (array[n] & 0xFF) << 16 | (array[++n] & 0xFF) << 8 | (array[++n] & 0xFF);
    }
    
    public static long readUint32(final InputStream inputStream) throws IOException {
        final int read = inputStream.read();
        final int read2 = inputStream.read();
        final int read3 = inputStream.read();
        final int read4 = inputStream.read();
        if (read4 < 0) {
            throw new EOFException();
        }
        return (long)read << 24 | (long)read2 << 16 | (long)read3 << 8 | (long)read4;
    }
    
    public static long readUint48(final InputStream inputStream) throws IOException {
        final int read = inputStream.read();
        final int read2 = inputStream.read();
        final int read3 = inputStream.read();
        final int read4 = inputStream.read();
        final int read5 = inputStream.read();
        final int read6 = inputStream.read();
        if (read6 < 0) {
            throw new EOFException();
        }
        return (long)read << 40 | (long)read2 << 32 | (long)read3 << 24 | (long)read4 << 16 | (long)read5 << 8 | (long)read6;
    }
    
    public static long readUint48(final byte[] array, final int n) {
        return ((long)readUint24(array, n) & 0xFFFFFFFFL) << 24 | ((long)readUint24(array, n + 3) & 0xFFFFFFFFL);
    }
    
    public static byte[] readFully(final int n, final InputStream inputStream) throws IOException {
        if (n < 1) {
            return TlsUtils.EMPTY_BYTES;
        }
        final byte[] array = new byte[n];
        if (n != Streams.readFully(inputStream, array)) {
            throw new EOFException();
        }
        return array;
    }
    
    public static void readFully(final byte[] array, final InputStream inputStream) throws IOException {
        final int length = array.length;
        if (length > 0 && length != Streams.readFully(inputStream, array)) {
            throw new EOFException();
        }
    }
    
    public static byte[] readOpaque8(final InputStream inputStream) throws IOException {
        return readFully(readUint8(inputStream), inputStream);
    }
    
    public static byte[] readOpaque16(final InputStream inputStream) throws IOException {
        return readFully(readUint16(inputStream), inputStream);
    }
    
    public static byte[] readOpaque24(final InputStream inputStream) throws IOException {
        return readFully(readUint24(inputStream), inputStream);
    }
    
    public static short[] readUint8Array(final int n, final InputStream inputStream) throws IOException {
        final short[] array = new short[n];
        for (int i = 0; i < n; ++i) {
            array[i] = readUint8(inputStream);
        }
        return array;
    }
    
    public static int[] readUint16Array(final int n, final InputStream inputStream) throws IOException {
        final int[] array = new int[n];
        for (int i = 0; i < n; ++i) {
            array[i] = readUint16(inputStream);
        }
        return array;
    }
    
    public static ProtocolVersion readVersion(final byte[] array, final int n) throws IOException {
        return ProtocolVersion.get(array[n] & 0xFF, array[n + 1] & 0xFF);
    }
    
    public static ProtocolVersion readVersion(final InputStream inputStream) throws IOException {
        final int read = inputStream.read();
        final int read2 = inputStream.read();
        if (read2 < 0) {
            throw new EOFException();
        }
        return ProtocolVersion.get(read, read2);
    }
    
    public static int readVersionRaw(final InputStream inputStream) throws IOException {
        final int read = inputStream.read();
        final int read2 = inputStream.read();
        if (read2 < 0) {
            throw new EOFException();
        }
        return read << 8 | read2;
    }
    
    public static void writeGMTUnixTime(final byte[] array, final int n) {
        final int n2 = (int)(System.currentTimeMillis() / 1000L);
        array[n] = (byte)(n2 >> 24);
        array[n + 1] = (byte)(n2 >> 16);
        array[n + 2] = (byte)(n2 >> 8);
        array[n + 3] = (byte)n2;
    }
    
    public static void writeVersion(final ProtocolVersion protocolVersion, final OutputStream outputStream) throws IOException {
        outputStream.write(protocolVersion.getMajorVersion());
        outputStream.write(protocolVersion.getMinorVersion());
    }
    
    public static void writeVersion(final ProtocolVersion protocolVersion, final byte[] array, final int n) throws IOException {
        array[n] = (byte)protocolVersion.getMajorVersion();
        array[n + 1] = (byte)protocolVersion.getMinorVersion();
    }
    
    public static Vector getDefaultDSSSignatureAlgorithms() {
        return vectorOfOne(new SignatureAndHashAlgorithm((short)2, (short)2));
    }
    
    public static Vector getDefaultECDSASignatureAlgorithms() {
        return vectorOfOne(new SignatureAndHashAlgorithm((short)2, (short)3));
    }
    
    public static Vector getDefaultRSASignatureAlgorithms() {
        return vectorOfOne(new SignatureAndHashAlgorithm((short)2, (short)1));
    }
    
    public static boolean isSignatureAlgorithmsExtensionAllowed(final ProtocolVersion protocolVersion) {
        return ProtocolVersion.TLSv12.isEqualOrEarlierVersionOf(protocolVersion.getEquivalentTLSVersion());
    }
    
    public static void addSignatureAlgorithmsExtension(final Hashtable hashtable, final Vector vector) throws IOException {
        hashtable.put(TlsUtils.EXT_signature_algorithms, createSignatureAlgorithmsExtension(vector));
    }
    
    public static Vector getSignatureAlgorithmsExtension(final Hashtable hashtable) throws IOException {
        if (hashtable == null) {
            return null;
        }
        final byte[] array = hashtable.get(TlsUtils.EXT_signature_algorithms);
        if (array == null) {
            return null;
        }
        return readSignatureAlgorithmsExtension(array);
    }
    
    public static byte[] createSignatureAlgorithmsExtension(final Vector vector) throws IOException {
        if (vector == null || vector.size() < 1 || vector.size() >= 32768) {
            throw new IllegalArgumentException("'supportedSignatureAlgorithms' must have length from 1 to (2^15 - 1)");
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        writeUint16(2 * vector.size(), byteArrayOutputStream);
        for (int i = 0; i < vector.size(); ++i) {
            vector.elementAt(i).encode(byteArrayOutputStream);
        }
        return byteArrayOutputStream.toByteArray();
    }
    
    public static Vector readSignatureAlgorithmsExtension(final byte[] buf) throws IOException {
        if (buf == null) {
            throw new IllegalArgumentException("'extensionValue' cannot be null");
        }
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        final int uint16 = readUint16(byteArrayInputStream);
        if (uint16 < 2 || (uint16 & 0x1) != 0x0) {
            throw new TlsFatalAlert((short)50);
        }
        final int initialCapacity = uint16 / 2;
        final Vector vector = new Vector<SignatureAndHashAlgorithm>(initialCapacity);
        for (int i = 0; i < initialCapacity; ++i) {
            vector.addElement(SignatureAndHashAlgorithm.parse(byteArrayInputStream));
        }
        TlsProtocol.assertEmpty(byteArrayInputStream);
        return vector;
    }
    
    public static byte[] PRF(final TlsContext tlsContext, final byte[] array, final String s, final byte[] array2, final int n) {
        final ProtocolVersion serverVersion = tlsContext.getServerVersion();
        if (serverVersion.isSSL()) {
            throw new IllegalStateException("No PRF available for SSLv3 session");
        }
        final byte[] byteArray = Strings.toByteArray(s);
        final byte[] concat = concat(byteArray, array2);
        int prfAlgorithm = tlsContext.getSecurityParameters().getPrfAlgorithm();
        if (prfAlgorithm == 0) {
            if (!ProtocolVersion.TLSv12.isEqualOrEarlierVersionOf(serverVersion.getEquivalentTLSVersion())) {
                return PRF_legacy(array, byteArray, concat, n);
            }
            prfAlgorithm = 1;
        }
        final Digest prfHash = createPRFHash(prfAlgorithm);
        final byte[] array3 = new byte[n];
        hmac_hash(prfHash, array, concat, array3);
        return array3;
    }
    
    static byte[] PRF_legacy(final byte[] array, final byte[] array2, final byte[] array3, final int n) {
        final int n2 = (array.length + 1) / 2;
        final byte[] array4 = new byte[n2];
        final byte[] array5 = new byte[n2];
        System.arraycopy(array, 0, array4, 0, n2);
        System.arraycopy(array, array.length - n2, array5, 0, n2);
        final byte[] array6 = new byte[n];
        final byte[] array7 = new byte[n];
        hmac_hash(new MD5Digest(), array4, array3, array6);
        hmac_hash(new SHA1Digest(), array5, array3, array7);
        for (int i = 0; i < n; ++i) {
            final byte[] array8 = array6;
            final int n3 = i;
            array8[n3] ^= array7[i];
        }
        return array6;
    }
    
    static byte[] concat(final byte[] array, final byte[] array2) {
        final byte[] array3 = new byte[array.length + array2.length];
        System.arraycopy(array, 0, array3, 0, array.length);
        System.arraycopy(array2, 0, array3, array.length, array2.length);
        return array3;
    }
    
    static void hmac_hash(final Digest digest, final byte[] array, final byte[] array2, final byte[] array3) {
        final HMac hMac = new HMac(digest);
        final KeyParameter keyParameter = new KeyParameter(array);
        byte[] array4 = array2;
        final int digestSize = digest.getDigestSize();
        final int n = (array3.length + digestSize - 1) / digestSize;
        final byte[] array5 = new byte[hMac.getMacSize()];
        final byte[] array6 = new byte[hMac.getMacSize()];
        for (int i = 0; i < n; ++i) {
            hMac.init(keyParameter);
            hMac.update(array4, 0, array4.length);
            hMac.doFinal(array5, 0);
            array4 = array5;
            hMac.init(keyParameter);
            hMac.update(array4, 0, array4.length);
            hMac.update(array2, 0, array2.length);
            hMac.doFinal(array6, 0);
            System.arraycopy(array6, 0, array3, digestSize * i, Math.min(digestSize, array3.length - digestSize * i));
        }
    }
    
    static void validateKeyUsage(final Certificate certificate, final int n) throws IOException {
        final Extensions extensions = certificate.getTBSCertificate().getExtensions();
        if (extensions != null) {
            final KeyUsage fromExtensions = KeyUsage.fromExtensions(extensions);
            if (fromExtensions != null && (fromExtensions.getBytes()[0] & 0xFF & n) != n) {
                throw new TlsFatalAlert((short)46);
            }
        }
    }
    
    static byte[] calculateKeyBlock(final TlsContext tlsContext, final int n) {
        final SecurityParameters securityParameters = tlsContext.getSecurityParameters();
        final byte[] masterSecret = securityParameters.getMasterSecret();
        final byte[] concat = concat(securityParameters.getServerRandom(), securityParameters.getClientRandom());
        if (tlsContext.getServerVersion().isSSL()) {
            return calculateKeyBlock_SSL(masterSecret, concat, n);
        }
        return PRF(tlsContext, masterSecret, "key expansion", concat, n);
    }
    
    static byte[] calculateKeyBlock_SSL(final byte[] array, final byte[] array2, final int n) {
        final MD5Digest md5Digest = new MD5Digest();
        final SHA1Digest sha1Digest = new SHA1Digest();
        final int digestSize = md5Digest.getDigestSize();
        final byte[] array3 = new byte[sha1Digest.getDigestSize()];
        final byte[] array4 = new byte[n + digestSize];
        for (int n2 = 0, i = 0; i < n; i += digestSize, ++n2) {
            final byte[] array5 = TlsUtils.SSL3_CONST[n2];
            sha1Digest.update(array5, 0, array5.length);
            sha1Digest.update(array, 0, array.length);
            sha1Digest.update(array2, 0, array2.length);
            sha1Digest.doFinal(array3, 0);
            md5Digest.update(array, 0, array.length);
            md5Digest.update(array3, 0, array3.length);
            md5Digest.doFinal(array4, i);
        }
        final byte[] array6 = new byte[n];
        System.arraycopy(array4, 0, array6, 0, n);
        return array6;
    }
    
    static byte[] calculateMasterSecret(final TlsContext tlsContext, final byte[] array) {
        final SecurityParameters securityParameters = tlsContext.getSecurityParameters();
        final byte[] concat = concat(securityParameters.getClientRandom(), securityParameters.getServerRandom());
        if (tlsContext.getServerVersion().isSSL()) {
            return calculateMasterSecret_SSL(array, concat);
        }
        return PRF(tlsContext, array, "master secret", concat, 48);
    }
    
    static byte[] calculateMasterSecret_SSL(final byte[] array, final byte[] array2) {
        final MD5Digest md5Digest = new MD5Digest();
        final SHA1Digest sha1Digest = new SHA1Digest();
        final int digestSize = md5Digest.getDigestSize();
        final byte[] array3 = new byte[sha1Digest.getDigestSize()];
        final byte[] array4 = new byte[digestSize * 3];
        int n = 0;
        for (int i = 0; i < 3; ++i) {
            final byte[] array5 = TlsUtils.SSL3_CONST[i];
            sha1Digest.update(array5, 0, array5.length);
            sha1Digest.update(array, 0, array.length);
            sha1Digest.update(array2, 0, array2.length);
            sha1Digest.doFinal(array3, 0);
            md5Digest.update(array, 0, array.length);
            md5Digest.update(array3, 0, array3.length);
            md5Digest.doFinal(array4, n);
            n += digestSize;
        }
        return array4;
    }
    
    static byte[] calculateVerifyData(final TlsContext tlsContext, final String s, final byte[] array) {
        if (tlsContext.getServerVersion().isSSL()) {
            return array;
        }
        final SecurityParameters securityParameters = tlsContext.getSecurityParameters();
        return PRF(tlsContext, securityParameters.getMasterSecret(), s, array, securityParameters.getVerifyDataLength());
    }
    
    public static final Digest createHash(final int n) {
        switch (n) {
            case 1: {
                return new MD5Digest();
            }
            case 2: {
                return new SHA1Digest();
            }
            case 3: {
                return new SHA224Digest();
            }
            case 4: {
                return new SHA256Digest();
            }
            case 5: {
                return new SHA384Digest();
            }
            case 6: {
                return new SHA512Digest();
            }
            default: {
                throw new IllegalArgumentException("unknown HashAlgorithm");
            }
        }
    }
    
    public static final Digest cloneHash(final int n, final Digest digest) {
        switch (n) {
            case 1: {
                return new MD5Digest((MD5Digest)digest);
            }
            case 2: {
                return new SHA1Digest((SHA1Digest)digest);
            }
            case 3: {
                return new SHA224Digest((SHA224Digest)digest);
            }
            case 4: {
                return new SHA256Digest((SHA256Digest)digest);
            }
            case 5: {
                return new SHA384Digest((SHA384Digest)digest);
            }
            case 6: {
                return new SHA512Digest((SHA512Digest)digest);
            }
            default: {
                throw new IllegalArgumentException("unknown HashAlgorithm");
            }
        }
    }
    
    public static final Digest createPRFHash(final int n) {
        switch (n) {
            case 0: {
                return new CombinedHash();
            }
            default: {
                return createHash(getHashAlgorithmForPRFAlgorithm(n));
            }
        }
    }
    
    public static final Digest clonePRFHash(final int n, final Digest digest) {
        switch (n) {
            case 0: {
                return new CombinedHash((CombinedHash)digest);
            }
            default: {
                return cloneHash(getHashAlgorithmForPRFAlgorithm(n), digest);
            }
        }
    }
    
    public static final short getHashAlgorithmForPRFAlgorithm(final int n) {
        switch (n) {
            case 0: {
                throw new IllegalArgumentException("legacy PRF not a valid algorithm");
            }
            case 1: {
                return 4;
            }
            case 2: {
                return 5;
            }
            default: {
                throw new IllegalArgumentException("unknown PRFAlgorithm");
            }
        }
    }
    
    public static ASN1ObjectIdentifier getOIDForHashAlgorithm(final int n) {
        switch (n) {
            case 1: {
                return PKCSObjectIdentifiers.md5;
            }
            case 2: {
                return X509ObjectIdentifiers.id_SHA1;
            }
            case 3: {
                return NISTObjectIdentifiers.id_sha224;
            }
            case 4: {
                return NISTObjectIdentifiers.id_sha256;
            }
            case 5: {
                return NISTObjectIdentifiers.id_sha384;
            }
            case 6: {
                return NISTObjectIdentifiers.id_sha512;
            }
            default: {
                throw new IllegalArgumentException("unknown HashAlgorithm");
            }
        }
    }
    
    static short getClientCertificateType(final org.bouncycastle.crypto.tls.Certificate certificate, final org.bouncycastle.crypto.tls.Certificate certificate2) throws IOException {
        if (certificate.isEmpty()) {
            return -1;
        }
        final Certificate certificate3 = certificate.getCertificateAt(0);
        final SubjectPublicKeyInfo subjectPublicKeyInfo = certificate3.getSubjectPublicKeyInfo();
        try {
            final AsymmetricKeyParameter key = PublicKeyFactory.createKey(subjectPublicKeyInfo);
            if (key.isPrivate()) {
                throw new TlsFatalAlert((short)80);
            }
            if (key instanceof RSAKeyParameters) {
                validateKeyUsage(certificate3, 128);
                return 1;
            }
            if (key instanceof DSAPublicKeyParameters) {
                validateKeyUsage(certificate3, 128);
                return 2;
            }
            if (key instanceof ECPublicKeyParameters) {
                validateKeyUsage(certificate3, 128);
                return 64;
            }
        }
        catch (Exception ex) {}
        throw new TlsFatalAlert((short)43);
    }
    
    public static boolean hasSigningCapability(final short n) {
        switch (n) {
            case 1:
            case 2:
            case 64: {
                return true;
            }
            default: {
                return false;
            }
        }
    }
    
    public static TlsSigner createTlsSigner(final short n) {
        switch (n) {
            case 2: {
                return new TlsDSSSigner();
            }
            case 64: {
                return new TlsECDSASigner();
            }
            case 1: {
                return new TlsRSASigner();
            }
            default: {
                throw new IllegalArgumentException("'clientCertificateType' is not a type with signing capability");
            }
        }
    }
    
    private static byte[][] genConst() {
        final int n = 10;
        final byte[][] array = new byte[n][];
        for (int i = 0; i < n; ++i) {
            final byte[] array2 = new byte[i + 1];
            Arrays.fill(array2, (byte)(65 + i));
            array[i] = array2;
        }
        return array;
    }
    
    private static Vector vectorOfOne(final Object obj) {
        final Vector<Object> vector = new Vector<Object>(1);
        vector.addElement(obj);
        return vector;
    }
    
    static {
        TlsUtils.EMPTY_BYTES = new byte[0];
        EXT_signature_algorithms = Integers.valueOf(13);
        SSL_CLIENT = new byte[] { 67, 76, 78, 84 };
        SSL_SERVER = new byte[] { 83, 82, 86, 82 };
        SSL3_CONST = genConst();
    }
}
