// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.util.Enumeration;
import org.bouncycastle.util.Integers;
import java.util.Hashtable;
import java.util.Vector;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.util.Arrays;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;
import java.security.SecureRandom;

public abstract class TlsProtocol
{
    protected static final Integer EXT_RenegotiationInfo;
    protected static final Integer EXT_SessionTicket;
    private static final String TLS_ERROR_MESSAGE = "Internal TLS error, this could be an attack";
    protected static final short CS_START = 0;
    protected static final short CS_CLIENT_HELLO = 1;
    protected static final short CS_SERVER_HELLO = 2;
    protected static final short CS_SERVER_SUPPLEMENTAL_DATA = 3;
    protected static final short CS_SERVER_CERTIFICATE = 4;
    protected static final short CS_SERVER_KEY_EXCHANGE = 5;
    protected static final short CS_CERTIFICATE_REQUEST = 6;
    protected static final short CS_SERVER_HELLO_DONE = 7;
    protected static final short CS_CLIENT_SUPPLEMENTAL_DATA = 8;
    protected static final short CS_CLIENT_CERTIFICATE = 9;
    protected static final short CS_CLIENT_KEY_EXCHANGE = 10;
    protected static final short CS_CERTIFICATE_VERIFY = 11;
    protected static final short CS_CLIENT_CHANGE_CIPHER_SPEC = 12;
    protected static final short CS_CLIENT_FINISHED = 13;
    protected static final short CS_SERVER_SESSION_TICKET = 14;
    protected static final short CS_SERVER_CHANGE_CIPHER_SPEC = 15;
    protected static final short CS_SERVER_FINISHED = 16;
    private ByteQueue applicationDataQueue;
    private ByteQueue changeCipherSpecQueue;
    private ByteQueue alertQueue;
    private ByteQueue handshakeQueue;
    protected RecordStream recordStream;
    protected SecureRandom secureRandom;
    private TlsInputStream tlsInputStream;
    private TlsOutputStream tlsOutputStream;
    private volatile boolean closed;
    private volatile boolean failedWithError;
    private volatile boolean appDataReady;
    private volatile boolean writeExtraEmptyRecords;
    private byte[] expected_verify_data;
    protected SecurityParameters securityParameters;
    protected short connection_state;
    protected boolean secure_renegotiation;
    protected boolean expectSessionTicket;
    
    public TlsProtocol(final InputStream inputStream, final OutputStream outputStream, final SecureRandom secureRandom) {
        this.applicationDataQueue = new ByteQueue();
        this.changeCipherSpecQueue = new ByteQueue();
        this.alertQueue = new ByteQueue();
        this.handshakeQueue = new ByteQueue();
        this.tlsInputStream = null;
        this.tlsOutputStream = null;
        this.closed = false;
        this.failedWithError = false;
        this.appDataReady = false;
        this.writeExtraEmptyRecords = true;
        this.expected_verify_data = null;
        this.securityParameters = null;
        this.connection_state = 0;
        this.secure_renegotiation = false;
        this.expectSessionTicket = false;
        this.recordStream = new RecordStream(this, inputStream, outputStream);
        this.secureRandom = secureRandom;
    }
    
    protected abstract AbstractTlsContext getContext();
    
    protected abstract TlsPeer getPeer();
    
    protected abstract void handleChangeCipherSpecMessage() throws IOException;
    
    protected abstract void handleHandshakeMessage(final short p0, final byte[] p1) throws IOException;
    
    protected void handleWarningMessage(final short n) throws IOException {
    }
    
    protected void completeHandshake() throws IOException {
        this.expected_verify_data = null;
        while (this.connection_state != 16) {
            this.safeReadRecord();
        }
        this.recordStream.finaliseHandshake();
        this.writeExtraEmptyRecords = this.getContext().getServerVersion().isEqualOrEarlierVersionOf(ProtocolVersion.TLSv10);
        if (!this.appDataReady) {
            this.appDataReady = true;
            this.tlsInputStream = new TlsInputStream(this);
            this.tlsOutputStream = new TlsOutputStream(this);
        }
    }
    
    protected void processRecord(final short n, final byte[] array, final int n2, final int n3) throws IOException {
        switch (n) {
            case 20: {
                this.changeCipherSpecQueue.addData(array, n2, n3);
                this.processChangeCipherSpec();
                break;
            }
            case 21: {
                this.alertQueue.addData(array, n2, n3);
                this.processAlert();
                break;
            }
            case 22: {
                this.handshakeQueue.addData(array, n2, n3);
                this.processHandshake();
                break;
            }
            case 23: {
                if (!this.appDataReady) {
                    this.failWithError((short)2, (short)10);
                }
                this.applicationDataQueue.addData(array, n2, n3);
                this.processApplicationData();
                break;
            }
        }
    }
    
    private void processHandshake() throws IOException {
        boolean b;
        do {
            b = false;
            if (this.handshakeQueue.size() >= 4) {
                final byte[] buf = new byte[4];
                this.handshakeQueue.read(buf, 0, 4, 0);
                final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
                final short uint8 = TlsUtils.readUint8(byteArrayInputStream);
                final int uint9 = TlsUtils.readUint24(byteArrayInputStream);
                if (this.handshakeQueue.size() < uint9 + 4) {
                    continue;
                }
                final byte[] array = new byte[uint9];
                this.handshakeQueue.read(array, 0, uint9, 4);
                this.handshakeQueue.removeData(uint9 + 4);
                Label_0175: {
                    switch (uint8) {
                        case 0: {
                            break Label_0175;
                        }
                        case 20: {
                            if (this.expected_verify_data == null) {
                                this.expected_verify_data = this.createVerifyData(!this.getContext().isServer());
                                break;
                            }
                            break;
                        }
                    }
                    this.recordStream.updateHandshakeData(buf, 0, 4);
                    this.recordStream.updateHandshakeData(array, 0, uint9);
                }
                this.handleHandshakeMessage(uint8, array);
                b = true;
            }
        } while (b);
    }
    
    private void processApplicationData() {
    }
    
    private void processAlert() throws IOException {
        while (this.alertQueue.size() >= 2) {
            final byte[] array = new byte[2];
            this.alertQueue.read(array, 0, 2, 0);
            this.alertQueue.removeData(2);
            final short n = array[0];
            final short n2 = array[1];
            this.getPeer().notifyAlertReceived(n, n2);
            if (n == 2) {
                this.failedWithError = true;
                this.closed = true;
                try {
                    this.recordStream.close();
                }
                catch (Exception ex) {}
                throw new IOException("Internal TLS error, this could be an attack");
            }
            if (n2 == 0) {
                this.handleClose(false);
            }
            this.handleWarningMessage(n2);
        }
    }
    
    private void processChangeCipherSpec() throws IOException {
        while (this.changeCipherSpecQueue.size() > 0) {
            final byte[] array = { 0 };
            this.changeCipherSpecQueue.read(array, 0, 1, 0);
            this.changeCipherSpecQueue.removeData(1);
            if (array[0] != 1) {
                this.failWithError((short)2, (short)10);
            }
            this.recordStream.receivedReadCipherSpec();
            this.handleChangeCipherSpecMessage();
        }
    }
    
    protected int readApplicationData(final byte[] array, final int n, int min) throws IOException {
        if (min < 1) {
            return 0;
        }
        while (this.applicationDataQueue.size() == 0) {
            if (this.closed) {
                if (this.failedWithError) {
                    throw new IOException("Internal TLS error, this could be an attack");
                }
                return -1;
            }
            else {
                this.safeReadRecord();
            }
        }
        min = Math.min(min, this.applicationDataQueue.size());
        this.applicationDataQueue.read(array, n, min, 0);
        this.applicationDataQueue.removeData(min);
        return min;
    }
    
    protected void safeReadRecord() throws IOException {
        try {
            this.recordStream.readRecord();
        }
        catch (TlsFatalAlert tlsFatalAlert) {
            if (!this.closed) {
                this.failWithError((short)2, tlsFatalAlert.getAlertDescription());
            }
            throw tlsFatalAlert;
        }
        catch (IOException ex) {
            if (!this.closed) {
                this.failWithError((short)2, (short)80);
            }
            throw ex;
        }
        catch (RuntimeException ex2) {
            if (!this.closed) {
                this.failWithError((short)2, (short)80);
            }
            throw ex2;
        }
    }
    
    protected void safeWriteRecord(final short n, final byte[] array, final int n2, final int n3) throws IOException {
        try {
            this.recordStream.writeRecord(n, array, n2, n3);
        }
        catch (TlsFatalAlert tlsFatalAlert) {
            if (!this.closed) {
                this.failWithError((short)2, tlsFatalAlert.getAlertDescription());
            }
            throw tlsFatalAlert;
        }
        catch (IOException ex) {
            if (!this.closed) {
                this.failWithError((short)2, (short)80);
            }
            throw ex;
        }
        catch (RuntimeException ex2) {
            if (!this.closed) {
                this.failWithError((short)2, (short)80);
            }
            throw ex2;
        }
    }
    
    protected void writeData(final byte[] array, int n, int i) throws IOException {
        if (!this.closed) {
            while (i > 0) {
                if (this.writeExtraEmptyRecords) {
                    this.safeWriteRecord((short)23, TlsUtils.EMPTY_BYTES, 0, 0);
                }
                final int min = Math.min(i, 16384);
                this.safeWriteRecord((short)23, array, n, min);
                n += min;
                i -= min;
            }
            return;
        }
        if (this.failedWithError) {
            throw new IOException("Internal TLS error, this could be an attack");
        }
        throw new IOException("Sorry, connection has been closed, you cannot write more data");
    }
    
    public OutputStream getOutputStream() {
        return this.tlsOutputStream;
    }
    
    public InputStream getInputStream() {
        return this.tlsInputStream;
    }
    
    protected void failWithError(final short n, final short n2) throws IOException {
        if (this.closed) {
            throw new IOException("Internal TLS error, this could be an attack");
        }
        this.closed = true;
        if (n == 2) {
            this.failedWithError = true;
        }
        this.raiseAlert(n, n2, null, null);
        this.recordStream.close();
        if (n == 2) {
            throw new IOException("Internal TLS error, this could be an attack");
        }
    }
    
    protected void processFinishedMessage(final ByteArrayInputStream byteArrayInputStream) throws IOException {
        final byte[] fully = TlsUtils.readFully(this.expected_verify_data.length, byteArrayInputStream);
        assertEmpty(byteArrayInputStream);
        if (!Arrays.constantTimeAreEqual(this.expected_verify_data, fully)) {
            this.failWithError((short)2, (short)51);
        }
    }
    
    protected void raiseAlert(final short n, final short n2, final String s, final Exception ex) throws IOException {
        this.getPeer().notifyAlertRaised(n, n2, s, ex);
        this.safeWriteRecord((short)21, new byte[] { (byte)n, (byte)n2 }, 0, 2);
    }
    
    protected void raiseWarning(final short n, final String s) throws IOException {
        this.raiseAlert((short)1, n, s, null);
    }
    
    protected void sendCertificateMessage(Certificate empty_CHAIN) throws IOException {
        if (empty_CHAIN == null) {
            empty_CHAIN = Certificate.EMPTY_CHAIN;
        }
        if (empty_CHAIN.getLength() == 0 && !this.getContext().isServer()) {
            final ProtocolVersion serverVersion = this.getContext().getServerVersion();
            if (serverVersion.isSSL()) {
                this.raiseWarning((short)41, serverVersion.toString() + " client didn't provide credentials");
                return;
            }
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8((short)11, byteArrayOutputStream);
        TlsUtils.writeUint24(0, byteArrayOutputStream);
        empty_CHAIN.encode(byteArrayOutputStream);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        TlsUtils.writeUint24(byteArray.length - 4, byteArray, 1);
        this.safeWriteRecord((short)22, byteArray, 0, byteArray.length);
    }
    
    protected void sendChangeCipherSpecMessage() throws IOException {
        final byte[] array = { 1 };
        this.safeWriteRecord((short)20, array, 0, array.length);
        this.recordStream.sentWriteCipherSpec();
    }
    
    protected void sendFinishedMessage() throws IOException {
        final byte[] verifyData = this.createVerifyData(this.getContext().isServer());
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8((short)20, byteArrayOutputStream);
        TlsUtils.writeUint24(verifyData.length, byteArrayOutputStream);
        byteArrayOutputStream.write(verifyData);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        this.safeWriteRecord((short)22, byteArray, 0, byteArray.length);
    }
    
    protected void sendSupplementalDataMessage(final Vector vector) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8((short)23, byteArrayOutputStream);
        TlsUtils.writeUint24(0, byteArrayOutputStream);
        writeSupplementalData(byteArrayOutputStream, vector);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        TlsUtils.writeUint24(byteArray.length - 4, byteArray, 1);
        this.safeWriteRecord((short)22, byteArray, 0, byteArray.length);
    }
    
    protected byte[] createVerifyData(final boolean b) {
        final AbstractTlsContext context = this.getContext();
        if (b) {
            return TlsUtils.calculateVerifyData(context, "server finished", this.recordStream.getCurrentHash(TlsUtils.SSL_SERVER));
        }
        return TlsUtils.calculateVerifyData(context, "client finished", this.recordStream.getCurrentHash(TlsUtils.SSL_CLIENT));
    }
    
    public void close() throws IOException {
        this.handleClose(true);
    }
    
    protected void handleClose(final boolean b) throws IOException {
        if (!this.closed) {
            if (b && !this.appDataReady) {
                this.raiseWarning((short)90, "User canceled handshake");
            }
            this.failWithError((short)1, (short)0);
        }
    }
    
    protected void flush() throws IOException {
        this.recordStream.flush();
    }
    
    protected static boolean arrayContains(final short[] array, final short n) {
        for (int i = 0; i < array.length; ++i) {
            if (array[i] == n) {
                return true;
            }
        }
        return false;
    }
    
    protected static boolean arrayContains(final int[] array, final int n) {
        for (int i = 0; i < array.length; ++i) {
            if (array[i] == n) {
                return true;
            }
        }
        return false;
    }
    
    protected static void assertEmpty(final ByteArrayInputStream byteArrayInputStream) throws IOException {
        if (byteArrayInputStream.available() > 0) {
            throw new TlsFatalAlert((short)50);
        }
    }
    
    protected static byte[] createRandomBlock(final SecureRandom secureRandom) {
        final byte[] bytes = new byte[32];
        secureRandom.nextBytes(bytes);
        TlsUtils.writeGMTUnixTime(bytes, 0);
        return bytes;
    }
    
    protected static byte[] createRenegotiationInfo(final byte[] array) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeOpaque8(array, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    protected static void establishMasterSecret(final TlsContext tlsContext, final TlsKeyExchange tlsKeyExchange) throws IOException {
        final byte[] generatePremasterSecret = tlsKeyExchange.generatePremasterSecret();
        try {
            tlsContext.getSecurityParameters().masterSecret = TlsUtils.calculateMasterSecret(tlsContext, generatePremasterSecret);
        }
        finally {
            if (generatePremasterSecret != null) {
                Arrays.fill(generatePremasterSecret, (byte)0);
            }
        }
    }
    
    protected static Hashtable readExtensions(final ByteArrayInputStream byteArrayInputStream) throws IOException {
        if (byteArrayInputStream.available() < 1) {
            return null;
        }
        final byte[] opaque16 = TlsUtils.readOpaque16(byteArrayInputStream);
        assertEmpty(byteArrayInputStream);
        final ByteArrayInputStream byteArrayInputStream2 = new ByteArrayInputStream(opaque16);
        final Hashtable<Integer, byte[]> hashtable = new Hashtable<Integer, byte[]>();
        while (byteArrayInputStream2.available() > 0) {
            if (null != hashtable.put(Integers.valueOf(TlsUtils.readUint16(byteArrayInputStream2)), TlsUtils.readOpaque16(byteArrayInputStream2))) {
                throw new TlsFatalAlert((short)47);
            }
        }
        return hashtable;
    }
    
    protected static Vector readSupplementalDataMessage(final ByteArrayInputStream byteArrayInputStream) throws IOException {
        final byte[] opaque24 = TlsUtils.readOpaque24(byteArrayInputStream);
        assertEmpty(byteArrayInputStream);
        final ByteArrayInputStream byteArrayInputStream2 = new ByteArrayInputStream(opaque24);
        final Vector<SupplementalDataEntry> vector = new Vector<SupplementalDataEntry>();
        while (byteArrayInputStream2.available() > 0) {
            vector.addElement(new SupplementalDataEntry(TlsUtils.readUint16(byteArrayInputStream2), TlsUtils.readOpaque16(byteArrayInputStream2)));
        }
        return vector;
    }
    
    protected static void writeExtensions(final OutputStream outputStream, final Hashtable hashtable) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final Enumeration<Integer> keys = hashtable.keys();
        while (keys.hasMoreElements()) {
            final Integer key = keys.nextElement();
            final byte[] array = (Object)hashtable.get(key);
            TlsUtils.writeUint16(key, byteArrayOutputStream);
            TlsUtils.writeOpaque16(array, byteArrayOutputStream);
        }
        TlsUtils.writeOpaque16(byteArrayOutputStream.toByteArray(), outputStream);
    }
    
    protected static void writeSupplementalData(final OutputStream outputStream, final Vector vector) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (int i = 0; i < vector.size(); ++i) {
            final SupplementalDataEntry supplementalDataEntry = vector.elementAt(i);
            TlsUtils.writeUint16(supplementalDataEntry.getDataType(), byteArrayOutputStream);
            TlsUtils.writeOpaque16(supplementalDataEntry.getData(), byteArrayOutputStream);
        }
        TlsUtils.writeOpaque24(byteArrayOutputStream.toByteArray(), outputStream);
    }
    
    protected static int getPRFAlgorithm(final int n) {
        switch (n) {
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 156:
            case 158:
            case 160:
            case 162:
            case 164:
            case 49187:
            case 49189:
            case 49191:
            case 49193:
            case 49195:
            case 49197:
            case 49199:
            case 49201: {
                return 1;
            }
            case 157:
            case 159:
            case 161:
            case 163:
            case 165:
            case 49188:
            case 49190:
            case 49192:
            case 49194:
            case 49196:
            case 49198:
            case 49200:
            case 49202: {
                return 2;
            }
            default: {
                return 0;
            }
        }
    }
    
    static {
        EXT_RenegotiationInfo = Integers.valueOf(65281);
        EXT_SessionTicket = Integers.valueOf(35);
    }
}
