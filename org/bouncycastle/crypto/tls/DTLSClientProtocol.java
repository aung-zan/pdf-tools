// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.util.Enumeration;
import java.util.Hashtable;
import org.bouncycastle.util.Arrays;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.util.Vector;
import java.io.IOException;
import java.security.SecureRandom;

public class DTLSClientProtocol extends DTLSProtocol
{
    public DTLSClientProtocol(final SecureRandom secureRandom) {
        super(secureRandom);
    }
    
    public DTLSTransport connect(final TlsClient client, final DatagramTransport datagramTransport) throws IOException {
        if (client == null) {
            throw new IllegalArgumentException("'client' cannot be null");
        }
        if (datagramTransport == null) {
            throw new IllegalArgumentException("'transport' cannot be null");
        }
        final SecurityParameters securityParameters = new SecurityParameters();
        securityParameters.entity = 1;
        securityParameters.clientRandom = TlsProtocol.createRandomBlock(this.secureRandom);
        final ClientHandshakeState clientHandshakeState = new ClientHandshakeState();
        (clientHandshakeState.client = client).init(clientHandshakeState.clientContext = new TlsClientContextImpl(this.secureRandom, securityParameters));
        final DTLSRecordLayer dtlsRecordLayer = new DTLSRecordLayer(datagramTransport, clientHandshakeState.clientContext, client, (short)22);
        try {
            return this.clientHandshake(clientHandshakeState, dtlsRecordLayer);
        }
        catch (TlsFatalAlert tlsFatalAlert) {
            dtlsRecordLayer.fail(tlsFatalAlert.getAlertDescription());
            throw tlsFatalAlert;
        }
        catch (IOException ex) {
            dtlsRecordLayer.fail((short)80);
            throw ex;
        }
        catch (RuntimeException ex2) {
            dtlsRecordLayer.fail((short)80);
            throw new TlsFatalAlert((short)80);
        }
    }
    
    protected DTLSTransport clientHandshake(final ClientHandshakeState clientHandshakeState, final DTLSRecordLayer dtlsRecordLayer) throws IOException {
        final SecurityParameters securityParameters = clientHandshakeState.clientContext.getSecurityParameters();
        final DTLSReliableHandshake dtlsReliableHandshake = new DTLSReliableHandshake(clientHandshakeState.clientContext, dtlsRecordLayer);
        final byte[] generateClientHello = this.generateClientHello(clientHandshakeState, clientHandshakeState.client);
        dtlsReliableHandshake.sendMessage((short)1, generateClientHello);
        DTLSReliableHandshake.Message message = dtlsReliableHandshake.receiveMessage();
        final ProtocolVersion discoveredPeerVersion = dtlsRecordLayer.getDiscoveredPeerVersion();
        if (!discoveredPeerVersion.isEqualOrEarlierVersionOf(clientHandshakeState.clientContext.getClientVersion())) {
            throw new TlsFatalAlert((short)47);
        }
        clientHandshakeState.clientContext.setServerVersion(discoveredPeerVersion);
        clientHandshakeState.client.notifyServerVersion(discoveredPeerVersion);
        while (message.getType() == 3) {
            final byte[] patchClientHelloWithCookie = patchClientHelloWithCookie(generateClientHello, parseHelloVerifyRequest(clientHandshakeState.clientContext, message.getBody()));
            dtlsReliableHandshake.resetHandshakeMessagesDigest();
            dtlsReliableHandshake.sendMessage((short)1, patchClientHelloWithCookie);
            message = dtlsReliableHandshake.receiveMessage();
        }
        if (message.getType() != 2) {
            throw new TlsFatalAlert((short)10);
        }
        this.processServerHello(clientHandshakeState, message.getBody());
        DTLSReliableHandshake.Message message2 = dtlsReliableHandshake.receiveMessage();
        securityParameters.prfAlgorithm = TlsProtocol.getPRFAlgorithm(clientHandshakeState.selectedCipherSuite);
        securityParameters.compressionAlgorithm = clientHandshakeState.selectedCompressionMethod;
        securityParameters.verifyDataLength = 12;
        dtlsReliableHandshake.notifyHelloComplete();
        if (message2.getType() == 23) {
            this.processServerSupplementalData(clientHandshakeState, message2.getBody());
            message2 = dtlsReliableHandshake.receiveMessage();
        }
        else {
            clientHandshakeState.client.processServerSupplementalData(null);
        }
        (clientHandshakeState.keyExchange = clientHandshakeState.client.getKeyExchange()).init(clientHandshakeState.clientContext);
        if (message2.getType() == 11) {
            this.processServerCertificate(clientHandshakeState, message2.getBody());
            message2 = dtlsReliableHandshake.receiveMessage();
        }
        else {
            clientHandshakeState.keyExchange.skipServerCredentials();
        }
        if (message2.getType() == 12) {
            this.processServerKeyExchange(clientHandshakeState, message2.getBody());
            message2 = dtlsReliableHandshake.receiveMessage();
        }
        else {
            clientHandshakeState.keyExchange.skipServerKeyExchange();
        }
        if (message2.getType() == 13) {
            this.processCertificateRequest(clientHandshakeState, message2.getBody());
            message2 = dtlsReliableHandshake.receiveMessage();
        }
        if (message2.getType() != 14) {
            throw new TlsFatalAlert((short)10);
        }
        if (message2.getBody().length != 0) {
            throw new TlsFatalAlert((short)50);
        }
        final Vector clientSupplementalData = clientHandshakeState.client.getClientSupplementalData();
        if (clientSupplementalData != null) {
            dtlsReliableHandshake.sendMessage((short)23, DTLSProtocol.generateSupplementalData(clientSupplementalData));
        }
        if (clientHandshakeState.certificateRequest != null) {
            clientHandshakeState.clientCredentials = clientHandshakeState.authentication.getClientCredentials(clientHandshakeState.certificateRequest);
            Certificate certificate = null;
            if (clientHandshakeState.clientCredentials != null) {
                certificate = clientHandshakeState.clientCredentials.getCertificate();
            }
            if (certificate == null) {
                certificate = Certificate.EMPTY_CHAIN;
            }
            dtlsReliableHandshake.sendMessage((short)11, DTLSProtocol.generateCertificate(certificate));
        }
        if (clientHandshakeState.clientCredentials != null) {
            clientHandshakeState.keyExchange.processClientCredentials(clientHandshakeState.clientCredentials);
        }
        else {
            clientHandshakeState.keyExchange.skipClientCredentials();
        }
        dtlsReliableHandshake.sendMessage((short)16, this.generateClientKeyExchange(clientHandshakeState));
        TlsProtocol.establishMasterSecret(clientHandshakeState.clientContext, clientHandshakeState.keyExchange);
        if (clientHandshakeState.clientCredentials instanceof TlsSignerCredentials) {
            dtlsReliableHandshake.sendMessage((short)15, this.generateCertificateVerify(clientHandshakeState, ((TlsSignerCredentials)clientHandshakeState.clientCredentials).generateCertificateSignature(dtlsReliableHandshake.getCurrentHash())));
        }
        dtlsRecordLayer.initPendingEpoch(clientHandshakeState.client.getCipher());
        dtlsReliableHandshake.sendMessage((short)20, TlsUtils.calculateVerifyData(clientHandshakeState.clientContext, "client finished", dtlsReliableHandshake.getCurrentHash()));
        if (clientHandshakeState.expectSessionTicket) {
            final DTLSReliableHandshake.Message receiveMessage = dtlsReliableHandshake.receiveMessage();
            if (receiveMessage.getType() != 4) {
                throw new TlsFatalAlert((short)10);
            }
            this.processNewSessionTicket(clientHandshakeState, receiveMessage.getBody());
        }
        final byte[] calculateVerifyData = TlsUtils.calculateVerifyData(clientHandshakeState.clientContext, "server finished", dtlsReliableHandshake.getCurrentHash());
        final DTLSReliableHandshake.Message receiveMessage2 = dtlsReliableHandshake.receiveMessage();
        if (receiveMessage2.getType() == 20) {
            this.processFinished(receiveMessage2.getBody(), calculateVerifyData);
            dtlsReliableHandshake.finish();
            clientHandshakeState.client.notifyHandshakeComplete();
            return new DTLSTransport(dtlsRecordLayer);
        }
        throw new TlsFatalAlert((short)10);
    }
    
    protected byte[] generateCertificateVerify(final ClientHandshakeState clientHandshakeState, final byte[] array) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeOpaque16(array, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    protected byte[] generateClientHello(final ClientHandshakeState clientHandshakeState, final TlsClient tlsClient) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ProtocolVersion clientVersion = tlsClient.getClientVersion();
        if (!clientVersion.isDTLS()) {
            throw new TlsFatalAlert((short)80);
        }
        clientHandshakeState.clientContext.setClientVersion(clientVersion);
        TlsUtils.writeVersion(clientVersion, byteArrayOutputStream);
        byteArrayOutputStream.write(clientHandshakeState.clientContext.getSecurityParameters().getClientRandom());
        TlsUtils.writeOpaque8(TlsUtils.EMPTY_BYTES, byteArrayOutputStream);
        TlsUtils.writeOpaque8(TlsUtils.EMPTY_BYTES, byteArrayOutputStream);
        clientHandshakeState.offeredCipherSuites = tlsClient.getCipherSuites();
        clientHandshakeState.clientExtensions = tlsClient.getClientExtensions();
        final boolean b = clientHandshakeState.clientExtensions == null || clientHandshakeState.clientExtensions.get(TlsProtocol.EXT_RenegotiationInfo) == null;
        int length = clientHandshakeState.offeredCipherSuites.length;
        if (b) {
            ++length;
        }
        TlsUtils.writeUint16(2 * length, byteArrayOutputStream);
        TlsUtils.writeUint16Array(clientHandshakeState.offeredCipherSuites, byteArrayOutputStream);
        if (b) {
            TlsUtils.writeUint16(255, byteArrayOutputStream);
        }
        clientHandshakeState.offeredCompressionMethods = new short[] { 0 };
        TlsUtils.writeUint8((short)clientHandshakeState.offeredCompressionMethods.length, byteArrayOutputStream);
        TlsUtils.writeUint8Array(clientHandshakeState.offeredCompressionMethods, byteArrayOutputStream);
        if (clientHandshakeState.clientExtensions != null) {
            TlsProtocol.writeExtensions(byteArrayOutputStream, clientHandshakeState.clientExtensions);
        }
        return byteArrayOutputStream.toByteArray();
    }
    
    protected byte[] generateClientKeyExchange(final ClientHandshakeState clientHandshakeState) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        clientHandshakeState.keyExchange.generateClientKeyExchange(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    protected void processCertificateRequest(final ClientHandshakeState clientHandshakeState, final byte[] buf) throws IOException {
        if (clientHandshakeState.authentication == null) {
            throw new TlsFatalAlert((short)40);
        }
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        clientHandshakeState.certificateRequest = CertificateRequest.parse(byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        clientHandshakeState.keyExchange.validateCertificateRequest(clientHandshakeState.certificateRequest);
    }
    
    protected void processNewSessionTicket(final ClientHandshakeState clientHandshakeState, final byte[] buf) throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        final NewSessionTicket parse = NewSessionTicket.parse(byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        clientHandshakeState.client.notifyNewSessionTicket(parse);
    }
    
    protected void processServerCertificate(final ClientHandshakeState clientHandshakeState, final byte[] buf) throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        final Certificate parse = Certificate.parse(byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        clientHandshakeState.keyExchange.processServerCertificate(parse);
        (clientHandshakeState.authentication = clientHandshakeState.client.getAuthentication()).notifyServerCertificate(parse);
    }
    
    protected void processServerHello(final ClientHandshakeState clientHandshakeState, final byte[] buf) throws IOException {
        final SecurityParameters securityParameters = clientHandshakeState.clientContext.getSecurityParameters();
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        if (!TlsUtils.readVersion(byteArrayInputStream).equals(clientHandshakeState.clientContext.getServerVersion())) {
            throw new TlsFatalAlert((short)47);
        }
        securityParameters.serverRandom = TlsUtils.readFully(32, byteArrayInputStream);
        final byte[] opaque8 = TlsUtils.readOpaque8(byteArrayInputStream);
        if (opaque8.length > 32) {
            throw new TlsFatalAlert((short)47);
        }
        clientHandshakeState.client.notifySessionID(opaque8);
        clientHandshakeState.selectedCipherSuite = TlsUtils.readUint16(byteArrayInputStream);
        if (!TlsProtocol.arrayContains(clientHandshakeState.offeredCipherSuites, clientHandshakeState.selectedCipherSuite) || clientHandshakeState.selectedCipherSuite == 0 || clientHandshakeState.selectedCipherSuite == 255) {
            throw new TlsFatalAlert((short)47);
        }
        DTLSProtocol.validateSelectedCipherSuite(clientHandshakeState.selectedCipherSuite, (short)47);
        clientHandshakeState.client.notifySelectedCipherSuite(clientHandshakeState.selectedCipherSuite);
        clientHandshakeState.selectedCompressionMethod = TlsUtils.readUint8(byteArrayInputStream);
        if (!TlsProtocol.arrayContains(clientHandshakeState.offeredCompressionMethods, clientHandshakeState.selectedCompressionMethod)) {
            throw new TlsFatalAlert((short)47);
        }
        clientHandshakeState.client.notifySelectedCompressionMethod(clientHandshakeState.selectedCompressionMethod);
        final Hashtable extensions = TlsProtocol.readExtensions(byteArrayInputStream);
        if (extensions != null) {
            final Enumeration<Integer> keys = extensions.keys();
            while (keys.hasMoreElements()) {
                final Integer key = keys.nextElement();
                if (!key.equals(TlsProtocol.EXT_RenegotiationInfo) && (clientHandshakeState.clientExtensions == null || clientHandshakeState.clientExtensions.get(key) == null)) {
                    throw new TlsFatalAlert((short)110);
                }
            }
            final byte[] array = (Object)extensions.get(TlsProtocol.EXT_RenegotiationInfo);
            if (array != null) {
                clientHandshakeState.secure_renegotiation = true;
                if (!Arrays.constantTimeAreEqual(array, TlsProtocol.createRenegotiationInfo(TlsUtils.EMPTY_BYTES))) {
                    throw new TlsFatalAlert((short)40);
                }
            }
            clientHandshakeState.expectSessionTicket = extensions.containsKey(TlsProtocol.EXT_SessionTicket);
        }
        clientHandshakeState.client.notifySecureRenegotiation(clientHandshakeState.secure_renegotiation);
        if (clientHandshakeState.clientExtensions != null) {
            clientHandshakeState.client.processServerExtensions(extensions);
        }
    }
    
    protected void processServerKeyExchange(final ClientHandshakeState clientHandshakeState, final byte[] buf) throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        clientHandshakeState.keyExchange.processServerKeyExchange(byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
    }
    
    protected void processServerSupplementalData(final ClientHandshakeState clientHandshakeState, final byte[] buf) throws IOException {
        clientHandshakeState.client.processServerSupplementalData(TlsProtocol.readSupplementalDataMessage(new ByteArrayInputStream(buf)));
    }
    
    protected static byte[] parseHelloVerifyRequest(final TlsContext tlsContext, final byte[] buf) throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        if (!TlsUtils.readVersion(byteArrayInputStream).equals(tlsContext.getServerVersion())) {
            throw new TlsFatalAlert((short)47);
        }
        final byte[] opaque8 = TlsUtils.readOpaque8(byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        return opaque8;
    }
    
    protected static byte[] patchClientHelloWithCookie(final byte[] array, final byte[] array2) throws IOException {
        final int n = 34;
        final int n2 = n + 1 + TlsUtils.readUint8(array, n);
        final int n3 = n2 + 1;
        final byte[] array3 = new byte[array.length + array2.length];
        System.arraycopy(array, 0, array3, 0, n2);
        TlsUtils.writeUint8((short)array2.length, array3, n2);
        System.arraycopy(array2, 0, array3, n3, array2.length);
        System.arraycopy(array, n3, array3, n3 + array2.length, array.length - n3);
        return array3;
    }
    
    protected static class ClientHandshakeState
    {
        TlsClient client;
        TlsClientContextImpl clientContext;
        int[] offeredCipherSuites;
        short[] offeredCompressionMethods;
        Hashtable clientExtensions;
        int selectedCipherSuite;
        short selectedCompressionMethod;
        boolean secure_renegotiation;
        boolean expectSessionTicket;
        TlsKeyExchange keyExchange;
        TlsAuthentication authentication;
        CertificateRequest certificateRequest;
        TlsCredentials clientCredentials;
        
        protected ClientHandshakeState() {
            this.client = null;
            this.clientContext = null;
            this.offeredCipherSuites = null;
            this.offeredCompressionMethods = null;
            this.clientExtensions = null;
            this.selectedCipherSuite = -1;
            this.selectedCompressionMethod = -1;
            this.secure_renegotiation = false;
            this.expectSessionTicket = false;
            this.keyExchange = null;
            this.authentication = null;
            this.certificateRequest = null;
            this.clientCredentials = null;
        }
    }
}
