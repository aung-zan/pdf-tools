// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

public interface TlsPeer
{
    void notifyAlertRaised(final short p0, final short p1, final String p2, final Exception p3);
    
    void notifyAlertReceived(final short p0, final short p1);
}
