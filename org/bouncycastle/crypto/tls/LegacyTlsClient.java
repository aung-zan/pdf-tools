// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;

public class LegacyTlsClient extends DefaultTlsClient
{
    @Deprecated
    protected CertificateVerifyer verifyer;
    
    @Deprecated
    public LegacyTlsClient(final CertificateVerifyer verifyer) {
        this.verifyer = verifyer;
    }
    
    public TlsAuthentication getAuthentication() throws IOException {
        return new LegacyTlsAuthentication(this.verifyer);
    }
}
