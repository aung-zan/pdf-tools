// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.util.Enumeration;
import org.bouncycastle.util.Integers;
import java.io.IOException;
import java.util.Vector;
import java.util.Hashtable;

class DTLSReliableHandshake
{
    private static final int MAX_RECEIVE_AHEAD = 10;
    private final DTLSRecordLayer recordLayer;
    private TlsHandshakeHash hash;
    private Hashtable currentInboundFlight;
    private Hashtable previousInboundFlight;
    private Vector outboundFlight;
    private boolean sending;
    private int message_seq;
    private int next_receive_seq;
    
    DTLSReliableHandshake(final TlsContext tlsContext, final DTLSRecordLayer recordLayer) {
        this.hash = new DeferredHash();
        this.currentInboundFlight = new Hashtable();
        this.previousInboundFlight = null;
        this.outboundFlight = new Vector();
        this.sending = true;
        this.message_seq = 0;
        this.next_receive_seq = 0;
        this.recordLayer = recordLayer;
        this.hash.init(tlsContext);
    }
    
    void notifyHelloComplete() {
        this.hash = this.hash.commit();
    }
    
    byte[] getCurrentHash() {
        final TlsHandshakeHash fork = this.hash.fork();
        final byte[] array = new byte[fork.getDigestSize()];
        fork.doFinal(array, 0);
        return array;
    }
    
    void sendMessage(final short n, final byte[] array) throws IOException {
        if (!this.sending) {
            this.checkInboundFlight();
            this.sending = true;
            this.outboundFlight.removeAllElements();
        }
        final Message obj = new Message(this.message_seq++, n, array);
        this.outboundFlight.addElement(obj);
        this.writeMessage(obj);
        this.updateHandshakeMessagesDigest(obj);
    }
    
    Message receiveMessage() throws IOException {
        if (this.sending) {
            this.sending = false;
            this.prepareInboundFlight();
        }
        final DTLSReassembler dtlsReassembler = this.currentInboundFlight.get(Integers.valueOf(this.next_receive_seq));
        if (dtlsReassembler != null) {
            final byte[] bodyIfComplete = dtlsReassembler.getBodyIfComplete();
            if (bodyIfComplete != null) {
                this.previousInboundFlight = null;
                return this.updateHandshakeMessagesDigest(new Message(this.next_receive_seq++, dtlsReassembler.getType(), bodyIfComplete));
            }
        }
        byte[] array = null;
        int n = 1000;
        while (true) {
            final int receiveLimit = this.recordLayer.getReceiveLimit();
            Label_0109: {
                if (array != null && array.length >= receiveLimit) {
                    break Label_0109;
                }
                array = new byte[receiveLimit];
                try {
                    while (true) {
                        final int receive = this.recordLayer.receive(array, 0, receiveLimit, n);
                        if (receive < 0) {
                            break;
                        }
                        if (receive < 12) {
                            continue;
                        }
                        final int uint24 = TlsUtils.readUint24(array, 9);
                        if (receive != uint24 + 12) {
                            continue;
                        }
                        final int uint25 = TlsUtils.readUint16(array, 4);
                        if (uint25 > this.next_receive_seq + 10) {
                            continue;
                        }
                        final short uint26 = TlsUtils.readUint8(array, 0);
                        final int uint27 = TlsUtils.readUint24(array, 1);
                        final int uint28 = TlsUtils.readUint24(array, 6);
                        if (uint28 + uint24 > uint27) {
                            continue;
                        }
                        if (uint25 < this.next_receive_seq) {
                            if (this.previousInboundFlight == null) {
                                continue;
                            }
                            final DTLSReassembler dtlsReassembler2 = this.previousInboundFlight.get(Integers.valueOf(uint25));
                            if (dtlsReassembler2 == null) {
                                continue;
                            }
                            dtlsReassembler2.contributeFragment(uint26, uint27, array, 12, uint28, uint24);
                            if (!checkAll(this.previousInboundFlight)) {
                                continue;
                            }
                            this.resendOutboundFlight();
                            n = Math.min(n * 2, 60000);
                            resetAll(this.previousInboundFlight);
                        }
                        else {
                            DTLSReassembler value = this.currentInboundFlight.get(Integers.valueOf(uint25));
                            if (value == null) {
                                value = new DTLSReassembler(uint26, uint27);
                                this.currentInboundFlight.put(Integers.valueOf(uint25), value);
                            }
                            value.contributeFragment(uint26, uint27, array, 12, uint28, uint24);
                            if (uint25 != this.next_receive_seq) {
                                continue;
                            }
                            final byte[] bodyIfComplete2 = value.getBodyIfComplete();
                            if (bodyIfComplete2 != null) {
                                this.previousInboundFlight = null;
                                return this.updateHandshakeMessagesDigest(new Message(this.next_receive_seq++, value.getType(), bodyIfComplete2));
                            }
                            continue;
                        }
                    }
                }
                catch (IOException ex) {}
            }
            this.resendOutboundFlight();
            n = Math.min(n * 2, 60000);
        }
    }
    
    void finish() {
        DTLSHandshakeRetransmit dtlsHandshakeRetransmit = null;
        if (!this.sending) {
            this.checkInboundFlight();
        }
        else if (this.currentInboundFlight != null) {
            dtlsHandshakeRetransmit = new DTLSHandshakeRetransmit() {
                public void receivedHandshakeRecord(final int n, final byte[] array, final int n2, final int n3) throws IOException {
                    if (n3 < 12) {
                        return;
                    }
                    final int uint24 = TlsUtils.readUint24(array, n2 + 9);
                    if (n3 != uint24 + 12) {
                        return;
                    }
                    final int uint25 = TlsUtils.readUint16(array, n2 + 4);
                    if (uint25 >= DTLSReliableHandshake.this.next_receive_seq) {
                        return;
                    }
                    final short uint26 = TlsUtils.readUint8(array, n2);
                    if (n != ((uint26 == 20) ? 1 : 0)) {
                        return;
                    }
                    final int uint27 = TlsUtils.readUint24(array, n2 + 1);
                    final int uint28 = TlsUtils.readUint24(array, n2 + 6);
                    if (uint28 + uint24 > uint27) {
                        return;
                    }
                    final DTLSReassembler dtlsReassembler = DTLSReliableHandshake.this.currentInboundFlight.get(Integers.valueOf(uint25));
                    if (dtlsReassembler != null) {
                        dtlsReassembler.contributeFragment(uint26, uint27, array, n2 + 12, uint28, uint24);
                        if (checkAll(DTLSReliableHandshake.this.currentInboundFlight)) {
                            DTLSReliableHandshake.this.resendOutboundFlight();
                            resetAll(DTLSReliableHandshake.this.currentInboundFlight);
                        }
                    }
                }
            };
        }
        this.recordLayer.handshakeSuccessful(dtlsHandshakeRetransmit);
    }
    
    void resetHandshakeMessagesDigest() {
        this.hash.reset();
    }
    
    private void checkInboundFlight() {
        final Enumeration<Integer> keys = this.currentInboundFlight.keys();
        while (keys.hasMoreElements()) {
            if (keys.nextElement() >= this.next_receive_seq) {}
        }
    }
    
    private void prepareInboundFlight() {
        resetAll(this.currentInboundFlight);
        this.previousInboundFlight = this.currentInboundFlight;
        this.currentInboundFlight = new Hashtable();
    }
    
    private void resendOutboundFlight() throws IOException {
        this.recordLayer.resetWriteEpoch();
        for (int i = 0; i < this.outboundFlight.size(); ++i) {
            this.writeMessage((Message)this.outboundFlight.elementAt(i));
        }
    }
    
    private Message updateHandshakeMessagesDigest(final Message message) throws IOException {
        if (message.getType() != 0) {
            final byte[] body = message.getBody();
            final byte[] array = new byte[12];
            TlsUtils.writeUint8(message.getType(), array, 0);
            TlsUtils.writeUint24(body.length, array, 1);
            TlsUtils.writeUint16(message.getSeq(), array, 4);
            TlsUtils.writeUint24(0, array, 6);
            TlsUtils.writeUint24(body.length, array, 9);
            this.hash.update(array, 0, array.length);
            this.hash.update(body, 0, body.length);
        }
        return message;
    }
    
    private void writeMessage(final Message message) throws IOException {
        final int b = this.recordLayer.getSendLimit() - 12;
        if (b < 1) {
            throw new TlsFatalAlert((short)80);
        }
        final int length = message.getBody().length;
        int i = 0;
        do {
            final int min = Math.min(length - i, b);
            this.writeHandshakeFragment(message, i, min);
            i += min;
        } while (i < length);
    }
    
    private void writeHandshakeFragment(final Message message, final int off, final int len) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8(message.getType(), byteArrayOutputStream);
        TlsUtils.writeUint24(message.getBody().length, byteArrayOutputStream);
        TlsUtils.writeUint16(message.getSeq(), byteArrayOutputStream);
        TlsUtils.writeUint24(off, byteArrayOutputStream);
        TlsUtils.writeUint24(len, byteArrayOutputStream);
        byteArrayOutputStream.write(message.getBody(), off, len);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        this.recordLayer.send(byteArray, 0, byteArray.length);
    }
    
    private static boolean checkAll(final Hashtable hashtable) {
        final Enumeration<DTLSReassembler> elements = hashtable.elements();
        while (elements.hasMoreElements()) {
            if (elements.nextElement().getBodyIfComplete() == null) {
                return false;
            }
        }
        return true;
    }
    
    private static void resetAll(final Hashtable hashtable) {
        final Enumeration<DTLSReassembler> elements = hashtable.elements();
        while (elements.hasMoreElements()) {
            elements.nextElement().reset();
        }
    }
    
    static class Message
    {
        private final int message_seq;
        private final short msg_type;
        private final byte[] body;
        
        private Message(final int message_seq, final short msg_type, final byte[] body) {
            this.message_seq = message_seq;
            this.msg_type = msg_type;
            this.body = body;
        }
        
        public int getSeq() {
            return this.message_seq;
        }
        
        public short getType() {
            return this.msg_type;
        }
        
        public byte[] getBody() {
            return this.body;
        }
    }
}
