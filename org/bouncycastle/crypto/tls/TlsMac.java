// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.digests.LongDigest;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.Mac;

public class TlsMac
{
    protected TlsContext context;
    protected byte[] secret;
    protected Mac mac;
    protected int digestBlockSize;
    protected int digestOverhead;
    
    public TlsMac(final TlsContext context, final Digest digest, final byte[] array, final int n, final int n2) {
        this.context = context;
        final KeyParameter keyParameter = new KeyParameter(array, n, n2);
        this.secret = Arrays.clone(keyParameter.getKey());
        if (digest instanceof LongDigest) {
            this.digestBlockSize = 128;
            this.digestOverhead = 16;
        }
        else {
            this.digestBlockSize = 64;
            this.digestOverhead = 8;
        }
        if (context.getServerVersion().isSSL()) {
            this.mac = new SSL3Mac(digest);
            if (digest.getDigestSize() == 20) {
                this.digestOverhead = 4;
            }
        }
        else {
            this.mac = new HMac(digest);
        }
        this.mac.init(keyParameter);
    }
    
    public byte[] getMACSecret() {
        return this.secret;
    }
    
    public int getSize() {
        return this.mac.getMacSize();
    }
    
    public byte[] calculateMac(final long n, final short n2, final byte[] array, final int n3, final int n4) {
        final ProtocolVersion serverVersion = this.context.getServerVersion();
        final boolean ssl = serverVersion.isSSL();
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(ssl ? 11 : 13);
        try {
            TlsUtils.writeUint64(n, byteArrayOutputStream);
            TlsUtils.writeUint8(n2, byteArrayOutputStream);
            if (!ssl) {
                TlsUtils.writeVersion(serverVersion, byteArrayOutputStream);
            }
            TlsUtils.writeUint16(n4, byteArrayOutputStream);
        }
        catch (IOException ex) {
            throw new IllegalStateException("Internal error during mac calculation");
        }
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        this.mac.update(byteArray, 0, byteArray.length);
        this.mac.update(array, n3, n4);
        final byte[] array2 = new byte[this.mac.getMacSize()];
        this.mac.doFinal(array2, 0);
        return array2;
    }
    
    public byte[] calculateMacConstantTime(final long n, final short n2, final byte[] array, final int n3, final int n4, final int n5, final byte[] array2) {
        final byte[] calculateMac = this.calculateMac(n, n2, array, n3, n4);
        final int n6 = this.context.getServerVersion().isSSL() ? 11 : 13;
        int n7 = this.getDigestBlockCount(n6 + n5) - this.getDigestBlockCount(n6 + n4);
        while (--n7 >= 0) {
            this.mac.update(array2, 0, this.digestBlockSize);
        }
        this.mac.update(array2[0]);
        this.mac.reset();
        return calculateMac;
    }
    
    private int getDigestBlockCount(final int n) {
        return (n + this.digestOverhead) / this.digestBlockSize;
    }
}
