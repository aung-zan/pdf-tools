// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.security.SecureRandom;

public interface TlsContext
{
    SecureRandom getSecureRandom();
    
    SecurityParameters getSecurityParameters();
    
    boolean isServer();
    
    ProtocolVersion getClientVersion();
    
    ProtocolVersion getServerVersion();
    
    Object getUserObject();
    
    void setUserObject(final Object p0);
    
    byte[] exportKeyingMaterial(final String p0, final byte[] p1, final int p2);
}
