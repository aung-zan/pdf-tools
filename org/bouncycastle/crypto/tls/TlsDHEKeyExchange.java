// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.io.SignerInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.io.OutputStream;
import org.bouncycastle.crypto.params.DHPublicKeyParameters;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.DHKeyGenerationParameters;
import org.bouncycastle.crypto.generators.DHKeyPairGenerator;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.bouncycastle.crypto.params.DHParameters;
import java.util.Vector;

public class TlsDHEKeyExchange extends TlsDHKeyExchange
{
    protected TlsSignerCredentials serverCredentials;
    
    public TlsDHEKeyExchange(final int n, final Vector vector, final DHParameters dhParameters) {
        super(n, vector, dhParameters);
        this.serverCredentials = null;
    }
    
    @Override
    public void processServerCredentials(final TlsCredentials tlsCredentials) throws IOException {
        if (!(tlsCredentials instanceof TlsSignerCredentials)) {
            throw new TlsFatalAlert((short)80);
        }
        this.processServerCertificate(tlsCredentials.getCertificate());
        this.serverCredentials = (TlsSignerCredentials)tlsCredentials;
    }
    
    @Override
    public byte[] generateServerKeyExchange() throws IOException {
        if (this.dhParameters == null) {
            throw new TlsFatalAlert((short)80);
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final DHKeyPairGenerator dhKeyPairGenerator = new DHKeyPairGenerator();
        dhKeyPairGenerator.init(new DHKeyGenerationParameters(this.context.getSecureRandom(), this.dhParameters));
        final BigInteger y = ((DHPublicKeyParameters)dhKeyPairGenerator.generateKeyPair().getPublic()).getY();
        TlsDHUtils.writeDHParameter(this.dhParameters.getP(), byteArrayOutputStream);
        TlsDHUtils.writeDHParameter(this.dhParameters.getG(), byteArrayOutputStream);
        TlsDHUtils.writeDHParameter(y, byteArrayOutputStream);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        final CombinedHash combinedHash = new CombinedHash();
        final SecurityParameters securityParameters = this.context.getSecurityParameters();
        combinedHash.update(securityParameters.clientRandom, 0, securityParameters.clientRandom.length);
        combinedHash.update(securityParameters.serverRandom, 0, securityParameters.serverRandom.length);
        combinedHash.update(byteArray, 0, byteArray.length);
        final byte[] array = new byte[combinedHash.getDigestSize()];
        combinedHash.doFinal(array, 0);
        TlsUtils.writeOpaque16(this.serverCredentials.generateCertificateSignature(array), byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    @Override
    public void processServerKeyExchange(final InputStream inputStream) throws IOException {
        final Signer initVerifyer = this.initVerifyer(this.tlsSigner, this.context.getSecurityParameters());
        final SignerInputStream signerInputStream = new SignerInputStream(inputStream, initVerifyer);
        final BigInteger dhParameter = TlsDHUtils.readDHParameter(signerInputStream);
        final BigInteger dhParameter2 = TlsDHUtils.readDHParameter(signerInputStream);
        final BigInteger dhParameter3 = TlsDHUtils.readDHParameter(signerInputStream);
        if (!initVerifyer.verifySignature(TlsUtils.readOpaque16(inputStream))) {
            throw new TlsFatalAlert((short)51);
        }
        this.dhAgreeServerPublicKey = this.validateDHPublicKey(new DHPublicKeyParameters(dhParameter3, new DHParameters(dhParameter, dhParameter2)));
    }
    
    protected Signer initVerifyer(final TlsSigner tlsSigner, final SecurityParameters securityParameters) {
        final Signer verifyer = tlsSigner.createVerifyer(this.serverPublicKey);
        verifyer.update(securityParameters.clientRandom, 0, securityParameters.clientRandom.length);
        verifyer.update(securityParameters.serverRandom, 0, securityParameters.serverRandom.length);
        return verifyer;
    }
}
