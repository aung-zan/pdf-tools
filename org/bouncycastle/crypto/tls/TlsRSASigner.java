// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.encodings.PKCS1Encoding;
import org.bouncycastle.crypto.engines.RSABlindedEngine;
import org.bouncycastle.crypto.signers.GenericSigner;
import org.bouncycastle.crypto.signers.RSADigestSigner;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.Signer;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class TlsRSASigner extends AbstractTlsSigner
{
    public byte[] generateRawSignature(final AsymmetricKeyParameter asymmetricKeyParameter, final byte[] array) throws CryptoException {
        final AsymmetricBlockCipher rsaImpl = this.createRSAImpl();
        rsaImpl.init(true, new ParametersWithRandom(asymmetricKeyParameter, this.context.getSecureRandom()));
        return rsaImpl.processBlock(array, 0, array.length);
    }
    
    public boolean verifyRawSignature(final byte[] array, final AsymmetricKeyParameter asymmetricKeyParameter, final byte[] array2) throws CryptoException {
        final AsymmetricBlockCipher rsaImpl = this.createRSAImpl();
        rsaImpl.init(false, asymmetricKeyParameter);
        return Arrays.constantTimeAreEqual(rsaImpl.processBlock(array, 0, array.length), array2);
    }
    
    public Signer createSigner(final AsymmetricKeyParameter asymmetricKeyParameter) {
        return this.makeSigner(new CombinedHash(), true, new ParametersWithRandom(asymmetricKeyParameter, this.context.getSecureRandom()));
    }
    
    public Signer createVerifyer(final AsymmetricKeyParameter asymmetricKeyParameter) {
        return this.makeSigner(new CombinedHash(), false, asymmetricKeyParameter);
    }
    
    public boolean isValidPublicKey(final AsymmetricKeyParameter asymmetricKeyParameter) {
        return asymmetricKeyParameter instanceof RSAKeyParameters && !asymmetricKeyParameter.isPrivate();
    }
    
    protected Signer makeSigner(final Digest digest, final boolean b, final CipherParameters cipherParameters) {
        Signer signer;
        if (ProtocolVersion.TLSv12.isEqualOrEarlierVersionOf(this.context.getServerVersion().getEquivalentTLSVersion())) {
            signer = new RSADigestSigner(digest);
        }
        else {
            signer = new GenericSigner(this.createRSAImpl(), digest);
        }
        signer.init(b, cipherParameters);
        return signer;
    }
    
    protected AsymmetricBlockCipher createRSAImpl() {
        return new PKCS1Encoding(new RSABlindedEngine());
    }
}
