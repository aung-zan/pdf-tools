// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.io.SignerInputStream;
import java.io.InputStream;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.ECDomainParameters;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import java.io.IOException;
import java.util.Vector;

public class TlsECDHEKeyExchange extends TlsECDHKeyExchange
{
    protected TlsSignerCredentials serverCredentials;
    
    public TlsECDHEKeyExchange(final int n, final Vector vector, final int[] array, final short[] array2, final short[] array3) {
        super(n, vector, array, array2, array3);
        this.serverCredentials = null;
    }
    
    @Override
    public void processServerCredentials(final TlsCredentials tlsCredentials) throws IOException {
        if (!(tlsCredentials instanceof TlsSignerCredentials)) {
            throw new TlsFatalAlert((short)80);
        }
        this.processServerCertificate(tlsCredentials.getCertificate());
        this.serverCredentials = (TlsSignerCredentials)tlsCredentials;
    }
    
    @Override
    public byte[] generateServerKeyExchange() throws IOException {
        int n = -1;
        if (this.namedCurves == null) {
            n = 23;
        }
        else {
            for (int i = 0; i < this.namedCurves.length; ++i) {
                final int n2 = this.namedCurves[i];
                if (TlsECCUtils.isSupportedNamedCurve(n2)) {
                    n = n2;
                    break;
                }
            }
        }
        ECDomainParameters ecDomainParameters = null;
        if (n >= 0) {
            ecDomainParameters = TlsECCUtils.getParametersForNamedCurve(n);
        }
        else if (TlsProtocol.arrayContains(this.namedCurves, 65281)) {
            ecDomainParameters = TlsECCUtils.getParametersForNamedCurve(23);
        }
        else if (TlsProtocol.arrayContains(this.namedCurves, 65282)) {
            ecDomainParameters = TlsECCUtils.getParametersForNamedCurve(7);
        }
        if (ecDomainParameters == null) {
            throw new TlsFatalAlert((short)80);
        }
        final AsymmetricCipherKeyPair generateECKeyPair = TlsECCUtils.generateECKeyPair(this.context.getSecureRandom(), ecDomainParameters);
        this.ecAgreeServerPrivateKey = (ECPrivateKeyParameters)generateECKeyPair.getPrivate();
        final byte[] serializeECPublicKey = TlsECCUtils.serializeECPublicKey(this.clientECPointFormats, (ECPublicKeyParameters)generateECKeyPair.getPublic());
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (n < 0) {
            TlsECCUtils.writeExplicitECParameters(this.clientECPointFormats, ecDomainParameters, byteArrayOutputStream);
        }
        else {
            TlsECCUtils.writeNamedECParameters(n, byteArrayOutputStream);
        }
        TlsUtils.writeOpaque8(serializeECPublicKey, byteArrayOutputStream);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        final CombinedHash combinedHash = new CombinedHash();
        final SecurityParameters securityParameters = this.context.getSecurityParameters();
        combinedHash.update(securityParameters.clientRandom, 0, securityParameters.clientRandom.length);
        combinedHash.update(securityParameters.serverRandom, 0, securityParameters.serverRandom.length);
        combinedHash.update(byteArray, 0, byteArray.length);
        final byte[] array = new byte[combinedHash.getDigestSize()];
        combinedHash.doFinal(array, 0);
        TlsUtils.writeOpaque16(this.serverCredentials.generateCertificateSignature(array), byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    @Override
    public void processServerKeyExchange(final InputStream inputStream) throws IOException {
        final Signer initVerifyer = this.initVerifyer(this.tlsSigner, this.context.getSecurityParameters());
        final SignerInputStream signerInputStream = new SignerInputStream(inputStream, initVerifyer);
        final ECDomainParameters ecParameters = TlsECCUtils.readECParameters(this.namedCurves, this.clientECPointFormats, signerInputStream);
        final byte[] opaque8 = TlsUtils.readOpaque8(signerInputStream);
        if (!initVerifyer.verifySignature(TlsUtils.readOpaque16(inputStream))) {
            throw new TlsFatalAlert((short)51);
        }
        this.ecAgreeServerPublicKey = TlsECCUtils.validateECPublicKey(TlsECCUtils.deserializeECPublicKey(this.clientECPointFormats, ecParameters, opaque8));
    }
    
    @Override
    public void validateCertificateRequest(final CertificateRequest certificateRequest) throws IOException {
        final short[] certificateTypes = certificateRequest.getCertificateTypes();
        int i = 0;
        while (i < certificateTypes.length) {
            switch (certificateTypes[i]) {
                case 1:
                case 2:
                case 64: {
                    ++i;
                    continue;
                }
                default: {
                    throw new TlsFatalAlert((short)47);
                }
            }
        }
    }
    
    @Override
    public void processClientCredentials(final TlsCredentials tlsCredentials) throws IOException {
        if (tlsCredentials instanceof TlsSignerCredentials) {
            return;
        }
        throw new TlsFatalAlert((short)80);
    }
    
    protected Signer initVerifyer(final TlsSigner tlsSigner, final SecurityParameters securityParameters) {
        final Signer verifyer = tlsSigner.createVerifyer(this.serverPublicKey);
        verifyer.update(securityParameters.clientRandom, 0, securityParameters.clientRandom.length);
        verifyer.update(securityParameters.serverRandom, 0, securityParameters.serverRandom.length);
        return verifyer;
    }
}
