// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

public interface TlsPSKIdentity
{
    void skipIdentityHint();
    
    void notifyIdentityHint(final byte[] p0);
    
    byte[] getPSKIdentity();
    
    byte[] getPSK();
}
