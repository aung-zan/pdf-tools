// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.ByteArrayOutputStream;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import java.util.Vector;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.io.OutputStream;
import java.io.InputStream;
import java.util.Hashtable;

public class TlsServerProtocol extends TlsProtocol
{
    protected TlsServer tlsServer;
    protected TlsServerContextImpl tlsServerContext;
    protected int[] offeredCipherSuites;
    protected short[] offeredCompressionMethods;
    protected Hashtable clientExtensions;
    protected int selectedCipherSuite;
    protected short selectedCompressionMethod;
    protected Hashtable serverExtensions;
    protected TlsKeyExchange keyExchange;
    protected TlsCredentials serverCredentials;
    protected CertificateRequest certificateRequest;
    protected short clientCertificateType;
    protected Certificate clientCertificate;
    protected byte[] certificateVerifyHash;
    
    public TlsServerProtocol(final InputStream inputStream, final OutputStream outputStream, final SecureRandom secureRandom) {
        super(inputStream, outputStream, secureRandom);
        this.tlsServer = null;
        this.tlsServerContext = null;
        this.keyExchange = null;
        this.serverCredentials = null;
        this.certificateRequest = null;
        this.clientCertificateType = -1;
        this.clientCertificate = null;
        this.certificateVerifyHash = null;
    }
    
    public void accept(final TlsServer tlsServer) throws IOException {
        if (tlsServer == null) {
            throw new IllegalArgumentException("'tlsServer' cannot be null");
        }
        if (this.tlsServer != null) {
            throw new IllegalStateException("accept can only be called once");
        }
        this.tlsServer = tlsServer;
        this.securityParameters = new SecurityParameters();
        this.securityParameters.entity = 0;
        this.securityParameters.serverRandom = TlsProtocol.createRandomBlock(this.secureRandom);
        this.tlsServerContext = new TlsServerContextImpl(this.secureRandom, this.securityParameters);
        this.tlsServer.init(this.tlsServerContext);
        this.recordStream.init(this.tlsServerContext);
        this.recordStream.setRestrictReadVersion(false);
        this.completeHandshake();
        this.tlsServer.notifyHandshakeComplete();
    }
    
    @Override
    protected AbstractTlsContext getContext() {
        return this.tlsServerContext;
    }
    
    @Override
    protected TlsPeer getPeer() {
        return this.tlsServer;
    }
    
    @Override
    protected void handleChangeCipherSpecMessage() throws IOException {
        switch (this.connection_state) {
            case 10: {
                if (this.certificateVerifyHash != null) {
                    this.failWithError((short)2, (short)10);
                }
            }
            case 11: {
                this.connection_state = 12;
                break;
            }
            default: {
                this.failWithError((short)2, (short)40);
                break;
            }
        }
    }
    
    @Override
    protected void handleHandshakeMessage(final short n, final byte[] buf) throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        Label_0869: {
            switch (n) {
                case 1: {
                    switch (this.connection_state) {
                        case 0: {
                            this.receiveClientHelloMessage(byteArrayInputStream);
                            this.connection_state = 1;
                            this.sendServerHelloMessage();
                            this.connection_state = 2;
                            this.securityParameters.prfAlgorithm = TlsProtocol.getPRFAlgorithm(this.selectedCipherSuite);
                            this.securityParameters.compressionAlgorithm = this.selectedCompressionMethod;
                            this.securityParameters.verifyDataLength = 12;
                            this.recordStream.notifyHelloComplete();
                            final Vector serverSupplementalData = this.tlsServer.getServerSupplementalData();
                            if (serverSupplementalData != null) {
                                this.sendSupplementalDataMessage(serverSupplementalData);
                            }
                            this.connection_state = 3;
                            (this.keyExchange = this.tlsServer.getKeyExchange()).init(this.getContext());
                            this.serverCredentials = this.tlsServer.getCredentials();
                            if (this.serverCredentials == null) {
                                this.keyExchange.skipServerCredentials();
                            }
                            else {
                                this.keyExchange.processServerCredentials(this.serverCredentials);
                                this.sendCertificateMessage(this.serverCredentials.getCertificate());
                            }
                            this.connection_state = 4;
                            final byte[] generateServerKeyExchange = this.keyExchange.generateServerKeyExchange();
                            if (generateServerKeyExchange != null) {
                                this.sendServerKeyExchangeMessage(generateServerKeyExchange);
                            }
                            this.connection_state = 5;
                            if (this.serverCredentials != null) {
                                this.certificateRequest = this.tlsServer.getCertificateRequest();
                                if (this.certificateRequest != null) {
                                    this.keyExchange.validateCertificateRequest(this.certificateRequest);
                                    this.sendCertificateRequestMessage(this.certificateRequest);
                                }
                            }
                            this.connection_state = 6;
                            this.sendServerHelloDoneMessage();
                            this.connection_state = 7;
                            break Label_0869;
                        }
                        default: {
                            this.failWithError((short)2, (short)10);
                            break Label_0869;
                        }
                    }
                    break;
                }
                case 23: {
                    switch (this.connection_state) {
                        case 7: {
                            this.tlsServer.processClientSupplementalData(TlsProtocol.readSupplementalDataMessage(byteArrayInputStream));
                            this.connection_state = 8;
                            break Label_0869;
                        }
                        default: {
                            this.failWithError((short)2, (short)10);
                            break Label_0869;
                        }
                    }
                    break;
                }
                case 11: {
                    switch (this.connection_state) {
                        case 7: {
                            this.tlsServer.processClientSupplementalData(null);
                        }
                        case 8: {
                            if (this.certificateRequest == null) {
                                this.failWithError((short)2, (short)10);
                            }
                            this.receiveCertificateMessage(byteArrayInputStream);
                            this.connection_state = 9;
                            break Label_0869;
                        }
                        default: {
                            this.failWithError((short)2, (short)10);
                            break Label_0869;
                        }
                    }
                    break;
                }
                case 16: {
                    Label_0686: {
                        switch (this.connection_state) {
                            case 7: {
                                this.tlsServer.processClientSupplementalData(null);
                            }
                            case 8: {
                                if (this.certificateRequest == null) {
                                    this.keyExchange.skipClientCredentials();
                                    break Label_0686;
                                }
                                final ProtocolVersion equivalentTLSVersion = this.getContext().getServerVersion().getEquivalentTLSVersion();
                                if (ProtocolVersion.TLSv12.isEqualOrEarlierVersionOf(equivalentTLSVersion)) {
                                    this.failWithError((short)2, (short)10);
                                    break Label_0686;
                                }
                                if (!equivalentTLSVersion.isSSL()) {
                                    this.notifyClientCertificate(Certificate.EMPTY_CHAIN);
                                    break Label_0686;
                                }
                                if (this.clientCertificate == null) {
                                    this.failWithError((short)2, (short)10);
                                }
                                break Label_0686;
                            }
                            case 9: {
                                this.receiveClientKeyExchangeMessage(byteArrayInputStream);
                                this.connection_state = 10;
                                break Label_0869;
                            }
                            default: {
                                this.failWithError((short)2, (short)10);
                                break Label_0869;
                            }
                        }
                    }
                    break;
                }
                case 15: {
                    switch (this.connection_state) {
                        case 10: {
                            if (this.certificateVerifyHash == null) {
                                this.failWithError((short)2, (short)10);
                            }
                            this.receiveCertificateVerifyMessage(byteArrayInputStream);
                            this.connection_state = 11;
                            break Label_0869;
                        }
                        default: {
                            this.failWithError((short)2, (short)10);
                            break Label_0869;
                        }
                    }
                    break;
                }
                case 20: {
                    switch (this.connection_state) {
                        case 12: {
                            this.processFinishedMessage(byteArrayInputStream);
                            this.connection_state = 13;
                            if (this.expectSessionTicket) {
                                this.sendNewSessionTicketMessage(this.tlsServer.getNewSessionTicket());
                            }
                            this.connection_state = 14;
                            this.sendChangeCipherSpecMessage();
                            this.connection_state = 15;
                            this.sendFinishedMessage();
                            this.connection_state = 16;
                            break Label_0869;
                        }
                        default: {
                            this.failWithError((short)2, (short)10);
                            break Label_0869;
                        }
                    }
                    break;
                }
                default: {
                    this.failWithError((short)2, (short)10);
                    break;
                }
            }
        }
    }
    
    @Override
    protected void handleWarningMessage(final short n) throws IOException {
        switch (n) {
            case 41: {
                if (this.getContext().getServerVersion().isSSL() && this.certificateRequest != null) {
                    this.notifyClientCertificate(Certificate.EMPTY_CHAIN);
                    break;
                }
                break;
            }
            default: {
                super.handleWarningMessage(n);
                break;
            }
        }
    }
    
    protected void notifyClientCertificate(final Certificate clientCertificate) throws IOException {
        if (this.certificateRequest == null) {
            throw new IllegalStateException();
        }
        if (this.clientCertificate != null) {
            throw new TlsFatalAlert((short)10);
        }
        this.clientCertificate = clientCertificate;
        if (clientCertificate.isEmpty()) {
            this.keyExchange.skipClientCredentials();
        }
        else {
            this.clientCertificateType = TlsUtils.getClientCertificateType(clientCertificate, this.serverCredentials.getCertificate());
            this.keyExchange.processClientCertificate(clientCertificate);
        }
        this.tlsServer.notifyClientCertificate(clientCertificate);
    }
    
    protected void receiveCertificateMessage(final ByteArrayInputStream byteArrayInputStream) throws IOException {
        final Certificate parse = Certificate.parse(byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        this.notifyClientCertificate(parse);
    }
    
    protected void receiveCertificateVerifyMessage(final ByteArrayInputStream byteArrayInputStream) throws IOException {
        final byte[] opaque16 = TlsUtils.readOpaque16(byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        try {
            final TlsSigner tlsSigner = TlsUtils.createTlsSigner(this.clientCertificateType);
            tlsSigner.init(this.getContext());
            tlsSigner.verifyRawSignature(opaque16, PublicKeyFactory.createKey(this.clientCertificate.getCertificateAt(0).getSubjectPublicKeyInfo()), this.certificateVerifyHash);
        }
        catch (Exception ex) {
            throw new TlsFatalAlert((short)51);
        }
    }
    
    protected void receiveClientHelloMessage(final ByteArrayInputStream byteArrayInputStream) throws IOException {
        final ProtocolVersion version = TlsUtils.readVersion(byteArrayInputStream);
        if (version.isDTLS()) {
            this.failWithError((short)2, (short)47);
        }
        final byte[] fully = TlsUtils.readFully(32, byteArrayInputStream);
        if (TlsUtils.readOpaque8(byteArrayInputStream).length > 32) {
            this.failWithError((short)2, (short)47);
        }
        final int uint16 = TlsUtils.readUint16(byteArrayInputStream);
        if (uint16 < 2 || (uint16 & 0x1) != 0x0) {
            this.failWithError((short)2, (short)50);
        }
        this.offeredCipherSuites = TlsUtils.readUint16Array(uint16 / 2, byteArrayInputStream);
        final short uint17 = TlsUtils.readUint8(byteArrayInputStream);
        if (uint17 < 1) {
            this.failWithError((short)2, (short)47);
        }
        this.offeredCompressionMethods = TlsUtils.readUint8Array(uint17, byteArrayInputStream);
        this.clientExtensions = TlsProtocol.readExtensions(byteArrayInputStream);
        this.getContext().setClientVersion(version);
        this.tlsServer.notifyClientVersion(version);
        this.securityParameters.clientRandom = fully;
        this.tlsServer.notifyOfferedCipherSuites(this.offeredCipherSuites);
        this.tlsServer.notifyOfferedCompressionMethods(this.offeredCompressionMethods);
        if (TlsProtocol.arrayContains(this.offeredCipherSuites, 255)) {
            this.secure_renegotiation = true;
        }
        if (this.clientExtensions != null) {
            final byte[] array = this.clientExtensions.get(TlsServerProtocol.EXT_RenegotiationInfo);
            if (array != null) {
                this.secure_renegotiation = true;
                if (!Arrays.constantTimeAreEqual(array, TlsProtocol.createRenegotiationInfo(TlsUtils.EMPTY_BYTES))) {
                    this.failWithError((short)2, (short)40);
                }
            }
        }
        this.tlsServer.notifySecureRenegotiation(this.secure_renegotiation);
        if (this.clientExtensions != null) {
            this.tlsServer.processClientExtensions(this.clientExtensions);
        }
    }
    
    protected void receiveClientKeyExchangeMessage(final ByteArrayInputStream byteArrayInputStream) throws IOException {
        this.keyExchange.processClientKeyExchange(byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        TlsProtocol.establishMasterSecret(this.getContext(), this.keyExchange);
        this.recordStream.setPendingConnectionState(this.tlsServer.getCompression(), this.tlsServer.getCipher());
        if (this.expectCertificateVerifyMessage()) {
            this.certificateVerifyHash = this.recordStream.getCurrentHash(null);
        }
    }
    
    protected void sendCertificateRequestMessage(final CertificateRequest certificateRequest) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8((short)13, byteArrayOutputStream);
        TlsUtils.writeUint24(0, byteArrayOutputStream);
        certificateRequest.encode(byteArrayOutputStream);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        TlsUtils.writeUint24(byteArray.length - 4, byteArray, 1);
        this.safeWriteRecord((short)22, byteArray, 0, byteArray.length);
    }
    
    protected void sendNewSessionTicketMessage(final NewSessionTicket newSessionTicket) throws IOException {
        if (newSessionTicket == null) {
            throw new TlsFatalAlert((short)80);
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8((short)4, byteArrayOutputStream);
        TlsUtils.writeUint24(0, byteArrayOutputStream);
        newSessionTicket.encode(byteArrayOutputStream);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        TlsUtils.writeUint24(byteArray.length - 4, byteArray, 1);
        this.safeWriteRecord((short)22, byteArray, 0, byteArray.length);
    }
    
    protected void sendServerHelloMessage() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8((short)2, byteArrayOutputStream);
        TlsUtils.writeUint24(0, byteArrayOutputStream);
        final ProtocolVersion serverVersion = this.tlsServer.getServerVersion();
        if (!serverVersion.isEqualOrEarlierVersionOf(this.getContext().getClientVersion())) {
            this.failWithError((short)2, (short)80);
        }
        this.recordStream.setReadVersion(serverVersion);
        this.recordStream.setWriteVersion(serverVersion);
        this.recordStream.setRestrictReadVersion(true);
        this.getContext().setServerVersion(serverVersion);
        TlsUtils.writeVersion(serverVersion, byteArrayOutputStream);
        byteArrayOutputStream.write(this.securityParameters.serverRandom);
        TlsUtils.writeOpaque8(TlsUtils.EMPTY_BYTES, byteArrayOutputStream);
        this.selectedCipherSuite = this.tlsServer.getSelectedCipherSuite();
        if (!TlsProtocol.arrayContains(this.offeredCipherSuites, this.selectedCipherSuite) || this.selectedCipherSuite == 0 || this.selectedCipherSuite == 255) {
            this.failWithError((short)2, (short)80);
        }
        this.selectedCompressionMethod = this.tlsServer.getSelectedCompressionMethod();
        if (!TlsProtocol.arrayContains(this.offeredCompressionMethods, this.selectedCompressionMethod)) {
            this.failWithError((short)2, (short)80);
        }
        TlsUtils.writeUint16(this.selectedCipherSuite, byteArrayOutputStream);
        TlsUtils.writeUint8(this.selectedCompressionMethod, byteArrayOutputStream);
        this.serverExtensions = this.tlsServer.getServerExtensions();
        if (this.secure_renegotiation && (this.serverExtensions == null || !this.serverExtensions.containsKey(TlsServerProtocol.EXT_RenegotiationInfo))) {
            if (this.serverExtensions == null) {
                this.serverExtensions = new Hashtable();
            }
            this.serverExtensions.put(TlsServerProtocol.EXT_RenegotiationInfo, TlsProtocol.createRenegotiationInfo(TlsUtils.EMPTY_BYTES));
        }
        if (this.serverExtensions != null) {
            this.expectSessionTicket = this.serverExtensions.containsKey(TlsServerProtocol.EXT_SessionTicket);
            TlsProtocol.writeExtensions(byteArrayOutputStream, this.serverExtensions);
        }
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        TlsUtils.writeUint24(byteArray.length - 4, byteArray, 1);
        this.safeWriteRecord((short)22, byteArray, 0, byteArray.length);
    }
    
    protected void sendServerHelloDoneMessage() throws IOException {
        final byte[] array = new byte[4];
        TlsUtils.writeUint8((short)14, array, 0);
        TlsUtils.writeUint24(0, array, 1);
        this.safeWriteRecord((short)22, array, 0, array.length);
    }
    
    protected void sendServerKeyExchangeMessage(final byte[] b) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8((short)12, byteArrayOutputStream);
        TlsUtils.writeUint24(b.length, byteArrayOutputStream);
        byteArrayOutputStream.write(b);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        this.safeWriteRecord((short)22, byteArray, 0, byteArray.length);
    }
    
    protected boolean expectCertificateVerifyMessage() {
        return this.clientCertificateType >= 0 && TlsUtils.hasSigningCapability(this.clientCertificateType);
    }
}
