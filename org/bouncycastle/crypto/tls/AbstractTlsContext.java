// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.security.SecureRandom;

abstract class AbstractTlsContext implements TlsContext
{
    private SecureRandom secureRandom;
    private SecurityParameters securityParameters;
    private ProtocolVersion clientVersion;
    private ProtocolVersion serverVersion;
    private Object userObject;
    
    AbstractTlsContext(final SecureRandom secureRandom, final SecurityParameters securityParameters) {
        this.clientVersion = null;
        this.serverVersion = null;
        this.userObject = null;
        this.secureRandom = secureRandom;
        this.securityParameters = securityParameters;
    }
    
    public SecureRandom getSecureRandom() {
        return this.secureRandom;
    }
    
    public SecurityParameters getSecurityParameters() {
        return this.securityParameters;
    }
    
    public ProtocolVersion getClientVersion() {
        return this.clientVersion;
    }
    
    public void setClientVersion(final ProtocolVersion clientVersion) {
        this.clientVersion = clientVersion;
    }
    
    public ProtocolVersion getServerVersion() {
        return this.serverVersion;
    }
    
    public void setServerVersion(final ProtocolVersion serverVersion) {
        this.serverVersion = serverVersion;
    }
    
    public Object getUserObject() {
        return this.userObject;
    }
    
    public void setUserObject(final Object userObject) {
        this.userObject = userObject;
    }
    
    public byte[] exportKeyingMaterial(final String s, final byte[] array, final int n) {
        final SecurityParameters securityParameters = this.getSecurityParameters();
        final byte[] clientRandom = securityParameters.getClientRandom();
        final byte[] serverRandom = securityParameters.getServerRandom();
        int n2 = clientRandom.length + serverRandom.length;
        if (array != null) {
            n2 += 2 + array.length;
        }
        final byte[] array2 = new byte[n2];
        final int n3 = 0;
        System.arraycopy(clientRandom, 0, array2, n3, clientRandom.length);
        final int n4 = n3 + clientRandom.length;
        System.arraycopy(serverRandom, 0, array2, n4, serverRandom.length);
        int n5 = n4 + serverRandom.length;
        if (array != null) {
            TlsUtils.writeUint16(array.length, array2, n5);
            n5 += 2;
            System.arraycopy(array, 0, array2, n5, array.length);
            n5 += array.length;
        }
        if (n5 != n2) {
            throw new IllegalStateException("error in calculation of seed for export");
        }
        return TlsUtils.PRF(this, securityParameters.getMasterSecret(), s, array2, n);
    }
}
