// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;

public class LegacyTlsAuthentication extends ServerOnlyTlsAuthentication
{
    protected CertificateVerifyer verifyer;
    
    public LegacyTlsAuthentication(final CertificateVerifyer verifyer) {
        this.verifyer = verifyer;
    }
    
    public void notifyServerCertificate(final Certificate certificate) throws IOException {
        if (!this.verifyer.isValid(certificate.getCertificateList())) {
            throw new TlsFatalAlert((short)90);
        }
    }
}
