// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.io.OutputStream;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import java.io.IOException;
import java.util.Vector;
import org.bouncycastle.crypto.params.DHPrivateKeyParameters;
import org.bouncycastle.crypto.params.DHPublicKeyParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.DHParameters;
import java.math.BigInteger;

public class TlsDHKeyExchange extends AbstractTlsKeyExchange
{
    protected static final BigInteger ONE;
    protected static final BigInteger TWO;
    protected TlsSigner tlsSigner;
    protected DHParameters dhParameters;
    protected AsymmetricKeyParameter serverPublicKey;
    protected DHPublicKeyParameters dhAgreeServerPublicKey;
    protected TlsAgreementCredentials agreementCredentials;
    protected DHPrivateKeyParameters dhAgreeClientPrivateKey;
    protected DHPublicKeyParameters dhAgreeClientPublicKey;
    
    public TlsDHKeyExchange(final int n, final Vector vector, final DHParameters dhParameters) {
        super(n, vector);
        switch (n) {
            case 7:
            case 9: {
                this.tlsSigner = null;
                break;
            }
            case 5: {
                this.tlsSigner = new TlsRSASigner();
                break;
            }
            case 3: {
                this.tlsSigner = new TlsDSSSigner();
                break;
            }
            default: {
                throw new IllegalArgumentException("unsupported key exchange algorithm");
            }
        }
        this.dhParameters = dhParameters;
    }
    
    @Override
    public void init(final TlsContext tlsContext) {
        super.init(tlsContext);
        if (this.tlsSigner != null) {
            this.tlsSigner.init(tlsContext);
        }
    }
    
    public void skipServerCredentials() throws IOException {
        throw new TlsFatalAlert((short)10);
    }
    
    @Override
    public void processServerCertificate(final Certificate certificate) throws IOException {
        if (certificate.isEmpty()) {
            throw new TlsFatalAlert((short)42);
        }
        final org.bouncycastle.asn1.x509.Certificate certificate2 = certificate.getCertificateAt(0);
        final SubjectPublicKeyInfo subjectPublicKeyInfo = certificate2.getSubjectPublicKeyInfo();
        try {
            this.serverPublicKey = PublicKeyFactory.createKey(subjectPublicKeyInfo);
        }
        catch (RuntimeException ex) {
            throw new TlsFatalAlert((short)43);
        }
        if (this.tlsSigner == null) {
            try {
                this.dhAgreeServerPublicKey = this.validateDHPublicKey((DHPublicKeyParameters)this.serverPublicKey);
            }
            catch (ClassCastException ex2) {
                throw new TlsFatalAlert((short)46);
            }
            TlsUtils.validateKeyUsage(certificate2, 8);
        }
        else {
            if (!this.tlsSigner.isValidPublicKey(this.serverPublicKey)) {
                throw new TlsFatalAlert((short)46);
            }
            TlsUtils.validateKeyUsage(certificate2, 128);
        }
        super.processServerCertificate(certificate);
    }
    
    @Override
    public boolean requiresServerKeyExchange() {
        switch (this.keyExchange) {
            case 3:
            case 5:
            case 11: {
                return true;
            }
            default: {
                return false;
            }
        }
    }
    
    public void validateCertificateRequest(final CertificateRequest certificateRequest) throws IOException {
        final short[] certificateTypes = certificateRequest.getCertificateTypes();
        int i = 0;
        while (i < certificateTypes.length) {
            switch (certificateTypes[i]) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 64: {
                    ++i;
                    continue;
                }
                default: {
                    throw new TlsFatalAlert((short)47);
                }
            }
        }
    }
    
    public void processClientCredentials(final TlsCredentials tlsCredentials) throws IOException {
        if (tlsCredentials instanceof TlsAgreementCredentials) {
            this.agreementCredentials = (TlsAgreementCredentials)tlsCredentials;
        }
        else if (!(tlsCredentials instanceof TlsSignerCredentials)) {
            throw new TlsFatalAlert((short)80);
        }
    }
    
    public void generateClientKeyExchange(final OutputStream outputStream) throws IOException {
        if (this.agreementCredentials == null) {
            this.dhAgreeClientPrivateKey = TlsDHUtils.generateEphemeralClientKeyExchange(this.context.getSecureRandom(), this.dhAgreeServerPublicKey.getParameters(), outputStream);
        }
    }
    
    public byte[] generatePremasterSecret() throws IOException {
        if (this.agreementCredentials != null) {
            return this.agreementCredentials.generateAgreement(this.dhAgreeServerPublicKey);
        }
        return this.calculateDHBasicAgreement(this.dhAgreeServerPublicKey, this.dhAgreeClientPrivateKey);
    }
    
    protected boolean areCompatibleParameters(final DHParameters dhParameters, final DHParameters dhParameters2) {
        return dhParameters.getP().equals(dhParameters2.getP()) && dhParameters.getG().equals(dhParameters2.getG());
    }
    
    protected byte[] calculateDHBasicAgreement(final DHPublicKeyParameters dhPublicKeyParameters, final DHPrivateKeyParameters dhPrivateKeyParameters) {
        return TlsDHUtils.calculateDHBasicAgreement(dhPublicKeyParameters, dhPrivateKeyParameters);
    }
    
    protected AsymmetricCipherKeyPair generateDHKeyPair(final DHParameters dhParameters) {
        return TlsDHUtils.generateDHKeyPair(this.context.getSecureRandom(), dhParameters);
    }
    
    protected DHPublicKeyParameters validateDHPublicKey(final DHPublicKeyParameters dhPublicKeyParameters) throws IOException {
        return TlsDHUtils.validateDHPublicKey(dhPublicKeyParameters);
    }
    
    static {
        ONE = BigInteger.valueOf(1L);
        TWO = BigInteger.valueOf(2L);
    }
}
