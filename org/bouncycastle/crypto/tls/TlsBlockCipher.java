// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.security.SecureRandom;
import java.io.IOException;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.BlockCipher;

public class TlsBlockCipher implements TlsCipher
{
    protected TlsContext context;
    protected byte[] randomData;
    protected boolean useExplicitIV;
    protected BlockCipher encryptCipher;
    protected BlockCipher decryptCipher;
    protected TlsMac writeMac;
    protected TlsMac readMac;
    
    public TlsMac getWriteMac() {
        return this.writeMac;
    }
    
    public TlsMac getReadMac() {
        return this.readMac;
    }
    
    public TlsBlockCipher(final TlsContext context, final BlockCipher blockCipher, final BlockCipher blockCipher2, final Digest digest, final Digest digest2, final int n) throws IOException {
        this.context = context;
        this.randomData = new byte[256];
        context.getSecureRandom().nextBytes(this.randomData);
        this.useExplicitIV = ProtocolVersion.TLSv11.isEqualOrEarlierVersionOf(context.getServerVersion().getEquivalentTLSVersion());
        int n2 = 2 * n + digest.getDigestSize() + digest2.getDigestSize();
        if (!this.useExplicitIV) {
            n2 += blockCipher.getBlockSize() + blockCipher2.getBlockSize();
        }
        final byte[] calculateKeyBlock = TlsUtils.calculateKeyBlock(context, n2);
        final int n3 = 0;
        final TlsMac tlsMac = new TlsMac(context, digest, calculateKeyBlock, n3, digest.getDigestSize());
        final int n4 = n3 + digest.getDigestSize();
        final TlsMac tlsMac2 = new TlsMac(context, digest2, calculateKeyBlock, n4, digest2.getDigestSize());
        final int n5 = n4 + digest2.getDigestSize();
        final KeyParameter keyParameter = new KeyParameter(calculateKeyBlock, n5, n);
        final int n6 = n5 + n;
        final KeyParameter keyParameter2 = new KeyParameter(calculateKeyBlock, n6, n);
        int n7 = n6 + n;
        byte[] copyOfRange;
        byte[] copyOfRange2;
        if (this.useExplicitIV) {
            copyOfRange = new byte[blockCipher.getBlockSize()];
            copyOfRange2 = new byte[blockCipher2.getBlockSize()];
        }
        else {
            copyOfRange = Arrays.copyOfRange(calculateKeyBlock, n7, n7 + blockCipher.getBlockSize());
            final int n8 = n7 + blockCipher.getBlockSize();
            copyOfRange2 = Arrays.copyOfRange(calculateKeyBlock, n8, n8 + blockCipher2.getBlockSize());
            n7 = n8 + blockCipher2.getBlockSize();
        }
        if (n7 != n2) {
            throw new TlsFatalAlert((short)80);
        }
        ParametersWithIV parametersWithIV;
        ParametersWithIV parametersWithIV2;
        if (context.isServer()) {
            this.writeMac = tlsMac2;
            this.readMac = tlsMac;
            this.encryptCipher = blockCipher2;
            this.decryptCipher = blockCipher;
            parametersWithIV = new ParametersWithIV(keyParameter2, copyOfRange2);
            parametersWithIV2 = new ParametersWithIV(keyParameter, copyOfRange);
        }
        else {
            this.writeMac = tlsMac;
            this.readMac = tlsMac2;
            this.encryptCipher = blockCipher;
            this.decryptCipher = blockCipher2;
            parametersWithIV = new ParametersWithIV(keyParameter, copyOfRange);
            parametersWithIV2 = new ParametersWithIV(keyParameter2, copyOfRange2);
        }
        this.encryptCipher.init(true, parametersWithIV);
        this.decryptCipher.init(false, parametersWithIV2);
    }
    
    public int getPlaintextLimit(final int n) {
        final int blockSize = this.encryptCipher.getBlockSize();
        int n2 = n - n % blockSize - this.writeMac.getSize() - 1;
        if (this.useExplicitIV) {
            n2 -= blockSize;
        }
        return n2;
    }
    
    public byte[] encodePlaintext(final long n, final short n2, final byte[] array, final int n3, final int n4) {
        final int blockSize = this.encryptCipher.getBlockSize();
        final int size = this.writeMac.getSize();
        final ProtocolVersion serverVersion = this.context.getServerVersion();
        int n5 = blockSize - 1 - (n4 + size) % blockSize;
        if (!serverVersion.isDTLS() && !serverVersion.isSSL()) {
            n5 += this.chooseExtraPadBlocks(this.context.getSecureRandom(), (255 - n5) / blockSize) * blockSize;
        }
        int n6 = n4 + size + n5 + 1;
        if (this.useExplicitIV) {
            n6 += blockSize;
        }
        final byte[] array2 = new byte[n6];
        int n7 = 0;
        if (this.useExplicitIV) {
            final byte[] bytes = new byte[blockSize];
            this.context.getSecureRandom().nextBytes(bytes);
            this.encryptCipher.init(true, new ParametersWithIV(null, bytes));
            System.arraycopy(bytes, 0, array2, n7, blockSize);
            n7 += blockSize;
        }
        final byte[] calculateMac = this.writeMac.calculateMac(n, n2, array, n3, n4);
        System.arraycopy(array, n3, array2, n7, n4);
        System.arraycopy(calculateMac, 0, array2, n7 + n4, calculateMac.length);
        final int n8 = n7 + n4 + calculateMac.length;
        for (int i = 0; i <= n5; ++i) {
            array2[i + n8] = (byte)n5;
        }
        for (int j = n7; j < n6; j += blockSize) {
            this.encryptCipher.processBlock(array2, j, array2, j);
        }
        return array2;
    }
    
    public byte[] decodeCiphertext(final long n, final short n2, final byte[] array, int n3, int n4) throws IOException {
        final int blockSize = this.decryptCipher.getBlockSize();
        final int size = this.readMac.getSize();
        int max = Math.max(blockSize, size + 1);
        if (this.useExplicitIV) {
            max += blockSize;
        }
        if (n4 < max) {
            throw new TlsFatalAlert((short)50);
        }
        if (n4 % blockSize != 0) {
            throw new TlsFatalAlert((short)21);
        }
        if (this.useExplicitIV) {
            this.decryptCipher.init(false, new ParametersWithIV(null, array, n3, blockSize));
            n3 += blockSize;
            n4 -= blockSize;
        }
        for (int i = 0; i < n4; i += blockSize) {
            this.decryptCipher.processBlock(array, n3 + i, array, n3 + i);
        }
        final int checkPaddingConstantTime = this.checkPaddingConstantTime(array, n3, n4, blockSize, size);
        final int n5 = n4 - checkPaddingConstantTime - size;
        if (!Arrays.constantTimeAreEqual(this.readMac.calculateMacConstantTime(n, n2, array, n3, n5, n4 - size, this.randomData), Arrays.copyOfRange(array, n3 + n5, n3 + n5 + size)) || checkPaddingConstantTime == 0) {
            throw new TlsFatalAlert((short)20);
        }
        return Arrays.copyOfRange(array, n3, n3 + n5);
    }
    
    protected int checkPaddingConstantTime(final byte[] array, final int n, final int n2, final int n3, final int n4) {
        final int n5 = n + n2;
        final byte b = array[n5 - 1];
        int n6 = (b & 0xFF) + 1;
        int i = 0;
        byte b2 = 0;
        if ((this.context.getServerVersion().isSSL() && n6 > n3) || n4 + n6 > n2) {
            n6 = 0;
        }
        else {
            int j = n5 - n6;
            do {
                b2 |= (byte)(array[j++] ^ b);
            } while (j < n5);
            i = n6;
            if (b2 != 0) {
                n6 = 0;
            }
        }
        byte[] randomData;
        for (randomData = this.randomData; i < 256; b2 |= (byte)(randomData[i++] ^ b)) {}
        final byte[] array2 = randomData;
        final int n7 = 0;
        array2[n7] ^= b2;
        return n6;
    }
    
    protected int chooseExtraPadBlocks(final SecureRandom secureRandom, final int b) {
        return Math.min(this.lowestBitSet(secureRandom.nextInt()), b);
    }
    
    protected int lowestBitSet(int n) {
        if (n == 0) {
            return 32;
        }
        int n2 = 0;
        while ((n & 0x1) == 0x0) {
            ++n2;
            n >>= 1;
        }
        return n2;
    }
}
