// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.signers.DSASigner;
import org.bouncycastle.crypto.DSA;
import org.bouncycastle.crypto.params.DSAPublicKeyParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class TlsDSSSigner extends TlsDSASigner
{
    public boolean isValidPublicKey(final AsymmetricKeyParameter asymmetricKeyParameter) {
        return asymmetricKeyParameter instanceof DSAPublicKeyParameters;
    }
    
    @Override
    protected DSA createDSAImpl() {
        return new DSASigner();
    }
}
