// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

public class SupplementalDataEntry
{
    private int supp_data_type;
    private byte[] data;
    
    public SupplementalDataEntry(final int supp_data_type, final byte[] data) {
        this.supp_data_type = supp_data_type;
        this.data = data;
    }
    
    public int getDataType() {
        return this.supp_data_type;
    }
    
    public byte[] getData() {
        return this.data;
    }
}
