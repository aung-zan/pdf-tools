// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.encodings.PKCS1Encoding;
import org.bouncycastle.crypto.engines.RSABlindedEngine;
import java.io.OutputStream;
import org.bouncycastle.crypto.params.RSAKeyParameters;

public class TlsRSAUtils
{
    public static byte[] generateEncryptedPreMasterSecret(final TlsContext tlsContext, final RSAKeyParameters rsaKeyParameters, final OutputStream outputStream) throws IOException {
        final byte[] bytes = new byte[48];
        tlsContext.getSecureRandom().nextBytes(bytes);
        TlsUtils.writeVersion(tlsContext.getClientVersion(), bytes, 0);
        final PKCS1Encoding pkcs1Encoding = new PKCS1Encoding(new RSABlindedEngine());
        pkcs1Encoding.init(true, new ParametersWithRandom(rsaKeyParameters, tlsContext.getSecureRandom()));
        try {
            final byte[] processBlock = pkcs1Encoding.processBlock(bytes, 0, bytes.length);
            if (tlsContext.getServerVersion().isSSL()) {
                outputStream.write(processBlock);
            }
            else {
                TlsUtils.writeOpaque16(processBlock, outputStream);
            }
        }
        catch (InvalidCipherTextException ex) {
            throw new TlsFatalAlert((short)80);
        }
        return bytes;
    }
}
