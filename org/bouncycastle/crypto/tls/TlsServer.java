// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.util.Vector;
import java.util.Hashtable;
import java.io.IOException;

public interface TlsServer extends TlsPeer
{
    void init(final TlsServerContext p0);
    
    void notifyClientVersion(final ProtocolVersion p0) throws IOException;
    
    void notifyOfferedCipherSuites(final int[] p0) throws IOException;
    
    void notifyOfferedCompressionMethods(final short[] p0) throws IOException;
    
    void notifySecureRenegotiation(final boolean p0) throws IOException;
    
    void processClientExtensions(final Hashtable p0) throws IOException;
    
    ProtocolVersion getServerVersion() throws IOException;
    
    int getSelectedCipherSuite() throws IOException;
    
    short getSelectedCompressionMethod() throws IOException;
    
    Hashtable getServerExtensions() throws IOException;
    
    Vector getServerSupplementalData() throws IOException;
    
    TlsCredentials getCredentials() throws IOException;
    
    TlsKeyExchange getKeyExchange() throws IOException;
    
    CertificateRequest getCertificateRequest();
    
    void processClientSupplementalData(final Vector p0) throws IOException;
    
    void notifyClientCertificate(final Certificate p0) throws IOException;
    
    TlsCompression getCompression() throws IOException;
    
    TlsCipher getCipher() throws IOException;
    
    NewSessionTicket getNewSessionTicket() throws IOException;
    
    void notifyHandshakeComplete() throws IOException;
}
