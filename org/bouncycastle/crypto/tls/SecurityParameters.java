// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

public class SecurityParameters
{
    int entity;
    int prfAlgorithm;
    short compressionAlgorithm;
    int verifyDataLength;
    byte[] masterSecret;
    byte[] clientRandom;
    byte[] serverRandom;
    
    public SecurityParameters() {
        this.entity = -1;
        this.prfAlgorithm = -1;
        this.compressionAlgorithm = -1;
        this.verifyDataLength = -1;
        this.masterSecret = null;
        this.clientRandom = null;
        this.serverRandom = null;
    }
    
    public int getEntity() {
        return this.entity;
    }
    
    public int getPrfAlgorithm() {
        return this.prfAlgorithm;
    }
    
    public short getCompressionAlgorithm() {
        return this.compressionAlgorithm;
    }
    
    public int getVerifyDataLength() {
        return this.verifyDataLength;
    }
    
    public byte[] getMasterSecret() {
        return this.masterSecret;
    }
    
    public byte[] getClientRandom() {
        return this.clientRandom;
    }
    
    public byte[] getServerRandom() {
        return this.serverRandom;
    }
}
