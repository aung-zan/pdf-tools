// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.Vector;
import java.io.OutputStream;

public class Certificate
{
    public static final Certificate EMPTY_CHAIN;
    protected org.bouncycastle.asn1.x509.Certificate[] certificateList;
    
    public Certificate(final org.bouncycastle.asn1.x509.Certificate[] certificateList) {
        if (certificateList == null) {
            throw new IllegalArgumentException("'certificateList' cannot be null");
        }
        this.certificateList = certificateList;
    }
    
    @Deprecated
    public org.bouncycastle.asn1.x509.Certificate[] getCerts() {
        return this.clone(this.certificateList);
    }
    
    public org.bouncycastle.asn1.x509.Certificate[] getCertificateList() {
        return this.clone(this.certificateList);
    }
    
    public org.bouncycastle.asn1.x509.Certificate getCertificateAt(final int n) {
        return this.certificateList[n];
    }
    
    public int getLength() {
        return this.certificateList.length;
    }
    
    public boolean isEmpty() {
        return this.certificateList.length == 0;
    }
    
    public void encode(final OutputStream outputStream) throws IOException {
        final Vector<byte[]> vector = new Vector<byte[]>(this.certificateList.length);
        int n = 0;
        for (int i = 0; i < this.certificateList.length; ++i) {
            final byte[] encoded = this.certificateList[i].getEncoded("DER");
            vector.addElement(encoded);
            n += encoded.length + 3;
        }
        TlsUtils.writeUint24(n, outputStream);
        for (int j = 0; j < vector.size(); ++j) {
            TlsUtils.writeOpaque24(vector.elementAt(j), outputStream);
        }
    }
    
    public static Certificate parse(final InputStream inputStream) throws IOException {
        int i = TlsUtils.readUint24(inputStream);
        if (i == 0) {
            return Certificate.EMPTY_CHAIN;
        }
        final Vector<org.bouncycastle.asn1.x509.Certificate> vector = new Vector<org.bouncycastle.asn1.x509.Certificate>();
        while (i > 0) {
            final int uint24 = TlsUtils.readUint24(inputStream);
            i -= 3 + uint24;
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(TlsUtils.readFully(uint24, inputStream));
            final ASN1Primitive object = new ASN1InputStream(byteArrayInputStream).readObject();
            TlsProtocol.assertEmpty(byteArrayInputStream);
            vector.addElement(org.bouncycastle.asn1.x509.Certificate.getInstance(object));
        }
        final org.bouncycastle.asn1.x509.Certificate[] array = new org.bouncycastle.asn1.x509.Certificate[vector.size()];
        for (int j = 0; j < vector.size(); ++j) {
            array[j] = vector.elementAt(j);
        }
        return new Certificate(array);
    }
    
    private org.bouncycastle.asn1.x509.Certificate[] clone(final org.bouncycastle.asn1.x509.Certificate[] array) {
        final org.bouncycastle.asn1.x509.Certificate[] array2 = new org.bouncycastle.asn1.x509.Certificate[array.length];
        System.arraycopy(array, 0, array2, 0, array2.length);
        return array2;
    }
    
    static {
        EMPTY_CHAIN = new Certificate(new org.bouncycastle.asn1.x509.Certificate[0]);
    }
}
