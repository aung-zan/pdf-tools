// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.ByteArrayOutputStream;
import java.util.Enumeration;
import org.bouncycastle.util.Arrays;
import java.util.Vector;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;
import org.bouncycastle.crypto.prng.ThreadedSeedGenerator;
import java.security.SecureRandom;
import java.util.Hashtable;

public class TlsClientProtocol extends TlsProtocol
{
    protected TlsClient tlsClient;
    protected TlsClientContextImpl tlsClientContext;
    protected int[] offeredCipherSuites;
    protected short[] offeredCompressionMethods;
    protected Hashtable clientExtensions;
    protected int selectedCipherSuite;
    protected short selectedCompressionMethod;
    protected TlsKeyExchange keyExchange;
    protected TlsAuthentication authentication;
    protected CertificateRequest certificateRequest;
    
    private static SecureRandom createSecureRandom() {
        final ThreadedSeedGenerator threadedSeedGenerator = new ThreadedSeedGenerator();
        final SecureRandom secureRandom = new SecureRandom();
        secureRandom.setSeed(threadedSeedGenerator.generateSeed(20, true));
        return secureRandom;
    }
    
    public TlsClientProtocol(final InputStream inputStream, final OutputStream outputStream) {
        this(inputStream, outputStream, createSecureRandom());
    }
    
    public TlsClientProtocol(final InputStream inputStream, final OutputStream outputStream, final SecureRandom secureRandom) {
        super(inputStream, outputStream, secureRandom);
        this.tlsClient = null;
        this.tlsClientContext = null;
        this.offeredCipherSuites = null;
        this.offeredCompressionMethods = null;
        this.clientExtensions = null;
        this.keyExchange = null;
        this.authentication = null;
        this.certificateRequest = null;
    }
    
    public void connect(final TlsClient tlsClient) throws IOException {
        if (tlsClient == null) {
            throw new IllegalArgumentException("'tlsClient' cannot be null");
        }
        if (this.tlsClient != null) {
            throw new IllegalStateException("connect can only be called once");
        }
        this.tlsClient = tlsClient;
        this.securityParameters = new SecurityParameters();
        this.securityParameters.entity = 1;
        this.securityParameters.clientRandom = TlsProtocol.createRandomBlock(this.secureRandom);
        this.tlsClientContext = new TlsClientContextImpl(this.secureRandom, this.securityParameters);
        this.tlsClient.init(this.tlsClientContext);
        this.recordStream.init(this.tlsClientContext);
        this.sendClientHelloMessage();
        this.connection_state = 1;
        this.completeHandshake();
        this.tlsClient.notifyHandshakeComplete();
    }
    
    @Override
    protected AbstractTlsContext getContext() {
        return this.tlsClientContext;
    }
    
    @Override
    protected TlsPeer getPeer() {
        return this.tlsClient;
    }
    
    @Override
    protected void handleChangeCipherSpecMessage() throws IOException {
        switch (this.connection_state) {
            case 13: {
                if (this.expectSessionTicket) {
                    this.failWithError((short)2, (short)40);
                }
            }
            case 14: {
                this.connection_state = 15;
                break;
            }
            default: {
                this.failWithError((short)2, (short)40);
                break;
            }
        }
    }
    
    @Override
    protected void handleHandshakeMessage(final short n, final byte[] buf) throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        Label_0959: {
            switch (n) {
                case 11: {
                    switch (this.connection_state) {
                        case 2: {
                            this.handleSupplementalData(null);
                        }
                        case 3: {
                            final Certificate parse = Certificate.parse(byteArrayInputStream);
                            TlsProtocol.assertEmpty(byteArrayInputStream);
                            this.keyExchange.processServerCertificate(parse);
                            (this.authentication = this.tlsClient.getAuthentication()).notifyServerCertificate(parse);
                            break;
                        }
                        default: {
                            this.failWithError((short)2, (short)10);
                            break;
                        }
                    }
                    this.connection_state = 4;
                    break;
                }
                case 20: {
                    switch (this.connection_state) {
                        case 15: {
                            this.processFinishedMessage(byteArrayInputStream);
                            this.connection_state = 16;
                            break Label_0959;
                        }
                        default: {
                            this.failWithError((short)2, (short)10);
                            break Label_0959;
                        }
                    }
                    break;
                }
                case 2: {
                    switch (this.connection_state) {
                        case 1: {
                            this.receiveServerHelloMessage(byteArrayInputStream);
                            this.connection_state = 2;
                            this.securityParameters.prfAlgorithm = TlsProtocol.getPRFAlgorithm(this.selectedCipherSuite);
                            this.securityParameters.compressionAlgorithm = this.selectedCompressionMethod;
                            this.securityParameters.verifyDataLength = 12;
                            this.recordStream.notifyHelloComplete();
                            break Label_0959;
                        }
                        default: {
                            this.failWithError((short)2, (short)10);
                            break Label_0959;
                        }
                    }
                    break;
                }
                case 23: {
                    switch (this.connection_state) {
                        case 2: {
                            this.handleSupplementalData(TlsProtocol.readSupplementalDataMessage(byteArrayInputStream));
                            break Label_0959;
                        }
                        default: {
                            this.failWithError((short)2, (short)10);
                            break Label_0959;
                        }
                    }
                    break;
                }
                case 14: {
                    switch (this.connection_state) {
                        case 2: {
                            this.handleSupplementalData(null);
                        }
                        case 3: {
                            this.keyExchange.skipServerCredentials();
                            this.authentication = null;
                        }
                        case 4: {
                            this.keyExchange.skipServerKeyExchange();
                        }
                        case 5:
                        case 6: {
                            TlsProtocol.assertEmpty(byteArrayInputStream);
                            this.connection_state = 7;
                            final Vector clientSupplementalData = this.tlsClient.getClientSupplementalData();
                            if (clientSupplementalData != null) {
                                this.sendSupplementalDataMessage(clientSupplementalData);
                            }
                            this.connection_state = 8;
                            TlsCredentials clientCredentials = null;
                            if (this.certificateRequest == null) {
                                this.keyExchange.skipClientCredentials();
                            }
                            else {
                                clientCredentials = this.authentication.getClientCredentials(this.certificateRequest);
                                if (clientCredentials == null) {
                                    this.keyExchange.skipClientCredentials();
                                    this.sendCertificateMessage(Certificate.EMPTY_CHAIN);
                                }
                                else {
                                    this.keyExchange.processClientCredentials(clientCredentials);
                                    this.sendCertificateMessage(clientCredentials.getCertificate());
                                }
                            }
                            this.connection_state = 9;
                            this.sendClientKeyExchangeMessage();
                            TlsProtocol.establishMasterSecret(this.getContext(), this.keyExchange);
                            this.recordStream.setPendingConnectionState(this.tlsClient.getCompression(), this.tlsClient.getCipher());
                            this.connection_state = 10;
                            if (clientCredentials != null && clientCredentials instanceof TlsSignerCredentials) {
                                this.sendCertificateVerifyMessage(((TlsSignerCredentials)clientCredentials).generateCertificateSignature(this.recordStream.getCurrentHash(null)));
                                this.connection_state = 11;
                            }
                            this.sendChangeCipherSpecMessage();
                            this.connection_state = 12;
                            this.sendFinishedMessage();
                            this.connection_state = 13;
                            break;
                        }
                        default: {
                            this.failWithError((short)2, (short)40);
                            break;
                        }
                    }
                    break;
                }
                case 12: {
                    switch (this.connection_state) {
                        case 2: {
                            this.handleSupplementalData(null);
                        }
                        case 3: {
                            this.keyExchange.skipServerCredentials();
                            this.authentication = null;
                        }
                        case 4: {
                            this.keyExchange.processServerKeyExchange(byteArrayInputStream);
                            TlsProtocol.assertEmpty(byteArrayInputStream);
                            break;
                        }
                        default: {
                            this.failWithError((short)2, (short)10);
                            break;
                        }
                    }
                    this.connection_state = 5;
                    break;
                }
                case 13: {
                    switch (this.connection_state) {
                        case 4: {
                            this.keyExchange.skipServerKeyExchange();
                        }
                        case 5: {
                            if (this.authentication == null) {
                                this.failWithError((short)2, (short)40);
                            }
                            this.certificateRequest = CertificateRequest.parse(byteArrayInputStream);
                            TlsProtocol.assertEmpty(byteArrayInputStream);
                            this.keyExchange.validateCertificateRequest(this.certificateRequest);
                            break;
                        }
                        default: {
                            this.failWithError((short)2, (short)10);
                            break;
                        }
                    }
                    this.connection_state = 6;
                    break;
                }
                case 4: {
                    switch (this.connection_state) {
                        case 13: {
                            if (!this.expectSessionTicket) {
                                this.failWithError((short)2, (short)10);
                            }
                            this.receiveNewSessionTicketMessage(byteArrayInputStream);
                            this.connection_state = 14;
                            break Label_0959;
                        }
                        default: {
                            this.failWithError((short)2, (short)10);
                            break Label_0959;
                        }
                    }
                    break;
                }
                case 0: {
                    TlsProtocol.assertEmpty(byteArrayInputStream);
                    if (this.connection_state == 16) {
                        this.raiseWarning((short)100, "Renegotiation not supported");
                        break;
                    }
                    break;
                }
                default: {
                    this.failWithError((short)2, (short)10);
                    break;
                }
            }
        }
    }
    
    protected void handleSupplementalData(final Vector vector) throws IOException {
        this.tlsClient.processServerSupplementalData(vector);
        this.connection_state = 3;
        (this.keyExchange = this.tlsClient.getKeyExchange()).init(this.getContext());
    }
    
    protected void receiveNewSessionTicketMessage(final ByteArrayInputStream byteArrayInputStream) throws IOException {
        final NewSessionTicket parse = NewSessionTicket.parse(byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        this.tlsClient.notifyNewSessionTicket(parse);
    }
    
    protected void receiveServerHelloMessage(final ByteArrayInputStream byteArrayInputStream) throws IOException {
        final ProtocolVersion version = TlsUtils.readVersion(byteArrayInputStream);
        if (version.isDTLS()) {
            this.failWithError((short)2, (short)47);
        }
        if (!version.equals(this.recordStream.getReadVersion())) {
            this.failWithError((short)2, (short)47);
        }
        if (!version.isEqualOrEarlierVersionOf(this.getContext().getClientVersion())) {
            this.failWithError((short)2, (short)47);
        }
        this.recordStream.setWriteVersion(version);
        this.getContext().setServerVersion(version);
        this.tlsClient.notifyServerVersion(version);
        this.securityParameters.serverRandom = TlsUtils.readFully(32, byteArrayInputStream);
        final byte[] opaque8 = TlsUtils.readOpaque8(byteArrayInputStream);
        if (opaque8.length > 32) {
            this.failWithError((short)2, (short)47);
        }
        this.tlsClient.notifySessionID(opaque8);
        this.selectedCipherSuite = TlsUtils.readUint16(byteArrayInputStream);
        if (!TlsProtocol.arrayContains(this.offeredCipherSuites, this.selectedCipherSuite) || this.selectedCipherSuite == 0 || this.selectedCipherSuite == 255) {
            this.failWithError((short)2, (short)47);
        }
        this.tlsClient.notifySelectedCipherSuite(this.selectedCipherSuite);
        final short uint8 = TlsUtils.readUint8(byteArrayInputStream);
        if (!TlsProtocol.arrayContains(this.offeredCompressionMethods, uint8)) {
            this.failWithError((short)2, (short)47);
        }
        this.tlsClient.notifySelectedCompressionMethod(uint8);
        final Hashtable extensions = TlsProtocol.readExtensions(byteArrayInputStream);
        if (extensions != null) {
            final Enumeration<Integer> keys = extensions.keys();
            while (keys.hasMoreElements()) {
                final Integer key = keys.nextElement();
                if (!key.equals(TlsClientProtocol.EXT_RenegotiationInfo) && (this.clientExtensions == null || this.clientExtensions.get(key) == null)) {
                    this.failWithError((short)2, (short)110);
                }
            }
            final byte[] array = (Object)extensions.get(TlsClientProtocol.EXT_RenegotiationInfo);
            if (array != null) {
                this.secure_renegotiation = true;
                if (!Arrays.constantTimeAreEqual(array, TlsProtocol.createRenegotiationInfo(TlsUtils.EMPTY_BYTES))) {
                    this.failWithError((short)2, (short)40);
                }
            }
            this.expectSessionTicket = extensions.containsKey(TlsClientProtocol.EXT_SessionTicket);
        }
        this.tlsClient.notifySecureRenegotiation(this.secure_renegotiation);
        if (this.clientExtensions != null) {
            this.tlsClient.processServerExtensions(extensions);
        }
    }
    
    protected void sendCertificateVerifyMessage(final byte[] array) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8((short)15, byteArrayOutputStream);
        TlsUtils.writeUint24(array.length + 2, byteArrayOutputStream);
        TlsUtils.writeOpaque16(array, byteArrayOutputStream);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        this.safeWriteRecord((short)22, byteArray, 0, byteArray.length);
    }
    
    protected void sendClientHelloMessage() throws IOException {
        this.recordStream.setWriteVersion(this.tlsClient.getClientHelloRecordLayerVersion());
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8((short)1, byteArrayOutputStream);
        TlsUtils.writeUint24(0, byteArrayOutputStream);
        final ProtocolVersion clientVersion = this.tlsClient.getClientVersion();
        if (clientVersion.isDTLS()) {
            this.failWithError((short)2, (short)80);
        }
        this.getContext().setClientVersion(clientVersion);
        TlsUtils.writeVersion(clientVersion, byteArrayOutputStream);
        byteArrayOutputStream.write(this.securityParameters.clientRandom);
        TlsUtils.writeOpaque8(TlsUtils.EMPTY_BYTES, byteArrayOutputStream);
        this.offeredCipherSuites = this.tlsClient.getCipherSuites();
        this.clientExtensions = this.tlsClient.getClientExtensions();
        final boolean b = this.clientExtensions == null || this.clientExtensions.get(TlsClientProtocol.EXT_RenegotiationInfo) == null;
        int length = this.offeredCipherSuites.length;
        if (b) {
            ++length;
        }
        TlsUtils.writeUint16(2 * length, byteArrayOutputStream);
        TlsUtils.writeUint16Array(this.offeredCipherSuites, byteArrayOutputStream);
        if (b) {
            TlsUtils.writeUint16(255, byteArrayOutputStream);
        }
        this.offeredCompressionMethods = this.tlsClient.getCompressionMethods();
        TlsUtils.writeUint8((short)this.offeredCompressionMethods.length, byteArrayOutputStream);
        TlsUtils.writeUint8Array(this.offeredCompressionMethods, byteArrayOutputStream);
        if (this.clientExtensions != null) {
            TlsProtocol.writeExtensions(byteArrayOutputStream, this.clientExtensions);
        }
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        TlsUtils.writeUint24(byteArray.length - 4, byteArray, 1);
        this.safeWriteRecord((short)22, byteArray, 0, byteArray.length);
    }
    
    protected void sendClientKeyExchangeMessage() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8((short)16, byteArrayOutputStream);
        TlsUtils.writeUint24(0, byteArrayOutputStream);
        this.keyExchange.generateClientKeyExchange(byteArrayOutputStream);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        TlsUtils.writeUint24(byteArray.length - 4, byteArray, 1);
        this.safeWriteRecord((short)22, byteArray, 0, byteArray.length);
    }
}
