// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

public class TlsRuntimeException extends RuntimeException
{
    private static final long serialVersionUID = 1928023487348344086L;
    Throwable e;
    
    public TlsRuntimeException(final String message, final Throwable e) {
        super(message);
        this.e = e;
    }
    
    public TlsRuntimeException(final String message) {
        super(message);
    }
    
    @Override
    public Throwable getCause() {
        return this.e;
    }
}
