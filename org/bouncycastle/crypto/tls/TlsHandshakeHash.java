// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.Digest;

interface TlsHandshakeHash extends Digest
{
    void init(final TlsContext p0);
    
    TlsHandshakeHash commit();
    
    TlsHandshakeHash fork();
}
