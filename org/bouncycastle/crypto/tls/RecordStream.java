// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.Digest;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.InputStream;

class RecordStream
{
    private static int PLAINTEXT_LIMIT;
    private static int COMPRESSED_LIMIT;
    private static int CIPHERTEXT_LIMIT;
    private TlsProtocol handler;
    private InputStream input;
    private OutputStream output;
    private TlsCompression pendingCompression;
    private TlsCompression readCompression;
    private TlsCompression writeCompression;
    private TlsCipher pendingCipher;
    private TlsCipher readCipher;
    private TlsCipher writeCipher;
    private long readSeqNo;
    private long writeSeqNo;
    private ByteArrayOutputStream buffer;
    private TlsContext context;
    private TlsHandshakeHash hash;
    private ProtocolVersion readVersion;
    private ProtocolVersion writeVersion;
    private boolean restrictReadVersion;
    
    RecordStream(final TlsProtocol handler, final InputStream input, final OutputStream output) {
        this.pendingCompression = null;
        this.readCompression = null;
        this.writeCompression = null;
        this.pendingCipher = null;
        this.readCipher = null;
        this.writeCipher = null;
        this.readSeqNo = 0L;
        this.writeSeqNo = 0L;
        this.buffer = new ByteArrayOutputStream();
        this.context = null;
        this.hash = null;
        this.readVersion = null;
        this.writeVersion = null;
        this.restrictReadVersion = true;
        this.handler = handler;
        this.input = input;
        this.output = output;
        this.readCompression = new TlsNullCompression();
        this.writeCompression = this.readCompression;
        this.readCipher = new TlsNullCipher(this.context);
        this.writeCipher = this.readCipher;
    }
    
    void init(final TlsContext context) {
        this.context = context;
        (this.hash = new DeferredHash()).init(context);
    }
    
    ProtocolVersion getReadVersion() {
        return this.readVersion;
    }
    
    void setReadVersion(final ProtocolVersion readVersion) {
        this.readVersion = readVersion;
    }
    
    void setWriteVersion(final ProtocolVersion writeVersion) {
        this.writeVersion = writeVersion;
    }
    
    void setRestrictReadVersion(final boolean restrictReadVersion) {
        this.restrictReadVersion = restrictReadVersion;
    }
    
    void notifyHelloComplete() {
        this.hash = this.hash.commit();
    }
    
    void setPendingConnectionState(final TlsCompression pendingCompression, final TlsCipher pendingCipher) {
        this.pendingCompression = pendingCompression;
        this.pendingCipher = pendingCipher;
    }
    
    void sentWriteCipherSpec() throws IOException {
        if (this.pendingCompression == null || this.pendingCipher == null) {
            throw new TlsFatalAlert((short)40);
        }
        this.writeCompression = this.pendingCompression;
        this.writeCipher = this.pendingCipher;
        this.writeSeqNo = 0L;
    }
    
    void receivedReadCipherSpec() throws IOException {
        if (this.pendingCompression == null || this.pendingCipher == null) {
            throw new TlsFatalAlert((short)40);
        }
        this.readCompression = this.pendingCompression;
        this.readCipher = this.pendingCipher;
        this.readSeqNo = 0L;
    }
    
    void finaliseHandshake() throws IOException {
        if (this.readCompression != this.pendingCompression || this.writeCompression != this.pendingCompression || this.readCipher != this.pendingCipher || this.writeCipher != this.pendingCipher) {
            throw new TlsFatalAlert((short)40);
        }
        this.pendingCompression = null;
        this.pendingCipher = null;
    }
    
    public void readRecord() throws IOException {
        final short uint8 = TlsUtils.readUint8(this.input);
        checkType(uint8, (short)10);
        if (!this.restrictReadVersion) {
            if ((TlsUtils.readVersionRaw(this.input) & 0xFFFFFF00) != 0x300) {
                throw new TlsFatalAlert((short)47);
            }
        }
        else {
            final ProtocolVersion version = TlsUtils.readVersion(this.input);
            if (this.readVersion == null) {
                this.readVersion = version;
            }
            else if (!version.equals(this.readVersion)) {
                throw new TlsFatalAlert((short)47);
            }
        }
        final byte[] decodeAndVerify = this.decodeAndVerify(uint8, this.input, TlsUtils.readUint16(this.input));
        this.handler.processRecord(uint8, decodeAndVerify, 0, decodeAndVerify.length);
    }
    
    protected byte[] decodeAndVerify(final short n, final InputStream inputStream, final int n2) throws IOException {
        checkLength(n2, RecordStream.CIPHERTEXT_LIMIT, (short)22);
        final byte[] fully = TlsUtils.readFully(n2, inputStream);
        byte[] b = this.readCipher.decodeCiphertext(this.readSeqNo++, n, fully, 0, fully.length);
        checkLength(b.length, RecordStream.COMPRESSED_LIMIT, (short)22);
        final OutputStream decompress = this.readCompression.decompress(this.buffer);
        if (decompress != this.buffer) {
            decompress.write(b, 0, b.length);
            decompress.flush();
            b = this.getBufferContents();
        }
        checkLength(b.length, RecordStream.PLAINTEXT_LIMIT, (short)30);
        return b;
    }
    
    protected void writeRecord(final short n, final byte[] b, final int off, final int len) throws IOException {
        checkType(n, (short)80);
        checkLength(len, RecordStream.PLAINTEXT_LIMIT, (short)80);
        if (len < 1 && n != 23) {
            throw new TlsFatalAlert((short)80);
        }
        if (n == 22) {
            this.updateHandshakeData(b, off, len);
        }
        final OutputStream compress = this.writeCompression.compress(this.buffer);
        byte[] array;
        if (compress == this.buffer) {
            array = this.writeCipher.encodePlaintext(this.writeSeqNo++, n, b, off, len);
        }
        else {
            compress.write(b, off, len);
            compress.flush();
            final byte[] bufferContents = this.getBufferContents();
            checkLength(bufferContents.length, len + 1024, (short)80);
            array = this.writeCipher.encodePlaintext(this.writeSeqNo++, n, bufferContents, 0, bufferContents.length);
        }
        checkLength(array.length, RecordStream.CIPHERTEXT_LIMIT, (short)80);
        final byte[] b2 = new byte[array.length + 5];
        TlsUtils.writeUint8(n, b2, 0);
        TlsUtils.writeVersion(this.writeVersion, b2, 1);
        TlsUtils.writeUint16(array.length, b2, 3);
        System.arraycopy(array, 0, b2, 5, array.length);
        this.output.write(b2);
        this.output.flush();
    }
    
    void updateHandshakeData(final byte[] array, final int n, final int n2) {
        this.hash.update(array, n, n2);
    }
    
    byte[] getCurrentHash(final byte[] array) {
        final TlsHandshakeHash fork = this.hash.fork();
        if (this.context.getServerVersion().isSSL() && array != null) {
            fork.update(array, 0, array.length);
        }
        return doFinal(fork);
    }
    
    protected void close() throws IOException {
        IOException ex = null;
        try {
            this.input.close();
        }
        catch (IOException ex2) {
            ex = ex2;
        }
        try {
            this.output.close();
        }
        catch (IOException ex3) {
            ex = ex3;
        }
        if (ex != null) {
            throw ex;
        }
    }
    
    protected void flush() throws IOException {
        this.output.flush();
    }
    
    private byte[] getBufferContents() {
        final byte[] byteArray = this.buffer.toByteArray();
        this.buffer.reset();
        return byteArray;
    }
    
    private static byte[] doFinal(final Digest digest) {
        final byte[] array = new byte[digest.getDigestSize()];
        digest.doFinal(array, 0);
        return array;
    }
    
    private static void checkType(final short n, final short n2) throws IOException {
        switch (n) {
            case 20:
            case 21:
            case 22:
            case 23: {}
            default: {
                throw new TlsFatalAlert(n2);
            }
        }
    }
    
    private static void checkLength(final int n, final int n2, final short n3) throws IOException {
        if (n > n2) {
            throw new TlsFatalAlert(n3);
        }
    }
    
    static {
        RecordStream.PLAINTEXT_LIMIT = 16384;
        RecordStream.COMPRESSED_LIMIT = RecordStream.PLAINTEXT_LIMIT + 1024;
        RecordStream.CIPHERTEXT_LIMIT = RecordStream.COMPRESSED_LIMIT + 1024;
    }
}
