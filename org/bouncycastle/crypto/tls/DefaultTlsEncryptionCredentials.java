// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.encodings.PKCS1Encoding;
import org.bouncycastle.crypto.engines.RSABlindedEngine;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class DefaultTlsEncryptionCredentials implements TlsEncryptionCredentials
{
    protected TlsContext context;
    protected Certificate certificate;
    protected AsymmetricKeyParameter privateKey;
    
    public DefaultTlsEncryptionCredentials(final TlsContext context, final Certificate certificate, final AsymmetricKeyParameter privateKey) {
        if (certificate == null) {
            throw new IllegalArgumentException("'certificate' cannot be null");
        }
        if (certificate.isEmpty()) {
            throw new IllegalArgumentException("'certificate' cannot be empty");
        }
        if (privateKey == null) {
            throw new IllegalArgumentException("'privateKey' cannot be null");
        }
        if (!privateKey.isPrivate()) {
            throw new IllegalArgumentException("'privateKey' must be private");
        }
        if (privateKey instanceof RSAKeyParameters) {
            this.context = context;
            this.certificate = certificate;
            this.privateKey = privateKey;
            return;
        }
        throw new IllegalArgumentException("'privateKey' type not supported: " + privateKey.getClass().getName());
    }
    
    public Certificate getCertificate() {
        return this.certificate;
    }
    
    public byte[] decryptPreMasterSecret(final byte[] array) throws IOException {
        final PKCS1Encoding pkcs1Encoding = new PKCS1Encoding(new RSABlindedEngine());
        pkcs1Encoding.init(false, new ParametersWithRandom(this.privateKey, this.context.getSecureRandom()));
        try {
            return pkcs1Encoding.processBlock(array, 0, array.length);
        }
        catch (InvalidCipherTextException ex) {
            throw new TlsFatalAlert((short)47);
        }
    }
}
