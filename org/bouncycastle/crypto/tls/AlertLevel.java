// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

public class AlertLevel
{
    public static final short warning = 1;
    public static final short fatal = 2;
}
