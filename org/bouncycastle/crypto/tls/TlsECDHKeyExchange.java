// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.InputStream;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.io.OutputStream;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import java.io.IOException;
import java.util.Vector;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class TlsECDHKeyExchange extends AbstractTlsKeyExchange
{
    protected TlsSigner tlsSigner;
    protected int[] namedCurves;
    protected short[] clientECPointFormats;
    protected short[] serverECPointFormats;
    protected AsymmetricKeyParameter serverPublicKey;
    protected ECPublicKeyParameters ecAgreeServerPublicKey;
    protected TlsAgreementCredentials agreementCredentials;
    protected ECPrivateKeyParameters ecAgreeClientPrivateKey;
    protected ECPrivateKeyParameters ecAgreeServerPrivateKey;
    protected ECPublicKeyParameters ecAgreeClientPublicKey;
    
    public TlsECDHKeyExchange(final int keyExchange, final Vector vector, final int[] namedCurves, final short[] clientECPointFormats, final short[] serverECPointFormats) {
        super(keyExchange, vector);
        switch (keyExchange) {
            case 19: {
                this.tlsSigner = new TlsRSASigner();
                break;
            }
            case 17: {
                this.tlsSigner = new TlsECDSASigner();
                break;
            }
            case 16:
            case 18: {
                this.tlsSigner = null;
                break;
            }
            default: {
                throw new IllegalArgumentException("unsupported key exchange algorithm");
            }
        }
        this.keyExchange = keyExchange;
        this.namedCurves = namedCurves;
        this.clientECPointFormats = clientECPointFormats;
        this.serverECPointFormats = serverECPointFormats;
    }
    
    @Override
    public void init(final TlsContext tlsContext) {
        super.init(tlsContext);
        if (this.tlsSigner != null) {
            this.tlsSigner.init(tlsContext);
        }
    }
    
    public void skipServerCredentials() throws IOException {
        throw new TlsFatalAlert((short)10);
    }
    
    @Override
    public void processServerCertificate(final Certificate certificate) throws IOException {
        if (certificate.isEmpty()) {
            throw new TlsFatalAlert((short)42);
        }
        final org.bouncycastle.asn1.x509.Certificate certificate2 = certificate.getCertificateAt(0);
        final SubjectPublicKeyInfo subjectPublicKeyInfo = certificate2.getSubjectPublicKeyInfo();
        try {
            this.serverPublicKey = PublicKeyFactory.createKey(subjectPublicKeyInfo);
        }
        catch (RuntimeException ex) {
            throw new TlsFatalAlert((short)43);
        }
        if (this.tlsSigner == null) {
            try {
                this.ecAgreeServerPublicKey = TlsECCUtils.validateECPublicKey((ECPublicKeyParameters)this.serverPublicKey);
            }
            catch (ClassCastException ex2) {
                throw new TlsFatalAlert((short)46);
            }
            TlsUtils.validateKeyUsage(certificate2, 8);
        }
        else {
            if (!this.tlsSigner.isValidPublicKey(this.serverPublicKey)) {
                throw new TlsFatalAlert((short)46);
            }
            TlsUtils.validateKeyUsage(certificate2, 128);
        }
        super.processServerCertificate(certificate);
    }
    
    @Override
    public boolean requiresServerKeyExchange() {
        switch (this.keyExchange) {
            case 17:
            case 19:
            case 20: {
                return true;
            }
            default: {
                return false;
            }
        }
    }
    
    public void validateCertificateRequest(final CertificateRequest certificateRequest) throws IOException {
        final short[] certificateTypes = certificateRequest.getCertificateTypes();
        int i = 0;
        while (i < certificateTypes.length) {
            switch (certificateTypes[i]) {
                case 1:
                case 2:
                case 64:
                case 65:
                case 66: {
                    ++i;
                    continue;
                }
                default: {
                    throw new TlsFatalAlert((short)47);
                }
            }
        }
    }
    
    public void processClientCredentials(final TlsCredentials tlsCredentials) throws IOException {
        if (tlsCredentials instanceof TlsAgreementCredentials) {
            this.agreementCredentials = (TlsAgreementCredentials)tlsCredentials;
        }
        else if (!(tlsCredentials instanceof TlsSignerCredentials)) {
            throw new TlsFatalAlert((short)80);
        }
    }
    
    public void generateClientKeyExchange(final OutputStream outputStream) throws IOException {
        if (this.agreementCredentials != null) {
            return;
        }
        final AsymmetricCipherKeyPair generateECKeyPair = TlsECCUtils.generateECKeyPair(this.context.getSecureRandom(), this.ecAgreeServerPublicKey.getParameters());
        this.ecAgreeClientPrivateKey = (ECPrivateKeyParameters)generateECKeyPair.getPrivate();
        TlsUtils.writeOpaque8(TlsECCUtils.serializeECPublicKey(this.serverECPointFormats, (ECPublicKeyParameters)generateECKeyPair.getPublic()), outputStream);
    }
    
    @Override
    public void processClientCertificate(final Certificate certificate) throws IOException {
    }
    
    @Override
    public void processClientKeyExchange(final InputStream inputStream) throws IOException {
        if (this.ecAgreeClientPublicKey != null) {
            return;
        }
        this.ecAgreeClientPublicKey = TlsECCUtils.validateECPublicKey(TlsECCUtils.deserializeECPublicKey(this.serverECPointFormats, this.ecAgreeServerPrivateKey.getParameters(), TlsUtils.readOpaque8(inputStream)));
    }
    
    public byte[] generatePremasterSecret() throws IOException {
        if (this.agreementCredentials != null) {
            return this.agreementCredentials.generateAgreement(this.ecAgreeServerPublicKey);
        }
        if (this.ecAgreeServerPrivateKey != null) {
            return TlsECCUtils.calculateECDHBasicAgreement(this.ecAgreeClientPublicKey, this.ecAgreeServerPrivateKey);
        }
        if (this.ecAgreeClientPrivateKey != null) {
            return TlsECCUtils.calculateECDHBasicAgreement(this.ecAgreeServerPublicKey, this.ecAgreeClientPrivateKey);
        }
        throw new TlsFatalAlert((short)80);
    }
}
