// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.asn1.ASN1Primitive;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.IOException;
import org.bouncycastle.asn1.x500.X500Name;
import java.io.OutputStream;
import java.util.Vector;

public class CertificateRequest
{
    private short[] certificateTypes;
    private Vector certificateAuthorities;
    
    public CertificateRequest(final short[] certificateTypes, final Vector certificateAuthorities) {
        this.certificateTypes = certificateTypes;
        this.certificateAuthorities = certificateAuthorities;
    }
    
    public short[] getCertificateTypes() {
        return this.certificateTypes;
    }
    
    public Vector getCertificateAuthorities() {
        return this.certificateAuthorities;
    }
    
    public void encode(final OutputStream outputStream) throws IOException {
        if (this.certificateTypes == null || this.certificateTypes.length == 0) {
            TlsUtils.writeUint8((short)0, outputStream);
        }
        else {
            TlsUtils.writeUint8((short)this.certificateTypes.length, outputStream);
            TlsUtils.writeUint8Array(this.certificateTypes, outputStream);
        }
        if (this.certificateAuthorities == null || this.certificateAuthorities.isEmpty()) {
            TlsUtils.writeUint16(0, outputStream);
        }
        else {
            final Vector<byte[]> vector = new Vector<byte[]>(this.certificateAuthorities.size());
            int n = 0;
            for (int i = 0; i < this.certificateAuthorities.size(); ++i) {
                final byte[] encoded = this.certificateAuthorities.elementAt(i).getEncoded("DER");
                vector.addElement(encoded);
                n += encoded.length;
            }
            TlsUtils.writeUint16(n, outputStream);
            for (int j = 0; j < vector.size(); ++j) {
                outputStream.write(vector.elementAt(j));
            }
        }
    }
    
    public static CertificateRequest parse(final InputStream inputStream) throws IOException {
        final short uint8 = TlsUtils.readUint8(inputStream);
        final short[] array = new short[uint8];
        for (short n = 0; n < uint8; ++n) {
            array[n] = TlsUtils.readUint8(inputStream);
        }
        final byte[] opaque16 = TlsUtils.readOpaque16(inputStream);
        final Vector<X500Name> vector = new Vector<X500Name>();
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(opaque16);
        while (byteArrayInputStream.available() > 0) {
            vector.addElement(X500Name.getInstance(ASN1Primitive.fromByteArray(TlsUtils.readOpaque16(byteArrayInputStream))));
        }
        return new CertificateRequest(array, vector);
    }
}
