// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.util.Vector;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.bouncycastle.util.Arrays;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.security.SecureRandom;

public abstract class DTLSProtocol
{
    protected final SecureRandom secureRandom;
    
    protected DTLSProtocol(final SecureRandom secureRandom) {
        if (secureRandom == null) {
            throw new IllegalArgumentException("'secureRandom' cannot be null");
        }
        this.secureRandom = secureRandom;
    }
    
    protected void processFinished(final byte[] buf, final byte[] array) throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        final byte[] fully = TlsUtils.readFully(array.length, byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        if (!Arrays.constantTimeAreEqual(array, fully)) {
            throw new TlsFatalAlert((short)40);
        }
    }
    
    protected static byte[] generateCertificate(final Certificate certificate) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        certificate.encode(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    protected static byte[] generateSupplementalData(final Vector vector) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsProtocol.writeSupplementalData(byteArrayOutputStream, vector);
        return byteArrayOutputStream.toByteArray();
    }
    
    protected static void validateSelectedCipherSuite(final int n, final short n2) throws IOException {
        switch (n) {
            case 3:
            case 4:
            case 5:
            case 23:
            case 24:
            case 138:
            case 142:
            case 146:
            case 49154:
            case 49159:
            case 49164:
            case 49169:
            case 49174: {
                throw new IllegalStateException("RC4 MUST NOT be used with DTLS");
            }
            default: {}
        }
    }
}
