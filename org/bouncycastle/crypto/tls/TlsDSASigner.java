// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.DSA;
import org.bouncycastle.crypto.signers.DSADigestSigner;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.digests.NullDigest;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public abstract class TlsDSASigner extends AbstractTlsSigner
{
    public byte[] generateRawSignature(final AsymmetricKeyParameter asymmetricKeyParameter, final byte[] array) throws CryptoException {
        final Signer signer = this.makeSigner(new NullDigest(), true, new ParametersWithRandom(asymmetricKeyParameter, this.context.getSecureRandom()));
        signer.update(array, 16, 20);
        return signer.generateSignature();
    }
    
    public boolean verifyRawSignature(final byte[] array, final AsymmetricKeyParameter asymmetricKeyParameter, final byte[] array2) throws CryptoException {
        final Signer signer = this.makeSigner(new NullDigest(), false, asymmetricKeyParameter);
        signer.update(array2, 16, 20);
        return signer.verifySignature(array);
    }
    
    public Signer createSigner(final AsymmetricKeyParameter asymmetricKeyParameter) {
        return this.makeSigner(new SHA1Digest(), true, new ParametersWithRandom(asymmetricKeyParameter, this.context.getSecureRandom()));
    }
    
    public Signer createVerifyer(final AsymmetricKeyParameter asymmetricKeyParameter) {
        return this.makeSigner(new SHA1Digest(), false, asymmetricKeyParameter);
    }
    
    protected Signer makeSigner(final Digest digest, final boolean b, final CipherParameters cipherParameters) {
        final DSADigestSigner dsaDigestSigner = new DSADigestSigner(this.createDSAImpl(), digest);
        dsaDigestSigner.init(b, cipherParameters);
        return dsaDigestSigner;
    }
    
    protected abstract DSA createDSAImpl();
}
