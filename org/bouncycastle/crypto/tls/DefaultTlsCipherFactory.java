// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.digests.SHA384Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.engines.SEEDEngine;
import org.bouncycastle.crypto.engines.DESedeEngine;
import org.bouncycastle.crypto.engines.CamelliaEngine;
import org.bouncycastle.crypto.modes.GCMBlockCipher;
import org.bouncycastle.crypto.modes.AEADBlockCipher;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.engines.RC4Engine;
import org.bouncycastle.crypto.StreamCipher;
import java.io.IOException;

public class DefaultTlsCipherFactory extends AbstractTlsCipherFactory
{
    @Override
    public TlsCipher createCipher(final TlsContext tlsContext, final int n, final int n2) throws IOException {
        switch (n) {
            case 7: {
                return this.createDESedeCipher(tlsContext, n2);
            }
            case 8: {
                return this.createAESCipher(tlsContext, 16, n2);
            }
            case 10: {
                return this.createCipher_AES_GCM(tlsContext, 16, 16);
            }
            case 9: {
                return this.createAESCipher(tlsContext, 32, n2);
            }
            case 11: {
                return this.createCipher_AES_GCM(tlsContext, 32, 16);
            }
            case 12: {
                return this.createCamelliaCipher(tlsContext, 16, n2);
            }
            case 13: {
                return this.createCamelliaCipher(tlsContext, 32, n2);
            }
            case 0: {
                return this.createNullCipher(tlsContext, n2);
            }
            case 2: {
                return this.createRC4Cipher(tlsContext, 16, n2);
            }
            case 14: {
                return this.createSEEDCipher(tlsContext, n2);
            }
            default: {
                throw new TlsFatalAlert((short)80);
            }
        }
    }
    
    protected TlsBlockCipher createAESCipher(final TlsContext tlsContext, final int n, final int n2) throws IOException {
        return new TlsBlockCipher(tlsContext, this.createAESBlockCipher(), this.createAESBlockCipher(), this.createHMACDigest(n2), this.createHMACDigest(n2), n);
    }
    
    protected TlsAEADCipher createCipher_AES_GCM(final TlsContext tlsContext, final int n, final int n2) throws IOException {
        return new TlsAEADCipher(tlsContext, this.createAEADBlockCipher_AES_GCM(), this.createAEADBlockCipher_AES_GCM(), n, n2);
    }
    
    protected TlsBlockCipher createCamelliaCipher(final TlsContext tlsContext, final int n, final int n2) throws IOException {
        return new TlsBlockCipher(tlsContext, this.createCamelliaBlockCipher(), this.createCamelliaBlockCipher(), this.createHMACDigest(n2), this.createHMACDigest(n2), n);
    }
    
    protected TlsNullCipher createNullCipher(final TlsContext tlsContext, final int n) throws IOException {
        return new TlsNullCipher(tlsContext, this.createHMACDigest(n), this.createHMACDigest(n));
    }
    
    protected TlsStreamCipher createRC4Cipher(final TlsContext tlsContext, final int n, final int n2) throws IOException {
        return new TlsStreamCipher(tlsContext, this.createRC4StreamCipher(), this.createRC4StreamCipher(), this.createHMACDigest(n2), this.createHMACDigest(n2), n);
    }
    
    protected TlsBlockCipher createDESedeCipher(final TlsContext tlsContext, final int n) throws IOException {
        return new TlsBlockCipher(tlsContext, this.createDESedeBlockCipher(), this.createDESedeBlockCipher(), this.createHMACDigest(n), this.createHMACDigest(n), 24);
    }
    
    protected TlsBlockCipher createSEEDCipher(final TlsContext tlsContext, final int n) throws IOException {
        return new TlsBlockCipher(tlsContext, this.createSEEDBlockCipher(), this.createSEEDBlockCipher(), this.createHMACDigest(n), this.createHMACDigest(n), 16);
    }
    
    protected StreamCipher createRC4StreamCipher() {
        return new RC4Engine();
    }
    
    protected BlockCipher createAESBlockCipher() {
        return new CBCBlockCipher(new AESFastEngine());
    }
    
    protected AEADBlockCipher createAEADBlockCipher_AES_GCM() {
        return new GCMBlockCipher(new AESFastEngine());
    }
    
    protected BlockCipher createCamelliaBlockCipher() {
        return new CBCBlockCipher(new CamelliaEngine());
    }
    
    protected BlockCipher createDESedeBlockCipher() {
        return new CBCBlockCipher(new DESedeEngine());
    }
    
    protected BlockCipher createSEEDBlockCipher() {
        return new CBCBlockCipher(new SEEDEngine());
    }
    
    protected Digest createHMACDigest(final int n) throws IOException {
        switch (n) {
            case 0: {
                return null;
            }
            case 1: {
                return new MD5Digest();
            }
            case 2: {
                return new SHA1Digest();
            }
            case 3: {
                return new SHA256Digest();
            }
            case 4: {
                return new SHA384Digest();
            }
            case 5: {
                return new SHA512Digest();
            }
            default: {
                throw new TlsFatalAlert((short)80);
            }
        }
    }
}
