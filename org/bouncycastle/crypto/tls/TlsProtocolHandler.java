// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.security.SecureRandom;
import java.io.OutputStream;
import java.io.InputStream;

public class TlsProtocolHandler extends TlsClientProtocol
{
    public TlsProtocolHandler(final InputStream inputStream, final OutputStream outputStream) {
        super(inputStream, outputStream);
    }
    
    public TlsProtocolHandler(final InputStream inputStream, final OutputStream outputStream, final SecureRandom secureRandom) {
        super(inputStream, outputStream, secureRandom);
    }
}
