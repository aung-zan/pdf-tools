// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.Digest;
import java.io.ByteArrayOutputStream;

class DeferredHash implements TlsHandshakeHash
{
    protected TlsContext context;
    private ByteArrayOutputStream buf;
    private int prfAlgorithm;
    private Digest hash;
    
    DeferredHash() {
        this.buf = new ByteArrayOutputStream();
        this.prfAlgorithm = -1;
        this.hash = null;
        this.buf = new ByteArrayOutputStream();
        this.hash = null;
    }
    
    private DeferredHash(final Digest hash) {
        this.buf = new ByteArrayOutputStream();
        this.prfAlgorithm = -1;
        this.hash = null;
        this.buf = null;
        this.hash = hash;
    }
    
    public void init(final TlsContext context) {
        this.context = context;
    }
    
    public TlsHandshakeHash commit() {
        final int prfAlgorithm = this.context.getSecurityParameters().getPrfAlgorithm();
        final Digest prfHash = TlsUtils.createPRFHash(prfAlgorithm);
        final byte[] byteArray = this.buf.toByteArray();
        prfHash.update(byteArray, 0, byteArray.length);
        if (prfHash instanceof TlsHandshakeHash) {
            final TlsHandshakeHash tlsHandshakeHash = (TlsHandshakeHash)prfHash;
            tlsHandshakeHash.init(this.context);
            return tlsHandshakeHash.commit();
        }
        this.prfAlgorithm = prfAlgorithm;
        this.hash = prfHash;
        this.buf = null;
        return this;
    }
    
    public TlsHandshakeHash fork() {
        this.checkHash();
        return new DeferredHash(TlsUtils.clonePRFHash(this.prfAlgorithm, this.hash));
    }
    
    public String getAlgorithmName() {
        this.checkHash();
        return this.hash.getAlgorithmName();
    }
    
    public int getDigestSize() {
        this.checkHash();
        return this.hash.getDigestSize();
    }
    
    public void update(final byte b) {
        if (this.hash == null) {
            this.buf.write(b);
        }
        else {
            this.hash.update(b);
        }
    }
    
    public void update(final byte[] b, final int off, final int len) {
        if (this.hash == null) {
            this.buf.write(b, off, len);
        }
        else {
            this.hash.update(b, off, len);
        }
    }
    
    public int doFinal(final byte[] array, final int n) {
        this.checkHash();
        return this.hash.doFinal(array, n);
    }
    
    public void reset() {
        if (this.hash == null) {
            this.buf.reset();
        }
        else {
            this.hash.reset();
        }
    }
    
    protected void checkHash() {
        if (this.hash == null) {
            throw new IllegalStateException("No hash algorithm has been set");
        }
    }
}
