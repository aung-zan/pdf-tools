// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.util.Integers;
import java.io.IOException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.util.Hashtable;
import org.bouncycastle.util.Arrays;

public abstract class SRPTlsClient extends AbstractTlsClient
{
    public static final Integer EXT_SRP;
    protected byte[] identity;
    protected byte[] password;
    
    public SRPTlsClient(final byte[] array, final byte[] array2) {
        this.identity = Arrays.clone(array);
        this.password = Arrays.clone(array2);
    }
    
    public SRPTlsClient(final TlsCipherFactory tlsCipherFactory, final byte[] array, final byte[] array2) {
        super(tlsCipherFactory);
        this.identity = Arrays.clone(array);
        this.password = Arrays.clone(array2);
    }
    
    public int[] getCipherSuites() {
        return new int[] { 49185, 49182, 49179, 49184, 49181, 49178 };
    }
    
    @Override
    public Hashtable getClientExtensions() throws IOException {
        Hashtable<Integer, byte[]> clientExtensions = (Hashtable<Integer, byte[]>)super.getClientExtensions();
        if (clientExtensions == null) {
            clientExtensions = new Hashtable<Integer, byte[]>();
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeOpaque8(this.identity, byteArrayOutputStream);
        clientExtensions.put(SRPTlsClient.EXT_SRP, byteArrayOutputStream.toByteArray());
        return clientExtensions;
    }
    
    @Override
    public void processServerExtensions(final Hashtable hashtable) throws IOException {
        if (hashtable != null) {
            final byte[] array = hashtable.get(SRPTlsClient.EXT_SRP);
            if (array != null && array.length > 0) {
                throw new TlsFatalAlert((short)47);
            }
        }
    }
    
    public TlsKeyExchange getKeyExchange() throws IOException {
        switch (this.selectedCipherSuite) {
            case 49178:
            case 49181:
            case 49184: {
                return this.createSRPKeyExchange(21);
            }
            case 49179:
            case 49182:
            case 49185: {
                return this.createSRPKeyExchange(23);
            }
            case 49180:
            case 49183:
            case 49186: {
                return this.createSRPKeyExchange(22);
            }
            default: {
                throw new TlsFatalAlert((short)80);
            }
        }
    }
    
    public TlsCipher getCipher() throws IOException {
        switch (this.selectedCipherSuite) {
            case 49178:
            case 49179:
            case 49180: {
                return this.cipherFactory.createCipher(this.context, 7, 2);
            }
            case 49181:
            case 49182:
            case 49183: {
                return this.cipherFactory.createCipher(this.context, 8, 2);
            }
            case 49184:
            case 49185:
            case 49186: {
                return this.cipherFactory.createCipher(this.context, 9, 2);
            }
            default: {
                throw new TlsFatalAlert((short)80);
            }
        }
    }
    
    protected TlsKeyExchange createSRPKeyExchange(final int n) {
        return new TlsSRPKeyExchange(n, this.supportedSignatureAlgorithms, this.identity, this.password);
    }
    
    static {
        EXT_SRP = Integers.valueOf(12);
    }
}
