// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.DSAPrivateKeyParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class DefaultTlsSignerCredentials implements TlsSignerCredentials
{
    protected TlsContext context;
    protected Certificate certificate;
    protected AsymmetricKeyParameter privateKey;
    protected TlsSigner signer;
    
    public DefaultTlsSignerCredentials(final TlsContext context, final Certificate certificate, final AsymmetricKeyParameter privateKey) {
        if (certificate == null) {
            throw new IllegalArgumentException("'certificate' cannot be null");
        }
        if (certificate.isEmpty()) {
            throw new IllegalArgumentException("'certificate' cannot be empty");
        }
        if (privateKey == null) {
            throw new IllegalArgumentException("'privateKey' cannot be null");
        }
        if (!privateKey.isPrivate()) {
            throw new IllegalArgumentException("'privateKey' must be private");
        }
        if (privateKey instanceof RSAKeyParameters) {
            this.signer = new TlsRSASigner();
        }
        else if (privateKey instanceof DSAPrivateKeyParameters) {
            this.signer = new TlsDSSSigner();
        }
        else {
            if (!(privateKey instanceof ECPrivateKeyParameters)) {
                throw new IllegalArgumentException("'privateKey' type not supported: " + privateKey.getClass().getName());
            }
            this.signer = new TlsECDSASigner();
        }
        this.signer.init(context);
        this.context = context;
        this.certificate = certificate;
        this.privateKey = privateKey;
    }
    
    public Certificate getCertificate() {
        return this.certificate;
    }
    
    public byte[] generateCertificateSignature(final byte[] array) throws IOException {
        try {
            return this.signer.generateRawSignature(this.privateKey, array);
        }
        catch (CryptoException ex) {
            throw new TlsFatalAlert((short)80);
        }
    }
}
