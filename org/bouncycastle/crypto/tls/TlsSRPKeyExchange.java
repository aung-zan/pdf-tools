// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.util.BigIntegers;
import java.io.OutputStream;
import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.agreement.srp.SRP6Util;
import org.bouncycastle.crypto.io.SignerInputStream;
import java.io.InputStream;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import java.io.IOException;
import java.util.Vector;
import org.bouncycastle.crypto.agreement.srp.SRP6Client;
import java.math.BigInteger;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class TlsSRPKeyExchange extends AbstractTlsKeyExchange
{
    protected TlsSigner tlsSigner;
    protected byte[] identity;
    protected byte[] password;
    protected AsymmetricKeyParameter serverPublicKey;
    protected byte[] s;
    protected BigInteger B;
    protected SRP6Client srpClient;
    
    public TlsSRPKeyExchange(final int keyExchange, final Vector vector, final byte[] identity, final byte[] password) {
        super(keyExchange, vector);
        this.serverPublicKey = null;
        this.s = null;
        this.B = null;
        this.srpClient = new SRP6Client();
        switch (keyExchange) {
            case 21: {
                this.tlsSigner = null;
                break;
            }
            case 23: {
                this.tlsSigner = new TlsRSASigner();
                break;
            }
            case 22: {
                this.tlsSigner = new TlsDSSSigner();
                break;
            }
            default: {
                throw new IllegalArgumentException("unsupported key exchange algorithm");
            }
        }
        this.keyExchange = keyExchange;
        this.identity = identity;
        this.password = password;
    }
    
    @Override
    public void init(final TlsContext tlsContext) {
        super.init(tlsContext);
        if (this.tlsSigner != null) {
            this.tlsSigner.init(tlsContext);
        }
    }
    
    public void skipServerCredentials() throws IOException {
        if (this.tlsSigner != null) {
            throw new TlsFatalAlert((short)10);
        }
    }
    
    @Override
    public void processServerCertificate(final Certificate certificate) throws IOException {
        if (this.tlsSigner == null) {
            throw new TlsFatalAlert((short)10);
        }
        if (certificate.isEmpty()) {
            throw new TlsFatalAlert((short)42);
        }
        final org.bouncycastle.asn1.x509.Certificate certificate2 = certificate.getCertificateAt(0);
        final SubjectPublicKeyInfo subjectPublicKeyInfo = certificate2.getSubjectPublicKeyInfo();
        try {
            this.serverPublicKey = PublicKeyFactory.createKey(subjectPublicKeyInfo);
        }
        catch (RuntimeException ex) {
            throw new TlsFatalAlert((short)43);
        }
        if (!this.tlsSigner.isValidPublicKey(this.serverPublicKey)) {
            throw new TlsFatalAlert((short)46);
        }
        TlsUtils.validateKeyUsage(certificate2, 128);
        super.processServerCertificate(certificate);
    }
    
    @Override
    public boolean requiresServerKeyExchange() {
        return true;
    }
    
    @Override
    public void processServerKeyExchange(final InputStream inputStream) throws IOException {
        final SecurityParameters securityParameters = this.context.getSecurityParameters();
        InputStream inputStream2 = inputStream;
        Signer initVerifyer = null;
        if (this.tlsSigner != null) {
            initVerifyer = this.initVerifyer(this.tlsSigner, securityParameters);
            inputStream2 = new SignerInputStream(inputStream, initVerifyer);
        }
        final byte[] opaque16 = TlsUtils.readOpaque16(inputStream2);
        final byte[] opaque17 = TlsUtils.readOpaque16(inputStream2);
        final byte[] opaque18 = TlsUtils.readOpaque8(inputStream2);
        final byte[] opaque19 = TlsUtils.readOpaque16(inputStream2);
        if (initVerifyer != null && !initVerifyer.verifySignature(TlsUtils.readOpaque16(inputStream))) {
            throw new TlsFatalAlert((short)51);
        }
        final BigInteger bigInteger = new BigInteger(1, opaque16);
        final BigInteger bigInteger2 = new BigInteger(1, opaque17);
        this.s = opaque18;
        try {
            this.B = SRP6Util.validatePublicValue(bigInteger, new BigInteger(1, opaque19));
        }
        catch (CryptoException ex) {
            throw new TlsFatalAlert((short)47);
        }
        this.srpClient.init(bigInteger, bigInteger2, new SHA1Digest(), this.context.getSecureRandom());
    }
    
    public void validateCertificateRequest(final CertificateRequest certificateRequest) throws IOException {
        throw new TlsFatalAlert((short)10);
    }
    
    public void processClientCredentials(final TlsCredentials tlsCredentials) throws IOException {
        throw new TlsFatalAlert((short)80);
    }
    
    public void generateClientKeyExchange(final OutputStream outputStream) throws IOException {
        TlsUtils.writeOpaque16(BigIntegers.asUnsignedByteArray(this.srpClient.generateClientCredentials(this.s, this.identity, this.password)), outputStream);
    }
    
    public byte[] generatePremasterSecret() throws IOException {
        try {
            return BigIntegers.asUnsignedByteArray(this.srpClient.calculateSecret(this.B));
        }
        catch (CryptoException ex) {
            throw new TlsFatalAlert((short)47);
        }
    }
    
    protected Signer initVerifyer(final TlsSigner tlsSigner, final SecurityParameters securityParameters) {
        final Signer verifyer = tlsSigner.createVerifyer(this.serverPublicKey);
        verifyer.update(securityParameters.clientRandom, 0, securityParameters.clientRandom.length);
        verifyer.update(securityParameters.serverRandom, 0, securityParameters.serverRandom.length);
        return verifyer;
    }
}
