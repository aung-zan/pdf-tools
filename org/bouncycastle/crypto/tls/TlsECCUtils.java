// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.util.Integers;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.SecureRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.agreement.ECDHBasicAgreement;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.util.BigIntegers;
import java.math.BigInteger;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.crypto.params.ECDomainParameters;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Hashtable;

public class TlsECCUtils
{
    public static final Integer EXT_elliptic_curves;
    public static final Integer EXT_ec_point_formats;
    private static final String[] curveNames;
    
    public static void addSupportedEllipticCurvesExtension(final Hashtable hashtable, final int[] array) throws IOException {
        hashtable.put(TlsECCUtils.EXT_elliptic_curves, createSupportedEllipticCurvesExtension(array));
    }
    
    public static void addSupportedPointFormatsExtension(final Hashtable hashtable, final short[] array) throws IOException {
        hashtable.put(TlsECCUtils.EXT_ec_point_formats, createSupportedPointFormatsExtension(array));
    }
    
    public static int[] getSupportedEllipticCurvesExtension(final Hashtable hashtable) throws IOException {
        if (hashtable == null) {
            return null;
        }
        final byte[] array = hashtable.get(TlsECCUtils.EXT_elliptic_curves);
        if (array == null) {
            return null;
        }
        return readSupportedEllipticCurvesExtension(array);
    }
    
    public static short[] getSupportedPointFormatsExtension(final Hashtable hashtable) throws IOException {
        if (hashtable == null) {
            return null;
        }
        final byte[] array = hashtable.get(TlsECCUtils.EXT_ec_point_formats);
        if (array == null) {
            return null;
        }
        return readSupportedPointFormatsExtension(array);
    }
    
    public static byte[] createSupportedEllipticCurvesExtension(final int[] array) throws IOException {
        if (array == null || array.length < 1) {
            throw new TlsFatalAlert((short)80);
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint16(2 * array.length, byteArrayOutputStream);
        TlsUtils.writeUint16Array(array, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    public static byte[] createSupportedPointFormatsExtension(short[] array) throws IOException {
        if (array == null) {
            array = new short[] { 0 };
        }
        else if (!TlsProtocol.arrayContains(array, (short)0)) {
            final short[] array2 = new short[array.length + 1];
            System.arraycopy(array, 0, array2, 0, array.length);
            array2[array.length] = 0;
            array = array2;
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        TlsUtils.writeUint8((short)array.length, byteArrayOutputStream);
        TlsUtils.writeUint8Array(array, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    public static int[] readSupportedEllipticCurvesExtension(final byte[] buf) throws IOException {
        if (buf == null) {
            throw new IllegalArgumentException("'extensionValue' cannot be null");
        }
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        final int uint16 = TlsUtils.readUint16(byteArrayInputStream);
        if (uint16 < 2 || (uint16 & 0x1) != 0x0) {
            throw new TlsFatalAlert((short)50);
        }
        final int[] uint16Array = TlsUtils.readUint16Array(uint16 / 2, byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        return uint16Array;
    }
    
    public static short[] readSupportedPointFormatsExtension(final byte[] buf) throws IOException {
        if (buf == null) {
            throw new IllegalArgumentException("'extensionValue' cannot be null");
        }
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        final short uint8 = TlsUtils.readUint8(byteArrayInputStream);
        if (uint8 < 1) {
            throw new TlsFatalAlert((short)50);
        }
        final short[] uint8Array = TlsUtils.readUint8Array(uint8, byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        if (!TlsProtocol.arrayContains(uint8Array, (short)0)) {
            throw new TlsFatalAlert((short)47);
        }
        return uint8Array;
    }
    
    public static String getNameOfNamedCurve(final int n) {
        return isSupportedNamedCurve(n) ? TlsECCUtils.curveNames[n - 1] : null;
    }
    
    public static ECDomainParameters getParametersForNamedCurve(final int n) {
        final String nameOfNamedCurve = getNameOfNamedCurve(n);
        if (nameOfNamedCurve == null) {
            return null;
        }
        final X9ECParameters byName = SECNamedCurves.getByName(nameOfNamedCurve);
        if (byName == null) {
            return null;
        }
        return new ECDomainParameters(byName.getCurve(), byName.getG(), byName.getN(), byName.getH(), byName.getSeed());
    }
    
    public static boolean hasAnySupportedNamedCurves() {
        return TlsECCUtils.curveNames.length > 0;
    }
    
    public static boolean containsECCCipherSuites(final int[] array) {
        for (int i = 0; i < array.length; ++i) {
            if (isECCCipherSuite(array[i])) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isECCCipherSuite(final int n) {
        switch (n) {
            case 49153:
            case 49154:
            case 49155:
            case 49156:
            case 49157:
            case 49158:
            case 49159:
            case 49160:
            case 49161:
            case 49162:
            case 49163:
            case 49164:
            case 49165:
            case 49166:
            case 49167:
            case 49168:
            case 49169:
            case 49170:
            case 49171:
            case 49172:
            case 49173:
            case 49174:
            case 49175:
            case 49176:
            case 49177:
            case 49187:
            case 49188:
            case 49189:
            case 49190:
            case 49191:
            case 49192:
            case 49193:
            case 49194:
            case 49195:
            case 49196:
            case 49197:
            case 49198:
            case 49199:
            case 49200:
            case 49201:
            case 49202: {
                return true;
            }
            default: {
                return false;
            }
        }
    }
    
    public static boolean areOnSameCurve(final ECDomainParameters ecDomainParameters, final ECDomainParameters ecDomainParameters2) {
        return ecDomainParameters.getCurve().equals(ecDomainParameters2.getCurve()) && ecDomainParameters.getG().equals(ecDomainParameters2.getG()) && ecDomainParameters.getN().equals(ecDomainParameters2.getN()) && ecDomainParameters.getH().equals(ecDomainParameters2.getH());
    }
    
    public static boolean isSupportedNamedCurve(final int n) {
        return n > 0 && n <= TlsECCUtils.curveNames.length;
    }
    
    public static boolean isCompressionPreferred(final short[] array, final short n) {
        if (array == null) {
            return false;
        }
        for (int i = 0; i < array.length; ++i) {
            final short n2 = array[i];
            if (n2 == 0) {
                return false;
            }
            if (n2 == n) {
                return true;
            }
        }
        return false;
    }
    
    public static byte[] serializeECFieldElement(final int n, final BigInteger bigInteger) throws IOException {
        return BigIntegers.asUnsignedByteArray((n + 7) / 8, bigInteger);
    }
    
    public static byte[] serializeECPoint(final short[] array, final ECPoint ecPoint) throws IOException {
        final ECCurve curve = ecPoint.getCurve();
        boolean b = false;
        if (curve instanceof ECCurve.F2m) {
            b = isCompressionPreferred(array, (short)2);
        }
        else if (curve instanceof ECCurve.Fp) {
            b = isCompressionPreferred(array, (short)1);
        }
        return ecPoint.getEncoded(b);
    }
    
    public static byte[] serializeECPublicKey(final short[] array, final ECPublicKeyParameters ecPublicKeyParameters) throws IOException {
        return serializeECPoint(array, ecPublicKeyParameters.getQ());
    }
    
    public static BigInteger deserializeECFieldElement(final int n, final byte[] magnitude) throws IOException {
        if (magnitude.length != (n + 7) / 8) {
            throw new TlsFatalAlert((short)50);
        }
        return new BigInteger(1, magnitude);
    }
    
    public static ECPoint deserializeECPoint(final short[] array, final ECCurve ecCurve, final byte[] array2) throws IOException {
        return ecCurve.decodePoint(array2);
    }
    
    public static ECPublicKeyParameters deserializeECPublicKey(final short[] array, final ECDomainParameters ecDomainParameters, final byte[] array2) throws IOException {
        try {
            return new ECPublicKeyParameters(deserializeECPoint(array, ecDomainParameters.getCurve(), array2), ecDomainParameters);
        }
        catch (RuntimeException ex) {
            throw new TlsFatalAlert((short)47);
        }
    }
    
    public static byte[] calculateECDHBasicAgreement(final ECPublicKeyParameters ecPublicKeyParameters, final ECPrivateKeyParameters ecPrivateKeyParameters) {
        final ECDHBasicAgreement ecdhBasicAgreement = new ECDHBasicAgreement();
        ecdhBasicAgreement.init(ecPrivateKeyParameters);
        return BigIntegers.asUnsignedByteArray(ecdhBasicAgreement.getFieldSize(), ecdhBasicAgreement.calculateAgreement(ecPublicKeyParameters));
    }
    
    public static AsymmetricCipherKeyPair generateECKeyPair(final SecureRandom secureRandom, final ECDomainParameters ecDomainParameters) {
        final ECKeyPairGenerator ecKeyPairGenerator = new ECKeyPairGenerator();
        ecKeyPairGenerator.init(new ECKeyGenerationParameters(ecDomainParameters, secureRandom));
        return ecKeyPairGenerator.generateKeyPair();
    }
    
    public static ECPublicKeyParameters validateECPublicKey(final ECPublicKeyParameters ecPublicKeyParameters) throws IOException {
        return ecPublicKeyParameters;
    }
    
    public static int readECExponent(final int n, final InputStream inputStream) throws IOException {
        final BigInteger ecParameter = readECParameter(inputStream);
        if (ecParameter.bitLength() < 32) {
            final int intValue = ecParameter.intValue();
            if (intValue > 0 && intValue < n) {
                return intValue;
            }
        }
        throw new TlsFatalAlert((short)47);
    }
    
    public static BigInteger readECFieldElement(final int n, final InputStream inputStream) throws IOException {
        return deserializeECFieldElement(n, TlsUtils.readOpaque8(inputStream));
    }
    
    public static BigInteger readECParameter(final InputStream inputStream) throws IOException {
        return new BigInteger(1, TlsUtils.readOpaque8(inputStream));
    }
    
    public static ECDomainParameters readECParameters(final int[] array, final short[] array2, final InputStream inputStream) throws IOException {
        try {
            switch (TlsUtils.readUint8(inputStream)) {
                case 1: {
                    final BigInteger ecParameter = readECParameter(inputStream);
                    final ECCurve.Fp fp = new ECCurve.Fp(ecParameter, readECFieldElement(ecParameter.bitLength(), inputStream), readECFieldElement(ecParameter.bitLength(), inputStream));
                    return new ECDomainParameters(fp, deserializeECPoint(array2, fp, TlsUtils.readOpaque8(inputStream)), readECParameter(inputStream), readECParameter(inputStream));
                }
                case 2: {
                    final int uint16 = TlsUtils.readUint16(inputStream);
                    ECCurve.F2m f2m = null;
                    switch (TlsUtils.readUint8(inputStream)) {
                        case 1: {
                            f2m = new ECCurve.F2m(uint16, readECExponent(uint16, inputStream), readECFieldElement(uint16, inputStream), readECFieldElement(uint16, inputStream));
                            break;
                        }
                        case 2: {
                            f2m = new ECCurve.F2m(uint16, readECExponent(uint16, inputStream), readECExponent(uint16, inputStream), readECExponent(uint16, inputStream), readECFieldElement(uint16, inputStream), readECFieldElement(uint16, inputStream));
                            break;
                        }
                        default: {
                            throw new TlsFatalAlert((short)47);
                        }
                    }
                    return new ECDomainParameters(f2m, deserializeECPoint(array2, f2m, TlsUtils.readOpaque8(inputStream)), readECParameter(inputStream), readECParameter(inputStream));
                }
                case 3: {
                    final int uint17 = TlsUtils.readUint16(inputStream);
                    if (!NamedCurve.refersToASpecificNamedCurve(uint17)) {
                        throw new TlsFatalAlert((short)47);
                    }
                    if (!TlsProtocol.arrayContains(array, uint17)) {
                        throw new TlsFatalAlert((short)47);
                    }
                    return getParametersForNamedCurve(uint17);
                }
                default: {
                    throw new TlsFatalAlert((short)47);
                }
            }
        }
        catch (RuntimeException ex) {
            throw new TlsFatalAlert((short)47);
        }
    }
    
    public static void writeECExponent(final int n, final OutputStream outputStream) throws IOException {
        writeECParameter(BigInteger.valueOf(n), outputStream);
    }
    
    public static void writeECFieldElement(final int n, final BigInteger bigInteger, final OutputStream outputStream) throws IOException {
        TlsUtils.writeOpaque8(serializeECFieldElement(n, bigInteger), outputStream);
    }
    
    public static void writeECParameter(final BigInteger bigInteger, final OutputStream outputStream) throws IOException {
        TlsUtils.writeOpaque8(BigIntegers.asUnsignedByteArray(bigInteger), outputStream);
    }
    
    public static void writeExplicitECParameters(final short[] array, final ECDomainParameters ecDomainParameters, final OutputStream outputStream) throws IOException {
        final ECCurve curve = ecDomainParameters.getCurve();
        if (curve instanceof ECCurve.Fp) {
            TlsUtils.writeUint8((short)1, outputStream);
            writeECParameter(((ECCurve.Fp)curve).getQ(), outputStream);
        }
        else {
            if (!(curve instanceof ECCurve.F2m)) {
                throw new IllegalArgumentException("'ecParameters' not a known curve type");
            }
            TlsUtils.writeUint8((short)2, outputStream);
            final ECCurve.F2m f2m = (ECCurve.F2m)curve;
            TlsUtils.writeUint16(f2m.getM(), outputStream);
            if (f2m.isTrinomial()) {
                TlsUtils.writeUint8((short)1, outputStream);
                writeECExponent(f2m.getK1(), outputStream);
            }
            else {
                TlsUtils.writeUint8((short)2, outputStream);
                writeECExponent(f2m.getK1(), outputStream);
                writeECExponent(f2m.getK2(), outputStream);
                writeECExponent(f2m.getK3(), outputStream);
            }
        }
        writeECFieldElement(curve.getFieldSize(), curve.getA().toBigInteger(), outputStream);
        writeECFieldElement(curve.getFieldSize(), curve.getB().toBigInteger(), outputStream);
        TlsUtils.writeOpaque8(serializeECPoint(array, ecDomainParameters.getG()), outputStream);
        writeECParameter(ecDomainParameters.getN(), outputStream);
        writeECParameter(ecDomainParameters.getH(), outputStream);
    }
    
    public static void writeNamedECParameters(final int n, final OutputStream outputStream) throws IOException {
        if (!NamedCurve.refersToASpecificNamedCurve(n)) {
            throw new TlsFatalAlert((short)80);
        }
        TlsUtils.writeUint8((short)3, outputStream);
        TlsUtils.writeUint16(n, outputStream);
    }
    
    static {
        EXT_elliptic_curves = Integers.valueOf(10);
        EXT_ec_point_formats = Integers.valueOf(11);
        curveNames = new String[] { "sect163k1", "sect163r1", "sect163r2", "sect193r1", "sect193r2", "sect233k1", "sect233r1", "sect239k1", "sect283k1", "sect283r1", "sect409k1", "sect409r1", "sect571k1", "sect571r1", "secp160k1", "secp160r1", "secp160r2", "secp192k1", "secp192r1", "secp224k1", "secp224r1", "secp256k1", "secp256r1", "secp384r1", "secp521r1" };
    }
}
