// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import org.bouncycastle.crypto.params.DHParameters;
import java.math.BigInteger;
import java.io.InputStream;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import java.io.IOException;
import java.util.Vector;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.DHPrivateKeyParameters;
import org.bouncycastle.crypto.params.DHPublicKeyParameters;

public class TlsPSKKeyExchange extends AbstractTlsKeyExchange
{
    protected TlsPSKIdentity pskIdentity;
    protected byte[] psk_identity_hint;
    protected DHPublicKeyParameters dhAgreeServerPublicKey;
    protected DHPrivateKeyParameters dhAgreeClientPrivateKey;
    protected AsymmetricKeyParameter serverPublicKey;
    protected RSAKeyParameters rsaServerPublicKey;
    protected byte[] premasterSecret;
    
    public TlsPSKKeyExchange(final int n, final Vector vector, final TlsPSKIdentity pskIdentity) {
        super(n, vector);
        this.psk_identity_hint = null;
        this.dhAgreeServerPublicKey = null;
        this.dhAgreeClientPrivateKey = null;
        this.serverPublicKey = null;
        this.rsaServerPublicKey = null;
        switch (n) {
            case 13:
            case 14:
            case 15: {
                this.pskIdentity = pskIdentity;
            }
            default: {
                throw new IllegalArgumentException("unsupported key exchange algorithm");
            }
        }
    }
    
    public void skipServerCredentials() throws IOException {
        if (this.keyExchange == 15) {
            throw new TlsFatalAlert((short)10);
        }
    }
    
    @Override
    public void processServerCertificate(final Certificate certificate) throws IOException {
        if (this.keyExchange != 15) {
            throw new TlsFatalAlert((short)10);
        }
        if (certificate.isEmpty()) {
            throw new TlsFatalAlert((short)42);
        }
        final org.bouncycastle.asn1.x509.Certificate certificate2 = certificate.getCertificateAt(0);
        final SubjectPublicKeyInfo subjectPublicKeyInfo = certificate2.getSubjectPublicKeyInfo();
        try {
            this.serverPublicKey = PublicKeyFactory.createKey(subjectPublicKeyInfo);
        }
        catch (RuntimeException ex) {
            throw new TlsFatalAlert((short)43);
        }
        if (this.serverPublicKey.isPrivate()) {
            throw new TlsFatalAlert((short)80);
        }
        this.rsaServerPublicKey = this.validateRSAPublicKey((RSAKeyParameters)this.serverPublicKey);
        TlsUtils.validateKeyUsage(certificate2, 32);
        super.processServerCertificate(certificate);
    }
    
    @Override
    public boolean requiresServerKeyExchange() {
        return this.keyExchange == 14;
    }
    
    @Override
    public void processServerKeyExchange(final InputStream inputStream) throws IOException {
        this.psk_identity_hint = TlsUtils.readOpaque16(inputStream);
        if (this.keyExchange == 14) {
            this.dhAgreeServerPublicKey = TlsDHUtils.validateDHPublicKey(new DHPublicKeyParameters(new BigInteger(1, TlsUtils.readOpaque16(inputStream)), new DHParameters(new BigInteger(1, TlsUtils.readOpaque16(inputStream)), new BigInteger(1, TlsUtils.readOpaque16(inputStream)))));
        }
    }
    
    public void validateCertificateRequest(final CertificateRequest certificateRequest) throws IOException {
        throw new TlsFatalAlert((short)10);
    }
    
    public void processClientCredentials(final TlsCredentials tlsCredentials) throws IOException {
        throw new TlsFatalAlert((short)80);
    }
    
    public void generateClientKeyExchange(final OutputStream outputStream) throws IOException {
        if (this.psk_identity_hint == null) {
            this.pskIdentity.skipIdentityHint();
        }
        else {
            this.pskIdentity.notifyIdentityHint(this.psk_identity_hint);
        }
        TlsUtils.writeOpaque16(this.pskIdentity.getPSKIdentity(), outputStream);
        if (this.keyExchange == 15) {
            this.premasterSecret = TlsRSAUtils.generateEncryptedPreMasterSecret(this.context, this.rsaServerPublicKey, outputStream);
        }
        else if (this.keyExchange == 14) {
            this.dhAgreeClientPrivateKey = TlsDHUtils.generateEphemeralClientKeyExchange(this.context.getSecureRandom(), this.dhAgreeServerPublicKey.getParameters(), outputStream);
        }
    }
    
    public byte[] generatePremasterSecret() throws IOException {
        final byte[] psk = this.pskIdentity.getPSK();
        final byte[] generateOtherSecret = this.generateOtherSecret(psk.length);
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(4 + generateOtherSecret.length + psk.length);
        TlsUtils.writeOpaque16(generateOtherSecret, byteArrayOutputStream);
        TlsUtils.writeOpaque16(psk, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    protected byte[] generateOtherSecret(final int n) {
        if (this.keyExchange == 14) {
            return TlsDHUtils.calculateDHBasicAgreement(this.dhAgreeServerPublicKey, this.dhAgreeClientPrivateKey);
        }
        if (this.keyExchange == 15) {
            return this.premasterSecret;
        }
        return new byte[n];
    }
    
    protected RSAKeyParameters validateRSAPublicKey(final RSAKeyParameters rsaKeyParameters) throws IOException {
        if (!rsaKeyParameters.getExponent().isProbablePrime(2)) {
            throw new TlsFatalAlert((short)47);
        }
        return rsaKeyParameters;
    }
}
