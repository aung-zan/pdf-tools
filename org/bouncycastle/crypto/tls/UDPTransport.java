// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.net.DatagramPacket;
import java.io.IOException;
import java.net.DatagramSocket;

public class UDPTransport implements DatagramTransport
{
    private static final int MIN_IP_OVERHEAD = 20;
    private static final int MAX_IP_OVERHEAD = 84;
    private static final int UDP_OVERHEAD = 8;
    private final DatagramSocket socket;
    private final int receiveLimit;
    private final int sendLimit;
    
    public UDPTransport(final DatagramSocket socket, final int n) throws IOException {
        if (!socket.isBound() || !socket.isConnected()) {
            throw new IllegalArgumentException("'socket' must be bound and connected");
        }
        this.socket = socket;
        this.receiveLimit = n - 20 - 8;
        this.sendLimit = n - 84 - 8;
    }
    
    public int getReceiveLimit() {
        return this.receiveLimit;
    }
    
    public int getSendLimit() {
        return this.sendLimit;
    }
    
    public int receive(final byte[] buf, final int offset, final int length, final int soTimeout) throws IOException {
        this.socket.setSoTimeout(soTimeout);
        final DatagramPacket p4 = new DatagramPacket(buf, offset, length);
        this.socket.receive(p4);
        return p4.getLength();
    }
    
    public void send(final byte[] buf, final int offset, final int length) throws IOException {
        if (length > this.getSendLimit()) {}
        this.socket.send(new DatagramPacket(buf, offset, length));
    }
    
    public void close() throws IOException {
        this.socket.close();
    }
}
