// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

public abstract class AbstractTlsClient extends AbstractTlsPeer implements TlsClient
{
    protected TlsCipherFactory cipherFactory;
    protected TlsClientContext context;
    protected Vector supportedSignatureAlgorithms;
    protected int selectedCipherSuite;
    protected short selectedCompressionMethod;
    
    public AbstractTlsClient() {
        this(new DefaultTlsCipherFactory());
    }
    
    public AbstractTlsClient(final TlsCipherFactory cipherFactory) {
        this.cipherFactory = cipherFactory;
    }
    
    public void init(final TlsClientContext context) {
        this.context = context;
    }
    
    public ProtocolVersion getClientHelloRecordLayerVersion() {
        return this.getClientVersion();
    }
    
    public ProtocolVersion getClientVersion() {
        return ProtocolVersion.TLSv11;
    }
    
    public Hashtable getClientExtensions() throws IOException {
        Hashtable hashtable = null;
        if (TlsUtils.isSignatureAlgorithmsExtensionAllowed(this.context.getClientVersion())) {
            final short[] array = { 6, 5, 4, 3, 2 };
            final short[] array2 = { 1 };
            this.supportedSignatureAlgorithms = new Vector();
            for (int i = 0; i < array.length; ++i) {
                for (int j = 0; j < array2.length; ++j) {
                    this.supportedSignatureAlgorithms.addElement(new SignatureAndHashAlgorithm(array[i], array2[j]));
                }
            }
            this.supportedSignatureAlgorithms.addElement(new SignatureAndHashAlgorithm((short)2, (short)2));
            if (hashtable == null) {
                hashtable = new Hashtable();
            }
            TlsUtils.addSignatureAlgorithmsExtension(hashtable, this.supportedSignatureAlgorithms);
        }
        return hashtable;
    }
    
    public ProtocolVersion getMinimumVersion() {
        return ProtocolVersion.TLSv10;
    }
    
    public void notifyServerVersion(final ProtocolVersion protocolVersion) throws IOException {
        if (!this.getMinimumVersion().isEqualOrEarlierVersionOf(protocolVersion)) {
            throw new TlsFatalAlert((short)70);
        }
    }
    
    public short[] getCompressionMethods() {
        return new short[] { 0 };
    }
    
    public void notifySessionID(final byte[] array) {
    }
    
    public void notifySelectedCipherSuite(final int selectedCipherSuite) {
        this.selectedCipherSuite = selectedCipherSuite;
    }
    
    public void notifySelectedCompressionMethod(final short selectedCompressionMethod) {
        this.selectedCompressionMethod = selectedCompressionMethod;
    }
    
    public void notifySecureRenegotiation(final boolean b) throws IOException {
        if (!b) {}
    }
    
    public void processServerExtensions(final Hashtable hashtable) throws IOException {
        if (hashtable != null && hashtable.containsKey(TlsUtils.EXT_signature_algorithms)) {
            throw new TlsFatalAlert((short)47);
        }
    }
    
    public void processServerSupplementalData(final Vector vector) throws IOException {
        if (vector != null) {
            throw new TlsFatalAlert((short)10);
        }
    }
    
    public Vector getClientSupplementalData() throws IOException {
        return null;
    }
    
    public TlsCompression getCompression() throws IOException {
        switch (this.selectedCompressionMethod) {
            case 0: {
                return new TlsNullCompression();
            }
            default: {
                throw new TlsFatalAlert((short)80);
            }
        }
    }
    
    public void notifyNewSessionTicket(final NewSessionTicket newSessionTicket) throws IOException {
    }
    
    public void notifyHandshakeComplete() throws IOException {
    }
}
