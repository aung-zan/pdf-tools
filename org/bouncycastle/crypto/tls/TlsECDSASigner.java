// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.signers.ECDSASigner;
import org.bouncycastle.crypto.DSA;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class TlsECDSASigner extends TlsDSASigner
{
    public boolean isValidPublicKey(final AsymmetricKeyParameter asymmetricKeyParameter) {
        return asymmetricKeyParameter instanceof ECPublicKeyParameters;
    }
    
    @Override
    protected DSA createDSAImpl() {
        return new ECDSASigner();
    }
}
