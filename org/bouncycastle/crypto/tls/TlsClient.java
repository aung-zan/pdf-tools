// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.util.Vector;
import java.io.IOException;
import java.util.Hashtable;

public interface TlsClient extends TlsPeer
{
    void init(final TlsClientContext p0);
    
    ProtocolVersion getClientHelloRecordLayerVersion();
    
    ProtocolVersion getClientVersion();
    
    int[] getCipherSuites();
    
    short[] getCompressionMethods();
    
    Hashtable getClientExtensions() throws IOException;
    
    void notifyServerVersion(final ProtocolVersion p0) throws IOException;
    
    void notifySessionID(final byte[] p0);
    
    void notifySelectedCipherSuite(final int p0);
    
    void notifySelectedCompressionMethod(final short p0);
    
    void notifySecureRenegotiation(final boolean p0) throws IOException;
    
    void processServerExtensions(final Hashtable p0) throws IOException;
    
    void processServerSupplementalData(final Vector p0) throws IOException;
    
    TlsKeyExchange getKeyExchange() throws IOException;
    
    TlsAuthentication getAuthentication() throws IOException;
    
    Vector getClientSupplementalData() throws IOException;
    
    TlsCompression getCompression() throws IOException;
    
    TlsCipher getCipher() throws IOException;
    
    void notifyNewSessionTicket(final NewSessionTicket p0) throws IOException;
    
    void notifyHandshakeComplete() throws IOException;
}
