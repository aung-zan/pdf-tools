// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.AEADParameters;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.modes.AEADBlockCipher;

public class TlsAEADCipher implements TlsCipher
{
    protected TlsContext context;
    protected int macSize;
    protected int nonce_explicit_length;
    protected AEADBlockCipher encryptCipher;
    protected AEADBlockCipher decryptCipher;
    protected byte[] encryptImplicitNonce;
    protected byte[] decryptImplicitNonce;
    
    public TlsAEADCipher(final TlsContext context, final AEADBlockCipher aeadBlockCipher, final AEADBlockCipher aeadBlockCipher2, final int n, final int macSize) throws IOException {
        if (!ProtocolVersion.TLSv12.isEqualOrEarlierVersionOf(context.getServerVersion().getEquivalentTLSVersion())) {
            throw new TlsFatalAlert((short)80);
        }
        this.context = context;
        this.macSize = macSize;
        this.nonce_explicit_length = 8;
        final int n2 = 4;
        final int n3 = 2 * n + 2 * n2;
        final byte[] calculateKeyBlock = TlsUtils.calculateKeyBlock(context, n3);
        final int n4 = 0;
        final KeyParameter keyParameter = new KeyParameter(calculateKeyBlock, n4, n);
        final int n5 = n4 + n;
        final KeyParameter keyParameter2 = new KeyParameter(calculateKeyBlock, n5, n);
        final int n6 = n5 + n;
        final byte[] copyOfRange = Arrays.copyOfRange(calculateKeyBlock, n6, n6 + n2);
        final int n7 = n6 + n2;
        final byte[] copyOfRange2 = Arrays.copyOfRange(calculateKeyBlock, n7, n7 + n2);
        if (n7 + n2 != n3) {
            throw new TlsFatalAlert((short)80);
        }
        KeyParameter keyParameter3;
        KeyParameter keyParameter4;
        if (context.isServer()) {
            this.encryptCipher = aeadBlockCipher2;
            this.decryptCipher = aeadBlockCipher;
            this.encryptImplicitNonce = copyOfRange2;
            this.decryptImplicitNonce = copyOfRange;
            keyParameter3 = keyParameter2;
            keyParameter4 = keyParameter;
        }
        else {
            this.encryptCipher = aeadBlockCipher;
            this.decryptCipher = aeadBlockCipher2;
            this.encryptImplicitNonce = copyOfRange;
            this.decryptImplicitNonce = copyOfRange2;
            keyParameter3 = keyParameter;
            keyParameter4 = keyParameter2;
        }
        final byte[] array = new byte[n2 + this.nonce_explicit_length];
        this.encryptCipher.init(true, new AEADParameters(keyParameter3, 8 * macSize, array));
        this.decryptCipher.init(false, new AEADParameters(keyParameter4, 8 * macSize, array));
    }
    
    public int getPlaintextLimit(final int n) {
        return n - this.macSize - this.nonce_explicit_length;
    }
    
    public byte[] encodePlaintext(final long n, final short n2, final byte[] array, final int n3, final int n4) throws IOException {
        final byte[] array2 = new byte[this.encryptImplicitNonce.length + this.nonce_explicit_length];
        System.arraycopy(this.encryptImplicitNonce, 0, array2, 0, this.encryptImplicitNonce.length);
        TlsUtils.writeUint64(n, array2, this.encryptImplicitNonce.length);
        final byte[] array3 = new byte[this.nonce_explicit_length + this.encryptCipher.getOutputSize(n4)];
        System.arraycopy(array2, this.encryptImplicitNonce.length, array3, 0, this.nonce_explicit_length);
        final int nonce_explicit_length = this.nonce_explicit_length;
        this.encryptCipher.init(true, new AEADParameters(null, 8 * this.macSize, array2, this.getAdditionalData(n, n2, n4)));
        final int n5 = nonce_explicit_length + this.encryptCipher.processBytes(array, n3, n4, array3, nonce_explicit_length);
        int n6;
        try {
            n6 = n5 + this.encryptCipher.doFinal(array3, n5);
        }
        catch (Exception ex) {
            throw new TlsFatalAlert((short)80);
        }
        if (n6 != array3.length) {
            throw new TlsFatalAlert((short)80);
        }
        return array3;
    }
    
    public byte[] decodeCiphertext(final long n, final short n2, final byte[] array, final int n3, final int n4) throws IOException {
        if (this.getPlaintextLimit(n4) < 0) {
            throw new TlsFatalAlert((short)50);
        }
        final byte[] array2 = new byte[this.decryptImplicitNonce.length + this.nonce_explicit_length];
        System.arraycopy(this.decryptImplicitNonce, 0, array2, 0, this.decryptImplicitNonce.length);
        System.arraycopy(array, n3, array2, this.decryptImplicitNonce.length, this.nonce_explicit_length);
        final int n5 = n3 + this.nonce_explicit_length;
        final int n6 = n4 - this.nonce_explicit_length;
        final int outputSize = this.decryptCipher.getOutputSize(n6);
        final byte[] array3 = new byte[outputSize];
        final int n7 = 0;
        this.decryptCipher.init(false, new AEADParameters(null, 8 * this.macSize, array2, this.getAdditionalData(n, n2, outputSize)));
        final int n8 = n7 + this.decryptCipher.processBytes(array, n5, n6, array3, n7);
        int n9;
        try {
            n9 = n8 + this.decryptCipher.doFinal(array3, n8);
        }
        catch (Exception ex) {
            throw new TlsFatalAlert((short)20);
        }
        if (n9 != array3.length) {
            throw new TlsFatalAlert((short)80);
        }
        return array3;
    }
    
    protected byte[] getAdditionalData(final long n, final short n2, final int n3) throws IOException {
        final byte[] array = new byte[13];
        TlsUtils.writeUint64(n, array, 0);
        TlsUtils.writeUint8(n2, array, 8);
        TlsUtils.writeVersion(this.context.getServerVersion(), array, 9);
        TlsUtils.writeUint16(n3, array, 11);
        return array;
    }
}
