// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;

public abstract class PSKTlsClient extends AbstractTlsClient
{
    protected TlsPSKIdentity pskIdentity;
    
    public PSKTlsClient(final TlsPSKIdentity pskIdentity) {
        this.pskIdentity = pskIdentity;
    }
    
    public PSKTlsClient(final TlsCipherFactory tlsCipherFactory, final TlsPSKIdentity pskIdentity) {
        super(tlsCipherFactory);
        this.pskIdentity = pskIdentity;
    }
    
    public int[] getCipherSuites() {
        return new int[] { 145, 144, 143, 142, 149, 148, 147, 146, 141, 140, 139, 138 };
    }
    
    public TlsKeyExchange getKeyExchange() throws IOException {
        switch (this.selectedCipherSuite) {
            case 44:
            case 138:
            case 139:
            case 140:
            case 141: {
                return this.createPSKKeyExchange(13);
            }
            case 46:
            case 146:
            case 147:
            case 148:
            case 149: {
                return this.createPSKKeyExchange(15);
            }
            case 45:
            case 142:
            case 143:
            case 144:
            case 145: {
                return this.createPSKKeyExchange(14);
            }
            default: {
                throw new TlsFatalAlert((short)80);
            }
        }
    }
    
    public TlsCipher getCipher() throws IOException {
        switch (this.selectedCipherSuite) {
            case 139:
            case 143:
            case 147: {
                return this.cipherFactory.createCipher(this.context, 7, 2);
            }
            case 140:
            case 144:
            case 148: {
                return this.cipherFactory.createCipher(this.context, 8, 2);
            }
            case 141:
            case 145:
            case 149: {
                return this.cipherFactory.createCipher(this.context, 9, 2);
            }
            case 44:
            case 45:
            case 46: {
                return this.cipherFactory.createCipher(this.context, 0, 2);
            }
            case 138:
            case 142:
            case 146: {
                return this.cipherFactory.createCipher(this.context, 2, 2);
            }
            default: {
                throw new TlsFatalAlert((short)80);
            }
        }
    }
    
    protected TlsKeyExchange createPSKKeyExchange(final int n) {
        return new TlsPSKKeyExchange(n, this.supportedSignatureAlgorithms, this.pskIdentity);
    }
}
