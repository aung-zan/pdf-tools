// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.agreement.DHStandardGroups;
import org.bouncycastle.crypto.params.DHParameters;
import java.io.IOException;

public abstract class DefaultTlsServer extends AbstractTlsServer
{
    public DefaultTlsServer() {
    }
    
    public DefaultTlsServer(final TlsCipherFactory tlsCipherFactory) {
        super(tlsCipherFactory);
    }
    
    protected TlsEncryptionCredentials getRSAEncryptionCredentials() throws IOException {
        throw new TlsFatalAlert((short)80);
    }
    
    protected TlsSignerCredentials getRSASignerCredentials() throws IOException {
        throw new TlsFatalAlert((short)80);
    }
    
    protected DHParameters getDHParameters() {
        return DHStandardGroups.rfc5114_1024_160;
    }
    
    @Override
    protected int[] getCipherSuites() {
        return new int[] { 49172, 49171, 49170, 57, 51, 22, 53, 47, 10 };
    }
    
    public TlsCredentials getCredentials() throws IOException {
        switch (this.selectedCipherSuite) {
            case 1:
            case 2:
            case 4:
            case 5:
            case 10:
            case 47:
            case 53:
            case 59:
            case 60:
            case 61:
            case 65:
            case 132:
            case 150:
            case 156:
            case 157: {
                return this.getRSAEncryptionCredentials();
            }
            case 22:
            case 51:
            case 57:
            case 69:
            case 103:
            case 107:
            case 136:
            case 154:
            case 158:
            case 159:
            case 49170:
            case 49171:
            case 49172:
            case 49191:
            case 49192:
            case 49199:
            case 49200: {
                return this.getRSASignerCredentials();
            }
            default: {
                throw new TlsFatalAlert((short)80);
            }
        }
    }
    
    public TlsKeyExchange getKeyExchange() throws IOException {
        switch (this.selectedCipherSuite) {
            case 13:
            case 48:
            case 54:
            case 62:
            case 66:
            case 104:
            case 133:
            case 151:
            case 164:
            case 165: {
                return this.createDHKeyExchange(7);
            }
            case 16:
            case 49:
            case 55:
            case 63:
            case 67:
            case 105:
            case 134:
            case 152:
            case 160:
            case 161: {
                return this.createDHKeyExchange(9);
            }
            case 19:
            case 50:
            case 56:
            case 64:
            case 68:
            case 106:
            case 135:
            case 153:
            case 162:
            case 163: {
                return this.createDHEKeyExchange(3);
            }
            case 22:
            case 51:
            case 57:
            case 69:
            case 103:
            case 107:
            case 136:
            case 154:
            case 158:
            case 159: {
                return this.createDHEKeyExchange(5);
            }
            case 49153:
            case 49154:
            case 49155:
            case 49156:
            case 49157:
            case 49189:
            case 49190:
            case 49197:
            case 49198: {
                return this.createECDHKeyExchange(16);
            }
            case 49163:
            case 49164:
            case 49165:
            case 49166:
            case 49167:
            case 49193:
            case 49194:
            case 49201:
            case 49202: {
                return this.createECDHKeyExchange(18);
            }
            case 49158:
            case 49159:
            case 49160:
            case 49161:
            case 49162:
            case 49187:
            case 49188:
            case 49195:
            case 49196: {
                return this.createECDHEKeyExchange(17);
            }
            case 49168:
            case 49169:
            case 49170:
            case 49171:
            case 49172:
            case 49191:
            case 49192:
            case 49199:
            case 49200: {
                return this.createECDHEKeyExchange(19);
            }
            case 1:
            case 2:
            case 4:
            case 5:
            case 10:
            case 47:
            case 53:
            case 59:
            case 60:
            case 61:
            case 65:
            case 132:
            case 150:
            case 156:
            case 157: {
                return this.createRSAKeyExchange();
            }
            default: {
                throw new TlsFatalAlert((short)80);
            }
        }
    }
    
    public TlsCipher getCipher() throws IOException {
        switch (this.selectedCipherSuite) {
            case 10:
            case 13:
            case 16:
            case 19:
            case 22:
            case 49155:
            case 49160:
            case 49165:
            case 49170: {
                return this.cipherFactory.createCipher(this.context, 7, 2);
            }
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 49156:
            case 49161:
            case 49166:
            case 49171: {
                return this.cipherFactory.createCipher(this.context, 8, 2);
            }
            case 60:
            case 62:
            case 63:
            case 64:
            case 103:
            case 49187:
            case 49189:
            case 49191:
            case 49193: {
                return this.cipherFactory.createCipher(this.context, 8, 3);
            }
            case 156:
            case 158:
            case 160:
            case 162:
            case 164:
            case 49195:
            case 49197:
            case 49199:
            case 49201: {
                return this.cipherFactory.createCipher(this.context, 10, 0);
            }
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 49157:
            case 49162:
            case 49167:
            case 49172: {
                return this.cipherFactory.createCipher(this.context, 9, 2);
            }
            case 61:
            case 104:
            case 105:
            case 106:
            case 107: {
                return this.cipherFactory.createCipher(this.context, 9, 3);
            }
            case 49188:
            case 49190:
            case 49192:
            case 49194: {
                return this.cipherFactory.createCipher(this.context, 9, 4);
            }
            case 157:
            case 159:
            case 161:
            case 163:
            case 165:
            case 49196:
            case 49198:
            case 49200:
            case 49202: {
                return this.cipherFactory.createCipher(this.context, 11, 0);
            }
            case 65:
            case 66:
            case 67:
            case 68:
            case 69: {
                return this.cipherFactory.createCipher(this.context, 12, 2);
            }
            case 132:
            case 133:
            case 134:
            case 135:
            case 136: {
                return this.cipherFactory.createCipher(this.context, 13, 2);
            }
            case 1: {
                return this.cipherFactory.createCipher(this.context, 0, 1);
            }
            case 2:
            case 49153:
            case 49158:
            case 49163:
            case 49168: {
                return this.cipherFactory.createCipher(this.context, 0, 2);
            }
            case 59: {
                return this.cipherFactory.createCipher(this.context, 0, 3);
            }
            case 4: {
                return this.cipherFactory.createCipher(this.context, 2, 1);
            }
            case 5:
            case 49154:
            case 49159:
            case 49164:
            case 49169: {
                return this.cipherFactory.createCipher(this.context, 2, 2);
            }
            case 150:
            case 151:
            case 152:
            case 153:
            case 154: {
                return this.cipherFactory.createCipher(this.context, 14, 2);
            }
            default: {
                throw new TlsFatalAlert((short)80);
            }
        }
    }
    
    protected TlsKeyExchange createDHKeyExchange(final int n) {
        return new TlsDHKeyExchange(n, this.supportedSignatureAlgorithms, this.getDHParameters());
    }
    
    protected TlsKeyExchange createDHEKeyExchange(final int n) {
        return new TlsDHEKeyExchange(n, this.supportedSignatureAlgorithms, this.getDHParameters());
    }
    
    protected TlsKeyExchange createECDHKeyExchange(final int n) {
        return new TlsECDHKeyExchange(n, this.supportedSignatureAlgorithms, this.namedCurves, this.clientECPointFormats, this.serverECPointFormats);
    }
    
    protected TlsKeyExchange createECDHEKeyExchange(final int n) {
        return new TlsECDHEKeyExchange(n, this.supportedSignatureAlgorithms, this.namedCurves, this.clientECPointFormats, this.serverECPointFormats);
    }
    
    protected TlsKeyExchange createRSAKeyExchange() {
        return new TlsRSAKeyExchange(this.supportedSignatureAlgorithms);
    }
}
