// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.util.Hashtable;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.util.Vector;
import java.io.IOException;
import java.security.SecureRandom;

public class DTLSServerProtocol extends DTLSProtocol
{
    protected boolean verifyRequests;
    
    public DTLSServerProtocol(final SecureRandom secureRandom) {
        super(secureRandom);
        this.verifyRequests = true;
    }
    
    public boolean getVerifyRequests() {
        return this.verifyRequests;
    }
    
    public void setVerifyRequests(final boolean verifyRequests) {
        this.verifyRequests = verifyRequests;
    }
    
    public DTLSTransport accept(final TlsServer server, final DatagramTransport datagramTransport) throws IOException {
        if (server == null) {
            throw new IllegalArgumentException("'server' cannot be null");
        }
        if (datagramTransport == null) {
            throw new IllegalArgumentException("'transport' cannot be null");
        }
        final SecurityParameters securityParameters = new SecurityParameters();
        securityParameters.entity = 0;
        securityParameters.serverRandom = TlsProtocol.createRandomBlock(this.secureRandom);
        final ServerHandshakeState serverHandshakeState = new ServerHandshakeState();
        (serverHandshakeState.server = server).init(serverHandshakeState.serverContext = new TlsServerContextImpl(this.secureRandom, securityParameters));
        final DTLSRecordLayer dtlsRecordLayer = new DTLSRecordLayer(datagramTransport, serverHandshakeState.serverContext, server, (short)22);
        try {
            return this.serverHandshake(serverHandshakeState, dtlsRecordLayer);
        }
        catch (TlsFatalAlert tlsFatalAlert) {
            dtlsRecordLayer.fail(tlsFatalAlert.getAlertDescription());
            throw tlsFatalAlert;
        }
        catch (IOException ex) {
            dtlsRecordLayer.fail((short)80);
            throw ex;
        }
        catch (RuntimeException ex2) {
            dtlsRecordLayer.fail((short)80);
            throw new TlsFatalAlert((short)80);
        }
    }
    
    public DTLSTransport serverHandshake(final ServerHandshakeState serverHandshakeState, final DTLSRecordLayer dtlsRecordLayer) throws IOException {
        final SecurityParameters securityParameters = serverHandshakeState.serverContext.getSecurityParameters();
        final DTLSReliableHandshake dtlsReliableHandshake = new DTLSReliableHandshake(serverHandshakeState.serverContext, dtlsRecordLayer);
        final DTLSReliableHandshake.Message receiveMessage = dtlsReliableHandshake.receiveMessage();
        serverHandshakeState.serverContext.setClientVersion(dtlsRecordLayer.getDiscoveredPeerVersion());
        if (receiveMessage.getType() != 1) {
            throw new TlsFatalAlert((short)10);
        }
        this.processClientHello(serverHandshakeState, receiveMessage.getBody());
        dtlsReliableHandshake.sendMessage((short)2, this.generateServerHello(serverHandshakeState));
        securityParameters.prfAlgorithm = TlsProtocol.getPRFAlgorithm(serverHandshakeState.selectedCipherSuite);
        securityParameters.compressionAlgorithm = serverHandshakeState.selectedCompressionMethod;
        securityParameters.verifyDataLength = 12;
        dtlsReliableHandshake.notifyHelloComplete();
        final Vector serverSupplementalData = serverHandshakeState.server.getServerSupplementalData();
        if (serverSupplementalData != null) {
            dtlsReliableHandshake.sendMessage((short)23, DTLSProtocol.generateSupplementalData(serverSupplementalData));
        }
        (serverHandshakeState.keyExchange = serverHandshakeState.server.getKeyExchange()).init(serverHandshakeState.serverContext);
        serverHandshakeState.serverCredentials = serverHandshakeState.server.getCredentials();
        if (serverHandshakeState.serverCredentials == null) {
            serverHandshakeState.keyExchange.skipServerCredentials();
        }
        else {
            serverHandshakeState.keyExchange.processServerCredentials(serverHandshakeState.serverCredentials);
            dtlsReliableHandshake.sendMessage((short)11, DTLSProtocol.generateCertificate(serverHandshakeState.serverCredentials.getCertificate()));
        }
        final byte[] generateServerKeyExchange = serverHandshakeState.keyExchange.generateServerKeyExchange();
        if (generateServerKeyExchange != null) {
            dtlsReliableHandshake.sendMessage((short)12, generateServerKeyExchange);
        }
        if (serverHandshakeState.serverCredentials != null) {
            serverHandshakeState.certificateRequest = serverHandshakeState.server.getCertificateRequest();
            if (serverHandshakeState.certificateRequest != null) {
                serverHandshakeState.keyExchange.validateCertificateRequest(serverHandshakeState.certificateRequest);
                dtlsReliableHandshake.sendMessage((short)13, this.generateCertificateRequest(serverHandshakeState, serverHandshakeState.certificateRequest));
            }
        }
        dtlsReliableHandshake.sendMessage((short)14, TlsUtils.EMPTY_BYTES);
        DTLSReliableHandshake.Message message = dtlsReliableHandshake.receiveMessage();
        if (message.getType() == 23) {
            this.processClientSupplementalData(serverHandshakeState, message.getBody());
            message = dtlsReliableHandshake.receiveMessage();
        }
        else {
            serverHandshakeState.server.processClientSupplementalData(null);
        }
        if (serverHandshakeState.certificateRequest == null) {
            serverHandshakeState.keyExchange.skipClientCredentials();
        }
        else if (message.getType() == 11) {
            this.processClientCertificate(serverHandshakeState, message.getBody());
            message = dtlsReliableHandshake.receiveMessage();
        }
        else {
            if (ProtocolVersion.TLSv12.isEqualOrEarlierVersionOf(serverHandshakeState.serverContext.getServerVersion().getEquivalentTLSVersion())) {
                throw new TlsFatalAlert((short)10);
            }
            this.notifyClientCertificate(serverHandshakeState, Certificate.EMPTY_CHAIN);
        }
        if (message.getType() != 16) {
            throw new TlsFatalAlert((short)10);
        }
        this.processClientKeyExchange(serverHandshakeState, message.getBody());
        dtlsRecordLayer.initPendingEpoch(serverHandshakeState.server.getCipher());
        if (this.expectCertificateVerifyMessage(serverHandshakeState)) {
            final byte[] currentHash = dtlsReliableHandshake.getCurrentHash();
            final DTLSReliableHandshake.Message receiveMessage2 = dtlsReliableHandshake.receiveMessage();
            if (receiveMessage2.getType() != 15) {
                throw new TlsFatalAlert((short)10);
            }
            this.processCertificateVerify(serverHandshakeState, receiveMessage2.getBody(), currentHash);
        }
        final byte[] currentHash2 = dtlsReliableHandshake.getCurrentHash();
        final DTLSReliableHandshake.Message receiveMessage3 = dtlsReliableHandshake.receiveMessage();
        if (receiveMessage3.getType() == 20) {
            this.processFinished(receiveMessage3.getBody(), TlsUtils.calculateVerifyData(serverHandshakeState.serverContext, "client finished", currentHash2));
            if (serverHandshakeState.expectSessionTicket) {
                dtlsReliableHandshake.sendMessage((short)4, this.generateNewSessionTicket(serverHandshakeState, serverHandshakeState.server.getNewSessionTicket()));
            }
            dtlsReliableHandshake.sendMessage((short)20, TlsUtils.calculateVerifyData(serverHandshakeState.serverContext, "server finished", dtlsReliableHandshake.getCurrentHash()));
            dtlsReliableHandshake.finish();
            serverHandshakeState.server.notifyHandshakeComplete();
            return new DTLSTransport(dtlsRecordLayer);
        }
        throw new TlsFatalAlert((short)10);
    }
    
    protected byte[] generateCertificateRequest(final ServerHandshakeState serverHandshakeState, final CertificateRequest certificateRequest) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        certificateRequest.encode(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    protected byte[] generateNewSessionTicket(final ServerHandshakeState serverHandshakeState, final NewSessionTicket newSessionTicket) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        newSessionTicket.encode(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    protected byte[] generateServerHello(final ServerHandshakeState serverHandshakeState) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ProtocolVersion serverVersion = serverHandshakeState.server.getServerVersion();
        if (!serverVersion.isEqualOrEarlierVersionOf(serverHandshakeState.serverContext.getClientVersion())) {
            throw new TlsFatalAlert((short)80);
        }
        serverHandshakeState.serverContext.setServerVersion(serverVersion);
        TlsUtils.writeVersion(serverHandshakeState.serverContext.getServerVersion(), byteArrayOutputStream);
        byteArrayOutputStream.write(serverHandshakeState.serverContext.getSecurityParameters().serverRandom);
        TlsUtils.writeOpaque8(TlsUtils.EMPTY_BYTES, byteArrayOutputStream);
        serverHandshakeState.selectedCipherSuite = serverHandshakeState.server.getSelectedCipherSuite();
        if (!TlsProtocol.arrayContains(serverHandshakeState.offeredCipherSuites, serverHandshakeState.selectedCipherSuite) || serverHandshakeState.selectedCipherSuite == 0 || serverHandshakeState.selectedCipherSuite == 255) {
            throw new TlsFatalAlert((short)80);
        }
        DTLSProtocol.validateSelectedCipherSuite(serverHandshakeState.selectedCipherSuite, (short)80);
        serverHandshakeState.selectedCompressionMethod = serverHandshakeState.server.getSelectedCompressionMethod();
        if (!TlsProtocol.arrayContains(serverHandshakeState.offeredCompressionMethods, serverHandshakeState.selectedCompressionMethod)) {
            throw new TlsFatalAlert((short)80);
        }
        TlsUtils.writeUint16(serverHandshakeState.selectedCipherSuite, byteArrayOutputStream);
        TlsUtils.writeUint8(serverHandshakeState.selectedCompressionMethod, byteArrayOutputStream);
        serverHandshakeState.serverExtensions = serverHandshakeState.server.getServerExtensions();
        if (serverHandshakeState.secure_renegotiation && (serverHandshakeState.serverExtensions == null || !serverHandshakeState.serverExtensions.containsKey(TlsProtocol.EXT_RenegotiationInfo))) {
            if (serverHandshakeState.serverExtensions == null) {
                serverHandshakeState.serverExtensions = new Hashtable();
            }
            serverHandshakeState.serverExtensions.put(TlsProtocol.EXT_RenegotiationInfo, TlsProtocol.createRenegotiationInfo(TlsUtils.EMPTY_BYTES));
        }
        if (serverHandshakeState.serverExtensions != null) {
            serverHandshakeState.expectSessionTicket = serverHandshakeState.serverExtensions.containsKey(TlsProtocol.EXT_SessionTicket);
            TlsProtocol.writeExtensions(byteArrayOutputStream, serverHandshakeState.serverExtensions);
        }
        return byteArrayOutputStream.toByteArray();
    }
    
    protected void notifyClientCertificate(final ServerHandshakeState serverHandshakeState, final Certificate clientCertificate) throws IOException {
        if (serverHandshakeState.certificateRequest == null) {
            throw new IllegalStateException();
        }
        if (serverHandshakeState.clientCertificate != null) {
            throw new TlsFatalAlert((short)10);
        }
        serverHandshakeState.clientCertificate = clientCertificate;
        if (clientCertificate.isEmpty()) {
            serverHandshakeState.keyExchange.skipClientCredentials();
        }
        else {
            serverHandshakeState.clientCertificateType = TlsUtils.getClientCertificateType(clientCertificate, serverHandshakeState.serverCredentials.getCertificate());
            serverHandshakeState.keyExchange.processClientCertificate(clientCertificate);
        }
        serverHandshakeState.server.notifyClientCertificate(clientCertificate);
    }
    
    protected void processClientCertificate(final ServerHandshakeState serverHandshakeState, final byte[] buf) throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        final Certificate parse = Certificate.parse(byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        this.notifyClientCertificate(serverHandshakeState, parse);
    }
    
    protected void processCertificateVerify(final ServerHandshakeState serverHandshakeState, final byte[] buf, final byte[] array) throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        final byte[] opaque16 = TlsUtils.readOpaque16(byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        try {
            final TlsSigner tlsSigner = TlsUtils.createTlsSigner(serverHandshakeState.clientCertificateType);
            tlsSigner.init(serverHandshakeState.serverContext);
            tlsSigner.verifyRawSignature(opaque16, PublicKeyFactory.createKey(serverHandshakeState.clientCertificate.getCertificateAt(0).getSubjectPublicKeyInfo()), array);
        }
        catch (Exception ex) {
            throw new TlsFatalAlert((short)51);
        }
    }
    
    protected void processClientHello(final ServerHandshakeState serverHandshakeState, final byte[] buf) throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        final ProtocolVersion version = TlsUtils.readVersion(byteArrayInputStream);
        if (!version.isDTLS()) {
            throw new TlsFatalAlert((short)47);
        }
        final byte[] fully = TlsUtils.readFully(32, byteArrayInputStream);
        if (TlsUtils.readOpaque8(byteArrayInputStream).length > 32) {
            throw new TlsFatalAlert((short)47);
        }
        TlsUtils.readOpaque8(byteArrayInputStream);
        final int uint16 = TlsUtils.readUint16(byteArrayInputStream);
        if (uint16 < 2 || (uint16 & 0x1) != 0x0) {
            throw new TlsFatalAlert((short)50);
        }
        serverHandshakeState.offeredCipherSuites = TlsUtils.readUint16Array(uint16 / 2, byteArrayInputStream);
        final short uint17 = TlsUtils.readUint8(byteArrayInputStream);
        if (uint17 < 1) {
            throw new TlsFatalAlert((short)47);
        }
        serverHandshakeState.offeredCompressionMethods = TlsUtils.readUint8Array(uint17, byteArrayInputStream);
        serverHandshakeState.clientExtensions = TlsProtocol.readExtensions(byteArrayInputStream);
        serverHandshakeState.serverContext.setClientVersion(version);
        serverHandshakeState.server.notifyClientVersion(version);
        serverHandshakeState.serverContext.getSecurityParameters().clientRandom = fully;
        serverHandshakeState.server.notifyOfferedCipherSuites(serverHandshakeState.offeredCipherSuites);
        serverHandshakeState.server.notifyOfferedCompressionMethods(serverHandshakeState.offeredCompressionMethods);
        if (TlsProtocol.arrayContains(serverHandshakeState.offeredCipherSuites, 255)) {
            serverHandshakeState.secure_renegotiation = true;
        }
        if (serverHandshakeState.clientExtensions != null) {
            final byte[] array = serverHandshakeState.clientExtensions.get(TlsProtocol.EXT_RenegotiationInfo);
            if (array != null) {
                serverHandshakeState.secure_renegotiation = true;
                if (!Arrays.constantTimeAreEqual(array, TlsProtocol.createRenegotiationInfo(TlsUtils.EMPTY_BYTES))) {
                    throw new TlsFatalAlert((short)40);
                }
            }
        }
        serverHandshakeState.server.notifySecureRenegotiation(serverHandshakeState.secure_renegotiation);
        if (serverHandshakeState.clientExtensions != null) {
            serverHandshakeState.server.processClientExtensions(serverHandshakeState.clientExtensions);
        }
    }
    
    protected void processClientKeyExchange(final ServerHandshakeState serverHandshakeState, final byte[] buf) throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf);
        serverHandshakeState.keyExchange.processClientKeyExchange(byteArrayInputStream);
        TlsProtocol.assertEmpty(byteArrayInputStream);
        TlsProtocol.establishMasterSecret(serverHandshakeState.serverContext, serverHandshakeState.keyExchange);
    }
    
    protected void processClientSupplementalData(final ServerHandshakeState serverHandshakeState, final byte[] buf) throws IOException {
        serverHandshakeState.server.processClientSupplementalData(TlsProtocol.readSupplementalDataMessage(new ByteArrayInputStream(buf)));
    }
    
    protected boolean expectCertificateVerifyMessage(final ServerHandshakeState serverHandshakeState) {
        return serverHandshakeState.clientCertificateType >= 0 && TlsUtils.hasSigningCapability(serverHandshakeState.clientCertificateType);
    }
    
    protected static class ServerHandshakeState
    {
        TlsServer server;
        TlsServerContextImpl serverContext;
        int[] offeredCipherSuites;
        short[] offeredCompressionMethods;
        Hashtable clientExtensions;
        int selectedCipherSuite;
        short selectedCompressionMethod;
        boolean secure_renegotiation;
        boolean expectSessionTicket;
        Hashtable serverExtensions;
        TlsKeyExchange keyExchange;
        TlsCredentials serverCredentials;
        CertificateRequest certificateRequest;
        short clientCertificateType;
        Certificate clientCertificate;
        
        protected ServerHandshakeState() {
            this.server = null;
            this.serverContext = null;
            this.selectedCipherSuite = -1;
            this.selectedCompressionMethod = -1;
            this.secure_renegotiation = false;
            this.expectSessionTicket = false;
            this.serverExtensions = null;
            this.keyExchange = null;
            this.serverCredentials = null;
            this.certificateRequest = null;
            this.clientCertificateType = -1;
            this.clientCertificate = null;
        }
    }
}
