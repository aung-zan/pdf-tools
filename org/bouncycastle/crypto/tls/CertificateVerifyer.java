// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.asn1.x509.Certificate;

public interface CertificateVerifyer
{
    boolean isValid(final Certificate[] p0);
}
