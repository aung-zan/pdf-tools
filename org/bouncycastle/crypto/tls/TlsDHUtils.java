// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.DHKeyGenerationParameters;
import org.bouncycastle.crypto.generators.DHBasicKeyPairGenerator;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.DHParameters;
import java.security.SecureRandom;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.agreement.DHBasicAgreement;
import org.bouncycastle.crypto.params.DHPrivateKeyParameters;
import org.bouncycastle.crypto.params.DHPublicKeyParameters;
import java.math.BigInteger;

public class TlsDHUtils
{
    static final BigInteger ONE;
    static final BigInteger TWO;
    
    public static byte[] calculateDHBasicAgreement(final DHPublicKeyParameters dhPublicKeyParameters, final DHPrivateKeyParameters dhPrivateKeyParameters) {
        final DHBasicAgreement dhBasicAgreement = new DHBasicAgreement();
        dhBasicAgreement.init(dhPrivateKeyParameters);
        return BigIntegers.asUnsignedByteArray(dhBasicAgreement.calculateAgreement(dhPublicKeyParameters));
    }
    
    public static AsymmetricCipherKeyPair generateDHKeyPair(final SecureRandom secureRandom, final DHParameters dhParameters) {
        final DHBasicKeyPairGenerator dhBasicKeyPairGenerator = new DHBasicKeyPairGenerator();
        dhBasicKeyPairGenerator.init(new DHKeyGenerationParameters(secureRandom, dhParameters));
        return dhBasicKeyPairGenerator.generateKeyPair();
    }
    
    public static DHPrivateKeyParameters generateEphemeralClientKeyExchange(final SecureRandom secureRandom, final DHParameters dhParameters, final OutputStream outputStream) throws IOException {
        final AsymmetricCipherKeyPair generateDHKeyPair = generateDHKeyPair(secureRandom, dhParameters);
        final DHPrivateKeyParameters dhPrivateKeyParameters = (DHPrivateKeyParameters)generateDHKeyPair.getPrivate();
        TlsUtils.writeOpaque16(BigIntegers.asUnsignedByteArray(((DHPublicKeyParameters)generateDHKeyPair.getPublic()).getY()), outputStream);
        return dhPrivateKeyParameters;
    }
    
    public static DHPublicKeyParameters validateDHPublicKey(final DHPublicKeyParameters dhPublicKeyParameters) throws IOException {
        final BigInteger y = dhPublicKeyParameters.getY();
        final DHParameters parameters = dhPublicKeyParameters.getParameters();
        final BigInteger p = parameters.getP();
        final BigInteger g = parameters.getG();
        if (!p.isProbablePrime(2)) {
            throw new TlsFatalAlert((short)47);
        }
        if (g.compareTo(TlsDHUtils.TWO) < 0 || g.compareTo(p.subtract(TlsDHUtils.TWO)) > 0) {
            throw new TlsFatalAlert((short)47);
        }
        if (y.compareTo(TlsDHUtils.TWO) < 0 || y.compareTo(p.subtract(TlsDHUtils.ONE)) > 0) {
            throw new TlsFatalAlert((short)47);
        }
        return dhPublicKeyParameters;
    }
    
    public static BigInteger readDHParameter(final InputStream inputStream) throws IOException {
        return new BigInteger(1, TlsUtils.readOpaque16(inputStream));
    }
    
    public static void writeDHParameter(final BigInteger bigInteger, final OutputStream outputStream) throws IOException {
        TlsUtils.writeOpaque16(BigIntegers.asUnsignedByteArray(bigInteger), outputStream);
    }
    
    static {
        ONE = BigInteger.valueOf(1L);
        TWO = BigInteger.valueOf(2L);
    }
}
