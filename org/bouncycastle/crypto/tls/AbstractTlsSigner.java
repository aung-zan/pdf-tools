// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

public abstract class AbstractTlsSigner implements TlsSigner
{
    protected TlsContext context;
    
    public void init(final TlsContext context) {
        this.context = context;
    }
}
