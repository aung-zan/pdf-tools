// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto;

public class OutputLengthException extends DataLengthException
{
    public OutputLengthException(final String s) {
        super(s);
    }
}
