// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.math.ec.ECCurve;
import java.util.Random;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.ECAlgorithms;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.ECKeyParameters;
import java.math.BigInteger;
import org.bouncycastle.crypto.DSA;

public class DSTU4145Signer implements DSA
{
    private static final BigInteger ONE;
    private ECKeyParameters key;
    private SecureRandom random;
    
    public void init(final boolean b, CipherParameters parameters) {
        if (b) {
            if (parameters instanceof ParametersWithRandom) {
                final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)parameters;
                this.random = parametersWithRandom.getRandom();
                parameters = parametersWithRandom.getParameters();
            }
            else {
                this.random = new SecureRandom();
            }
            this.key = (ECPrivateKeyParameters)parameters;
        }
        else {
            this.key = (ECPublicKeyParameters)parameters;
        }
    }
    
    public BigInteger[] generateSignature(final byte[] array) {
        ECFieldElement ecFieldElement = hash2FieldElement(this.key.getParameters().getCurve(), array);
        if (ecFieldElement.toBigInteger().signum() == 0) {
            ecFieldElement = this.key.getParameters().getCurve().fromBigInteger(DSTU4145Signer.ONE);
        }
        BigInteger fieldElement2Integer;
        BigInteger mod;
        while (true) {
            final BigInteger generateRandomInteger = generateRandomInteger(this.key.getParameters().getN(), this.random);
            final ECFieldElement x = this.key.getParameters().getG().multiply(generateRandomInteger).getX();
            if (x.toBigInteger().signum() != 0) {
                fieldElement2Integer = fieldElement2Integer(this.key.getParameters().getN(), ecFieldElement.multiply(x));
                if (fieldElement2Integer.signum() == 0) {
                    continue;
                }
                mod = fieldElement2Integer.multiply(((ECPrivateKeyParameters)this.key).getD()).add(generateRandomInteger).mod(this.key.getParameters().getN());
                if (mod.signum() != 0) {
                    break;
                }
                continue;
            }
        }
        return new BigInteger[] { fieldElement2Integer, mod };
    }
    
    public boolean verifySignature(final byte[] array, final BigInteger val, final BigInteger bigInteger) {
        if (val.signum() == 0 || bigInteger.signum() == 0) {
            return false;
        }
        if (val.compareTo(this.key.getParameters().getN()) >= 0 || bigInteger.compareTo(this.key.getParameters().getN()) >= 0) {
            return false;
        }
        ECFieldElement ecFieldElement = hash2FieldElement(this.key.getParameters().getCurve(), array);
        if (ecFieldElement.toBigInteger().signum() == 0) {
            ecFieldElement = this.key.getParameters().getCurve().fromBigInteger(DSTU4145Signer.ONE);
        }
        final ECPoint sumOfTwoMultiplies = ECAlgorithms.sumOfTwoMultiplies(this.key.getParameters().getG(), bigInteger, ((ECPublicKeyParameters)this.key).getQ(), val);
        return !sumOfTwoMultiplies.isInfinity() && fieldElement2Integer(this.key.getParameters().getN(), ecFieldElement.multiply(sumOfTwoMultiplies.getX())).compareTo(val) == 0;
    }
    
    private static BigInteger generateRandomInteger(final BigInteger bigInteger, final SecureRandom rnd) {
        return new BigInteger(bigInteger.bitLength() - 1, rnd);
    }
    
    private static void reverseBytes(final byte[] array) {
        for (int i = 0; i < array.length / 2; ++i) {
            final byte b = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = b;
        }
    }
    
    private static ECFieldElement hash2FieldElement(final ECCurve ecCurve, final byte[] array) {
        final byte[] clone = Arrays.clone(array);
        reverseBytes(clone);
        BigInteger clearBit;
        for (clearBit = new BigInteger(1, clone); clearBit.bitLength() >= ecCurve.getFieldSize(); clearBit = clearBit.clearBit(clearBit.bitLength() - 1)) {}
        return ecCurve.fromBigInteger(clearBit);
    }
    
    private static BigInteger fieldElement2Integer(final BigInteger bigInteger, final ECFieldElement ecFieldElement) {
        BigInteger bigInteger2;
        for (bigInteger2 = ecFieldElement.toBigInteger(); bigInteger2.bitLength() >= bigInteger.bitLength(); bigInteger2 = bigInteger2.clearBit(bigInteger2.bitLength() - 1)) {}
        return bigInteger2;
    }
    
    static {
        ONE = BigInteger.valueOf(1L);
    }
}
