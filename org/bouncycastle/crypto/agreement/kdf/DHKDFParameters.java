// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.agreement.kdf;

import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.crypto.DerivationParameters;

public class DHKDFParameters implements DerivationParameters
{
    private ASN1ObjectIdentifier algorithm;
    private int keySize;
    private byte[] z;
    private byte[] extraInfo;
    
    public DHKDFParameters(final DERObjectIdentifier derObjectIdentifier, final int n, final byte[] array) {
        this(derObjectIdentifier, n, array, null);
    }
    
    public DHKDFParameters(final DERObjectIdentifier derObjectIdentifier, final int keySize, final byte[] z, final byte[] extraInfo) {
        this.algorithm = new ASN1ObjectIdentifier(derObjectIdentifier.getId());
        this.keySize = keySize;
        this.z = z;
        this.extraInfo = extraInfo;
    }
    
    public ASN1ObjectIdentifier getAlgorithm() {
        return this.algorithm;
    }
    
    public int getKeySize() {
        return this.keySize;
    }
    
    public byte[] getZ() {
        return this.z;
    }
    
    public byte[] getExtraInfo() {
        return this.extraInfo;
    }
}
