// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.openssl;

import java.io.InputStream;
import java.security.cert.CertificateFactory;
import java.io.ByteArrayInputStream;
import org.bouncycastle.x509.X509V2AttributeCertificate;
import org.bouncycastle.asn1.pkcs.RSAPublicKey;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;
import org.bouncycastle.asn1.pkcs.RSAPrivateKey;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.jce.PKCS10CertificationRequest;
import org.bouncycastle.asn1.pkcs.EncryptionScheme;
import org.bouncycastle.asn1.pkcs.KeyDerivationFunc;
import org.bouncycastle.asn1.pkcs.PBEParameter;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import org.bouncycastle.asn1.pkcs.PKCS12PBEParams;
import java.security.AlgorithmParameters;
import org.bouncycastle.asn1.pkcs.PBKDF2Params;
import org.bouncycastle.asn1.pkcs.PBES2Parameters;
import org.bouncycastle.asn1.pkcs.EncryptedPrivateKeyInfo;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.asn1.DERObjectIdentifier;
import java.security.spec.X509EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.asn1.sec.ECPrivateKey;
import java.util.Iterator;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.util.encoders.Hex;
import java.util.StringTokenizer;
import org.bouncycastle.util.io.pem.PemHeader;
import org.bouncycastle.asn1.ASN1Sequence;
import java.security.KeyPair;
import java.security.spec.KeySpec;
import java.security.KeyFactory;
import java.security.spec.DSAPublicKeySpec;
import java.security.spec.DSAPrivateKeySpec;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.generators.OpenSSLPBEParametersGenerator;
import javax.crypto.SecretKey;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import java.security.Provider;
import java.security.Security;
import org.bouncycastle.util.io.pem.PemObject;
import java.io.IOException;
import org.bouncycastle.util.io.pem.PemObjectParser;
import java.util.HashMap;
import java.io.Reader;
import java.util.Map;
import org.bouncycastle.util.io.pem.PemReader;

public class PEMReader extends PemReader
{
    private final Map parsers;
    private PasswordFinder pFinder;
    
    @Deprecated
    public PEMReader(final Reader reader) {
        this(reader, null, "BC");
    }
    
    @Deprecated
    public PEMReader(final Reader reader, final PasswordFinder passwordFinder) {
        this(reader, passwordFinder, "BC");
    }
    
    @Deprecated
    public PEMReader(final Reader reader, final PasswordFinder passwordFinder, final String s) {
        this(reader, passwordFinder, s, s);
    }
    
    @Deprecated
    public PEMReader(final Reader reader, final PasswordFinder pFinder, final String s, final String s2) {
        super(reader);
        this.parsers = new HashMap();
        this.pFinder = pFinder;
        this.parsers.put("CERTIFICATE REQUEST", new PKCS10CertificationRequestParser());
        this.parsers.put("NEW CERTIFICATE REQUEST", new PKCS10CertificationRequestParser());
        this.parsers.put("CERTIFICATE", new X509CertificateParser(s2));
        this.parsers.put("X509 CERTIFICATE", new X509CertificateParser(s2));
        this.parsers.put("X509 CRL", new X509CRLParser(s2));
        this.parsers.put("PKCS7", new PKCS7Parser());
        this.parsers.put("ATTRIBUTE CERTIFICATE", new X509AttributeCertificateParser());
        this.parsers.put("EC PARAMETERS", new ECNamedCurveSpecParser());
        this.parsers.put("PUBLIC KEY", new PublicKeyParser(s2));
        this.parsers.put("RSA PUBLIC KEY", new RSAPublicKeyParser(s2));
        this.parsers.put("RSA PRIVATE KEY", new RSAKeyPairParser(s, s2));
        this.parsers.put("DSA PRIVATE KEY", new DSAKeyPairParser(s, s2));
        this.parsers.put("EC PRIVATE KEY", new ECDSAKeyPairParser(s, s2));
        this.parsers.put("ENCRYPTED PRIVATE KEY", new EncryptedPrivateKeyParser(s, s2));
        this.parsers.put("PRIVATE KEY", new PrivateKeyParser(s2));
    }
    
    public Object readObject() throws IOException {
        final PemObject pemObject = this.readPemObject();
        if (pemObject == null) {
            return null;
        }
        final String type = pemObject.getType();
        if (this.parsers.containsKey(type)) {
            return ((PemObjectParser)this.parsers.get(type)).parseObject(pemObject);
        }
        throw new IOException("unrecognised object: " + type);
    }
    
    static byte[] crypt(final boolean b, final String s, final byte[] array, final char[] array2, final String s2, final byte[] array3) throws IOException {
        Provider provider = null;
        if (s != null) {
            provider = Security.getProvider(s);
            if (provider == null) {
                throw new EncryptionException("cannot find provider: " + s);
            }
        }
        return crypt(b, provider, array, array2, s2, array3);
    }
    
    static byte[] crypt(final boolean b, final Provider provider, final byte[] input, final char[] array, final String s, final byte[] array2) throws IOException {
        AlgorithmParameterSpec params = new IvParameterSpec(array2);
        String str = "CBC";
        String str2 = "PKCS5Padding";
        if (s.endsWith("-CFB")) {
            str = "CFB";
            str2 = "NoPadding";
        }
        if (s.endsWith("-ECB") || "DES-EDE".equals(s) || "DES-EDE3".equals(s)) {
            str = "ECB";
            params = null;
        }
        if (s.endsWith("-OFB")) {
            str = "OFB";
            str2 = "NoPadding";
        }
        String str3;
        SecretKey secretKey;
        if (s.startsWith("DES-EDE")) {
            str3 = "DESede";
            secretKey = getKey(array, str3, 24, array2, !s.startsWith("DES-EDE3"));
        }
        else if (s.startsWith("DES-")) {
            str3 = "DES";
            secretKey = getKey(array, str3, 8, array2);
        }
        else if (s.startsWith("BF-")) {
            str3 = "Blowfish";
            secretKey = getKey(array, str3, 16, array2);
        }
        else if (s.startsWith("RC2-")) {
            str3 = "RC2";
            int n = 128;
            if (s.startsWith("RC2-40-")) {
                n = 40;
            }
            else if (s.startsWith("RC2-64-")) {
                n = 64;
            }
            secretKey = getKey(array, str3, n / 8, array2);
            if (params == null) {
                params = new RC2ParameterSpec(n);
            }
            else {
                params = new RC2ParameterSpec(n, array2);
            }
        }
        else {
            if (!s.startsWith("AES-")) {
                throw new EncryptionException("unknown encryption with private key");
            }
            str3 = "AES";
            byte[] array3 = array2;
            if (array3.length > 8) {
                array3 = new byte[8];
                System.arraycopy(array2, 0, array3, 0, 8);
            }
            int n2;
            if (s.startsWith("AES-128-")) {
                n2 = 128;
            }
            else if (s.startsWith("AES-192-")) {
                n2 = 192;
            }
            else {
                if (!s.startsWith("AES-256-")) {
                    throw new EncryptionException("unknown AES encryption with private key");
                }
                n2 = 256;
            }
            secretKey = getKey(array, "AES", n2 / 8, array3);
        }
        final String string = str3 + "/" + str + "/" + str2;
        try {
            final Cipher instance = Cipher.getInstance(string, provider);
            final int n3 = b ? 1 : 2;
            if (params == null) {
                instance.init(n3, secretKey);
            }
            else {
                instance.init(n3, secretKey, params);
            }
            return instance.doFinal(input);
        }
        catch (Exception ex) {
            throw new EncryptionException("exception using cipher - please check password and data.", (Throwable)ex);
        }
    }
    
    private static SecretKey getKey(final char[] array, final String s, final int n, final byte[] array2) {
        return getKey(array, s, n, array2, false);
    }
    
    private static SecretKey getKey(final char[] array, final String algorithm, final int n, final byte[] array2, final boolean b) {
        final OpenSSLPBEParametersGenerator openSSLPBEParametersGenerator = new OpenSSLPBEParametersGenerator();
        openSSLPBEParametersGenerator.init(PBEParametersGenerator.PKCS5PasswordToBytes(array), array2);
        final byte[] key = ((KeyParameter)openSSLPBEParametersGenerator.generateDerivedParameters(n * 8)).getKey();
        if (b && key.length >= 24) {
            System.arraycopy(key, 0, key, 16, 8);
        }
        return new SecretKeySpec(key, algorithm);
    }
    
    public static SecretKey generateSecretKeyForPKCS5Scheme2(final String algorithm, final char[] array, final byte[] array2, final int n) {
        final PKCS5S2ParametersGenerator pkcs5S2ParametersGenerator = new PKCS5S2ParametersGenerator();
        pkcs5S2ParametersGenerator.init(PBEParametersGenerator.PKCS5PasswordToBytes(array), array2, n);
        return new SecretKeySpec(((KeyParameter)pkcs5S2ParametersGenerator.generateDerivedParameters(PEMUtilities.getKeySize(algorithm))).getKey(), algorithm);
    }
    
    private class DSAKeyPairParser extends KeyPairParser
    {
        private String asymProvider;
        
        public DSAKeyPairParser(final String s, final String asymProvider) {
            super(s);
            this.asymProvider = asymProvider;
        }
        
        public Object parseObject(final PemObject pemObject) throws IOException {
            try {
                final ASN1Sequence keyPair = this.readKeyPair(pemObject);
                if (keyPair.size() != 6) {
                    throw new PEMException("malformed sequence in DSA private key");
                }
                final DERInteger derInteger = (DERInteger)keyPair.getObjectAt(1);
                final DERInteger derInteger2 = (DERInteger)keyPair.getObjectAt(2);
                final DERInteger derInteger3 = (DERInteger)keyPair.getObjectAt(3);
                final DERInteger derInteger4 = (DERInteger)keyPair.getObjectAt(4);
                final DSAPrivateKeySpec keySpec = new DSAPrivateKeySpec(((DERInteger)keyPair.getObjectAt(5)).getValue(), derInteger.getValue(), derInteger2.getValue(), derInteger3.getValue());
                final DSAPublicKeySpec keySpec2 = new DSAPublicKeySpec(derInteger4.getValue(), derInteger.getValue(), derInteger2.getValue(), derInteger3.getValue());
                final KeyFactory instance = KeyFactory.getInstance("DSA", this.asymProvider);
                return new KeyPair(instance.generatePublic(keySpec2), instance.generatePrivate(keySpec));
            }
            catch (IOException ex) {
                throw ex;
            }
            catch (Exception ex2) {
                throw new PEMException("problem creating DSA private key: " + ex2.toString(), ex2);
            }
        }
    }
    
    private abstract class KeyPairParser implements PemObjectParser
    {
        protected String symProvider;
        
        public KeyPairParser(final String symProvider) {
            this.symProvider = symProvider;
        }
        
        protected ASN1Sequence readKeyPair(final PemObject pemObject) throws IOException {
            boolean b = false;
            String value = null;
            for (final PemHeader pemHeader : pemObject.getHeaders()) {
                if (pemHeader.getName().equals("Proc-Type") && pemHeader.getValue().equals("4,ENCRYPTED")) {
                    b = true;
                }
                else {
                    if (!pemHeader.getName().equals("DEK-Info")) {
                        continue;
                    }
                    value = pemHeader.getValue();
                }
            }
            byte[] array = pemObject.getContent();
            if (b) {
                if (PEMReader.this.pFinder == null) {
                    throw new PasswordException("No password finder specified, but a password is required");
                }
                final char[] password = PEMReader.this.pFinder.getPassword();
                if (password == null) {
                    throw new PasswordException("Password is null, but a password is required");
                }
                final StringTokenizer stringTokenizer = new StringTokenizer(value, ",");
                array = PEMReader.crypt(false, this.symProvider, array, password, stringTokenizer.nextToken(), Hex.decode(stringTokenizer.nextToken()));
            }
            try {
                return ASN1Sequence.getInstance(ASN1Primitive.fromByteArray(array));
            }
            catch (IOException ex) {
                if (b) {
                    throw new PEMException("exception decoding - please check password and data.", ex);
                }
                throw new PEMException(ex.getMessage(), ex);
            }
            catch (IllegalArgumentException ex2) {
                if (b) {
                    throw new PEMException("exception decoding - please check password and data.", ex2);
                }
                throw new PEMException(ex2.getMessage(), ex2);
            }
        }
    }
    
    private class ECDSAKeyPairParser extends KeyPairParser
    {
        private String asymProvider;
        
        public ECDSAKeyPairParser(final String s, final String asymProvider) {
            super(s);
            this.asymProvider = asymProvider;
        }
        
        public Object parseObject(final PemObject pemObject) throws IOException {
            try {
                final ECPrivateKey instance = ECPrivateKey.getInstance(this.readKeyPair(pemObject));
                final AlgorithmIdentifier algorithmIdentifier = new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, instance.getParameters());
                final PrivateKeyInfo privateKeyInfo = new PrivateKeyInfo(algorithmIdentifier, instance);
                final SubjectPublicKeyInfo subjectPublicKeyInfo = new SubjectPublicKeyInfo(algorithmIdentifier, instance.getPublicKey().getBytes());
                final PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyInfo.getEncoded());
                final X509EncodedKeySpec keySpec2 = new X509EncodedKeySpec(subjectPublicKeyInfo.getEncoded());
                final KeyFactory instance2 = KeyFactory.getInstance("ECDSA", this.asymProvider);
                return new KeyPair(instance2.generatePublic(keySpec2), instance2.generatePrivate(keySpec));
            }
            catch (IOException ex) {
                throw ex;
            }
            catch (Exception ex2) {
                throw new PEMException("problem creating EC private key: " + ex2.toString(), ex2);
            }
        }
    }
    
    private class ECNamedCurveSpecParser implements PemObjectParser
    {
        public Object parseObject(final PemObject pemObject) throws IOException {
            try {
                final ECNamedCurveParameterSpec parameterSpec = ECNamedCurveTable.getParameterSpec(((DERObjectIdentifier)ASN1Primitive.fromByteArray(pemObject.getContent())).getId());
                if (parameterSpec == null) {
                    throw new IOException("object ID not found in EC curve table");
                }
                return parameterSpec;
            }
            catch (IOException ex) {
                throw ex;
            }
            catch (Exception ex2) {
                throw new PEMException("exception extracting EC named curve: " + ex2.toString());
            }
        }
    }
    
    private class EncryptedPrivateKeyParser implements PemObjectParser
    {
        private String symProvider;
        private String asymProvider;
        
        public EncryptedPrivateKeyParser(final String symProvider, final String asymProvider) {
            this.symProvider = symProvider;
            this.asymProvider = asymProvider;
        }
        
        public Object parseObject(final PemObject pemObject) throws IOException {
            try {
                final EncryptedPrivateKeyInfo instance = EncryptedPrivateKeyInfo.getInstance(ASN1Primitive.fromByteArray(pemObject.getContent()));
                final AlgorithmIdentifier encryptionAlgorithm = instance.getEncryptionAlgorithm();
                if (PEMReader.this.pFinder == null) {
                    throw new PEMException("no PasswordFinder specified");
                }
                if (PEMUtilities.isPKCS5Scheme2(encryptionAlgorithm.getAlgorithm())) {
                    final PBES2Parameters instance2 = PBES2Parameters.getInstance(encryptionAlgorithm.getParameters());
                    final KeyDerivationFunc keyDerivationFunc = instance2.getKeyDerivationFunc();
                    final EncryptionScheme encryptionScheme = instance2.getEncryptionScheme();
                    final PBKDF2Params pbkdf2Params = (PBKDF2Params)keyDerivationFunc.getParameters();
                    final int intValue = pbkdf2Params.getIterationCount().intValue();
                    final byte[] salt = pbkdf2Params.getSalt();
                    final String id = encryptionScheme.getAlgorithm().getId();
                    final SecretKey generateSecretKeyForPKCS5Scheme2 = PEMReader.generateSecretKeyForPKCS5Scheme2(id, PEMReader.this.pFinder.getPassword(), salt, intValue);
                    final Cipher instance3 = Cipher.getInstance(id, this.symProvider);
                    final AlgorithmParameters instance4 = AlgorithmParameters.getInstance(id, this.symProvider);
                    instance4.init(encryptionScheme.getParameters().toASN1Primitive().getEncoded());
                    instance3.init(2, generateSecretKeyForPKCS5Scheme2, instance4);
                    final PrivateKeyInfo instance5 = PrivateKeyInfo.getInstance(ASN1Primitive.fromByteArray(instance3.doFinal(instance.getEncryptedData())));
                    return KeyFactory.getInstance(instance5.getPrivateKeyAlgorithm().getAlgorithm().getId(), this.asymProvider).generatePrivate(new PKCS8EncodedKeySpec(instance5.getEncoded()));
                }
                if (PEMUtilities.isPKCS12(encryptionAlgorithm.getAlgorithm())) {
                    final PKCS12PBEParams instance6 = PKCS12PBEParams.getInstance(encryptionAlgorithm.getParameters());
                    final String id2 = encryptionAlgorithm.getAlgorithm().getId();
                    final PBEKeySpec keySpec = new PBEKeySpec(PEMReader.this.pFinder.getPassword());
                    final SecretKeyFactory instance7 = SecretKeyFactory.getInstance(id2, this.symProvider);
                    final PBEParameterSpec params = new PBEParameterSpec(instance6.getIV(), instance6.getIterations().intValue());
                    final Cipher instance8 = Cipher.getInstance(id2, this.symProvider);
                    instance8.init(2, instance7.generateSecret(keySpec), params);
                    final PrivateKeyInfo instance9 = PrivateKeyInfo.getInstance(ASN1Primitive.fromByteArray(instance8.doFinal(instance.getEncryptedData())));
                    return KeyFactory.getInstance(instance9.getPrivateKeyAlgorithm().getAlgorithm().getId(), this.asymProvider).generatePrivate(new PKCS8EncodedKeySpec(instance9.getEncoded()));
                }
                if (PEMUtilities.isPKCS5Scheme1(encryptionAlgorithm.getAlgorithm())) {
                    final PBEParameter instance10 = PBEParameter.getInstance(encryptionAlgorithm.getParameters());
                    final String id3 = encryptionAlgorithm.getAlgorithm().getId();
                    final PBEKeySpec keySpec2 = new PBEKeySpec(PEMReader.this.pFinder.getPassword());
                    final SecretKeyFactory instance11 = SecretKeyFactory.getInstance(id3, this.symProvider);
                    final PBEParameterSpec params2 = new PBEParameterSpec(instance10.getSalt(), instance10.getIterationCount().intValue());
                    final Cipher instance12 = Cipher.getInstance(id3, this.symProvider);
                    instance12.init(2, instance11.generateSecret(keySpec2), params2);
                    final PrivateKeyInfo instance13 = PrivateKeyInfo.getInstance(ASN1Primitive.fromByteArray(instance12.doFinal(instance.getEncryptedData())));
                    return KeyFactory.getInstance(instance13.getPrivateKeyAlgorithm().getAlgorithm().getId(), this.asymProvider).generatePrivate(new PKCS8EncodedKeySpec(instance13.getEncoded()));
                }
                throw new PEMException("Unknown algorithm: " + encryptionAlgorithm.getAlgorithm());
            }
            catch (IOException ex) {
                throw ex;
            }
            catch (Exception ex2) {
                throw new PEMException("problem parsing ENCRYPTED PRIVATE KEY: " + ex2.toString(), ex2);
            }
        }
    }
    
    private class PKCS10CertificationRequestParser implements PemObjectParser
    {
        public Object parseObject(final PemObject pemObject) throws IOException {
            try {
                return new PKCS10CertificationRequest(pemObject.getContent());
            }
            catch (Exception ex) {
                throw new PEMException("problem parsing certrequest: " + ex.toString(), ex);
            }
        }
    }
    
    private class PKCS7Parser implements PemObjectParser
    {
        public Object parseObject(final PemObject pemObject) throws IOException {
            try {
                return ContentInfo.getInstance(new ASN1InputStream(pemObject.getContent()).readObject());
            }
            catch (Exception ex) {
                throw new PEMException("problem parsing PKCS7 object: " + ex.toString(), ex);
            }
        }
    }
    
    private class PrivateKeyParser implements PemObjectParser
    {
        private String provider;
        
        public PrivateKeyParser(final String provider) {
            this.provider = provider;
        }
        
        public Object parseObject(final PemObject pemObject) throws IOException {
            try {
                return KeyFactory.getInstance(PrivateKeyInfo.getInstance(ASN1Primitive.fromByteArray(pemObject.getContent())).getPrivateKeyAlgorithm().getAlgorithm().getId(), this.provider).generatePrivate(new PKCS8EncodedKeySpec(pemObject.getContent()));
            }
            catch (Exception ex) {
                throw new PEMException("problem parsing PRIVATE KEY: " + ex.toString(), ex);
            }
        }
    }
    
    private class PublicKeyParser implements PemObjectParser
    {
        private String provider;
        
        public PublicKeyParser(final String provider) {
            this.provider = provider;
        }
        
        public Object parseObject(final PemObject pemObject) throws IOException {
            final X509EncodedKeySpec keySpec = new X509EncodedKeySpec(pemObject.getContent());
            final String[] array = { "DSA", "RSA" };
            for (int i = 0; i < array.length; ++i) {
                try {
                    return KeyFactory.getInstance(array[i], this.provider).generatePublic(keySpec);
                }
                catch (NoSuchAlgorithmException ex) {}
                catch (InvalidKeySpecException ex2) {}
                catch (NoSuchProviderException ex3) {
                    throw new RuntimeException("can't find provider " + this.provider);
                }
            }
            return null;
        }
    }
    
    private class RSAKeyPairParser extends KeyPairParser
    {
        private String asymProvider;
        
        public RSAKeyPairParser(final String s, final String asymProvider) {
            super(s);
            this.asymProvider = asymProvider;
        }
        
        public Object parseObject(final PemObject pemObject) throws IOException {
            try {
                final ASN1Sequence keyPair = this.readKeyPair(pemObject);
                if (keyPair.size() != 9) {
                    throw new PEMException("malformed sequence in RSA private key");
                }
                final RSAPrivateKey instance = RSAPrivateKey.getInstance(keyPair);
                final RSAPublicKeySpec keySpec = new RSAPublicKeySpec(instance.getModulus(), instance.getPublicExponent());
                final RSAPrivateCrtKeySpec keySpec2 = new RSAPrivateCrtKeySpec(instance.getModulus(), instance.getPublicExponent(), instance.getPrivateExponent(), instance.getPrime1(), instance.getPrime2(), instance.getExponent1(), instance.getExponent2(), instance.getCoefficient());
                final KeyFactory instance2 = KeyFactory.getInstance("RSA", this.asymProvider);
                return new KeyPair(instance2.generatePublic(keySpec), instance2.generatePrivate(keySpec2));
            }
            catch (IOException ex) {
                throw ex;
            }
            catch (Exception ex2) {
                throw new PEMException("problem creating RSA private key: " + ex2.toString(), ex2);
            }
        }
    }
    
    private class RSAPublicKeyParser implements PemObjectParser
    {
        private String provider;
        
        public RSAPublicKeyParser(final String provider) {
            this.provider = provider;
        }
        
        public Object parseObject(final PemObject pemObject) throws IOException {
            try {
                final RSAPublicKey instance = RSAPublicKey.getInstance(new ASN1InputStream(pemObject.getContent()).readObject());
                return KeyFactory.getInstance("RSA", this.provider).generatePublic(new RSAPublicKeySpec(instance.getModulus(), instance.getPublicExponent()));
            }
            catch (IOException ex) {
                throw ex;
            }
            catch (NoSuchProviderException ex3) {
                throw new IOException("can't find provider " + this.provider);
            }
            catch (Exception ex2) {
                throw new PEMException("problem extracting key: " + ex2.toString(), ex2);
            }
        }
    }
    
    private class X509AttributeCertificateParser implements PemObjectParser
    {
        public Object parseObject(final PemObject pemObject) throws IOException {
            return new X509V2AttributeCertificate(pemObject.getContent());
        }
    }
    
    private class X509CRLParser implements PemObjectParser
    {
        private String provider;
        
        public X509CRLParser(final String provider) {
            this.provider = provider;
        }
        
        public Object parseObject(final PemObject pemObject) throws IOException {
            final ByteArrayInputStream inStream = new ByteArrayInputStream(pemObject.getContent());
            try {
                return CertificateFactory.getInstance("X.509", this.provider).generateCRL(inStream);
            }
            catch (Exception ex) {
                throw new PEMException("problem parsing cert: " + ex.toString(), ex);
            }
        }
    }
    
    private class X509CertificateParser implements PemObjectParser
    {
        private String provider;
        
        public X509CertificateParser(final String provider) {
            this.provider = provider;
        }
        
        public Object parseObject(final PemObject pemObject) throws IOException {
            final ByteArrayInputStream inStream = new ByteArrayInputStream(pemObject.getContent());
            try {
                return CertificateFactory.getInstance("X.509", this.provider).generateCertificate(inStream);
            }
            catch (Exception ex) {
                throw new PEMException("problem parsing cert: " + ex.toString(), ex);
            }
        }
    }
}
