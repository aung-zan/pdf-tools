// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.openssl;

import org.bouncycastle.openssl.jcajce.JcePEMEncryptorBuilder;
import java.security.SecureRandom;
import org.bouncycastle.util.io.pem.PemGenerationException;
import org.bouncycastle.util.io.pem.PemObjectGenerator;
import org.bouncycastle.openssl.jcajce.JcaMiscPEMGenerator;
import java.io.IOException;
import java.io.Writer;
import org.bouncycastle.util.io.pem.PemWriter;

public class PEMWriter extends PemWriter
{
    private String provider;
    
    public PEMWriter(final Writer writer) {
        this(writer, "BC");
    }
    
    @Deprecated
    public PEMWriter(final Writer writer, final String provider) {
        super(writer);
        this.provider = provider;
    }
    
    public void writeObject(final Object o) throws IOException {
        this.writeObject(o, null);
    }
    
    public void writeObject(final Object o, final PEMEncryptor pemEncryptor) throws IOException {
        try {
            super.writeObject(new JcaMiscPEMGenerator(o, pemEncryptor));
        }
        catch (PemGenerationException ex) {
            if (ex.getCause() instanceof IOException) {
                throw (IOException)ex.getCause();
            }
            throw ex;
        }
    }
    
    @Override
    public void writeObject(final PemObjectGenerator pemObjectGenerator) throws IOException {
        super.writeObject(pemObjectGenerator);
    }
    
    @Deprecated
    public void writeObject(final Object o, final String s, final char[] array, final SecureRandom secureRandom) throws IOException {
        this.writeObject(o, new JcePEMEncryptorBuilder(s).setSecureRandom(secureRandom).setProvider(this.provider).build(array));
    }
}
