// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.openssl;

public interface PEMDecryptor
{
    byte[] decrypt(final byte[] p0, final byte[] p1) throws PEMException;
}
