// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.openssl;

import java.io.IOException;

interface PEMKeyPairParser
{
    PEMKeyPair parse(final byte[] p0) throws IOException;
}
