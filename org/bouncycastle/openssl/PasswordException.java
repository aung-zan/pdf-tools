// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.openssl;

public class PasswordException extends PEMException
{
    public PasswordException(final String s) {
        super(s);
    }
}
