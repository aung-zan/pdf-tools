// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.openssl;

import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import java.io.IOException;
import org.bouncycastle.asn1.pkcs.EncryptedPrivateKeyInfo;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.util.io.pem.PemGenerationException;
import org.bouncycastle.util.io.pem.PemObject;
import java.security.SecureRandom;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.PrivateKey;
import org.bouncycastle.openssl.jcajce.JceOpenSSLPKCS8EncryptorBuilder;
import org.bouncycastle.operator.OutputEncryptor;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.util.io.pem.PemObjectGenerator;

public class PKCS8Generator implements PemObjectGenerator
{
    public static final ASN1ObjectIdentifier AES_128_CBC;
    public static final ASN1ObjectIdentifier AES_192_CBC;
    public static final ASN1ObjectIdentifier AES_256_CBC;
    public static final ASN1ObjectIdentifier DES3_CBC;
    public static final ASN1ObjectIdentifier PBE_SHA1_RC4_128;
    public static final ASN1ObjectIdentifier PBE_SHA1_RC4_40;
    public static final ASN1ObjectIdentifier PBE_SHA1_3DES;
    public static final ASN1ObjectIdentifier PBE_SHA1_2DES;
    public static final ASN1ObjectIdentifier PBE_SHA1_RC2_128;
    public static final ASN1ObjectIdentifier PBE_SHA1_RC2_40;
    private PrivateKeyInfo key;
    private OutputEncryptor outputEncryptor;
    private JceOpenSSLPKCS8EncryptorBuilder encryptorBuilder;
    
    @Deprecated
    public PKCS8Generator(final PrivateKey privateKey) {
        this.key = PrivateKeyInfo.getInstance(privateKey.getEncoded());
    }
    
    @Deprecated
    public PKCS8Generator(final PrivateKey privateKey, final ASN1ObjectIdentifier asn1ObjectIdentifier, final String s) throws NoSuchProviderException, NoSuchAlgorithmException {
        final Provider provider = Security.getProvider(s);
        if (provider == null) {
            throw new NoSuchProviderException("cannot find provider: " + s);
        }
        this.init(privateKey, asn1ObjectIdentifier, provider);
    }
    
    @Deprecated
    public PKCS8Generator(final PrivateKey privateKey, final ASN1ObjectIdentifier asn1ObjectIdentifier, final Provider provider) throws NoSuchAlgorithmException {
        this.init(privateKey, asn1ObjectIdentifier, provider);
    }
    
    public PKCS8Generator(final PrivateKeyInfo key, final OutputEncryptor outputEncryptor) {
        this.key = key;
        this.outputEncryptor = outputEncryptor;
    }
    
    private void init(final PrivateKey privateKey, final ASN1ObjectIdentifier asn1ObjectIdentifier, final Provider provider) throws NoSuchAlgorithmException {
        this.key = PrivateKeyInfo.getInstance(privateKey.getEncoded());
        (this.encryptorBuilder = new JceOpenSSLPKCS8EncryptorBuilder(asn1ObjectIdentifier)).setProvider(provider);
    }
    
    @Deprecated
    public PKCS8Generator setSecureRandom(final SecureRandom random) {
        this.encryptorBuilder.setRandom(random);
        return this;
    }
    
    @Deprecated
    public PKCS8Generator setPassword(final char[] passsword) {
        this.encryptorBuilder.setPasssword(passsword);
        return this;
    }
    
    @Deprecated
    public PKCS8Generator setIterationCount(final int iterationCount) {
        this.encryptorBuilder.setIterationCount(iterationCount);
        return this;
    }
    
    public PemObject generate() throws PemGenerationException {
        try {
            if (this.encryptorBuilder != null) {
                this.outputEncryptor = this.encryptorBuilder.build();
            }
        }
        catch (OperatorCreationException ex) {
            throw new PemGenerationException("unable to create operator: " + ex.getMessage(), ex);
        }
        if (this.outputEncryptor != null) {
            return this.generate(this.key, this.outputEncryptor);
        }
        return this.generate(this.key, null);
    }
    
    private PemObject generate(final PrivateKeyInfo privateKeyInfo, final OutputEncryptor outputEncryptor) throws PemGenerationException {
        try {
            final byte[] encoded = privateKeyInfo.getEncoded();
            if (outputEncryptor == null) {
                return new PemObject("PRIVATE KEY", encoded);
            }
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            final OutputStream outputStream = outputEncryptor.getOutputStream(byteArrayOutputStream);
            outputStream.write(privateKeyInfo.getEncoded());
            outputStream.close();
            return new PemObject("ENCRYPTED PRIVATE KEY", new EncryptedPrivateKeyInfo(outputEncryptor.getAlgorithmIdentifier(), byteArrayOutputStream.toByteArray()).getEncoded());
        }
        catch (IOException ex) {
            throw new PemGenerationException("unable to process encoded key data: " + ex.getMessage(), ex);
        }
    }
    
    static {
        AES_128_CBC = NISTObjectIdentifiers.id_aes128_CBC;
        AES_192_CBC = NISTObjectIdentifiers.id_aes192_CBC;
        AES_256_CBC = NISTObjectIdentifiers.id_aes256_CBC;
        DES3_CBC = PKCSObjectIdentifiers.des_EDE3_CBC;
        PBE_SHA1_RC4_128 = PKCSObjectIdentifiers.pbeWithSHAAnd128BitRC4;
        PBE_SHA1_RC4_40 = PKCSObjectIdentifiers.pbeWithSHAAnd40BitRC4;
        PBE_SHA1_3DES = PKCSObjectIdentifiers.pbeWithSHAAnd3_KeyTripleDES_CBC;
        PBE_SHA1_2DES = PKCSObjectIdentifiers.pbeWithSHAAnd2_KeyTripleDES_CBC;
        PBE_SHA1_RC2_128 = PKCSObjectIdentifiers.pbeWithSHAAnd128BitRC2_CBC;
        PBE_SHA1_RC2_40 = PKCSObjectIdentifiers.pbeWithSHAAnd40BitRC2_CBC;
    }
}
