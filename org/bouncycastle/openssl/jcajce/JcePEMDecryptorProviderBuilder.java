// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.openssl.jcajce;

import org.bouncycastle.openssl.PasswordException;
import org.bouncycastle.openssl.PEMDecryptor;
import org.bouncycastle.openssl.PEMException;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;
import org.bouncycastle.jcajce.JcaJceHelper;

public class JcePEMDecryptorProviderBuilder
{
    private JcaJceHelper helper;
    
    public JcePEMDecryptorProviderBuilder() {
        this.helper = new DefaultJcaJceHelper();
    }
    
    public JcePEMDecryptorProviderBuilder setProvider(final Provider provider) {
        this.helper = new ProviderJcaJceHelper(provider);
        return this;
    }
    
    public JcePEMDecryptorProviderBuilder setProvider(final String s) {
        this.helper = new NamedJcaJceHelper(s);
        return this;
    }
    
    public PEMDecryptorProvider build(final char[] array) {
        return new PEMDecryptorProvider() {
            public PEMDecryptor get(final String s) {
                return new PEMDecryptor() {
                    public byte[] decrypt(final byte[] array, final byte[] array2) throws PEMException {
                        if (array == null) {
                            throw new PasswordException("Password is null, but a password is required");
                        }
                        return PEMUtilities.crypt(false, JcePEMDecryptorProviderBuilder.this.helper, array, array, s, array2);
                    }
                };
            }
        };
    }
}
