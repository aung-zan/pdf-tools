// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.openssl.jcajce;

import java.security.PrivateKey;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.PublicKey;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.security.KeyFactory;
import org.bouncycastle.openssl.PEMException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import java.security.KeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;
import org.bouncycastle.jcajce.JcaJceHelper;

public class JcaPEMKeyConverter
{
    private JcaJceHelper helper;
    
    public JcaPEMKeyConverter() {
        this.helper = new DefaultJcaJceHelper();
    }
    
    public JcaPEMKeyConverter setProvider(final Provider provider) {
        this.helper = new ProviderJcaJceHelper(provider);
        return this;
    }
    
    public JcaPEMKeyConverter setProvider(final String s) {
        this.helper = new NamedJcaJceHelper(s);
        return this;
    }
    
    public KeyPair getKeyPair(final PEMKeyPair pemKeyPair) throws PEMException {
        try {
            String id = pemKeyPair.getPrivateKeyInfo().getPrivateKeyAlgorithm().getAlgorithm().getId();
            if (X9ObjectIdentifiers.id_ecPublicKey.getId().equals(id)) {
                id = "ECDSA";
            }
            final KeyFactory keyFactory = this.helper.createKeyFactory(id);
            return new KeyPair(keyFactory.generatePublic(new X509EncodedKeySpec(pemKeyPair.getPublicKeyInfo().getEncoded())), keyFactory.generatePrivate(new PKCS8EncodedKeySpec(pemKeyPair.getPrivateKeyInfo().getEncoded())));
        }
        catch (Exception ex) {
            throw new PEMException("unable to convert key pair: " + ex.getMessage(), ex);
        }
    }
    
    public PublicKey getPublicKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws PEMException {
        try {
            String id = subjectPublicKeyInfo.getAlgorithm().getAlgorithm().getId();
            if (X9ObjectIdentifiers.id_ecPublicKey.getId().equals(id)) {
                id = "ECDSA";
            }
            return this.helper.createKeyFactory(id).generatePublic(new X509EncodedKeySpec(subjectPublicKeyInfo.getEncoded()));
        }
        catch (Exception ex) {
            throw new PEMException("unable to convert key pair: " + ex.getMessage(), ex);
        }
    }
    
    public PrivateKey getPrivateKey(final PrivateKeyInfo privateKeyInfo) throws PEMException {
        try {
            String id = privateKeyInfo.getPrivateKeyAlgorithm().getAlgorithm().getId();
            if (X9ObjectIdentifiers.id_ecPublicKey.getId().equals(id)) {
                id = "ECDSA";
            }
            return this.helper.createKeyFactory(id).generatePrivate(new PKCS8EncodedKeySpec(privateKeyInfo.getEncoded()));
        }
        catch (Exception ex) {
            throw new PEMException("unable to convert key pair: " + ex.getMessage(), ex);
        }
    }
}
