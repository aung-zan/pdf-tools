// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.openssl.jcajce;

import javax.crypto.SecretKeyFactory;
import java.security.AlgorithmParameters;
import javax.crypto.SecretKey;
import org.bouncycastle.asn1.pkcs.EncryptionScheme;
import org.bouncycastle.asn1.pkcs.KeyDerivationFunc;
import java.security.GeneralSecurityException;
import java.io.IOException;
import javax.crypto.CipherInputStream;
import java.io.InputStream;
import javax.crypto.Cipher;
import org.bouncycastle.asn1.pkcs.PBEParameter;
import org.bouncycastle.openssl.PEMException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import org.bouncycastle.asn1.pkcs.PKCS12PBEParams;
import org.bouncycastle.asn1.DERObjectIdentifier;
import java.security.Key;
import org.bouncycastle.asn1.pkcs.PBKDF2Params;
import org.bouncycastle.asn1.pkcs.PBES2Parameters;
import org.bouncycastle.operator.InputDecryptor;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.InputDecryptorProvider;
import org.bouncycastle.jcajce.ProviderJcaJceHelper;
import java.security.Provider;
import org.bouncycastle.jcajce.NamedJcaJceHelper;
import org.bouncycastle.jcajce.DefaultJcaJceHelper;
import org.bouncycastle.jcajce.JcaJceHelper;

public class JceOpenSSLPKCS8DecryptorProviderBuilder
{
    private JcaJceHelper helper;
    
    public JceOpenSSLPKCS8DecryptorProviderBuilder() {
        this.helper = new DefaultJcaJceHelper();
        this.helper = new DefaultJcaJceHelper();
    }
    
    public JceOpenSSLPKCS8DecryptorProviderBuilder setProvider(final String s) {
        this.helper = new NamedJcaJceHelper(s);
        return this;
    }
    
    public JceOpenSSLPKCS8DecryptorProviderBuilder setProvider(final Provider provider) {
        this.helper = new ProviderJcaJceHelper(provider);
        return this;
    }
    
    public InputDecryptorProvider build(final char[] array) throws OperatorCreationException {
        return new InputDecryptorProvider() {
            public InputDecryptor get(final AlgorithmIdentifier algorithmIdentifier) throws OperatorCreationException {
                try {
                    Cipher cipher;
                    if (PEMUtilities.isPKCS5Scheme2(algorithmIdentifier.getAlgorithm())) {
                        final PBES2Parameters instance = PBES2Parameters.getInstance(algorithmIdentifier.getParameters());
                        final KeyDerivationFunc keyDerivationFunc = instance.getKeyDerivationFunc();
                        final EncryptionScheme encryptionScheme = instance.getEncryptionScheme();
                        final PBKDF2Params pbkdf2Params = (PBKDF2Params)keyDerivationFunc.getParameters();
                        final int intValue = pbkdf2Params.getIterationCount().intValue();
                        final byte[] salt = pbkdf2Params.getSalt();
                        final String id = encryptionScheme.getAlgorithm().getId();
                        final SecretKey generateSecretKeyForPKCS5Scheme2 = PEMUtilities.generateSecretKeyForPKCS5Scheme2(id, array, salt, intValue);
                        cipher = JceOpenSSLPKCS8DecryptorProviderBuilder.this.helper.createCipher(id);
                        final AlgorithmParameters algorithmParameters = JceOpenSSLPKCS8DecryptorProviderBuilder.this.helper.createAlgorithmParameters(id);
                        algorithmParameters.init(encryptionScheme.getParameters().toASN1Primitive().getEncoded());
                        cipher.init(2, generateSecretKeyForPKCS5Scheme2, algorithmParameters);
                    }
                    else if (PEMUtilities.isPKCS12(algorithmIdentifier.getAlgorithm())) {
                        final PKCS12PBEParams instance2 = PKCS12PBEParams.getInstance(algorithmIdentifier.getParameters());
                        final PBEKeySpec keySpec = new PBEKeySpec(array);
                        final SecretKeyFactory secretKeyFactory = JceOpenSSLPKCS8DecryptorProviderBuilder.this.helper.createSecretKeyFactory(algorithmIdentifier.getAlgorithm().getId());
                        final PBEParameterSpec params = new PBEParameterSpec(instance2.getIV(), instance2.getIterations().intValue());
                        cipher = JceOpenSSLPKCS8DecryptorProviderBuilder.this.helper.createCipher(algorithmIdentifier.getAlgorithm().getId());
                        cipher.init(2, secretKeyFactory.generateSecret(keySpec), params);
                    }
                    else {
                        if (!PEMUtilities.isPKCS5Scheme1(algorithmIdentifier.getAlgorithm())) {
                            throw new PEMException("Unknown algorithm: " + algorithmIdentifier.getAlgorithm());
                        }
                        final PBEParameter instance3 = PBEParameter.getInstance(algorithmIdentifier.getParameters());
                        final PBEKeySpec keySpec2 = new PBEKeySpec(array);
                        final SecretKeyFactory secretKeyFactory2 = JceOpenSSLPKCS8DecryptorProviderBuilder.this.helper.createSecretKeyFactory(algorithmIdentifier.getAlgorithm().getId());
                        final PBEParameterSpec params2 = new PBEParameterSpec(instance3.getSalt(), instance3.getIterationCount().intValue());
                        cipher = JceOpenSSLPKCS8DecryptorProviderBuilder.this.helper.createCipher(algorithmIdentifier.getAlgorithm().getId());
                        cipher.init(2, secretKeyFactory2.generateSecret(keySpec2), params2);
                    }
                    return new InputDecryptor() {
                        public AlgorithmIdentifier getAlgorithmIdentifier() {
                            return algorithmIdentifier;
                        }
                        
                        public InputStream getInputStream(final InputStream is) {
                            return new CipherInputStream(is, cipher);
                        }
                    };
                }
                catch (IOException ex) {
                    throw new OperatorCreationException(algorithmIdentifier.getAlgorithm() + " not available: " + ex.getMessage(), ex);
                }
                catch (GeneralSecurityException ex2) {
                    throw new OperatorCreationException(algorithmIdentifier.getAlgorithm() + " not available: " + ex2.getMessage(), ex2);
                }
            }
        };
    }
}
