// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.openssl.jcajce;

import org.bouncycastle.util.Integers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import java.util.HashSet;
import java.util.HashMap;
import org.bouncycastle.crypto.generators.OpenSSLPBEParametersGenerator;
import org.bouncycastle.openssl.PEMException;
import javax.crypto.Cipher;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Key;
import org.bouncycastle.openssl.EncryptionException;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.jcajce.JcaJceHelper;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import javax.crypto.SecretKey;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERObjectIdentifier;
import java.util.Set;
import java.util.Map;

class PEMUtilities
{
    private static final Map KEYSIZES;
    private static final Set PKCS5_SCHEME_1;
    private static final Set PKCS5_SCHEME_2;
    
    static int getKeySize(final String str) {
        if (!PEMUtilities.KEYSIZES.containsKey(str)) {
            throw new IllegalStateException("no key size for algorithm: " + str);
        }
        return PEMUtilities.KEYSIZES.get(str);
    }
    
    static boolean isPKCS5Scheme1(final DERObjectIdentifier derObjectIdentifier) {
        return PEMUtilities.PKCS5_SCHEME_1.contains(derObjectIdentifier);
    }
    
    static boolean isPKCS5Scheme2(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        return PEMUtilities.PKCS5_SCHEME_2.contains(asn1ObjectIdentifier);
    }
    
    public static boolean isPKCS12(final DERObjectIdentifier derObjectIdentifier) {
        return derObjectIdentifier.getId().startsWith(PKCSObjectIdentifiers.pkcs_12PbeIds.getId());
    }
    
    public static SecretKey generateSecretKeyForPKCS5Scheme2(final String algorithm, final char[] array, final byte[] array2, final int n) {
        final PKCS5S2ParametersGenerator pkcs5S2ParametersGenerator = new PKCS5S2ParametersGenerator();
        pkcs5S2ParametersGenerator.init(PBEParametersGenerator.PKCS5PasswordToBytes(array), array2, n);
        return new SecretKeySpec(((KeyParameter)pkcs5S2ParametersGenerator.generateDerivedParameters(getKeySize(algorithm))).getKey(), algorithm);
    }
    
    static byte[] crypt(final boolean b, final JcaJceHelper jcaJceHelper, final byte[] input, final char[] array, final String s, final byte[] array2) throws PEMException {
        AlgorithmParameterSpec params = new IvParameterSpec(array2);
        String str = "CBC";
        String str2 = "PKCS5Padding";
        if (s.endsWith("-CFB")) {
            str = "CFB";
            str2 = "NoPadding";
        }
        if (s.endsWith("-ECB") || "DES-EDE".equals(s) || "DES-EDE3".equals(s)) {
            str = "ECB";
            params = null;
        }
        if (s.endsWith("-OFB")) {
            str = "OFB";
            str2 = "NoPadding";
        }
        String str3;
        SecretKey secretKey;
        if (s.startsWith("DES-EDE")) {
            str3 = "DESede";
            secretKey = getKey(array, str3, 24, array2, !s.startsWith("DES-EDE3"));
        }
        else if (s.startsWith("DES-")) {
            str3 = "DES";
            secretKey = getKey(array, str3, 8, array2);
        }
        else if (s.startsWith("BF-")) {
            str3 = "Blowfish";
            secretKey = getKey(array, str3, 16, array2);
        }
        else if (s.startsWith("RC2-")) {
            str3 = "RC2";
            int n = 128;
            if (s.startsWith("RC2-40-")) {
                n = 40;
            }
            else if (s.startsWith("RC2-64-")) {
                n = 64;
            }
            secretKey = getKey(array, str3, n / 8, array2);
            if (params == null) {
                params = new RC2ParameterSpec(n);
            }
            else {
                params = new RC2ParameterSpec(n, array2);
            }
        }
        else {
            if (!s.startsWith("AES-")) {
                throw new EncryptionException("unknown encryption with private key");
            }
            str3 = "AES";
            byte[] array3 = array2;
            if (array3.length > 8) {
                array3 = new byte[8];
                System.arraycopy(array2, 0, array3, 0, 8);
            }
            int n2;
            if (s.startsWith("AES-128-")) {
                n2 = 128;
            }
            else if (s.startsWith("AES-192-")) {
                n2 = 192;
            }
            else {
                if (!s.startsWith("AES-256-")) {
                    throw new EncryptionException("unknown AES encryption with private key");
                }
                n2 = 256;
            }
            secretKey = getKey(array, "AES", n2 / 8, array3);
        }
        final String string = str3 + "/" + str + "/" + str2;
        try {
            final Cipher cipher = jcaJceHelper.createCipher(string);
            final int n3 = b ? 1 : 2;
            if (params == null) {
                cipher.init(n3, secretKey);
            }
            else {
                cipher.init(n3, secretKey, params);
            }
            return cipher.doFinal(input);
        }
        catch (Exception ex) {
            throw new EncryptionException("exception using cipher - please check password and data.", (Throwable)ex);
        }
    }
    
    private static SecretKey getKey(final char[] array, final String s, final int n, final byte[] array2) {
        return getKey(array, s, n, array2, false);
    }
    
    private static SecretKey getKey(final char[] array, final String algorithm, final int n, final byte[] array2, final boolean b) {
        final OpenSSLPBEParametersGenerator openSSLPBEParametersGenerator = new OpenSSLPBEParametersGenerator();
        openSSLPBEParametersGenerator.init(PBEParametersGenerator.PKCS5PasswordToBytes(array), array2);
        final byte[] key = ((KeyParameter)openSSLPBEParametersGenerator.generateDerivedParameters(n * 8)).getKey();
        if (b && key.length >= 24) {
            System.arraycopy(key, 0, key, 16, 8);
        }
        return new SecretKeySpec(key, algorithm);
    }
    
    static {
        KEYSIZES = new HashMap();
        PKCS5_SCHEME_1 = new HashSet();
        PKCS5_SCHEME_2 = new HashSet();
        PEMUtilities.PKCS5_SCHEME_1.add(PKCSObjectIdentifiers.pbeWithMD2AndDES_CBC);
        PEMUtilities.PKCS5_SCHEME_1.add(PKCSObjectIdentifiers.pbeWithMD2AndRC2_CBC);
        PEMUtilities.PKCS5_SCHEME_1.add(PKCSObjectIdentifiers.pbeWithMD5AndDES_CBC);
        PEMUtilities.PKCS5_SCHEME_1.add(PKCSObjectIdentifiers.pbeWithMD5AndRC2_CBC);
        PEMUtilities.PKCS5_SCHEME_1.add(PKCSObjectIdentifiers.pbeWithSHA1AndDES_CBC);
        PEMUtilities.PKCS5_SCHEME_1.add(PKCSObjectIdentifiers.pbeWithSHA1AndRC2_CBC);
        PEMUtilities.PKCS5_SCHEME_2.add(PKCSObjectIdentifiers.id_PBES2);
        PEMUtilities.PKCS5_SCHEME_2.add(PKCSObjectIdentifiers.des_EDE3_CBC);
        PEMUtilities.PKCS5_SCHEME_2.add(NISTObjectIdentifiers.id_aes128_CBC);
        PEMUtilities.PKCS5_SCHEME_2.add(NISTObjectIdentifiers.id_aes192_CBC);
        PEMUtilities.PKCS5_SCHEME_2.add(NISTObjectIdentifiers.id_aes256_CBC);
        PEMUtilities.KEYSIZES.put(PKCSObjectIdentifiers.des_EDE3_CBC.getId(), Integers.valueOf(192));
        PEMUtilities.KEYSIZES.put(NISTObjectIdentifiers.id_aes128_CBC.getId(), Integers.valueOf(128));
        PEMUtilities.KEYSIZES.put(NISTObjectIdentifiers.id_aes192_CBC.getId(), Integers.valueOf(192));
        PEMUtilities.KEYSIZES.put(NISTObjectIdentifiers.id_aes256_CBC.getId(), Integers.valueOf(256));
    }
}
