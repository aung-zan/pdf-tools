// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider;

import java.util.HashMap;
import java.security.PrivateKey;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.io.IOException;
import java.security.PublicKey;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jcajce.provider.util.AsymmetricKeyInfoConverter;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.jcajce.provider.util.AlgorithmProvider;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Map;
import org.bouncycastle.jcajce.provider.config.ProviderConfiguration;
import org.bouncycastle.jcajce.provider.config.ConfigurableProvider;
import java.security.Provider;

public class BouncyCastlePQCProvider extends Provider implements ConfigurableProvider
{
    private static String info;
    public static String PROVIDER_NAME;
    public static final ProviderConfiguration CONFIGURATION;
    private static final Map keyInfoConverters;
    private static final String ALGORITHM_PACKAGE = "org.bouncycastle.pqc.jcajce.provider.";
    private static final String[] ALGORITHMS;
    
    public BouncyCastlePQCProvider() {
        super(BouncyCastlePQCProvider.PROVIDER_NAME, 1.48, BouncyCastlePQCProvider.info);
        AccessController.doPrivileged((PrivilegedAction<Object>)new PrivilegedAction() {
            public Object run() {
                BouncyCastlePQCProvider.this.setup();
                return null;
            }
        });
    }
    
    private void setup() {
        this.loadAlgorithms("org.bouncycastle.pqc.jcajce.provider.", BouncyCastlePQCProvider.ALGORITHMS);
    }
    
    private void loadAlgorithms(final String str, final String[] array) {
        for (int i = 0; i != array.length; ++i) {
            Class<?> clazz = null;
            try {
                final ClassLoader classLoader = this.getClass().getClassLoader();
                if (classLoader != null) {
                    clazz = classLoader.loadClass(str + array[i] + "$Mappings");
                }
                else {
                    clazz = Class.forName(str + array[i] + "$Mappings");
                }
            }
            catch (ClassNotFoundException ex) {}
            if (clazz != null) {
                try {
                    ((AlgorithmProvider)clazz.newInstance()).configure(this);
                }
                catch (Exception obj) {
                    throw new InternalError("cannot create instance of " + str + array[i] + "$Mappings : " + obj);
                }
            }
        }
    }
    
    public void setParameter(final String s, final Object o) {
        synchronized (BouncyCastlePQCProvider.CONFIGURATION) {
        }
        // monitorexit(BouncyCastlePQCProvider.CONFIGURATION)
    }
    
    public boolean hasAlgorithm(final String s, final String s2) {
        return this.containsKey(s + "." + s2) || this.containsKey("Alg.Alias." + s + "." + s2);
    }
    
    public void addAlgorithm(final String key, final String value) {
        if (this.containsKey(key)) {
            throw new IllegalStateException("duplicate provider key (" + key + ") found");
        }
        this.put(key, value);
    }
    
    public void addKeyInfoConverter(final ASN1ObjectIdentifier asn1ObjectIdentifier, final AsymmetricKeyInfoConverter asymmetricKeyInfoConverter) {
        BouncyCastlePQCProvider.keyInfoConverters.put(asn1ObjectIdentifier, asymmetricKeyInfoConverter);
    }
    
    public static PublicKey getPublicKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        final AsymmetricKeyInfoConverter asymmetricKeyInfoConverter = BouncyCastlePQCProvider.keyInfoConverters.get(subjectPublicKeyInfo.getAlgorithm().getAlgorithm());
        if (asymmetricKeyInfoConverter == null) {
            return null;
        }
        return asymmetricKeyInfoConverter.generatePublic(subjectPublicKeyInfo);
    }
    
    public static PrivateKey getPrivateKey(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final AsymmetricKeyInfoConverter asymmetricKeyInfoConverter = BouncyCastlePQCProvider.keyInfoConverters.get(privateKeyInfo.getPrivateKeyAlgorithm().getAlgorithm());
        if (asymmetricKeyInfoConverter == null) {
            return null;
        }
        return asymmetricKeyInfoConverter.generatePrivate(privateKeyInfo);
    }
    
    static {
        BouncyCastlePQCProvider.info = "BouncyCastle Post-Quantum Security Provider v1.48";
        BouncyCastlePQCProvider.PROVIDER_NAME = "BCPQC";
        CONFIGURATION = null;
        keyInfoConverters = new HashMap();
        ALGORITHMS = new String[] { "Rainbow", "McEliece" };
    }
}
