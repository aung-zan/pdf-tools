// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.mceliece;

import org.bouncycastle.pqc.asn1.McElieceCCA2PrivateKey;
import org.bouncycastle.pqc.asn1.McElieceCCA2PublicKey;
import java.security.InvalidKeyException;
import java.security.Key;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.spec.PKCS8EncodedKeySpec;
import org.bouncycastle.pqc.jcajce.spec.McElieceCCA2PrivateKeySpec;
import java.security.PrivateKey;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import java.io.IOException;
import java.security.spec.InvalidKeySpecException;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1Primitive;
import java.security.spec.X509EncodedKeySpec;
import org.bouncycastle.pqc.jcajce.spec.McElieceCCA2PublicKeySpec;
import java.security.PublicKey;
import java.security.spec.KeySpec;
import java.security.KeyFactorySpi;

public class McElieceCCA2KeyFactorySpi extends KeyFactorySpi
{
    public static final String OID = "1.3.6.1.4.1.8301.3.1.3.4.2";
    
    public PublicKey generatePublic(final KeySpec keySpec) throws InvalidKeySpecException {
        if (keySpec instanceof McElieceCCA2PublicKeySpec) {
            return new BCMcElieceCCA2PublicKey((McElieceCCA2PublicKeySpec)keySpec);
        }
        if (keySpec instanceof X509EncodedKeySpec) {
            final byte[] encoded = ((X509EncodedKeySpec)keySpec).getEncoded();
            SubjectPublicKeyInfo instance;
            try {
                instance = SubjectPublicKeyInfo.getInstance(ASN1Primitive.fromByteArray(encoded));
            }
            catch (IOException ex) {
                throw new InvalidKeySpecException(ex.toString());
            }
            try {
                final ASN1Sequence asn1Sequence = (ASN1Sequence)instance.parsePublicKey();
                ((ASN1ObjectIdentifier)asn1Sequence.getObjectAt(0)).toString();
                return new BCMcElieceCCA2PublicKey(new McElieceCCA2PublicKeySpec("1.3.6.1.4.1.8301.3.1.3.4.2", ((ASN1Integer)asn1Sequence.getObjectAt(1)).getValue().intValue(), ((ASN1Integer)asn1Sequence.getObjectAt(2)).getValue().intValue(), ((ASN1OctetString)asn1Sequence.getObjectAt(3)).getOctets()));
            }
            catch (IOException ex2) {
                throw new InvalidKeySpecException("Unable to decode X509EncodedKeySpec: " + ex2.getMessage());
            }
        }
        throw new InvalidKeySpecException("Unsupported key specification: " + keySpec.getClass() + ".");
    }
    
    public PrivateKey generatePrivate(final KeySpec keySpec) throws InvalidKeySpecException {
        if (keySpec instanceof McElieceCCA2PrivateKeySpec) {
            return new BCMcElieceCCA2PrivateKey((McElieceCCA2PrivateKeySpec)keySpec);
        }
        if (keySpec instanceof PKCS8EncodedKeySpec) {
            final byte[] encoded = ((PKCS8EncodedKeySpec)keySpec).getEncoded();
            PrivateKeyInfo instance;
            try {
                instance = PrivateKeyInfo.getInstance(ASN1Primitive.fromByteArray(encoded));
            }
            catch (IOException obj) {
                throw new InvalidKeySpecException("Unable to decode PKCS8EncodedKeySpec: " + obj);
            }
            try {
                final ASN1Sequence asn1Sequence = (ASN1Sequence)instance.parsePrivateKey().toASN1Primitive();
                ((ASN1ObjectIdentifier)asn1Sequence.getObjectAt(0)).toString();
                final int intValue = ((ASN1Integer)asn1Sequence.getObjectAt(1)).getValue().intValue();
                final int intValue2 = ((ASN1Integer)asn1Sequence.getObjectAt(2)).getValue().intValue();
                final byte[] octets = ((ASN1OctetString)asn1Sequence.getObjectAt(3)).getOctets();
                final byte[] octets2 = ((ASN1OctetString)asn1Sequence.getObjectAt(4)).getOctets();
                final byte[] octets3 = ((ASN1OctetString)asn1Sequence.getObjectAt(5)).getOctets();
                final byte[] octets4 = ((ASN1OctetString)asn1Sequence.getObjectAt(6)).getOctets();
                final ASN1Sequence asn1Sequence2 = (ASN1Sequence)asn1Sequence.getObjectAt(7);
                final byte[][] array = new byte[asn1Sequence2.size()][];
                for (int i = 0; i < asn1Sequence2.size(); ++i) {
                    array[i] = ((ASN1OctetString)asn1Sequence2.getObjectAt(i)).getOctets();
                }
                return new BCMcElieceCCA2PrivateKey(new McElieceCCA2PrivateKeySpec("1.3.6.1.4.1.8301.3.1.3.4.2", intValue, intValue2, octets, octets2, octets3, octets4, array));
            }
            catch (IOException ex) {
                throw new InvalidKeySpecException("Unable to decode PKCS8EncodedKeySpec.");
            }
        }
        throw new InvalidKeySpecException("Unsupported key specification: " + keySpec.getClass() + ".");
    }
    
    public KeySpec getKeySpec(final Key key, final Class obj) throws InvalidKeySpecException {
        if (key instanceof BCMcElieceCCA2PrivateKey) {
            if (PKCS8EncodedKeySpec.class.isAssignableFrom(obj)) {
                return new PKCS8EncodedKeySpec(key.getEncoded());
            }
            if (McElieceCCA2PrivateKeySpec.class.isAssignableFrom(obj)) {
                final BCMcElieceCCA2PrivateKey bcMcElieceCCA2PrivateKey = (BCMcElieceCCA2PrivateKey)key;
                return new McElieceCCA2PrivateKeySpec("1.3.6.1.4.1.8301.3.1.3.4.2", bcMcElieceCCA2PrivateKey.getN(), bcMcElieceCCA2PrivateKey.getK(), bcMcElieceCCA2PrivateKey.getField(), bcMcElieceCCA2PrivateKey.getGoppaPoly(), bcMcElieceCCA2PrivateKey.getP(), bcMcElieceCCA2PrivateKey.getH(), bcMcElieceCCA2PrivateKey.getQInv());
            }
        }
        else {
            if (!(key instanceof BCMcElieceCCA2PublicKey)) {
                throw new InvalidKeySpecException("Unsupported key type: " + key.getClass() + ".");
            }
            if (X509EncodedKeySpec.class.isAssignableFrom(obj)) {
                return new X509EncodedKeySpec(key.getEncoded());
            }
            if (McElieceCCA2PublicKeySpec.class.isAssignableFrom(obj)) {
                final BCMcElieceCCA2PublicKey bcMcElieceCCA2PublicKey = (BCMcElieceCCA2PublicKey)key;
                return new McElieceCCA2PublicKeySpec("1.3.6.1.4.1.8301.3.1.3.4.2", bcMcElieceCCA2PublicKey.getN(), bcMcElieceCCA2PublicKey.getT(), bcMcElieceCCA2PublicKey.getG());
            }
        }
        throw new InvalidKeySpecException("Unknown key specification: " + obj + ".");
    }
    
    public Key translateKey(final Key key) throws InvalidKeyException {
        if (key instanceof BCMcElieceCCA2PrivateKey || key instanceof BCMcElieceCCA2PublicKey) {
            return key;
        }
        throw new InvalidKeyException("Unsupported key type.");
    }
    
    public PublicKey generatePublic(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws InvalidKeySpecException {
        try {
            final McElieceCCA2PublicKey instance = McElieceCCA2PublicKey.getInstance(subjectPublicKeyInfo.parsePublicKey());
            return new BCMcElieceCCA2PublicKey(instance.getOID().getId(), instance.getN(), instance.getT(), instance.getG());
        }
        catch (IOException ex) {
            throw new InvalidKeySpecException("Unable to decode X509EncodedKeySpec");
        }
    }
    
    public PrivateKey generatePrivate(final PrivateKeyInfo privateKeyInfo) throws InvalidKeySpecException {
        try {
            final McElieceCCA2PrivateKey instance = McElieceCCA2PrivateKey.getInstance(privateKeyInfo.parsePrivateKey().toASN1Primitive());
            return new BCMcElieceCCA2PrivateKey(instance.getOID().getId(), instance.getN(), instance.getK(), instance.getField(), instance.getGoppaPoly(), instance.getP(), instance.getH(), instance.getQInv());
        }
        catch (IOException ex) {
            throw new InvalidKeySpecException("Unable to decode PKCS8EncodedKeySpec");
        }
    }
    
    @Override
    protected PublicKey engineGeneratePublic(final KeySpec keySpec) throws InvalidKeySpecException {
        return null;
    }
    
    @Override
    protected PrivateKey engineGeneratePrivate(final KeySpec keySpec) throws InvalidKeySpecException {
        return null;
    }
    
    @Override
    protected KeySpec engineGetKeySpec(final Key key, final Class clazz) throws InvalidKeySpecException {
        return null;
    }
    
    @Override
    protected Key engineTranslateKey(final Key key) throws InvalidKeyException {
        return null;
    }
}
