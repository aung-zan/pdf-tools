// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.mceliece;

import org.bouncycastle.pqc.crypto.mceliece.McEliecePrivateKeyParameters;
import java.security.PrivateKey;
import java.security.InvalidKeyException;
import org.bouncycastle.pqc.crypto.mceliece.McEliecePublicKeyParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.security.PublicKey;

public class McElieceKeysToParams
{
    public static AsymmetricKeyParameter generatePublicKeyParameter(final PublicKey publicKey) throws InvalidKeyException {
        if (publicKey instanceof BCMcEliecePublicKey) {
            final BCMcEliecePublicKey bcMcEliecePublicKey = (BCMcEliecePublicKey)publicKey;
            return new McEliecePublicKeyParameters(bcMcEliecePublicKey.getOIDString(), bcMcEliecePublicKey.getN(), bcMcEliecePublicKey.getT(), bcMcEliecePublicKey.getG(), bcMcEliecePublicKey.getMcElieceParameters());
        }
        throw new InvalidKeyException("can't identify McEliece public key: " + publicKey.getClass().getName());
    }
    
    public static AsymmetricKeyParameter generatePrivateKeyParameter(final PrivateKey privateKey) throws InvalidKeyException {
        if (privateKey instanceof BCMcEliecePrivateKey) {
            final BCMcEliecePrivateKey bcMcEliecePrivateKey = (BCMcEliecePrivateKey)privateKey;
            return new McEliecePrivateKeyParameters(bcMcEliecePrivateKey.getOIDString(), bcMcEliecePrivateKey.getN(), bcMcEliecePrivateKey.getK(), bcMcEliecePrivateKey.getField(), bcMcEliecePrivateKey.getGoppaPoly(), bcMcEliecePrivateKey.getSInv(), bcMcEliecePrivateKey.getP1(), bcMcEliecePrivateKey.getP2(), bcMcEliecePrivateKey.getH(), bcMcEliecePrivateKey.getQInv(), bcMcEliecePrivateKey.getMcElieceParameters());
        }
        throw new InvalidKeyException("can't identify McEliece private key.");
    }
}
