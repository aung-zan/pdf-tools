// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.mceliece;

import org.bouncycastle.pqc.crypto.mceliece.McElieceCCA2PrivateKeyParameters;
import java.security.PrivateKey;
import java.security.InvalidKeyException;
import org.bouncycastle.pqc.crypto.mceliece.McElieceCCA2PublicKeyParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.security.PublicKey;

public class McElieceCCA2KeysToParams
{
    public static AsymmetricKeyParameter generatePublicKeyParameter(final PublicKey publicKey) throws InvalidKeyException {
        if (publicKey instanceof BCMcElieceCCA2PublicKey) {
            final BCMcElieceCCA2PublicKey bcMcElieceCCA2PublicKey = (BCMcElieceCCA2PublicKey)publicKey;
            return new McElieceCCA2PublicKeyParameters(bcMcElieceCCA2PublicKey.getOIDString(), bcMcElieceCCA2PublicKey.getN(), bcMcElieceCCA2PublicKey.getT(), bcMcElieceCCA2PublicKey.getG(), bcMcElieceCCA2PublicKey.getMcElieceCCA2Parameters());
        }
        throw new InvalidKeyException("can't identify McElieceCCA2 public key: " + publicKey.getClass().getName());
    }
    
    public static AsymmetricKeyParameter generatePrivateKeyParameter(final PrivateKey privateKey) throws InvalidKeyException {
        if (privateKey instanceof BCMcElieceCCA2PrivateKey) {
            final BCMcElieceCCA2PrivateKey bcMcElieceCCA2PrivateKey = (BCMcElieceCCA2PrivateKey)privateKey;
            return new McElieceCCA2PrivateKeyParameters(bcMcElieceCCA2PrivateKey.getOIDString(), bcMcElieceCCA2PrivateKey.getN(), bcMcElieceCCA2PrivateKey.getK(), bcMcElieceCCA2PrivateKey.getField(), bcMcElieceCCA2PrivateKey.getGoppaPoly(), bcMcElieceCCA2PrivateKey.getP(), bcMcElieceCCA2PrivateKey.getH(), bcMcElieceCCA2PrivateKey.getQInv(), bcMcElieceCCA2PrivateKey.getMcElieceCCA2Parameters());
        }
        throw new InvalidKeyException("can't identify McElieceCCA2 private key.");
    }
}
