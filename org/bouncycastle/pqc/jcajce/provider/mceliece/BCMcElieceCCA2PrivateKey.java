// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.mceliece;

import java.io.IOException;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.pqc.asn1.McElieceCCA2PrivateKey;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.pqc.crypto.mceliece.McElieceCCA2PrivateKeyParameters;
import org.bouncycastle.pqc.jcajce.spec.McElieceCCA2PrivateKeySpec;
import org.bouncycastle.pqc.crypto.mceliece.McElieceCCA2Parameters;
import org.bouncycastle.pqc.math.linearalgebra.GF2Matrix;
import org.bouncycastle.pqc.math.linearalgebra.Permutation;
import org.bouncycastle.pqc.math.linearalgebra.PolynomialGF2mSmallM;
import org.bouncycastle.pqc.math.linearalgebra.GF2mField;
import java.security.PrivateKey;
import org.bouncycastle.crypto.CipherParameters;

public class BCMcElieceCCA2PrivateKey implements CipherParameters, PrivateKey
{
    private static final long serialVersionUID = 1L;
    private String oid;
    private int n;
    private int k;
    private GF2mField field;
    private PolynomialGF2mSmallM goppaPoly;
    private Permutation p;
    private GF2Matrix h;
    private PolynomialGF2mSmallM[] qInv;
    private McElieceCCA2Parameters mcElieceCCA2Params;
    
    public BCMcElieceCCA2PrivateKey(final String oid, final int n, final int k, final GF2mField field, final PolynomialGF2mSmallM goppaPoly, final Permutation p8, final GF2Matrix h, final PolynomialGF2mSmallM[] qInv) {
        this.oid = oid;
        this.n = n;
        this.k = k;
        this.field = field;
        this.goppaPoly = goppaPoly;
        this.p = p8;
        this.h = h;
        this.qInv = qInv;
    }
    
    public BCMcElieceCCA2PrivateKey(final McElieceCCA2PrivateKeySpec mcElieceCCA2PrivateKeySpec) {
        this(mcElieceCCA2PrivateKeySpec.getOIDString(), mcElieceCCA2PrivateKeySpec.getN(), mcElieceCCA2PrivateKeySpec.getK(), mcElieceCCA2PrivateKeySpec.getField(), mcElieceCCA2PrivateKeySpec.getGoppaPoly(), mcElieceCCA2PrivateKeySpec.getP(), mcElieceCCA2PrivateKeySpec.getH(), mcElieceCCA2PrivateKeySpec.getQInv());
    }
    
    public BCMcElieceCCA2PrivateKey(final McElieceCCA2PrivateKeyParameters mcElieceCCA2PrivateKeyParameters) {
        this(mcElieceCCA2PrivateKeyParameters.getOIDString(), mcElieceCCA2PrivateKeyParameters.getN(), mcElieceCCA2PrivateKeyParameters.getK(), mcElieceCCA2PrivateKeyParameters.getField(), mcElieceCCA2PrivateKeyParameters.getGoppaPoly(), mcElieceCCA2PrivateKeyParameters.getP(), mcElieceCCA2PrivateKeyParameters.getH(), mcElieceCCA2PrivateKeyParameters.getQInv());
        this.mcElieceCCA2Params = mcElieceCCA2PrivateKeyParameters.getParameters();
    }
    
    public String getAlgorithm() {
        return "McEliece";
    }
    
    public int getN() {
        return this.n;
    }
    
    public int getK() {
        return this.k;
    }
    
    public int getT() {
        return this.goppaPoly.getDegree();
    }
    
    public GF2mField getField() {
        return this.field;
    }
    
    public PolynomialGF2mSmallM getGoppaPoly() {
        return this.goppaPoly;
    }
    
    public Permutation getP() {
        return this.p;
    }
    
    public GF2Matrix getH() {
        return this.h;
    }
    
    public PolynomialGF2mSmallM[] getQInv() {
        return this.qInv;
    }
    
    @Override
    public String toString() {
        return "" + " extension degree of the field      : " + this.n + "\n" + " dimension of the code              : " + this.k + "\n" + " irreducible Goppa polynomial       : " + this.goppaPoly + "\n";
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null || !(o instanceof BCMcElieceCCA2PrivateKey)) {
            return false;
        }
        final BCMcElieceCCA2PrivateKey bcMcElieceCCA2PrivateKey = (BCMcElieceCCA2PrivateKey)o;
        return this.n == bcMcElieceCCA2PrivateKey.n && this.k == bcMcElieceCCA2PrivateKey.k && this.field.equals(bcMcElieceCCA2PrivateKey.field) && this.goppaPoly.equals(bcMcElieceCCA2PrivateKey.goppaPoly) && this.p.equals(bcMcElieceCCA2PrivateKey.p) && this.h.equals(bcMcElieceCCA2PrivateKey.h);
    }
    
    @Override
    public int hashCode() {
        return this.k + this.n + this.field.hashCode() + this.goppaPoly.hashCode() + this.p.hashCode() + this.h.hashCode();
    }
    
    public String getOIDString() {
        return this.oid;
    }
    
    protected ASN1ObjectIdentifier getOID() {
        return new ASN1ObjectIdentifier("1.3.6.1.4.1.8301.3.1.3.4.2");
    }
    
    protected ASN1Primitive getAlgParams() {
        return null;
    }
    
    public byte[] getEncoded() {
        final McElieceCCA2PrivateKey mcElieceCCA2PrivateKey = new McElieceCCA2PrivateKey(new ASN1ObjectIdentifier(this.oid), this.n, this.k, this.field, this.goppaPoly, this.p, this.h, this.qInv);
        PrivateKeyInfo privateKeyInfo;
        try {
            privateKeyInfo = new PrivateKeyInfo(new AlgorithmIdentifier(this.getOID(), DERNull.INSTANCE), mcElieceCCA2PrivateKey);
        }
        catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            return privateKeyInfo.getEncoded();
        }
        catch (IOException ex2) {
            ex2.printStackTrace();
            return null;
        }
    }
    
    public String getFormat() {
        return null;
    }
    
    public McElieceCCA2Parameters getMcElieceCCA2Parameters() {
        return this.mcElieceCCA2Params;
    }
}
