// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.mceliece;

import java.io.IOException;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.pqc.asn1.McEliecePrivateKey;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.pqc.crypto.mceliece.McEliecePrivateKeyParameters;
import org.bouncycastle.pqc.jcajce.spec.McEliecePrivateKeySpec;
import org.bouncycastle.pqc.crypto.mceliece.McElieceParameters;
import org.bouncycastle.pqc.math.linearalgebra.Permutation;
import org.bouncycastle.pqc.math.linearalgebra.GF2Matrix;
import org.bouncycastle.pqc.math.linearalgebra.PolynomialGF2mSmallM;
import org.bouncycastle.pqc.math.linearalgebra.GF2mField;
import java.security.PrivateKey;
import org.bouncycastle.crypto.CipherParameters;

public class BCMcEliecePrivateKey implements CipherParameters, PrivateKey
{
    private static final long serialVersionUID = 1L;
    private String oid;
    private int n;
    private int k;
    private GF2mField field;
    private PolynomialGF2mSmallM goppaPoly;
    private GF2Matrix sInv;
    private Permutation p1;
    private Permutation p2;
    private GF2Matrix h;
    private PolynomialGF2mSmallM[] qInv;
    private McElieceParameters mcElieceParams;
    
    public BCMcEliecePrivateKey(final String oid, final int n, final int k, final GF2mField field, final PolynomialGF2mSmallM goppaPoly, final GF2Matrix sInv, final Permutation p10, final Permutation p11, final GF2Matrix h, final PolynomialGF2mSmallM[] qInv) {
        this.oid = oid;
        this.n = n;
        this.k = k;
        this.field = field;
        this.goppaPoly = goppaPoly;
        this.sInv = sInv;
        this.p1 = p10;
        this.p2 = p11;
        this.h = h;
        this.qInv = qInv;
    }
    
    public BCMcEliecePrivateKey(final McEliecePrivateKeySpec mcEliecePrivateKeySpec) {
        this(mcEliecePrivateKeySpec.getOIDString(), mcEliecePrivateKeySpec.getN(), mcEliecePrivateKeySpec.getK(), mcEliecePrivateKeySpec.getField(), mcEliecePrivateKeySpec.getGoppaPoly(), mcEliecePrivateKeySpec.getSInv(), mcEliecePrivateKeySpec.getP1(), mcEliecePrivateKeySpec.getP2(), mcEliecePrivateKeySpec.getH(), mcEliecePrivateKeySpec.getQInv());
    }
    
    public BCMcEliecePrivateKey(final McEliecePrivateKeyParameters mcEliecePrivateKeyParameters) {
        this(mcEliecePrivateKeyParameters.getOIDString(), mcEliecePrivateKeyParameters.getN(), mcEliecePrivateKeyParameters.getK(), mcEliecePrivateKeyParameters.getField(), mcEliecePrivateKeyParameters.getGoppaPoly(), mcEliecePrivateKeyParameters.getSInv(), mcEliecePrivateKeyParameters.getP1(), mcEliecePrivateKeyParameters.getP2(), mcEliecePrivateKeyParameters.getH(), mcEliecePrivateKeyParameters.getQInv());
        this.mcElieceParams = mcEliecePrivateKeyParameters.getParameters();
    }
    
    public String getAlgorithm() {
        return "McEliece";
    }
    
    public int getN() {
        return this.n;
    }
    
    public int getK() {
        return this.k;
    }
    
    public GF2mField getField() {
        return this.field;
    }
    
    public PolynomialGF2mSmallM getGoppaPoly() {
        return this.goppaPoly;
    }
    
    public GF2Matrix getSInv() {
        return this.sInv;
    }
    
    public Permutation getP1() {
        return this.p1;
    }
    
    public Permutation getP2() {
        return this.p2;
    }
    
    public GF2Matrix getH() {
        return this.h;
    }
    
    public PolynomialGF2mSmallM[] getQInv() {
        return this.qInv;
    }
    
    public String getOIDString() {
        return this.oid;
    }
    
    @Override
    public String toString() {
        return " length of the code          : " + this.n + "\n" + " dimension of the code       : " + this.k + "\n" + " irreducible Goppa polynomial: " + this.goppaPoly + "\n" + " (k x k)-matrix S^-1         : " + this.sInv + "\n" + " permutation P1              : " + this.p1 + "\n" + " permutation P2              : " + this.p2;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof BCMcEliecePrivateKey)) {
            return false;
        }
        final BCMcEliecePrivateKey bcMcEliecePrivateKey = (BCMcEliecePrivateKey)o;
        return this.n == bcMcEliecePrivateKey.n && this.k == bcMcEliecePrivateKey.k && this.field.equals(bcMcEliecePrivateKey.field) && this.goppaPoly.equals(bcMcEliecePrivateKey.goppaPoly) && this.sInv.equals(bcMcEliecePrivateKey.sInv) && this.p1.equals(bcMcEliecePrivateKey.p1) && this.p2.equals(bcMcEliecePrivateKey.p2) && this.h.equals(bcMcEliecePrivateKey.h);
    }
    
    @Override
    public int hashCode() {
        return this.k + this.n + this.field.hashCode() + this.goppaPoly.hashCode() + this.sInv.hashCode() + this.p1.hashCode() + this.p2.hashCode() + this.h.hashCode();
    }
    
    protected ASN1ObjectIdentifier getOID() {
        return new ASN1ObjectIdentifier("1.3.6.1.4.1.8301.3.1.3.4.1");
    }
    
    protected ASN1Primitive getAlgParams() {
        return null;
    }
    
    public byte[] getEncoded() {
        final McEliecePrivateKey mcEliecePrivateKey = new McEliecePrivateKey(new ASN1ObjectIdentifier(this.oid), this.n, this.k, this.field, this.goppaPoly, this.sInv, this.p1, this.p2, this.h, this.qInv);
        PrivateKeyInfo privateKeyInfo;
        try {
            privateKeyInfo = new PrivateKeyInfo(new AlgorithmIdentifier(this.getOID(), DERNull.INSTANCE), mcEliecePrivateKey);
        }
        catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            return privateKeyInfo.getEncoded();
        }
        catch (IOException ex2) {
            ex2.printStackTrace();
            return null;
        }
    }
    
    public String getFormat() {
        return null;
    }
    
    public McElieceParameters getMcElieceParameters() {
        return this.mcElieceParams;
    }
}
