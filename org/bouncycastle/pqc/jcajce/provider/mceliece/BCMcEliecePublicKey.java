// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.mceliece;

import java.io.IOException;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.pqc.asn1.McEliecePublicKey;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.pqc.crypto.mceliece.McEliecePublicKeyParameters;
import org.bouncycastle.pqc.jcajce.spec.McEliecePublicKeySpec;
import org.bouncycastle.pqc.crypto.mceliece.McElieceParameters;
import org.bouncycastle.pqc.math.linearalgebra.GF2Matrix;
import java.security.PublicKey;
import org.bouncycastle.crypto.CipherParameters;

public class BCMcEliecePublicKey implements CipherParameters, PublicKey
{
    private static final long serialVersionUID = 1L;
    private String oid;
    private int n;
    private int t;
    private GF2Matrix g;
    private McElieceParameters McElieceParams;
    
    public BCMcEliecePublicKey(final String oid, final int n, final int t, final GF2Matrix g) {
        this.oid = oid;
        this.n = n;
        this.t = t;
        this.g = g;
    }
    
    public BCMcEliecePublicKey(final McEliecePublicKeySpec mcEliecePublicKeySpec) {
        this(mcEliecePublicKeySpec.getOIDString(), mcEliecePublicKeySpec.getN(), mcEliecePublicKeySpec.getT(), mcEliecePublicKeySpec.getG());
    }
    
    public BCMcEliecePublicKey(final McEliecePublicKeyParameters mcEliecePublicKeyParameters) {
        this(mcEliecePublicKeyParameters.getOIDString(), mcEliecePublicKeyParameters.getN(), mcEliecePublicKeyParameters.getT(), mcEliecePublicKeyParameters.getG());
        this.McElieceParams = mcEliecePublicKeyParameters.getParameters();
    }
    
    public String getAlgorithm() {
        return "McEliece";
    }
    
    public int getN() {
        return this.n;
    }
    
    public int getK() {
        return this.g.getNumRows();
    }
    
    public int getT() {
        return this.t;
    }
    
    public GF2Matrix getG() {
        return this.g;
    }
    
    @Override
    public String toString() {
        return "McEliecePublicKey:\n" + " length of the code         : " + this.n + "\n" + " error correction capability: " + this.t + "\n" + " generator matrix           : " + this.g.toString();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof BCMcEliecePublicKey)) {
            return false;
        }
        final BCMcEliecePublicKey bcMcEliecePublicKey = (BCMcEliecePublicKey)o;
        return this.n == bcMcEliecePublicKey.n && this.t == bcMcEliecePublicKey.t && this.g.equals(bcMcEliecePublicKey.g);
    }
    
    @Override
    public int hashCode() {
        return this.n + this.t + this.g.hashCode();
    }
    
    public String getOIDString() {
        return this.oid;
    }
    
    protected ASN1ObjectIdentifier getOID() {
        return new ASN1ObjectIdentifier("1.3.6.1.4.1.8301.3.1.3.4.1");
    }
    
    protected ASN1Primitive getAlgParams() {
        return null;
    }
    
    public byte[] getEncoded() {
        final McEliecePublicKey mcEliecePublicKey = new McEliecePublicKey(new ASN1ObjectIdentifier(this.oid), this.n, this.t, this.g);
        final AlgorithmIdentifier algorithmIdentifier = new AlgorithmIdentifier(this.getOID(), DERNull.INSTANCE);
        try {
            return new SubjectPublicKeyInfo(algorithmIdentifier, mcEliecePublicKey).getEncoded();
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    public String getFormat() {
        return null;
    }
    
    public McElieceParameters getMcElieceParameters() {
        return this.McElieceParams;
    }
}
