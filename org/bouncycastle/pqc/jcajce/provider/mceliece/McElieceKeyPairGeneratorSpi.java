// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.mceliece;

import org.bouncycastle.pqc.crypto.mceliece.McElieceCCA2PrivateKeyParameters;
import org.bouncycastle.pqc.crypto.mceliece.McElieceCCA2PublicKeyParameters;
import org.bouncycastle.pqc.jcajce.spec.McElieceCCA2ParameterSpec;
import org.bouncycastle.pqc.crypto.mceliece.McElieceCCA2KeyGenerationParameters;
import org.bouncycastle.pqc.crypto.mceliece.McElieceCCA2Parameters;
import org.bouncycastle.pqc.crypto.mceliece.McElieceCCA2KeyPairGenerator;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import org.bouncycastle.pqc.crypto.mceliece.McEliecePrivateKeyParameters;
import org.bouncycastle.pqc.crypto.mceliece.McEliecePublicKeyParameters;
import java.security.KeyPair;
import java.security.InvalidAlgorithmParameterException;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.pqc.crypto.mceliece.McElieceKeyGenerationParameters;
import org.bouncycastle.pqc.crypto.mceliece.McElieceParameters;
import java.security.SecureRandom;
import org.bouncycastle.pqc.jcajce.spec.ECCKeyGenParameterSpec;
import java.security.spec.AlgorithmParameterSpec;
import org.bouncycastle.pqc.crypto.mceliece.McElieceKeyPairGenerator;
import java.security.KeyPairGenerator;

public abstract class McElieceKeyPairGeneratorSpi extends KeyPairGenerator
{
    public McElieceKeyPairGeneratorSpi(final String algorithm) {
        super(algorithm);
    }
    
    public static class McEliece extends McElieceKeyPairGeneratorSpi
    {
        McElieceKeyPairGenerator kpg;
        
        public McEliece() {
            super("McEliece");
        }
        
        @Override
        public void initialize(final AlgorithmParameterSpec params) throws InvalidAlgorithmParameterException {
            this.kpg = new McElieceKeyPairGenerator();
            super.initialize(params);
            final ECCKeyGenParameterSpec eccKeyGenParameterSpec = (ECCKeyGenParameterSpec)params;
            this.kpg.init(new McElieceKeyGenerationParameters(new SecureRandom(), new McElieceParameters(eccKeyGenParameterSpec.getM(), eccKeyGenParameterSpec.getT())));
        }
        
        @Override
        public void initialize(final int n, final SecureRandom secureRandom) {
            final ECCKeyGenParameterSpec eccKeyGenParameterSpec = new ECCKeyGenParameterSpec();
            try {
                this.initialize(eccKeyGenParameterSpec);
            }
            catch (InvalidAlgorithmParameterException ex) {}
        }
        
        @Override
        public KeyPair generateKeyPair() {
            final AsymmetricCipherKeyPair generateKeyPair = this.kpg.generateKeyPair();
            return new KeyPair(new BCMcEliecePublicKey((McEliecePublicKeyParameters)generateKeyPair.getPublic()), new BCMcEliecePrivateKey((McEliecePrivateKeyParameters)generateKeyPair.getPrivate()));
        }
    }
    
    public static class McElieceCCA2 extends McElieceKeyPairGeneratorSpi
    {
        McElieceCCA2KeyPairGenerator kpg;
        
        public McElieceCCA2() {
            super("McElieceCCA-2");
        }
        
        public McElieceCCA2(final String s) {
            super(s);
        }
        
        @Override
        public void initialize(final AlgorithmParameterSpec params) throws InvalidAlgorithmParameterException {
            this.kpg = new McElieceCCA2KeyPairGenerator();
            super.initialize(params);
            final ECCKeyGenParameterSpec eccKeyGenParameterSpec = (ECCKeyGenParameterSpec)params;
            this.kpg.init(new McElieceCCA2KeyGenerationParameters(new SecureRandom(), new McElieceCCA2Parameters(eccKeyGenParameterSpec.getM(), eccKeyGenParameterSpec.getT())));
        }
        
        @Override
        public void initialize(final int n, final SecureRandom secureRandom) {
            final McElieceCCA2ParameterSpec mcElieceCCA2ParameterSpec = new McElieceCCA2ParameterSpec();
            try {
                this.initialize(mcElieceCCA2ParameterSpec);
            }
            catch (InvalidAlgorithmParameterException ex) {}
        }
        
        @Override
        public KeyPair generateKeyPair() {
            final AsymmetricCipherKeyPair generateKeyPair = this.kpg.generateKeyPair();
            return new KeyPair(new BCMcElieceCCA2PublicKey((McElieceCCA2PublicKeyParameters)generateKeyPair.getPublic()), new BCMcElieceCCA2PrivateKey((McElieceCCA2PrivateKeyParameters)generateKeyPair.getPrivate()));
        }
    }
}
