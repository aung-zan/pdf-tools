// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.mceliece;

import java.io.IOException;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.pqc.asn1.McElieceCCA2PublicKey;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.pqc.crypto.mceliece.McElieceCCA2PublicKeyParameters;
import org.bouncycastle.pqc.jcajce.spec.McElieceCCA2PublicKeySpec;
import org.bouncycastle.pqc.crypto.mceliece.McElieceCCA2Parameters;
import org.bouncycastle.pqc.math.linearalgebra.GF2Matrix;
import java.security.PublicKey;
import org.bouncycastle.crypto.CipherParameters;

public class BCMcElieceCCA2PublicKey implements CipherParameters, PublicKey
{
    private static final long serialVersionUID = 1L;
    private String oid;
    private int n;
    private int t;
    private GF2Matrix g;
    private McElieceCCA2Parameters McElieceCCA2Params;
    
    public BCMcElieceCCA2PublicKey(final String oid, final int n, final int t, final GF2Matrix g) {
        this.oid = oid;
        this.n = n;
        this.t = t;
        this.g = g;
    }
    
    public BCMcElieceCCA2PublicKey(final McElieceCCA2PublicKeySpec mcElieceCCA2PublicKeySpec) {
        this(mcElieceCCA2PublicKeySpec.getOIDString(), mcElieceCCA2PublicKeySpec.getN(), mcElieceCCA2PublicKeySpec.getT(), mcElieceCCA2PublicKeySpec.getMatrixG());
    }
    
    public BCMcElieceCCA2PublicKey(final McElieceCCA2PublicKeyParameters mcElieceCCA2PublicKeyParameters) {
        this(mcElieceCCA2PublicKeyParameters.getOIDString(), mcElieceCCA2PublicKeyParameters.getN(), mcElieceCCA2PublicKeyParameters.getT(), mcElieceCCA2PublicKeyParameters.getMatrixG());
        this.McElieceCCA2Params = mcElieceCCA2PublicKeyParameters.getParameters();
    }
    
    public String getAlgorithm() {
        return "McEliece";
    }
    
    public int getN() {
        return this.n;
    }
    
    public int getK() {
        return this.g.getNumRows();
    }
    
    public int getT() {
        return this.t;
    }
    
    public GF2Matrix getG() {
        return this.g;
    }
    
    @Override
    public String toString() {
        return "McEliecePublicKey:\n" + " length of the code         : " + this.n + "\n" + " error correction capability: " + this.t + "\n" + " generator matrix           : " + this.g.toString();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null || !(o instanceof BCMcElieceCCA2PublicKey)) {
            return false;
        }
        final BCMcElieceCCA2PublicKey bcMcElieceCCA2PublicKey = (BCMcElieceCCA2PublicKey)o;
        return this.n == bcMcElieceCCA2PublicKey.n && this.t == bcMcElieceCCA2PublicKey.t && this.g.equals(bcMcElieceCCA2PublicKey.g);
    }
    
    @Override
    public int hashCode() {
        return this.n + this.t + this.g.hashCode();
    }
    
    public String getOIDString() {
        return this.oid;
    }
    
    protected ASN1ObjectIdentifier getOID() {
        return new ASN1ObjectIdentifier("1.3.6.1.4.1.8301.3.1.3.4.2");
    }
    
    protected ASN1Primitive getAlgParams() {
        return null;
    }
    
    public byte[] getEncoded() {
        final McElieceCCA2PublicKey mcElieceCCA2PublicKey = new McElieceCCA2PublicKey(new ASN1ObjectIdentifier(this.oid), this.n, this.t, this.g);
        final AlgorithmIdentifier algorithmIdentifier = new AlgorithmIdentifier(this.getOID(), DERNull.INSTANCE);
        try {
            return new SubjectPublicKeyInfo(algorithmIdentifier, mcElieceCCA2PublicKey).getEncoded();
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    public String getFormat() {
        return null;
    }
    
    public McElieceCCA2Parameters getMcElieceCCA2Parameters() {
        return this.McElieceCCA2Params;
    }
}
