// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.spec;

import org.bouncycastle.pqc.math.linearalgebra.GF2Matrix;
import java.security.spec.KeySpec;

public class McEliecePublicKeySpec implements KeySpec
{
    private String oid;
    private int n;
    private int t;
    private GF2Matrix g;
    
    public McEliecePublicKeySpec(final String oid, final int n, final int t, final GF2Matrix gf2Matrix) {
        this.oid = oid;
        this.n = n;
        this.t = t;
        this.g = new GF2Matrix(gf2Matrix);
    }
    
    public McEliecePublicKeySpec(final String oid, final int t, final int n, final byte[] array) {
        this.oid = oid;
        this.n = n;
        this.t = t;
        this.g = new GF2Matrix(array);
    }
    
    public int getN() {
        return this.n;
    }
    
    public int getT() {
        return this.t;
    }
    
    public GF2Matrix getG() {
        return this.g;
    }
    
    public String getOIDString() {
        return this.oid;
    }
}
