// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.spec;

import org.bouncycastle.pqc.crypto.gmss.GMSSParameters;

public class GMSSPublicKeySpec extends GMSSKeySpec
{
    private byte[] gmssPublicKey;
    
    public GMSSPublicKeySpec(final byte[] gmssPublicKey, final GMSSParameters gmssParameters) {
        super(gmssParameters);
        this.gmssPublicKey = gmssPublicKey;
    }
    
    public byte[] getPublicKey() {
        return this.gmssPublicKey;
    }
}
