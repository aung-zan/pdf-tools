// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.spec;

import java.util.Collection;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.pqc.crypto.gmss.GMSSRootSig;
import org.bouncycastle.pqc.crypto.gmss.GMSSRootCalc;
import org.bouncycastle.pqc.crypto.gmss.GMSSParameters;
import org.bouncycastle.pqc.crypto.gmss.GMSSLeaf;
import java.util.Vector;
import org.bouncycastle.pqc.crypto.gmss.Treehash;
import java.security.spec.KeySpec;

public class GMSSPrivateKeySpec implements KeySpec
{
    private int[] index;
    private byte[][] currentSeed;
    private byte[][] nextNextSeed;
    private byte[][][] currentAuthPath;
    private byte[][][] nextAuthPath;
    private Treehash[][] currentTreehash;
    private Treehash[][] nextTreehash;
    private Vector[] currentStack;
    private Vector[] nextStack;
    private Vector[][] currentRetain;
    private Vector[][] nextRetain;
    private byte[][][] keep;
    private GMSSLeaf[] nextNextLeaf;
    private GMSSLeaf[] upperLeaf;
    private GMSSLeaf[] upperTreehashLeaf;
    private int[] minTreehash;
    private GMSSParameters gmssPS;
    private byte[][] nextRoot;
    private GMSSRootCalc[] nextNextRoot;
    private byte[][] currentRootSig;
    private GMSSRootSig[] nextRootSig;
    
    public GMSSPrivateKeySpec(final int[] index, final byte[][] currentSeed, final byte[][] nextNextSeed, final byte[][][] currentAuthPath, final byte[][][] nextAuthPath, final Treehash[][] currentTreehash, final Treehash[][] nextTreehash, final Vector[] currentStack, final Vector[] nextStack, final Vector[][] currentRetain, final Vector[][] nextRetain, final byte[][][] keep, final GMSSLeaf[] nextNextLeaf, final GMSSLeaf[] upperLeaf, final GMSSLeaf[] upperTreehashLeaf, final int[] minTreehash, final byte[][] nextRoot, final GMSSRootCalc[] nextNextRoot, final byte[][] currentRootSig, final GMSSRootSig[] nextRootSig, final GMSSParameters gmssPS) {
        this.index = index;
        this.currentSeed = currentSeed;
        this.nextNextSeed = nextNextSeed;
        this.currentAuthPath = currentAuthPath;
        this.nextAuthPath = nextAuthPath;
        this.currentTreehash = currentTreehash;
        this.nextTreehash = nextTreehash;
        this.currentStack = currentStack;
        this.nextStack = nextStack;
        this.currentRetain = currentRetain;
        this.nextRetain = nextRetain;
        this.keep = keep;
        this.nextNextLeaf = nextNextLeaf;
        this.upperLeaf = upperLeaf;
        this.upperTreehashLeaf = upperTreehashLeaf;
        this.minTreehash = minTreehash;
        this.nextRoot = nextRoot;
        this.nextNextRoot = nextNextRoot;
        this.currentRootSig = currentRootSig;
        this.nextRootSig = nextRootSig;
        this.gmssPS = gmssPS;
    }
    
    public int[] getIndex() {
        return Arrays.clone(this.index);
    }
    
    public byte[][] getCurrentSeed() {
        return clone(this.currentSeed);
    }
    
    public byte[][] getNextNextSeed() {
        return clone(this.nextNextSeed);
    }
    
    public byte[][][] getCurrentAuthPath() {
        return clone(this.currentAuthPath);
    }
    
    public byte[][][] getNextAuthPath() {
        return clone(this.nextAuthPath);
    }
    
    public Treehash[][] getCurrentTreehash() {
        return clone(this.currentTreehash);
    }
    
    public Treehash[][] getNextTreehash() {
        return clone(this.nextTreehash);
    }
    
    public byte[][][] getKeep() {
        return clone(this.keep);
    }
    
    public Vector[] getCurrentStack() {
        return clone(this.currentStack);
    }
    
    public Vector[] getNextStack() {
        return clone(this.nextStack);
    }
    
    public Vector[][] getCurrentRetain() {
        return clone(this.currentRetain);
    }
    
    public Vector[][] getNextRetain() {
        return clone(this.nextRetain);
    }
    
    public GMSSLeaf[] getNextNextLeaf() {
        return clone(this.nextNextLeaf);
    }
    
    public GMSSLeaf[] getUpperLeaf() {
        return clone(this.upperLeaf);
    }
    
    public GMSSLeaf[] getUpperTreehashLeaf() {
        return clone(this.upperTreehashLeaf);
    }
    
    public int[] getMinTreehash() {
        return Arrays.clone(this.minTreehash);
    }
    
    public GMSSRootSig[] getNextRootSig() {
        return clone(this.nextRootSig);
    }
    
    public GMSSParameters getGmssPS() {
        return this.gmssPS;
    }
    
    public byte[][] getNextRoot() {
        return clone(this.nextRoot);
    }
    
    public GMSSRootCalc[] getNextNextRoot() {
        return clone(this.nextNextRoot);
    }
    
    public byte[][] getCurrentRootSig() {
        return clone(this.currentRootSig);
    }
    
    private static GMSSLeaf[] clone(final GMSSLeaf[] array) {
        if (array == null) {
            return null;
        }
        final GMSSLeaf[] array2 = new GMSSLeaf[array.length];
        System.arraycopy(array, 0, array2, 0, array.length);
        return array2;
    }
    
    private static GMSSRootCalc[] clone(final GMSSRootCalc[] array) {
        if (array == null) {
            return null;
        }
        final GMSSRootCalc[] array2 = new GMSSRootCalc[array.length];
        System.arraycopy(array, 0, array2, 0, array.length);
        return array2;
    }
    
    private static GMSSRootSig[] clone(final GMSSRootSig[] array) {
        if (array == null) {
            return null;
        }
        final GMSSRootSig[] array2 = new GMSSRootSig[array.length];
        System.arraycopy(array, 0, array2, 0, array.length);
        return array2;
    }
    
    private static byte[][] clone(final byte[][] array) {
        if (array == null) {
            return null;
        }
        final byte[][] array2 = new byte[array.length][];
        for (int i = 0; i != array.length; ++i) {
            array2[i] = Arrays.clone(array[i]);
        }
        return array2;
    }
    
    private static byte[][][] clone(final byte[][][] array) {
        if (array == null) {
            return null;
        }
        final byte[][][] array2 = new byte[array.length][][];
        for (int i = 0; i != array.length; ++i) {
            array2[i] = clone(array[i]);
        }
        return array2;
    }
    
    private static Treehash[] clone(final Treehash[] array) {
        if (array == null) {
            return null;
        }
        final Treehash[] array2 = new Treehash[array.length];
        System.arraycopy(array, 0, array2, 0, array.length);
        return array2;
    }
    
    private static Treehash[][] clone(final Treehash[][] array) {
        if (array == null) {
            return null;
        }
        final Treehash[][] array2 = new Treehash[array.length][];
        for (int i = 0; i != array.length; ++i) {
            array2[i] = clone(array[i]);
        }
        return array2;
    }
    
    private static Vector[] clone(final Vector[] array) {
        if (array == null) {
            return null;
        }
        final Vector[] array2 = new Vector[array.length];
        for (int i = 0; i != array.length; ++i) {
            array2[i] = new Vector(array[i]);
        }
        return array2;
    }
    
    private static Vector[][] clone(final Vector[][] array) {
        if (array == null) {
            return null;
        }
        final Vector[][] array2 = new Vector[array.length][];
        for (int i = 0; i != array.length; ++i) {
            array2[i] = clone(array[i]);
        }
        return array2;
    }
}
