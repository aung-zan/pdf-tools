// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.spec;

import java.security.spec.AlgorithmParameterSpec;

public class McElieceCCA2ParameterSpec implements AlgorithmParameterSpec
{
    public static final String DEFAULT_MD = "SHA256";
    private String mdName;
    
    public McElieceCCA2ParameterSpec() {
        this("SHA256");
    }
    
    public McElieceCCA2ParameterSpec(final String mdName) {
        this.mdName = mdName;
    }
    
    public String getMDName() {
        return this.mdName;
    }
}
