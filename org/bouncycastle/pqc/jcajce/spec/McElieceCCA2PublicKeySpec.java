// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.spec;

import org.bouncycastle.pqc.math.linearalgebra.GF2Matrix;
import java.security.spec.KeySpec;

public class McElieceCCA2PublicKeySpec implements KeySpec
{
    private String oid;
    private int n;
    private int t;
    private GF2Matrix matrixG;
    
    public McElieceCCA2PublicKeySpec(final String oid, final int n, final int t, final GF2Matrix gf2Matrix) {
        this.oid = oid;
        this.n = n;
        this.t = t;
        this.matrixG = new GF2Matrix(gf2Matrix);
    }
    
    public McElieceCCA2PublicKeySpec(final String oid, final int n, final int t, final byte[] array) {
        this.oid = oid;
        this.n = n;
        this.t = t;
        this.matrixG = new GF2Matrix(array);
    }
    
    public int getN() {
        return this.n;
    }
    
    public int getT() {
        return this.t;
    }
    
    public GF2Matrix getMatrixG() {
        return this.matrixG;
    }
    
    public String getOIDString() {
        return this.oid;
    }
}
