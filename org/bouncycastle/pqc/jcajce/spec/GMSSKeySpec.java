// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.spec;

import org.bouncycastle.pqc.crypto.gmss.GMSSParameters;
import java.security.spec.KeySpec;

public class GMSSKeySpec implements KeySpec
{
    private GMSSParameters gmssParameterSet;
    
    protected GMSSKeySpec(final GMSSParameters gmssParameterSet) {
        this.gmssParameterSet = gmssParameterSet;
    }
    
    public GMSSParameters getParameters() {
        return this.gmssParameterSet;
    }
}
