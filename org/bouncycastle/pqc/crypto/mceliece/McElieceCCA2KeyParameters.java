// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.mceliece;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class McElieceCCA2KeyParameters extends AsymmetricKeyParameter
{
    private McElieceCCA2Parameters params;
    
    public McElieceCCA2KeyParameters(final boolean b, final McElieceCCA2Parameters params) {
        super(b);
        this.params = params;
    }
    
    public McElieceCCA2Parameters getParameters() {
        return this.params;
    }
}
