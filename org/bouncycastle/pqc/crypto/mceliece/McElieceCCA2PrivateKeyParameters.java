// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.mceliece;

import org.bouncycastle.pqc.math.linearalgebra.GF2Matrix;
import org.bouncycastle.pqc.math.linearalgebra.Permutation;
import org.bouncycastle.pqc.math.linearalgebra.PolynomialGF2mSmallM;
import org.bouncycastle.pqc.math.linearalgebra.GF2mField;

public class McElieceCCA2PrivateKeyParameters extends McElieceCCA2KeyParameters
{
    private String oid;
    private int n;
    private int k;
    private GF2mField field;
    private PolynomialGF2mSmallM goppaPoly;
    private Permutation p;
    private GF2Matrix h;
    private PolynomialGF2mSmallM[] qInv;
    
    public McElieceCCA2PrivateKeyParameters(final String oid, final int n, final int k, final GF2mField field, final PolynomialGF2mSmallM goppaPoly, final Permutation p9, final GF2Matrix h, final PolynomialGF2mSmallM[] qInv, final McElieceCCA2Parameters mcElieceCCA2Parameters) {
        super(true, mcElieceCCA2Parameters);
        this.oid = oid;
        this.n = n;
        this.k = k;
        this.field = field;
        this.goppaPoly = goppaPoly;
        this.p = p9;
        this.h = h;
        this.qInv = qInv;
    }
    
    public McElieceCCA2PrivateKeyParameters(final String oid, final int n, final int k, final byte[] array, final byte[] array2, final byte[] array3, final byte[] array4, final byte[][] array5, final McElieceCCA2Parameters mcElieceCCA2Parameters) {
        super(true, mcElieceCCA2Parameters);
        this.oid = oid;
        this.n = n;
        this.k = k;
        this.field = new GF2mField(array);
        this.goppaPoly = new PolynomialGF2mSmallM(this.field, array2);
        this.p = new Permutation(array3);
        this.h = new GF2Matrix(array4);
        this.qInv = new PolynomialGF2mSmallM[array5.length];
        for (int i = 0; i < array5.length; ++i) {
            this.qInv[i] = new PolynomialGF2mSmallM(this.field, array5[i]);
        }
    }
    
    public int getN() {
        return this.n;
    }
    
    public int getK() {
        return this.k;
    }
    
    public int getT() {
        return this.goppaPoly.getDegree();
    }
    
    public GF2mField getField() {
        return this.field;
    }
    
    public PolynomialGF2mSmallM getGoppaPoly() {
        return this.goppaPoly;
    }
    
    public Permutation getP() {
        return this.p;
    }
    
    public GF2Matrix getH() {
        return this.h;
    }
    
    public PolynomialGF2mSmallM[] getQInv() {
        return this.qInv;
    }
    
    public String getOIDString() {
        return this.oid;
    }
}
