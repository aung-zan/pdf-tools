// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.mceliece;

import org.bouncycastle.pqc.math.linearalgebra.GF2Matrix;

public class McEliecePublicKeyParameters extends McElieceKeyParameters
{
    private String oid;
    private int n;
    private int t;
    private GF2Matrix g;
    
    public McEliecePublicKeyParameters(final String oid, final int n, final int t, final GF2Matrix gf2Matrix, final McElieceParameters mcElieceParameters) {
        super(false, mcElieceParameters);
        this.oid = oid;
        this.n = n;
        this.t = t;
        this.g = new GF2Matrix(gf2Matrix);
    }
    
    public McEliecePublicKeyParameters(final String oid, final int t, final int n, final byte[] array, final McElieceParameters mcElieceParameters) {
        super(false, mcElieceParameters);
        this.oid = oid;
        this.n = n;
        this.t = t;
        this.g = new GF2Matrix(array);
    }
    
    public int getN() {
        return this.n;
    }
    
    public int getT() {
        return this.t;
    }
    
    public GF2Matrix getG() {
        return this.g;
    }
    
    public String getOIDString() {
        return this.oid;
    }
    
    public int getK() {
        return this.g.getNumRows();
    }
}
