// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.mceliece;

import org.bouncycastle.pqc.math.linearalgebra.GF2Matrix;

public class McElieceCCA2PublicKeyParameters extends McElieceCCA2KeyParameters
{
    private String oid;
    private int n;
    private int t;
    private GF2Matrix matrixG;
    
    public McElieceCCA2PublicKeyParameters(final String oid, final int n, final int t, final GF2Matrix gf2Matrix, final McElieceCCA2Parameters mcElieceCCA2Parameters) {
        super(false, mcElieceCCA2Parameters);
        this.oid = oid;
        this.n = n;
        this.t = t;
        this.matrixG = new GF2Matrix(gf2Matrix);
    }
    
    public McElieceCCA2PublicKeyParameters(final String oid, final int n, final int t, final byte[] array, final McElieceCCA2Parameters mcElieceCCA2Parameters) {
        super(false, mcElieceCCA2Parameters);
        this.oid = oid;
        this.n = n;
        this.t = t;
        this.matrixG = new GF2Matrix(array);
    }
    
    public int getN() {
        return this.n;
    }
    
    public int getT() {
        return this.t;
    }
    
    public GF2Matrix getMatrixG() {
        return this.matrixG;
    }
    
    public int getK() {
        return this.matrixG.getNumRows();
    }
    
    public String getOIDString() {
        return this.oid;
    }
}
