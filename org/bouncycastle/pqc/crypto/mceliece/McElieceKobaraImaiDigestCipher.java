// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.mceliece;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.pqc.crypto.MessageEncryptor;
import org.bouncycastle.crypto.Digest;

public class McElieceKobaraImaiDigestCipher
{
    private final Digest messDigest;
    private final MessageEncryptor mcElieceCCA2Cipher;
    private boolean forEncrypting;
    
    public McElieceKobaraImaiDigestCipher(final MessageEncryptor mcElieceCCA2Cipher, final Digest messDigest) {
        this.mcElieceCCA2Cipher = mcElieceCCA2Cipher;
        this.messDigest = messDigest;
    }
    
    public void init(final boolean forEncrypting, final CipherParameters cipherParameters) {
        this.forEncrypting = forEncrypting;
        AsymmetricKeyParameter asymmetricKeyParameter;
        if (cipherParameters instanceof ParametersWithRandom) {
            asymmetricKeyParameter = (AsymmetricKeyParameter)((ParametersWithRandom)cipherParameters).getParameters();
        }
        else {
            asymmetricKeyParameter = (AsymmetricKeyParameter)cipherParameters;
        }
        if (forEncrypting && asymmetricKeyParameter.isPrivate()) {
            throw new IllegalArgumentException("Encrypting Requires Public Key.");
        }
        if (!forEncrypting && !asymmetricKeyParameter.isPrivate()) {
            throw new IllegalArgumentException("Decrypting Requires Private Key.");
        }
        this.reset();
        this.mcElieceCCA2Cipher.init(forEncrypting, cipherParameters);
    }
    
    public byte[] messageEncrypt() {
        if (!this.forEncrypting) {
            throw new IllegalStateException("McElieceKobaraImaiDigestCipher not initialised for encrypting.");
        }
        final byte[] array = new byte[this.messDigest.getDigestSize()];
        this.messDigest.doFinal(array, 0);
        byte[] messageEncrypt = null;
        try {
            messageEncrypt = this.mcElieceCCA2Cipher.messageEncrypt(array);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return messageEncrypt;
    }
    
    public byte[] messageDecrypt(final byte[] array) {
        byte[] messageDecrypt = null;
        if (this.forEncrypting) {
            throw new IllegalStateException("McElieceKobaraImaiDigestCipher not initialised for decrypting.");
        }
        try {
            messageDecrypt = this.mcElieceCCA2Cipher.messageDecrypt(array);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return messageDecrypt;
    }
    
    public void update(final byte b) {
        this.messDigest.update(b);
    }
    
    public void update(final byte[] array, final int n, final int n2) {
        this.messDigest.update(array, n, n2);
    }
    
    public void reset() {
        this.messDigest.reset();
    }
}
