// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.mceliece;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.pqc.crypto.MessageEncryptor;
import org.bouncycastle.crypto.Digest;

public class McEliecePKCSDigestCipher
{
    private final Digest messDigest;
    private final MessageEncryptor mcElieceCipher;
    private boolean forEncrypting;
    
    public McEliecePKCSDigestCipher(final MessageEncryptor mcElieceCipher, final Digest messDigest) {
        this.mcElieceCipher = mcElieceCipher;
        this.messDigest = messDigest;
    }
    
    public void init(final boolean forEncrypting, final CipherParameters cipherParameters) {
        this.forEncrypting = forEncrypting;
        AsymmetricKeyParameter asymmetricKeyParameter;
        if (cipherParameters instanceof ParametersWithRandom) {
            asymmetricKeyParameter = (AsymmetricKeyParameter)((ParametersWithRandom)cipherParameters).getParameters();
        }
        else {
            asymmetricKeyParameter = (AsymmetricKeyParameter)cipherParameters;
        }
        if (forEncrypting && asymmetricKeyParameter.isPrivate()) {
            throw new IllegalArgumentException("Encrypting Requires Public Key.");
        }
        if (!forEncrypting && !asymmetricKeyParameter.isPrivate()) {
            throw new IllegalArgumentException("Decrypting Requires Private Key.");
        }
        this.reset();
        this.mcElieceCipher.init(forEncrypting, cipherParameters);
    }
    
    public byte[] messageEncrypt() {
        if (!this.forEncrypting) {
            throw new IllegalStateException("McEliecePKCSDigestCipher not initialised for encrypting.");
        }
        final byte[] array = new byte[this.messDigest.getDigestSize()];
        this.messDigest.doFinal(array, 0);
        byte[] messageEncrypt = null;
        try {
            messageEncrypt = this.mcElieceCipher.messageEncrypt(array);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return messageEncrypt;
    }
    
    public byte[] messageDecrypt(final byte[] array) {
        byte[] messageDecrypt = null;
        if (this.forEncrypting) {
            throw new IllegalStateException("McEliecePKCSDigestCipher not initialised for decrypting.");
        }
        try {
            messageDecrypt = this.mcElieceCipher.messageDecrypt(array);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return messageDecrypt;
    }
    
    public void update(final byte b) {
        this.messDigest.update(b);
    }
    
    public void update(final byte[] array, final int n, final int n2) {
        this.messDigest.update(array, n, n2);
    }
    
    public void reset() {
        this.messDigest.reset();
    }
}
