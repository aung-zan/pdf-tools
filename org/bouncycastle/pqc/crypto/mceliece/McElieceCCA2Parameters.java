// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.mceliece;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.Digest;

public class McElieceCCA2Parameters extends McElieceParameters
{
    public Digest digest;
    
    public McElieceCCA2Parameters() {
        this.digest = new SHA256Digest();
    }
    
    public McElieceCCA2Parameters(final int n, final int n2) {
        super(n, n2);
        this.digest = new SHA256Digest();
    }
    
    public McElieceCCA2Parameters(final Digest digest) {
        this.digest = digest;
    }
    
    public Digest getDigest() {
        return this.digest;
    }
}
