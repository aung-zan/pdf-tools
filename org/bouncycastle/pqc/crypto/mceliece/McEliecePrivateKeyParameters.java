// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.mceliece;

import org.bouncycastle.pqc.math.linearalgebra.Permutation;
import org.bouncycastle.pqc.math.linearalgebra.GF2Matrix;
import org.bouncycastle.pqc.math.linearalgebra.PolynomialGF2mSmallM;
import org.bouncycastle.pqc.math.linearalgebra.GF2mField;

public class McEliecePrivateKeyParameters extends McElieceKeyParameters
{
    private String oid;
    private int n;
    private int k;
    private GF2mField field;
    private PolynomialGF2mSmallM goppaPoly;
    private GF2Matrix sInv;
    private Permutation p1;
    private Permutation p2;
    private GF2Matrix h;
    private PolynomialGF2mSmallM[] qInv;
    
    public McEliecePrivateKeyParameters(final String oid, final int n, final int k, final GF2mField field, final PolynomialGF2mSmallM goppaPoly, final GF2Matrix sInv, final Permutation p11, final Permutation p12, final GF2Matrix h, final PolynomialGF2mSmallM[] qInv, final McElieceParameters mcElieceParameters) {
        super(true, mcElieceParameters);
        this.oid = oid;
        this.k = k;
        this.n = n;
        this.field = field;
        this.goppaPoly = goppaPoly;
        this.sInv = sInv;
        this.p1 = p11;
        this.p2 = p12;
        this.h = h;
        this.qInv = qInv;
    }
    
    public McEliecePrivateKeyParameters(final String oid, final int n, final int k, final byte[] array, final byte[] array2, final byte[] array3, final byte[] array4, final byte[] array5, final byte[] array6, final byte[][] array7, final McElieceParameters mcElieceParameters) {
        super(true, mcElieceParameters);
        this.oid = oid;
        this.n = n;
        this.k = k;
        this.field = new GF2mField(array);
        this.goppaPoly = new PolynomialGF2mSmallM(this.field, array2);
        this.sInv = new GF2Matrix(array3);
        this.p1 = new Permutation(array4);
        this.p2 = new Permutation(array5);
        this.h = new GF2Matrix(array6);
        this.qInv = new PolynomialGF2mSmallM[array7.length];
        for (int i = 0; i < array7.length; ++i) {
            this.qInv[i] = new PolynomialGF2mSmallM(this.field, array7[i]);
        }
    }
    
    public int getN() {
        return this.n;
    }
    
    public int getK() {
        return this.k;
    }
    
    public GF2mField getField() {
        return this.field;
    }
    
    public PolynomialGF2mSmallM getGoppaPoly() {
        return this.goppaPoly;
    }
    
    public GF2Matrix getSInv() {
        return this.sInv;
    }
    
    public Permutation getP1() {
        return this.p1;
    }
    
    public Permutation getP2() {
        return this.p2;
    }
    
    public GF2Matrix getH() {
        return this.h;
    }
    
    public PolynomialGF2mSmallM[] getQInv() {
        return this.qInv;
    }
    
    public String getOIDString() {
        return this.oid;
    }
}
