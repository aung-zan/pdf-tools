// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.gmss;

import org.bouncycastle.crypto.Digest;

public interface GMSSDigestProvider
{
    Digest get();
}
