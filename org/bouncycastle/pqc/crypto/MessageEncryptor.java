// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto;

import org.bouncycastle.crypto.CipherParameters;

public interface MessageEncryptor
{
    void init(final boolean p0, final CipherParameters p1);
    
    byte[] messageEncrypt(final byte[] p0) throws Exception;
    
    byte[] messageDecrypt(final byte[] p0) throws Exception;
}
