// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.asn1;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public interface PQCObjectIdentifiers
{
    public static final ASN1ObjectIdentifier rainbow = new ASN1ObjectIdentifier("1.3.6.1.4.1.8301.3.1.3.5.3.2");
    public static final ASN1ObjectIdentifier rainbowWithSha1 = PQCObjectIdentifiers.rainbow.branch("1");
    public static final ASN1ObjectIdentifier rainbowWithSha224 = PQCObjectIdentifiers.rainbow.branch("2");
    public static final ASN1ObjectIdentifier rainbowWithSha256 = PQCObjectIdentifiers.rainbow.branch("3");
    public static final ASN1ObjectIdentifier rainbowWithSha384 = PQCObjectIdentifiers.rainbow.branch("4");
    public static final ASN1ObjectIdentifier rainbowWithSha512 = PQCObjectIdentifiers.rainbow.branch("5");
    public static final ASN1ObjectIdentifier gmss = new ASN1ObjectIdentifier("1.3.6.1.4.1.8301.3.1.3.3");
    public static final ASN1ObjectIdentifier gmssWithSha1 = PQCObjectIdentifiers.gmss.branch("1");
    public static final ASN1ObjectIdentifier gmssWithSha224 = PQCObjectIdentifiers.gmss.branch("2");
    public static final ASN1ObjectIdentifier gmssWithSha256 = PQCObjectIdentifiers.gmss.branch("3");
    public static final ASN1ObjectIdentifier gmssWithSha384 = PQCObjectIdentifiers.gmss.branch("4");
    public static final ASN1ObjectIdentifier gmssWithSha512 = PQCObjectIdentifiers.gmss.branch("5");
    public static final ASN1ObjectIdentifier mcEliece = new ASN1ObjectIdentifier("1.3.6.1.4.1.8301.3.1.3.4.1");
    public static final ASN1ObjectIdentifier mcElieceCca2 = new ASN1ObjectIdentifier("1.3.6.1.4.1.8301.3.1.3.4.2");
}
