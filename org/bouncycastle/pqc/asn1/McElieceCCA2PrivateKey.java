// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.asn1;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.pqc.math.linearalgebra.GF2Matrix;
import org.bouncycastle.pqc.math.linearalgebra.Permutation;
import org.bouncycastle.pqc.math.linearalgebra.PolynomialGF2mSmallM;
import org.bouncycastle.pqc.math.linearalgebra.GF2mField;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Object;

public class McElieceCCA2PrivateKey extends ASN1Object
{
    private ASN1ObjectIdentifier oid;
    private int n;
    private int k;
    private byte[] encField;
    private byte[] encGp;
    private byte[] encP;
    private byte[] encH;
    private byte[][] encqInv;
    
    public McElieceCCA2PrivateKey(final ASN1ObjectIdentifier oid, final int n, final int k, final GF2mField gf2mField, final PolynomialGF2mSmallM polynomialGF2mSmallM, final Permutation permutation, final GF2Matrix gf2Matrix, final PolynomialGF2mSmallM[] array) {
        this.oid = oid;
        this.n = n;
        this.k = k;
        this.encField = gf2mField.getEncoded();
        this.encGp = polynomialGF2mSmallM.getEncoded();
        this.encP = permutation.getEncoded();
        this.encH = gf2Matrix.getEncoded();
        this.encqInv = new byte[array.length][];
        for (int i = 0; i != array.length; ++i) {
            this.encqInv[i] = array[i].getEncoded();
        }
    }
    
    private McElieceCCA2PrivateKey(final ASN1Sequence asn1Sequence) {
        this.oid = (ASN1ObjectIdentifier)asn1Sequence.getObjectAt(0);
        this.n = ((ASN1Integer)asn1Sequence.getObjectAt(1)).getValue().intValue();
        this.k = ((ASN1Integer)asn1Sequence.getObjectAt(2)).getValue().intValue();
        this.encField = ((ASN1OctetString)asn1Sequence.getObjectAt(3)).getOctets();
        this.encGp = ((ASN1OctetString)asn1Sequence.getObjectAt(4)).getOctets();
        this.encP = ((ASN1OctetString)asn1Sequence.getObjectAt(5)).getOctets();
        this.encH = ((ASN1OctetString)asn1Sequence.getObjectAt(6)).getOctets();
        final ASN1Sequence asn1Sequence2 = (ASN1Sequence)asn1Sequence.getObjectAt(7);
        this.encqInv = new byte[asn1Sequence2.size()][];
        for (int i = 0; i < asn1Sequence2.size(); ++i) {
            this.encqInv[i] = ((ASN1OctetString)asn1Sequence2.getObjectAt(i)).getOctets();
        }
    }
    
    public ASN1ObjectIdentifier getOID() {
        return this.oid;
    }
    
    public int getN() {
        return this.n;
    }
    
    public int getK() {
        return this.k;
    }
    
    public GF2mField getField() {
        return new GF2mField(this.encField);
    }
    
    public PolynomialGF2mSmallM getGoppaPoly() {
        return new PolynomialGF2mSmallM(this.getField(), this.encGp);
    }
    
    public Permutation getP() {
        return new Permutation(this.encP);
    }
    
    public GF2Matrix getH() {
        return new GF2Matrix(this.encH);
    }
    
    public PolynomialGF2mSmallM[] getQInv() {
        final PolynomialGF2mSmallM[] array = new PolynomialGF2mSmallM[this.encqInv.length];
        final GF2mField field = this.getField();
        for (int i = 0; i < this.encqInv.length; ++i) {
            array[i] = new PolynomialGF2mSmallM(field, this.encqInv[i]);
        }
        return array;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        asn1EncodableVector.add(this.oid);
        asn1EncodableVector.add(new ASN1Integer(this.n));
        asn1EncodableVector.add(new ASN1Integer(this.k));
        asn1EncodableVector.add(new DEROctetString(this.encField));
        asn1EncodableVector.add(new DEROctetString(this.encGp));
        asn1EncodableVector.add(new DEROctetString(this.encP));
        asn1EncodableVector.add(new DEROctetString(this.encH));
        final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
        for (int i = 0; i < this.encqInv.length; ++i) {
            asn1EncodableVector2.add(new DEROctetString(this.encqInv[i]));
        }
        asn1EncodableVector.add(new DERSequence(asn1EncodableVector2));
        return new DERSequence(asn1EncodableVector);
    }
    
    public static McElieceCCA2PrivateKey getInstance(final Object o) {
        if (o instanceof McElieceCCA2PrivateKey) {
            return (McElieceCCA2PrivateKey)o;
        }
        if (o != null) {
            return new McElieceCCA2PrivateKey(ASN1Sequence.getInstance(o));
        }
        return null;
    }
}
