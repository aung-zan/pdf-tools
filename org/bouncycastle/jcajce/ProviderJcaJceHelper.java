// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce;

import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.Signature;
import java.security.MessageDigest;
import java.security.KeyPairGenerator;
import javax.crypto.SecretKeyFactory;
import java.security.KeyFactory;
import javax.crypto.KeyGenerator;
import java.security.AlgorithmParameters;
import java.security.AlgorithmParameterGenerator;
import javax.crypto.KeyAgreement;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import java.security.Provider;

public class ProviderJcaJceHelper implements JcaJceHelper
{
    protected final Provider provider;
    
    public ProviderJcaJceHelper(final Provider provider) {
        this.provider = provider;
    }
    
    public Cipher createCipher(final String transformation) throws NoSuchAlgorithmException, NoSuchPaddingException {
        return Cipher.getInstance(transformation, this.provider);
    }
    
    public Mac createMac(final String algorithm) throws NoSuchAlgorithmException {
        return Mac.getInstance(algorithm, this.provider);
    }
    
    public KeyAgreement createKeyAgreement(final String algorithm) throws NoSuchAlgorithmException {
        return KeyAgreement.getInstance(algorithm, this.provider);
    }
    
    public AlgorithmParameterGenerator createAlgorithmParameterGenerator(final String algorithm) throws NoSuchAlgorithmException {
        return AlgorithmParameterGenerator.getInstance(algorithm, this.provider);
    }
    
    public AlgorithmParameters createAlgorithmParameters(final String algorithm) throws NoSuchAlgorithmException {
        return AlgorithmParameters.getInstance(algorithm, this.provider);
    }
    
    public KeyGenerator createKeyGenerator(final String algorithm) throws NoSuchAlgorithmException {
        return KeyGenerator.getInstance(algorithm, this.provider);
    }
    
    public KeyFactory createKeyFactory(final String algorithm) throws NoSuchAlgorithmException {
        return KeyFactory.getInstance(algorithm, this.provider);
    }
    
    public SecretKeyFactory createSecretKeyFactory(final String algorithm) throws NoSuchAlgorithmException {
        return SecretKeyFactory.getInstance(algorithm, this.provider);
    }
    
    public KeyPairGenerator createKeyPairGenerator(final String algorithm) throws NoSuchAlgorithmException {
        return KeyPairGenerator.getInstance(algorithm, this.provider);
    }
    
    public MessageDigest createDigest(final String algorithm) throws NoSuchAlgorithmException {
        return MessageDigest.getInstance(algorithm, this.provider);
    }
    
    public Signature createSignature(final String algorithm) throws NoSuchAlgorithmException {
        return Signature.getInstance(algorithm, this.provider);
    }
    
    public CertificateFactory createCertificateFactory(final String type) throws NoSuchAlgorithmException, CertificateException {
        return CertificateFactory.getInstance(type, this.provider);
    }
}
