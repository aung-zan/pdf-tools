// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce;

import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.Signature;
import java.security.MessageDigest;
import java.security.KeyPairGenerator;
import javax.crypto.SecretKeyFactory;
import java.security.KeyFactory;
import javax.crypto.KeyGenerator;
import java.security.AlgorithmParameters;
import java.security.AlgorithmParameterGenerator;
import javax.crypto.KeyAgreement;
import javax.crypto.Mac;
import java.security.NoSuchProviderException;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;

public class NamedJcaJceHelper implements JcaJceHelper
{
    protected final String providerName;
    
    public NamedJcaJceHelper(final String providerName) {
        this.providerName = providerName;
    }
    
    public Cipher createCipher(final String transformation) throws NoSuchAlgorithmException, NoSuchPaddingException, NoSuchProviderException {
        return Cipher.getInstance(transformation, this.providerName);
    }
    
    public Mac createMac(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return Mac.getInstance(algorithm, this.providerName);
    }
    
    public KeyAgreement createKeyAgreement(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return KeyAgreement.getInstance(algorithm, this.providerName);
    }
    
    public AlgorithmParameterGenerator createAlgorithmParameterGenerator(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return AlgorithmParameterGenerator.getInstance(algorithm, this.providerName);
    }
    
    public AlgorithmParameters createAlgorithmParameters(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return AlgorithmParameters.getInstance(algorithm, this.providerName);
    }
    
    public KeyGenerator createKeyGenerator(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return KeyGenerator.getInstance(algorithm, this.providerName);
    }
    
    public KeyFactory createKeyFactory(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return KeyFactory.getInstance(algorithm, this.providerName);
    }
    
    public SecretKeyFactory createSecretKeyFactory(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return SecretKeyFactory.getInstance(algorithm, this.providerName);
    }
    
    public KeyPairGenerator createKeyPairGenerator(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return KeyPairGenerator.getInstance(algorithm, this.providerName);
    }
    
    public MessageDigest createDigest(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return MessageDigest.getInstance(algorithm, this.providerName);
    }
    
    public Signature createSignature(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return Signature.getInstance(algorithm, this.providerName);
    }
    
    public CertificateFactory createCertificateFactory(final String type) throws NoSuchAlgorithmException, CertificateException, NoSuchProviderException {
        return CertificateFactory.getInstance(type, this.providerName);
    }
}
