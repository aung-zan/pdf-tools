// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce;

import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.Signature;
import java.security.MessageDigest;
import java.security.KeyPairGenerator;
import javax.crypto.SecretKeyFactory;
import java.security.KeyFactory;
import javax.crypto.KeyGenerator;
import java.security.AlgorithmParameters;
import java.security.AlgorithmParameterGenerator;
import javax.crypto.KeyAgreement;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;

public class DefaultJcaJceHelper implements JcaJceHelper
{
    public Cipher createCipher(final String transformation) throws NoSuchAlgorithmException, NoSuchPaddingException {
        return Cipher.getInstance(transformation);
    }
    
    public Mac createMac(final String algorithm) throws NoSuchAlgorithmException {
        return Mac.getInstance(algorithm);
    }
    
    public KeyAgreement createKeyAgreement(final String algorithm) throws NoSuchAlgorithmException {
        return KeyAgreement.getInstance(algorithm);
    }
    
    public AlgorithmParameterGenerator createAlgorithmParameterGenerator(final String algorithm) throws NoSuchAlgorithmException {
        return AlgorithmParameterGenerator.getInstance(algorithm);
    }
    
    public AlgorithmParameters createAlgorithmParameters(final String algorithm) throws NoSuchAlgorithmException {
        return AlgorithmParameters.getInstance(algorithm);
    }
    
    public KeyGenerator createKeyGenerator(final String algorithm) throws NoSuchAlgorithmException {
        return KeyGenerator.getInstance(algorithm);
    }
    
    public KeyFactory createKeyFactory(final String algorithm) throws NoSuchAlgorithmException {
        return KeyFactory.getInstance(algorithm);
    }
    
    public SecretKeyFactory createSecretKeyFactory(final String algorithm) throws NoSuchAlgorithmException {
        return SecretKeyFactory.getInstance(algorithm);
    }
    
    public KeyPairGenerator createKeyPairGenerator(final String algorithm) throws NoSuchAlgorithmException {
        return KeyPairGenerator.getInstance(algorithm);
    }
    
    public MessageDigest createDigest(final String algorithm) throws NoSuchAlgorithmException {
        return MessageDigest.getInstance(algorithm);
    }
    
    public Signature createSignature(final String algorithm) throws NoSuchAlgorithmException {
        return Signature.getInstance(algorithm);
    }
    
    public CertificateFactory createCertificateFactory(final String type) throws NoSuchAlgorithmException, CertificateException {
        return CertificateFactory.getInstance(type);
    }
}
