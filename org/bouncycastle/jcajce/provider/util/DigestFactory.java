// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.util;

import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.util.HashMap;
import java.util.HashSet;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.digests.SHA384Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.digests.SHA224Digest;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.util.Strings;
import org.bouncycastle.crypto.Digest;
import java.util.Map;
import java.util.Set;

public class DigestFactory
{
    private static Set md5;
    private static Set sha1;
    private static Set sha224;
    private static Set sha256;
    private static Set sha384;
    private static Set sha512;
    private static Map oids;
    
    public static Digest getDigest(String upperCase) {
        upperCase = Strings.toUpperCase(upperCase);
        if (DigestFactory.sha1.contains(upperCase)) {
            return new SHA1Digest();
        }
        if (DigestFactory.md5.contains(upperCase)) {
            return new MD5Digest();
        }
        if (DigestFactory.sha224.contains(upperCase)) {
            return new SHA224Digest();
        }
        if (DigestFactory.sha256.contains(upperCase)) {
            return new SHA256Digest();
        }
        if (DigestFactory.sha384.contains(upperCase)) {
            return new SHA384Digest();
        }
        if (DigestFactory.sha512.contains(upperCase)) {
            return new SHA512Digest();
        }
        return null;
    }
    
    public static boolean isSameDigest(final String s, final String s2) {
        return (DigestFactory.sha1.contains(s) && DigestFactory.sha1.contains(s2)) || (DigestFactory.sha224.contains(s) && DigestFactory.sha224.contains(s2)) || (DigestFactory.sha256.contains(s) && DigestFactory.sha256.contains(s2)) || (DigestFactory.sha384.contains(s) && DigestFactory.sha384.contains(s2)) || (DigestFactory.sha512.contains(s) && DigestFactory.sha512.contains(s2)) || (DigestFactory.md5.contains(s) && DigestFactory.md5.contains(s2));
    }
    
    public static ASN1ObjectIdentifier getOID(final String s) {
        return DigestFactory.oids.get(s);
    }
    
    static {
        DigestFactory.md5 = new HashSet();
        DigestFactory.sha1 = new HashSet();
        DigestFactory.sha224 = new HashSet();
        DigestFactory.sha256 = new HashSet();
        DigestFactory.sha384 = new HashSet();
        DigestFactory.sha512 = new HashSet();
        DigestFactory.oids = new HashMap();
        DigestFactory.md5.add("MD5");
        DigestFactory.md5.add(PKCSObjectIdentifiers.md5.getId());
        DigestFactory.sha1.add("SHA1");
        DigestFactory.sha1.add("SHA-1");
        DigestFactory.sha1.add(OIWObjectIdentifiers.idSHA1.getId());
        DigestFactory.sha224.add("SHA224");
        DigestFactory.sha224.add("SHA-224");
        DigestFactory.sha224.add(NISTObjectIdentifiers.id_sha224.getId());
        DigestFactory.sha256.add("SHA256");
        DigestFactory.sha256.add("SHA-256");
        DigestFactory.sha256.add(NISTObjectIdentifiers.id_sha256.getId());
        DigestFactory.sha384.add("SHA384");
        DigestFactory.sha384.add("SHA-384");
        DigestFactory.sha384.add(NISTObjectIdentifiers.id_sha384.getId());
        DigestFactory.sha512.add("SHA512");
        DigestFactory.sha512.add("SHA-512");
        DigestFactory.sha512.add(NISTObjectIdentifiers.id_sha512.getId());
        DigestFactory.oids.put("MD5", PKCSObjectIdentifiers.md5);
        DigestFactory.oids.put(PKCSObjectIdentifiers.md5.getId(), PKCSObjectIdentifiers.md5);
        DigestFactory.oids.put("SHA1", OIWObjectIdentifiers.idSHA1);
        DigestFactory.oids.put("SHA-1", OIWObjectIdentifiers.idSHA1);
        DigestFactory.oids.put(OIWObjectIdentifiers.idSHA1.getId(), OIWObjectIdentifiers.idSHA1);
        DigestFactory.oids.put("SHA224", NISTObjectIdentifiers.id_sha224);
        DigestFactory.oids.put("SHA-224", NISTObjectIdentifiers.id_sha224);
        DigestFactory.oids.put(NISTObjectIdentifiers.id_sha224.getId(), NISTObjectIdentifiers.id_sha224);
        DigestFactory.oids.put("SHA256", NISTObjectIdentifiers.id_sha256);
        DigestFactory.oids.put("SHA-256", NISTObjectIdentifiers.id_sha256);
        DigestFactory.oids.put(NISTObjectIdentifiers.id_sha256.getId(), NISTObjectIdentifiers.id_sha256);
        DigestFactory.oids.put("SHA384", NISTObjectIdentifiers.id_sha384);
        DigestFactory.oids.put("SHA-384", NISTObjectIdentifiers.id_sha384);
        DigestFactory.oids.put(NISTObjectIdentifiers.id_sha384.getId(), NISTObjectIdentifiers.id_sha384);
        DigestFactory.oids.put("SHA512", NISTObjectIdentifiers.id_sha512);
        DigestFactory.oids.put("SHA-512", NISTObjectIdentifiers.id_sha512);
        DigestFactory.oids.put(NISTObjectIdentifiers.id_sha512.getId(), NISTObjectIdentifiers.id_sha512);
    }
}
