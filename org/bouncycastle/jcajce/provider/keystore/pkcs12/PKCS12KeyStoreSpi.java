// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.keystore.pkcs12;

import org.bouncycastle.util.Strings;
import javax.crypto.Mac;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.BEROutputStream;
import org.bouncycastle.asn1.DEROutputStream;
import java.io.ByteArrayOutputStream;
import java.security.cert.CertificateEncodingException;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.BEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.io.OutputStream;
import java.security.cert.CertificateException;
import org.bouncycastle.jce.provider.JDKPKCS12StoreParameter;
import org.bouncycastle.jcajce.provider.config.PKCS12StoreParameter;
import java.security.KeyStore;
import org.bouncycastle.asn1.x509.DigestInfo;
import org.bouncycastle.asn1.pkcs.MacData;
import org.bouncycastle.asn1.pkcs.ContentInfo;
import java.io.ByteArrayInputStream;
import org.bouncycastle.asn1.pkcs.CertBag;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.pkcs.EncryptedData;
import org.bouncycastle.asn1.util.ASN1Dump;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import org.bouncycastle.asn1.pkcs.EncryptedPrivateKeyInfo;
import org.bouncycastle.asn1.pkcs.AuthenticatedSafe;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.pkcs.SafeBag;
import org.bouncycastle.asn1.pkcs.Pfx;
import java.io.BufferedInputStream;
import java.io.InputStream;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.jcajce.provider.util.SecretKeyUtil;
import org.bouncycastle.asn1.pkcs.PBKDF2Params;
import org.bouncycastle.asn1.pkcs.PBES2Parameters;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.Cipher;
import org.bouncycastle.jcajce.provider.symmetric.util.BCPBEKey;
import java.security.spec.KeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import org.bouncycastle.asn1.pkcs.PKCS12PBEParams;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.security.Principal;
import java.io.IOException;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x509.Extension;
import java.security.cert.X509Certificate;
import java.util.Vector;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.security.Key;
import java.util.Enumeration;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import java.security.PublicKey;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.cert.CertificateFactory;
import java.security.SecureRandom;
import java.util.Hashtable;
import java.security.Provider;
import org.bouncycastle.jce.interfaces.BCKeyStore;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.security.KeyStoreSpi;

public class PKCS12KeyStoreSpi extends KeyStoreSpi implements PKCSObjectIdentifiers, X509ObjectIdentifiers, BCKeyStore
{
    private static final int SALT_SIZE = 20;
    private static final int MIN_ITERATIONS = 1024;
    private static final Provider bcProvider;
    private IgnoresCaseHashtable keys;
    private Hashtable localIds;
    private IgnoresCaseHashtable certs;
    private Hashtable chainCerts;
    private Hashtable keyCerts;
    static final int NULL = 0;
    static final int CERTIFICATE = 1;
    static final int KEY = 2;
    static final int SECRET = 3;
    static final int SEALED = 4;
    static final int KEY_PRIVATE = 0;
    static final int KEY_PUBLIC = 1;
    static final int KEY_SECRET = 2;
    protected SecureRandom random;
    private CertificateFactory certFact;
    private ASN1ObjectIdentifier keyAlgorithm;
    private ASN1ObjectIdentifier certAlgorithm;
    
    public PKCS12KeyStoreSpi(final Provider provider, final ASN1ObjectIdentifier keyAlgorithm, final ASN1ObjectIdentifier certAlgorithm) {
        this.keys = new IgnoresCaseHashtable();
        this.localIds = new Hashtable();
        this.certs = new IgnoresCaseHashtable();
        this.chainCerts = new Hashtable();
        this.keyCerts = new Hashtable();
        this.random = new SecureRandom();
        this.keyAlgorithm = keyAlgorithm;
        this.certAlgorithm = certAlgorithm;
        try {
            if (provider != null) {
                this.certFact = CertificateFactory.getInstance("X.509", provider);
            }
            else {
                this.certFact = CertificateFactory.getInstance("X.509");
            }
        }
        catch (Exception ex) {
            throw new IllegalArgumentException("can't create cert factory - " + ex.toString());
        }
    }
    
    private SubjectKeyIdentifier createSubjectKeyId(final PublicKey publicKey) {
        try {
            return new SubjectKeyIdentifier(new SubjectPublicKeyInfo((ASN1Sequence)ASN1Primitive.fromByteArray(publicKey.getEncoded())));
        }
        catch (Exception ex) {
            throw new RuntimeException("error creating key");
        }
    }
    
    public void setRandom(final SecureRandom random) {
        this.random = random;
    }
    
    @Override
    public Enumeration engineAliases() {
        final Hashtable<String, String> hashtable = new Hashtable<String, String>();
        final Enumeration keys = this.certs.keys();
        while (keys.hasMoreElements()) {
            hashtable.put(keys.nextElement(), "cert");
        }
        final Enumeration keys2 = this.keys.keys();
        while (keys2.hasMoreElements()) {
            final String s = keys2.nextElement();
            if (hashtable.get(s) == null) {
                hashtable.put(s, "key");
            }
        }
        return hashtable.keys();
    }
    
    @Override
    public boolean engineContainsAlias(final String s) {
        return this.certs.get(s) != null || this.keys.get(s) != null;
    }
    
    @Override
    public void engineDeleteEntry(final String key) throws KeyStoreException {
        final Key key2 = (Key)this.keys.remove(key);
        Certificate certificate = (Certificate)this.certs.remove(key);
        if (certificate != null) {
            this.chainCerts.remove(new CertId(certificate.getPublicKey()));
        }
        if (key2 != null) {
            final String key3 = this.localIds.remove(key);
            if (key3 != null) {
                certificate = (Certificate)this.keyCerts.remove(key3);
            }
            if (certificate != null) {
                this.chainCerts.remove(new CertId(certificate.getPublicKey()));
            }
        }
    }
    
    @Override
    public Certificate engineGetCertificate(final String s) {
        if (s == null) {
            throw new IllegalArgumentException("null alias passed to getCertificate.");
        }
        Certificate certificate = (Certificate)this.certs.get(s);
        if (certificate == null) {
            final String key = this.localIds.get(s);
            if (key != null) {
                certificate = (Certificate)this.keyCerts.get(key);
            }
            else {
                certificate = this.keyCerts.get(s);
            }
        }
        return certificate;
    }
    
    @Override
    public String engineGetCertificateAlias(final Certificate certificate) {
        final Enumeration elements = this.certs.elements();
        final Enumeration keys = this.certs.keys();
        while (elements.hasMoreElements()) {
            final Certificate certificate2 = elements.nextElement();
            final String s = keys.nextElement();
            if (certificate2.equals(certificate)) {
                return s;
            }
        }
        final Enumeration<Certificate> elements2 = (Enumeration<Certificate>)this.keyCerts.elements();
        final Enumeration<String> keys2 = this.keyCerts.keys();
        while (elements2.hasMoreElements()) {
            final Certificate certificate3 = elements2.nextElement();
            final String s2 = keys2.nextElement();
            if (certificate3.equals(certificate)) {
                return s2;
            }
        }
        return null;
    }
    
    @Override
    public Certificate[] engineGetCertificateChain(final String s) {
        if (s == null) {
            throw new IllegalArgumentException("null alias passed to getCertificateChain.");
        }
        if (!this.engineIsKeyEntry(s)) {
            return null;
        }
        Certificate engineGetCertificate = this.engineGetCertificate(s);
        if (engineGetCertificate != null) {
            final Vector<X509Certificate> vector = new Vector<X509Certificate>();
            while (engineGetCertificate != null) {
                final X509Certificate x509Certificate = (X509Certificate)engineGetCertificate;
                Certificate certificate = null;
                final byte[] extensionValue = x509Certificate.getExtensionValue(Extension.authorityKeyIdentifier.getId());
                if (extensionValue != null) {
                    try {
                        final AuthorityKeyIdentifier instance = AuthorityKeyIdentifier.getInstance(new ASN1InputStream(((ASN1OctetString)new ASN1InputStream(extensionValue).readObject()).getOctets()).readObject());
                        if (instance.getKeyIdentifier() != null) {
                            certificate = (Certificate)this.chainCerts.get(new CertId(instance.getKeyIdentifier()));
                        }
                    }
                    catch (IOException ex) {
                        throw new RuntimeException(ex.toString());
                    }
                }
                if (certificate == null) {
                    final Principal issuerDN = x509Certificate.getIssuerDN();
                    if (!issuerDN.equals(x509Certificate.getSubjectDN())) {
                        final Enumeration<Object> keys = this.chainCerts.keys();
                        while (keys.hasMoreElements()) {
                            final X509Certificate x509Certificate2 = this.chainCerts.get(keys.nextElement());
                            if (x509Certificate2.getSubjectDN().equals(issuerDN)) {
                                try {
                                    x509Certificate.verify(x509Certificate2.getPublicKey());
                                    certificate = x509Certificate2;
                                    break;
                                }
                                catch (Exception ex2) {}
                            }
                        }
                    }
                }
                vector.addElement((X509Certificate)engineGetCertificate);
                if (certificate != engineGetCertificate) {
                    engineGetCertificate = certificate;
                }
                else {
                    engineGetCertificate = null;
                }
            }
            final Certificate[] array = new Certificate[vector.size()];
            for (int i = 0; i != array.length; ++i) {
                array[i] = vector.elementAt(i);
            }
            return array;
        }
        return null;
    }
    
    @Override
    public Date engineGetCreationDate(final String s) {
        if (s == null) {
            throw new NullPointerException("alias == null");
        }
        if (this.keys.get(s) == null && this.certs.get(s) == null) {
            return null;
        }
        return new Date();
    }
    
    @Override
    public Key engineGetKey(final String s, final char[] array) throws NoSuchAlgorithmException, UnrecoverableKeyException {
        if (s == null) {
            throw new IllegalArgumentException("null alias passed to getKey.");
        }
        return (Key)this.keys.get(s);
    }
    
    @Override
    public boolean engineIsCertificateEntry(final String s) {
        return this.certs.get(s) != null && this.keys.get(s) == null;
    }
    
    @Override
    public boolean engineIsKeyEntry(final String s) {
        return this.keys.get(s) != null;
    }
    
    @Override
    public void engineSetCertificateEntry(final String str, final Certificate value) throws KeyStoreException {
        if (this.keys.get(str) != null) {
            throw new KeyStoreException("There is a key entry with the name " + str + ".");
        }
        this.certs.put(str, value);
        this.chainCerts.put(new CertId(value.getPublicKey()), value);
    }
    
    @Override
    public void engineSetKeyEntry(final String s, final byte[] array, final Certificate[] array2) throws KeyStoreException {
        throw new RuntimeException("operation not supported");
    }
    
    @Override
    public void engineSetKeyEntry(final String s, final Key key, final char[] array, final Certificate[] array2) throws KeyStoreException {
        if (!(key instanceof PrivateKey)) {
            throw new KeyStoreException("PKCS12 does not support non-PrivateKeys");
        }
        if (key instanceof PrivateKey && array2 == null) {
            throw new KeyStoreException("no certificate chain for private key");
        }
        if (this.keys.get(s) != null) {
            this.engineDeleteEntry(s);
        }
        this.keys.put(s, key);
        if (array2 != null) {
            this.certs.put(s, array2[0]);
            for (int i = 0; i != array2.length; ++i) {
                this.chainCerts.put(new CertId(array2[i].getPublicKey()), array2[i]);
            }
        }
    }
    
    @Override
    public int engineSize() {
        final Hashtable<String, String> hashtable = new Hashtable<String, String>();
        final Enumeration keys = this.certs.keys();
        while (keys.hasMoreElements()) {
            hashtable.put(keys.nextElement(), "cert");
        }
        final Enumeration keys2 = this.keys.keys();
        while (keys2.hasMoreElements()) {
            final String s = keys2.nextElement();
            if (hashtable.get(s) == null) {
                hashtable.put(s, "key");
            }
        }
        return hashtable.size();
    }
    
    protected PrivateKey unwrapKey(final AlgorithmIdentifier algorithmIdentifier, final byte[] array, final char[] array2, final boolean tryWrongPKCS12Zero) throws IOException {
        final ASN1ObjectIdentifier algorithm = algorithmIdentifier.getAlgorithm();
        try {
            if (algorithm.on(PKCSObjectIdentifiers.pkcs_12PbeIds)) {
                final PKCS12PBEParams instance = PKCS12PBEParams.getInstance(algorithmIdentifier.getParameters());
                final PBEKeySpec keySpec = new PBEKeySpec(array2);
                final SecretKeyFactory instance2 = SecretKeyFactory.getInstance(algorithm.getId(), PKCS12KeyStoreSpi.bcProvider);
                final PBEParameterSpec params = new PBEParameterSpec(instance.getIV(), instance.getIterations().intValue());
                final SecretKey generateSecret = instance2.generateSecret(keySpec);
                ((BCPBEKey)generateSecret).setTryWrongPKCS12Zero(tryWrongPKCS12Zero);
                final Cipher instance3 = Cipher.getInstance(algorithm.getId(), PKCS12KeyStoreSpi.bcProvider);
                instance3.init(4, generateSecret, params);
                return (PrivateKey)instance3.unwrap(array, "", 2);
            }
            if (algorithm.equals(PKCSObjectIdentifiers.id_PBES2)) {
                final PBES2Parameters instance4 = PBES2Parameters.getInstance(algorithmIdentifier.getParameters());
                final PBKDF2Params instance5 = PBKDF2Params.getInstance(instance4.getKeyDerivationFunc().getParameters());
                final SecretKey generateSecret2 = SecretKeyFactory.getInstance(instance4.getKeyDerivationFunc().getAlgorithm().getId(), PKCS12KeyStoreSpi.bcProvider).generateSecret(new PBEKeySpec(array2, instance5.getSalt(), instance5.getIterationCount().intValue(), SecretKeyUtil.getKeySize(instance4.getEncryptionScheme().getAlgorithm())));
                final Cipher instance6 = Cipher.getInstance(instance4.getEncryptionScheme().getAlgorithm().getId(), PKCS12KeyStoreSpi.bcProvider);
                instance6.init(4, generateSecret2, new IvParameterSpec(ASN1OctetString.getInstance(instance4.getEncryptionScheme().getParameters()).getOctets()));
                return (PrivateKey)instance6.unwrap(array, "", 2);
            }
        }
        catch (Exception ex) {
            throw new IOException("exception unwrapping private key - " + ex.toString());
        }
        throw new IOException("exception unwrapping private key - cannot recognise: " + algorithm);
    }
    
    protected byte[] wrapKey(final String s, final Key key, final PKCS12PBEParams pkcs12PBEParams, final char[] password) throws IOException {
        final PBEKeySpec keySpec = new PBEKeySpec(password);
        byte[] wrap;
        try {
            final SecretKeyFactory instance = SecretKeyFactory.getInstance(s, PKCS12KeyStoreSpi.bcProvider);
            final PBEParameterSpec params = new PBEParameterSpec(pkcs12PBEParams.getIV(), pkcs12PBEParams.getIterations().intValue());
            final Cipher instance2 = Cipher.getInstance(s, PKCS12KeyStoreSpi.bcProvider);
            instance2.init(3, instance.generateSecret(keySpec), params);
            wrap = instance2.wrap(key);
        }
        catch (Exception ex) {
            throw new IOException("exception encrypting data - " + ex.toString());
        }
        return wrap;
    }
    
    protected byte[] cryptData(final boolean b, final AlgorithmIdentifier algorithmIdentifier, final char[] password, final boolean tryWrongPKCS12Zero, final byte[] input) throws IOException {
        final String id = algorithmIdentifier.getAlgorithm().getId();
        final PKCS12PBEParams instance = PKCS12PBEParams.getInstance(algorithmIdentifier.getParameters());
        final PBEKeySpec keySpec = new PBEKeySpec(password);
        try {
            final SecretKeyFactory instance2 = SecretKeyFactory.getInstance(id, PKCS12KeyStoreSpi.bcProvider);
            final PBEParameterSpec params = new PBEParameterSpec(instance.getIV(), instance.getIterations().intValue());
            final BCPBEKey key = (BCPBEKey)instance2.generateSecret(keySpec);
            key.setTryWrongPKCS12Zero(tryWrongPKCS12Zero);
            final Cipher instance3 = Cipher.getInstance(id, PKCS12KeyStoreSpi.bcProvider);
            instance3.init(b ? 1 : 2, key, params);
            return instance3.doFinal(input);
        }
        catch (Exception ex) {
            throw new IOException("exception decrypting data - " + ex.toString());
        }
    }
    
    @Override
    public void engineLoad(final InputStream in, final char[] array) throws IOException {
        if (in == null) {
            return;
        }
        if (array == null) {
            throw new NullPointerException("No password supplied for PKCS#12 KeyStore.");
        }
        final BufferedInputStream bufferedInputStream = new BufferedInputStream(in);
        bufferedInputStream.mark(10);
        if (bufferedInputStream.read() != 48) {
            throw new IOException("stream does not represent a PKCS12 key store");
        }
        bufferedInputStream.reset();
        final Pfx instance = Pfx.getInstance(new ASN1InputStream(bufferedInputStream).readObject());
        final ContentInfo authSafe = instance.getAuthSafe();
        final Vector<SafeBag> vector = new Vector<SafeBag>();
        boolean b = false;
        boolean b2 = false;
        if (instance.getMacData() != null) {
            final MacData macData = instance.getMacData();
            final DigestInfo mac = macData.getMac();
            final AlgorithmIdentifier algorithmId = mac.getAlgorithmId();
            final byte[] salt = macData.getSalt();
            final int intValue = macData.getIterationCount().intValue();
            final byte[] octets = ((ASN1OctetString)authSafe.getContent()).getOctets();
            try {
                final byte[] calculatePbeMac = calculatePbeMac(algorithmId.getAlgorithm(), salt, intValue, array, false, octets);
                final byte[] digest = mac.getDigest();
                if (!Arrays.constantTimeAreEqual(calculatePbeMac, digest)) {
                    if (array.length > 0) {
                        throw new IOException("PKCS12 key store mac invalid - wrong password or corrupted file.");
                    }
                    if (!Arrays.constantTimeAreEqual(calculatePbeMac(algorithmId.getAlgorithm(), salt, intValue, array, true, octets), digest)) {
                        throw new IOException("PKCS12 key store mac invalid - wrong password or corrupted file.");
                    }
                    b2 = true;
                }
            }
            catch (IOException ex) {
                throw ex;
            }
            catch (Exception ex2) {
                throw new IOException("error constructing MAC: " + ex2.toString());
            }
        }
        this.keys = new IgnoresCaseHashtable();
        this.localIds = new Hashtable();
        if (authSafe.getContentType().equals(PKCS12KeyStoreSpi.data)) {
            final ContentInfo[] contentInfo = AuthenticatedSafe.getInstance(new ASN1InputStream(((ASN1OctetString)authSafe.getContent()).getOctets()).readObject()).getContentInfo();
            for (int i = 0; i != contentInfo.length; ++i) {
                if (contentInfo[i].getContentType().equals(PKCS12KeyStoreSpi.data)) {
                    final ASN1Sequence asn1Sequence = (ASN1Sequence)new ASN1InputStream(((ASN1OctetString)contentInfo[i].getContent()).getOctets()).readObject();
                    for (int j = 0; j != asn1Sequence.size(); ++j) {
                        final SafeBag instance2 = SafeBag.getInstance(asn1Sequence.getObjectAt(j));
                        if (instance2.getBagId().equals(PKCS12KeyStoreSpi.pkcs8ShroudedKeyBag)) {
                            final EncryptedPrivateKeyInfo instance3 = EncryptedPrivateKeyInfo.getInstance(instance2.getBagValue());
                            final PrivateKey unwrapKey = this.unwrapKey(instance3.getEncryptionAlgorithm(), instance3.getEncryptedData(), array, b2);
                            final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier = (PKCS12BagAttributeCarrier)unwrapKey;
                            String string = null;
                            ASN1OctetString asn1OctetString = null;
                            if (instance2.getBagAttributes() != null) {
                                final Enumeration objects = instance2.getBagAttributes().getObjects();
                                while (objects.hasMoreElements()) {
                                    final ASN1Sequence asn1Sequence2 = objects.nextElement();
                                    final ASN1ObjectIdentifier asn1ObjectIdentifier = (ASN1ObjectIdentifier)asn1Sequence2.getObjectAt(0);
                                    final ASN1Set set = (ASN1Set)asn1Sequence2.getObjectAt(1);
                                    ASN1Primitive asn1Primitive = null;
                                    if (set.size() > 0) {
                                        asn1Primitive = (ASN1Primitive)set.getObjectAt(0);
                                        final ASN1Encodable bagAttribute = pkcs12BagAttributeCarrier.getBagAttribute(asn1ObjectIdentifier);
                                        if (bagAttribute != null) {
                                            if (!bagAttribute.toASN1Primitive().equals(asn1Primitive)) {
                                                throw new IOException("attempt to add existing attribute with different value");
                                            }
                                        }
                                        else {
                                            pkcs12BagAttributeCarrier.setBagAttribute(asn1ObjectIdentifier, asn1Primitive);
                                        }
                                    }
                                    if (asn1ObjectIdentifier.equals(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName)) {
                                        string = ((DERBMPString)asn1Primitive).getString();
                                        this.keys.put(string, unwrapKey);
                                    }
                                    else {
                                        if (!asn1ObjectIdentifier.equals(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId)) {
                                            continue;
                                        }
                                        asn1OctetString = (ASN1OctetString)asn1Primitive;
                                    }
                                }
                            }
                            if (asn1OctetString != null) {
                                final String value = new String(Hex.encode(asn1OctetString.getOctets()));
                                if (string == null) {
                                    this.keys.put(value, unwrapKey);
                                }
                                else {
                                    this.localIds.put(string, value);
                                }
                            }
                            else {
                                b = true;
                                this.keys.put("unmarked", unwrapKey);
                            }
                        }
                        else if (instance2.getBagId().equals(PKCS12KeyStoreSpi.certBag)) {
                            vector.addElement(instance2);
                        }
                        else {
                            System.out.println("extra in data " + instance2.getBagId());
                            System.out.println(ASN1Dump.dumpAsString(instance2));
                        }
                    }
                }
                else if (contentInfo[i].getContentType().equals(PKCS12KeyStoreSpi.encryptedData)) {
                    final EncryptedData instance4 = EncryptedData.getInstance(contentInfo[i].getContent());
                    final ASN1Sequence asn1Sequence3 = (ASN1Sequence)ASN1Primitive.fromByteArray(this.cryptData(false, instance4.getEncryptionAlgorithm(), array, b2, instance4.getContent().getOctets()));
                    for (int k = 0; k != asn1Sequence3.size(); ++k) {
                        final SafeBag instance5 = SafeBag.getInstance(asn1Sequence3.getObjectAt(k));
                        if (instance5.getBagId().equals(PKCS12KeyStoreSpi.certBag)) {
                            vector.addElement(instance5);
                        }
                        else if (instance5.getBagId().equals(PKCS12KeyStoreSpi.pkcs8ShroudedKeyBag)) {
                            final EncryptedPrivateKeyInfo instance6 = EncryptedPrivateKeyInfo.getInstance(instance5.getBagValue());
                            final PrivateKey unwrapKey2 = this.unwrapKey(instance6.getEncryptionAlgorithm(), instance6.getEncryptedData(), array, b2);
                            final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier2 = (PKCS12BagAttributeCarrier)unwrapKey2;
                            String string2 = null;
                            ASN1OctetString asn1OctetString2 = null;
                            final Enumeration objects2 = instance5.getBagAttributes().getObjects();
                            while (objects2.hasMoreElements()) {
                                final ASN1Sequence asn1Sequence4 = objects2.nextElement();
                                final ASN1ObjectIdentifier asn1ObjectIdentifier2 = (ASN1ObjectIdentifier)asn1Sequence4.getObjectAt(0);
                                final ASN1Set set2 = (ASN1Set)asn1Sequence4.getObjectAt(1);
                                ASN1Primitive asn1Primitive2 = null;
                                if (set2.size() > 0) {
                                    asn1Primitive2 = (ASN1Primitive)set2.getObjectAt(0);
                                    final ASN1Encodable bagAttribute2 = pkcs12BagAttributeCarrier2.getBagAttribute(asn1ObjectIdentifier2);
                                    if (bagAttribute2 != null) {
                                        if (!bagAttribute2.toASN1Primitive().equals(asn1Primitive2)) {
                                            throw new IOException("attempt to add existing attribute with different value");
                                        }
                                    }
                                    else {
                                        pkcs12BagAttributeCarrier2.setBagAttribute(asn1ObjectIdentifier2, asn1Primitive2);
                                    }
                                }
                                if (asn1ObjectIdentifier2.equals(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName)) {
                                    string2 = ((DERBMPString)asn1Primitive2).getString();
                                    this.keys.put(string2, unwrapKey2);
                                }
                                else {
                                    if (!asn1ObjectIdentifier2.equals(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId)) {
                                        continue;
                                    }
                                    asn1OctetString2 = (ASN1OctetString)asn1Primitive2;
                                }
                            }
                            final String value2 = new String(Hex.encode(asn1OctetString2.getOctets()));
                            if (string2 == null) {
                                this.keys.put(value2, unwrapKey2);
                            }
                            else {
                                this.localIds.put(string2, value2);
                            }
                        }
                        else if (instance5.getBagId().equals(PKCS12KeyStoreSpi.keyBag)) {
                            final PrivateKey privateKey = BouncyCastleProvider.getPrivateKey(PrivateKeyInfo.getInstance(instance5.getBagValue()));
                            final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier3 = (PKCS12BagAttributeCarrier)privateKey;
                            String string3 = null;
                            ASN1OctetString asn1OctetString3 = null;
                            final Enumeration objects3 = instance5.getBagAttributes().getObjects();
                            while (objects3.hasMoreElements()) {
                                final ASN1Sequence asn1Sequence5 = objects3.nextElement();
                                final ASN1ObjectIdentifier asn1ObjectIdentifier3 = (ASN1ObjectIdentifier)asn1Sequence5.getObjectAt(0);
                                final ASN1Set set3 = (ASN1Set)asn1Sequence5.getObjectAt(1);
                                ASN1Primitive asn1Primitive3 = null;
                                if (set3.size() > 0) {
                                    asn1Primitive3 = (ASN1Primitive)set3.getObjectAt(0);
                                    final ASN1Encodable bagAttribute3 = pkcs12BagAttributeCarrier3.getBagAttribute(asn1ObjectIdentifier3);
                                    if (bagAttribute3 != null) {
                                        if (!bagAttribute3.toASN1Primitive().equals(asn1Primitive3)) {
                                            throw new IOException("attempt to add existing attribute with different value");
                                        }
                                    }
                                    else {
                                        pkcs12BagAttributeCarrier3.setBagAttribute(asn1ObjectIdentifier3, asn1Primitive3);
                                    }
                                }
                                if (asn1ObjectIdentifier3.equals(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName)) {
                                    string3 = ((DERBMPString)asn1Primitive3).getString();
                                    this.keys.put(string3, privateKey);
                                }
                                else {
                                    if (!asn1ObjectIdentifier3.equals(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId)) {
                                        continue;
                                    }
                                    asn1OctetString3 = (ASN1OctetString)asn1Primitive3;
                                }
                            }
                            final String value3 = new String(Hex.encode(asn1OctetString3.getOctets()));
                            if (string3 == null) {
                                this.keys.put(value3, privateKey);
                            }
                            else {
                                this.localIds.put(string3, value3);
                            }
                        }
                        else {
                            System.out.println("extra in encryptedData " + instance5.getBagId());
                            System.out.println(ASN1Dump.dumpAsString(instance5));
                        }
                    }
                }
                else {
                    System.out.println("extra " + contentInfo[i].getContentType().getId());
                    System.out.println("extra " + ASN1Dump.dumpAsString(contentInfo[i].getContent()));
                }
            }
        }
        this.certs = new IgnoresCaseHashtable();
        this.chainCerts = new Hashtable();
        this.keyCerts = new Hashtable();
        for (int l = 0; l != vector.size(); ++l) {
            final SafeBag safeBag = vector.elementAt(l);
            final CertBag instance7 = CertBag.getInstance(safeBag.getBagValue());
            if (!instance7.getCertId().equals(PKCS12KeyStoreSpi.x509Certificate)) {
                throw new RuntimeException("Unsupported certificate type: " + instance7.getCertId());
            }
            Certificate generateCertificate;
            try {
                generateCertificate = this.certFact.generateCertificate(new ByteArrayInputStream(((ASN1OctetString)instance7.getCertValue()).getOctets()));
            }
            catch (Exception ex3) {
                throw new RuntimeException(ex3.toString());
            }
            ASN1OctetString asn1OctetString4 = null;
            String string4 = null;
            if (safeBag.getBagAttributes() != null) {
                final Enumeration objects4 = safeBag.getBagAttributes().getObjects();
                while (objects4.hasMoreElements()) {
                    final ASN1Sequence asn1Sequence6 = objects4.nextElement();
                    final ASN1ObjectIdentifier asn1ObjectIdentifier4 = (ASN1ObjectIdentifier)asn1Sequence6.getObjectAt(0);
                    final ASN1Primitive asn1Primitive4 = (ASN1Primitive)((ASN1Set)asn1Sequence6.getObjectAt(1)).getObjectAt(0);
                    if (generateCertificate instanceof PKCS12BagAttributeCarrier) {
                        final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier4 = (PKCS12BagAttributeCarrier)generateCertificate;
                        final ASN1Encodable bagAttribute4 = pkcs12BagAttributeCarrier4.getBagAttribute(asn1ObjectIdentifier4);
                        if (bagAttribute4 != null) {
                            if (!bagAttribute4.toASN1Primitive().equals(asn1Primitive4)) {
                                throw new IOException("attempt to add existing attribute with different value");
                            }
                        }
                        else {
                            pkcs12BagAttributeCarrier4.setBagAttribute(asn1ObjectIdentifier4, asn1Primitive4);
                        }
                    }
                    if (asn1ObjectIdentifier4.equals(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName)) {
                        string4 = ((DERBMPString)asn1Primitive4).getString();
                    }
                    else {
                        if (!asn1ObjectIdentifier4.equals(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId)) {
                            continue;
                        }
                        asn1OctetString4 = (ASN1OctetString)asn1Primitive4;
                    }
                }
            }
            this.chainCerts.put(new CertId(generateCertificate.getPublicKey()), generateCertificate);
            if (b) {
                if (this.keyCerts.isEmpty()) {
                    final String key = new String(Hex.encode(this.createSubjectKeyId(generateCertificate.getPublicKey()).getKeyIdentifier()));
                    this.keyCerts.put(key, generateCertificate);
                    this.keys.put(key, this.keys.remove("unmarked"));
                }
            }
            else {
                if (asn1OctetString4 != null) {
                    this.keyCerts.put(new String(Hex.encode(asn1OctetString4.getOctets())), generateCertificate);
                }
                if (string4 != null) {
                    this.certs.put(string4, generateCertificate);
                }
            }
        }
    }
    
    @Override
    public void engineStore(final KeyStore.LoadStoreParameter loadStoreParameter) throws IOException, NoSuchAlgorithmException, CertificateException {
        if (loadStoreParameter == null) {
            throw new IllegalArgumentException("'param' arg cannot be null");
        }
        if (!(loadStoreParameter instanceof PKCS12StoreParameter) && !(loadStoreParameter instanceof JDKPKCS12StoreParameter)) {
            throw new IllegalArgumentException("No support for 'param' of type " + loadStoreParameter.getClass().getName());
        }
        PKCS12StoreParameter pkcs12StoreParameter;
        if (loadStoreParameter instanceof PKCS12StoreParameter) {
            pkcs12StoreParameter = (PKCS12StoreParameter)loadStoreParameter;
        }
        else {
            pkcs12StoreParameter = new PKCS12StoreParameter(((JDKPKCS12StoreParameter)loadStoreParameter).getOutputStream(), loadStoreParameter.getProtectionParameter(), ((JDKPKCS12StoreParameter)loadStoreParameter).isUseDEREncoding());
        }
        final KeyStore.ProtectionParameter protectionParameter = loadStoreParameter.getProtectionParameter();
        char[] password;
        if (protectionParameter == null) {
            password = null;
        }
        else {
            if (!(protectionParameter instanceof KeyStore.PasswordProtection)) {
                throw new IllegalArgumentException("No support for protection parameter of type " + ((KeyStore.PasswordProtection)protectionParameter).getClass().getName());
            }
            password = ((KeyStore.PasswordProtection)protectionParameter).getPassword();
        }
        this.doStore(pkcs12StoreParameter.getOutputStream(), password, pkcs12StoreParameter.isForDEREncoding());
    }
    
    @Override
    public void engineStore(final OutputStream outputStream, final char[] array) throws IOException {
        this.doStore(outputStream, array, false);
    }
    
    private void doStore(final OutputStream outputStream, final char[] array, final boolean b) throws IOException {
        if (array == null) {
            throw new NullPointerException("No password supplied for PKCS#12 KeyStore.");
        }
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final Enumeration keys = this.keys.keys();
        while (keys.hasMoreElements()) {
            final byte[] bytes = new byte[20];
            this.random.nextBytes(bytes);
            final String anObject = keys.nextElement();
            final PrivateKey privateKey = (PrivateKey)this.keys.get(anObject);
            final PKCS12PBEParams pkcs12PBEParams = new PKCS12PBEParams(bytes, 1024);
            final EncryptedPrivateKeyInfo encryptedPrivateKeyInfo = new EncryptedPrivateKeyInfo(new AlgorithmIdentifier(this.keyAlgorithm, pkcs12PBEParams.toASN1Primitive()), this.wrapKey(this.keyAlgorithm.getId(), privateKey, pkcs12PBEParams, array));
            boolean b2 = false;
            final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
            if (privateKey instanceof PKCS12BagAttributeCarrier) {
                final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier = (PKCS12BagAttributeCarrier)privateKey;
                final DERBMPString derbmpString = (DERBMPString)pkcs12BagAttributeCarrier.getBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName);
                if (derbmpString == null || !derbmpString.getString().equals(anObject)) {
                    pkcs12BagAttributeCarrier.setBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName, new DERBMPString(anObject));
                }
                if (pkcs12BagAttributeCarrier.getBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId) == null) {
                    pkcs12BagAttributeCarrier.setBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId, this.createSubjectKeyId(this.engineGetCertificate(anObject).getPublicKey()));
                }
                final Enumeration bagAttributeKeys = pkcs12BagAttributeCarrier.getBagAttributeKeys();
                while (bagAttributeKeys.hasMoreElements()) {
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = bagAttributeKeys.nextElement();
                    final ASN1EncodableVector asn1EncodableVector3 = new ASN1EncodableVector();
                    asn1EncodableVector3.add(asn1ObjectIdentifier);
                    asn1EncodableVector3.add(new DERSet(pkcs12BagAttributeCarrier.getBagAttribute(asn1ObjectIdentifier)));
                    b2 = true;
                    asn1EncodableVector2.add(new DERSequence(asn1EncodableVector3));
                }
            }
            if (!b2) {
                final ASN1EncodableVector asn1EncodableVector4 = new ASN1EncodableVector();
                final Certificate engineGetCertificate = this.engineGetCertificate(anObject);
                asn1EncodableVector4.add(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId);
                asn1EncodableVector4.add(new DERSet(this.createSubjectKeyId(engineGetCertificate.getPublicKey())));
                asn1EncodableVector2.add(new DERSequence(asn1EncodableVector4));
                final ASN1EncodableVector asn1EncodableVector5 = new ASN1EncodableVector();
                asn1EncodableVector5.add(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName);
                asn1EncodableVector5.add(new DERSet(new DERBMPString(anObject)));
                asn1EncodableVector2.add(new DERSequence(asn1EncodableVector5));
            }
            asn1EncodableVector.add(new SafeBag(PKCS12KeyStoreSpi.pkcs8ShroudedKeyBag, encryptedPrivateKeyInfo.toASN1Primitive(), new DERSet(asn1EncodableVector2)));
        }
        final BEROctetString berOctetString = new BEROctetString(new DERSequence(asn1EncodableVector).getEncoded("DER"));
        final byte[] bytes2 = new byte[20];
        this.random.nextBytes(bytes2);
        final ASN1EncodableVector asn1EncodableVector6 = new ASN1EncodableVector();
        final AlgorithmIdentifier algorithmIdentifier = new AlgorithmIdentifier(this.certAlgorithm, new PKCS12PBEParams(bytes2, 1024).toASN1Primitive());
        final Hashtable<Certificate, Certificate> hashtable = new Hashtable<Certificate, Certificate>();
        final Enumeration keys2 = this.keys.keys();
        while (keys2.hasMoreElements()) {
            try {
                final String anObject2 = keys2.nextElement();
                final Certificate engineGetCertificate2 = this.engineGetCertificate(anObject2);
                boolean b3 = false;
                final CertBag certBag = new CertBag(PKCS12KeyStoreSpi.x509Certificate, new DEROctetString(engineGetCertificate2.getEncoded()));
                final ASN1EncodableVector asn1EncodableVector7 = new ASN1EncodableVector();
                if (engineGetCertificate2 instanceof PKCS12BagAttributeCarrier) {
                    final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier2 = (PKCS12BagAttributeCarrier)engineGetCertificate2;
                    final DERBMPString derbmpString2 = (DERBMPString)pkcs12BagAttributeCarrier2.getBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName);
                    if (derbmpString2 == null || !derbmpString2.getString().equals(anObject2)) {
                        pkcs12BagAttributeCarrier2.setBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName, new DERBMPString(anObject2));
                    }
                    if (pkcs12BagAttributeCarrier2.getBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId) == null) {
                        pkcs12BagAttributeCarrier2.setBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId, this.createSubjectKeyId(engineGetCertificate2.getPublicKey()));
                    }
                    final Enumeration bagAttributeKeys2 = pkcs12BagAttributeCarrier2.getBagAttributeKeys();
                    while (bagAttributeKeys2.hasMoreElements()) {
                        final ASN1ObjectIdentifier asn1ObjectIdentifier2 = bagAttributeKeys2.nextElement();
                        final ASN1EncodableVector asn1EncodableVector8 = new ASN1EncodableVector();
                        asn1EncodableVector8.add(asn1ObjectIdentifier2);
                        asn1EncodableVector8.add(new DERSet(pkcs12BagAttributeCarrier2.getBagAttribute(asn1ObjectIdentifier2)));
                        asn1EncodableVector7.add(new DERSequence(asn1EncodableVector8));
                        b3 = true;
                    }
                }
                if (!b3) {
                    final ASN1EncodableVector asn1EncodableVector9 = new ASN1EncodableVector();
                    asn1EncodableVector9.add(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId);
                    asn1EncodableVector9.add(new DERSet(this.createSubjectKeyId(engineGetCertificate2.getPublicKey())));
                    asn1EncodableVector7.add(new DERSequence(asn1EncodableVector9));
                    final ASN1EncodableVector asn1EncodableVector10 = new ASN1EncodableVector();
                    asn1EncodableVector10.add(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName);
                    asn1EncodableVector10.add(new DERSet(new DERBMPString(anObject2)));
                    asn1EncodableVector7.add(new DERSequence(asn1EncodableVector10));
                }
                asn1EncodableVector6.add(new SafeBag(PKCS12KeyStoreSpi.certBag, certBag.toASN1Primitive(), new DERSet(asn1EncodableVector7)));
                hashtable.put(engineGetCertificate2, engineGetCertificate2);
                continue;
            }
            catch (CertificateEncodingException ex) {
                throw new IOException("Error encoding certificate: " + ex.toString());
            }
            break;
        }
        final Enumeration keys3 = this.certs.keys();
        while (keys3.hasMoreElements()) {
            try {
                final String anObject3 = keys3.nextElement();
                final Certificate certificate = (Certificate)this.certs.get(anObject3);
                boolean b4 = false;
                if (this.keys.get(anObject3) != null) {
                    continue;
                }
                final CertBag certBag2 = new CertBag(PKCS12KeyStoreSpi.x509Certificate, new DEROctetString(certificate.getEncoded()));
                final ASN1EncodableVector asn1EncodableVector11 = new ASN1EncodableVector();
                if (certificate instanceof PKCS12BagAttributeCarrier) {
                    final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier3 = (PKCS12BagAttributeCarrier)certificate;
                    final DERBMPString derbmpString3 = (DERBMPString)pkcs12BagAttributeCarrier3.getBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName);
                    if (derbmpString3 == null || !derbmpString3.getString().equals(anObject3)) {
                        pkcs12BagAttributeCarrier3.setBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName, new DERBMPString(anObject3));
                    }
                    final Enumeration bagAttributeKeys3 = pkcs12BagAttributeCarrier3.getBagAttributeKeys();
                    while (bagAttributeKeys3.hasMoreElements()) {
                        final ASN1ObjectIdentifier asn1ObjectIdentifier3 = bagAttributeKeys3.nextElement();
                        if (asn1ObjectIdentifier3.equals(PKCSObjectIdentifiers.pkcs_9_at_localKeyId)) {
                            continue;
                        }
                        final ASN1EncodableVector asn1EncodableVector12 = new ASN1EncodableVector();
                        asn1EncodableVector12.add(asn1ObjectIdentifier3);
                        asn1EncodableVector12.add(new DERSet(pkcs12BagAttributeCarrier3.getBagAttribute(asn1ObjectIdentifier3)));
                        asn1EncodableVector11.add(new DERSequence(asn1EncodableVector12));
                        b4 = true;
                    }
                }
                if (!b4) {
                    final ASN1EncodableVector asn1EncodableVector13 = new ASN1EncodableVector();
                    asn1EncodableVector13.add(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName);
                    asn1EncodableVector13.add(new DERSet(new DERBMPString(anObject3)));
                    asn1EncodableVector11.add(new DERSequence(asn1EncodableVector13));
                }
                asn1EncodableVector6.add(new SafeBag(PKCS12KeyStoreSpi.certBag, certBag2.toASN1Primitive(), new DERSet(asn1EncodableVector11)));
                hashtable.put(certificate, certificate);
                continue;
            }
            catch (CertificateEncodingException ex2) {
                throw new IOException("Error encoding certificate: " + ex2.toString());
            }
            break;
        }
        final Enumeration<CertId> keys4 = (Enumeration<CertId>)this.chainCerts.keys();
        while (keys4.hasMoreElements()) {
            try {
                final Certificate key = this.chainCerts.get(keys4.nextElement());
                if (hashtable.get(key) != null) {
                    continue;
                }
                final CertBag certBag3 = new CertBag(PKCS12KeyStoreSpi.x509Certificate, new DEROctetString(key.getEncoded()));
                final ASN1EncodableVector asn1EncodableVector14 = new ASN1EncodableVector();
                if (key instanceof PKCS12BagAttributeCarrier) {
                    final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier4 = (PKCS12BagAttributeCarrier)key;
                    final Enumeration bagAttributeKeys4 = pkcs12BagAttributeCarrier4.getBagAttributeKeys();
                    while (bagAttributeKeys4.hasMoreElements()) {
                        final ASN1ObjectIdentifier asn1ObjectIdentifier4 = bagAttributeKeys4.nextElement();
                        if (asn1ObjectIdentifier4.equals(PKCSObjectIdentifiers.pkcs_9_at_localKeyId)) {
                            continue;
                        }
                        final ASN1EncodableVector asn1EncodableVector15 = new ASN1EncodableVector();
                        asn1EncodableVector15.add(asn1ObjectIdentifier4);
                        asn1EncodableVector15.add(new DERSet(pkcs12BagAttributeCarrier4.getBagAttribute(asn1ObjectIdentifier4)));
                        asn1EncodableVector14.add(new DERSequence(asn1EncodableVector15));
                    }
                }
                asn1EncodableVector6.add(new SafeBag(PKCS12KeyStoreSpi.certBag, certBag3.toASN1Primitive(), new DERSet(asn1EncodableVector14)));
                continue;
            }
            catch (CertificateEncodingException ex3) {
                throw new IOException("Error encoding certificate: " + ex3.toString());
            }
            break;
        }
        final AuthenticatedSafe authenticatedSafe = new AuthenticatedSafe(new ContentInfo[] { new ContentInfo(PKCS12KeyStoreSpi.data, berOctetString), new ContentInfo(PKCS12KeyStoreSpi.encryptedData, new EncryptedData(PKCS12KeyStoreSpi.data, algorithmIdentifier, new BEROctetString(this.cryptData(true, algorithmIdentifier, array, false, new DERSequence(asn1EncodableVector6).getEncoded("DER")))).toASN1Primitive()) });
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DEROutputStream derOutputStream;
        if (b) {
            derOutputStream = new DEROutputStream(byteArrayOutputStream);
        }
        else {
            derOutputStream = new BEROutputStream(byteArrayOutputStream);
        }
        derOutputStream.writeObject(authenticatedSafe);
        final ContentInfo contentInfo = new ContentInfo(PKCS12KeyStoreSpi.data, new BEROctetString(byteArrayOutputStream.toByteArray()));
        final byte[] bytes3 = new byte[20];
        final int n = 1024;
        this.random.nextBytes(bytes3);
        final byte[] octets = ((ASN1OctetString)contentInfo.getContent()).getOctets();
        MacData macData;
        try {
            macData = new MacData(new DigestInfo(new AlgorithmIdentifier(PKCS12KeyStoreSpi.id_SHA1, DERNull.INSTANCE), calculatePbeMac(PKCS12KeyStoreSpi.id_SHA1, bytes3, n, array, false, octets)), bytes3, n);
        }
        catch (Exception ex4) {
            throw new IOException("error constructing MAC: " + ex4.toString());
        }
        final Pfx pfx = new Pfx(contentInfo, macData);
        DEROutputStream derOutputStream2;
        if (b) {
            derOutputStream2 = new DEROutputStream(outputStream);
        }
        else {
            derOutputStream2 = new BEROutputStream(outputStream);
        }
        derOutputStream2.writeObject(pfx);
    }
    
    private static byte[] calculatePbeMac(final ASN1ObjectIdentifier asn1ObjectIdentifier, final byte[] salt, final int iterationCount, final char[] password, final boolean tryWrongPKCS12Zero, final byte[] input) throws Exception {
        final SecretKeyFactory instance = SecretKeyFactory.getInstance(asn1ObjectIdentifier.getId(), PKCS12KeyStoreSpi.bcProvider);
        final PBEParameterSpec params = new PBEParameterSpec(salt, iterationCount);
        final BCPBEKey key = (BCPBEKey)instance.generateSecret(new PBEKeySpec(password));
        key.setTryWrongPKCS12Zero(tryWrongPKCS12Zero);
        final Mac instance2 = Mac.getInstance(asn1ObjectIdentifier.getId(), PKCS12KeyStoreSpi.bcProvider);
        instance2.init(key, params);
        instance2.update(input);
        return instance2.doFinal();
    }
    
    static {
        bcProvider = new BouncyCastleProvider();
    }
    
    public static class BCPKCS12KeyStore extends PKCS12KeyStoreSpi
    {
        public BCPKCS12KeyStore() {
            super(PKCS12KeyStoreSpi.bcProvider, BCPKCS12KeyStore.pbeWithSHAAnd3_KeyTripleDES_CBC, BCPKCS12KeyStore.pbeWithSHAAnd40BitRC2_CBC);
        }
    }
    
    public static class BCPKCS12KeyStore3DES extends PKCS12KeyStoreSpi
    {
        public BCPKCS12KeyStore3DES() {
            super(PKCS12KeyStoreSpi.bcProvider, BCPKCS12KeyStore3DES.pbeWithSHAAnd3_KeyTripleDES_CBC, BCPKCS12KeyStore3DES.pbeWithSHAAnd3_KeyTripleDES_CBC);
        }
    }
    
    private class CertId
    {
        byte[] id;
        
        CertId(final PublicKey publicKey) {
            this.id = PKCS12KeyStoreSpi.this.createSubjectKeyId(publicKey).getKeyIdentifier();
        }
        
        CertId(final byte[] id) {
            this.id = id;
        }
        
        @Override
        public int hashCode() {
            return Arrays.hashCode(this.id);
        }
        
        @Override
        public boolean equals(final Object o) {
            return o == this || (o instanceof CertId && Arrays.areEqual(this.id, ((CertId)o).id));
        }
    }
    
    public static class DefPKCS12KeyStore extends PKCS12KeyStoreSpi
    {
        public DefPKCS12KeyStore() {
            super(null, DefPKCS12KeyStore.pbeWithSHAAnd3_KeyTripleDES_CBC, DefPKCS12KeyStore.pbeWithSHAAnd40BitRC2_CBC);
        }
    }
    
    public static class DefPKCS12KeyStore3DES extends PKCS12KeyStoreSpi
    {
        public DefPKCS12KeyStore3DES() {
            super(null, DefPKCS12KeyStore3DES.pbeWithSHAAnd3_KeyTripleDES_CBC, DefPKCS12KeyStore3DES.pbeWithSHAAnd3_KeyTripleDES_CBC);
        }
    }
    
    private static class IgnoresCaseHashtable
    {
        private Hashtable orig;
        private Hashtable keys;
        
        private IgnoresCaseHashtable() {
            this.orig = new Hashtable();
            this.keys = new Hashtable();
        }
        
        public void put(final String s, final Object value) {
            final String s2 = (s == null) ? null : Strings.toLowerCase(s);
            final String key = this.keys.get(s2);
            if (key != null) {
                this.orig.remove(key);
            }
            this.keys.put(s2, s);
            this.orig.put(s, value);
        }
        
        public Enumeration keys() {
            return this.orig.keys();
        }
        
        public Object remove(final String s) {
            final String key = this.keys.remove((s == null) ? null : Strings.toLowerCase(s));
            if (key == null) {
                return null;
            }
            return this.orig.remove(key);
        }
        
        public Object get(final String s) {
            final String key = this.keys.get((s == null) ? null : Strings.toLowerCase(s));
            if (key == null) {
                return null;
            }
            return this.orig.get(key);
        }
        
        public Enumeration elements() {
            return this.orig.elements();
        }
    }
}
