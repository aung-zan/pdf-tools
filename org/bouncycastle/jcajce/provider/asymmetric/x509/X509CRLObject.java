// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.x509;

import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.security.cert.Certificate;
import java.util.Iterator;
import org.bouncycastle.asn1.util.ASN1Dump;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.CRLNumber;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.util.encoders.Hex;
import java.util.Collections;
import java.security.cert.X509CRLEntry;
import java.math.BigInteger;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.TBSCertList;
import java.util.Date;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.asn1.x500.X500Name;
import java.security.Principal;
import java.security.Signature;
import java.security.SignatureException;
import java.security.NoSuchProviderException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.io.IOException;
import java.util.Enumeration;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.HashSet;
import java.util.Set;
import org.bouncycastle.jce.provider.RFC3280CertPathUtilities;
import java.security.cert.CRLException;
import org.bouncycastle.asn1.x509.IssuingDistributionPoint;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.CertificateList;
import java.security.cert.X509CRL;

class X509CRLObject extends X509CRL
{
    private CertificateList c;
    private String sigAlgName;
    private byte[] sigAlgParams;
    private boolean isIndirect;
    
    static boolean isIndirectCRL(final X509CRL x509CRL) throws CRLException {
        try {
            final byte[] extensionValue = x509CRL.getExtensionValue(Extension.issuingDistributionPoint.getId());
            return extensionValue != null && IssuingDistributionPoint.getInstance(ASN1OctetString.getInstance(extensionValue).getOctets()).isIndirectCRL();
        }
        catch (Exception ex) {
            throw new ExtCRLException("Exception reading IssuingDistributionPoint", ex);
        }
    }
    
    public X509CRLObject(final CertificateList c) throws CRLException {
        this.c = c;
        try {
            this.sigAlgName = X509SignatureUtil.getSignatureName(c.getSignatureAlgorithm());
            if (c.getSignatureAlgorithm().getParameters() != null) {
                this.sigAlgParams = c.getSignatureAlgorithm().getParameters().toASN1Primitive().getEncoded("DER");
            }
            else {
                this.sigAlgParams = null;
            }
            this.isIndirect = isIndirectCRL(this);
        }
        catch (Exception obj) {
            throw new CRLException("CRL contents invalid: " + obj);
        }
    }
    
    public boolean hasUnsupportedCriticalExtension() {
        final Set criticalExtensionOIDs = this.getCriticalExtensionOIDs();
        if (criticalExtensionOIDs == null) {
            return false;
        }
        criticalExtensionOIDs.remove(RFC3280CertPathUtilities.ISSUING_DISTRIBUTION_POINT);
        criticalExtensionOIDs.remove(RFC3280CertPathUtilities.DELTA_CRL_INDICATOR);
        return !criticalExtensionOIDs.isEmpty();
    }
    
    private Set getExtensionOIDs(final boolean b) {
        if (this.getVersion() == 2) {
            final Extensions extensions = this.c.getTBSCertList().getExtensions();
            if (extensions != null) {
                final HashSet<String> set = new HashSet<String>();
                final Enumeration oids = extensions.oids();
                while (oids.hasMoreElements()) {
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = oids.nextElement();
                    if (b == extensions.getExtension(asn1ObjectIdentifier).isCritical()) {
                        set.add(asn1ObjectIdentifier.getId());
                    }
                }
                return set;
            }
        }
        return null;
    }
    
    public Set getCriticalExtensionOIDs() {
        return this.getExtensionOIDs(true);
    }
    
    public Set getNonCriticalExtensionOIDs() {
        return this.getExtensionOIDs(false);
    }
    
    public byte[] getExtensionValue(final String s) {
        final Extensions extensions = this.c.getTBSCertList().getExtensions();
        if (extensions != null) {
            final Extension extension = extensions.getExtension(new ASN1ObjectIdentifier(s));
            if (extension != null) {
                try {
                    return extension.getExtnValue().getEncoded();
                }
                catch (Exception ex) {
                    throw new IllegalStateException("error parsing " + ex.toString());
                }
            }
        }
        return null;
    }
    
    @Override
    public byte[] getEncoded() throws CRLException {
        try {
            return this.c.getEncoded("DER");
        }
        catch (IOException ex) {
            throw new CRLException(ex.toString());
        }
    }
    
    @Override
    public void verify(final PublicKey publicKey) throws CRLException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException {
        this.verify(publicKey, "BC");
    }
    
    @Override
    public void verify(final PublicKey publicKey, final String provider) throws CRLException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException {
        if (!this.c.getSignatureAlgorithm().equals(this.c.getTBSCertList().getSignature())) {
            throw new CRLException("Signature algorithm on CertificateList does not match TBSCertList.");
        }
        Signature signature;
        if (provider != null) {
            signature = Signature.getInstance(this.getSigAlgName(), provider);
        }
        else {
            signature = Signature.getInstance(this.getSigAlgName());
        }
        signature.initVerify(publicKey);
        signature.update(this.getTBSCertList());
        if (!signature.verify(this.getSignature())) {
            throw new SignatureException("CRL does not verify with supplied public key.");
        }
    }
    
    @Override
    public int getVersion() {
        return this.c.getVersionNumber();
    }
    
    @Override
    public Principal getIssuerDN() {
        return new X509Principal(X500Name.getInstance(this.c.getIssuer().toASN1Primitive()));
    }
    
    @Override
    public X500Principal getIssuerX500Principal() {
        try {
            return new X500Principal(this.c.getIssuer().getEncoded());
        }
        catch (IOException ex) {
            throw new IllegalStateException("can't encode issuer DN");
        }
    }
    
    @Override
    public Date getThisUpdate() {
        return this.c.getThisUpdate().getDate();
    }
    
    @Override
    public Date getNextUpdate() {
        if (this.c.getNextUpdate() != null) {
            return this.c.getNextUpdate().getDate();
        }
        return null;
    }
    
    private Set loadCRLEntries() {
        final HashSet<X509CRLEntryObject> set = new HashSet<X509CRLEntryObject>();
        final Enumeration revokedCertificateEnumeration = this.c.getRevokedCertificateEnumeration();
        X500Name instance = null;
        while (revokedCertificateEnumeration.hasMoreElements()) {
            final TBSCertList.CRLEntry crlEntry = revokedCertificateEnumeration.nextElement();
            set.add(new X509CRLEntryObject(crlEntry, this.isIndirect, instance));
            if (this.isIndirect && crlEntry.hasExtensions()) {
                final Extension extension = crlEntry.getExtensions().getExtension(Extension.certificateIssuer);
                if (extension == null) {
                    continue;
                }
                instance = X500Name.getInstance(GeneralNames.getInstance(extension.getParsedValue()).getNames()[0].getName());
            }
        }
        return set;
    }
    
    @Override
    public X509CRLEntry getRevokedCertificate(final BigInteger bigInteger) {
        final Enumeration revokedCertificateEnumeration = this.c.getRevokedCertificateEnumeration();
        X500Name instance = null;
        while (revokedCertificateEnumeration.hasMoreElements()) {
            final TBSCertList.CRLEntry crlEntry = revokedCertificateEnumeration.nextElement();
            if (bigInteger.equals(crlEntry.getUserCertificate().getValue())) {
                return new X509CRLEntryObject(crlEntry, this.isIndirect, instance);
            }
            if (!this.isIndirect || !crlEntry.hasExtensions()) {
                continue;
            }
            final Extension extension = crlEntry.getExtensions().getExtension(Extension.certificateIssuer);
            if (extension == null) {
                continue;
            }
            instance = X500Name.getInstance(GeneralNames.getInstance(extension.getParsedValue()).getNames()[0].getName());
        }
        return null;
    }
    
    @Override
    public Set getRevokedCertificates() {
        final Set loadCRLEntries = this.loadCRLEntries();
        if (!loadCRLEntries.isEmpty()) {
            return Collections.unmodifiableSet((Set<?>)loadCRLEntries);
        }
        return null;
    }
    
    @Override
    public byte[] getTBSCertList() throws CRLException {
        try {
            return this.c.getTBSCertList().getEncoded("DER");
        }
        catch (IOException ex) {
            throw new CRLException(ex.toString());
        }
    }
    
    @Override
    public byte[] getSignature() {
        return this.c.getSignature().getBytes();
    }
    
    @Override
    public String getSigAlgName() {
        return this.sigAlgName;
    }
    
    @Override
    public String getSigAlgOID() {
        return this.c.getSignatureAlgorithm().getAlgorithm().getId();
    }
    
    @Override
    public byte[] getSigAlgParams() {
        if (this.sigAlgParams != null) {
            final byte[] array = new byte[this.sigAlgParams.length];
            System.arraycopy(this.sigAlgParams, 0, array, 0, array.length);
            return array;
        }
        return null;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        final String property = System.getProperty("line.separator");
        sb.append("              Version: ").append(this.getVersion()).append(property);
        sb.append("             IssuerDN: ").append(this.getIssuerDN()).append(property);
        sb.append("          This update: ").append(this.getThisUpdate()).append(property);
        sb.append("          Next update: ").append(this.getNextUpdate()).append(property);
        sb.append("  Signature Algorithm: ").append(this.getSigAlgName()).append(property);
        final byte[] signature = this.getSignature();
        sb.append("            Signature: ").append(new String(Hex.encode(signature, 0, 20))).append(property);
        for (int i = 20; i < signature.length; i += 20) {
            if (i < signature.length - 20) {
                sb.append("                       ").append(new String(Hex.encode(signature, i, 20))).append(property);
            }
            else {
                sb.append("                       ").append(new String(Hex.encode(signature, i, signature.length - i))).append(property);
            }
        }
        final Extensions extensions = this.c.getTBSCertList().getExtensions();
        if (extensions != null) {
            final Enumeration oids = extensions.oids();
            if (oids.hasMoreElements()) {
                sb.append("           Extensions: ").append(property);
            }
            while (oids.hasMoreElements()) {
                final ASN1ObjectIdentifier asn1ObjectIdentifier = oids.nextElement();
                final Extension extension = extensions.getExtension(asn1ObjectIdentifier);
                if (extension.getExtnValue() != null) {
                    final ASN1InputStream asn1InputStream = new ASN1InputStream(extension.getExtnValue().getOctets());
                    sb.append("                       critical(").append(extension.isCritical()).append(") ");
                    try {
                        if (asn1ObjectIdentifier.equals(Extension.cRLNumber)) {
                            sb.append(new CRLNumber(DERInteger.getInstance(asn1InputStream.readObject()).getPositiveValue())).append(property);
                        }
                        else if (asn1ObjectIdentifier.equals(Extension.deltaCRLIndicator)) {
                            sb.append("Base CRL: " + new CRLNumber(DERInteger.getInstance(asn1InputStream.readObject()).getPositiveValue())).append(property);
                        }
                        else if (asn1ObjectIdentifier.equals(Extension.issuingDistributionPoint)) {
                            sb.append(IssuingDistributionPoint.getInstance(asn1InputStream.readObject())).append(property);
                        }
                        else if (asn1ObjectIdentifier.equals(Extension.cRLDistributionPoints)) {
                            sb.append(CRLDistPoint.getInstance(asn1InputStream.readObject())).append(property);
                        }
                        else if (asn1ObjectIdentifier.equals(Extension.freshestCRL)) {
                            sb.append(CRLDistPoint.getInstance(asn1InputStream.readObject())).append(property);
                        }
                        else {
                            sb.append(asn1ObjectIdentifier.getId());
                            sb.append(" value = ").append(ASN1Dump.dumpAsString(asn1InputStream.readObject())).append(property);
                        }
                    }
                    catch (Exception ex) {
                        sb.append(asn1ObjectIdentifier.getId());
                        sb.append(" value = ").append("*****").append(property);
                    }
                }
                else {
                    sb.append(property);
                }
            }
        }
        final Set revokedCertificates = this.getRevokedCertificates();
        if (revokedCertificates != null) {
            final Iterator<Object> iterator = revokedCertificates.iterator();
            while (iterator.hasNext()) {
                sb.append(iterator.next());
                sb.append(property);
            }
        }
        return sb.toString();
    }
    
    @Override
    public boolean isRevoked(final Certificate certificate) {
        if (!certificate.getType().equals("X.509")) {
            throw new RuntimeException("X.509 CRL used with non X.509 Cert");
        }
        final TBSCertList.CRLEntry[] revokedCertificates = this.c.getRevokedCertificates();
        X500Name x500Name = this.c.getIssuer();
        if (revokedCertificates != null) {
            final BigInteger serialNumber = ((X509Certificate)certificate).getSerialNumber();
            for (int i = 0; i < revokedCertificates.length; ++i) {
                if (this.isIndirect && revokedCertificates[i].hasExtensions()) {
                    final Extension extension = revokedCertificates[i].getExtensions().getExtension(Extension.certificateIssuer);
                    if (extension != null) {
                        x500Name = X500Name.getInstance(GeneralNames.getInstance(extension.getParsedValue()).getNames()[0].getName());
                    }
                }
                if (revokedCertificates[i].getUserCertificate().getValue().equals(serialNumber)) {
                    X500Name x500Name2;
                    if (certificate instanceof X509Certificate) {
                        x500Name2 = X500Name.getInstance(((X509Certificate)certificate).getIssuerX500Principal().getEncoded());
                    }
                    else {
                        try {
                            x500Name2 = org.bouncycastle.asn1.x509.Certificate.getInstance(certificate.getEncoded()).getIssuer();
                        }
                        catch (CertificateEncodingException ex) {
                            throw new RuntimeException("Cannot process certificate");
                        }
                    }
                    return x500Name.equals(x500Name2);
                }
            }
        }
        return false;
    }
}
