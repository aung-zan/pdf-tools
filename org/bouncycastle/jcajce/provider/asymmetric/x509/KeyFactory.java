// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.x509;

import java.security.InvalidKeyException;
import java.security.Key;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.security.spec.X509EncodedKeySpec;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.PrivateKey;
import java.security.spec.KeySpec;
import java.security.KeyFactorySpi;

public class KeyFactory extends KeyFactorySpi
{
    @Override
    protected PrivateKey engineGeneratePrivate(final KeySpec keySpec) throws InvalidKeySpecException {
        if (keySpec instanceof PKCS8EncodedKeySpec) {
            try {
                final PrivateKeyInfo instance = PrivateKeyInfo.getInstance(((PKCS8EncodedKeySpec)keySpec).getEncoded());
                final PrivateKey privateKey = BouncyCastleProvider.getPrivateKey(instance);
                if (privateKey != null) {
                    return privateKey;
                }
                throw new InvalidKeySpecException("no factory found for OID: " + instance.getPrivateKeyAlgorithm().getAlgorithm());
            }
            catch (Exception ex) {
                throw new InvalidKeySpecException(ex.toString());
            }
        }
        throw new InvalidKeySpecException("Unknown KeySpec type: " + keySpec.getClass().getName());
    }
    
    @Override
    protected PublicKey engineGeneratePublic(final KeySpec keySpec) throws InvalidKeySpecException {
        if (keySpec instanceof X509EncodedKeySpec) {
            try {
                final SubjectPublicKeyInfo instance = SubjectPublicKeyInfo.getInstance(((X509EncodedKeySpec)keySpec).getEncoded());
                final PublicKey publicKey = BouncyCastleProvider.getPublicKey(instance);
                if (publicKey != null) {
                    return publicKey;
                }
                throw new InvalidKeySpecException("no factory found for OID: " + instance.getAlgorithm().getAlgorithm());
            }
            catch (Exception ex) {
                throw new InvalidKeySpecException(ex.toString());
            }
        }
        throw new InvalidKeySpecException("Unknown KeySpec type: " + keySpec.getClass().getName());
    }
    
    @Override
    protected KeySpec engineGetKeySpec(final Key obj, final Class obj2) throws InvalidKeySpecException {
        if (obj2.isAssignableFrom(PKCS8EncodedKeySpec.class) && obj.getFormat().equals("PKCS#8")) {
            return new PKCS8EncodedKeySpec(obj.getEncoded());
        }
        if (obj2.isAssignableFrom(X509EncodedKeySpec.class) && obj.getFormat().equals("X.509")) {
            return new X509EncodedKeySpec(obj.getEncoded());
        }
        throw new InvalidKeySpecException("not implemented yet " + obj + " " + obj2);
    }
    
    @Override
    protected Key engineTranslateKey(final Key obj) throws InvalidKeyException {
        throw new InvalidKeyException("not implemented yet " + obj);
    }
}
