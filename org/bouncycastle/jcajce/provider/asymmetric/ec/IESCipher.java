// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.ec;

import org.bouncycastle.crypto.engines.DESedeEngine;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.DerivationFunction;
import org.bouncycastle.crypto.BasicAgreement;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.generators.KDF2BytesGenerator;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.agreement.ECDHBasicAgreement;
import javax.crypto.ShortBufferException;
import javax.crypto.IllegalBlockSizeException;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.KeyParser;
import org.bouncycastle.crypto.parsers.ECIESPublicKeyParser;
import org.bouncycastle.crypto.AsymmetricCipherKeyPairGenerator;
import org.bouncycastle.crypto.generators.EphemeralKeyPairGenerator;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.KeyEncoder;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import javax.crypto.BadPaddingException;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ECKeyParameters;
import org.bouncycastle.crypto.params.IESWithCipherParameters;
import java.security.PrivateKey;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.jce.interfaces.IESKey;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import java.security.PublicKey;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.jcajce.provider.asymmetric.util.IESUtil;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.util.Strings;
import java.security.spec.AlgorithmParameterSpec;
import org.bouncycastle.jce.interfaces.ECKey;
import java.security.Key;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.jce.spec.IESParameterSpec;
import java.security.AlgorithmParameters;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.crypto.engines.IESEngine;
import javax.crypto.CipherSpi;

public class IESCipher extends CipherSpi
{
    private IESEngine engine;
    private int state;
    private ByteArrayOutputStream buffer;
    private AlgorithmParameters engineParam;
    private IESParameterSpec engineSpec;
    private AsymmetricKeyParameter key;
    private SecureRandom random;
    private boolean dhaesMode;
    private AsymmetricKeyParameter otherKeyParameter;
    
    public IESCipher(final IESEngine engine) {
        this.state = -1;
        this.buffer = new ByteArrayOutputStream();
        this.engineParam = null;
        this.engineSpec = null;
        this.dhaesMode = false;
        this.otherKeyParameter = null;
        this.engine = engine;
    }
    
    public int engineGetBlockSize() {
        if (this.engine.getCipher() != null) {
            return this.engine.getCipher().getBlockSize();
        }
        return 0;
    }
    
    public int engineGetKeySize(final Key key) {
        if (key instanceof ECKey) {
            return ((ECKey)key).getParameters().getCurve().getFieldSize();
        }
        throw new IllegalArgumentException("not an EC key");
    }
    
    public byte[] engineGetIV() {
        return null;
    }
    
    public AlgorithmParameters engineGetParameters() {
        if (this.engineParam == null && this.engineSpec != null) {
            try {
                (this.engineParam = AlgorithmParameters.getInstance("IES", "BC")).init(this.engineSpec);
            }
            catch (Exception ex) {
                throw new RuntimeException(ex.toString());
            }
        }
        return this.engineParam;
    }
    
    public void engineSetMode(final String str) throws NoSuchAlgorithmException {
        final String upperCase = Strings.toUpperCase(str);
        if (upperCase.equals("NONE")) {
            this.dhaesMode = false;
        }
        else {
            if (!upperCase.equals("DHAES")) {
                throw new IllegalArgumentException("can't support mode " + str);
            }
            this.dhaesMode = true;
        }
    }
    
    public int engineGetOutputSize(final int n) {
        final int macSize = this.engine.getMac().getMacSize();
        if (this.key == null) {
            throw new IllegalStateException("cipher not initialised");
        }
        final int n2 = 1 + 2 * (((ECKey)this.key).getParameters().getCurve().getFieldSize() + 7) / 8;
        int n3;
        if (this.engine.getCipher() == null) {
            n3 = n;
        }
        else if (this.state == 1 || this.state == 3) {
            n3 = this.engine.getCipher().getOutputSize(n);
        }
        else {
            if (this.state != 2 && this.state != 4) {
                throw new IllegalStateException("cipher not initialised");
            }
            n3 = this.engine.getCipher().getOutputSize(n - macSize - n2);
        }
        if (this.state == 1 || this.state == 3) {
            return this.buffer.size() + macSize + n2 + n3;
        }
        if (this.state == 2 || this.state == 4) {
            return this.buffer.size() - macSize - n2 + n3;
        }
        throw new IllegalStateException("cipher not initialised");
    }
    
    public void engineSetPadding(final String s) throws NoSuchPaddingException {
        final String upperCase = Strings.toUpperCase(s);
        if (!upperCase.equals("NOPADDING")) {
            if (!upperCase.equals("PKCS5PADDING")) {
                if (!upperCase.equals("PKCS7PADDING")) {
                    throw new NoSuchPaddingException("padding not available with IESCipher");
                }
            }
        }
    }
    
    public void engineInit(final int n, final Key key, final AlgorithmParameters engineParam, final SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        AlgorithmParameterSpec parameterSpec = null;
        if (engineParam != null) {
            try {
                parameterSpec = engineParam.getParameterSpec(IESParameterSpec.class);
            }
            catch (Exception ex) {
                throw new InvalidAlgorithmParameterException("cannot recognise parameters: " + ex.toString());
            }
        }
        this.engineParam = engineParam;
        this.engineInit(n, key, parameterSpec, secureRandom);
    }
    
    public void engineInit(final int state, final Key key, final AlgorithmParameterSpec algorithmParameterSpec, final SecureRandom random) throws InvalidAlgorithmParameterException, InvalidKeyException {
        this.otherKeyParameter = null;
        if (algorithmParameterSpec == null) {
            this.engineSpec = IESUtil.guessParameterSpec(this.engine);
        }
        else {
            if (!(algorithmParameterSpec instanceof IESParameterSpec)) {
                throw new InvalidAlgorithmParameterException("must be passed IES parameters");
            }
            this.engineSpec = (IESParameterSpec)algorithmParameterSpec;
        }
        if (state == 1 || state == 3) {
            if (key instanceof ECPublicKey) {
                this.key = ECUtil.generatePublicKeyParameter((PublicKey)key);
            }
            else {
                if (!(key instanceof IESKey)) {
                    throw new InvalidKeyException("must be passed recipient's public EC key for encryption");
                }
                final IESKey iesKey = (IESKey)key;
                this.key = ECUtil.generatePublicKeyParameter(iesKey.getPublic());
                this.otherKeyParameter = ECUtil.generatePrivateKeyParameter(iesKey.getPrivate());
            }
        }
        else {
            if (state != 2 && state != 4) {
                throw new InvalidKeyException("must be passed EC key");
            }
            if (key instanceof ECPrivateKey) {
                this.key = ECUtil.generatePrivateKeyParameter((PrivateKey)key);
            }
            else {
                if (!(key instanceof IESKey)) {
                    throw new InvalidKeyException("must be passed recipient's private EC key for decryption");
                }
                final IESKey iesKey2 = (IESKey)key;
                this.otherKeyParameter = ECUtil.generatePublicKeyParameter(iesKey2.getPublic());
                this.key = ECUtil.generatePrivateKeyParameter(iesKey2.getPrivate());
            }
        }
        this.random = random;
        this.state = state;
        this.buffer.reset();
    }
    
    public void engineInit(final int n, final Key key, final SecureRandom secureRandom) throws InvalidKeyException {
        try {
            this.engineInit(n, key, (AlgorithmParameterSpec)null, secureRandom);
        }
        catch (InvalidAlgorithmParameterException ex) {
            throw new IllegalArgumentException("can't handle supplied parameter spec");
        }
    }
    
    public byte[] engineUpdate(final byte[] b, final int off, final int len) {
        this.buffer.write(b, off, len);
        return null;
    }
    
    public int engineUpdate(final byte[] b, final int off, final int len, final byte[] array, final int n) {
        this.buffer.write(b, off, len);
        return 0;
    }
    
    public byte[] engineDoFinal(final byte[] b, final int off, final int len) throws IllegalBlockSizeException, BadPaddingException {
        if (len != 0) {
            this.buffer.write(b, off, len);
        }
        final byte[] byteArray = this.buffer.toByteArray();
        this.buffer.reset();
        final IESWithCipherParameters iesWithCipherParameters = new IESWithCipherParameters(this.engineSpec.getDerivationV(), this.engineSpec.getEncodingV(), this.engineSpec.getMacKeySize(), this.engineSpec.getCipherKeySize());
        final ECDomainParameters parameters = ((ECKeyParameters)this.key).getParameters();
        if (this.otherKeyParameter != null) {
            try {
                if (this.state == 1 || this.state == 3) {
                    this.engine.init(true, this.otherKeyParameter, this.key, iesWithCipherParameters);
                }
                else {
                    this.engine.init(false, this.key, this.otherKeyParameter, iesWithCipherParameters);
                }
                return this.engine.processBlock(byteArray, 0, byteArray.length);
            }
            catch (Exception ex) {
                throw new BadPaddingException(ex.getMessage());
            }
        }
        if (this.state == 1 || this.state == 3) {
            final ECKeyPairGenerator ecKeyPairGenerator = new ECKeyPairGenerator();
            ecKeyPairGenerator.init(new ECKeyGenerationParameters(parameters, this.random));
            final EphemeralKeyPairGenerator ephemeralKeyPairGenerator = new EphemeralKeyPairGenerator(ecKeyPairGenerator, new KeyEncoder() {
                public byte[] getEncoded(final AsymmetricKeyParameter asymmetricKeyParameter) {
                    return ((ECPublicKeyParameters)asymmetricKeyParameter).getQ().getEncoded();
                }
            });
            try {
                this.engine.init(this.key, iesWithCipherParameters, ephemeralKeyPairGenerator);
                return this.engine.processBlock(byteArray, 0, byteArray.length);
            }
            catch (Exception ex2) {
                throw new BadPaddingException(ex2.getMessage());
            }
        }
        if (this.state != 2) {
            if (this.state != 4) {
                throw new IllegalStateException("cipher not initialised");
            }
        }
        try {
            this.engine.init(this.key, iesWithCipherParameters, new ECIESPublicKeyParser(parameters));
            return this.engine.processBlock(byteArray, 0, byteArray.length);
        }
        catch (InvalidCipherTextException ex3) {
            throw new BadPaddingException(ex3.getMessage());
        }
        throw new IllegalStateException("cipher not initialised");
    }
    
    public int engineDoFinal(final byte[] array, final int n, final int n2, final byte[] array2, final int n3) throws ShortBufferException, IllegalBlockSizeException, BadPaddingException {
        final byte[] engineDoFinal = this.engineDoFinal(array, n, n2);
        System.arraycopy(engineDoFinal, 0, array2, n3, engineDoFinal.length);
        return engineDoFinal.length;
    }
    
    public static class ECIES extends IESCipher
    {
        public ECIES() {
            super(new IESEngine(new ECDHBasicAgreement(), new KDF2BytesGenerator(new SHA1Digest()), new HMac(new SHA1Digest())));
        }
    }
    
    public static class ECIESwithAES extends IESCipher
    {
        public ECIESwithAES() {
            super(new IESEngine(new ECDHBasicAgreement(), new KDF2BytesGenerator(new SHA1Digest()), new HMac(new SHA1Digest()), new PaddedBufferedBlockCipher(new AESEngine())));
        }
    }
    
    public static class ECIESwithDESede extends IESCipher
    {
        public ECIESwithDESede() {
            super(new IESEngine(new ECDHBasicAgreement(), new KDF2BytesGenerator(new SHA1Digest()), new HMac(new SHA1Digest()), new PaddedBufferedBlockCipher(new DESedeEngine())));
        }
    }
}
