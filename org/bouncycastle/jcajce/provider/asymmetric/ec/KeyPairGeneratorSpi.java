// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.ec;

import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import java.security.KeyPair;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.teletrust.TeleTrusTNamedCurves;
import org.bouncycastle.asn1.nist.NISTNamedCurves;
import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X962NamedCurves;
import org.bouncycastle.jce.spec.ECNamedCurveGenParameterSpec;
import java.math.BigInteger;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.jce.spec.ECParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidParameterException;
import java.security.spec.AlgorithmParameterSpec;
import org.bouncycastle.util.Integers;
import java.security.spec.ECGenParameterSpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.util.Hashtable;
import org.bouncycastle.jcajce.provider.config.ProviderConfiguration;
import java.security.SecureRandom;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import java.security.KeyPairGenerator;

public abstract class KeyPairGeneratorSpi extends KeyPairGenerator
{
    public KeyPairGeneratorSpi(final String algorithm) {
        super(algorithm);
    }
    
    public static class EC extends KeyPairGeneratorSpi
    {
        ECKeyGenerationParameters param;
        ECKeyPairGenerator engine;
        Object ecParams;
        int strength;
        int certainty;
        SecureRandom random;
        boolean initialised;
        String algorithm;
        ProviderConfiguration configuration;
        private static Hashtable ecParameters;
        
        public EC() {
            super("EC");
            this.engine = new ECKeyPairGenerator();
            this.ecParams = null;
            this.strength = 239;
            this.certainty = 50;
            this.random = new SecureRandom();
            this.initialised = false;
            this.algorithm = "EC";
            this.configuration = BouncyCastleProvider.CONFIGURATION;
        }
        
        public EC(final String algorithm, final ProviderConfiguration configuration) {
            super(algorithm);
            this.engine = new ECKeyPairGenerator();
            this.ecParams = null;
            this.strength = 239;
            this.certainty = 50;
            this.random = new SecureRandom();
            this.initialised = false;
            this.algorithm = algorithm;
            this.configuration = configuration;
        }
        
        @Override
        public void initialize(final int strength, final SecureRandom random) {
            this.strength = strength;
            this.random = random;
            final ECGenParameterSpec ecGenParameterSpec = EC.ecParameters.get(Integers.valueOf(strength));
            if (ecGenParameterSpec != null) {
                try {
                    this.initialize(ecGenParameterSpec, random);
                    return;
                }
                catch (InvalidAlgorithmParameterException ex) {
                    throw new InvalidParameterException("key size not configurable.");
                }
                throw new InvalidParameterException("unknown key size.");
            }
            throw new InvalidParameterException("unknown key size.");
        }
        
        @Override
        public void initialize(final AlgorithmParameterSpec ecParams, final SecureRandom secureRandom) throws InvalidAlgorithmParameterException {
            if (ecParams instanceof ECParameterSpec) {
                final ECParameterSpec ecParameterSpec = (ECParameterSpec)ecParams;
                this.ecParams = ecParams;
                this.param = new ECKeyGenerationParameters(new ECDomainParameters(ecParameterSpec.getCurve(), ecParameterSpec.getG(), ecParameterSpec.getN()), secureRandom);
                this.engine.init(this.param);
                this.initialised = true;
            }
            else if (ecParams instanceof java.security.spec.ECParameterSpec) {
                final java.security.spec.ECParameterSpec ecParameterSpec2 = (java.security.spec.ECParameterSpec)ecParams;
                this.ecParams = ecParams;
                final ECCurve convertCurve = EC5Util.convertCurve(ecParameterSpec2.getCurve());
                this.param = new ECKeyGenerationParameters(new ECDomainParameters(convertCurve, EC5Util.convertPoint(convertCurve, ecParameterSpec2.getGenerator(), false), ecParameterSpec2.getOrder(), BigInteger.valueOf(ecParameterSpec2.getCofactor())), secureRandom);
                this.engine.init(this.param);
                this.initialised = true;
            }
            else if (ecParams instanceof ECGenParameterSpec || ecParams instanceof ECNamedCurveGenParameterSpec) {
                String s;
                if (ecParams instanceof ECGenParameterSpec) {
                    s = ((ECGenParameterSpec)ecParams).getName();
                }
                else {
                    s = ((ECNamedCurveGenParameterSpec)ecParams).getName();
                }
                X9ECParameters x9ECParameters = X962NamedCurves.getByName(s);
                if (x9ECParameters == null) {
                    x9ECParameters = SECNamedCurves.getByName(s);
                    if (x9ECParameters == null) {
                        x9ECParameters = NISTNamedCurves.getByName(s);
                    }
                    if (x9ECParameters == null) {
                        x9ECParameters = TeleTrusTNamedCurves.getByName(s);
                    }
                    if (x9ECParameters == null) {
                        try {
                            final ASN1ObjectIdentifier asn1ObjectIdentifier = new ASN1ObjectIdentifier(s);
                            x9ECParameters = X962NamedCurves.getByOID(asn1ObjectIdentifier);
                            if (x9ECParameters == null) {
                                x9ECParameters = SECNamedCurves.getByOID(asn1ObjectIdentifier);
                            }
                            if (x9ECParameters == null) {
                                x9ECParameters = NISTNamedCurves.getByOID(asn1ObjectIdentifier);
                            }
                            if (x9ECParameters == null) {
                                x9ECParameters = TeleTrusTNamedCurves.getByOID(asn1ObjectIdentifier);
                            }
                            if (x9ECParameters == null) {
                                throw new InvalidAlgorithmParameterException("unknown curve OID: " + s);
                            }
                        }
                        catch (IllegalArgumentException ex) {
                            throw new InvalidAlgorithmParameterException("unknown curve name: " + s);
                        }
                    }
                }
                this.ecParams = new ECNamedCurveSpec(s, x9ECParameters.getCurve(), x9ECParameters.getG(), x9ECParameters.getN(), x9ECParameters.getH(), null);
                final java.security.spec.ECParameterSpec ecParameterSpec3 = (java.security.spec.ECParameterSpec)this.ecParams;
                final ECCurve convertCurve2 = EC5Util.convertCurve(ecParameterSpec3.getCurve());
                this.param = new ECKeyGenerationParameters(new ECDomainParameters(convertCurve2, EC5Util.convertPoint(convertCurve2, ecParameterSpec3.getGenerator(), false), ecParameterSpec3.getOrder(), BigInteger.valueOf(ecParameterSpec3.getCofactor())), secureRandom);
                this.engine.init(this.param);
                this.initialised = true;
            }
            else if (ecParams == null && this.configuration.getEcImplicitlyCa() != null) {
                final ECParameterSpec ecImplicitlyCa = this.configuration.getEcImplicitlyCa();
                this.ecParams = ecParams;
                this.param = new ECKeyGenerationParameters(new ECDomainParameters(ecImplicitlyCa.getCurve(), ecImplicitlyCa.getG(), ecImplicitlyCa.getN()), secureRandom);
                this.engine.init(this.param);
                this.initialised = true;
            }
            else {
                if (ecParams == null && this.configuration.getEcImplicitlyCa() == null) {
                    throw new InvalidAlgorithmParameterException("null parameter passed but no implicitCA set");
                }
                throw new InvalidAlgorithmParameterException("parameter object not a ECParameterSpec");
            }
        }
        
        @Override
        public KeyPair generateKeyPair() {
            if (!this.initialised) {
                this.initialize(this.strength, new SecureRandom());
            }
            final AsymmetricCipherKeyPair generateKeyPair = this.engine.generateKeyPair();
            final ECPublicKeyParameters ecPublicKeyParameters = (ECPublicKeyParameters)generateKeyPair.getPublic();
            final ECPrivateKeyParameters ecPrivateKeyParameters = (ECPrivateKeyParameters)generateKeyPair.getPrivate();
            if (this.ecParams instanceof ECParameterSpec) {
                final ECParameterSpec ecParameterSpec = (ECParameterSpec)this.ecParams;
                final BCECPublicKey publicKey = new BCECPublicKey(this.algorithm, ecPublicKeyParameters, ecParameterSpec, this.configuration);
                return new KeyPair(publicKey, new BCECPrivateKey(this.algorithm, ecPrivateKeyParameters, publicKey, ecParameterSpec, this.configuration));
            }
            if (this.ecParams == null) {
                return new KeyPair(new BCECPublicKey(this.algorithm, ecPublicKeyParameters, this.configuration), new BCECPrivateKey(this.algorithm, ecPrivateKeyParameters, this.configuration));
            }
            final java.security.spec.ECParameterSpec ecParameterSpec2 = (java.security.spec.ECParameterSpec)this.ecParams;
            final BCECPublicKey publicKey2 = new BCECPublicKey(this.algorithm, ecPublicKeyParameters, ecParameterSpec2, this.configuration);
            return new KeyPair(publicKey2, new BCECPrivateKey(this.algorithm, ecPrivateKeyParameters, publicKey2, ecParameterSpec2, this.configuration));
        }
        
        static {
            (EC.ecParameters = new Hashtable()).put(Integers.valueOf(192), new ECGenParameterSpec("prime192v1"));
            EC.ecParameters.put(Integers.valueOf(239), new ECGenParameterSpec("prime239v1"));
            EC.ecParameters.put(Integers.valueOf(256), new ECGenParameterSpec("prime256v1"));
            EC.ecParameters.put(Integers.valueOf(224), new ECGenParameterSpec("P-224"));
            EC.ecParameters.put(Integers.valueOf(384), new ECGenParameterSpec("P-384"));
            EC.ecParameters.put(Integers.valueOf(521), new ECGenParameterSpec("P-521"));
        }
    }
    
    public static class ECDH extends EC
    {
        public ECDH() {
            super("ECDH", BouncyCastleProvider.CONFIGURATION);
        }
    }
    
    public static class ECDHC extends EC
    {
        public ECDHC() {
            super("ECDHC", BouncyCastleProvider.CONFIGURATION);
        }
    }
    
    public static class ECDSA extends EC
    {
        public ECDSA() {
            super("ECDSA", BouncyCastleProvider.CONFIGURATION);
        }
    }
    
    public static class ECMQV extends EC
    {
        public ECMQV() {
            super("ECMQV", BouncyCastleProvider.CONFIGURATION);
        }
    }
}
