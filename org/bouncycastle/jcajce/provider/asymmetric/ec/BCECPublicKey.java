// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.ec;

import java.io.ObjectOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.io.ObjectInputStream;
import org.bouncycastle.jcajce.provider.asymmetric.util.KeyUtil;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import java.math.BigInteger;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.asn1.x9.X9ECPoint;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x9.X9IntegerConverter;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x9.X962Parameters;
import org.bouncycastle.asn1.ASN1Primitive;
import java.security.spec.EllipticCurve;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import java.security.spec.ECPublicKeySpec;
import org.bouncycastle.jcajce.provider.config.ProviderConfiguration;
import java.security.spec.ECParameterSpec;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.jce.interfaces.ECPointEncoder;
import java.security.interfaces.ECPublicKey;

public class BCECPublicKey implements ECPublicKey, org.bouncycastle.jce.interfaces.ECPublicKey, ECPointEncoder
{
    static final long serialVersionUID = 2422789860422731812L;
    private String algorithm;
    private boolean withCompression;
    private transient ECPoint q;
    private transient ECParameterSpec ecSpec;
    private transient ProviderConfiguration configuration;
    
    public BCECPublicKey(final String algorithm, final BCECPublicKey bcecPublicKey) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        this.q = bcecPublicKey.q;
        this.ecSpec = bcecPublicKey.ecSpec;
        this.withCompression = bcecPublicKey.withCompression;
        this.configuration = bcecPublicKey.configuration;
    }
    
    public BCECPublicKey(final String algorithm, final ECPublicKeySpec ecPublicKeySpec, final ProviderConfiguration configuration) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        this.ecSpec = ecPublicKeySpec.getParams();
        this.q = EC5Util.convertPoint(this.ecSpec, ecPublicKeySpec.getW(), false);
        this.configuration = configuration;
    }
    
    public BCECPublicKey(final String algorithm, final org.bouncycastle.jce.spec.ECPublicKeySpec ecPublicKeySpec, final ProviderConfiguration configuration) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        this.q = ecPublicKeySpec.getQ();
        if (ecPublicKeySpec.getParams() != null) {
            this.ecSpec = EC5Util.convertSpec(EC5Util.convertCurve(ecPublicKeySpec.getParams().getCurve(), ecPublicKeySpec.getParams().getSeed()), ecPublicKeySpec.getParams());
        }
        else {
            if (this.q.getCurve() == null) {
                this.q = configuration.getEcImplicitlyCa().getCurve().createPoint(this.q.getX().toBigInteger(), this.q.getY().toBigInteger(), false);
            }
            this.ecSpec = null;
        }
        this.configuration = configuration;
    }
    
    public BCECPublicKey(final String algorithm, final ECPublicKeyParameters ecPublicKeyParameters, final ECParameterSpec ecSpec, final ProviderConfiguration configuration) {
        this.algorithm = "EC";
        final ECDomainParameters parameters = ecPublicKeyParameters.getParameters();
        this.algorithm = algorithm;
        this.q = ecPublicKeyParameters.getQ();
        if (ecSpec == null) {
            this.ecSpec = this.createSpec(EC5Util.convertCurve(parameters.getCurve(), parameters.getSeed()), parameters);
        }
        else {
            this.ecSpec = ecSpec;
        }
        this.configuration = configuration;
    }
    
    public BCECPublicKey(final String algorithm, final ECPublicKeyParameters ecPublicKeyParameters, final org.bouncycastle.jce.spec.ECParameterSpec ecParameterSpec, final ProviderConfiguration configuration) {
        this.algorithm = "EC";
        final ECDomainParameters parameters = ecPublicKeyParameters.getParameters();
        this.algorithm = algorithm;
        this.q = ecPublicKeyParameters.getQ();
        if (ecParameterSpec == null) {
            this.ecSpec = this.createSpec(EC5Util.convertCurve(parameters.getCurve(), parameters.getSeed()), parameters);
        }
        else {
            this.ecSpec = EC5Util.convertSpec(EC5Util.convertCurve(ecParameterSpec.getCurve(), ecParameterSpec.getSeed()), ecParameterSpec);
        }
        this.configuration = configuration;
    }
    
    public BCECPublicKey(final String algorithm, final ECPublicKeyParameters ecPublicKeyParameters, final ProviderConfiguration configuration) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        this.q = ecPublicKeyParameters.getQ();
        this.ecSpec = null;
        this.configuration = configuration;
    }
    
    public BCECPublicKey(final ECPublicKey ecPublicKey, final ProviderConfiguration providerConfiguration) {
        this.algorithm = "EC";
        this.algorithm = ecPublicKey.getAlgorithm();
        this.ecSpec = ecPublicKey.getParams();
        this.q = EC5Util.convertPoint(this.ecSpec, ecPublicKey.getW(), false);
    }
    
    BCECPublicKey(final String algorithm, final SubjectPublicKeyInfo subjectPublicKeyInfo, final ProviderConfiguration configuration) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        this.configuration = configuration;
        this.populateFromPubKeyInfo(subjectPublicKeyInfo);
    }
    
    private ECParameterSpec createSpec(final EllipticCurve curve, final ECDomainParameters ecDomainParameters) {
        return new ECParameterSpec(curve, new java.security.spec.ECPoint(ecDomainParameters.getG().getX().toBigInteger(), ecDomainParameters.getG().getY().toBigInteger()), ecDomainParameters.getN(), ecDomainParameters.getH().intValue());
    }
    
    private void populateFromPubKeyInfo(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        final X962Parameters x962Parameters = new X962Parameters((ASN1Primitive)subjectPublicKeyInfo.getAlgorithm().getParameters());
        ECCurve ecCurve;
        if (x962Parameters.isNamedCurve()) {
            final ASN1ObjectIdentifier asn1ObjectIdentifier = (ASN1ObjectIdentifier)x962Parameters.getParameters();
            final X9ECParameters namedCurveByOid = ECUtil.getNamedCurveByOid(asn1ObjectIdentifier);
            ecCurve = namedCurveByOid.getCurve();
            this.ecSpec = new ECNamedCurveSpec(ECUtil.getCurveName(asn1ObjectIdentifier), EC5Util.convertCurve(ecCurve, namedCurveByOid.getSeed()), new java.security.spec.ECPoint(namedCurveByOid.getG().getX().toBigInteger(), namedCurveByOid.getG().getY().toBigInteger()), namedCurveByOid.getN(), namedCurveByOid.getH());
        }
        else if (x962Parameters.isImplicitlyCA()) {
            this.ecSpec = null;
            ecCurve = this.configuration.getEcImplicitlyCa().getCurve();
        }
        else {
            final X9ECParameters instance = X9ECParameters.getInstance(x962Parameters.getParameters());
            ecCurve = instance.getCurve();
            this.ecSpec = new ECParameterSpec(EC5Util.convertCurve(ecCurve, instance.getSeed()), new java.security.spec.ECPoint(instance.getG().getX().toBigInteger(), instance.getG().getY().toBigInteger()), instance.getN(), instance.getH().intValue());
        }
        final byte[] bytes = subjectPublicKeyInfo.getPublicKeyData().getBytes();
        ASN1OctetString asn1OctetString = new DEROctetString(bytes);
        if (bytes[0] == 4 && bytes[1] == bytes.length - 2 && (bytes[2] == 2 || bytes[2] == 3) && new X9IntegerConverter().getByteLength(ecCurve) >= bytes.length - 3) {
            try {
                asn1OctetString = (ASN1OctetString)ASN1Primitive.fromByteArray(bytes);
            }
            catch (IOException ex) {
                throw new IllegalArgumentException("error recovering public key");
            }
        }
        this.q = new X9ECPoint(ecCurve, asn1OctetString).getPoint();
    }
    
    public String getAlgorithm() {
        return this.algorithm;
    }
    
    public String getFormat() {
        return "X.509";
    }
    
    public byte[] getEncoded() {
        X962Parameters x962Parameters;
        if (this.ecSpec instanceof ECNamedCurveSpec) {
            ASN1ObjectIdentifier namedCurveOid = ECUtil.getNamedCurveOid(((ECNamedCurveSpec)this.ecSpec).getName());
            if (namedCurveOid == null) {
                namedCurveOid = new ASN1ObjectIdentifier(((ECNamedCurveSpec)this.ecSpec).getName());
            }
            x962Parameters = new X962Parameters(namedCurveOid);
        }
        else if (this.ecSpec == null) {
            x962Parameters = new X962Parameters(DERNull.INSTANCE);
        }
        else {
            final ECCurve convertCurve = EC5Util.convertCurve(this.ecSpec.getCurve());
            x962Parameters = new X962Parameters(new X9ECParameters(convertCurve, EC5Util.convertPoint(convertCurve, this.ecSpec.getGenerator(), this.withCompression), this.ecSpec.getOrder(), BigInteger.valueOf(this.ecSpec.getCofactor()), this.ecSpec.getCurve().getSeed()));
        }
        return KeyUtil.getEncodedSubjectPublicKeyInfo(new SubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, x962Parameters), ((ASN1OctetString)new X9ECPoint(this.engineGetQ().getCurve().createPoint(this.getQ().getX().toBigInteger(), this.getQ().getY().toBigInteger(), this.withCompression)).toASN1Primitive()).getOctets()));
    }
    
    private void extractBytes(final byte[] array, final int n, final BigInteger bigInteger) {
        byte[] byteArray = bigInteger.toByteArray();
        if (byteArray.length < 32) {
            final byte[] array2 = new byte[32];
            System.arraycopy(byteArray, 0, array2, array2.length - byteArray.length, byteArray.length);
            byteArray = array2;
        }
        for (int i = 0; i != 32; ++i) {
            array[n + i] = byteArray[byteArray.length - 1 - i];
        }
    }
    
    public ECParameterSpec getParams() {
        return this.ecSpec;
    }
    
    public org.bouncycastle.jce.spec.ECParameterSpec getParameters() {
        if (this.ecSpec == null) {
            return null;
        }
        return EC5Util.convertSpec(this.ecSpec, this.withCompression);
    }
    
    public java.security.spec.ECPoint getW() {
        return new java.security.spec.ECPoint(this.q.getX().toBigInteger(), this.q.getY().toBigInteger());
    }
    
    public ECPoint getQ() {
        if (this.ecSpec != null) {
            return this.q;
        }
        if (this.q instanceof ECPoint.Fp) {
            return new ECPoint.Fp(null, this.q.getX(), this.q.getY());
        }
        return new ECPoint.F2m(null, this.q.getX(), this.q.getY());
    }
    
    public ECPoint engineGetQ() {
        return this.q;
    }
    
    org.bouncycastle.jce.spec.ECParameterSpec engineGetSpec() {
        if (this.ecSpec != null) {
            return EC5Util.convertSpec(this.ecSpec, this.withCompression);
        }
        return this.configuration.getEcImplicitlyCa();
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        final String property = System.getProperty("line.separator");
        sb.append("EC Public Key").append(property);
        sb.append("            X: ").append(this.q.getX().toBigInteger().toString(16)).append(property);
        sb.append("            Y: ").append(this.q.getY().toBigInteger().toString(16)).append(property);
        return sb.toString();
    }
    
    public void setPointFormat(final String anotherString) {
        this.withCompression = !"UNCOMPRESSED".equalsIgnoreCase(anotherString);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof BCECPublicKey)) {
            return false;
        }
        final BCECPublicKey bcecPublicKey = (BCECPublicKey)o;
        return this.engineGetQ().equals(bcecPublicKey.engineGetQ()) && this.engineGetSpec().equals(bcecPublicKey.engineGetSpec());
    }
    
    @Override
    public int hashCode() {
        return this.engineGetQ().hashCode() ^ this.engineGetSpec().hashCode();
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.populateFromPubKeyInfo(SubjectPublicKeyInfo.getInstance(ASN1Primitive.fromByteArray((byte[])objectInputStream.readObject())));
        this.configuration = BouncyCastleProvider.CONFIGURATION;
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
}
