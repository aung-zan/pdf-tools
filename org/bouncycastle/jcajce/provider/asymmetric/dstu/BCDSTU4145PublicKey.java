// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.dstu;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.jcajce.provider.asymmetric.util.KeyUtil;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ua.DSTU4145BinaryField;
import org.bouncycastle.asn1.ua.DSTU4145ECBinary;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.x9.X9ECPoint;
import org.bouncycastle.asn1.x9.X9IntegerConverter;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x9.X962Parameters;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.bouncycastle.asn1.ua.DSTU4145PointEncoder;
import org.bouncycastle.math.ec.ECCurve;
import java.math.BigInteger;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.asn1.ua.DSTU4145NamedCurves;
import org.bouncycastle.asn1.ASN1Sequence;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ua.UAObjectIdentifiers;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.security.spec.EllipticCurve;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import java.security.spec.ECPublicKeySpec;
import org.bouncycastle.asn1.ua.DSTU4145Params;
import java.security.spec.ECParameterSpec;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.jce.interfaces.ECPointEncoder;
import java.security.interfaces.ECPublicKey;

public class BCDSTU4145PublicKey implements ECPublicKey, org.bouncycastle.jce.interfaces.ECPublicKey, ECPointEncoder
{
    static final long serialVersionUID = 7026240464295649314L;
    private String algorithm;
    private boolean withCompression;
    private transient ECPoint q;
    private transient ECParameterSpec ecSpec;
    private transient DSTU4145Params dstuParams;
    
    public BCDSTU4145PublicKey(final BCDSTU4145PublicKey bcdstu4145PublicKey) {
        this.algorithm = "DSTU4145";
        this.q = bcdstu4145PublicKey.q;
        this.ecSpec = bcdstu4145PublicKey.ecSpec;
        this.withCompression = bcdstu4145PublicKey.withCompression;
        this.dstuParams = bcdstu4145PublicKey.dstuParams;
    }
    
    public BCDSTU4145PublicKey(final ECPublicKeySpec ecPublicKeySpec) {
        this.algorithm = "DSTU4145";
        this.ecSpec = ecPublicKeySpec.getParams();
        this.q = EC5Util.convertPoint(this.ecSpec, ecPublicKeySpec.getW(), false);
    }
    
    public BCDSTU4145PublicKey(final org.bouncycastle.jce.spec.ECPublicKeySpec ecPublicKeySpec) {
        this.algorithm = "DSTU4145";
        this.q = ecPublicKeySpec.getQ();
        if (ecPublicKeySpec.getParams() != null) {
            this.ecSpec = EC5Util.convertSpec(EC5Util.convertCurve(ecPublicKeySpec.getParams().getCurve(), ecPublicKeySpec.getParams().getSeed()), ecPublicKeySpec.getParams());
        }
        else {
            if (this.q.getCurve() == null) {
                this.q = BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa().getCurve().createPoint(this.q.getX().toBigInteger(), this.q.getY().toBigInteger(), false);
            }
            this.ecSpec = null;
        }
    }
    
    public BCDSTU4145PublicKey(final String algorithm, final ECPublicKeyParameters ecPublicKeyParameters, final ECParameterSpec ecSpec) {
        this.algorithm = "DSTU4145";
        final ECDomainParameters parameters = ecPublicKeyParameters.getParameters();
        this.algorithm = algorithm;
        this.q = ecPublicKeyParameters.getQ();
        if (ecSpec == null) {
            this.ecSpec = this.createSpec(EC5Util.convertCurve(parameters.getCurve(), parameters.getSeed()), parameters);
        }
        else {
            this.ecSpec = ecSpec;
        }
    }
    
    public BCDSTU4145PublicKey(final String algorithm, final ECPublicKeyParameters ecPublicKeyParameters, final org.bouncycastle.jce.spec.ECParameterSpec ecParameterSpec) {
        this.algorithm = "DSTU4145";
        final ECDomainParameters parameters = ecPublicKeyParameters.getParameters();
        this.algorithm = algorithm;
        this.q = ecPublicKeyParameters.getQ();
        if (ecParameterSpec == null) {
            this.ecSpec = this.createSpec(EC5Util.convertCurve(parameters.getCurve(), parameters.getSeed()), parameters);
        }
        else {
            this.ecSpec = EC5Util.convertSpec(EC5Util.convertCurve(ecParameterSpec.getCurve(), ecParameterSpec.getSeed()), ecParameterSpec);
        }
    }
    
    public BCDSTU4145PublicKey(final String algorithm, final ECPublicKeyParameters ecPublicKeyParameters) {
        this.algorithm = "DSTU4145";
        this.algorithm = algorithm;
        this.q = ecPublicKeyParameters.getQ();
        this.ecSpec = null;
    }
    
    private ECParameterSpec createSpec(final EllipticCurve curve, final ECDomainParameters ecDomainParameters) {
        return new ECParameterSpec(curve, new java.security.spec.ECPoint(ecDomainParameters.getG().getX().toBigInteger(), ecDomainParameters.getG().getY().toBigInteger()), ecDomainParameters.getN(), ecDomainParameters.getH().intValue());
    }
    
    public BCDSTU4145PublicKey(final ECPublicKey ecPublicKey) {
        this.algorithm = "DSTU4145";
        this.algorithm = ecPublicKey.getAlgorithm();
        this.ecSpec = ecPublicKey.getParams();
        this.q = EC5Util.convertPoint(this.ecSpec, ecPublicKey.getW(), false);
    }
    
    BCDSTU4145PublicKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        this.algorithm = "DSTU4145";
        this.populateFromPubKeyInfo(subjectPublicKeyInfo);
    }
    
    private void reverseBytes(final byte[] array) {
        for (int i = 0; i < array.length / 2; ++i) {
            final byte b = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = b;
        }
    }
    
    private void populateFromPubKeyInfo(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        if (subjectPublicKeyInfo.getAlgorithm().getAlgorithm().equals(UAObjectIdentifiers.dstu4145be) || subjectPublicKeyInfo.getAlgorithm().getAlgorithm().equals(UAObjectIdentifiers.dstu4145le)) {
            final DERBitString publicKeyData = subjectPublicKeyInfo.getPublicKeyData();
            this.algorithm = "DSTU4145";
            ASN1OctetString asn1OctetString;
            try {
                asn1OctetString = (ASN1OctetString)ASN1Primitive.fromByteArray(publicKeyData.getBytes());
            }
            catch (IOException ex) {
                throw new IllegalArgumentException("error recovering public key");
            }
            final byte[] octets = asn1OctetString.getOctets();
            if (subjectPublicKeyInfo.getAlgorithm().getAlgorithm().equals(UAObjectIdentifiers.dstu4145le)) {
                this.reverseBytes(octets);
            }
            this.dstuParams = DSTU4145Params.getInstance(subjectPublicKeyInfo.getAlgorithm().getParameters());
            org.bouncycastle.jce.spec.ECParameterSpec ecParameterSpec;
            if (this.dstuParams.isNamedCurve()) {
                final ASN1ObjectIdentifier namedCurve = this.dstuParams.getNamedCurve();
                final ECDomainParameters byOID = DSTU4145NamedCurves.getByOID(namedCurve);
                ecParameterSpec = new ECNamedCurveParameterSpec(namedCurve.getId(), byOID.getCurve(), byOID.getG(), byOID.getN(), byOID.getH(), byOID.getSeed());
            }
            else {
                final DSTU4145ECBinary ecBinary = this.dstuParams.getECBinary();
                final byte[] b = ecBinary.getB();
                if (subjectPublicKeyInfo.getAlgorithm().getAlgorithm().equals(UAObjectIdentifiers.dstu4145le)) {
                    this.reverseBytes(b);
                }
                final DSTU4145BinaryField field = ecBinary.getField();
                final ECCurve.F2m f2m = new ECCurve.F2m(field.getM(), field.getK1(), field.getK2(), field.getK3(), ecBinary.getA(), new BigInteger(1, b));
                final byte[] g = ecBinary.getG();
                if (subjectPublicKeyInfo.getAlgorithm().getAlgorithm().equals(UAObjectIdentifiers.dstu4145le)) {
                    this.reverseBytes(g);
                }
                ecParameterSpec = new org.bouncycastle.jce.spec.ECParameterSpec(f2m, DSTU4145PointEncoder.decodePoint(f2m, g), ecBinary.getN());
            }
            final ECCurve curve = ecParameterSpec.getCurve();
            final EllipticCurve convertCurve = EC5Util.convertCurve(curve, ecParameterSpec.getSeed());
            this.q = DSTU4145PointEncoder.decodePoint(curve, octets);
            if (this.dstuParams.isNamedCurve()) {
                this.ecSpec = new ECNamedCurveSpec(this.dstuParams.getNamedCurve().getId(), convertCurve, new java.security.spec.ECPoint(ecParameterSpec.getG().getX().toBigInteger(), ecParameterSpec.getG().getY().toBigInteger()), ecParameterSpec.getN(), ecParameterSpec.getH());
            }
            else {
                this.ecSpec = new ECParameterSpec(convertCurve, new java.security.spec.ECPoint(ecParameterSpec.getG().getX().toBigInteger(), ecParameterSpec.getG().getY().toBigInteger()), ecParameterSpec.getN(), ecParameterSpec.getH().intValue());
            }
        }
        else {
            final X962Parameters x962Parameters = new X962Parameters((ASN1Primitive)subjectPublicKeyInfo.getAlgorithm().getParameters());
            ECCurve ecCurve;
            if (x962Parameters.isNamedCurve()) {
                final ASN1ObjectIdentifier asn1ObjectIdentifier = (ASN1ObjectIdentifier)x962Parameters.getParameters();
                final X9ECParameters namedCurveByOid = ECUtil.getNamedCurveByOid(asn1ObjectIdentifier);
                ecCurve = namedCurveByOid.getCurve();
                this.ecSpec = new ECNamedCurveSpec(ECUtil.getCurveName(asn1ObjectIdentifier), EC5Util.convertCurve(ecCurve, namedCurveByOid.getSeed()), new java.security.spec.ECPoint(namedCurveByOid.getG().getX().toBigInteger(), namedCurveByOid.getG().getY().toBigInteger()), namedCurveByOid.getN(), namedCurveByOid.getH());
            }
            else if (x962Parameters.isImplicitlyCA()) {
                this.ecSpec = null;
                ecCurve = BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa().getCurve();
            }
            else {
                final X9ECParameters instance = X9ECParameters.getInstance(x962Parameters.getParameters());
                ecCurve = instance.getCurve();
                this.ecSpec = new ECParameterSpec(EC5Util.convertCurve(ecCurve, instance.getSeed()), new java.security.spec.ECPoint(instance.getG().getX().toBigInteger(), instance.getG().getY().toBigInteger()), instance.getN(), instance.getH().intValue());
            }
            final byte[] bytes = subjectPublicKeyInfo.getPublicKeyData().getBytes();
            ASN1OctetString asn1OctetString2 = new DEROctetString(bytes);
            if (bytes[0] == 4 && bytes[1] == bytes.length - 2 && (bytes[2] == 2 || bytes[2] == 3) && new X9IntegerConverter().getByteLength(ecCurve) >= bytes.length - 3) {
                try {
                    asn1OctetString2 = (ASN1OctetString)ASN1Primitive.fromByteArray(bytes);
                }
                catch (IOException ex2) {
                    throw new IllegalArgumentException("error recovering public key");
                }
            }
            this.q = new X9ECPoint(ecCurve, asn1OctetString2).getPoint();
        }
    }
    
    public byte[] getSbox() {
        if (null != this.dstuParams) {
            return this.dstuParams.getDKE();
        }
        return DSTU4145Params.getDefaultDKE();
    }
    
    public String getAlgorithm() {
        return this.algorithm;
    }
    
    public String getFormat() {
        return "X.509";
    }
    
    public byte[] getEncoded() {
        SubjectPublicKeyInfo subjectPublicKeyInfo;
        if (this.algorithm.equals("DSTU4145")) {
            ASN1Object dstuParams;
            if (this.dstuParams != null) {
                dstuParams = this.dstuParams;
            }
            else if (this.ecSpec instanceof ECNamedCurveSpec) {
                dstuParams = new DSTU4145Params(new ASN1ObjectIdentifier(((ECNamedCurveSpec)this.ecSpec).getName()));
            }
            else {
                final ECCurve convertCurve = EC5Util.convertCurve(this.ecSpec.getCurve());
                dstuParams = new X962Parameters(new X9ECParameters(convertCurve, EC5Util.convertPoint(convertCurve, this.ecSpec.getGenerator(), this.withCompression), this.ecSpec.getOrder(), BigInteger.valueOf(this.ecSpec.getCofactor()), this.ecSpec.getCurve().getSeed()));
            }
            final byte[] encodePoint = DSTU4145PointEncoder.encodePoint(this.q);
            try {
                subjectPublicKeyInfo = new SubjectPublicKeyInfo(new AlgorithmIdentifier(UAObjectIdentifiers.dstu4145be, dstuParams), new DEROctetString(encodePoint));
            }
            catch (IOException ex) {
                return null;
            }
        }
        else {
            X962Parameters x962Parameters;
            if (this.ecSpec instanceof ECNamedCurveSpec) {
                ASN1ObjectIdentifier namedCurveOid = ECUtil.getNamedCurveOid(((ECNamedCurveSpec)this.ecSpec).getName());
                if (namedCurveOid == null) {
                    namedCurveOid = new ASN1ObjectIdentifier(((ECNamedCurveSpec)this.ecSpec).getName());
                }
                x962Parameters = new X962Parameters(namedCurveOid);
            }
            else if (this.ecSpec == null) {
                x962Parameters = new X962Parameters(DERNull.INSTANCE);
            }
            else {
                final ECCurve convertCurve2 = EC5Util.convertCurve(this.ecSpec.getCurve());
                x962Parameters = new X962Parameters(new X9ECParameters(convertCurve2, EC5Util.convertPoint(convertCurve2, this.ecSpec.getGenerator(), this.withCompression), this.ecSpec.getOrder(), BigInteger.valueOf(this.ecSpec.getCofactor()), this.ecSpec.getCurve().getSeed()));
            }
            subjectPublicKeyInfo = new SubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, x962Parameters), ((ASN1OctetString)new X9ECPoint(this.engineGetQ().getCurve().createPoint(this.getQ().getX().toBigInteger(), this.getQ().getY().toBigInteger(), this.withCompression)).toASN1Primitive()).getOctets());
        }
        return KeyUtil.getEncodedSubjectPublicKeyInfo(subjectPublicKeyInfo);
    }
    
    public ECParameterSpec getParams() {
        return this.ecSpec;
    }
    
    public org.bouncycastle.jce.spec.ECParameterSpec getParameters() {
        if (this.ecSpec == null) {
            return null;
        }
        return EC5Util.convertSpec(this.ecSpec, this.withCompression);
    }
    
    public java.security.spec.ECPoint getW() {
        return new java.security.spec.ECPoint(this.q.getX().toBigInteger(), this.q.getY().toBigInteger());
    }
    
    public ECPoint getQ() {
        if (this.ecSpec != null) {
            return this.q;
        }
        if (this.q instanceof ECPoint.Fp) {
            return new ECPoint.Fp(null, this.q.getX(), this.q.getY());
        }
        return new ECPoint.F2m(null, this.q.getX(), this.q.getY());
    }
    
    public ECPoint engineGetQ() {
        return this.q;
    }
    
    org.bouncycastle.jce.spec.ECParameterSpec engineGetSpec() {
        if (this.ecSpec != null) {
            return EC5Util.convertSpec(this.ecSpec, this.withCompression);
        }
        return BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa();
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        final String property = System.getProperty("line.separator");
        sb.append("EC Public Key").append(property);
        sb.append("            X: ").append(this.q.getX().toBigInteger().toString(16)).append(property);
        sb.append("            Y: ").append(this.q.getY().toBigInteger().toString(16)).append(property);
        return sb.toString();
    }
    
    public void setPointFormat(final String anotherString) {
        this.withCompression = !"UNCOMPRESSED".equalsIgnoreCase(anotherString);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof BCDSTU4145PublicKey)) {
            return false;
        }
        final BCDSTU4145PublicKey bcdstu4145PublicKey = (BCDSTU4145PublicKey)o;
        return this.engineGetQ().equals(bcdstu4145PublicKey.engineGetQ()) && this.engineGetSpec().equals(bcdstu4145PublicKey.engineGetSpec());
    }
    
    @Override
    public int hashCode() {
        return this.engineGetQ().hashCode() ^ this.engineGetSpec().hashCode();
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.populateFromPubKeyInfo(SubjectPublicKeyInfo.getInstance(ASN1Primitive.fromByteArray((byte[])objectInputStream.readObject())));
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
}
