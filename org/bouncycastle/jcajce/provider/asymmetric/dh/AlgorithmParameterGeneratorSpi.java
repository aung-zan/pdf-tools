// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.dh;

import org.bouncycastle.crypto.params.DHParameters;
import javax.crypto.spec.DHParameterSpec;
import org.bouncycastle.crypto.generators.DHParametersGenerator;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.spec.DHGenParameterSpec;
import java.security.spec.AlgorithmParameterSpec;
import java.security.SecureRandom;

public class AlgorithmParameterGeneratorSpi extends java.security.AlgorithmParameterGeneratorSpi
{
    protected SecureRandom random;
    protected int strength;
    private int l;
    
    public AlgorithmParameterGeneratorSpi() {
        this.strength = 1024;
        this.l = 0;
    }
    
    @Override
    protected void engineInit(final int strength, final SecureRandom random) {
        this.strength = strength;
        this.random = random;
    }
    
    @Override
    protected void engineInit(final AlgorithmParameterSpec algorithmParameterSpec, final SecureRandom random) throws InvalidAlgorithmParameterException {
        if (!(algorithmParameterSpec instanceof DHGenParameterSpec)) {
            throw new InvalidAlgorithmParameterException("DH parameter generator requires a DHGenParameterSpec for initialisation");
        }
        final DHGenParameterSpec dhGenParameterSpec = (DHGenParameterSpec)algorithmParameterSpec;
        this.strength = dhGenParameterSpec.getPrimeSize();
        this.l = dhGenParameterSpec.getExponentSize();
        this.random = random;
    }
    
    @Override
    protected AlgorithmParameters engineGenerateParameters() {
        final DHParametersGenerator dhParametersGenerator = new DHParametersGenerator();
        if (this.random != null) {
            dhParametersGenerator.init(this.strength, 20, this.random);
        }
        else {
            dhParametersGenerator.init(this.strength, 20, new SecureRandom());
        }
        final DHParameters generateParameters = dhParametersGenerator.generateParameters();
        AlgorithmParameters instance;
        try {
            instance = AlgorithmParameters.getInstance("DH", "BC");
            instance.init(new DHParameterSpec(generateParameters.getP(), generateParameters.getG(), this.l));
        }
        catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
        return instance;
    }
}
