// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.ies;

import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import java.io.IOException;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.AlgorithmParameterSpec;
import org.bouncycastle.jce.spec.IESParameterSpec;

public class AlgorithmParametersSpi extends java.security.AlgorithmParametersSpi
{
    IESParameterSpec currentSpec;
    
    protected boolean isASN1FormatString(final String s) {
        return s == null || s.equals("ASN.1");
    }
    
    @Override
    protected AlgorithmParameterSpec engineGetParameterSpec(final Class clazz) throws InvalidParameterSpecException {
        if (clazz == null) {
            throw new NullPointerException("argument to getParameterSpec must not be null");
        }
        return this.localEngineGetParameterSpec(clazz);
    }
    
    @Override
    protected byte[] engineGetEncoded() {
        try {
            final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
            asn1EncodableVector.add(new DEROctetString(this.currentSpec.getDerivationV()));
            asn1EncodableVector.add(new DEROctetString(this.currentSpec.getEncodingV()));
            asn1EncodableVector.add(new DERInteger(this.currentSpec.getMacKeySize()));
            return new DERSequence(asn1EncodableVector).getEncoded("DER");
        }
        catch (IOException ex) {
            throw new RuntimeException("Error encoding IESParameters");
        }
    }
    
    @Override
    protected byte[] engineGetEncoded(final String s) {
        if (this.isASN1FormatString(s) || s.equalsIgnoreCase("X.509")) {
            return this.engineGetEncoded();
        }
        return null;
    }
    
    protected AlgorithmParameterSpec localEngineGetParameterSpec(final Class clazz) throws InvalidParameterSpecException {
        if (clazz == IESParameterSpec.class) {
            return this.currentSpec;
        }
        throw new InvalidParameterSpecException("unknown parameter spec passed to ElGamal parameters object.");
    }
    
    @Override
    protected void engineInit(final AlgorithmParameterSpec algorithmParameterSpec) throws InvalidParameterSpecException {
        if (!(algorithmParameterSpec instanceof IESParameterSpec)) {
            throw new InvalidParameterSpecException("IESParameterSpec required to initialise a IES algorithm parameters object");
        }
        this.currentSpec = (IESParameterSpec)algorithmParameterSpec;
    }
    
    @Override
    protected void engineInit(final byte[] array) throws IOException {
        try {
            final ASN1Sequence asn1Sequence = (ASN1Sequence)ASN1Primitive.fromByteArray(array);
            this.currentSpec = new IESParameterSpec(((ASN1OctetString)asn1Sequence.getObjectAt(0)).getOctets(), ((ASN1OctetString)asn1Sequence.getObjectAt(0)).getOctets(), ((DERInteger)asn1Sequence.getObjectAt(0)).getValue().intValue());
        }
        catch (ClassCastException ex) {
            throw new IOException("Not a valid IES Parameter encoding.");
        }
        catch (ArrayIndexOutOfBoundsException ex2) {
            throw new IOException("Not a valid IES Parameter encoding.");
        }
    }
    
    @Override
    protected void engineInit(final byte[] array, final String str) throws IOException {
        if (this.isASN1FormatString(str) || str.equalsIgnoreCase("X.509")) {
            this.engineInit(array);
            return;
        }
        throw new IOException("Unknown parameter format " + str);
    }
    
    @Override
    protected String engineToString() {
        return "IES Parameters";
    }
}
