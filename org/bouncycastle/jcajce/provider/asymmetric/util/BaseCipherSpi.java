// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.util;

import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchProviderException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.security.KeyFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.crypto.InvalidCipherTextException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.security.InvalidKeyException;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;
import java.security.Key;
import javax.crypto.spec.RC5ParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.crypto.Wrapper;
import java.security.AlgorithmParameters;
import javax.crypto.CipherSpi;

public abstract class BaseCipherSpi extends CipherSpi
{
    private Class[] availableSpecs;
    protected AlgorithmParameters engineParams;
    protected Wrapper wrapEngine;
    private int ivSize;
    private byte[] iv;
    
    protected BaseCipherSpi() {
        this.availableSpecs = new Class[] { IvParameterSpec.class, PBEParameterSpec.class, RC2ParameterSpec.class, RC5ParameterSpec.class };
        this.engineParams = null;
        this.wrapEngine = null;
    }
    
    @Override
    protected int engineGetBlockSize() {
        return 0;
    }
    
    @Override
    protected byte[] engineGetIV() {
        return null;
    }
    
    @Override
    protected int engineGetKeySize(final Key key) {
        return key.getEncoded().length;
    }
    
    @Override
    protected int engineGetOutputSize(final int n) {
        return -1;
    }
    
    @Override
    protected AlgorithmParameters engineGetParameters() {
        return null;
    }
    
    @Override
    protected void engineSetMode(final String str) throws NoSuchAlgorithmException {
        throw new NoSuchAlgorithmException("can't support mode " + str);
    }
    
    @Override
    protected void engineSetPadding(final String str) throws NoSuchPaddingException {
        throw new NoSuchPaddingException("Padding " + str + " unknown.");
    }
    
    @Override
    protected byte[] engineWrap(final Key key) throws IllegalBlockSizeException, InvalidKeyException {
        final byte[] encoded = key.getEncoded();
        if (encoded == null) {
            throw new InvalidKeyException("Cannot wrap key, null encoding.");
        }
        try {
            if (this.wrapEngine == null) {
                return this.engineDoFinal(encoded, 0, encoded.length);
            }
            return this.wrapEngine.wrap(encoded, 0, encoded.length);
        }
        catch (BadPaddingException ex) {
            throw new IllegalBlockSizeException(ex.getMessage());
        }
    }
    
    @Override
    protected Key engineUnwrap(final byte[] array, final String s, final int i) throws InvalidKeyException {
        byte[] encodedKey;
        try {
            if (this.wrapEngine == null) {
                encodedKey = this.engineDoFinal(array, 0, array.length);
            }
            else {
                encodedKey = this.wrapEngine.unwrap(array, 0, array.length);
            }
        }
        catch (InvalidCipherTextException ex) {
            throw new InvalidKeyException(ex.getMessage());
        }
        catch (BadPaddingException ex2) {
            throw new InvalidKeyException(ex2.getMessage());
        }
        catch (IllegalBlockSizeException ex3) {
            throw new InvalidKeyException(ex3.getMessage());
        }
        if (i == 3) {
            return new SecretKeySpec(encodedKey, s);
        }
        if (s.equals("") && i == 2) {
            try {
                final PrivateKeyInfo instance = PrivateKeyInfo.getInstance(encodedKey);
                final PrivateKey privateKey = BouncyCastleProvider.getPrivateKey(instance);
                if (privateKey != null) {
                    return privateKey;
                }
                throw new InvalidKeyException("algorithm " + instance.getPrivateKeyAlgorithm().getAlgorithm() + " not supported");
            }
            catch (Exception ex7) {
                throw new InvalidKeyException("Invalid key encoding.");
            }
        }
        try {
            final KeyFactory instance2 = KeyFactory.getInstance(s, "BC");
            if (i == 1) {
                return instance2.generatePublic(new X509EncodedKeySpec(encodedKey));
            }
            if (i == 2) {
                return instance2.generatePrivate(new PKCS8EncodedKeySpec(encodedKey));
            }
        }
        catch (NoSuchProviderException ex4) {
            throw new InvalidKeyException("Unknown key type " + ex4.getMessage());
        }
        catch (NoSuchAlgorithmException ex5) {
            throw new InvalidKeyException("Unknown key type " + ex5.getMessage());
        }
        catch (InvalidKeySpecException ex6) {
            throw new InvalidKeyException("Unknown key type " + ex6.getMessage());
        }
        throw new InvalidKeyException("Unknown key type " + i);
    }
}
