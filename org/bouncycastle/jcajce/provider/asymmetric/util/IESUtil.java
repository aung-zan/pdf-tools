// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.util;

import org.bouncycastle.jce.spec.IESParameterSpec;
import org.bouncycastle.crypto.engines.IESEngine;

public class IESUtil
{
    public static IESParameterSpec guessParameterSpec(final IESEngine iesEngine) {
        if (iesEngine.getCipher() == null) {
            return new IESParameterSpec(null, null, 128);
        }
        if (iesEngine.getCipher().getUnderlyingCipher().getAlgorithmName().equals("DES") || iesEngine.getCipher().getUnderlyingCipher().getAlgorithmName().equals("RC2") || iesEngine.getCipher().getUnderlyingCipher().getAlgorithmName().equals("RC5-32") || iesEngine.getCipher().getUnderlyingCipher().getAlgorithmName().equals("RC5-64")) {
            return new IESParameterSpec(null, null, 64, 64);
        }
        if (iesEngine.getCipher().getUnderlyingCipher().getAlgorithmName().equals("SKIPJACK")) {
            return new IESParameterSpec(null, null, 80, 80);
        }
        if (iesEngine.getCipher().getUnderlyingCipher().getAlgorithmName().equals("GOST28147")) {
            return new IESParameterSpec(null, null, 256, 256);
        }
        return new IESParameterSpec(null, null, 128, 128);
    }
}
