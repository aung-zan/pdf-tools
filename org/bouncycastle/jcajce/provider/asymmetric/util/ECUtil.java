// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.util;

import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.asn1.cryptopro.ECGOST3410NamedCurves;
import org.bouncycastle.asn1.teletrust.TeleTrusTNamedCurves;
import org.bouncycastle.asn1.nist.NISTNamedCurves;
import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X962NamedCurves;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import java.security.PrivateKey;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.security.InvalidKeyException;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.security.PublicKey;

public class ECUtil
{
    static int[] convertMidTerms(final int[] array) {
        final int[] array2 = new int[3];
        if (array.length == 1) {
            array2[0] = array[0];
        }
        else {
            if (array.length != 3) {
                throw new IllegalArgumentException("Only Trinomials and pentanomials supported");
            }
            if (array[0] < array[1] && array[0] < array[2]) {
                array2[0] = array[0];
                if (array[1] < array[2]) {
                    array2[1] = array[1];
                    array2[2] = array[2];
                }
                else {
                    array2[1] = array[2];
                    array2[2] = array[1];
                }
            }
            else if (array[1] < array[2]) {
                array2[0] = array[1];
                if (array[0] < array[2]) {
                    array2[1] = array[0];
                    array2[2] = array[2];
                }
                else {
                    array2[1] = array[2];
                    array2[2] = array[0];
                }
            }
            else {
                array2[0] = array[2];
                if (array[0] < array[1]) {
                    array2[1] = array[0];
                    array2[2] = array[1];
                }
                else {
                    array2[1] = array[1];
                    array2[2] = array[0];
                }
            }
        }
        return array2;
    }
    
    public static AsymmetricKeyParameter generatePublicKeyParameter(final PublicKey publicKey) throws InvalidKeyException {
        if (publicKey instanceof ECPublicKey) {
            final ECPublicKey ecPublicKey = (ECPublicKey)publicKey;
            final ECParameterSpec parameters = ecPublicKey.getParameters();
            if (parameters == null) {
                final ECParameterSpec ecImplicitlyCa = BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa();
                return new ECPublicKeyParameters(((BCECPublicKey)ecPublicKey).engineGetQ(), new ECDomainParameters(ecImplicitlyCa.getCurve(), ecImplicitlyCa.getG(), ecImplicitlyCa.getN(), ecImplicitlyCa.getH(), ecImplicitlyCa.getSeed()));
            }
            return new ECPublicKeyParameters(ecPublicKey.getQ(), new ECDomainParameters(parameters.getCurve(), parameters.getG(), parameters.getN(), parameters.getH(), parameters.getSeed()));
        }
        else {
            if (publicKey instanceof java.security.interfaces.ECPublicKey) {
                final java.security.interfaces.ECPublicKey ecPublicKey2 = (java.security.interfaces.ECPublicKey)publicKey;
                final ECParameterSpec convertSpec = EC5Util.convertSpec(ecPublicKey2.getParams(), false);
                return new ECPublicKeyParameters(EC5Util.convertPoint(ecPublicKey2.getParams(), ecPublicKey2.getW(), false), new ECDomainParameters(convertSpec.getCurve(), convertSpec.getG(), convertSpec.getN(), convertSpec.getH(), convertSpec.getSeed()));
            }
            try {
                final byte[] encoded = publicKey.getEncoded();
                if (encoded == null) {
                    throw new InvalidKeyException("no encoding for EC public key");
                }
                final PublicKey publicKey2 = BouncyCastleProvider.getPublicKey(SubjectPublicKeyInfo.getInstance(encoded));
                if (publicKey2 instanceof java.security.interfaces.ECPublicKey) {
                    return generatePublicKeyParameter(publicKey2);
                }
            }
            catch (Exception ex) {
                throw new InvalidKeyException("cannot identify EC public key: " + ex.toString());
            }
            throw new InvalidKeyException("cannot identify EC public key.");
        }
    }
    
    public static AsymmetricKeyParameter generatePrivateKeyParameter(final PrivateKey privateKey) throws InvalidKeyException {
        if (privateKey instanceof ECPrivateKey) {
            final ECPrivateKey ecPrivateKey = (ECPrivateKey)privateKey;
            ECParameterSpec ecParameterSpec = ecPrivateKey.getParameters();
            if (ecParameterSpec == null) {
                ecParameterSpec = BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa();
            }
            return new ECPrivateKeyParameters(ecPrivateKey.getD(), new ECDomainParameters(ecParameterSpec.getCurve(), ecParameterSpec.getG(), ecParameterSpec.getN(), ecParameterSpec.getH(), ecParameterSpec.getSeed()));
        }
        if (privateKey instanceof java.security.interfaces.ECPrivateKey) {
            final java.security.interfaces.ECPrivateKey ecPrivateKey2 = (java.security.interfaces.ECPrivateKey)privateKey;
            final ECParameterSpec convertSpec = EC5Util.convertSpec(ecPrivateKey2.getParams(), false);
            return new ECPrivateKeyParameters(ecPrivateKey2.getS(), new ECDomainParameters(convertSpec.getCurve(), convertSpec.getG(), convertSpec.getN(), convertSpec.getH(), convertSpec.getSeed()));
        }
        try {
            final byte[] encoded = privateKey.getEncoded();
            if (encoded == null) {
                throw new InvalidKeyException("no encoding for EC private key");
            }
            final PrivateKey privateKey2 = BouncyCastleProvider.getPrivateKey(PrivateKeyInfo.getInstance(encoded));
            if (privateKey2 instanceof java.security.interfaces.ECPrivateKey) {
                return generatePrivateKeyParameter(privateKey2);
            }
        }
        catch (Exception ex) {
            throw new InvalidKeyException("cannot identify EC private key: " + ex.toString());
        }
        throw new InvalidKeyException("can't identify EC private key.");
    }
    
    public static ASN1ObjectIdentifier getNamedCurveOid(final String s) {
        ASN1ObjectIdentifier asn1ObjectIdentifier = X962NamedCurves.getOID(s);
        if (asn1ObjectIdentifier == null) {
            asn1ObjectIdentifier = SECNamedCurves.getOID(s);
            if (asn1ObjectIdentifier == null) {
                asn1ObjectIdentifier = NISTNamedCurves.getOID(s);
            }
            if (asn1ObjectIdentifier == null) {
                asn1ObjectIdentifier = TeleTrusTNamedCurves.getOID(s);
            }
            if (asn1ObjectIdentifier == null) {
                asn1ObjectIdentifier = ECGOST3410NamedCurves.getOID(s);
            }
        }
        return asn1ObjectIdentifier;
    }
    
    public static X9ECParameters getNamedCurveByOid(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        X9ECParameters x9ECParameters = X962NamedCurves.getByOID(asn1ObjectIdentifier);
        if (x9ECParameters == null) {
            x9ECParameters = SECNamedCurves.getByOID(asn1ObjectIdentifier);
            if (x9ECParameters == null) {
                x9ECParameters = NISTNamedCurves.getByOID(asn1ObjectIdentifier);
            }
            if (x9ECParameters == null) {
                x9ECParameters = TeleTrusTNamedCurves.getByOID(asn1ObjectIdentifier);
            }
        }
        return x9ECParameters;
    }
    
    public static String getCurveName(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        String s = X962NamedCurves.getName(asn1ObjectIdentifier);
        if (s == null) {
            s = SECNamedCurves.getName(asn1ObjectIdentifier);
            if (s == null) {
                s = NISTNamedCurves.getName(asn1ObjectIdentifier);
            }
            if (s == null) {
                s = TeleTrusTNamedCurves.getName(asn1ObjectIdentifier);
            }
            if (s == null) {
                s = ECGOST3410NamedCurves.getName(asn1ObjectIdentifier);
            }
        }
        return s;
    }
}
