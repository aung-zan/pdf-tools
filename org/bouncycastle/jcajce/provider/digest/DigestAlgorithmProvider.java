// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.digest;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.jcajce.provider.config.ConfigurableProvider;
import org.bouncycastle.jcajce.provider.util.AlgorithmProvider;

abstract class DigestAlgorithmProvider extends AlgorithmProvider
{
    protected void addHMACAlgorithm(final ConfigurableProvider configurableProvider, final String str, final String s, final String s2) {
        final String string = "HMAC" + str;
        configurableProvider.addAlgorithm("Mac." + string, s);
        configurableProvider.addAlgorithm("Alg.Alias.Mac.HMAC-" + str, string);
        configurableProvider.addAlgorithm("Alg.Alias.Mac.HMAC/" + str, string);
        configurableProvider.addAlgorithm("KeyGenerator." + string, s2);
        configurableProvider.addAlgorithm("Alg.Alias.KeyGenerator.HMAC-" + str, string);
        configurableProvider.addAlgorithm("Alg.Alias.KeyGenerator.HMAC/" + str, string);
    }
    
    protected void addHMACAlias(final ConfigurableProvider configurableProvider, final String str, final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        final String string = "HMAC" + str;
        configurableProvider.addAlgorithm("Alg.Alias.Mac." + asn1ObjectIdentifier, string);
        configurableProvider.addAlgorithm("Alg.Alias.KeyGenerator." + asn1ObjectIdentifier, string);
    }
}
