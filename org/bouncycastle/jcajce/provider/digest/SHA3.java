// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.digest;

import org.bouncycastle.jcajce.provider.config.ConfigurableProvider;
import org.bouncycastle.crypto.CipherKeyGenerator;
import org.bouncycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.jcajce.provider.symmetric.util.BaseMac;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.SHA3Digest;

public class SHA3
{
    private SHA3() {
    }
    
    public static class Digest224 extends DigestSHA3
    {
        public Digest224() {
            super(224);
        }
    }
    
    public static class Digest256 extends DigestSHA3
    {
        public Digest256() {
            super(256);
        }
    }
    
    public static class DigestSHA3 extends BCMessageDigest implements Cloneable
    {
        public DigestSHA3(final int n) {
            super(new SHA3Digest(n));
        }
        
        @Override
        public Object clone() throws CloneNotSupportedException {
            final BCMessageDigest bcMessageDigest = (BCMessageDigest)super.clone();
            bcMessageDigest.digest = new SHA3Digest((SHA3Digest)this.digest);
            return bcMessageDigest;
        }
    }
    
    public static class Digest384 extends DigestSHA3
    {
        public Digest384() {
            super(384);
        }
    }
    
    public static class Digest512 extends DigestSHA3
    {
        public Digest512() {
            super(512);
        }
    }
    
    public static class HashMac224 extends BaseMac
    {
        public HashMac224() {
            super(new HMac(new SHA3Digest(224)));
        }
    }
    
    public static class HashMac256 extends BaseMac
    {
        public HashMac256() {
            super(new HMac(new SHA3Digest(256)));
        }
    }
    
    public static class HashMac384 extends BaseMac
    {
        public HashMac384() {
            super(new HMac(new SHA3Digest(384)));
        }
    }
    
    public static class HashMac512 extends BaseMac
    {
        public HashMac512() {
            super(new HMac(new SHA3Digest(512)));
        }
    }
    
    public static class KeyGenerator224 extends BaseKeyGenerator
    {
        public KeyGenerator224() {
            super("HMACSHA3-224", 224, new CipherKeyGenerator());
        }
    }
    
    public static class KeyGenerator256 extends BaseKeyGenerator
    {
        public KeyGenerator256() {
            super("HMACSHA3-256", 256, new CipherKeyGenerator());
        }
    }
    
    public static class KeyGenerator384 extends BaseKeyGenerator
    {
        public KeyGenerator384() {
            super("HMACSHA3-384", 384, new CipherKeyGenerator());
        }
    }
    
    public static class KeyGenerator512 extends BaseKeyGenerator
    {
        public KeyGenerator512() {
            super("HMACSHA3-512", 512, new CipherKeyGenerator());
        }
    }
    
    public static class Mappings extends DigestAlgorithmProvider
    {
        private static final String PREFIX;
        
        @Override
        public void configure(final ConfigurableProvider configurableProvider) {
            configurableProvider.addAlgorithm("MessageDigest.SHA3-224", Mappings.PREFIX + "$Digest224");
            configurableProvider.addAlgorithm("MessageDigest.SHA3-256", Mappings.PREFIX + "$Digest256");
            configurableProvider.addAlgorithm("MessageDigest.SHA3-384", Mappings.PREFIX + "$Digest384");
            configurableProvider.addAlgorithm("MessageDigest.SHA3-512", Mappings.PREFIX + "$Digest512");
            this.addHMACAlgorithm(configurableProvider, "SHA3-224", Mappings.PREFIX + "$HashMac224", Mappings.PREFIX + "$KeyGenerator224");
            this.addHMACAlgorithm(configurableProvider, "SHA3-256", Mappings.PREFIX + "$HashMac256", Mappings.PREFIX + "$KeyGenerator256");
            this.addHMACAlgorithm(configurableProvider, "SHA3-384", Mappings.PREFIX + "$HashMac384", Mappings.PREFIX + "$KeyGenerator384");
            this.addHMACAlgorithm(configurableProvider, "SHA3-512", Mappings.PREFIX + "$HashMac512", Mappings.PREFIX + "$KeyGenerator512");
        }
        
        static {
            PREFIX = SHA3.class.getName();
        }
    }
}
