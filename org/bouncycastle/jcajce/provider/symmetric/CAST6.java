// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.symmetric;

import org.bouncycastle.jcajce.provider.config.ConfigurableProvider;
import org.bouncycastle.crypto.CipherKeyGenerator;
import org.bouncycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.macs.GMac;
import org.bouncycastle.crypto.modes.GCMBlockCipher;
import org.bouncycastle.jcajce.provider.symmetric.util.BaseMac;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.engines.CAST6Engine;
import org.bouncycastle.jcajce.provider.symmetric.util.BaseBlockCipher;

public final class CAST6
{
    private CAST6() {
    }
    
    public static class ECB extends BaseBlockCipher
    {
        public ECB() {
            super(new CAST6Engine());
        }
    }
    
    public static class GMAC extends BaseMac
    {
        public GMAC() {
            super(new GMac(new GCMBlockCipher(new CAST6Engine())));
        }
    }
    
    public static class KeyGen extends BaseKeyGenerator
    {
        public KeyGen() {
            super("CAST6", 256, new CipherKeyGenerator());
        }
    }
    
    public static class Mappings extends SymmetricAlgorithmProvider
    {
        private static final String PREFIX;
        
        @Override
        public void configure(final ConfigurableProvider configurableProvider) {
            configurableProvider.addAlgorithm("Cipher.CAST6", Mappings.PREFIX + "$ECB");
            configurableProvider.addAlgorithm("KeyGenerator.CAST6", Mappings.PREFIX + "$KeyGen");
            this.addGMacAlgorithm(configurableProvider, "CAST6", Mappings.PREFIX + "$GMAC", Mappings.PREFIX + "$KeyGen");
        }
        
        static {
            PREFIX = CAST6.class.getName();
        }
    }
}
