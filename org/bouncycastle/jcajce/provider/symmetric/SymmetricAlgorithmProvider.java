// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.symmetric;

import org.bouncycastle.jcajce.provider.config.ConfigurableProvider;
import org.bouncycastle.jcajce.provider.util.AlgorithmProvider;

abstract class SymmetricAlgorithmProvider extends AlgorithmProvider
{
    protected void addGMacAlgorithm(final ConfigurableProvider configurableProvider, final String s, final String s2, final String s3) {
        configurableProvider.addAlgorithm("Mac." + s + "-GMAC", s2);
        configurableProvider.addAlgorithm("Alg.Alias.Mac." + s + "GMAC", s + "-GMAC");
        configurableProvider.addAlgorithm("KeyGenerator." + s + "-GMAC", s3);
        configurableProvider.addAlgorithm("Alg.Alias.KeyGenerator." + s + "GMAC", s + "-GMAC");
    }
}
