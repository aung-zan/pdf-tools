// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.symmetric;

import org.bouncycastle.jcajce.provider.config.ConfigurableProvider;
import org.bouncycastle.jcajce.provider.util.AlgorithmProvider;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.jcajce.provider.symmetric.util.BaseMac;

public final class SipHash
{
    private SipHash() {
    }
    
    public static class Mac extends BaseMac
    {
        public Mac() {
            super(new org.bouncycastle.crypto.macs.SipHash());
        }
    }
    
    public static class Mac48 extends BaseMac
    {
        public Mac48() {
            super(new org.bouncycastle.crypto.macs.SipHash(4, 8));
        }
    }
    
    public static class Mappings extends AlgorithmProvider
    {
        private static final String PREFIX;
        
        @Override
        public void configure(final ConfigurableProvider configurableProvider) {
            configurableProvider.addAlgorithm("Mac.SIPHASH", Mappings.PREFIX + "$Mac");
            configurableProvider.addAlgorithm("Alg.Alias.Mac.SIPHASH-2-4", "SIPHASH");
            configurableProvider.addAlgorithm("Mac.SIPHASH-4-8", Mappings.PREFIX + "$Mac48");
        }
        
        static {
            PREFIX = SipHash.class.getName();
        }
    }
}
