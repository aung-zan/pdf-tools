// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.dvcs;

public class DVCSException extends Exception
{
    private static final long serialVersionUID = 389345256020131488L;
    private Throwable cause;
    
    public DVCSException(final String message) {
        super(message);
    }
    
    public DVCSException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
